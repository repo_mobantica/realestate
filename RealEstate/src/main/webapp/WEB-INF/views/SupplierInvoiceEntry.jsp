<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | VENDER INVOICE</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


  <!-- Google Font -->
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#enquirydbStatusSpan').delay(1000).fadeOut();
      });
    </script>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

  <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        VENDER INVOICE:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Account</a></li>
        <li class="active">VENDER INVOICE</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="supplierbillpaymentform" action="${pageContext.request.contextPath}/SupplierInvoiceEntry" onSubmit="return validate()" method="post">
    <section class="content">

 
      <div class="box box-default">
      <div class="box box-danger"> </div>  
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
		
	<div class="box-body">
       <div class="row">
       
         <input type="hidden" id="supplierId" name="supplierId" value="${purchesedList[0].supplierId}">
          <input type="hidden" id="materialsPurchasedId" name="materialsPurchasedId" value="${purchesedList[0].materialsPurchasedId}">
          <input type="hidden" id="storeId" name="storeId" value="${purchesedList[0].storeId}">
            <input type="hidden" id="supplierBillPaymentHistoriId" name="supplierBillPaymentHistoriId" value="${supplierBillPaymentHistoriId}">
           
          <input type="hidden" id="totalAmount" name="totalAmount" value="${purchesedList[0].totalAmount}">
          <input type="hidden" id="supplierPaidAmount" name="supplierPaidAmount" value="${supplierPaidAmount}">
               
               
           <div class="col-xs-4">
            <h4><label>Purchase Id :</label> ${purchesedList[0].materialsPurchasedId}</h4>
           </div>
              
              
           <div class="col-xs-6">
            <h4><label>Store Name :</label> ${purchesedList[0].storeName}</h4>
           </div>
               
           </div>
 <!--       </div>     
               
           
	<div class="box-body"> -->
       <div class="row">
            
           <div class="col-xs-4">
            <h4><label>Vender Firm :</label> ${supplierfirmName}</h4>
           </div>
              
              
           <div class="col-xs-4">
            <h4><label>Firm PAN No :</label> ${firmpanNumber}</h4>
           </div>
              
           <div class="col-xs-4">
            <h4><label>Firm GST No :</label> ${firmgstNumber}</h4>
           </div>
                       
         </div>  
       <div class="row">          
            
           <div class="col-xs-4">
            <h4><label>Total Item Price :</label> ${purchesedList[0].totalPrice}</h4>
           </div>
              
           <div class="col-xs-4">
            <h4><label>Total Disct Price :</label> ${purchesedList[0].totalDiscount}</h4>
           </div>
           
           <div class="col-xs-4">
            <h4><label>Total GST Price :</label> ${purchesedList[0].totalGstAmount}</h4>
           </div>
                 
         </div>  
       <div class="row">  
           <div class="col-xs-4">
            <h4><label>Grand Total Price :</label> ${purchesedList[0].totalAmount}</h4>
           </div>
            
           <div class="col-xs-4">
            <h4><label>Challan No :</label> ${challanNo}</h4>
           </div>
           
	  </div>
	  </div>
	<div class="box-body">
       <div class="row">  
        
           <div class="col-xs-3">
             <label for="">Invoice No</label><label class="text-red">* </label>
                <input type="text" class="form-control" id="invoiceNo" name="invoiceNo"  value="" >
                      <span id="invoiceNoSpan" style="color:#FF0000"></span>
              </div>
           
	  </div>
	  
	</div>	
	
</div>	

  <div class="col-md-12">
		
	 <div class="box-body">
        <div class="row">
          
          <div class="col-xs-12">
		  <h4><label>Item Details :</label></h4>
                
              <table id="materialDetailsTable" class="table table-bordered">
                <thead>
                <tr bgcolor="#4682B4">
               		<th style="width:20px">Sr.No</th>
               		<td hidden="hidden"> </td> 
                  	<th>Sub-Category</th>
                  	<th>Item Name</th>
                  	<th>Quantity</th>
                  	<th>Size</th>
                  	<th>Unit</th>
                    <th>Brand</th>
                   
                    <th>Rate</th>
                    <th>Total</th>
                    <th>Discount%</th>
                    <th>Discount Amount</th>
                    <th>GST%</th>
                    <th>GST Amount</th>
                    <th>Grand Total</th>
                  </tr>
                </thead>
                
                <tbody>
            
                  <c:forEach items="${purchesedMaterialList}" var="purchesedMaterialList" varStatus="loopStatus">
                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                      <td>${loopStatus.index+1}</td>
                      <td hidden="hidden">${purchesedMaterialList.materialPurchesHistoriId}</td>
                      <td>${purchesedMaterialList.subsuppliertypeName}</td>
                      <td>${purchesedMaterialList.itemName}</td>
                      <td>${purchesedMaterialList.previousQuantity}</td> 
                      <td>${purchesedMaterialList.itenSize}</td>
                      <td>${purchesedMaterialList.itemUnit}</td>
                      <td>${purchesedMaterialList.itemBrandName}</td>
                      
                      <td contenteditable='true' onkeyup="javascript:calcuate(${loopStatus.index+1});">${purchesedMaterialList.rate}</td>
                      <td>${purchesedMaterialList.total}</td>
                  	  <td contenteditable='true' onkeyup="javascript:calcuate(${loopStatus.index+1});">${purchesedMaterialList.discountPer}</td>
                      <td>${purchesedMaterialList.discountAmount}</td>
                      <td contenteditable='true' onkeyup="javascript:calcuate(${loopStatus.index+1});">${purchesedMaterialList.gstPer}</td>
                      <td>${purchesedMaterialList.gstAmount}</td>
                      <td>${purchesedMaterialList.grandTotal}</td>
                      </tr>
				 </c:forEach>
		       <tr>
			          <td></td>
                      <td hidden="hidden"></td>
                      <td></td>
                      <td></td> 
                      <td></td> 
                      <td></td>
                      <td></td>
                      <td></td>
                      <td><label>Total</label></td>
                      <td id="total1">${purchesedList[0].totalPrice}</td> 
                      <td></td> 
                      <td id="discountAmount1">${purchesedList[0].totalDiscount}</td>
                      <td></td> 
                      <td id="gstAmount1">${purchesedList[0].totalGstAmount}</td> 
                      <td id="grandTotal1"><label> ${purchesedList[0].totalAmount}</label></td>
			   
			    </tr>
                </tbody>
      
              </table>
		  </div>              
            
        </div>
      </div>
			
	
          	  <input type="hidden" id="creationDate" name="creationDate" value="">
			  <input type="hidden" id="updateDate" name="updateDate" value="">
			  <input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>">   
		
				
     	<input type="hidden" id="materialPurchesHistoriIds" name="materialPurchesHistoriIds" value="">
     	<input type="hidden" id="itemQuantitys" name="itemQuantitys" value="">
     	<input type="hidden" id="rates" name="rates" value="">
     	<input type="hidden" id="totals" name="totals" value="">
     	<input type="hidden" id="discountPers" name="discountPers" value="">
     	<input type="hidden" id="discountAmounts" name="discountAmounts" value="">
     	<input type="hidden" id="gstPers" name="gstPers" value="">
     	<input type="hidden" id="gstAmounts" name="gstAmounts" value="">
     	<input type="hidden" id="grandTotals" name="grandTotals" value="">
       	
  </div>
</div>
      
	<div class="box-body">
      <div class="row">
      <br/><br/>
        <div class="col-xs-1">
        </div>
        <div class="col-xs-4">
            <div class="col-xs-2">	<a href="SupplierInvoiceMaster"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
			</div> 
		</div>
			    
	   		    <div class="col-xs-2">
	                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
			    </div>
			    
					    
			<div class="col-xs-3">
		  		<button type="button" class="btn btn-info pull-right" onclick="return AddAllPurchesData()">Submit</button>
			</div>
			
			   <div class="col-xs-3" hidden="hidden">
		  			<button type="submit" class="btn btn-info pull-right" name="submit" id="submit">Submit</button>
			    </div> 
			    	     
       </div>
	 </div>
 </div>
        
</div>
</section>
</form>
</div>

   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>

<script>

function calcuate(i)
{
	var oTable = document.getElementById('materialDetailsTable');

	var oCells = oTable.rows.item(i).cells;
	
	var quantity=0;
	quantity=Number(oCells.item(4).innerHTML); 
	
	var rate=0;
	rate=Number(oCells.item(8).innerHTML); 

	var discountPer1=0;
	discountPer1=Number(oCells.item(10).innerHTML); 
	var gstPer1=0;
	gstPer1=Number(oCells.item(12).innerHTML); 
	
	var discountAmount1=0;
	var gstAmount1=0;
	var grandTotal1=0;
	
	var total1=0;
	total1=quantity*rate;
	
	oCells.item(9).innerHTML=total1.toFixed(1);
	discountAmount1=(total1/100)*discountPer1;
	
	var ab=discountAmount1.toFixed(0);
	oCells.item(11).innerHTML=ab;
	var a=0;
	a=total1-discountAmount1;
	gstAmount1=(a/100)*gstPer1;
	
	var xy=gstAmount1.toFixed(0);
	oCells.item(13).innerHTML=xy;
	grandTotal1=a+gstAmount1;
	var pq=grandTotal1.toFixed(0);
	oCells.item(14).innerHTML=pq;
	
	var rowLength = oTable.rows.length;
	var grandTotal=0;
	var total=0;
	var discountAmount=0;
	var gstAmount=0;
	
	for (var j = 1; j < rowLength-1; j++){

		   var oCells = oTable.rows.item(j).cells;

		   var cellLength = oCells.length;
		  
		   total=total+Number(oCells.item(9).innerHTML);
		   discountAmount=discountAmount+Number(oCells.item(11).innerHTML);
		   gstAmount=gstAmount+Number(oCells.item(13).innerHTML);
		   grandTotal=grandTotal+Number(oCells.item(14).innerHTML);
			
			
	}		
	
	document.getElementById('total1').innerHTML = total;
	document.getElementById('discountAmount1').innerHTML = discountAmount;
	document.getElementById('gstAmount1').innerHTML = gstAmount;
	document.getElementById('grandTotal1').innerHTML = grandTotal;
}


function clearall()
{
/* 	$('#invoiceNoSpan').html('');
	$('#amountSpan').html('');
	$('#paymentModeSpan').html('');
	$('#refNumberSpan').html('');
	$('#narrationSpan').html(''); */

}

function validate()
{
	$('#invoiceNoSpan').html('');

	if(document.supplierbillpaymentform.invoiceNo.value=="")
	{
		 $('#invoiceNoSpan').html('Please, enter invoice No..!');
		document.supplierbillpaymentform.invoiceNo.focus();
		return false;
	}
	else if(document.supplierbillpaymentform.invoiceNo.value.match(/^[\s]+$/))
	{
		$('#invoiceNoSpan').html('Please, enter valid invoice No..!');
		document.supplierbillpaymentform.invoiceNo.value="";
		document.supplierbillpaymentform.invoiceNo.focus();
		return false; 	
	}
	
/* 
	if(document.supplierbillpaymentform.companyBankId.value=="Default")
	{
		$('#companyBankIdSpan').html('Please, select company A/c No..!');
		document.supplierbillpaymentform.companyBankId.focus();
		return false;
	}

	if(document.supplierbillpaymentform.amount.value=="")
	{
		 $('#amountSpan').html('Please, enter payment amount..!');
		document.supplierbillpaymentform.amount.focus();
		return false;
	}
	else if(document.supplierbillpaymentform.amount.value.match(/^[\s]+$/))
	{
		$('#amountSpan').html('Please, enter valid payment amount..!');
		document.supplierbillpaymentform.amount.value="";
		document.supplierbillpaymentform.amount.focus();
		return false; 	
	}
	else if(!document.supplierbillpaymentform.amount.value.match(/^[0-9]+$/))
	{
		$('#amountSpan').html('Please, enter valid payment amount..!');
		//document.supplierbillpaymentform.employeeBankacno.value="";
		document.supplierbillpaymentform.amount.focus();
		return false;
	}
	

	if(document.supplierbillpaymentform.paymentMode.value=="Default")
	{
		$('#paymentModeSpan').html('Please, select payment mode..!');
		document.supplierbillpaymentform.paymentMode.focus();
		return false;
	}
	
		
	if(document.supplierbillpaymentform.refNumber.value=="")
	{
		 $('#refNumberSpan').html('Please, enter ref. no..!');
		document.supplierbillpaymentform.refNumber.focus();
		return false;
	}
	else if(document.supplierbillpaymentform.refNumber.value.match(/^[\s]+$/))
	{
		$('#refNumberSpan').html('Please, enter valid ref. no..!');
		document.supplierbillpaymentform.refNumber.value="";
		document.supplierbillpaymentform.refNumber.focus();
		return false; 	
	}
	
	if(document.supplierbillpaymentform.narration.value=="")
	{
		 $('#narrationSpan').html('Please, enter narration..!');
		document.supplierbillpaymentform.narration.focus();
		return false;
	}
	else if(document.supplierbillpaymentform.narration.value.match(/^[\s]+$/))
	{
		$('#narrationSpan').html('Please, enter valid narration..!');
		document.supplierbillpaymentform.narration.value="";
		document.supplierbillpaymentform.narration.focus();
		return false; 	
	}
	if(document.supplierbillpaymentform.chartaccountId.value=="Default")
	{
		$('#chartaccountNameSpan').html('Please, select chart account Name..!');
		document.supplierbillpaymentform.chartaccountId.focus();
		return false;
	}
	
	if(document.supplierbillpaymentform.subchartaccountId.value=="Default")
	{
		$('#subchartaccountIdSpan').html('Please, select sub chart account Name..!');
		document.supplierbillpaymentform.subchartaccountId.focus();
		return false;
	}
	 */
}



function AddAllPurchesData()
{
	
	var materialPurchesHistoriId;
	var itemQuantity;
	var rate;
	var total;
	var discountPer;
	var discountAmount;
	var gstPer;
	var gstAmount;
	var grandTotal;
	

	var materialPurchesHistoriIds="";
	var itemQuantitys="";
	var rates="";
	var totals="";
	var discountPers="";
	var discountAmounts="";
	var gstPers="";
	var gstAmounts="";
	var grandTotals="";
	
	
	var oTable = document.getElementById('materialDetailsTable');
	var rowLength = oTable.rows.length;
	for (var i = 1; i < rowLength-1; i++){
		
		calcuate(i);
		   var oCells = oTable.rows.item(i).cells;

		   var cellLength = oCells.length;
		   
		   materialPurchesHistoriId=oCells.item(1).innerHTML;
		   itemQuantity = oCells.item(4).innerHTML;
		   rate = oCells.item(8).innerHTML;
		   total = oCells.item(9).innerHTML;
		   discountPer = oCells.item(10).innerHTML;
		   discountAmount = oCells.item(11).innerHTML;
		   gstPer = oCells.item(12).innerHTML;
		   gstAmount = oCells.item(13).innerHTML;
		   grandTotal = oCells.item(14).innerHTML;
			
			
		   materialPurchesHistoriIds=materialPurchesHistoriIds+materialPurchesHistoriId+"###!###";
		   itemQuantitys=itemQuantitys+itemQuantity+"###!###";
		        rates=rates+rate+"###!###";
		        totals=totals+total+"###!###";
		        discountPers=discountPers+discountPer+"###!###";
		        discountAmounts=discountAmounts+discountAmount+"###!###";
		        gstPers=gstPers+gstPer+"###!###";
		        gstAmounts=gstAmounts+gstAmount+"###!###";
		        grandTotals=grandTotals+grandTotal+"###!###";
		        
		      
		}

		document.supplierbillpaymentform.materialPurchesHistoriIds.value=materialPurchesHistoriIds;
		document.supplierbillpaymentform.itemQuantitys.value=itemQuantitys;
		document.supplierbillpaymentform.rates.value=rates;
		document.supplierbillpaymentform.totals.value=totals;
		document.supplierbillpaymentform.discountPers.value=discountPers;
		document.supplierbillpaymentform.discountAmounts.value=discountAmounts;
		document.supplierbillpaymentform.gstPers.value=gstPers;
		document.supplierbillpaymentform.gstAmounts.value=gstAmounts;
		document.supplierbillpaymentform.grandTotals.value=grandTotals;
	 	
	submitFunction();
}

	
function submitFunction()
{
submit.click();
}
function init()
{
	clearall();
	
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
}



</script>
</body>
</html>

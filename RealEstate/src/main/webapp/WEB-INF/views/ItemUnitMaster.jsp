<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate |Item Unit Master</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>

<div class="wrapper">

     <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Item Unit Master Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Item Unit Master</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="itemunitmasterform" action="${pageContext.request.contextPath}/ItemUnitMaster" onSubmit="return validate()" method="post">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              
              <!-- /.form-group -->
            </div>
            
            <!-- /.col -->
			
          </div>
		   	 
			     <div class="box-body">
              <div class="row">
                 
				  <div class="col-xs-3">
				
            	 </div>
            	 
			
              
              	<div class="col-xs-2">
			        <a href="AddItemUnit"> <button type="button" class="btn btn-success"><i class="fa fa-plus"></i>Add New Item Unit</button></a>
            	</div>
			       
			  </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
              
         </div>
      <!-- /.box -->
	
		<div class="box box-default">
       <div  class="panel box box-danger"></div>
			 <div class="box-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="ItemUnitListTable">
                  <thead>
	                  <tr bgcolor="#4682B4">
	                    <th style="width:300px">Item Unit Name</th>
	                     <th style="width:300px">Item Unit Sort Name</th>
	                    <th style="width:150px">Creation Date</th>
	                    <th style="width:150px">Last update Date</th>
	                    <th style="width:100px">Action</th>
	                  </tr>
                  </thead>
                  <tbody>
                   <c:forEach items="${itemunitList}" var="itemunitList" varStatus="loopStatus">
                   
	                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}" >
	                        <td>${itemunitList.itemunitName}</td>
	                        <td>${itemunitList.itemunitSymbol}</td>
	                        <td>${itemunitList.creationDate}</td>
	                        <td>${itemunitList.updateDate}</td>
	                        
	                        <td><a href="${pageContext.request.contextPath}/EditItemUnit?itemunitId=${itemunitList.itemunitId}" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>
	                      </tr>
	                      
					  </c:forEach>
                 
                 </tbody>
                </table>
              </div>
            </div>
				
      </div>
  </div>   
  </section>
	</form>
    <!-- /.content -->
    
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
       <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<!-- Page script -->
<script>

function ItemUnitSearch()
{

	$("#ItemUnitListTable tr").detach();
	 var itemunitName = $('#itemunitName').val();

	 $.ajax({

		 url : '${pageContext.request.contextPath}/SearchItemUnitListByName',
		type : 'Post',
		data : { itemunitName : itemunitName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
									$('#ItemUnitListTable').append('<tr style="background-color: #4682B4;"><td style="width:150px"><b>Item Unit Id</b></td><td style="width:300px"><b>Item Unit Name</b></td><td style="width:150px"><b>Creation Date</b></td><td style="width:150px"><b>Last update Date</b></td><td style="width:150px"><b>User Name</b></td>');
					
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									$('#ItemUnitListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].itemunitId+'</td><td>'+result[i].itemunitName+'</td><td>'+result[i].creationDate+'</td><td>'+result[i].updateDate+'</td><td>'+result[i].userName+'</td>');
								}
								else
								{
									$('#ItemUnitListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].itemunitId+'</td><td>'+result[i].itemunitName+'</td><td>'+result[i].creationDate+'</td><td>'+result[i].updateDate+'</td><td>'+result[i].userName+'</td>');
								}
							 } 
						}							
						else
						{
							alert("failure111");
						}

					}
		});
	
}

$(function () {
    $('#ItemUnitListTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
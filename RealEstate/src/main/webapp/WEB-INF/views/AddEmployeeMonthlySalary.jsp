<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Add Employee Salary Details</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
    </script>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

   <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
  <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Employee Salary Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="EmployeeSalaryDetails">Employee Salary Details</a></li>
        <li class="active">Add Employee Salary Details</li>
      </ol>
    </section>

    <!-- Main content -->
	<form name="employeeSalaryform" action="${pageContext.request.contextPath}/AddEmployeeMonthlySalary" onSubmit="return validate()" method="post">

    <section class="content">
   
      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
       <div class="panel box box-danger"></div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">

                  <div class="box-body">
                     <div class="row">
                     
	                   <div class="col-xs-3">
	                 		<h4><label>Employee Id : </label> ${employeeList[0].employeeId}</h4> 
				      </div>
				      
	                   <div class="col-xs-12">
	                 		<h4><label>Employee Name : </label> ${employeeList[0].employeefirstName} ${employeeList[0].employeemiddleName} ${employeeList[0].employeelastName}</h4> 
				      </div>  
				      
	                   <div class="col-xs-6">
	                 		<h4><label>Department Name : </label> ${departmentName}</h4> 
				      </div>
				      
	                   <div class="col-xs-6">
	                 		<h4><label>Designation Name : </label> ${designationName}</h4> 
				      </div>
				      
	                   <div class="col-xs-3">
	                 		<h4><label>Email Id : </label> ${employeeList[0].employeeEmailId}</h4> 
				      </div>
				      
	                   <div class="col-xs-9">
	                 		<h4><label>Mobile No. : </label> ${employeeList[0].employeeMobileno}</h4> 
				      </div>
				      
	                   <div class="col-xs-3">
	                 		<h4><label>Basic Pay : </label> ${employeeList[0].employeeBasicpay}</h4> 
				      </div>
				      
	                   <div class="col-xs-3">
	                 		<h4><label>HRA : </label> ${employeeList[0].employeeHra}</h4> 
				      </div>
				      
	                   <div class="col-xs-3">
	                 		<h4><label>DA : </label> ${employeeList[0].employeeDa}</h4> 
				      </div>
				      
	                   <div class="col-xs-3">
	                 		<h4><label>CA : </label> ${employeeList[0].employeeCa}</h4> 
				      </div>
				      
	                   <div class="col-xs-3">
	                 		<h4><label>PF. No : </label> ${employeeList[0].employeePfacno}</h4> 
				      </div>
				       
	                   <div class="col-xs-9">
	                 		<h4><label>ESI No. : </label> ${employeeList[0].employeeEsino}</h4> 
				      </div>
				      
	                   <div class="col-xs-3">
	                 		<h4><label>Bank Name : </label> ${employeeList[0].employeeBankName}</h4> 
				      </div>
				       
	                   <div class="col-xs-3">
	                 		<h4><label>Branch Name : </label> ${employeeList[0].branchName}</h4> 
				      </div>
				       
	                   <div class="col-xs-3">
	                 		<h4><label>IFSC Code : </label> ${employeeList[0].bankifscCode}</h4> 
				      </div>
				       
	                   <div class="col-xs-3">
	                 		<h4><label>Account No. : </label> ${employeeList[0].employeeBankacno}</h4> 
				      </div>
				       
                     </div>
                  </div>   
                   
            	  
            	 <div class="box-body">
	               <div class="row">
	               
	                 <div class="col-xs-2">
					   <label for="month">Salary Month</label> <label class="text-red">* </label>
	                    <select class="form-control" id="month" name="month" onchange="getEmployeeMonthLeave(this.value)">
				    	    <option selected="selected" value="Default">-Select Salary Month-</option>
					  		<option value="1">January</option>
	                    	<option value="2">February</option>
	                    	<option value="3">March</option>
	                    	<option value="4">April</option>
	                    	<option value="5">May</option>
	                    	<option value="6">June</option>
	                    	<option value="7">July</option>
	                    	<option value="8">August</option>
	                    	<option value="9">September</option>
	                    	<option value="10">October</option>
	                    	<option value="11">November</option>
	                    	<option value="12">December</option>
	                  	</select>
	                  	
	                   <span id="monthSpan" style="color:#FF0000"></span>
	                 </div>
	                 
	                 <div class="col-xs-2">
					   <label for="year">Salary Year</label> <label class="text-red">* </label>
					   
	                    <select class="form-control" id="year" name="year" onchange="getEmployeeMonthLeave(this.value)">
				    	    <option selected="selected" value="Default">-Select Salary Year-</option>
					  		<option value="2017">2017</option>
	                    	<option value="2018">2018</option>
	                    	<option value="2019">2019</option>
	                    	<option value="2020">2020</option>
	                    	
	                    	<option value="2021">2021</option>
	                    	<option value="2022">2022</option>
	                    	<option value="2023">2023</option>
	                    	<option value="2024">2024</option>
	                    	<option value="2025">2025</option>
	                    	<option value="2026">2026</option>
	                    	<option value="2027">2027</option>
	                    	<option value="2028">2028</option>
	                    	<option value="2029">2029</option>
	                    	<option value="2030">2030</option>
	                    	
	                    	<option value="2031">2031</option>
	                    	<option value="2032">2032</option>
	                    	<option value="2033">2033</option>
	                    	<option value="2034">2034</option>
	                    	<option value="2035">2035</option>
	                    	<option value="2036">2036</option>
	                    	<option value="2037">2037</option>
	                    	<option value="2038">2038</option>
	                    	<option value="2039">2039</option>
	                    	<option value="2040">2040</option>
	                    	
	                    	<option value="2041">2041</option>
	                    	<option value="2042">2042</option>
	                    	<option value="2043">2043</option>
	                    	<option value="2044">2044</option>
	                    	<option value="2045">2045</option>
	                    	<option value="2046">2046</option>
	                    	<option value="2047">2047</option>
	                    	<option value="2048">2048</option>
	                    	<option value="2049">2049</option>
	                    	<option value="2050">2050</option>
	                    	
	                  	</select>
	                   <span id="yearSpan" style="color:#FF0000"></span>
	                 </div>
			     	 
			     	 <div class="col-xs-2">
			    	  <label for="totalleaveInMonth">Total Leave in Month</label> 
                  	  <input type="text" class="form-control" id="totalleaveInMonth" placeholder="Total Leave Details" name="totalleaveInMonth" readonly="readonly" >
                	  <span id="totalleaveInMonthSpan" style="color:#FF0000"></span>
			        </div>
			        
			     	 <div class="col-xs-2">
			    	  <label for="leaveDeduction">Leave Deduction</label> 
                  	  <input type="text" class="form-control" id="leaveDeduction" placeholder="Provident Fund Amount" name="leaveDeduction"onchange="CalculateNetSalary(this.value)">
                	  <span id="leaveDeductionSpan" style="color:#FF0000"></span>
			        </div>
			        
			     	 <div class="col-xs-2">
			    	  <label for="providentFund">Provident Fund</label> 
                  	  <input type="text" class="form-control" id="providentFund" placeholder="Provident Fund Amount" name="providentFund"onchange="CalculateNetSalary(this.value)">
                	  <span id="providentFundSpan" style="color:#FF0000"></span>
			        </div>
			        
			     </div>
			    </div>
			    
			<div class="box-body">
              <div class="row">
			     	 
			        <div class="col-xs-2">
			     	  <label for="esiAmount">ESI Amount</label>  
                      <input type="text" class="form-control" id="esiAmount" placeholder="ESI Amount" name="esiAmount" onchange="CalculateNetSalary(this.value)">
                      <span id="esiAmountSpan" style="color:#FF0000"></span>
			        </div>
			        
			        <div class="col-xs-2">
			     	  <label for="professionalTax">Professional Tax</label>  
                      <input type="text" class="form-control" id="professionalTax" placeholder="Professional Tax" name="professionalTax" onchange="CalculateNetSalary(this.value)">
                      <span id="professionalTaxSpan" style="color:#FF0000"></span>
			        </div>
			        
			        <div class="col-xs-2">
			     	  <label for="loanAmount">Loan Amount</label>  
                      <input type="text" class="form-control" id="loanAmount" placeholder="Loan Amount" name="loanAmount" onchange="CalculateNetSalary(this.value)">
                      <span id="loanAmountSpan" style="color:#FF0000"></span>
			        </div>
			        
			        <div class="col-xs-2">
			     	  <label for="tsd">TDS/IT Amount</label>  
                      <input type="text" class="form-control" id="tsd" placeholder="TDS" name="tsd" onchange="CalculateNetSalary(this.value)">
                      <span id="tsdSpan" style="color:#FF0000"></span>
			        </div>
			        
			        <div class="col-xs-2">
			     	  <label for="totalDeduction">Total Deduction</label>  
                      <input type="text" class="form-control" id="tsd" placeholder="totalDeduction" name="totalDeduction" readonly="readonly">
                      <span id="totalDeductionSpan" style="color:#FF0000"></span>
			        </div>
			        
			        <div class="col-xs-2">
			     	  <label for="netSalary">Net Salary</label>  
                      <input type="text" class="form-control" id="netSalary" placeholder="Net Salary" name="netSalary" readonly="readonly">
                      <span id="netSalarySpan" style="color:#FF0000"></span>
			        </div>
			        
			         <input type="hidden" id="employeeId" name="employeeId" value="${employeeList[0].employeeId}">
			         <input type="hidden" id="totalAddition" name="totalAddition" value="${totalAddition}">
			         <input type="hidden" id="daysPresent" name="daysPresent" >	
			         
			    </div>
		     </div>
		      
		      
		    <div class="box-body">
              <div class="row">
		    <br/>
	            <div class="col-xs-1">
	            
	            </div>
	            
	            <div class="col-xs-4">
	            <div class="col-xs-2">
                	<a href="EmployeeSalaryDetails"><button type="button" class="btn btn-block btn-primary" value="reset" style="width:90px">Back</button></a>
			    </div>
			    </div>
			    
	   		    <div class="col-xs-2">
	                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
			    </div>
			    
				<div class="col-xs-3">
		  			<button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
			    </div>
			    
			  </div>
			</div>
              
            </div>
          </div>
        </div>
        
     
	 </div>
     </section>
	</form>

  </div>

 <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script>

function CalculateNetSalary()
{
	clearall();
	
	var leaveDeduction = Number($('#leaveDeduction').val());
    var providentFund = Number($('#providentFund').val());
    
	var esiAmount = Number($('#esiAmount').val());
    var professionalTax = Number($('#professionalTax').val());
	var loanAmount = Number($('#loanAmount').val());
    var tsd = Number($('#tsd').val());
    
    var totalDeduction=leaveDeduction+providentFund+esiAmount+professionalTax+loanAmount-tsd;
    var totalAddition= Number($('#totalAddition').val());
    
    var netSalary=totalAddition-totalDeduction;
    
    document.employeeSalaryform.totalDeduction.value=totalDeduction;
    document.employeeSalaryform.netSalary.value=netSalary;
    
}

function clearall()
{

	$('#monthSpan').html('');
	$('#yearSpan').html('');
}

function validate()
{
	clearall();

	if(document.employeeSalaryform.month.value=="Default")
	{
		$('#monthSpan').html('Please, select Salary Month..!');
		document.employeeSalaryform.month.focus();
		return false;
	}
	
    if(document.employeeSalaryform.year.value=="Default")
	{
		$('#yearSpan').html('Please, select Salary Year..!');
		document.employeeSalaryform.year.focus();
		return false;
	}
	
}

function init()
{
	clearall();
	
	 document.employeeSalaryform.month.focus();
}

function getEmployeeMonthLeave()
{
	 var year = $('#year').val();
     var month = $('#month').val();
     var employeeId = $('#employeeId').val();

     document.employeeSalaryform.totalleaveInMonth.value="";
     
	 if(year!="Default" && month!="Default")
		 {
		 $.ajax({
	
			url : '${pageContext.request.contextPath}/getEmployeeMonthLeave',
			type : 'Post',
			data : { employeeId : employeeId, month : month, year : year},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{
								$('#totalleaveInMonth').val(result[0].noOfDays);
								$('#daysPresent').val(result[0].daysPresent);
							} 
							else
							{
								alert("failure111");
							}
	
						}
			});
		 }
}


	
$(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>

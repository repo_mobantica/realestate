<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Flat Status</title>
  <%-- 
   <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/dist/img/loginlogo.png" />
   --%>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
 <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
 
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
    		
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

    <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Flat Status Details:
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Report</a></li>
        <li class="active">Flat Status</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="flatmasterform" action="${pageContext.request.contextPath}/FlatStatus"  onSubmit="return validate()" method="post">
    <section class="content">
   
     
      <div class="box box-default">
        
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
      
          <div class="box-body">
          <div class="row">
          
                 <div class="col-md-3">
                  <label>Project</label><label class="text-red">* </label>
                   <select class="form-control" name="projectId" id="projectId" onchange="getBuldingList(this.value)">
                  	 <option selected="selected" value="Default">-Select Project-</option>
                   	  <c:forEach var="projectList" items="${projectList}">
                      <option value="${projectList.projectId}">${projectList.projectName}</option>
				     </c:forEach>
				   </select>
				   <span id="projectIdSpan" style="color:#FF0000"></span>
                  </div>
                  
                   <div class="col-md-2">
                     <label>Project Building</label><label class="text-red">* </label>
                     <select class="form-control" name="buildingId" id="buildingId" onchange="getWingNameList(this.value)">
				  	 <option selected="selected" value="Default">-Select Project Building-</option>
                     </select>
                     <span id="buildingIdSpan" style="color:#FF0000"></span>
                  </div>
                  
                  <div class="col-xs-2">
			     	 <label>Wing </label> <label class="text-red">* </label>
             		 <select class="form-control" name="wingId" id="wingId" >
				 	 <option selected="selected" value="Default">-Select Wing Name-</option>
                    </select>
                    <span id="wingIdSpan" style="color:#FF0000"></span>
                   </div>
                   
                 <div class="col-xs-1">
                </div>
                
                <div class="col-xs-1">
			      <br/>
			      
			  				<button type="submit" class="btn btn-info pull-right" name="submit">Search</button>
			      	<!-- 
			      	<button type="button" class="btn btn-success" onclick="SearchAllFlat()">Search</button>
			      	 -->
            	  </div>
            	  	<div class="col-xs-1">
            	  	<br/>
			  			<a href="FlatStatus">  <button type="button" class="btn btn-default" value="reset" style="width:90px"> Reset</button></a>
              		</div> 
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
      
	
				
<div class="box box-default">

	<div class="box-body">
		<div class="col-xs-8">
		</div>
		
		<div class="col-xs-2">
		 <i class="fa fa-text-width"></i> <label>Instructions</label>
		 </br><font size="4"><label class="label label-danger"> .</label> - Sold</font>
		 </br>  <font size="4"><label class="label label-success">.</label> - UnSold</font>
		 </div>	
		 <div class="col-xs-2">
		<label>.</label>
		 </br><font size="4"><label class="label label-info"> .</label> - Agreement</font>
		 </br>  <font size="4"><label class="label label-warning">.</label> - Hold</font>
		 </div>	
		 																
	</div>
          	 	
<div class="box-body">

 <div class="table-responsive">
     
 <font size="4"> <label id="setProjectName">Project Name :</label> </font>	 <label>${projectName}</label> &nbsp; &nbsp;
 <font size="4"> <label>Building Name : </label> </font>	<label>${buildingName}</label>	 	&nbsp; &nbsp;
 <font size="4"> <label>Wing Name :</label> </font>		<label>${wingName}</label> 	
   
   
   
    <table  class="table table-bordered" id="flatStatus" >
    <tr>
    <td><label>Floor Number</label></td>
    <td><label>Flat Number</label></td>
    </tr>
    
         <tr>
           <s:forEach items="${floorDetails}" var="floorDetails" varStatus="loopStatus">
           	  <tr>
             	 <td style="width:150px"><b>${floorDetails.floortypeName}</b></td>
             	 <td>
             	    <c:forEach items="${flatList}" var="flatList" varStatus="loopStatus">
             	    <s:choose>
             	       <c:when test="${flatList.floorId eq floorDetails.floorId}">
	                  	<c:choose>
					      <c:when test="${flatList.flatstatus eq 'Booking Completed'}">
					      &nbsp
					       <a href="#" data-toggle="tooltip" title="${flatList.bookingName}">
					       <font size="4"><label class="label label-danger">${flatList.flatNumber}</label></font></a>
					      </c:when>
					      
					      <c:when test="${flatList.flatstatus eq 'Aggreement Completed'}">
					      &nbsp
					       <a href="#" data-toggle="tooltip" title="${flatList.bookingName}">
					       <font size="4"><label class="label label-info">${flatList.flatNumber}</label></font></a>
					      </c:when>
					      
						  <c:otherwise> 
					      &nbsp
					       <a href="#" data-toggle="tooltip" title="${flatList.bookingName}">
					       <font size="4"><label class="label label-success">${flatList.flatNumber}</label></font></a>
						  </c:otherwise>
				       	 </c:choose>
				       </c:when>
				    </s:choose>				 
	           		</c:forEach>
             	 </td>
              </tr>
            </s:forEach>
          </tr>
     </table>
    
  </div>
 </div>
 </div>
</section>
</form>
</div>
<%@ include file="footer.jsp" %>
<div class="control-sidebar-bg"></div>
</div>
   
<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script>

function ClearAll()
{
	$('#projectIdSpan').html('');
	$('#buildingIdSpan').html('');
	$('#wingIdSpan').html('');
}

function validate()
{

	ClearAll();
	 var projectId = $('#projectId').val();
	 var buildingId = $('#buildingId').val();
	 var wingId = $('#wingId').val();

		if(document.flatmasterform.projectId.value=="Default")
		{
			$('#projectIdSpan').html('Please, select project name..!');
			document.flatmasterform.projectId.focus();
			return false;
		}

		if(document.flatmasterform.buildingId.value=="Default")
		{
			$('#buildingIdSpan').html('Please, select Building name..!');
			document.flatmasterform.buildingId.focus();
			return false;
		}

		if(document.flatmasterform.wingId.value=="Default")
		{
			$('#wingIdSpan').html('Please, select Wing name..!');
			document.flatmasterform.wingId.focus();
			return false;
		}
	
	
}
/* 
function SearchAllFlat()
{
	ClearAll();
	 var projectId = $('#projectId').val();
	 var buildingId = $('#buildingId').val();
	 var wingId = $('#wingId').val();

		if(document.flatmasterform.projectId.value=="Default")
		{
			$('#projectIdSpan').html('Please, select project name..!');
			document.flatmasterform.projectId.focus();
			return false;
		}

		if(document.flatmasterform.buildingId.value=="Default")
		{
			$('#buildingIdSpan').html('Please, select Building name..!');
			document.flatmasterform.buildingId.focus();
			return false;
		}

		if(document.flatmasterform.wingId.value=="Default")
		{
			$('#wingIdSpan').html('Please, select Wing name..!');
			document.flatmasterform.wingId.focus();
			return false;
		}
	
		 document.getElementById('setProjectName').innerHTML='';
	// document.getElementById('setProjectName').innerHTML ="Project Name: "+projectId+" - "+buildingId+" - "+wingId;
	 $("#flatStatus tr").detach();

	 $.ajax({
		//alert("hjhjh");

		url : '${pageContext.request.contextPath}/getWingWiseflatStatus',
		type : 'Post',
		data : { projectId : projectId, buildingId : buildingId, wingId : wingId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
							for(var i=0;i<result.length;i++)
							{ 
								$('#flatStatus').append('<tr style="width:500px">');		
								if(result[i].flatstatus=="Booking Completed")
								{
								$('#flatStatus').append(' &nbsp <td > <font size="4"><label class="label label-danger">'+result[i].flatNumber+'</label></font></td>');
								}
								else if(result[i].flatstatus=="Aggreement Completed")
								{
									$('#flatStatus').append(' &nbsp <td > <font size="4"><label class="label label-info">'+result[i].flatNumber+'</label></font></td>');
									}
									
								else
								{
								$('#flatStatus').append(' &nbsp <td > <font size="4"><label class="label label-success">'+result[i].flatNumber+'</label></font></td>');
								}								
									 
								$('#flatStatus').append("<br> </tr>") 		
							 } 
							// $('#flatStatus').append("</tr>")
							  
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}
 */
function getBuldingList()
{
	 $("#buildingId").empty();
	 var projectId = $('#projectId').val();

	 $.ajax({

		 url : '${pageContext.request.contextPath}/getBuildingList',
		type : 'Post',
		data : { projectId : projectId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Building Name-");
							$("#buildingId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].buildingId).text(result[i].buildingName);
							    $("#buildingId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	 

	 
	 /*
		$("#flatStatus tr").detach();
		 var projectId = $('#projectId').val();
		 var wingId = [];
		// var flag=0;
		 //$("#buildingId").empty();
		$.ajax({
			//alert("hjhjh");

			url : '${pageContext.request.contextPath}/getProjectWiseflatStatus',
			type : 'Post',
			data : { projectId : projectId},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{ 
								for(var i=0;i<result.length;i++)
									{
									var flag=0;
									if(wingId.length==0)
										{
										wingId.push(result[i].wingId);
										
										}
									else
										{
										for(var k=0;k<=wingId.length;k++)
											{
											if(wingId[k]==result[i].wingId)
												{
												flag=1;
												}
											}
										if(flag==0)
											{
											wingId.push(result[i].wingId);
											}
										}
									}
								$('#flatStatus').append('<tr style="width:500px">');
								
								for(var i=0;i<wingId.length;i++)
								{ 
									$('#flatStatus').append('<td style="width:250px" >'+wingId[i]+' </td>');
										for(var j=0;j<result.length;j++)
										{
											if(result[j].wingId==wingId[i])
												{
												if(result[j].flatstatus=="Booking Completed")
													{
													$('#flatStatus').append(' &nbsp <td > <font size="4"><label class="label label-danger">'+result[j].flatNumber+'</label></font></td>');
													}
												else
													{
													$('#flatStatus').append(' &nbsp <td > <font size="4"><label class="label label-success">'+result[j].flatNumber+'</label></font></td>');
													}
												}
										 }
										
								 } 
								// $('#flatStatus').append("</tr>")
								  $('#flatStatus').append("<br> </tr>") 
							} 
							else
							{
								alert("failure111");
								//$("#ajax_div").hide();
							}

						}
			});
		*/
	
}//end of get Building List



function getWingNameList()
{
	 $("#wingId").empty();
	 var buildingId = $('#buildingId').val();
	 var projectId = $('#projectId').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getprojectwingList',
		type : 'Post',
		data : { buildingId : buildingId, projectId : projectId },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Wing Name-");
							$("#wingId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].wingId).text(result[i].wingName);
							    $("#wingId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
	 /*
		$("#flatStatus tr").detach();
		 var buildingId = $('#buildingId').val();
		 var projectId = $('#projectId').val();
		 //$("#floorId").empty();
		$.ajax({
			//alert("hjhjh");

			url : '${pageContext.request.contextPath}/getBuildingWiseFlatList',
			type : 'Post',
			data : { buildingId : buildingId , projectId : projectId},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{ 
								$('#flatStatus').append('<tr style="background-color: #4682B4;"><th style="width:150px">Flat Id</th><th style="width:150px">Project Name</th> <th style="width:150px">Building Name</th> <th style="width:150px">Wing Name</th>  <th style="width:150px">Flat Number</th> <th style="width:100px">Flat Facing</th><th style="width:150px">Flat Type</th>	');
							
								for(var i=0;i<result.length;i++)
								{ 
									if(i%2==0)
									{
										$('#flatStatus').append('<tr style="background-color: #F0F8FF;"><td onclick="setFlatId()" style="color: blue;">'+result[i].flatId+'</td><td>'+result[i].projectId+'</td><td>'+result[i].buildingId+'</td><td>'+result[i].wingId+'</td><td>'+result[i].flatNumber+'</td><td>'+result[i].flatfacingName+'</td><td>'+result[i].flatType+'</td>');
									}
									else
									{
										$('#flatStatus').append('<tr style="background-color: #CCE5FF;"><td onclick="setFlatId()" style="color: blue;">'+result[i].flatId+'</td><td>'+result[i].projectId+'</td><td>'+result[i].buildingId+'</td><td>'+result[i].wingId+'</td><td>'+result[i].flatNumber+'</td><td>'+result[i].flatfacingName+'</td><td>'+result[i].flatType+'</td>');
									}
								
								 } 
							} 
							else
							{
								alert("failure111");
							}

						}
			});
		*/
}


function getFloorNameList()
{
	 $("#floorId").empty();
	 var wingId = $('#wingId').val();
	 var buildingId = $('#buildingId').val();
	 var projectId = $('#projectId').val();
	 
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getwingfloorNameList',
		type : 'Post',
		data : {wingId : wingId, buildingId : buildingId, projectId : projectId },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Floor Name-");
							$("#floorId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].floorId).text(result[i].floortypeName);
							    $("#floorId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	 
	 /* 
	 $("#flatStatus tr").detach();
	 var wingId = $('#wingId').val();
	 var buildingId = $('#buildingId').val();
	 var projectId = $('#projectId').val();
	 //$("#floorId").empty();
	$.ajax({
		//alert("hjhjh");

		url : '${pageContext.request.contextPath}/getWingWiseFlatList',
		type : 'Post',
		data : {wingId : wingId, buildingId : buildingId , projectId : projectId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
							$('#flatStatus').append('<tr style="background-color: #4682B4;"><th style="width:150px">Flat Id</th><th style="width:150px">Project Name</th> <th style="width:150px">Building Name</th> <th style="width:150px">Wing Name</th>  <th style="width:150px">Flat Number</th> <th style="width:100px">Flat Facing</th><th style="width:150px">Flat Type</th>	');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									$('#flatStatus').append('<tr style="background-color: #F0F8FF;"><td onclick="setFlatId()" style="color: blue;">'+result[i].flatId+'</td><td>'+result[i].projectId+'</td><td>'+result[i].buildingId+'</td><td>'+result[i].wingId+'</td><td>'+result[i].flatNumber+'</td><td>'+result[i].flatfacingName+'</td><td>'+result[i].flatType+'</td>');
								}
								else
								{
									$('#flatStatus').append('<tr style="background-color: #CCE5FF;"><td onclick="setFlatId()" style="color: blue;">'+result[i].flatId+'</td><td>'+result[i].projectId+'</td><td>'+result[i].buildingId+'</td><td>'+result[i].wingId+'</td><td>'+result[i].flatNumber+'</td><td>'+result[i].flatfacingName+'</td><td>'+result[i].flatType+'</td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		}); */
}

</script>

<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
</script>

</body>
</html>

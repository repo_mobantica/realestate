<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Flat Possession Check List</title>
  <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <style type="text/css">
   tr.odd {background-color: #CCE5FF}
   tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
 
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
	
<div class="wrapper">

   <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      Flat Possession Check List:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Flat Possession Check List</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="FlatPossessionCheckListform" action="${pageContext.request.contextPath}/FlatPossessionCheckList" method="post">
  <section class="content">
    	
	<div class="box box-default">
         <div class="panel box box-danger">
         <div class="box-body">
          <div class="row">
          
            <div class="col-md-6">
            
            	
            </div>
            
              <div class="col-xs-4">
                <a href="FlatPossessionCheckList"><button type="button" class="btn btn-default" style="width:90px">Reset</button></a>
			     </div>
			     
           </div>
          </div>  
            
          <div class="box-body">
          <div class="row">
            <div class="col-md-12">
            
            <div class="table-responsive">
              <table id="bookingListTable" class="table table-bordered">
                  <thead>
                  <tr bgcolor="#4682B4">
                  <th>Booking Id</th>
                  <th >Name</th>
				  <th>Project </th>
				  <th>Building </th>
				  <th>Wing </th>
				  <th>Flat Number</th>
				  <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                   <c:forEach items="${bookingList}" var="bookingList" varStatus="loopStatus">
                   <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                   <td>${bookingList.bookingId}</td>
	               <td>${bookingList.bookingfirstname} ${bookingList.bookinglastname}</td>
	               <td>${bookingList.projectId}</td>
	               <td>${bookingList.buildingId}</td>
	               <td>${bookingList.wingId}</td>
	               <td>${bookingList.flatType}-${bookingList.flatId}</td>
	               <td>
	                  <a href="${pageContext.request.contextPath}/AddFlatPossessionCheckList?bookingId=${bookingList.bookingId}" class="btn btn-success btn-sm" data-toggle="tooltip" title="Add"><i class="glyphicon glyphicon-edit"></i></a>
	               </td>
                      </tr>
				   </c:forEach>
                 </tbody>
                </table>
              </div>
              
            </div>
           </div>
         </div>  
          
    </div>
  </div>
  </section>
</form>
    
</div>         
            
 <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  
<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<script>

$(function () {
    $('#bookingListTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>


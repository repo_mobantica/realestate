<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Add Project Work</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
   
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
    </script>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>

<div class="wrapper">

   <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Project Work Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Add Project Work</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="projectworkform" action="${pageContext.request.contextPath}/AddProjectDevelopmentWork" method="Post" >
<section class="content">

<div class="box box-default">
   <div  class="panel box box-danger">     
   <div class="box-body">
       <div class="row">
        <div class="col-md-12">
        
          <div class="box-body">
           <div class="row">
                 
			     <div class="col-xs-3">
			          <table>
			             <tr>
			              <td> <h4><label>Project Name</label></h4></td>
			              <td style="width:20px"></td>
			              <td><h4><label class="text-red">${projectName}</label></h4></td>
			             </tr>
			         </table>
			     </div> 
			
           </div>
          </div>
              
		  <div class="box-body">
            <div class="row">
            
              <div class="col-xs-3">
			    <label for="commonAreasAndFacilitiesAmenities">Common areas And Facilities, Amenities</label> <label class="text-red">* </label>
                <input type="text" class="form-control" id="commonAreasAndFacilitiesAmenities" placeholder="Common areas And Facilities, Amenities" name="commonAreasAndFacilitiesAmenities"  style="text-transform: capitalize;">
                 <span id="commonAreasAndFacilitiesAmenitiesSpan" style="color:#FF0000"></span>
			   </div> 
			   
			  <div class="col-xs-3">
			    <label for="availableStatus">Proposed </label>
                    <select class="form-control" name="availableStatus" id="availableStatus">
                  	 <option selected="" value="Yes">Yes</option>
                  	 <option value="No">No</option>
				  </select>
                 <span id="availableStatusSpan" style="color:#FF0000"></span>
			  </div>
			  
			  <div class="col-xs-2">
			    <label for="completionPercent">Percent of Completion  </label>
                <input type="text" class="form-control" id="completionPercent" placeholder="Completion Percent" name="completionPercent"   style="text-transform: capitalize;">
                <span id="completionPercentSpan" style="color:#FF0000"></span> 
			  </div> 
			    
			    <div class="col-xs-3">
			     <label for="details">Details </label>
			      <input type="text" class="form-control" id="details" placeholder="Details" name="details"   style="text-transform: capitalize;">
			       <span id="detailsSpan" style="color:#FF0000"></span> 
			    </div>
			    
			    <div class="col-xs-1" id="add">
			      <br/>
			     <button type="button" class="btn btn-success" onclick="return AddProjectWork()"><i class="fa fa-plus"></i>Add</button>
			    </div>
		     
			    <div class="col-xs-1"  id="update" hidden="hidden">
			       <br/>
			      <button type="button" class="btn btn-success" onclick="return UpdateProjectWork()"><i class="fa fa-plus"></i>Update</button>
			    </div> 
			 
			</div>
          </div>
			  
             
		  <div class="box-body">
            <div class="row">
                 
        	<div class="col-xs-12">
              <table class="table table-bordered" id="projectWorkListTable">
                  <thead>
                  <tr bgcolor=#4682B4>
                  	  <th>Sr.No</th>
		              <th>Common areas And Facilities, Amenities</th>
		              <th>Proposed</th>
		              <th>Percent of Completion </th>
		              <th>Details</th>
		              <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                   <c:forEach items="${projectdevelopmentworkList}" var="projectdevelopmentworkList" varStatus="loopStatus">
                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
	                        <td>${loopStatus.index+1}</td>
	                        <td>${projectdevelopmentworkList.commonAreasAndFacilitiesAmenities}</td>
	                        <td>${projectdevelopmentworkList.availableStatus}</td>
	                        <td>${projectdevelopmentworkList.completionPercent}</td>
	                        <td>${projectdevelopmentworkList.details}</td>
	                       <td>
                            <a onclick="EditProjectWork(${projectdevelopmentworkList.projectWorkId},${loopStatus.index+1})" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>
                           |<a onclick="DeleteProjectWork(${projectdevelopmentworkList.projectWorkId})" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a>
                       
                        </td>
	                        
                      </tr>
					</c:forEach>
                 
                 </tbody>
        
              </table>
            </div>
            
            </div>
          </div>		
						  
			<div class="box-body">
              <div class="row">
              
			     <input type="hidden" class="form-control" id="projectWorkId" name="projectWorkId" readonly>
			     <input type="hidden" class="form-control" id="projectId" name="projectId" value="${projectId}" readonly>
			
			  <input type="hidden" id="projectdbStatus" name="projectdbStatus" value="${projectdbStatus}">	
			  <input type="hidden" id="creationDate" name="creationDate" value="">
			  <input type="hidden" id="updateDate" name="updateDate" value="">
			  <input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>">
              
              <div class="col-xs-1">
              </div>
              
                  <div class="4">
                  <div class="col-xs-2">
                    <a href="ProjectDevelopmentWorkMaster"><button type="button" class="btn btn-block btn-primary" style="width:90px">Back</button></a>
			      </div>
			      </div>
				  <div class="col-xs-4">
                   <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
			      </div>
					<div class="col-xs-2">
			         <button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
			     </div> 
			 
              </div>
			  </div>	
			     
		 </div>
       </div>
   </div>
   
   
   
</div>

</div> 
 </section>
</form>

</div>
<%@ include file="footer.jsp" %>
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>

<script>
function EditProjectWork(projectWorkId,rowNo)
{
	clearall();
	document.getElementById('add').style.display ='none';
	document.getElementById('update').style.display ='block';
	 
	var oTable = document.getElementById('projectWorkListTable');

	var oCells = oTable.rows.item(rowNo).cells;
	
	var commonAreasAndFacilitiesAmenities=oCells.item(1).innerHTML;
	var availableStatus=oCells.item(2).innerHTML;
	var completionPercent=oCells.item(3).innerHTML;
	var details=oCells.item(4).innerHTML;
	
 	 $('#commonAreasAndFacilitiesAmenities').val(commonAreasAndFacilitiesAmenities);
	// $('#availableStatus').val(availableStatus);
	 $('#completionPercent').val(completionPercent);
	 $('#details').val(details); 
	 $('#projectWorkId').val(projectWorkId);   
	 
	 $("#availableStatus").empty();
 	 
	 if(availableStatus=="Yes")
		 {
		var option = $('<option />');
		 option.attr('value',"Yes").text("Yes");
	    $("#availableStatus").append(option);
	    
	    option = $('<option />');
	    option.attr('value',"No").text("No");
	    $("#availableStatus").append(option);
		 }
	 else 
		 {
			var option = $('<option />');
			option.attr('value',"No").text("No");
		    $("#availableStatus").append(option);
		    
		    var option = $('<option />');
		    option.attr('value',"Yes").text("Yes");
		    $("#availableStatus").append(option);
		 
		 }
}


function UpdateProjectWork()
{

	clearall();
	if(document.projectworkform.commonAreasAndFacilitiesAmenities.value=="")
	{
		$('#commonAreasAndFacilitiesAmenitiesSpan').html('Common areas  should not be empty..!');
		document.projectworkform.commonAreasAndFacilitiesAmenities.focus();
		return false;
    }
	else if(document.projectworkform.commonAreasAndFacilitiesAmenities.value.match(/^[\s]+$/))
	{
		$('#commonAreasAndFacilitiesAmenitiesSpan').html('Common areas  name should not be empty..!');
		document.projectworkform.commonAreasAndFacilitiesAmenities.focus();
		return false;
	}

	/*  
	if(document.projectworkform.availableStatus.value=="")
	{
		$('#availableStatusSpan').html('Available should not be empty..!');
		document.projectworkform.availableStatus.focus();
		return false;
    }
	else if(document.projectworkform.availableStatus.value.match(/^[\s]+$/))
	{
		$('#availableStatusSpan').html('Available should not be empty..!');
		document.projectworkform.availableStatus.focus();
		return false;
	}
	 */
	 /* 
	if(document.projectworkform.completionPercent.value=="")
	{
		$('#completionPercentSpan').html('completion Percent should not be empty..!');
		document.projectworkform.completionPercent.focus();
		return false;
    }
	else if(document.projectworkform.completionPercent.value.match(/^[\s]+$/))
	{
		$('#completionPercentSpan').html('completion Percent should not be empty..!');
		document.projectworkform.completionPercent.focus();
		return false;
	}
	
	if(document.projectworkform.details.value=="")
	{
		$('#detailsSpan').html('Details should not be empty..!');
		document.projectworkform.details.focus();
		return false;
    }
	else if(document.projectworkform.details.value.match(/^[\s]+$/))
	{
		$('#detailsSpan').html('Details should not be empty..!');
		document.projectworkform.details.focus();
		return false;
	}
	 */
	 var projectWorkId = Number($('#projectWorkId').val());
	 var projectId = $('#projectId').val();
	 
	 var commonAreasAndFacilitiesAmenities = $('#commonAreasAndFacilitiesAmenities').val();
	 var availableStatus = $('#availableStatus').val();
	 var completionPercent = $('#completionPercent').val();
	 var details = $('#details').val();
	 
	 
	 
	$('#projectWorkListTable tr').detach();
		 
	 $.ajax({

		 url : '${pageContext.request.contextPath}/UpdateProjectWork',
		type : 'Post',
		data : { projectWorkId : projectWorkId, projectId : projectId, commonAreasAndFacilitiesAmenities : commonAreasAndFacilitiesAmenities, availableStatus : availableStatus, completionPercent : completionPercent, details : details},
		dataType : 'json',
		success : function(result)
				  {
					   if (result) 
					   { 
							$('#projectWorkListTable').append('<tr style="background-color: #4682B4;"><th>Sr.No</th> <th>Common areas And Facilities, Amenities</th><th>Available</th> <th>Completion Percent</th><th>Details</th><th>Action</th></tr>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].projectWorkId;
									$('#projectWorkListTable').append('<tr style="background-color: #F0F8FF;"><td>'+(i+1)+'</td><td>'+result[i].commonAreasAndFacilitiesAmenities+'</td><td>'+result[i].availableStatus+'</td><td>'+result[i].completionPercent+'</td><td>'+result[i].details+'</td><td><a onclick="EditProjectWork('+id+','+(i+1)+')" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>|<a onclick="DeleteProjectWork('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
								else
								{
									var id = result[i].projectWorkId;
									$('#projectWorkListTable').append('<tr style="background-color: #CCE5FF;"><td>'+(i+1)+'</td><td>'+result[i].commonAreasAndFacilitiesAmenities+'</td><td>'+result[i].availableStatus+'</td><td>'+result[i].completionPercent+'</td><td>'+result[i].details+'</td><td><a onclick="EditProjectWork('+id+','+(i+1)+')" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>|<a onclick="DeleteProjectWork('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}
				  } 

		});
	 
	 $('#commonAreasAndFacilitiesAmenities').val("");
	 $('#availableStatus').val("");
	 $('#completionPercent').val("");
	 $('#details').val("");
	 $('#projectWorkId').val("");
	 
	 document.getElementById('add').style.display ='block';
	 document.getElementById('update').style.display ='none';
}


function DeleteProjectWork(projectWorkId)
{

	var projectId = $('#projectId').val();
	 
	$('#projectWorkListTable tr').detach();
		
	 $.ajax({

		 url : '${pageContext.request.contextPath}/DeleteProjectWork',
		type : 'Post',
		data : { projectWorkId : projectWorkId, projectId : projectId},
		dataType : 'json',
		success : function(result)
				  {
					   if (result) 
					   { 
							$('#projectWorkListTable').append('<tr style="background-color: #4682B4;"><th>Sr.No</th> <th>Common areas And Facilities, Amenities</th><th>Available</th> <th>Completion Percent</th><th>Details</th><th>Action</th></tr>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].projectWorkId;
									$('#projectWorkListTable').append('<tr style="background-color: #F0F8FF;"><td>'+(i+1)+'</td><td>'+result[i].commonAreasAndFacilitiesAmenities+'</td><td>'+result[i].availableStatus+'</td><td>'+result[i].completionPercent+'</td><td>'+result[i].details+'</td><td><a onclick="EditProjectWork('+id+','+(i+1)+')" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>|<a onclick="DeleteProjectWork('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
								else
								{
									var id = result[i].projectWorkId;
									$('#projectWorkListTable').append('<tr style="background-color: #CCE5FF;"><td>'+(i+1)+'</td><td>'+result[i].commonAreasAndFacilitiesAmenities+'</td><td>'+result[i].availableStatus+'</td><td>'+result[i].completionPercent+'</td><td>'+result[i].details+'</td><td><a onclick="EditProjectWork('+id+','+(i+1)+')" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>|<a onclick="DeleteProjectWork('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}
				  } 

		});
	
}

function AddProjectWork()
{
	
	clearall();
	if(document.projectworkform.commonAreasAndFacilitiesAmenities.value=="")
	{
		$('#commonAreasAndFacilitiesAmenitiesSpan').html('Common areas  should not be empty..!');
		document.projectworkform.commonAreasAndFacilitiesAmenities.focus();
		return false;
    }
	else if(document.projectworkform.commonAreasAndFacilitiesAmenities.value.match(/^[\s]+$/))
	{
		$('#commonAreasAndFacilitiesAmenitiesSpan').html('Common areas  name should not be empty..!');
		document.projectworkform.commonAreasAndFacilitiesAmenities.focus();
		return false;
	}
/* 
	 
	if(document.projectworkform.availableStatus.value=="")
	{
		$('#availableStatusSpan').html('Available should not be empty..!');
		document.projectworkform.availableStatus.focus();
		return false;
    }
	else if(document.projectworkform.availableStatus.value.match(/^[\s]+$/))
	{
		$('#availableStatusSpan').html('Available should not be empty..!');
		document.projectworkform.availableStatus.focus();
		return false;
	}
	 */
	 
	if(document.projectworkform.completionPercent.value=="")
	{
		$('#completionPercentSpan').html('completion Percent should not be empty..!');
		document.projectworkform.completionPercent.focus();
		return false;
    }
	else if(document.projectworkform.completionPercent.value.match(/^[\s]+$/))
	{
		$('#completionPercentSpan').html('completion Percent should not be empty..!');
		document.projectworkform.completionPercent.focus();
		return false;
	}
	
	if(document.projectworkform.details.value=="")
	{
		$('#detailsSpan').html('Details should not be empty..!');
		document.projectworkform.details.focus();
		return false;
    }
	else if(document.projectworkform.details.value.match(/^[\s]+$/))
	{
		$('#detailsSpan').html('Details should not be empty..!');
		document.projectworkform.details.focus();
		return false;
	}
	
	 //var projectWorkId = Number$('#projectWorkId').val());
	 var projectId = $('#projectId').val();
	 
	 var commonAreasAndFacilitiesAmenities = $('#commonAreasAndFacilitiesAmenities').val();
	 var availableStatus = $('#availableStatus').val();
	 var completionPercent = $('#completionPercent').val();
	 var details = $('#details').val();
	 
	 
	 
	$('#projectWorkListTable tr').detach();
		 
	 $.ajax({

		 url : '${pageContext.request.contextPath}/AddProjectWork',
		type : 'Post',
		data : { projectId : projectId, commonAreasAndFacilitiesAmenities : commonAreasAndFacilitiesAmenities, availableStatus : availableStatus, completionPercent : completionPercent, details : details},
		dataType : 'json',
		success : function(result)
				  {
					   if (result) 
					   { 
							$('#projectWorkListTable').append('<tr style="background-color: #4682B4;"><th>Sr.No</th> <th>Common areas And Facilities, Amenities</th><th>Available</th> <th>Completion Percent</th><th>Details</th><th>Action</th></tr>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].projectWorkId;
									$('#projectWorkListTable').append('<tr style="background-color: #F0F8FF;"><td>'+(i+1)+'</td><td>'+result[i].commonAreasAndFacilitiesAmenities+'</td><td>'+result[i].availableStatus+'</td><td>'+result[i].completionPercent+'</td><td>'+result[i].details+'</td><td><a onclick="EditProjectWork('+id+','+(i+1)+')" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>|<a onclick="DeleteProjectWork('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
								else
								{
									var id = result[i].projectWorkId;
									$('#projectWorkListTable').append('<tr style="background-color: #CCE5FF;"><td>'+(i+1)+'</td><td>'+result[i].commonAreasAndFacilitiesAmenities+'</td><td>'+result[i].availableStatus+'</td><td>'+result[i].completionPercent+'</td><td>'+result[i].details+'</td><td><a onclick="EditProjectWork('+id+','+(i+1)+')" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>|<a onclick="DeleteProjectWork('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
							
							 }  
						} 
						else
						{
							alert("failure111");
						}
				  } 

		});
	 
	 $('#commonAreasAndFacilitiesAmenities').val("");
	// $('#availableStatus').val("");
	 $('#completionPercent').val("");
	 $('#details').val("");
}

function clearall()
{
	$('#commonAreasAndFacilitiesAmenitiesSpan').html('');
	$('#availableStatusSpan').html('');
	$('#completionPercentSpan').html('');
	$('#detailsSpan').html('');
	
}
function validate()
{		
	
}
function init()
{
	clearall();
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	/* 
	 
	 if(document.projectform.projectdbStatus.value == "Fail")
	 {
	  	alert("Sorry, record is present already..!");
	 }
	 else if(document.projectform.projectdbStatus.value == "Success")
	 {
		 $('#statusSpan').html('Record added successfully..!');
	 }
     document.projectform.projectName.focus(); */
}



$(function () {
  $('#projectWorkListTable').DataTable()
  $('#example2').DataTable({
    'paging'      : true,
    'lengthChange': false,
    'searching'   : false,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : false
  })
})
</script>
</body>
</html>

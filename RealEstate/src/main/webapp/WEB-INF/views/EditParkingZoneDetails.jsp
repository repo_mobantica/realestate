<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Edit Parking Zone Details</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
     <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

  <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Parking Zone Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Edit Parking Zone Details</li>
      </ol>
    </section>

    <!-- Main content -->
    
<form name="parkingzonedetailsform" action="${pageContext.request.contextPath}/EditParkingZoneDetails" method="Post" onSubmit="return validate()">
    
    <section class="content">
      <!-- SELECT2 EXAMPLE -->
      
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
                  <span id="statusSpan" style="color:#FF0000"></span>
              <!-- /.form-group -->
             <div class="box-body">
          <div class="row">
            <div class="col-md-2">
                  <label >Parking Zone Id</label>
                  <input type="text" class="form-control"  id="parkingZoneId" name="parkingZoneId"  value="${parkingZoneDetails.parkingZoneId}" readonly>
                </div>
			</div>
				</div>
								
		 <div class="box-body">
            <div class="row">

                <div class="col-md-3">
	                  <label>Project Name</label><label class="text-red">* </label>
	                  <select class="form-control" name="projectId" id="projectId" onchange="getBuldingList(this.value)">
	                  	 <option selected="selected" value="${parkingZoneDetails.projectId}">${projectName}</option>
	                 	 <c:forEach var="projectList" items="${projectList}">
                   	  <c:choose>
                   	   <c:when test="${parkingZoneDetails.projectId ne projectList.projectId}">
                    	<option value="${projectList.projectId}">${projectList.projectName}</option>
                       </c:when>
                      </c:choose>
				     </c:forEach>
					  </select>
					  <span id="projectIdSpan" style="color:#FF0000"></span>
                  </div>
                  
	              <div class="col-md-3">
	                  <label>Building Name</label><label class="text-red">* </label>
	                  <select class="form-control" name="buildingId" id="buildingId" onchange="getWingList(this.value)">
					  	<option selected="selected" value="${parkingZoneDetails.buildingId}">${buildingName}</option>
	                  </select>
	                      <span id="buildingIdSpan" style="color:#FF0000"></span>
	              </div>
	              
	              <div class="col-md-3">
                  <label>Wing Name</label><label class="text-red">* </label>
                  
                  <select class="form-control" name="wingId" id="wingId" onchange="getParkingFloorList(this.value)">
				  	<option selected="selected" value="${parkingZoneDetails.wingId}">${wingName}</option>
                  </select>
                      <span id="wingIdSpan" style="color:#FF0000"></span>
                  </div>
                    <div class="col-md-3">
                  <label>Parking Floor Name </label><label class="text-red">* </label>
                  <select class="form-control" name="floorId" id="floorId">
				 	 	<option selected="selected" value="${parkingZoneDetails.floorId}">${floortypeName}</option>
                  </select>
                      <span id="floorIdSpan" style="color:#FF0000"></span>
                </div>
	              
				</div>
                </div>
                
                <div class="box-body">
                   <div class="row">
                   	
	                   <div class="col-md-3">
	                      <label for="parkingType">Parking Type</label>
				     	  </br>
				     	  <c:choose>
				     	    <c:when test="${parkingZoneDetails.parkingType eq 'Open Parking'}">
		                  		<input type="radio" name="parkingType" id="parkingType" value = "Open Parking" onclick = "GetOpenParking()" checked="checked">&nbsp &nbsp  Open Parking
		                    </c:when>
		                    <c:otherwise>
		                    	<input type="radio" name="parkingType" id="parkingType" value = "Open Parking" onclick = "GetOpenParking()">&nbsp &nbsp Open Parking
		                    </c:otherwise>
		                  </c:choose>
						  &nbsp &nbsp &nbsp &nbsp 
						  <c:choose>
						     <c:when test="${parkingZoneDetails.parkingType eq 'Close Parking'}">
		  				  		<input type="radio" name="parkingType" id="parkingType" value ="Close Parking" onclick = "GetCloseParking()" checked="checked">&nbsp &nbsp Close Parking
		  				  	 </c:when>
		  				  	 <c:otherwise>
		  				  	    <input type="radio" name="parkingType" id="parkingType" value ="Close Parking" onclick = "GetCloseParking()">&nbsp &nbsp Close Parking
		  				  	 </c:otherwise>
		  				  </c:choose>
		                  <span id="parkingTypeSpan" style="color:#FF0000"></span>
		               </div>
	               
	                   <div class="col-md-3">
						    <label for="parkingNumber">Select Parking</label><label class="text-red">*</label>
						    <select class="form-control" name="parkingNumber" id="parkingNumber" onchange="CheckParkingNumberDetailsStatus(this.value)">
						    		<option value="Default">-Select Parking Number-</option>
						    		<option selected="selected" value="${parkingZoneDetails.parkingNumber}">${parkingZoneDetails.parkingNumber}</option>
                  		    </select>
                  		    <span id="parkingNumberSpan" style="color:#FF0000"></span>
           			   </div>
                   
                   
                   </div>
                </div>
            
                
            </div>
            <!-- /.col -->
        <div class="col-md-12">
		 <div class="box-body">
		   <h4>Parking size(Sq.Mtr.):</h4>
           <div class="row">
           
           		<div class="col-md-2">
                   <label for="parkingLength">Length(Sq.Mtr)</label><label class="text-red">* </label>
                   <input type="text" class="form-control" name="parkingLength" id="parkingLength" value="${parkingZoneDetails.parkingLength}" placeholder="Enter parking length in Sq.Mtr." onchange="CalculateAreaInSqMtrAndSqFt()">
                   <span id="parkingLengthSpan" style="color:#FF0000"></span>
                </div>
           		
	            <div class="col-md-2">
	               <label for="parkingWidth">Width(Sq.Mtr)</label><label class="text-red">*</label>
	               <input type="text" class="form-control" name="parkingWidth" id="parkingWidth" value="${parkingZoneDetails.parkingWidth}" placeholder="Enter parking width in Sq.Mtr." onchange="CalculateAreaInSqMtrAndSqFt()">
	               <span id="parkingWidthSpan" style="color:#FF0000"></span>
	            </div>
               
                <div class="col-md-2">
	               <label for="areaInSqMtr">Area In(Sq.Mtr)</label><label class="text-red">*</label>
	               <input type="text" class="form-control" name="areaInSqMtr" id="areaInSqMtr" value="${parkingZoneDetails.areaInSqMtr}" placeholder="" readonly="readonly">
	               <span id="areaInSqMtrSpan" style="color:#FF0000"></span>
	            </div>
               
                <div class="col-md-2">
                   <label for="areaInSqFt">Area In(Sq.Ft.)</label><label class="text-red">* </label>
                   <input type="text" class="form-control" name="areaInSqFt" id="areaInSqFt" value="${parkingZoneDetails.areaInSqFt}" placeholder="" readonly="readonly">
                   <span id="areaInSqFtSpan" style="color:#FF0000"></span>
                </div>
                
		   </div>
		 </div>	
				
       </div>
              <input type="hidden" id="parkingZoneStatus" name="parkingZoneStatus" value="${parkingZoneStatus}">	
			  <input type="hidden" id="creationDate" name="creationDate" value="${parkingZoneDetails.creationDate}">
			  <input type="hidden" id="updateDate" name="updateDate" value="">
			  <input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>" >
			  
            <!-- /.col -->
			
          </div>
          
		  	<div class="box-body">
              <div class="row"><br/><br/>
                 <div class="col-xs-5">
                 <div class="col-xs-2">
                	<a href="ParkingZoneMaster"><button type="button" class="btn btn-block btn-primary" style="width:90px">Back</button></a>
			     </div>
			     </div>
				 
				 <div class="col-xs-4">
                	<button type="reset" class="btn btn-default" value="reset" style="width:90px">Reset</button>
			     </div>
				 
				 <div class="col-xs-2">
			  		<button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
			     </div>
			      
              </div>
			</div>
			
          <!-- /.row -->
        </div>
      </div>
    </section>
    </form>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
    </script>
<script>

function addparking()
{
	document.getElementById("numberofFlats").disabled = true;	
	document.getElementById("numberofShops").disabled = true; 
	document.parkingzonedetailsform.numberofFlats.value="";
	document.parkingzonedetailsform.numberofShops.value="";
}

function addshop()
{
	document.getElementById("numberofFlats").disabled = true;	
	document.getElementById("numberofShops").disabled = false; 
	document.parkingzonedetailsform.numberofFlats.value="";
	//document.parkingzonedetailsform.numberofShops.value="";
}

function addflat()
{
	document.getElementById("numberofFlats").disabled = false;	
	document.getElementById("numberofShops").disabled = true; 
	//document.parkingzonedetailsform.numberofFlats.value="";
	document.parkingzonedetailsform.numberofShops.value="";
}

function clearall()
{
	$('#projectIdSpan').html('');
	$('#buildingIdSpan').html('');
	$('#wingIdSpan').html('');
	$('#floorIdSpan').html('');
	$('#parkingTypeSpan').html('');
	$('#parkingNumberSpan').html('');
	$('#parkingLengthSpan').html('');
	$('#parkingWidthSpan').html('');
	$('#areaInSqMtrSpan').html('');
	$('#areaInSqftSpan').html('');
}

function validate()
{ 
	    clearall();
		
	    //validation for project name
		if(document.parkingzonedetailsform.projectId.value=="Default")
		{
			$('#projectIdSpan').html('Please, Select project name..!');
			document.parkingzonedetailsform.projectId.focus();
			return false;
		}
		
		//validation for project building
		if(document.parkingzonedetailsform.buildingId.value=="Default")
		{
			$('#buildingIdSpan').html('Please, select building name..!');
			document.parkingzonedetailsform.buildingId.focus();
			return false;
		}
		//validation for project building
		if(document.parkingzonedetailsform.wingId.value=="Default")
		{
			$('#wingIdSpan').html('Please, select wing name..!');
			document.parkingzonedetailsform.wingId.focus();
			return false;
		}
		//validation for floor type name
		if(document.parkingzonedetailsform.floorId.value=="Default")
		{
			$('#floorIdSpan').html('Please, select floor type..!');
			document.parkingzonedetailsform.floorId.focus();
			return false;
		}
		//validation for parking type
		if((document.parkingzonedetailsform.parkingType[0].checked == false) && (document.parkingzonedetailsform.parkingType[1].checked == false))
		{
			$('#parkingTypeSpan').html('Please, select parking type..!');
			document.parkingzonedetailsform.parkingType[0].focus();
			return false;
		}
		
	    //validation for parking number
		if(document.parkingzonedetailsform.parkingNumber.value=="Default")
		{
			$('#parkingNumberSpan').html('Please, select parking number..!');
			document.parkingzonedetailsform.parkingNumber.focus();
			return false;
		}
		
		//validation for parking length
		if(document.parkingzonedetailsform.parkingLength.value=="")
		{
			$('#parkingLengthSpan').html('Please, enter parking length..!');
			document.parkingzonedetailsform.parkingLength.focus();
			return false;
		}
		else if(!document.parkingzonedetailsform.parkingLength.value.match(/^[0-9]+$/))
		{
			if(!document.parkingzonedetailsform.parkingLength.value.match(/^[0-9]+[.]{0,1}[0-9]+$/))
			{
				$('#parkingLengthSpan').html('Please, enter valid parking length..!');
				document.parkingzonedetailsform.parkingLength.focus();
				return false;
			}
		}
		
		//validation for parking width
		if(document.parkingzonedetailsform.parkingWidth.value=="")
		{
			$('#parkingWidthSpan').html('Please, enter parking width..!');
			document.parkingzonedetailsform.parkingWidth.focus();
			return false;
		}
		else if(!document.parkingzonedetailsform.parkingWidth.value.match(/^[0-9]+$/))
		{
			if(!document.parkingzonedetailsform.parkingWidth.value.match(/^[0-9]+[.]{0,1}[0-9]+$/))
			{
				$('#parkingWidthSpan').html('Please, enter valid parking width..!');
				document.parkingzonedetailsform.parkingWidth.focus();
				return false;
			}
		}
		
		//validation for parking area in sq mtr
		if(document.parkingzonedetailsform.areaInSqMtr.value=="")
		{
			$('#areaInSqMtrSpan').html('Area in Sq.Mtr. not calculated..!');
			document.parkingzonedetailsform.areaInSqMtr.focus();
			return false;
		}
		
		if(document.parkingzonedetailsform.areaInSqFt.value=="")
		{
			$('#areaInSqFtSpan').html('Area in Sq.Ft. not calculated..!');
			document.parkingzonedetailsform.areaInSqFt.focus();
			return false;
		}
}

function init()
{
	 clearall();
	 
		var date =  new Date();
		var year = date.getFullYear();
		var month = date.getMonth() + 1;
		var day = date.getDate();
		
		/* document.getElementById("creationDate").value = day + "/" + month + "/" + year;
		 */document.getElementById("updateDate").value = day + "/" + month + "/" + year;
		
	 
	 if(document.parkingzonedetailsform.parkingZoneStatus.value == "Fail")
	 {
	  	//alert("Sorry, record is present already..!");
	 }
	 else if(document.parkingzonedetailsform.parkingZoneStatus.value == "Success")
	 {
		 $('#statusSpan').html('Record added successfully..!');
	 }
    
	 document.parkingzonedetailsform.projectId.focus();
}


function getBuldingList()
{
	 $("#buildingId").empty();
	 var projectId = $('#projectId').val();

	 $.ajax({

		url : '${pageContext.request.contextPath}/getBuildingList',
		type : 'Post',
		data : { projectId : projectId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Building Name-");
							$("#buildingId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].buildingId).text(result[i].buildingName);
							    $("#buildingId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get Building List


function getWingList()
{
	 $("#wingId").empty();
	 var buildingId = $('#buildingId').val();
	 var projectId = $('#projectId').val();
	 $.ajax({

		url : '${pageContext.request.contextPath}/getWingList',
		type : 'Post',
		data : { buildingId : buildingId, projectId : projectId },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Wing Name-");
							$("#wingId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].wingId).text(result[i].wingName);
							    $("#wingId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get Floor Type List

function getParkingFloorList()
{
	$("#floorId").empty();
	var wingId = $('#wingId').val();
	var buildingId = $('#buildingId').val();
	var projectId = $('#projectId').val();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getParkingFloorList',
		type : 'Post',
	 	data : { projectId : projectId, buildingId : buildingId ,wingId : wingId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
						  	var option = $('<option/>');
						 	option.attr('value',"Default").text("-Select Parking Floor-");
						  	$("#floorId").append(option);
						  
						  for(var i=0; i<result.length;i++)
						  {
							 var option = $('<option />');
							 option.attr('value',result[i].floorId).text(result[i].floortypeName);
							 $("#floorId").append(option); 					  		
						  } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
}

function GetOpenParking()
{
	$("#parkingNumber").empty();
	var projectId = $('#projectId').val();
	var buildingId = $('#buildingId').val();
	var wingId = $('#wingId').val();
	var floorId = $('#floorId').val();
	
	var numberOfOpenParking = 0;
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getOpenParkingsList',
		type : 'Post',
	 	data : { projectId : projectId, buildingId : buildingId , wingId : wingId, floorId : floorId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							numberOfOpenParking = result[0].numberofOpenParking;
							
						  	var option = $('<option/>');
						 	option.attr('value',"Default").text("-Select Parking Number-");
						  	$("#parkingNumber").append(option);
						  
						  for(var i=1; i<=numberOfOpenParking;i++)
						  {
							 var option = $('<option />');
							 option.attr('value',"OP"+i).text("OP"+i);
							 $("#parkingNumber").append(option); 					  		
						  } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
}

function GetCloseParking()
{
	$("#parkingNumber").empty();
	var projectId = $('#projectId').val();
	var buildingId = $('#buildingId').val();
	var wingId = $('#wingId').val();
	var floorId = $('#floorId').val();
	
	var numberofCloseParking = 0;
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getCloseParkingsList',
		type : 'Post',
	 	data : { projectId : projectId, buildingId : buildingId , wingId : wingId, floorId : floorId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							numberofCloseParking = result[0].numberofCloseParking;
							
						  	var option = $('<option/>');
						 	option.attr('value',"Default").text("-Select Parking Number-");
						  	$("#parkingNumber").append(option);
						  
						  for(var i=1; i<=numberofCloseParking;i++)
						  {
							 var option = $('<option />');
							 option.attr('value',"CP"+i).text("CP"+i);
							 $("#parkingNumber").append(option); 					  		
						  } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
}

function CheckParkingNumberDetailsStatus()
{
	var parkingType   = "";
	var projectId   = $('#projectId').val();
	var buildingId  = $('#buildingId').val();
	var wingId      = $('#wingId').val();
	var floorId = $('#floorId').val();
	var parkingNumber = $('#parkingNumber').val();
	
	if(document.parkingzonedetailsform.parkingType[0].checked == true)
	{
		parkingType   = "Open Parking";
	}
	else
	{
		parkingType   = "Close Parking";
	}
	
	var flag=0; 		  
	
	$.ajax({

		url : '${pageContext.request.contextPath}/checkParkingNumberDetailStatus',
		type : 'Post',
		data : {projectId : projectId, buildingId : buildingId , wingId : wingId, floorId : floorId, parkingNumber :parkingNumber},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							for(var i=0;i<result.length;i++)
							{
								
								if(result[i].projectId==projectId)
								{
									
									if(result[i].buildingId==buildingId)
									{
											
										    if(result[i].wingId==wingId)
											{
												
												if(result[i].floorId == floorId)
												{
													if(result[i].parkingNumber == parkingNumber)
													{
														clearall();
														flag = 1;
														document.parkingzonedetailsform.parkingNumber.focus();
														$('#parkingNumberSpan').html('This parking number already exist..!');
													}
												}
											}
									    }
								    }
							    } 
						  } 
						  else
						  {
							alert("failure111");
						  }
						
						if(flag==1)
						{
							if(parkingType == "Open Parking")
							{
								GetOpenParking();
							}
							else
							{
								GetCloseParking();	
							}
						}
						
					}
		    });
	
}

function CalculateAreaInSqMtrAndSqFt()
{
	var parkingLength = parseFloat($('#parkingLength').val());
	var parkingWidth  = parseFloat($('#parkingWidth').val());
	
	var areaInSqMtr = 0;
	
	if(parkingLength!=0 && parkingWidth!=0)
	{
	   areaInSqMtr   =  parkingLength * parkingWidth;
	}
	
	if(parkingLength == "NaN" || parkingWidth == "NaN" || parkingLength == "" || parkingWidth == "")
	{
	   areaInSqMtr   =  0;
	}
			
	$('#areaInSqMtr').val(areaInSqMtr.toFixed(2));
	
	var areaInSqFt = areaInSqMtr * 10.7639;
	
	$('#areaInSqFt').val(areaInSqFt.toFixed(2));
}

$(function () 
{
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
  
</script>
</body>
</html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Real Estate | Flat Market Price</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<!-- Theme style -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

<!-- Google Font -->
<style type="text/css">
tr.odd {
	background-color: #CCE5FF
}

tr.even {
	background-color: #F0F8FF
}
</style>
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control", "no-cache,no-store,must-revalidate");//HTTP 1.1
		response.setHeader("Pragma", "no-cache"); //HTTP 1.0
		response.setDateHeader("Expires", 0);

		if (session != null) {
			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null
					|| session.getAttribute("profile_img") == null) {
				response.sendRedirect("login");
			}
		}
	%>
	<div class="wrapper">


		<!-- Left side column. contains the logo and sidebar -->
		<%@ include file="headerpage.jsp"%>
		<!-- Left side column. contains the logo and sidebar -->
		<%@ include file="menu.jsp"%>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Flat Market Price Details: <small>Preview</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="home"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><a href="#">Report</a></li>
					<li class="active">Flat Market Price</li>
				</ol>
			</section>

			<!-- Main content -->
			<form name="flatmarketpriceform"
				action="${pageContext.request.contextPath}/FlatWiseMarketPrice"
				onSubmit="return validate()" method="Post">
				<section class="content">

					<!-- SELECT2 EXAMPLE -->
					<div class="box box-default">
						<div class="box-body">
							<div class="row">
								<div class="col-md-12">


									<div class="box-body">
										<div class="row">

											<div class="col-md-2">
												<label>Project</label><label class="text-red">* </label> <select
													class="form-control" name="projectId" id="projectId"
													onchange="getBuldingList(this.value)">
													<option selected="selected" value="Default">-Select
														Project-</option>
													<c:forEach var="projectList" items="${projectList}">
														<option value="${projectList.projectId}">${projectList.projectName}</option>
													</c:forEach>
												</select> <span id="projectIdSpan" style="color: #FF0000"></span>
											</div>

											<div class="col-md-2">
												<label>Project Building</label><label class="text-red">*
												</label> <select class="form-control" name="buildingId"
													id="buildingId" onchange="getAllWingList(this.value)">
													<option selected="selected" value="Default">-Select
														Project Building-</option>

												</select> <span id="buildingIdSpan" style="color: #FF0000"></span>
											</div>

											<div class="col-xs-2">
												<label>Wing </label> <label class="text-red">* </label> <select
													class="form-control" name="wingId" id="wingId">
													<option selected="selected" value="Default">-Select
														Wing Name-</option>
												</select> <span id="wingIdSpan" style="color: #FF0000"></span>
											</div>
											<div class="col-xs-2">
												<label for="agentId">Flat Number </label> <input type="text"
													class="form-control" id="flatNumber" name="flatNumber">
												<span id="flatNumberSpan" style="color: #FF0000"></span>
											</div>
											<div class="col-xs-4">
												<label>.</label> <span id="SearchFlatMarkertPriceSpan"
													style="color: #FF0000"></span><br />
												<button type="button" class="btn btn-success"
													onclick="return SearchFlatMarkertPrice()">Search</button>

												&nbsp &nbsp &nbsp
												<button type="reset" class="btn btn-default" value="reset"
													>Reset</button>
												&nbsp &nbsp &nbsp
												<button type="submit" class="btn btn-info">Booking</button>
												<span id="SearchFlatMarkertPriceSpan" style="color: #FF0000"></span>

												&nbsp &nbsp &nbsp
												<button type="button" class="btn btn-success" onclick="return PrintFlatWiseMarketPrice()">
												<span class="glyphicon glyphicon-print"></span>Print</button>
											</div>
										</div>
									</div>

									<div class="panel box box-danger"></div>

									<div class="box-body">
										<div class="row">
											<div class="col-md-8">

												<div class="box-body">
													<div class="table-responsive">
														<table class="table table-bordered"
															id="FlatMarketPriceTable">
															<thead>
																<tr bgcolor="#4682B4">
																	<th width="30%">Details</th>
																	<th width="20%">%</th>
																	<th width="20%">Initial Amount</th>
																	<th width="20%">Final Amount</th>

																</tr>
															</thead>
															<tbody>
																<tr>
																	<td>Rate</td>
																	<td></td>
																	<td><label id="rate"></label></td>
																	<td><input type="text" class="form-control"
																		id="editRate" name="editRate"
																		onchange="calculate(this.value)"></td>
																</tr>

																<tr>
																	<td>Flat Area(Sq.Ft)</td>
																	<td></td>
																	<td><label id="flatArea"></label></td>
																	<td><input type="text" class="form-control"
																		id="editflatArea" name="editflatArea" readonly></td>
																</tr>

																<tr>
																	<td>Flat Cost</td>
																	<td></td>
																	<td><label id="flatbasicCost"></label></td>
																	<td><input type="text" class="form-control"
																		id="editflatbasicCost" name="editflatbasicCost"
																		readonly></td>
																</tr>

																<tr>
																	<td>Infrastructure</td>
																	<td></td>
																	<td><label id="infrastructure"></label></td>
																	<td><input type="text" class="form-control"
																		id="editInfrastructure" name="editInfrastructure"
																		onchange="calculate(this.value)"></td>

																</tr>
																<tr>
																	<td>Agg Value</td>
																	<td></td>
																	<td><label id="aggValue"></label></td>
																	<td><input type="text" class="form-control"
																		id="editaggValue" name="editaggValue" readonly></td>

																</tr>
																<tr>
																	<td>Stamp Duty</td>
																	<td><input type="text" class="form-control"
																		id="stampDutyPer" name="stampDutyPer" 
																		onchange="calculate(this.value)"></td>
																	<td><label id="stampDuty"></label></td>
																	<td><input type="text" class="form-control"
																		id="editStampDuty" name="editStampDuty" readonly></td>

																</tr>
																<tr>
																	<td>Registration</td>
																	<td><input type="text" class="form-control"
																		id="registrationPer" name="registrationPer" 
																		onchange="calculate(this.value)"></td>
																	<td><label id="registration"></label></td>
																	<td><input type="text" class="form-control"
																		id="editRegistration" name="editRegistration" readonly></td>

																</tr>
																<tr>
																	<td>Legal Charges</td>
																	<td></td>
																	<td><label id="legalCharges"></label></td>
																	<td><input type="text" class="form-control"
																		id="editLegalCharges" name="editLegalCharges"
																		onchange="calculate(this.value)"></td>

																</tr>
																<tr>
																	<td>GST</td>
																	<td><input type="text" class="form-control"
																		id="totalGstPer" name="totalGstPer"
																		onchange="calculate(this.value)"></td>
																	<td><label id="totalGst"></label></td>
																	<td><input type="text" class="form-control"
																		id="editTotalGst" name="editTotalGst" readonly></td>

																</tr>
																<tr>
																	<td>Total</td>
																	<td></td>
																	<td><label id="totalAmount"></label></td>
																	<td><input type="text" class="form-control"
																		id="edittotalAmount" name="edittotalAmount" readonly></td>

																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>
											<input type="hidden" id="tdsAmount" name="tdsAmount">
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</section>
			</form>

		</div>

		<%@ include file="footer.jsp"%>
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

	<script
		src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap 3.3.7 -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- DataTables -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
	<!-- SlimScroll -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script
		src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script
		src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
	<!-- Page script -->
	<script>
		function clearAll() {

			$('#projectIdSpan').html('');
			$('#buildingIdSpan').html('');
			$('#wingIdSpan').html('');
			$('#flatNumberSpan').html('');
			$('#SearchFlatMarkertPriceSpan').html('');
		}

		function PrintFlatWiseMarketPrice() 
		{

			clearAll();
			//for project name
			if (document.flatmarketpriceform.projectId.value == "Default") {
				$('#projectIdSpan').html('Please Select Project');
				document.flatmarketpriceform.projectId.focus();
				return false;
			}
			//for Building name

			if (document.flatmarketpriceform.buildingId.value == "Default") {
				$('#buildingIdSpan').html('Please Select Project Building');
				document.flatmarketpriceform.buildingId.focus();
				return false;
			}
			if (document.flatmarketpriceform.wingId.value == "Default") {
				$('#wingIdSpan').html('Please Select Project wing');
				document.flatmarketpriceform.buildingId.focus();
				return false;
			}
			if (document.flatmarketpriceform.flatNumber.value == "") {
				$('#flatNumberSpan').html('Please, enter agent Flat Number..!');
				document.flatmarketpriceform.flatNumber.focus();
				return false;
			} else if (document.flatmarketpriceform.flatNumber.value
					.match(/^[\s]+$/)) {
				$('#flatNumberSpan').html('Please, enter valid Flat Number..!');
				document.flatmarketpriceform.flatNumber.value = "";
				document.flatmarketpriceform.flatNumber.focus();
				return false;
			}

			if (document.flatmarketpriceform.edittotalAmount.value == "") {
				$('#SearchFlatMarkertPriceSpan')
						.html('Please, search first..!');
				return false;
			} else if (document.flatmarketpriceform.edittotalAmount.value
					.match(/^[\s]+$/)) {
				$('#SearchFlatMarkertPriceSpan')
						.html('Please, search first..!');
				return false;
			}
			
			
			var projectId = $('#projectId').val();
			var buildingId = $('#buildingId').val();
			var wingId = $('#wingId').val();

			var flatNumber = $('#flatNumber').val();
			var editRate = $('#editRate').val();
			var editflatArea = $('#editflatArea').val();
			var editflatbasicCost = $('#editflatbasicCost').val();
			var editInfrastructure = $('#editInfrastructure').val();
			var editaggValue = $('#editaggValue').val();
			var editStampDuty = $('#editStampDuty').val();
			var editRegistration = $('#editRegistration').val();
			var editLegalCharges = $('#editLegalCharges').val();
			var editTotalGst = $('#editTotalGst').val();
			var edittotalAmount = $('#edittotalAmount').val();
			var tdsAmount = $('#tdsAmount').val();
			
			var newWindow = window.open("","_blank");
		
		newWindow.location.href = "${pageContext.request.contextPath}/PrintFlatWiseMarketPrice?projectId="+ projectId
					+ "&buildingId="+ buildingId
					+ "&wingId="+ wingId + ""
					+ "&flatNumber="+ flatNumber + ""
					+ "&editRate="+ editRate + ""
					+ "&editflatArea="+ editflatArea + ""
					+ "&editflatbasicCost="+ editflatbasicCost + ""
					+ "&editInfrastructure="+ editInfrastructure + ""
					+ "&editaggValue="+ editaggValue + ""
					+ "&editStampDuty="+ editStampDuty + ""
					+ "&editRegistration="+ editRegistration + ""
					+ "&editLegalCharges="+ editLegalCharges + ""
					+ "&editTotalGst="+ editTotalGst + ""
					+ "&edittotalAmount="+ edittotalAmount + ""
					+ "&tdsAmount="+ tdsAmount + "";

		
		}

		function getBuldingList() {
			clearAll();
			$("#buildingId").empty();
			var projectId = $('#projectId').val();

			$.ajax({

				url : '${pageContext.request.contextPath}/getBuildingList',
				type : 'Post',
				data : {
					projectId : projectId
				},
				dataType : 'json',
				success : function(result) {
					if (result) {
						var option = $('<option/>');
						option.attr('value', "Default").text(
								"-Select Building Name-");
						$("#buildingId").append(option);

						for (var i = 0; i < result.length; i++) {
							var option = $('<option />');
							option.attr('value', result[i].buildingId).text(
									result[i].buildingName);
							$("#buildingId").append(option);
						}
					} else {
						alert("failure111");
					}

				}
			});

			$.ajax({

				url : '${pageContext.request.contextPath}/getProjectLoading',
				type : 'Post',
				data : {
					projectId : projectId
				},
				dataType : 'json',
				success : function(result) {
					if (result) {
						document.flatmarketpriceform.totalGstPer.value = result[0].gstPer;
						document.flatmarketpriceform.stampDutyPer.value = result[0].stampdutyPer;
						document.flatmarketpriceform.registrationPer.value = result[0].registrationPer;
					} else {
						alert("failure111");
						//$("#ajax_div").hide();
					}

				}
			});
		}//end of get Building List

		function getAllWingList() {
			clearAll();
			$("#wingId").empty();
			var buildingId = $('#buildingId').val();
			var projectId = $('#projectId').val();

			$.ajax({

				url : '${pageContext.request.contextPath}/getprojectwingList',
				type : 'Post',
				data : {
					buildingId : buildingId,
					projectId : projectId
				},
				dataType : 'json',
				success : function(result) {
					if (result) {
						var option = $('<option/>');
						option.attr('value', "Default").text(
								"-Select Wing Name-");
						$("#wingId").append(option);

						for (var i = 0; i < result.length; i++) {
							var option = $('<option />');
							option.attr('value', result[i].wingId).text(
									result[i].wingName);
							$("#wingId").append(option);
						}
					} else {
						alert("failure111");
					}

				}
			});

		}

		function validate() {
			clearAll();
			//for project name
			if (document.flatmarketpriceform.projectId.value == "Default") {
				$('#projectIdSpan').html('Please Select Project');
				document.flatmarketpriceform.projectId.focus();
				return false;
			}
			//for Building name

			if (document.flatmarketpriceform.buildingId.value == "Default") {
				$('#buildingIdSpan').html('Please Select Project Building');
				document.flatmarketpriceform.buildingId.focus();
				return false;
			}
			if (document.flatmarketpriceform.wingId.value == "Default") {
				$('#wingIdSpan').html('Please Select Project wing');
				document.flatmarketpriceform.buildingId.focus();
				return false;
			}
			if (document.flatmarketpriceform.flatNumber.value == "") {
				$('#flatNumberSpan').html('Please, enter agent Flat Number..!');
				document.flatmarketpriceform.flatNumber.focus();
				return false;
			} else if (document.flatmarketpriceform.flatNumber.value
					.match(/^[\s]+$/)) {
				$('#flatNumberSpan').html('Please, enter valid Flat Number..!');
				document.flatmarketpriceform.flatNumber.value = "";
				document.flatmarketpriceform.flatNumber.focus();
				return false;
			}

			if (document.flatmarketpriceform.edittotalAmount.value == "") {
				$('#SearchFlatMarkertPriceSpan')
						.html('Please, search first..!');
				return false;
			} else if (document.flatmarketpriceform.edittotalAmount.value
					.match(/^[\s]+$/)) {
				$('#SearchFlatMarkertPriceSpan')
						.html('Please, search first..!');
				return false;
			}
		}

		function SearchFlatMarkertPrice() {
			clearAll();
			//for project name
			if (document.flatmarketpriceform.projectId.value == "Default") {
				$('#projectIdSpan').html('Please Select Project');
				document.flatmarketpriceform.projectId.focus();
				return false;
			}
			//for Building name

			if (document.flatmarketpriceform.buildingId.value == "Default") {
				$('#buildingIdSpan').html('Please Select Project Building');
				document.flatmarketpriceform.buildingId.focus();
				return false;
			}
			if (document.flatmarketpriceform.wingId.value == "Default") {
				$('#wingIdSpan').html('Please Select Project wing');
				document.flatmarketpriceform.buildingId.focus();
				return false;
			}
			if (document.flatmarketpriceform.flatNumber.value == "") {
				$('#flatNumberSpan').html('Please, enter agent Flat Number..!');
				document.flatmarketpriceform.flatNumber.focus();
				return false;
			} else if (document.flatmarketpriceform.flatNumber.value
					.match(/^[\s]+$/)) {
				$('#flatNumberSpan').html('Please, enter valid Flat Number..!');
				document.flatmarketpriceform.flatNumber.value = "";
				document.flatmarketpriceform.flatNumber.focus();
				return false;
			}

			var projectId = $('#projectId').val();
			var buildingId = $('#buildingId').val();
			var wingId = $('#wingId').val();
			var flatNumber = $('#flatNumber').val();
			var totaGst = 0;
			$
					.ajax({

						url : '${pageContext.request.contextPath}/SearchFlatWiseMarkertPrice',
						type : 'Post',
						data : {
							flatNumber : flatNumber,
							wingId : wingId,
							buildingId : buildingId,
							projectId : projectId
						},
						dataType : 'json',
						success : function(result) {
							if (result) {

								document.getElementById('rate').innerHTML = result.flatCost;
								document.flatmarketpriceform.editRate.value = result.flatCost;

								document.getElementById('flatArea').innerHTML = result.flatAreawithLoadingInFt;
								document.flatmarketpriceform.editflatArea.value = result.flatAreawithLoadingInFt;

								document.getElementById('flatbasicCost').innerHTML = (result.flatAreawithLoadingInFt * result.flatCostwithotfloorise);
								document.flatmarketpriceform.editflatbasicCost.value = (result.flatAreawithLoadingInFt * result.flatCostwithotfloorise);

								document.getElementById('infrastructure').innerHTML = result.infrastructureCharges;
								document.flatmarketpriceform.editInfrastructure.value = result.infrastructureCharges;

								document.getElementById('aggValue').innerHTML = result.aggreementAmount;
								document.flatmarketpriceform.editaggValue.value = result.aggreementAmount;

								document.getElementById('stampDuty').innerHTML = result.stampDuty;
								document.flatmarketpriceform.editStampDuty.value = result.stampDuty;

								document.getElementById('registration').innerHTML = result.registeration;
								document.flatmarketpriceform.editRegistration.value = result.registeration;

								totaGst = result.cgst + result.sgst;
								document.getElementById('totalGst').innerHTML = totaGst;
								document.flatmarketpriceform.editTotalGst.value = totaGst;

								document.getElementById('legalCharges').innerHTML = result.handlingCharge;
								document.flatmarketpriceform.editLegalCharges.value = result.handlingCharge;

								document.getElementById('totalAmount').innerHTML = result.totalCost;
								document.flatmarketpriceform.edittotalAmount.value = result.totalCost;

								calculate();
							} else {
								alert("failure111");
							}

						}
					});
		}

		function calculate() {

			var editRate = Number($('#editRate').val());
			var editflatArea = Number($('#editflatArea').val());
			var flatbasicCost = editRate * editflatArea;
			// var flatbasicCost = Number($('#editflatbasicCost').val());
			//var aggreementValue1 = Number($('#editaggValue').val());
			var registrationPer = Number($('#registrationPer').val());
			var stampDutyPer = Number($('#stampDutyPer').val());
			var infrastructureCharge = Number($('#editInfrastructure').val());
			var gstCost = Number($('#totalGstPer').val());
			var handlingCharges = Number($('#editLegalCharges').val());

			var aggreementValue1 = flatbasicCost + infrastructureCharge;

			aggreementValue1 = aggreementValue1.toFixed(0);

			var aggreementValue1Last = aggreementValue1.slice(
					aggreementValue1.length - 2, aggreementValue1.length);
			if (aggreementValue1Last > 0) {
				aggreementValue1 = Number(aggreementValue1)
						+ (100 - Number(aggreementValue1Last));
			}

			var stampDuty1 = (aggreementValue1 / 100) * stampDutyPer;
			stampDuty1 = stampDuty1.toFixed(0);

			var stampDuty1Last = stampDuty1.slice(stampDuty1.length - 2,
					stampDuty1.length);
			if (stampDuty1Last > 0) {
				stampDuty1 = Number(stampDuty1)
						+ (100 - Number(stampDuty1Last));
			}

			var registrationCost2 = 0;
			var tds = 0;

			if (aggreementValue1 > 3000000) {
				registrationCost2 = 30000;
			} else {
				registrationCost2 = (aggreementValue1 / 100) * registrationPer;
			}
			var registrationCost1 = registrationCost2;

			var gstAmount1 = (aggreementValue1 / 100) * gstCost;

			gstAmount1 = gstAmount1.toFixed(0);

			var gstAmount1Last = gstAmount1.slice(gstAmount1.length - 2,
					gstAmount1.length);
			if (gstAmount1Last > 0) {
				gstAmount1 = Number(gstAmount1)
						+ (100 - Number(gstAmount1Last));
			}

			var grandTotal1 = Number(aggreementValue1) + Number(stampDuty1)
					+ Number(registrationCost1) + Number(handlingCharges)
					+ Number(gstAmount1);

			registrationCost1 = registrationCost1.toFixed(0);
			grandTotal1 = grandTotal1.toFixed(0);

			document.flatmarketpriceform.editTotalGst.value = gstAmount1;
			document.flatmarketpriceform.editRegistration.value = registrationCost1;
			document.flatmarketpriceform.editLegalCharges.value = handlingCharges;
			document.flatmarketpriceform.editStampDuty.value = stampDuty1;
			document.flatmarketpriceform.editaggValue.value = aggreementValue1;

			document.flatmarketpriceform.edittotalAmount.value = grandTotal1;
			//document.flatmarketpriceform.bookingAmount1.value=bookingAmount1;

			if (grandTotal1 > 5000000) {
				tds = (grandTotal1) / 100;
				tds = tds.toFixed(0);
			} else {
				tds = 0;
			}

			document.flatmarketpriceform.tdsAmount.value = tds;
		}
	</script>
</body>
</html>
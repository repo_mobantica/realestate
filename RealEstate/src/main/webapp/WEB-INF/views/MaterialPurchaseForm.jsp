<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate |Add Material Purchase </title>
  <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">
 
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
    
 

  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>

<div class="wrapper">

   <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Material Purchase  Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Add Material Purchase </li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="materialform" action="${pageContext.request.contextPath}/MaterialPurchaseForm" onSubmit="return validate()" method="post">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
     
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
                <span id="statusSpan" style="color:#FF0000"></span>
              <!-- /.form-group -->
               <div class="box-body">
        	  <div class="row">
        	  
                  <input type="hidden" class="form-control" id="employeeId" name="employeeId"  value="${employeeId}" readonly>
                  <input type="hidden" class="form-control" id="materialsPurchasedHistriesId" name="materialsPurchasedHistriesId"  value="${materialsPurchasedHistriesId}" readonly>
               
        	    <div class="col-md-2">
                  <label for="materialsPurchasedId">Purchase Id</label>
                 
                  <input type="text" class="form-control" id="materialsPurchasedId" name="materialsPurchasedId"  value="${materialsPurchasedId}" readonly>
                </div>
                
            	<div class="col-md-2">
                  <label for="requisitionId">Requisition Id</label>
                  <input type="text" class="form-control" id="requisitionId" name="requisitionId"  value="${materialpurchaserequisition[0].requisitionId}" readonly>
                </div>
                 <div class="col-md-2">
                  <label for="">Applied Date</label>
                  <input type="text" class="form-control" id="" name=""  value="${materialpurchaserequisition[0].creationDate}" readonly>
                </div>
                 <div class="col-md-2">
                  <label for="materialsPurchasedId">Required Date</label>
                  <input type="text" class="form-control" id="requiredDate" name="requiredDate"  value="${materialpurchaserequisition[0].requiredDate}" readonly>
                </div>
			</div>
			</div>
			  <div class="box-body">
        	  <div class="row">
                 <div class="col-xs-5">
			   		<h4><label style="color:#008000">Employee Name : ${materialpurchaserequisition[0].employeeId}</label></h4>
			     </div> 
			     
                 <div class="col-xs-6">
			       <h4><label style="color:#008000">Project : ${materialpurchaserequisition[0].projectId} - ${materialpurchaserequisition[0].buildingId} - ${materialpurchaserequisition[0].wingId}</label></h4> 
			     
			     </div> 
			
			  </div>
            </div>	
              <div class="box-body">
        	  <div class="row">
        	     
        	     <div class="col-md-2">
	                  <label>Store Name</label><label class="text-red">* </label>
                      <select class="form-control" id="storeId" name="storeId">
				      		<option selected="selected" value="Default">-Select Store Name-</option>
                      	<c:forEach var="storeList" items="${storeList}">
	                    	<option value="${storeList.storeId}">${storeList.storeName}</option>
	                  	</c:forEach>
                      </select>
                      <span id="storeIdSpan" style="color:#FF0000"></span>
                   </div>
        	          
                   <div class="col-md-3">
	                  <label>Supplier Name</label><label class="text-red">* </label>
                      <select class="form-control" id="supplierId" name="supplierId">
				      		<option selected="selected" value="Default">-Select Supplier Name-</option>
                      	<c:forEach var="supplierList" items="${supplierList}">
	                    	<option value="${supplierList.supplierId}">${supplierList.supplierfirmName}</option>
	                  	</c:forEach>
                      </select>
                      <span id="supplierIdSpan" style="color:#FF0000"></span>
                   </div>    
        	            
			     <div class="col-xs-2">
			        <label for="delivery">Delivery</label> <label class="text-red">* </label>
                    <select class="form-control" id="delivery" name="delivery">
				  		<option selected="selected">Immediate</option>
                    	<option>Within One Week</option>
                    	<option>Within Two Week</option>
                  	</select>
                    <span id="deliverySpan" style="color:#FF0000"></span>
			     </div>
        	           
			     <div class="col-xs-2">
			        <label for="paymentterms">Payment Terms</label> <label class="text-red">* </label>
                    <select class="form-control" id="paymentterms" name="paymentterms">
				  		<option selected="selected">1</option>
                    	<option>1</option>
                    	<option>2</option>
                    	<option>3</option>
                    	<option>4</option>
                  	</select>
                    <span id="paymenttermsSpan" style="color:#FF0000"></span>
			     </div>
        	          
			     <div class="col-xs-2">
			        <label for="afterdelivery">After Delivery</label> <label class="text-red">* </label>
                    <select class="form-control" id="afterdelivery" name="afterdelivery">
				  		<option value="PDC" selected="selected">0-30 Days</option>
                    	<option value="PDC">30-45 days</option>
                    	<option value="Againest Delivery">45 Days</option>
                  	</select>
                    <span id="afterdeliverySpan" style="color:#FF0000"></span>
			     </div>
        	           
        	         
        	  </div>
        	  </div>   
        	            
        	    
              <div class="box-body">
        	  <div class="row">
        	       
        	       
        	    <div class="col-md-2">
                  <label for="transportCharges">Transport charges</label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="transportCharges" name="transportCharges" >
                    <span id="transportChargesSpan" style="color:#FF0000"></span>
                </div>
                <!-- 
                  <div class="col-xs-4">
				   <label for="billReportStatus">Bill Report</label><label class="text-red">* </label>   <span id="employeeMarriedSpan" style="color:#FF0000"></span></br>
	   				<input type="radio"  class="minimal" name="billReportStatus" id="billReportStatus" value="1" checked="checked" > Single
					   &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
	  				<input type="radio"  class="minimal" name="billReportStatus" id="billReportStatus" value="2"> Multiply
                 </div>
                  -->
        	           
        	  </div>
        	  </div>   
        	           
        	   <%--     
              <div class="box-body">
        	  <div class="row">
        	            
                  <div class="col-md-2">
                     <label for="totalPrice">Total </label><label class="text-red">*</label>
	                  <input type="text" class="form-control" id="totalPrice" name="totalPrice" value="${supplierquotationList1[0].total}" readonly="readonly">
                  </div>
                     
                  <div class="col-md-2">
                     <label for="totalDiscount">Total Discount</label><label class="text-red">*</label>
	                  <input type="text" class="form-control" id="totalDiscount" name="totalDiscount" value="${supplierquotationList1[0].discountAmount}" readonly="readonly">
                  </div>
                       
                  <div class="col-md-2">
                     <label for="totalGstAmount">Total GST</label><label class="text-red">*</label>
	                  <input type="text" class="form-control" id="totalGstAmount" name="totalGstAmount" value="${supplierquotationList1[0].gstAmount}" readonly="readonly">
                  </div>
                    
                  <div class="col-md-2">
                     <label for="totalAmount">Grand Total</label><label class="text-red">*</label>
	                  <input type="text" class="form-control" id="totalAmount" name="totalAmount" value="${supplierquotationList1[0].grandTotal}" readonly="readonly">
                  </div>
        	  </div>
        	  </div>
        	  
        	   --%>
           <div class="panel box box-danger"></div>		
	
	
	
	
	<div class="box-body">
          <div class="row">
			
			<div class="col-md-12">
              <h2 class="box-title">Material List</h2>
          <div class="box-body">
           <div class="row">        
        	<div class="col-xs-12">
        	
              <table id="materialDetailsTable" class="table table-bordered">
                <thead>
                <tr bgcolor="#4682B4">
               		<th style="width:20px">Sr.No</th>
               		<td hidden="hidden"> </td> 
                  	<th>Sub-Category</th>
                  	<th>Item Name</th>
                  	<th>Quantity</th>
                  	<th>Size</th>
                  	<th>Unit</th>
                    <th>Brand</th>
                   
                    <th>Rate</th>
                    <th>Total</th>
                    <th>Discount%</th>
                    <th>Discount Amount</th>
                    <th>GST%</th>
                    <th>GST Amount</th>
                    <th>Grand Total</th>
                  </tr>
                </thead>
                <tbody>
            
                  <c:forEach items="${materialpurchaserequisitionhistory}" var="materialpurchaserequisitionhistory" varStatus="loopStatus">
                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                      <td>${loopStatus.index+1}</td>
                      <td hidden="hidden">${materialpurchaserequisitionhistory.itemId}</td>
                      <td>${materialpurchaserequisitionhistory.subsuppliertypeId}</td>
                      <td>${materialpurchaserequisitionhistory.itemName}</td>
                      <td>${materialpurchaserequisitionhistory.itemQuantity}</td> 
                      <td>${materialpurchaserequisitionhistory.itemSize}</td>
                      <td>${materialpurchaserequisitionhistory.itemunitName}</td>
                      <td contenteditable='true' onkeyup="javascript:calcuate(${loopStatus.index+1});"></td>
                      
                      <td contenteditable='true' onkeyup="javascript:calcuate(${loopStatus.index+1});">${materialpurchaserequisitionhistory.itemInitialPrice}</td>
                      <td>0</td>
                  	  <td contenteditable='true' onkeyup="javascript:calcuate(${loopStatus.index+1});">${materialpurchaserequisitionhistory.itemInitialDiscount}</td>
                      <td>0</td>
                      <td contenteditable='true' onkeyup="javascript:calcuate(${loopStatus.index+1});">${materialpurchaserequisitionhistory.gstPer}</td>
                      <td>0</td>
                      <td>0</td>
                      </tr>
				 </c:forEach>
		       <tr>
			          <td></td>
                      <td hidden="hidden"></td>
                      <td></td>
                      <td></td> 
                      <td></td> 
                      <td></td>
                      <td></td>
                      <td></td>
                      <td><label>Total</label></td>
                      <td id="total1"></td> 
                      <td></td> 
                      <td id="discountAmount1"></td>
                      <td></td> 
                      <td id="gstAmount1"></td> 
                      <td id="grandTotal1"><label> </label></td>
			   
			    </tr>
                </tbody>
      
              </table>
            </div>
   	
            <div class="col-xs-5">
            </div>
            	<div class="col-xs-3">
            	
            	 <span id="itemBrandNameSpan" style="color:#FF0000"></span>
            	</div>
   	
			   	<div class="col-xs-4">
			   	
			   	</div>
   	<br/> <br/> <br/> <br/>
           </div>
          </div>
  		 
		  </div>
		  
	
       </div>
	 </div>
			  
		<input type="hidden" id="total" name="total">
     	<input type="hidden" id="discountAmount" name="discountAmount">
     	<input type="hidden" id="gstAmount" name="gstAmount">
     	<input type="hidden" id="grandTotal" name="grandTotal">		
     	
			   <input type="hidden" id="paymentStatus" name="paymentStatus" value="Pending"> 
				<input type="hidden" id="status" name="status" value="Applied"> 
				<input type="hidden" id="creationDate" name="creationDate" value="">
				<input type="hidden" id="updateDate" name="updateDate" value="">
				<input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>" >
				
				
				
     	<input type="hidden" id="itemIds" name="itemIds" value="">
     	<input type="hidden" id="itemNames" name="itemNames" value="">
     	<input type="hidden" id="itemSizes" name="itemSizes" value="">
     	<input type="hidden" id="itemUnits" name="itemUnits" value="">
     	<input type="hidden" id="itemBrandNames" name="itemBrandNames" value="">
     	<input type="hidden" id="quantitys" name="quantitys" value="">
     	<input type="hidden" id="rates" name="rates" value="">
     	<input type="hidden" id="totals" name="totals" value="">
     	<input type="hidden" id="discountPers" name="discountPers" value="">
     	<input type="hidden" id="discountAmounts" name="discountAmounts" value="">
     	<input type="hidden" id="gstPers" name="gstPers" value="">
     	<input type="hidden" id="gstAmounts" name="gstAmounts" value="">
     	<input type="hidden" id="grandTotals" name="grandTotals" value="">
     	<input type="hidden" id="subsuppliertypeIds" name="subsuppliertypeIds" value="">
     
		
              <!-- /.form-group -->
            </div>
            
            <!-- /.col -->
			
          </div>
          	
		   	 <br/>
			<div class="box-body">
              <div class="row">
   				 <div class="col-xs-4">
                 <div class="col-xs-2">
                	<a href="MaterialPurchaseRequisitionList"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
			     </div>
			     </div>

				 <div class="col-xs-4">
                	<button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
			     </div>

				    
			<div class="col-xs-3">
		  		<button type="button" class="btn btn-info pull-right" onclick="return AddAllPurchesData()">Submit</button>
			</div>
			
			   <div class="col-xs-3" hidden="hidden">
		  			<button type="submit" class="btn btn-info pull-right" name="submit" id="submit">Submit</button>
			    </div> 
			       
			  </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        
         </div>
      <!-- /.box -->
	
			
       </section>
	</form>
    <!-- /.content -->
    
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>

<script>
function init()
{
	//clearall();
	
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;

}


function calcuate(i)
{
	var oTable = document.getElementById('materialDetailsTable');

	var oCells = oTable.rows.item(i).cells;
	
	var quantity=0;
	quantity=Number(oCells.item(4).innerHTML); 
	
	var rate=0;
	rate=Number(oCells.item(8).innerHTML); 

	var discountPer1=0;
	discountPer1=Number(oCells.item(10).innerHTML); 
	var gstPer1=0;
	gstPer1=Number(oCells.item(12).innerHTML); 
	
	var discountAmount1=0;
	var gstAmount1=0;
	var grandTotal1=0;
	
	var total1=0;
	total1=quantity*rate;
	
	oCells.item(9).innerHTML=total1.toFixed(1);
	discountAmount1=(total1/100)*discountPer1;
	
	var ab=discountAmount1.toFixed(0);
	oCells.item(11).innerHTML=ab;
	var a=0;
	a=total1-discountAmount1;
	gstAmount1=(a/100)*gstPer1;
	
	var xy=gstAmount1.toFixed(0);
	oCells.item(13).innerHTML=xy;
	grandTotal1=a+gstAmount1;
	var pq=grandTotal1.toFixed(0);
	oCells.item(14).innerHTML=pq;
	
	var rowLength = oTable.rows.length;
	var grandTotal=0;
	var total=0;
	var discountAmount=0;
	var gstAmount=0;
	
	for (var j = 1; j < rowLength-1; j++){

		   var oCells = oTable.rows.item(j).cells;

		   var cellLength = oCells.length;
		  
		   total=total+Number(oCells.item(9).innerHTML);
		   discountAmount=discountAmount+Number(oCells.item(11).innerHTML);
		   gstAmount=gstAmount+Number(oCells.item(13).innerHTML);
		   grandTotal=grandTotal+Number(oCells.item(14).innerHTML);
			
			
	}		
	document.getElementById('total1').innerHTML = total;
	document.materialform.total.value=total;
	
	document.getElementById('discountAmount1').innerHTML = discountAmount;
	document.materialform.discountAmount.value=discountAmount;
	document.getElementById('gstAmount1').innerHTML = gstAmount;
	document.materialform.gstAmount.value=gstAmount;
	document.getElementById('grandTotal1').innerHTML = grandTotal;
	document.materialform.grandTotal.value=grandTotal; 
	//document.getElementById('grandTotal').innerHTML = grandTotal;
}



function AddAllPurchesData()
{
	$('#itemBrandNameSpan').html('');
	$('#supplierIdSpan').html('');
	$('#storeIdSpan').html('');
	$('#transportChargesSpan').html('');
	
	

	if(document.materialform.storeId.value=="Default")
	{
		$('#storeIdSpan').html('Please, select store name..!');
		document.materialform.storeId.focus();
		return false;
	}
	
	if(document.materialform.supplierId.value=="Default")
	{
		$('#supplierIdSpan').html('Please, select Supplier name..!');
		document.materialform.supplierId.focus();
		return false;
	}

	//validation for Agent firm name
	if(document.materialform.transportCharges.value=="")
	{
		 $('#transportChargesSpan').html('Please, enter transport Changes..!');
		document.materialform.transportCharges.focus();
		return false;
	}
	else if(document.materialform.transportCharges.value.match(/^[\s]+$/))
	{
		$('#transportChargesSpan').html('Please, enter transport Changes..!');
		document.materialform.transportCharges.value="";
		document.materialform.transportCharges.focus();
		return false; 	
	}
	var itemId;
	var itemName;
	var itemUnit;
	var itemSize;
	var itemBrandName;
	var quantity;
	var rate;
	var total;
	var discountPer;
	var discountAmount;
	var gstPer;
	var gstAmount;
	var grandTotal;
	
	var itemIds="";
	var itemSizes="";
	var itemUnits="";
	var itemBrandNames="";
	var quantitys="";
	var rates="";
	var totals="";
	var discountPers="";
	var discountAmounts="";
	var gstPers="";
	var gstAmounts="";
	var grandTotals="";
	var itemNames="";
	
	var oTable = document.getElementById('materialDetailsTable');
	var rowLength = oTable.rows.length;
	for (var i = 1; i < rowLength-1; i++){
		
		calcuate(i);
		   var oCells = oTable.rows.item(i).cells;

		   var cellLength = oCells.length;
		   
			itemId=oCells.item(1).innerHTML;
			itemName = oCells.item(3).innerHTML;
			quantity = oCells.item(4).innerHTML;
			itemSize= oCells.item(5).innerHTML;
			itemUnit= oCells.item(6).innerHTML;
			itemBrandName = oCells.item(7).innerHTML;
			rate = oCells.item(8).innerHTML;
			total= oCells.item(9).innerHTML;
			
			discountPer= oCells.item(10).innerHTML;
			discountAmount= oCells.item(11).innerHTML;
			gstPer= oCells.item(12).innerHTML;
			gstAmount= oCells.item(13).innerHTML;
			grandTotal= oCells.item(14).innerHTML;
			
			
			if(itemBrandName=="")
				{
				$('#itemBrandNameSpan').html('Please, select item Brand name..!');
				return false;
				}
			
		        itemIds=itemIds+itemId+"###!###";
		        itemNames=itemNames+itemName+"###!###";
		        itemSizes=itemSizes+itemSize+".###!###";
		  	 	itemUnits=itemUnits+itemUnit+"###!###";
		  	 	itemBrandNames=itemBrandNames+itemBrandName+"###!###";
		  	 	quantitys=quantitys+quantity+"###!###";
		  	 	rates=rates+rate+"###!###";
		  	 	totals=totals+total+"###!###";
		  	 	discountPers=discountPers+discountPer+"###!###";
		  	 	discountAmounts=discountAmounts+discountAmount+"###!###";
		  		gstPers=gstPers+gstPer+"###!###";
		  		gstAmounts=gstAmounts+gstAmount+"###!###";
		  	 	grandTotals=grandTotals+grandTotal+"###!###";
		      
		}

		document.materialform.itemIds.value=itemIds;
		document.materialform.itemNames.value=itemNames;
		document.materialform.itemSizes.value=itemSizes;
		document.materialform.itemUnits.value=itemUnits;
		document.materialform.itemBrandNames.value=itemBrandNames;
		document.materialform.quantitys.value=quantitys;
		document.materialform.rates.value=rates;
		document.materialform.totals.value=totals;
		document.materialform.discountPers.value=discountPers;
		document.materialform.discountAmounts.value=discountAmounts;
		document.materialform.gstPers.value=gstPers;
		document.materialform.gstAmounts.value=gstAmounts;
		document.materialform.grandTotals.value=grandTotals;
	 	
	submitFunction();
}

	
function submitFunction()
{
submit.click();
}
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
  /* 
 $(function () {
 $('#ItempurchesListTable').DataTable()
 $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
 $(function () {
 $('#ItemListTable').DataTable()
 $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
   */
   
   $(document).ready(function() {
	  $(window).keydown(function(event){
	    if(event.keyCode == 13) {
	      event.preventDefault();
	      return false;
	    }
	  });
	});
  
</script>
</body>
</html>

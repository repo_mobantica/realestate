<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate |Add Labour Attendance</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">
  <!-- Google Font -->
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
    
 

  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>

<div class="wrapper">

   <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Labour Attendance:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Add Labour Attendance </li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="materialform" action="${pageContext.request.contextPath}/AddLabourAttendance" onSubmit="return validate()" method="post">
<section class="content">
 <div class="box box-default">
  <div class="box-body">
   <div class="row">
    <div class="col-md-12"> 
    
    <div class="box-body">
         <div class="row">        
        	<div class="col-xs-12">
        	 <input type="hidden" id="labourAttendanceId" name="labourAttendanceId" value="${labourAttendanceId}">
        	 
        	<!--      	   
			  <div class="col-xs-3">
			    <label for="joiningdate">Attendance Date</label> <label class="text-red">* </label>
                <input type="text" class="form-control pull-right" id="labourJoiningdate" name="labourJoiningdate" >
                <span id="labourJoiningdateSpan" style="color:#FF0000"></span>
			 </div> 
              
        	    -->  	   
			  <div class="col-xs-3">
			    <label for="attendanceDate">Attendance Date</label> <label class="text-red">* </label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                <input type="text" class="form-control pull-right" id="attendanceDate" name="attendanceDate" >
                <span id="attendanceDateSpan" style="color:#FF0000"></span>
                </div>
			 </div> 
               
        	</div>
        </div>
      </div> 
       	
       <div class="box-body">
           <div class="row">        
        	<div class="col-xs-12">
        	
              <table id="labourDetailsTable" class="table table-bordered">
                <thead>
                <tr bgcolor="#4682B4">
               		<th style="width:20px">Sr.No</th>
                  	<th style="width:100px">Labour Id</th>
                  	<th style="width:300px">Name</th> 
                  	<th>Today Work</th>
                  	<td hidden="hidden"> </td>
                  	<th style="width:150px">Add Attendance</th>
                  	<th style="width:150px">Today Salary</th>
                  </tr>
                </thead>
                <tbody>
                
           		 <% int i=1; %>
                  <c:forEach items="${labourList}" var="labourList" varStatus="loopStatus">
                   
                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                      
                      <td>${loopStatus.index+1}</td>
                      <td>${labourList.labourId}</td>
                      <td>${labourList.labourfirstName} ${labourList.labourmiddleName} ${labourList.labourlastName}</td>
                      <td contenteditable='true'></td> 
                      <td hidden="hidden">${labourList.projectId}</td>
                      <td ><input type="checkbox" id='presentStatus<%= i %>' name='presentStatus<%= i %>' value="Present"/></td> 
                      <td contenteditable='true'>${labourList.labourBasicpay}</td>
                      </tr>
                  <% i++;%>
				 </c:forEach>
				
                </tbody>
      
              </table>
            </div>
   	
           </div>
          </div>
          
    <div class="box-body">
         <div class="row">
 
				 <div class="col-xs-1">
				 </div>
				 
                 <div class="col-xs-4">
                   <div class="col-xs-2">
                	<a href="LabourAttendance"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
			       </div>
			     </div>

				<!--  <div class="col-xs-4">
                	<button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
			     </div>
  -->
				    
			<div class="col-xs-3">
		  		<button type="button" class="btn btn-info pull-right" onclick="return AddAllAttendance()">Submit</button>
			</div>
			
			<div class="col-xs-3" hidden="hidden">
		  		<button type="submit" class="btn btn-info pull-right" name="submit" id="submit">Submit</button>
			</div> 
			       
	   </div>
    </div>
        
       	<input type="hidden" id="creationDate" name="creationDate" value="">
		<input type="hidden" id="updateDate" name="updateDate" value="">
		<input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>" >
				 
   </div>
  </div>
 </div>
</div>
</section>

</form>
 </div>
 <%@ include file="footer.jsp" %>
 <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->

<script>
function init()
{
	//clearall();
	
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	document.getElementById("attendanceDate").value = day + "/" + month + "/" + year;
}


 function AddAllAttendance()
 {
 	var id= Number($('#labourAttendanceId').val());
    var attendanceDate= $('#attendanceDate').val();

    var creationDate= $('#creationDate').val();
    var updateDate= $('#updateDate').val();
	var userName= $('#userName').val();
	
	var labourId;
	var presentStatus;
	var todayWork;
	var projectId;
	var todaySalary;
	var oTable = document.getElementById('labourDetailsTable');
	var rowLength = oTable.rows.length;
    var j=Number(1);
	for (var i = 1; i < rowLength; i++)
		{
		   var oCells = oTable.rows.item(i).cells;

		   var cellLength = oCells.length;
		   
		   labourId=oCells.item(1).innerHTML;
		   todayWork=oCells.item(3).innerHTML;
		   projectId=oCells.item(4).innerHTML;
		   todaySalary=oCells.item(6).innerHTML;
		   
		   if(document.getElementById("presentStatus"+i).checked==true)
			{
			   labourAttendanceId= id+j;
			   presentStatus="Present";
			   
				 $.ajax({

				url : '${pageContext.request.contextPath}/AddAttendanceForLabour',
				type : 'Post',
				data : { labourAttendanceId : labourAttendanceId, labourId : labourId, todayWork : todayWork, projectId : projectId, todaySalary : todaySalary, attendanceDate : attendanceDate, presentStatus : presentStatus,  
					     creationDate : creationDate, updateDate : updateDate, userName : userName},
					     
				dataType : 'json',
				success : function(result)
						  {

						  }
				   });  
				 j++;
			}
		  
			submitFunction();	
			
		}

 }
 
function submitFunction()
{
submit.click();
}

$(function ()
{
  //Initialize Select2 Elements
  $('.select2').select2()

  //Datemask dd/mm/yyyy
  $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
  //Datemask2 mm/dd/yyyy
  $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
  //Money Euro
  $('[data-mask]').inputmask()

  //Date range picker
  $('#reservation').daterangepicker()
  //Date range picker with time picker
  $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
  //Date range as a button
  $('#daterange-btn').daterangepicker(
    {
      ranges   : {
        'Today'       : [moment(), moment()],
        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      startDate: moment().subtract(29, 'days'),
      endDate  : moment()
    },
    function (start, end) {
      $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
    }
  )

  //Date picker
   $('#attendanceDate').datepicker({
    autoclose: true
  })

  //iCheck for checkbox and radio inputs
  $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
    checkboxClass: 'icheckbox_minimal-blue',
    radioClass   : 'iradio_minimal-blue'
  })
  //Red color scheme for iCheck
  $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
    checkboxClass: 'icheckbox_minimal-red',
    radioClass   : 'iradio_minimal-red'
  })
  //Flat red color scheme for iCheck
  $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
    checkboxClass: 'icheckbox_flat-green',
    radioClass   : 'iradio_flat-green'
  })

  //Colorpicker
  $('.my-colorpicker1').colorpicker()
  //color picker with addon
  $('.my-colorpicker2').colorpicker()

  //Timepicker
  $('.timepicker').timepicker({
    showInputs: false
  })
})
  /* 
 $(function () {
 $('#ItempurchesListTable').DataTable()
 $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
 $(function () {
 $('#ItemListTable').DataTable()
 $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
   */
   
   $(document).ready(function() {
	  $(window).keydown(function(event){
	    if(event.keyCode == 13) {
	      event.preventDefault();
	      return false;
	    }
	  });
	});
  
</script>
</body>
</html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Real Estate | User Model</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
<!-- daterange picker -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<!-- bootstrap datepicker -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
<!-- Bootstrap Color Picker -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
<!-- Bootstrap time Picker -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
<!-- Select2 -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
<!-- Theme style -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


<!-- Google Font -->
<style type="text/css">
tr.odd {
	background-color: #CCE5FF
}

tr.even {
	background-color: #F0F8FF
}
</style>
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control", "no-cache,no-store,must-revalidate");//HTTP 1.1
		response.setHeader("Pragma", "no-cache"); //HTTP 1.0
		response.setDateHeader("Expires", 0);

		if (session != null) {
			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null
					|| session.getAttribute("profile_img") == null) {
				response.sendRedirect("login");
			}
		}
	%>


	<div class="wrapper">

		<%@ include file="headerpage.jsp"%>
		<!-- Left side column. contains the logo and sidebar -->
		<%@ include file="menu.jsp"%>
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					User Model Access Details: <small>Preview</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><a href="#">User Management</a></li>
					<li class="active">User Model Access</li>
				</ol>
			</section>


			<form name="userModelForm"
				action="${pageContext.request.contextPath}/AddUserModel"
				onSubmit="return validate()" method="POST">
				<section class="content">

					<!-- SELECT2 EXAMPLE -->
					<div class="box box-default">

						<!-- /.box-header -->
						<div class="box-body">
							<div class="row">
								<div class="col-md-12">
									<span id="statusSpan" style="color: #FF0000"></span>
									<!-- /.form-group -->

									<div class="box-body">
										<div class="row">

											<div class="col-md-2">
												<label>User Model Id</label> <input type="text"
													class="form-control" id="userModelId" name="userModelId"
													value="${userModelId}" readonly>
											</div>

										</div>
									</div>


									<div class="box-body">
										<div class="row">

											<div class="col-md-3">
												<label>Employee Name</label> <select class="form-control"
													id="employeeId" name="employeeId">
													<option selected="selected" value="Default">-Select
														Employee Name-</option>
													<c:forEach var="employeeList" items="${employeeList}">
														<option value="${employeeList.employeeId}">${employeeList.employeefirstName}
															${employeeList.employeemiddleName}
															${employeeList.employeelastName}</option>
													</c:forEach>
												</select> <span id="employeeIdSpan" style="color: #ff0000"></span>
											</div>

											<div class="col-md-3">
												<label>Assign Role</label> <select class="form-control"
													id="role" name="role">
													<option selected="selected" value="Default">-Select
														Role-</option>
													<option value="Admin">Admin</option>
													<c:forEach var="departmentList" items="${departmentList}">
														<option value="${departmentList.departmentName}">${departmentList.departmentName}</option>
													</c:forEach>
												</select> <span id="roleSpan" style="color: #ff0000"></span>
											</div>

											<div class="col-md-1"></div>

											<div class="col-md-3">
												<label>Project Name</label>
												<div class="input-group">
													<div class="input-group-addon">
														<i class="fa fa-archive"></i>
													</div>
													<select class="form-control" id="projectId"
														name="projectId">
														<option value="Default">-select Project Name-</option>
														<option value="All">ALL</option>
														<c:forEach var="projectList" items="${projectList}">
															<option value="${projectList.projectId}">${projectList.projectName}</option>
														</c:forEach>
													</select> <span class="input-group-btn"> <a
														onclick="return AddProject()" class="btn btn-success"
														data-toggle="tooltip" title="Add Project"><i
															class="glyphicon glyphicon-plus"></i>&nbsp ADD</a>
													</span>
												</div>
												<span id="projectIdSpan" style="color: #ff0000"></span>
											</div>

										</div>
									</div>

									<div class="box-body">
										<div class="row">

											<div class="col-xs-7">
												<div class="table-responsive">

													<table id="checklisttable" class="table table-bordered">
														<thead>
															<tr bgcolor=#4682B4>
																<th>Sr.No</th>
																<th>Module Name</th>
																<th>Module Access Rights</th>
															</tr>
														</thead>

														<tbody>
															<tr>
																<td>1</td>
																<td>Administrator Master</td>
																<td><input type="checkbox" name="administrator"
																	id="administrator" value="OK">&nbsp
																	Administrator</td>
															</tr>

															<tr>
																<td>2</td>
																<td>Sales Management</td>
																<td><input type="checkbox" name="preSaleaManagment"
																	id="preSaleaManagment" value="OK">&nbsp
																	Pre-Sale Management &nbsp &nbsp &nbsp <input
																	type="checkbox" name="salesManagment"
																	id="salesManagment" value="OK">&nbsp Sales
																	Management</td>
															</tr>
															<tr>
																<td>3</td>
																<td>Reports</td>
																<td><input type="checkbox" name="flatStatusReport"
																	id="flatStatusReport" value="OK">&nbsp Flat
																	Status &nbsp &nbsp &nbsp <input type="checkbox"
																	name="enquiryReport" id="enquiryReport" value="OK">&nbsp
																	Enquiry Report &nbsp &nbsp &nbsp <input type="checkbox"
																	name="bookingReport" id="bookingReport" value="OK">&nbsp
																	Booking Report &nbsp &nbsp &nbsp <input type="checkbox"
																	name="customerPaymentReport" id="customerPaymentReport"
																	value="OK">&nbsp Customer Payment Report &nbsp
																	&nbsp &nbsp <input type="checkbox"
																	name="demandPaymentReport" id="demandPaymentReport"
																	value="OK">&nbsp Demand Payment Report &nbsp
																	&nbsp &nbsp <input type="checkbox" name="parkingReport"
																	id="parkingReport" value="OK">&nbsp Parking
																	Report &nbsp &nbsp &nbsp <input type="checkbox"
																	name="agentReport" id="agentReport" value="OK">&nbsp
																	Challan Partner Report &nbsp &nbsp &nbsp <input
																	type="checkbox" name="customerReport"
																	id="customerReport" value="OK">&nbsp Customer
																	Report</td>
															</tr>
															<tr>
																<td>4</td>
																<td>Assign Task</td>
																<td><input type="checkbox" name="addTask"
																	id="addTask" value="OK">&nbsp Add Task</td>
															</tr>

															<tr>
																<td>5</td>
																<td>HR & PayRoll Management</td>
																<td><input type="checkbox" name="employeeDetails"
																	id="employeeDetails" value="OK">&nbsp Employee
																	Details <input type="checkbox"
																	name="employeeLeaveDetails" id="employeeLeaveDetails"
																	value="OK">&nbsp Employee Leave Details <input
																	type="checkbox" name="employeeSalaryDetails"
																	id="employeeSalaryDetails" value="OK">&nbsp
																	Employee Salary Details <input type="checkbox"
																	name="employeeReport" id="employeeReport" value="OK">&nbsp
																	Report</td>
															</tr>

															<tr>
																<td>6</td>
																<td>Architectural Section</td>
																<td><input type="checkbox" name="consultancyMaster"
																	id="consultancyMaster" value="OK">&nbsp
																	Consultancy Master &nbsp &nbsp &nbsp <input
																	type="checkbox" name="architecturalSection"
																	id="architecturalSection" value="OK">&nbsp
																	Architectural Section</td>
															</tr>

															<tr>
																<td>7</td>
																<td>Engineering Management</td>
																<td><input type="checkbox"
																	name="contractorMagagement" id="contractorMagagement"
																	value="OK">&nbsp Contractor Management &nbsp
																	&nbsp &nbsp <input type="checkbox"
																	name="projectEngineering" id="projectEngineering"
																	value="OK">&nbsp Project Engineering &nbsp
																	&nbsp &nbsp <input type="checkbox" name="jrEngineering"
																	id="jrEngineering" value="OK">&nbsp
																	Jr.Engineering &nbsp &nbsp &nbsp <input type="checkbox"
																	name="qualityEngineering" id="qualityEngineering"
																	value="OK">&nbsp Quality Engineering &nbsp
																	&nbsp &nbsp <input type="checkbox" name=srSupervisor
																	id="srSupervisor" value="OK">&nbsp
																	Sr.Supervisor &nbsp &nbsp &nbsp <input type="checkbox"
																	name="jrSupervisor" id="jrSupervisor" value="OK">&nbsp
																	Jr.Supervisor</td>
															</tr>
															<tr>
																<td>8</td>
																<td>Purchase Management</td>
																<td><input type="checkbox" name="venderMaster"
																	id="venderMaster" value="OK">&nbsp Vender
																	Master <input type="checkbox" name="itemBrandMaster"
																	id="itemBrandMaster" value="OK">&nbsp Item
																	Brand Master <input type="checkbox"
																	name="inventoryMaster" id="inventoryMaster" value="OK">&nbsp
																	Inventoty Master <input type="checkbox"
																	name="materialRequisitionList"
																	id="materialRequisitionList" value="OK">&nbsp
																	Material Requisition List</td>
															</tr>
															<tr>
																<td>9</td>
																<td>Site Management</td>
																<td><input type="checkbox" name="storeManagement"
																	id="storeManagement" value="OK">&nbsp Store
																	Management &nbsp &nbsp &nbsp <input type="checkbox"
																	name="storeStocks" id="storeStocks" value="OK">&nbsp
																	Store Stocks &nbsp &nbsp &nbsp <input type="checkbox"
																	name="materialTransferToContractor"
																	id="materialTransferToContractor" value="OK">&nbsp
																	Material Transfer to contractor</td>
															</tr>
															<tr>
																<td>10</td>
																<td>Account Management</td>
																<td><input type="checkbox"
																	name="customerPaymentStatus" id="customerPaymentStatus"
																	value="OK">&nbsp Customer Payment Status &nbsp
																	&nbsp &nbsp <input type="checkbox"
																	name="supplierPaymentBill" id="supplierPaymentBill"
																	value="OK">&nbsp Supplier Payment Bill &nbsp
																	&nbsp &nbsp <input type="checkbox" name="agentPayment"
																	id="agentPayment" value="OK">&nbsp Agent
																	payment</td>
															</tr>
															<tr>
																<td>11</td>
																<td>Rera</td>
																<td><input type="checkbox"
																	name="projectDevelopmentWork"
																	id="projectDevelopmentWork" value="OK">&nbsp
																	Project Development Work &nbsp &nbsp &nbsp <input
																	type="checkbox" name="architectsCertificate"
																	id="architectsCertificate" value="OK">&nbsp
																	Architect's Certificate &nbsp &nbsp &nbsp <input
																	type="checkbox" name="projectSpecificationMaster"
																	id="projectSpecificationMaster" value="OK">&nbsp
																	Project Specification Master</td>
															</tr>
															<tr>
																<td>12</td>
																<td>User Management</td>
																<td><input type="checkbox" name="userManagement"
																	id="userManagement" value="OK">&nbsp User
																	Management</td>
															</tr>
															<tr>
																<td>13</td>
																<td>Licensing Department</td>
																<td><input type="checkbox"
																	name="licensingDepartment" id="licensingDepartment"
																	value="OK">&nbsp Licensing Department</td>
															</tr>
															<tr>
																<td>14</td>
																<td>Documentation</td>
																<td><input type="checkbox" name="companyDocuments"
																	id="companyDocuments" value="OK">&nbsp Company
																	Documents &nbsp &nbsp &nbsp <input type="checkbox"
																	name="reraDocuments" id="reraDocuments" value="OK">&nbsp
																	Rera Documents</td>
															</tr>
														</tbody>
													</table>
													<span id="accessMenuSpan" style="color: #ff0000"></span>
												</div>
											</div>

											<div class="col-xs-5">
												<div class="table-responsive">

													<table id="userProjectListTable"
														class="table table-bordered">
														<thead>

															<tr bgcolor=#4682B4>
																<th>Sr.No.</th>
																<th>Project Name</th>
																<th>Action</th>
															</tr>

														</thead>

														<tbody>

															<s:forEach items="${assignedProjectList}"
																var="assignedProjectList" varStatus="loopStatus">
																<tr
																	class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
																	<td>${loopStatus.index+1}</td>
																	<td>${assignedProjectList.projectName}</td>
																	<td><a
																		onclick="DeleteAssignedProject('${assignedProjectList.projectassignId}')"
																		class="btn btn-danger btn-sm" data-toggle="tooltip"
																		title="Delete Project"><i
																			class="glyphicon glyphicon-remove"></i></a></td>
																</tr>
															</s:forEach>

														</tbody>
													</table>
													<span id="assignProjectListSpan" style="color: #ff0000"></span>
												</div>
											</div>

										</div>
									</div>

									<div class="panel box box-danger"></div>
									<div class="box-body">
										<div class="row">

											<input type="hidden" id="status" name="status" value="Active">
											<input type="hidden" id="creationDate" name="creationDate"
												value=""> <input type="hidden" id="updateDate"
												name="updateDate" value=""> <input type="hidden"
												id="userName" name="userName"
												value="<%=session.getAttribute("user")%>"> <input
												type="hidden" id="userStatus" name="userStatus"
												value="${userStatus}">
											<!-- /.form-group -->
										</div>
									</div>
								</div>


								<!-- /.col -->

							</div>

							<div class="box-body">
								<div class="row">
									<div class="col-xs-4">
										<div class="col-xs-2">
											<a href="UserMaster"><button type="button"
													class="btn btn-block btn-primary" value="Back"
													style="width: 90px">Back</button></a>
										</div>
									</div>
									<div class="col-xs-3">
										<button type="reset" class="btn btn-default" value="reset"
											style="width: 90px">Reset</button>
									</div>
									<div class="col-xs-2">
										<button type="submit" class="btn btn-info pull-right"
											name="submit">Submit</button>
									</div>
								</div>
							</div>
							<!-- /.row -->
						</div>
						<!-- /.box-body -->

					</div>

				</section>
			</form>
			<!-- /.content -->

		</div>
		<!-- /.content-wrapper -->

		<!-- Control Sidebar -->
		<%@ include file="footer.jsp"%>
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

	<!-- jQuery 3 -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap 3.3.7 -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- Select2 -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
	<!-- InputMask -->
	<script
		src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
	<!-- date-range-picker -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
	<!-- bootstrap datepicker -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
	<!-- bootstrap color picker -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
	<!-- bootstrap time picker -->
	<script
		src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
	<!-- SlimScroll -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<!-- iCheck 1.0.1 -->
	<script
		src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
	<!-- FastClick -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script
		src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script
		src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
	<!-- Page script -->
	<script type="text/javascript"
		src="http://code.jquery.com/jquery-latest.js"></script>

	<script>
		function AddUserProjectList() {
			clearAll();
			if (document.userModelForm.employeeId.value == "Default") {
				$('#employeeIdSpan').html('Please, select employee name..!');
				document.userModelForm.employeeId.focus();
				return false;
			}

			var userModelId = $('#userModelId').val();
			var employeeId = $('#employeeId').val();
			var projectId = $('#projectId').val();

			$("#userProjectListTable tr").detach();

			$
					.ajax({

						url : '${pageContext.request.contextPath}/getUserProjectListList',
						type : 'Post',
						data : {
							userModelId : userModelId,
							employeeId : employeeId,
							projectId : projectId
						},
						dataType : 'json',
						success : function(result) {
							if (result) {
								$('#userProjectListTable')
										.append(
												'<tr style="background-color: #4682B4;">	<th style="width:300px">Project Name</th><th>Action</th></tr>');

								for (var i = 0; i < result.length; i++) {
									if (i % 2 == 0) {
										var id = result[i].userProjectListId;
										$('#userProjectListTable')
												.append(
														'<tr style="background-color: #F0F8FF;"><td>'
																+ result[i].projectName
																+ '</td><td><a onclick="DeleteUserProjectList(\''
																+ id
																+ '\')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
									} else {
										var id = result[i].userProjectListId;
										$('#userProjectListTable')
												.append(
														'<tr style="background-color: #CCE5FF;"><td>'
																+ result[i].projectName
																+ '</td><td><a onclick="DeleteUserProjectList(\''
																+ id
																+ '\')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
									}

								}
							} else {
								alert("failure111");
							}

						}
					});
		}

		function DeleteUserProjectList(userProjectListId) {

			var employeeId = $('#employeeId').val();

			$("#userProjectListTable td").detach();

			$("#userProjectListTable tr").detach();

			$
					.ajax({

						url : '${pageContext.request.contextPath}/DeleteUserProjectList',
						type : 'Post',
						data : {
							userProjectListId : userProjectListId,
							employeeId : employeeId
						},
						dataType : 'json',
						success : function(result) {
							if (result) {
								$('#userProjectListTable')
										.append(
												'<tr style="background-color: #4682B4;">	<th style="width:300px">Project Name</th><th>Action</th></tr>');

								for (var i = 0; i < result.length; i++) {
									if (i % 2 == 0) {
										var id = result[i].userProjectListId;
										$('#userProjectListTable')
												.append(
														'<tr style="background-color: #F0F8FF;"><td>'
																+ result[i].projectName
																+ '</td><td><a onclick="DeleteUserProjectList(\''
																+ id
																+ '\')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
									} else {
										var id = result[i].userProjectListId;
										$('#userProjectListTable')
												.append(
														'<tr style="background-color: #CCE5FF;"><td>'
																+ result[i].projectName
																+ '</td><td><a onclick="DeleteUserProjectList(\''
																+ id
																+ '\')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
									}

								}
							} else {
								alert("failure111");
							}

						}
					});
		}

		function clearAll() {
			$('#employeeIdSpan').html('');
			$('#roleSpan').html('');
			$('#accessMenuSpan').html('');
		}

		function validate() {
			clearAll();

			if (document.userModelForm.employeeId.value == "Default") {
				$('#employeeIdSpan').html('Please, select employee name..!');
				document.userModelForm.employeeId.focus();
				return false;
			}

			if (document.userModelForm.role.value == "Default") {
				$('#roleSpan').html('Please, select role..!');
				document.userModelForm.role.focus();
				return false;
			}

		}

		function init() {
			//clearall();

			var date = new Date();
			var year = date.getFullYear();
			var month = date.getMonth() + 1;
			var day = date.getDate();

			document.getElementById("creationDate").value = day + "/" + month
					+ "/" + year;
			document.getElementById("updateDate").value = day + "/" + month
					+ "/" + year;

			if (document.userModelForm.userStatus.value == "Fail") {
				$('#statusSpan').html('Failed to add user..!');
			}

			document.userModelForm.employeeId.focus();
		}

		function AddProject() {
			$('#employeeIdSpan').html('');
			$('#projectIdSpan').html('');

			var userModelId = $('#userModelId').val();
			var employeeId = $('#employeeId').val();
			var projectId = $('#projectId').val();

			if (employeeId == "Default") {
				$('#employeeIdSpan').html('Please, select employee name..!');
				document.userModelForm.employeeId.focus();
				return false;
			}

			if (projectId == "Default") {
				$('#projectIdSpan').html('Please, select project name..!');
				document.userModelForm.projectId.focus();
				return false;
			}

			$("#userProjectListTable tr").detach();
			$
					.ajax({

						url : '${pageContext.request.contextPath}/AssignProject',
						type : 'Post',
						data : {
							userModelId : userModelId,
							projectId : projectId,
							employeeId : employeeId
						},
						dataType : 'json',
						success : function(result) {
							$('#userProjectListTable')
									.append(
											'<tr style="background-color: #4682B4;"><th>Sr.No.</th> <th>Project Name</th><th>Action</th></tr>');

							for (var i = 0; i < result.length; i++) {
								if (i % 2 == 0) {
									var id = result[i].projectassignId;
									$('#userProjectListTable')
											.append(
													'<tr style="background-color: #F0F8FF;"><td>'
															+ (i + 1)
															+ '</td><td>'
															+ result[i].projectName
															+ '</td><td><a onclick="DeleteAssignedProject('
															+ id
															+ ')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td></tr>');
								} else {
									var id = result[i].projectassignId;
									$('#userProjectListTable')
											.append(
													'<tr style="background-color: #CCE5FF;"><td>'
															+ (i + 1)
															+ '</td><td>'
															+ result[i].projectName
															+ '</td><td><a onclick="DeleteAssignedProject('
															+ id
															+ ')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td></tr>');
								}

							}
						}
					});
		}

		function DeleteAssignedProject(projectassignId) {
			var userModelId = $('#userModelId').val();

			$("#userProjectListTable tr").detach();
			$
					.ajax({

						url : '${pageContext.request.contextPath}/DeleteAssignedProject',
						type : 'Post',
						data : {
							projectassignId : projectassignId,
							userModelId : userModelId
						},
						dataType : 'json',
						success : function(result) {
							$('#userProjectListTable')
									.append(
											'<tr style="background-color: #4682B4;"><th>Sr.No.</th> <th>Project Name</th><th>Action</th></tr>');

							for (var i = 0; i < result.length; i++) {
								if (i % 2 == 0) {
									var id = result[i].projectassignId;
									$('#userProjectListTable')
											.append(
													'<tr style="background-color: #F0F8FF;"><td>'
															+ (i + 1)
															+ '</td><td>'
															+ result[i].projectId
															+ '</td><td><a onclick="DeleteAssignedProject('
															+ id
															+ ')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td></tr>');
								} else {
									var id = result[i].projectassignId;
									$('#userProjectListTable')
											.append(
													'<tr style="background-color: #CCE5FF;"><td>'
															+ (i + 1)
															+ '</td><td>'
															+ result[i].projectId
															+ '</td><td><a onclick="DeleteAssignedProject('
															+ id
															+ ')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td></tr>');
								}

							}
						}
					});

		}

		/* $(function () {
		    $('#userProjectListTable').DataTable()
		    $('#example2').DataTable({
		      'paging'      : true,
		      'lengthChange': false,
		      'searching'   : false,
		      'ordering'    : true,
		      'info'        : true,
		      'autoWidth'   : false
		    })
		  }) */
	</script>
</body>
</html>

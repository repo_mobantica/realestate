<!-- Left side column. contains the logo and sidebar -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<aside class="main-sidebar">

	<section class="sidebar">

		<ul class="sidebar-menu" data-widget="tree">

			<li class="treeview "><a href="#">
					<li><a href="home"><i class="fa fa-home"></i>Home</a></li> <span
					class="pull-right-container"> </span>
			</a></li>

			<c:choose>
				<c:when test="${userMenuAccessList[0].administrator eq 'OK'}">
					<li class="treeview"><a href="#"> <i
							class="glyphicon glyphicon-user"></i> <span>Administrator
								Master</span> <span class="pull-right-container"> <i
								class="fa fa-angle-left pull-right"></i>
						</span>
					</a>

						<ul class="treeview-menu">
<!-- 
							<li class="treeview"><a href="#"><i
									class="glyphicon glyphicon-hand-right"></i> Area <span
									class="pull-right-container"> <i
										class="fa fa-angle-left pull-right"></i>
								</span> </a>
								<ul class="treeview-menu">

									<li><a href="CountryMaster"><i class="fa fa-circle-o"></i>Country
											Master</a></li>
									<li><a href="StateMaster"><i class="fa fa-circle-o"></i>State
											Master</a></li>
									<li><a href="CityMaster"><i class="fa fa-circle-o"></i>
											City Master</a></li>
									<li><a href="LocationAreaMaster"><i
											class="fa fa-circle-o"></i> Location Master</a></li>

								</ul></li>
  -->

							<li><a href="CompanyMaster"><i class="fa fa-circle-o"></i>
									Company Master</a></li>

							<li><a href="BankMaster"><i class="fa fa-circle-o"></i>
									Bank APF</a></li>

							<li class="treeview"><a href="#"><i
									class="glyphicon glyphicon-hand-right"></i> Project <span
									class="pull-right-container"> <i
										class="fa fa-angle-left pull-right"></i>
								</span> </a>
								<ul class="treeview-menu">
									<li><a href="ProjectMaster"><i class="fa fa-circle-o"></i>
											Project Master</a></li>
									<li><a href="ProjectDetailsMaster"><i
											class="fa fa-circle-o"></i> Project Details Master</a></li>
									<li><a href="ProjectBuildingMaster"><i
											class="fa fa-circle-o"></i> Project Building Master</a></li>
									<li><a href="ProjectWingMaster"><i
											class="fa fa-circle-o"></i> Project Wing Master</a></li>
									<li><a href="FloorMaster"><i class="fa fa-circle-o"></i>
											Floor Master</a></li>
									<li><a href="FlatMaster"><i class="fa fa-circle-o"></i>
											Flat Master</a></li>
									<li><a href="ParkingZoneMaster"><i
											class="fa fa-circle-o"></i> Parking Zone Master</a></li>
									<li><a href="PaymentSchedulerMaster"><i
											class="fa fa-circle-o"></i> Payment Scheduler Master</a></li>
								</ul></li>


							<li class="treeview"><a href="#"><i
									class="glyphicon glyphicon-hand-right"></i> Contractor <span
									class="pull-right-container"> <i
										class="fa fa-angle-left pull-right"></i>
								</span> </a>
								<ul class="treeview-menu">
									<li><a href="ContractorTypeMaster"><i
											class="fa fa-circle-o"></i>Contractor Type Master</a></li>
									<li><a href="SubContractorTypeMaster"><i
											class="fa fa-circle-o"></i>Sub Contractor Type Master</a></li>
									<li><a href="ContractorMaster"><i
											class="fa fa-circle-o"></i> Contractor Master</a></li>

								</ul></li>

							<li><a href="AgentMaster"><i class="fa fa-circle-o"></i>
									Challan Parter Master</a></li>

							<li class="treeview"><a href="#"><i
									class="glyphicon glyphicon-hand-right"></i> Other <span
									class="pull-right-container"> <i
										class="fa fa-angle-left pull-right"></i>
								</span> </a>
								<ul class="treeview-menu">
									<li><a href="CurrencyMaster"><i class="fa fa-circle-o"></i>
											Currency Master</a></li>
									<li><a href="TaxMaster"><i class="fa fa-circle-o"></i>
											Tax Master</a></li>
									<li><a href="EnquirySourceMaster"><i
											class="fa fa-circle-o"></i> Enquiry Source Master</a></li>
									<li><a href="SubEnquirySourceMaster"><i
											class="fa fa-circle-o"></i> Sub Enquiry Source Master</a></li>
									<li><a href="OccupationMaster"><i
											class="fa fa-circle-o"></i> Occupation Master</a></li>
									<li><a href="BudgetMaster"><i class="fa fa-circle-o"></i>
											Budget Master</a></li>
								</ul></li>

							<li class="treeview"><a href="#"><i
									class="glyphicon glyphicon-hand-right"></i> COA <span
									class="pull-right-container"> <i
										class="fa fa-angle-left pull-right"></i>
								</span> </a>
								<ul class="treeview-menu">
									<li><a href="ChartofAccountMaster"><i
											class="fa fa-circle-o"></i> Chart Of Account Master</a></li>
									<li><a href="SubChartofAccountMaster"><i
											class="fa fa-circle-o"></i> Sub-Chart Of Account Master</a></li>
								</ul></li>

						</ul></li>
				</c:when>
			</c:choose>

			<c:choose>
				<c:when
					test="${(userMenuAccessList[0].preSaleaManagment ne null) or (userMenuAccessList[0].salesManagment ne null)}">
					<li class="treeview"><a href="#"> <i
							class="glyphicon glyphicon-knight"></i> <span>Sales
								Management</span> <span class="pull-right-container"> <i
								class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
						<ul class="treeview-menu">

							<c:choose>
								<c:when
									test="${userMenuAccessList[0].preSaleaManagment eq 'OK'}">
									<li class="treeview"><a href="#"><i
											class="glyphicon glyphicon-hand-right"></i> Pre-Sale
											Management <span class="pull-right-container"> <i
												class="fa fa-angle-left pull-right"></i>
										</span> </a>
										<ul class="treeview-menu">
											<li><a href="EnquiryMaster"><i
													class="fa fa-circle-o"></i>Add Enquiry</a></li>
											<li><a href="TodayEnquiryFollowUp"><i
													class="fa fa-circle-o"></i>Todays Follow-up</a></li>
											<!-- 		
											<li><a href="FlatMarketPrice"><i
													class="fa fa-circle-o"></i> Flat Market Price </a></li>
													 -->
											<li><a href="FlatWiseMarketPrice"><i
													class="fa fa-circle-o"></i> Flat Market Price </a></li>
										</ul></li>
								</c:when>
							</c:choose>


							<c:choose>
								<c:when test="${userMenuAccessList[0].salesManagment eq 'OK'}">
									<li class="treeview"><a href="#"><i
											class="glyphicon glyphicon-hand-right"></i> Sales Management
											<span class="pull-right-container"> <i
												class="fa fa-angle-left pull-right"></i>
										</span> </a>
										<ul class="treeview-menu">

											<li><a href="BookingMaster"><i
													class="fa fa-circle-o"></i> Booking </a></li>

											<li><a href="AddCustomerPayment"><i
													class="fa fa-circle-o"></i>Payment Received</a></li>
											<li><a href="AllAgreementList"><i
													class="fa fa-circle-o"></i>Agreement Details</a></li>
											<li><a href="AllCustomerLoanList"><i
													class="fa fa-circle-o"></i>Loan Details</a></li>

											<li><a href="BanckNocList"><i class="fa fa-circle-o"></i>Bank
													NOC</a></li>
											<li><a href="AllCustomerExtraChargesList"><i
													class="fa fa-circle-o"></i>Extra Charges</a></li>

											<li><a href="AllBookingCancelList"><i
													class="fa fa-circle-o"></i>Cancellation </a></li>

											<li><a href="FlatPossessionCheckList"><i
													class="fa fa-circle-o"></i>Possession CheckList </a></li>

										</ul></li>

								</c:when>
							</c:choose>

							<c:choose>
								<c:when
									test="${userMenuAccessList[0].flatStatusReport ne null or userMenuAccessList[0].enquiryReport ne null or userMenuAccessList[0].bookingReport ne null or userMenuAccessList[0].customerPaymentReport ne null or userMenuAccessList[0].demandPaymentReport ne null or userMenuAccessList[0].parkingReport ne null or userMenuAccessList[0].agentReport ne null or userMenuAccessList[0].customerReport ne null}">
									<li class="treeview"><a href="#"> <i
											class="glyphicon glyphicon-level-up"></i> <span>Reports</span>
											<span class="pull-right-container"> <i
												class="fa fa-angle-left pull-right"></i>
										</span>
									</a>

										<ul class="treeview-menu">
											<c:choose>
												<c:when
													test="${userMenuAccessList[0].flatStatusReport ne null}">
													<li class="treeview"><a href="#"><i
															class="glyphicon glyphicon-hand-right"></i> Flat Status <span
															class="pull-right-container"> <i
																class="fa fa-angle-left pull-right"></i>
														</span> </a>
														<ul class="treeview-menu">
															<li><a href="FlatStatus"><i
																	class="fa fa-circle-o"></i>Flat Status</a></li>
															<li><a href="FlatWiseReport"><i
																	class="fa fa-circle-o"></i>Flat Status Report</a></li>
															<li><a href="SqFtWiseFlatReport"><i
																	class="fa fa-circle-o"></i>Area Wise Flat Report</a></li>
														</ul></li>
												</c:when>
											</c:choose>

											<c:choose>
												<c:when
													test="${userMenuAccessList[0].enquiryReport ne null}">
													<li class="treeview"><a href="#"><i
															class="glyphicon glyphicon-hand-right"></i> Enquiry <span
															class="pull-right-container"> <i
																class="fa fa-angle-left pull-right"></i>
														</span> </a>
														<ul class="treeview-menu">
															<li><a href="EnquiryReport"><i
																	class="fa fa-circle-o"></i>Daily Enquiry</a></li>
														</ul></li>
												</c:when>
											</c:choose>

											<c:choose>
												<c:when
													test="${userMenuAccessList[0].bookingReport ne null}">
													<li class="treeview"><a href="#"><i
															class="glyphicon glyphicon-hand-right"></i> Booking <span
															class="pull-right-container"> <i
																class="fa fa-angle-left pull-right"></i>
														</span> </a>
														<ul class="treeview-menu">
															<li><a href="WingWiseReport"><i
																	class="fa fa-circle-o"></i>Customer Booking </a></li>

														</ul></li>
												</c:when>
											</c:choose>

											<c:choose>
												<c:when
													test="${userMenuAccessList[0].customerPaymentReport ne null}">
													<li class="treeview"><a href="#"><i
															class="glyphicon glyphicon-hand-right"></i>Customer
															Payment <span class="pull-right-container"> <i
																class="fa fa-angle-left pull-right"></i>
														</span> </a>
														<ul class="treeview-menu">
															<li><a href="CustomerPaymentSummary"><i
																	class="fa fa-circle-o"></i> Payment Summary</a></li>
															<li><a href="PaymentReceiptReport"><i
																	class="fa fa-circle-o"></i>Receipt Summary</a></li>
															<li><a href="CustomerOtherPaymentSummary"><i
																	class="fa fa-circle-o"></i> Other Payment Summary</a></li>
														</ul></li>
												</c:when>
											</c:choose>

											<c:choose>
												<c:when
													test="${userMenuAccessList[0].demandPaymentReport ne null}">
													<li class="treeview"><a href="#"><i
															class="glyphicon glyphicon-hand-right"></i> Demand
															Payment <span class="pull-right-container"> <i
																class="fa fa-angle-left pull-right"></i>
														</span> </a>
														<ul class="treeview-menu">
															<li><a href="DemandPaymentDetailsReport"><i
																	class="fa fa-circle-o"></i>Demand Payment </a></li>
														</ul></li>
												</c:when>
											</c:choose>

											<c:choose>
												<c:when
													test="${userMenuAccessList[0].parkingReport ne null}">
													<li class="treeview"><a href="#"><i
															class="glyphicon glyphicon-hand-right"></i> Parking <span
															class="pull-right-container"> <i
																class="fa fa-angle-left pull-right"></i>
														</span> </a>
														<ul class="treeview-menu">
															<li><a href="WingWiseParkingReport"><i
																	class="fa fa-circle-o"></i>Wing Wise Parking</a></li>
														</ul></li>

												</c:when>
											</c:choose>

											<c:choose>
												<c:when test="${userMenuAccessList[0].agentReport ne null}">
													<li class="treeview"><a href="#"><i
															class="glyphicon glyphicon-hand-right"></i> Challan
															Parter Payment <span class="pull-right-container">
																<i class="fa fa-angle-left pull-right"></i>
														</span> </a>
														<ul class="treeview-menu">
															<li><a href="AgentPayment"><i
																	class="fa fa-circle-o"></i>Challan Parter Payment</a></li>
														</ul></li>

												</c:when>
											</c:choose>

											<c:choose>
												<c:when
													test="${userMenuAccessList[0].customerReport ne null}">
													<li class="treeview"><a href="#"><i
															class="glyphicon glyphicon-hand-right"></i>Customer
															Report <span class="pull-right-container"> <i
																class="fa fa-angle-left pull-right"></i>
														</span> </a>

														<ul class="treeview-menu">
															<li><a href="CustomerDailyReport"><i
																	class="fa fa-circle-o"></i> Daily Report</a></li>
															<li><a href="CustomerMonthlyReport"><i
																	class="fa fa-circle-o"></i> Monthly Report</a></li>
														</ul></li>

												</c:when>
											</c:choose>

											<li><a href="TdsReportMaster"><i
													class="glyphicon glyphicon-hand-right"></i> Customer TDS
													Report</a></li>

										</ul></li>
								</c:when>
							</c:choose>

						</ul></li>
				</c:when>
			</c:choose>


			<c:choose>
				<c:when
					test="${userMenuAccessList[0].hrANDpayRollManagement ne null}">
					<li class="treeview"><a href="#"> <i
							class="glyphicon glyphicon-hand-right"></i> <span>HR &
								Payroll Management</span> <span class="pull-right-container"> <i
								class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
						<ul class="treeview-menu">


							<c:choose>
								<c:when test="${userMenuAccessList[0].addTask ne null}">
									<li class="treeview"><a href="#"> <i
											class="fa fa-tasks"></i> <span>Assign Task</span> <span
											class="pull-right-container"> <i
												class="fa fa-angle-left pull-right"></i>
										</span>
									</a>
										<ul class="treeview-menu">
											<li><a href="TaskMaster"><i class="fa fa-circle-o"></i>Task
													Master</a></li>

										</ul></li>
								</c:when>
							</c:choose>

							<li class="treeview"><a href="#"><i
									class="glyphicon glyphicon-hand-right"></i> Employee <span
									class="pull-right-container"> <i
										class="fa fa-angle-left pull-right"></i>
								</span> </a>

								<ul class="treeview-menu">
									<li><a href="DepartmentMaster"><i
											class="fa fa-circle-o"></i> Department Master</a></li>
									<li><a href="DesignationMaster"><i
											class="fa fa-circle-o"></i> Designation Master</a></li>
									<li><a href="EmployeeMaster"><i class="fa fa-circle-o"></i>
											Employee Master</a></li>
								</ul></li>

							<li><a href="EmployeeMaster"><i class="fa fa-circle-o"></i>
									Employee Details</a></li>
							<li><a href="EmployeeLeaveDetails"><i
									class="fa fa-circle-o"></i> Employee Leave Details</a></li>
							<li><a href="EmployeeSalaryDetails"><i
									class="fa fa-circle-o"></i> Employee Salary Details</a></li>
							<li><a href="MonthlySalaryReport"><i
									class="fa fa-circle-o"></i> Monthly Salary Report</a></li>

						</ul></li>
				</c:when>
			</c:choose>



			<c:choose>
				<c:when
					test="${userMenuAccessList[0].contractorMagagement ne null or userMenuAccessList[0].projectEngineering ne null or userMenuAccessList[0].jrEngineering ne null or userMenuAccessList[0].qualityEngineering ne null or userMenuAccessList[0].srSupervisor ne null or userMenuAccessList[0].jrSupervisor ne null }">
					<li class="treeview"><a href="#"> <i class="fa fa-files-o"></i>
							<span>Engineering Department</span> <span
							class="pull-right-container"> <i
								class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
						<ul class="treeview-menu">

							<c:choose>
								<c:when
									test="${userMenuAccessList[0].consultancyMaster ne null or userMenuAccessList[0].architecturalSection ne null }">

									<li class="treeview"><a href="#"> <i
											class="fa fa-tasks"></i> <span>Architectural </span> <span
											class="pull-right-container"> <i
												class="fa fa-angle-left pull-right"></i>
										</span>
									</a>
										<ul class="treeview-menu">

											<c:choose>
												<c:when
													test="${userMenuAccessList[0].consultancyMaster ne null}">
													<li><a href="ConsultancyMaster"><i
															class="fa fa-circle-o"></i>Consultancy Master</a></li>

												</c:when>
											</c:choose>

											<c:choose>
												<c:when
													test="${userMenuAccessList[0].architecturalSection ne null}">
													<li><a href="ArchitecturalSectionMaster"><i
															class="fa fa-circle-o"></i>Architectural Section</a></li>

												</c:when>
											</c:choose>

										</ul></li>
								</c:when>
							</c:choose>

							<c:choose>
								<c:when
									test="${userMenuAccessList[0].contractorMagagement ne null}">
									<li class="treeview"><a href="#"> <i
											class="glyphicon glyphicon-hand-right"></i> <span>Contractors
												Management</span> <span class="pull-right-container"> <i
												class="fa fa-angle-left pull-right"></i>
										</span>
									</a>
										<ul class="treeview-menu">
											<li><a href="ContractorWorkListMaster"><i
													class="fa fa-circle-o"></i>Contractors Work List Master</a></li>
											<li><a href="ContractorWorkOrderMaster"><i
													class="fa fa-circle-o"></i>Contractors Work Order</a></li>
										</ul></li>
								</c:when>
							</c:choose>



							<c:choose>
								<c:when
									test="${userMenuAccessList[0].projectEngineering ne null}">

									<li class="treeview"><a href="#"><i
											class="glyphicon glyphicon-hand-right"></i> Project
											Engineering <span class="pull-right-container"> <i
												class="fa fa-angle-left pull-right"></i>
										</span> </a>
										<ul class="treeview-menu">
											<li><a href="PaymentSchedulerForProjectEnggMaster"><i
													class="fa fa-circle-o"></i>Project Payment Scheduler</a></li>
											<li><a href="ContractorPaymentScheduler"><i
													class="fa fa-circle-o"></i> Contractor Payment Scheduler</a></li>
											<li><a href="ContractorPaymentBill"><i
													class="fa fa-circle-o"></i>Contractor Payment Bill</a></li>
										</ul></li>
								</c:when>
							</c:choose>



							<c:choose>
								<c:when test="${userMenuAccessList[0].jrEngineering ne null}">
									<li class="treeview"><a href="#"><i
											class="glyphicon glyphicon-hand-right"></i> Jr. Engineering <span
											class="pull-right-container"> <i
												class="fa fa-angle-left pull-right"></i>
										</span> </a>
										<ul class="treeview-menu">
											<li><a href="MaterialPurchaseRequisitionMaster"><i
													class="fa fa-circle-o"></i> Material Purchase Requisition</a></li>

										</ul></li>
								</c:when>
							</c:choose>



							<c:choose>
								<c:when
									test="${userMenuAccessList[0].qualityEngineering ne null}">
									<li class="treeview"><a href="#"><i
											class="glyphicon glyphicon-hand-right"></i> Quality
											Engineering <span class="pull-right-container"> <i
												class="fa fa-angle-left pull-right"></i>
										</span> </a>
										<ul class="treeview-menu">
											<!-- 
                 <li><a href="PaymentSchedulerForProjectEnggMaster"><i class="fa fa-circle-o"></i>Project Payment Scheduler</a></li>
                 <li><a href="ContractorPaymentScheduler"><i class="fa fa-circle-o"></i> Contractor Payment Scheduler</a></li>
             -->
										</ul></li>
								</c:when>
							</c:choose>

							<c:choose>
								<c:when test="${userMenuAccessList[0].srSupervisor ne null}">
									<li class="treeview"><a href="#"><i
											class="glyphicon glyphicon-hand-right"></i> Sr. Supervisor <span
											class="pull-right-container"> <i
												class="fa fa-angle-left pull-right"></i>
										</span> </a>
										<ul class="treeview-menu">
											<li><a href="LabourMaster"><i class="fa fa-circle-o"></i>Labour
													Master</a></li>
											<li><a href="LabourAttendance"><i
													class="fa fa-circle-o"></i> Labour Attendance</a></li>

										</ul></li>

								</c:when>
							</c:choose>




							<c:choose>
								<c:when test="${userMenuAccessList[0].jrSupervisor ne null}">
									<li class="treeview"><a href="#"><i
											class="glyphicon glyphicon-hand-right"></i> Jr. Supervisor <span
											class="pull-right-container"> <i
												class="fa fa-angle-left pull-right"></i>
										</span> </a>
										<ul class="treeview-menu">
											<!-- 
                 <li><a href="PaymentSchedulerForProjectEnggMaster"><i class="fa fa-circle-o"></i>Project Payment Scheduler</a></li>
                 <li><a href="ContractorPaymentScheduler"><i class="fa fa-circle-o"></i> Contractor Payment Scheduler</a></li>
             -->
										</ul></li>
								</c:when>
							</c:choose>



						</ul></li>
				</c:when>
			</c:choose>








			<c:choose>
				<c:when
					test="${userMenuAccessList[0].materialRequisitionList ne null}">
					<li class="treeview"><a href="#"> <i class="fa fa-files-o"></i>
							<span>Purchase management</span> <span
							class="pull-right-container"> <i
								class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
						<ul class="treeview-menu">

							<li class="treeview"><a href="#"><i
									class="glyphicon glyphicon-hand-right"></i> Vender <span
									class="pull-right-container"> <i
										class="fa fa-angle-left pull-right"></i>
								</span> </a>
								<ul class="treeview-menu">
									<li><a href="SupplierTypeMaster"><i
											class="fa fa-circle-o"></i>Vender Type Master</a></li>
									<li><a href="SubSupplierTypeMaster"><i
											class="fa fa-circle-o"></i>Sub Vender Type Master</a></li>
									<li><a href="SupplierMaster"><i class="fa fa-circle-o"></i>
											Vender Master</a></li>

								</ul></li>

							<li class="treeview"><a href="#"><i
									class="glyphicon glyphicon-hand-right"></i> Inventory <span
									class="pull-right-container"> <i
										class="fa fa-angle-left pull-right"></i>
								</span> </a>
								<ul class="treeview-menu">

									<li><a href="BrandMaster"><i class="fa fa-circle-o"></i>
											Item Brand Master</a></li>

									<li><a href="StoreMaster"><i class="fa fa-circle-o"></i>
											Store Master</a></li>
									<li><a href="ItemUnitMaster"><i class="fa fa-circle-o"></i>
											Item Unit Master</a></li>
									<!--  <li><a href="ItemSubCategoryMaster"><i class="fa fa-circle-o"></i> Item Sub Category Master</a></li>  -->
									<li><a href="ItemMaster"><i class="fa fa-circle-o"></i>
											Items Master</a></li>
								</ul></li>

							<c:choose>
								<c:when
									test="${userMenuAccessList[0].materialRequisitionList ne null}">

									<li><a href="MaterialPurchaseRequisitionList"><i
											class="glyphicon glyphicon-hand-right"></i>Material
											Requisition List</a></li>

									<li><a href="MaterialPurchaseRequisitionApprovalMaster"><i
											class="glyphicon glyphicon-hand-right"></i>Requisition Approval Master</a></li>
											
									<li><a href="MaterialPurchaseOrderList"><i
											class="glyphicon glyphicon-hand-right"></i>Purchase Order</a></li>
								</c:when>
							</c:choose>

							<c:choose>
								<c:when
									test="${userMenuAccessList[0].storeManagement ne null or userMenuAccessList[0].storeStocks ne null or userMenuAccessList[0].materialTransferToContractor ne null }">

									<li class="treeview"><a href="#"><i
											class="glyphicon glyphicon-hand-right"></i> Store management
											<span class="pull-right-container"> <i
												class="fa fa-angle-left pull-right"></i>
										</span> </a>
										<ul class="treeview-menu">
											<c:choose>
												<c:when
													test="${userMenuAccessList[0].storeManagement ne null}">
													<li><a href="StoreManagement"><i
															class="fa fa-circle-o"></i>GRN Entry</a></li>
												</c:when>
											</c:choose>

											<c:choose>
												<c:when test="${userMenuAccessList[0].storeStocks ne null}">
													<li><a href="StoreWiseStock"><i
															class="fa fa-circle-o"></i>Store Stocks</a></li>
												</c:when>
											</c:choose>

											<c:choose>
												<c:when
													test="${userMenuAccessList[0].materialTransferToContractor ne null}">
													<li><a href="MaterialTransferToContractorMaster"><i
															class="fa fa-circle-o"></i>GRO -Material Out</a></li>
												</c:when>
											</c:choose>
										</ul></li>

								</c:when>
							</c:choose>





							<!--   <li><a href="MaterialPurchasedList"><i class="fa fa-circle-o"></i>Material Purchased List</a></li> -->
						</ul></li>
				</c:when>
			</c:choose>




			<c:choose>
				<c:when
					test="${userMenuAccessList[0].customerPaymentStatus ne null or userMenuAccessList[0].supplierPaymentBill ne null or userMenuAccessList[0].agentPayment ne null }">

					<li class="treeview"><a href="#"> <i class="fa fa-book"></i>
							<span>Account</span> <span class="pull-right-container"> <i
								class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
						<ul class="treeview-menu">

							<c:choose>
								<c:when
									test="${userMenuAccessList[0].customerPaymentStatus ne null}">
									<li><a href="CustomerPaymentStatusMaster"><i
											class="fa fa-circle-o"></i>Customer Payment Status</a></li>

								</c:when>
							</c:choose>

									<li><a href="SupplierInvoiceMaster"><i
											class="fa fa-circle-o"></i>Vender Invoice Master</a></li>

								

							<c:choose>
								<c:when
									test="${userMenuAccessList[0].supplierPaymentBill ne null}">
									<li><a href="SupplierPaymentBill"><i
											class="fa fa-circle-o"></i>Vender Payment Bill</a></li>

								</c:when>
							</c:choose>

							<c:choose>
								<c:when test="${userMenuAccessList[0].agentPayment ne null}">
									<li><a href="AddAgentPayment"><i
											class="fa fa-circle-o"></i>Challan Parter Payment</a></li>

								</c:when>
							</c:choose>
						</ul></li>

				</c:when>
			</c:choose>


			<c:choose>
				<c:when
					test="${userMenuAccessList[0].projectDevelopmentWork ne null or userMenuAccessList[0].architectsCertificate ne null or userMenuAccessList[0].projectSpecificationMaster ne null or userMenuAccessList[0].reraDocuments ne null }">

					<li class="treeview"><a href="#"> <i class="fa fa-book"></i>
							<span>Rera</span> <span class="pull-right-container"> <i
								class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
						<ul class="treeview-menu">

							<c:choose>
								<c:when
									test="${userMenuAccessList[0].projectDevelopmentWork ne null}">
									<li><a href="ProjectDevelopmentWorkMaster"><i
											class="fa fa-circle-o"></i>Project Development Work</a></li>

								</c:when>
							</c:choose>

							<c:choose>
								<c:when
									test="${userMenuAccessList[0].architectsCertificate ne null}">
									<li><a href="ArchitectCertificate"><i
											class="fa fa-circle-o"></i>Architect's Certificate</a></li>

								</c:when>
							</c:choose>

							<c:choose>
								<c:when
									test="${userMenuAccessList[0].projectSpecificationMaster ne null}">
									<li><a href="ProjectSpecificationMaster"><i
											class="fa fa-circle-o"></i>Project Specification Master</a></li>

								</c:when>
							</c:choose>

							<c:choose>
								<c:when test="${userMenuAccessList[0].reraDocuments ne null}">
									<li><a href="ReraDocumentMaster"><i
											class="fa fa-circle-o"></i>Rera Documents Master</a></li>
								</c:when>
							</c:choose>
						</ul></li>


				</c:when>
			</c:choose>

			<c:choose>
				<c:when test="${userMenuAccessList[0].userManagement ne null}">
					<li class="treeview"><a href="#"> <i class="fa fa-files-o"></i>
							<span>User Management</span> <span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
						<ul class="treeview-menu">
							<li><a href="UserMaster"><i class="fa fa-circle-o"></i>User
									Master</a></li>
						</ul></li>
				</c:when>
			</c:choose>






			<c:choose>
				<c:when test="${userMenuAccessList[0].licensingDepartment ne null }">

					<li class="treeview"><a href="#"> <i class="fa fa-book"></i>
							<span>Licensing Department</span> <span
							class="pull-right-container"> <i
								class="fa fa-angle-left pull-right"></i>
						</span>
					</a>

						<ul class="treeview-menu">
							<li><a href="ProvisionalLicensingDepartmentMaster"><i
									class="fa fa-circle-o"></i>Provisional Department</a></li>
							<li><a href="PartCompletionDepartmentMaster"><i
									class="fa fa-circle-o"></i>Part Completion Department</a></li>
							<li><a href="FinalLicensingDepartmentMaster"><i
									class="fa fa-circle-o"></i>Final Department</a></li>
						</ul></li>


				</c:when>
			</c:choose>






			<c:choose>
				<c:when
					test="${userMenuAccessList[0].companyDocuments ne null or userMenuAccessList[0].reraDocuments ne null}">
					<li class="treeview"><a href="#"> <i class="fa fa-book"></i>
							<span>Legal Documentation</span> <span
							class="pull-right-container"> <i
								class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
						<ul class="treeview-menu">

							<c:choose>
								<c:when
									test="${userMenuAccessList[0].companyDocuments ne null }">
									<li><a href="CompanyDocumentMaster"><i
											class="fa fa-circle-o"></i>Company Documents Master</a></li>
								</c:when>
							</c:choose>

						</ul></li>
				</c:when>
			</c:choose>


		</ul>
	</section>
</aside>

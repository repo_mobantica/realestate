<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Edit Aggreement</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#enquirydbStatusSpan').delay(1000).fadeOut();
      });
    </script>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
    	
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">
<%@ include file="headerpage.jsp" %>
 
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Aggreement Details
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Edit aggreement</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="aggreementform" action="${pageContext.request.contextPath}/ViewAggreementForm" onSubmit="return validate()" method="Post" >
    <section class="content">
    
      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
       <div class="panel box box-danger"> </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              
              <!-- /.form-group -->
             
			<span id="statusSpan" style="color:#FF0000"></span>
				<div class="box-body">
              <div class="row">
                  <div class="col-xs-2">
                  <label for="aggreementId">Aggrrement Id</label>
                  <input type="text" class="form-control" id="aggreementId" placeholder="ID" name="aggreementId" value="${aggreementDetails[0].aggreementId}" readonly>
                </div>               
                    <div class="col-xs-2">
                  <label for="bookingId">Booking Id</label>
                  <input type="text" class="form-control" id="bookingId" placeholder="ID" name="bookingId" value="${aggreementDetails[0].bookingId}" readonly>
                </div>
              </div>
            </div>
				
				<div class="box-body">
              <div class="row">
              <div class="col-xs-5">
              </div>
        	     
			     <div class="col-xs-3">
                   <div class="input-group">
                 	<h3><span class="fa fa-home"></span> 
                    <label>Unit Details :  </label> 
                   </h3> 
       		 	  </div>
			     </div> 
              </div>
              </div>
			<div class="box-body">
              <div class="row">
                   <div class="col-xs-3">
                   	<div class="input-group">
                 	<h4><span class="fa fa-university"></span> 
                   	<label >Project Name : ${projectName}</label> 
                   	</h4> 
       		 		</div>
			      </div>  
			     
			     <div class="col-xs-3">
                   <div class="input-group">
                 	<h4><span class="fa fa-certificate"></span> 
                    <label>Building Name : ${buildingName} </label> 
                   </h4> 
       		 	  </div>
			     </div>  
			     
			     <div class="col-xs-3">
                   <div class="input-group">
                 	<h4><span class="fa fa-cube"></span> 
                    <label>Wing Name : ${wingName} </label> 
                    </h4> 
       		 	   </div>
			     </div>  
			     
			     <div class="col-xs-3">
                   <div class="input-group">
                 	<h4><span class="fa fa-align-center"></span> 
                    <label >Floor Name : ${floortypeName} </label> 
                    </h4> 
       		 	   </div>
			     </div>
			     
			     <div class="col-xs-3">
                   <div class="input-group">
                 	<h4><span class="fa fa-home"></span> 
                    <label >Flat Number : ${flatNumber} </label> 
                    </h4> 
       		 	   </div>
			     </div>  
			     
			     <div class="col-xs-3">
                   <div class="input-group">
                 	<h4><span class="fa fa-trophy"></span> 
                    <label >Flat Type : ${bookingDetails[0].flatType} </label> 
                    </h4> 
       		 	   </div>
			     </div>
			     
			      
			     <div class="col-xs-3">
                   <div class="input-group">
                 	<h4><span class="fa fa-trophy"></span> 
                    <label >Flat Facing Type : ${bookingDetails[0].flatFacing} </label> 
                    </h4> 
       		 	   </div>
			     </div>  
			     
			     <div class="col-xs-3">
                   <div class="input-group">
                 	<h4><span class="fa fa-area-chart"></span> 
                    <label >Flat Total Area : ${bookingDetails[0].flatareainSqFt} </label> 
                    </h4> 
       		 	   </div>
			     </div>
			     
			     <div class="col-xs-3">
                   <div class="input-group">
                 	<h4><span class="fa fa-money"></span> 
                    <label for="projectName">Aggreement Amount: ${bookingDetails[0].aggreementValue1} </label> 
                    </h4> 
       		 	   </div>
			     </div>  
			     
			     <div class="col-xs-3">
                   <div class="input-group">
                 	<h4><span class="fa fa-money"></span> 
                    <label>Total Amount : ${bookingDetails[0].grandTotal1} </label> 
                    </h4> 
       		 	   </div>
			     </div>
			      
              </div>
            </div>
				
				
				
			<div class="box-body">
              <div class="row">
             <div  class="panel box box-success"></div>    
			     <div class="col-xs-6">
                   <div class="input-group">
                 	<h3><span class="fa fa-user"></span> 
                    <label >Sole/First Applicant Details :  </label> 
                   </h3> 
       		 	  </div>
			     </div> 
              </div>
              </div>	
			 
			 
          <div class="box-body">
              <div class="row">
                <div class="col-xs-6">
                  <h4>
                  <small><label>Customer Name: </label></small>
      				  <label>${aggreementDetails[0].firstApplicantfirstname} ${aggreementDetails[0].firstApplicantmiddlename} ${aggreementDetails[0].firstApplicantlastname}</label>
     				 </h4>
			 </div>
            
            
			<div class="box-body" id="maidenName1" hidden="hidden">
                <div class="col-xs-6">
                 	<h4>
                   <label for="firstApplicantmaidenfirstname">Maiden Name: ${aggreementDetails[0].firstApplicantmaidenfirstname} ${aggreementDetails[0].firstApplicantmaidenmiddlename} ${aggreementDetails[0].firstApplicantmaidenlastname}</label></h4>
				 </div>
           
            </div>
			</div>
		</div>		
			
			<div class="box-body">
              <div class="row">
                 <div class="col-xs-2">
                 	<h4>
                  <label for="firstApplicantGender">Gender: ${aggreementDetails[0].firstApplicantGender}</label>   </h4>
               		
			     </div>
                  <div class="col-xs-2">
                 	<h4> <label for="firstApplicantMarried">${aggreementDetails[0].firstApplicantMarried}</label></h4>  
               		
			     
   				
  			     </div>
                <div class="col-xs-2">
                 	<h4> 
                   <label>DOB: ${aggreementDetails[0].firstApplicantDob}</label> </h4>
				   </div>
				   
				<div class="col-xs-3">
                 	<h4><label>Mob. No: ${aggreementDetails[0].firstApplicantmobileNumber1}</label>  </h4>
				 </div>
				 
				  <div class="col-xs-3">
                 	<h4> 
                   <label>Mob. No: ${aggreementDetails[0].firstApplicantmobileNumber2}</label>  </h4> 
				 </div> 
				 </div>
            </div>
			
			<div class="box-body">
              <div class="row">
                 <div class="col-xs-3">
                 	<h4>
                 <label for="firstApplicantSpuseName">Spouse Name: ${aggreementDetails[0].firstApplicantSpuseName} </label> 
               		</h4> 
       		 	
                </div>
                  <div class="col-xs-3">
                 	<h4> 
                  <label>Spouse's Date Of Birth: ${aggreementDetails[0].firstApplicantSpouseDob} </label>   
               		</h4> 
				   </div>
				   
				   <div class="col-xs-3">
			     <h4> <label>Anniversary Date: ${aggreementDetails[0].firstApplicantAnniversaryDate} </label></h4>
				   </div>
				   <div class="col-xs-3">
				 <h4> <label for="firstApplicantFatherName">Father's Name: ${aggreementDetails[0].firstApplicantFatherName} </label></h4>
                </div>
              </div>
            </div>
			
			<div class="box-body">
              <div class="row">
              <div class="col-xs-3">
			    <h4> <label for="firstApplicantEmailId">Email ID: ${aggreementDetails[0].firstApplicantEmailId} </label></h4>
			     </div> 
                   <div class="col-xs-3">
			    <h4> <label for="firstApplicantPanCardNo">Pan Card No: ${aggreementDetails[0].firstApplicantPanCardNo} </label></h4>
			     </div> 
				  <div class="col-xs-3">
			      <h4> <label for="firstApplicantAadharno">Aadhar Card No: ${aggreementDetails[0].firstApplicantAadharno}</label></h4>
			     </div> 
			    <div class="col-xs-3">
				   <h4><label for="firstApplicantMotherTonque">Mother Tongue: ${aggreementDetails[0].firstApplicantMotherTonque}</label></h4>
                </div>
                      
			   </div>
			  </div>
			
			 <div class="box-body">
              <div class="row">
              <div class="col-xs-3">
              <h4> <label for="">Present Address </label></h4>
              </div>
                  
              </div>
             </div>
                  	
			<div class="box-body">
              <div class="row">
                <div class="col-xs-3">
			   <h4> <label for="firstApplicantPresentAddress">Address: ${aggreementDetails[0].firstApplicantPresentAddress} </label></h4>
			     </div> 
				   <div class="col-xs-2">
                   <h4><label>Country: ${aggreementDetails[0].firstApplicantPresentcountryId} </label></h4>
                </div>               
                <div class="col-xs-2">
				 <h4> <label>State: ${aggreementDetails[0].firstApplicantPresentstateId} </label></h4>
               </div> 
               <div class="col-xs-2">
				   <h4><label>City: ${aggreementDetails[0].firstApplicantPresentcityId}</label></h4>
				  </div> 
                 
				    <div class="col-xs-2">
				  <h4>   <label>Area: ${aggreementDetails[0].firstApplicantPresentlocationareaId} </label></h4>
				  </div> 
				   <div class="col-xs-1">
                  <h4> <label for="firstApplicantPresentPincode">Pin Code: ${aggreementDetails[0].firstApplicantPresentPincode} </label></h4>
			     </div> 
              </div>
            </div>
				
		
			 <div class="box-body">
              <div class="row">
              <div class="col-xs-2">
              <h4> <label for="bookingaddress">Permanent Address </label></h4>
              </div>
                  
              </div>
             </div>
                  	
			<div class="box-body">
              <div class="row">
                <div class="col-xs-3">
			  <h4>  <label for="firstApplicantPermanentaddress">Address: ${aggreementDetails[0].firstApplicantPermanentaddress} </label></h4>
			     </div> 
				   <div class="col-xs-2">
                   <h4><label>Country: ${aggreementDetails[0].firstApplicantPermanentcountryId}</label></h4>
                </div>               
                <div class="col-xs-2">
				 <h4> <label>State: ${aggreementDetails[0].firstApplicantPermanentstateId} </label></h4>
               </div> 
               <div class="col-xs-2">
				  <h4> <label>City: ${aggreementDetails[0].firstApplicantPermanentcityId} </label></h4>
				  </div> 
                 
				    <div class="col-xs-2">
				    <h4> <label>Area: ${aggreementDetails[0].firstApplicantPermanentlocationareaId}</label></h4>
				  </div> 
				   <div class="col-xs-1">
                  <h4> <label for="firstApplicantPermanentPincode">Pin Code: ${aggreementDetails[0].firstApplicantPermanentPincode} </label></h4>
			     </div> 
				  
			
              </div>
            </div>
					
			<div class="box-body">
              <div class="row">
              <div class="col-xs-3">
              <h4> <label for="bookingaddress">Professional Details </label></h4>
              </div>
              </div>
             </div>
                  	
			<div class="box-body">
              <div class="row">
				   <div class="col-xs-3">
				    <h4> <label>Education Qualification: ${aggreementDetails[0].firstApplicantEducation} </label></h4>
                 </div>  
                 
               <div class="col-xs-3">
			    <h4> <label for="firstApplicantOccupation">Occupation: ${aggreementDetails[0].firstApplicantOccupation}</label></h4>
                </div> 
                
				   <div class="col-xs-3">
				 <h4>    <label>Name of Organization/ Business: ${aggreementDetails[0].firstApplicantOrganizationName}</label></h4>
                 </div> 
                  
			  <div class="col-xs-3">
                   <h4><label>Organizational Type: ${aggreementDetails[0].firstApplicantOrganizationType}</label> </h4>
			   </div>
			 
               </div>
            </div>
            		
			<div class="box-body">
              <div class="row">
                              
                <div class="col-xs-3">
                  <h4><label>Address of Organization/ Business: ${aggreementDetails[0].firstApplicantOrganizationaddress}</label></h4>
               </div> 
               
               <div class="col-xs-3">
			       <h4><label for="firstApplicantofficeNumber">Office Phone Number: ${aggreementDetails[0].firstApplicantofficeNumber}</label></h4>
			     </div> 
			      
              <div class="col-xs-3">
			      <h4><label for="firstApplicantofficeEmail">Official Email ID: ${aggreementDetails[0].firstApplicantofficeEmail} </label></h4>
			     </div>
			 
			   
			  <div class="col-xs-3">
                  <h4> <label>Industry Sector of work/ Business: ${aggreementDetails[0].firstApplicantIndustrySector}</label> </h4>
			   </div>
			    </div>
			 </div>    	 
			  		
			<div class="box-body">
              <div class="row">   
			  <div class="col-xs-3">
                   <h4><label>Work Function/ Role: ${aggreementDetails[0].firstApplicantWorkFunction}</label> </h4>
			   </div>
			
			  <div class="col-xs-3">
               <h4>   <label>Number of Years of work Experience: ${aggreementDetails[0].firstApplicantExperience}</label> </h4>
			   </div>
			      	 
			        
			  <div class="col-xs-3">
                   <h4><label>Annual Household Income(Rupees): ${aggreementDetails[0].firstApplicantIncome}</label> </h4>
			   </div>
			      	 
			     	    
              </div>
            </div>
			
	
				
			<%-- <div class="box-body">
              <div class="row">
             <div  class="panel box box-success"></div>    
			     <div class="col-xs-6">
                   <div class="input-group">
                 	<h3><span class="fa fa-user"></span> 
                    <label >Second Applicant Details :  </label> 
                   </h3> 
       		 	  </div>
			     </div> 
              </div>
              </div>	
			 
			 
          <div class="box-body">
              <div class="row">
                <div class="col-xs-2">
				<label for="secondApplicantfirstname">First Name</label>
                  <input type="text" class="form-control" id="secondApplicantfirstname" placeholder="First Name" name="secondApplicantfirstname" value="${aggreementDetails[0].secondApplicantfirstname}">
                  <span id="secondApplicantfirstnameSpan" style="color:#FF0000"></span>
                </div>
                <div class="col-xs-2">
				<label for="secondApplicantmiddlename">Middle Name </label>
                  <input type="text" class="form-control" id="secondApplicantmiddlename" placeholder="Middle Name"  name="secondApplicantmiddlename" value="${aggreementDetails[0].secondApplicantmiddlename}">
                 <span id="secondApplicantmiddlenameSpan" style="color:#FF0000"></span>
                </div>
                <div class="col-xs-2">
				<label for="secondApplicantlastname">Last Name </label>
                  <input type="text" class="form-control" id="secondApplicantlastname" placeholder="Last Name" name="secondApplicantlastname" value="${aggreementDetails[0].secondApplicantlastname}">
                 <span id="secondApplicantlastnameSpan" style="color:#FF0000"></span>
                </div>
            
            
			<div class="box-body" id="maidenName2" hidden="hidden">
                <div class="col-xs-2">
				<label for="secondApplicantmaidenfirstname">Maiden First Name</label>
                  <input type="text" class="form-control" id="secondApplicantmaidenfirstname" placeholder="First Name" name="secondApplicantmaidenfirstname" value="${aggreementDetails[0].secondApplicantmaidenfirstname}">
                  <span id="secondApplicantmaidenfirstnameSpan" style="color:#FF0000"></span>
                </div>
                <div class="col-xs-2">
				<label for="secondApplicantmaidenmiddlename">Maiden Middle Name </label>
                  <input type="text" class="form-control" id="secondApplicantmaidenmiddlename" placeholder="Middle Name"  name="secondApplicantmaidenmiddlename" value="${aggreementDetails[0].secondApplicantmaidenmiddlename}">
                 <span id="secondApplicantmaidenmiddlenameSpan" style="color:#FF0000"></span>
                </div>
                <div class="col-xs-2">
				<label for="secondApplicantmaidenlastname">Maiden Last Name </label>
                  <input type="text" class="form-control" id="secondApplicantmaidenlastname" placeholder="Last Name" name="secondApplicantmaidenlastname" value="${aggreementDetails[0].secondApplicantmaidenlastname}">
                 <span id="secondApplicantmaidenlastnameSpan" style="color:#FF0000"></span>
                </div>
            </div>
			</div>
		</div>		
			
			<div class="box-body">
              <div class="row">
                 <div class="col-xs-2">
			   <label for="radiobutton">Gender</label>   <span id="secondApplicantGenderSpan" style="color:#FF0000"></span></br>
   				<c:choose>
			     <c:when test="${aggreementDetails[0].secondApplicantGender eq 'Male'}">
			      <input type="radio" name="secondApplicantGender" id="secondApplicantGender" value="Male"  checked="checked" onclick = "secondApplicantMaidenMale()"> Male
                 </c:when>
                 <c:otherwise>  
                  <input type="radio" name="secondApplicantGender" id="secondApplicantGender" value="Male" onclick = "secondApplicantMaidenMale()"> Male
                 </c:otherwise>
			   </c:choose>
			   &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
			   <c:choose>
				  <c:when test="${aggreementDetails[0].secondApplicantGender eq 'Female'}">
  				<input type="radio" name="secondApplicantGender" id="secondApplicantGender" value="Female"  checked="checked" onclick = "secondApplicantMaidenFemale()"> Female
  				  </c:when>
  				  <c:otherwise>
  				   <input type="radio" name="secondApplicantGender" id="secondApplicantGender" value="Female" onclick = "secondApplicantMaidenFemale()"> Female
  				  </c:otherwise>
  			   </c:choose>
  			   
   			   </div>
                  <div class="col-xs-2">
			   <label for="secondApplicantMarried">Married and Unmarried</label>   <span id="secondApplicantMarriedSpan" style="color:#FF0000"></span></br>
				<c:choose>
			     <c:when test="${aggreementDetails[0].secondApplicantMarried eq 'Married'}">
			      <input type="radio" name="secondApplicantMarried" id="secondApplicantMarried" value="Married"  checked="checked" onclick = "secondApplicantMaidenFemale()"> Married
                 </c:when>
                 <c:otherwise>  
                  <input type="radio" name="secondApplicantMarried" id="secondApplicantMarried" value="Married" onclick = "secondApplicantMaidenFemale()"> Married
                 </c:otherwise>
			   </c:choose>
			   &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
			   <c:choose>
				  <c:when test="${aggreementDetails[0].secondApplicantMarried eq 'Unmarried'}">
  				<input type="radio" name="secondApplicantMarried" id="secondApplicantMarried" value="Unmarried"  checked="checked" onclick = "secondApplicantMaidenMale()"> Unmarried
  				  </c:when>
  				  <c:otherwise>
  				   <input type="radio" name="secondApplicantMarried" id="secondApplicantMarried" value="Unmarried" onclick = "secondApplicantMaidenMale()"> Unmarried
  				  </c:otherwise>
  			   </c:choose>
                 </div>
                <div class="col-xs-2">
			      <label>Date Of Birth </label> 
				  <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                 </div>
                <input type="text" class="form-control pull-right" id="secondApplicantDob" name="secondApplicantDob" value="${aggreementDetails[0].secondApplicantDob}">
               </div>
                <span id="secondApplicantDobSpan" style="color:#FF0000"></span>
				   </div>
				   
				<div class="col-xs-3">
			    <label>Mobile No(Primary)</label>
				  <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask='"mask": "9999999999"' data-mask name="secondApplicantmobileNumber1" data-mask id="secondApplicantmobileNumber1" value="${aggreementDetails[0].secondApplicantmobileNumber1}" >
                </div>
                   <span id="secondApplicantmobileNumber1Span" style="color:#FF0000"></span>
				 </div>
				  <div class="col-xs-3">
			    <label>Mobile No 2</label>
				  <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask='"mask": "9999999999"' data-mask name="secondApplicantmobileNumber2" data-mask id="secondApplicantmobileNumber2" value="${aggreementDetails[0].secondApplicantmobileNumber2}">
                </div>
                 <span id="secondApplicantmobileNumber2Span" style="color:#FF0000"></span>
				 </div> 
				 </div>
            </div>
			
			<div class="box-body">
              <div class="row">
                 <div class="col-xs-3">
				 <label for="secondApplicantSpouseName">Spouse Name </label>
                  <input type="text" class="form-control" id="secondApplicantSpouseName" placeholder="Spouse Name" name="secondApplicantSpouseName" value="${aggreementDetails[0].secondApplicantSpouseName}">
                 <span id="aggreementmaidenlastnameSpan" style="color:#FF0000"></span>
                </div>
                  <div class="col-xs-3">
			      <label>Spouse's Date Of Birth </label>
				  <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                <input type="text" class="form-control pull-right" id="secondApplicantSpouseDob" name="secondApplicantSpouseDob" value="${aggreementDetails[0].secondApplicantSpouseDob}">
               </div>
                <span id="secondApplicantSpouseDobSpan" style="color:#FF0000"></span>
				   </div>
				   <div class="col-xs-3">
			      <label>Anniversary Date </label>
				  <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
	                <input type="text" class="form-control pull-right" id="secondApplicantAnnivaversaryDate" name="secondApplicantAnnivaversaryDate" value="${aggreementDetails[0].secondApplicantAnnivaversaryDate}" >
	               </div>
	                <span id="secondApplicantAnnivaversaryDateSpan" style="color:#FF0000"></span>
				   </div>
				   <div class="col-xs-3">
				  <label for="secondApplicantFatherName">Father's Name </label>
                  <input type="text" class="form-control" id="secondApplicantFatherName" placeholder="Father Name" name="secondApplicantFatherName" value="${aggreementDetails[0].secondApplicantFatherName}">
                 <span id="secondApplicantFatherNameSpan" style="color:#FF0000"></span>
                </div>
              </div>
            </div>
			
			<div class="box-body">
              <div class="row">
              <div class="col-xs-3">
			     <label for="secondApplicantEmail">Email ID </label>
				   <div class="input-group">
	                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
	                <input type="text" class="form-control" placeholder="Email" name ="secondApplicantEmail" id="secondApplicantEmail" value="${aggreementDetails[0].secondApplicantEmail}">
	              </div>
                   <span id="secondApplicantEmailSpan" style="color:#FF0000"></span>
			     </div> 
                   <div class="col-xs-3">
			    <label for="secondApplicantPancardno">Pan Card No </label> 
                  <input type="text" class="form-control" id="secondApplicantPancardno" placeholder="Pan Card No " name="secondApplicantPancardno" value="${aggreementDetails[0].secondApplicantPancardno}">
                  <span id="bookingPancardnoSpan" style="color:#FF0000"></span>
			     </div> 
				  <div class="col-xs-2">
			      <label for="secondApplicantAadharno">Aadhar Card No</label> 
                  <input type="text" class="form-control" id="secondApplicantAadharno" placeholder="Adhar Card No " name="secondApplicantAadharno" value="${aggreementDetails[0].secondApplicantAadharno}">
			       <span id="secondApplicantAadharnoSpan" style="color:#FF0000"></span>
			     </div> 
			    <div class="col-xs-2">
				  <label for="secondApplicantMotherTongue">Mother Tongue</label> 
                  <input type="text" class="form-control" id="secondApplicantMotherTongue" placeholder="Mother Tongue" name="secondApplicantMotherTongue" value="${aggreementDetails[0].secondApplicantMotherTongue}">
                 <span id="secondApplicantMotherTongueSpan" style="color:#FF0000"></span>
                </div>
                <div class="col-xs-2">
				  <label for="secondApplicantRelation">Relation With First Applicant</label> 
                  <input type="text" class="form-control" id="secondApplicantRelation" placeholder="Relation With First Applicant" name="secondApplicantRelation" value="${aggreementDetails[0].secondApplicantRelation}">
                 <span id="secondApplicantRelationSpan" style="color:#FF0000"></span>
                </div>
                         
			   </div>
			  </div>
			
			 <div class="box-body">
              <div class="row">
              <div class="col-xs-3">
              <h4> <label for="">Present Address </label></h4>
              </div>
                  
              </div>
             </div>
                  	
			<div class="box-body">
              <div class="row">
                <div class="col-xs-3">
			   <label for="secondApplicantPresentAddress">Address </label>
                 <textarea class="form-control" rows="1" id="secondApplicantPresentAddress" placeholder="Address" name="secondApplicantPresentAddress">${aggreementDetails[0].secondApplicantPresentAddress} </textarea>
                  <span id="secondApplicantPresentAddressSpan" style="color:#FF0000"></span>
			     </div> 
				   <div class="col-xs-2">
                  <label>Country </label>
    			 <select class="form-control" name="secondApplicantPresentcountryId" id="secondApplicantPresentcountryId" onchange="getsecondApplicantPresentStateList(this.value)">
				  <option selected="selected" value="${aggreementDetails[0].secondApplicantPresentcountryId}">${aggreementDetails[0].secondApplicantPresentcountryId}</option>
                    <c:forEach var="countryList" items="${countryList}">
                      <c:choose>
	                     <c:when test="${aggreementDetails[0].secondApplicantPresentcountryId ne countryList.countryId}">
		                    <option value="${countryList.countryId}">${countryList.countryId}</option>
		                 </c:when>
		              </c:choose>
	                 </c:forEach>
                  </select>
                   <span id="secondApplicantPresentcountryIdSpan" style="color:#FF0000"></span>
                </div>               
                <div class="col-xs-2">
				 <label>State </label>
				  <select class="form-control" name="secondApplicantPresentstateId" id="secondApplicantPresentstateId" onchange="getsecondApplicantPresentCityList(this.Value)">
			  		<option selected="" value="${aggreementDetails[0].secondApplicantPresentstateId}">${aggreementDetails[0].secondApplicantPresentstateId}</option>
                  </select>
                   <span id="secondApplicantPresentstateIdSpan" style="color:#FF0000"></span>
               </div> 
               <div class="col-xs-2">
				  <label>City </label>
                      <select class="form-control" name ="secondApplicantPresentcityId" id="secondApplicantPresentcityId" onchange="getsecondApplicantPresentLocationAreaList(this.value)">
			  	<option selected="" value="${aggreementDetails[0].secondApplicantPresentcityId}">${aggreementDetails[0].secondApplicantPresentcityId}</option>
                  </select>
                  <span id="secondApplicantPresentcityIdSpan" style="color:#FF0000"></span>
				  </div> 
                 
				    <div class="col-xs-2">
				    <label>Area </label>
				        <select class="form-control" name="secondApplicantPresentlocationareaId" id="secondApplicantPresentlocationareaId" onchange="getsecondApplicantPresentpinCode(this.value)">
			    		<option selected="" value="${aggreementDetails[0].secondApplicantPresentlocationareaId}">${aggreementDetails[0].secondApplicantPresentlocationareaId}</option>
				   </select>
                   <span id="secondApplicantPresentlocationareaIdSpan" style="color:#FF0000"></span>
				  </div> 
				   <div class="col-xs-1">
                  <label for="secondApplicantPresentPincode">Pin Code </label>
                  <input type="text" class="form-control" id="secondApplicantPresentPincode" placeholder="Pin Code" name="secondApplicantPresentPincode" value="${aggreementDetails[0].secondApplicantPresentPincode}" readonly>
                    <span id="secondApplicantPresentPincodeSpan" style="color:#FF0000"></span>
			     </div> 
              </div>
            </div>
				
		
			 <div class="box-body">
              <div class="row">
              <div class="col-xs-2">
              <h4> <label for="bookingaddress">Permanent Address </label></h4>
              </div>
              <div class="col-xs-3">
			  <label for="">Permanent Address Same as Present Address</label> 
   				</div>
   				 <div class="col-xs-4">
   				<input type="radio" name="secondApplicantPresentAndPermanentAddress" id="secondApplicantPresentAndPermanentAddress" value="Yes" onclick = "secondApplicantPresentAndPermanentAddressYes()"> Yes
				   &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
  					  <input type="radio" name="secondApplicantPresentAndPermanentAddress" id="secondApplicantPresentAndPermanentAddress" value="No" onclick = "secondApplicantPresentAndPermanentAddressNo()"> No
                 </div> 
                      
              </div>
             </div>
                  	
			<div class="box-body">
              <div class="row">
                <div class="col-xs-3">
			   <label for="secondApplicantPermanentaddress">Address </label>
                 <textarea class="form-control" rows="1" id="secondApplicantPermanentaddress" placeholder="Address" name="secondApplicantPermanentaddress">${aggreementDetails[0].secondApplicantPermanentaddress}</textarea>
                  <span id="bookingaddressSpan" style="color:#FF0000"></span>
			     </div> 
				   <div class="col-xs-2">
                  <label>Country </label>
    			  <select class="form-control" name="secondApplicantPermanentcountryId" id="secondApplicantPermanentcountryId" onchange="getsecondApplicantPermanentStateList(this.value)">
				  <option selected="selected" value="${aggreementDetails[0].secondApplicantPermanentcountryId}">${aggreementDetails[0].secondApplicantPermanentcountryId}</option>
                    <c:forEach var="countryList" items="${countryList}">
                      <c:choose>
	                     <c:when test="${aggreementDetails[0].secondApplicantPermanentcountryId ne countryList.countryId}">
		                    <option value="${countryList.countryId}">${countryList.countryId}</option>
		                 </c:when>
		              </c:choose>
	                 </c:forEach>
                  </select>
                   <span id="secondApplicantPermanentcountryIdSpan" style="color:#FF0000"></span>
                </div>               
                <div class="col-xs-2">
				 <label>State </label>
				  <select class="form-control" name="secondApplicantPermanentstateId" id="secondApplicantPermanentstateId" onchange="getsecondApplicantPermanentCityList(this.Value)">
			  		<option selected="" value="${aggreementDetails[0].secondApplicantPermanentstateId}">${aggreementDetails[0].secondApplicantPermanentstateId}</option>
                  </select>
                   <span id="secondApplicantPermanentstateIdSpan" style="color:#FF0000"></span>
               </div> 
               <div class="col-xs-2">
				  <label>City </label>
                      <select class="form-control" name ="secondApplicantPermanentcityId" id="secondApplicantPermanentcityId" onchange="getsecondApplicantPermanentLocationAreaList(this.value)">
			  		<option selected="" value="${aggreementDetails[0].secondApplicantPermanentcityId}">${aggreementDetails[0].secondApplicantPermanentcityId}</option>
                  </select>
                  <span id="secondApplicantPermanentcityIdSpan" style="color:#FF0000"></span>
				  </div> 
                 
				    <div class="col-xs-2">
				    <label>Area </label>
				        <select class="form-control" name="secondApplicantPermanentlocationareaId" id="secondApplicantPermanentlocationareaId" onchange="getsecondApplicantPermanentpinCode(this.value)">
			    	<option selected="" value="${aggreementDetails[0].secondApplicantPermanentlocationareaId}">${aggreementDetails[0].secondApplicantPermanentlocationareaId}</option>
				   </select>
                   <span id="secondApplicantPermanentlocationareaIdSpan" style="color:#FF0000"></span>
				  </div> 
				   <div class="col-xs-1">
                  <label for="secondApplicantPermanentPincode">Pin Code </label>
                  <input type="text" class="form-control" id="secondApplicantPermanentPincode" placeholder="Pin Code" name="secondApplicantPermanentPincode" value="${aggreementDetails[0].secondApplicantPermanentPincode}" readonly>
                    <span id="secondApplicantPermanentPincodeSpan" style="color:#FF0000"></span>
			     </div> 
				  
			
              </div>
            </div>
					
			<div class="box-body">
              <div class="row">
              <div class="col-xs-3">
              <h4> <label for="">Professional Details </label></h4>
              </div>
                  
              </div>
             </div>
                  	
			<div class="box-body">
              <div class="row">
				   <div class="col-xs-3">
				    <label>Education Qualification </label>
				     <input type="text" class="form-control" id="secondApplicantEducation" placeholder="Education Qualifaction" name="secondApplicantEducation" value="${aggreementDetails[0].secondApplicantEducation}">
                    <span id="secondApplicantEducationSpan" style="color:#FF0000"></span> 
                 </div>  
                 
               <div class="col-xs-3">
			    <label for="secondApplicantOccupation">Occupation </label>
			     <select class="form-control" name="secondApplicantOccupation" id="secondApplicantOccupation" >
				 <option selected="selected" value="${aggreementDetails[0].secondApplicantOccupation}">${aggreementDetails[0].secondApplicantOccupation}</option>
                    <c:forEach var="occupationList" items="${occupationList}">
                      <c:choose>
	                     <c:when test="${aggreementDetails[0].secondApplicantOccupation ne occupationList.occupationName}">
		                    <option value="${occupationList.occupationName}">${occupationList.occupationName}</option>
		                 </c:when>
		              </c:choose>
	                 </c:forEach> 				
				 </select>
                 <span id="secondApplicantOccupationOccupationSpan" style="color:#FF0000"></span>
                </div> 
                
				   <div class="col-xs-3">
				    <label>Name of Organization/ Business </label>
				     <input type="text" class="form-control" id="secondApplicantOrganizationName" placeholder="Name of Organization/ Business" name="secondApplicantOrganizationName" value="${aggreementDetails[0].secondApplicantOrganizationName}">
                    <span id="secondApplicantOrganizationNameSpan" style="color:#FF0000"></span> 
                 </div> 
                  
			  <div class="col-xs-3">
                  <label>Organizational Type</label> 
                  	<select class="form-control" name="secondApplicantOrganizationType" id="secondApplicantOrganizationType">
                    	<c:choose>
                  		  <c:when test="${aggreementDetails[0].secondApplicantOrganizationType eq 'Default'}">
                    		<option selected="selected" value="Default">-Select Organizational Type-</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	     <option value="Default">-Select Organizational Type-</option>
                    	  </c:otherwise>
                    	 </c:choose>
                   <c:choose>
                  		  <c:when test="${aggreementDetails[0].secondApplicantOrganizationType eq 'Govt. Services'}">
                    		<option selected="selected" value="Govt. Services">Govt. Services</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	     <option value="Govt. Services">Govt. Services</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 
                    	 <c:choose>
                    	  <c:when test="${aggreementDetails[0].secondApplicantOrganizationType eq 'Private Ltd.'}">
                    		<option selected="selected" value="Private Ltd.">Private Ltd.</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="Private Ltd.">Private Ltd.</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 
                    	 <c:choose>
                    	  <c:when test="${aggreementDetails[0].secondApplicantOrganizationType eq 'Public Ltd'}">
                    		<option selected="selected" value="Public Ltd">Public Ltd</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="Public Ltd">Public Ltd</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 	 <c:choose>
                    	  <c:when test="${aggreementDetails[0].secondApplicantOrganizationType eq 'Proprietary'}">
                    		<option selected="selected" value="Proprietary">Proprietary</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="Proprietary">Proprietary</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 <c:choose>
                    	  <c:when test="${aggreementDetails[0].secondApplicantOrganizationType eq 'Other'}">
                    		<option selected="selected" value="Other">Other</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="Other">Other</option>
                    	  </c:otherwise>
                    	 </c:choose>
                  	</select>
                    <span id="secondApplicantOrganizationTypeSpan" style="color:#FF0000"></span>
			   </div>
			 
               </div>
            </div>
            		
			<div class="box-body">
              <div class="row">
                              
                <div class="col-xs-3">
                 <label>Address of Organization/ Business </label>
  					 <textarea class="form-control" rows="1" id="secondApplicantOrganizationaddress" placeholder="Address" name="secondApplicantOrganizationaddress"> </textarea>
                    <span id="secondApplicantOrganizationaddressSpan" style="color:#FF0000"></span>
               </div> 
               
               <div class="col-xs-3">
			      <label for="secondApplicantofficeNumber">Office Phone Number</label>
				  <div class="input-group">
                  	<div class="input-group-addon">
                   <i class="fa fa-phone"></i>
                 	</div>
                    <input type="text" class="form-control" data-inputmask = '"mask": "(999) 999-99999"' data-mask name="secondApplicantofficeNumber" id="secondApplicantofficeNumber" value="${aggreementDetails[0].secondApplicantofficeNumber}">
                    <span id="secondApplicantofficeNumberSpan" style="color:#FF0000"></span>
                  </div>
			     </div> 
			      
              <div class="col-xs-3">
			     <label for="secondApplicantofficeEmail">Official Email ID </label>
				   <div class="input-group">
	                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
	                <input type="text" class="form-control" placeholder="Email" name ="secondApplicantofficeEmail" id="secondApplicantofficeEmail" value="${aggreementDetails[0].secondApplicantofficeEmail}">
	              </div>
                   <span id="secondApplicantofficeEmailSpan" style="color:#FF0000"></span>
			     </div>
			 
			   
			  <div class="col-xs-3">
                  <label>Industry Sector of work/ Business</label> 
                  	<select class="form-control" name="secondApplicantIndustrySector" id="secondApplicantIndustrySector" >
                  	<c:choose>
                  		  <c:when test="${aggreementDetails[0].secondApplicantIndustrySector eq 'Default'}">
                    		<option selected="selected" value="Default">-Select Industry Sector of work/ Business-</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	     <option value="Default">-Select Industry Sector of work/ Business-</option>
                    	  </c:otherwise>
                    	 </c:choose>
                	  	<c:choose>
                  		  <c:when test="${aggreementDetails[0].secondApplicantIndustrySector eq 'IT'}">
                    		<option selected="selected" value="IT">IT</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	     <option value="IT">IT</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 <c:choose>
                    	  <c:when test="${aggreementDetails[0].secondApplicantIndustrySector eq 'ITES/BPO/KPO'}">
                    		<option selected="selected" value="ITES/BPO/KPO">ITES/BPO/KPO</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="ITES/BPO/KPO">ITES/BPO/KPO</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 <c:choose>
                    	  <c:when test="${aggreementDetails[0].secondApplicantIndustrySector eq 'Manufacturing'}">
                    		<option selected="selected" value="Manufacturing">Manufacturing</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="Manufacturing">Manufacturing</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 	 <c:choose>
                    	  <c:when test="${aggreementDetails[0].secondApplicantIndustrySector eq 'Financial'}">
                    		<option selected="selected" value="Financial">Financial</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="Financial">Financial</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 <c:choose>
                  		  <c:when test="${aggreementDetails[0].secondApplicantIndustrySector eq 'Hospitality Services'}">
                    		<option selected="selected" value="Hospitality Services">Hospitality Services</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	     <option value="Hospitality Services">Hospitality Services</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 <c:choose>
                    	  <c:when test="${aggreementDetails[0].secondApplicantIndustrySector eq 'Medical/ Pharmaceutical'}">
                    		<option selected="selected" value="Medical/ Pharmaceutical">Medical/ Pharmaceutical</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="Medical/ Pharmaceutical">Medical/ Pharmaceutical</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 
                    	 <c:choose>
                    	  <c:when test="${aggreementDetails[0].secondApplicantIndustrySector eq 'Media/Entertainment'}">
                    		<option selected="selected" value="Media/Entertainment">Media/Entertainment</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="Media/Entertainment">Media/Entertainment</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 	 <c:choose>
                    	  <c:when test="${aggreementDetails[0].secondApplicantIndustrySector eq 'Travel/ transport'}">
                    		<option selected="selected" value="Travel/ transport">Travel/ transport</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="Travel/ transport">Travel/ transport</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	  <c:choose>
                    	  <c:when test="${aggreementDetails[0].secondApplicantIndustrySector eq 'Retails Services'}">
                    		<option selected="selected" value="Retails Services">Retails Services</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="Retails Services">Retails Services</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 <c:choose>
                    	  <c:when test="${aggreementDetails[0].secondApplicantIndustrySector eq 'Telecom'}">
                    		<option selected="selected" value="Telecom">Telecom</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="Telecom">Telecom</option>
                    	  </c:otherwise>
                    	 </c:choose> 
                    	 <c:choose>
                    	  <c:when test="${aggreementDetails[0].secondApplicantIndustrySector eq 'Other'}">
                    		<option selected="selected" value="Other">Other</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="Other">Other</option>
                    	  </c:otherwise>
                    	 </c:choose>
                   	</select>
                    <span id="secondApplicantIndustrySectorSpan" style="color:#FF0000"></span>
			   </div>
			    </div>
			 </div>    	 
			  		
			<div class="box-body">
              <div class="row">   
			  <div class="col-xs-3">
                  <label>Work Function/ Role</label> 
                  	<select class="form-control" name="secondApplicantWorkFunction" id="secondApplicantWorkFunction">
                  	 	 <c:choose>
                  		  <c:when test="${aggreementDetails[0].secondApplicantWorkFunction eq 'Default'}">
                    		<option selected="selected" value="Default">-Select Work Function/ Role-</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	     <option value="Default">-Select Work Function/ Role-</option>
                    	  </c:otherwise>
                    	 </c:choose>
                  	 <c:choose>
                  		  <c:when test="${aggreementDetails[0].secondApplicantWorkFunction eq 'Software'}">
                    		<option selected="selected" value="Software">Software</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	     <option value="Software">Software</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 <c:choose>
                    	  <c:when test="${aggreementDetails[0].secondApplicantWorkFunction eq 'Sales and Marketing'}">
                    		<option selected="selected" value="Sales and Marketing">Sales and Marketing</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="Sales and Marketing">Sales and Marketing</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 <c:choose>
                    	  <c:when test="${aggreementDetails[0].secondApplicantWorkFunction eq 'HR/ Administration'}">
                    		<option selected="selected" value="HR/ Administration">HR/ Administration</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="HR/ Administration">HR/ Administration</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 	 <c:choose>
                    	  <c:when test="${aggreementDetails[0].secondApplicantWorkFunction eq 'Finance'}">
                    		<option selected="selected" value="Finance">Finance</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="Finance">Finance</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 <c:choose>
                    	  <c:when test="${aggreementDetails[0].secondApplicantWorkFunction eq 'Production'}">
                    		<option selected="selected" value="Production">Production</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="Production">Production</option>
                    	  </c:otherwise>
                    	 </c:choose>
                  		 <c:choose>
                    	  <c:when test="${aggreementDetails[0].secondApplicantWorkFunction eq 'Legal'}">
                    		<option selected="selected" value="Legal">Legal</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="Legal">Legal</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	  <c:choose>
                    	  <c:when test="${aggreementDetails[0].secondApplicantWorkFunction eq 'Operations'}">
                    		<option selected="selected" value="Operations">Operations</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="Operations">Operations</option>
                    	  </c:otherwise>
                    	 </c:choose>
                  	 	 <c:choose>
                    	  <c:when test="${aggreementDetails[0].secondApplicantWorkFunction eq 'Other'}">
                    		<option selected="selected" value="Other">Other</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="Other">Other</option>
                    	  </c:otherwise>
                    	 </c:choose>
                  	</select>
                    <span id="secondApplicantWorkFunctionSpan" style="color:#FF0000"></span>
			   </div>
			
			  <div class="col-xs-3">
                  <label>Number of Years of work Experience</label> 
                  	<select class="form-control" name="secondApplicantExperience" id="secondApplicantExperience">
                  	   	 <c:choose>
                  		  <c:when test="${aggreementDetails[0].secondApplicantExperience eq 'Default'}">
                    		<option selected="selected" value="Default">-Select Number of Years of work Experience-</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	     <option value="Default">-Select Number of Years of work Experience-</option>
                    	  </c:otherwise>
                    	 </c:choose>
                  	 <c:choose>
                  		  <c:when test="${aggreementDetails[0].secondApplicantExperience eq '0-5 years'}">
                    		<option selected="selected" value="0-5 years">0-5 years</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	     <option value="0-5 years">0-5 years</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 
                    	 <c:choose>
                    	  <c:when test="${aggreementDetails[0].secondApplicantExperience eq '6-10 years'}">
                    		<option selected="selected" value="6-10 years">6-10 years</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="6-10 years">6-10 years</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 
                    	 <c:choose>
                    	  <c:when test="${aggreementDetails[0].secondApplicantExperience eq '11-15 years'}">
                    		<option selected="selected" value="11-15 years">11-15 years</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="11-15 years">11-15 years</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 	 <c:choose>
                    	  <c:when test="${aggreementDetails[0].secondApplicantExperience eq '16-20 years'}">
                    		<option selected="selected" value="16-20 years">16-20 years</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="16-20 years">16-20 years</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 <c:choose>
                    	  <c:when test="${aggreementDetails[0].secondApplicantExperience eq '21-25 years'}">
                    		<option selected="selected" value="21-25 years">21-25 years</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="21-25 years">21-25 years</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    
                    	 <c:choose>
                    	  <c:when test="${aggreementDetails[0].secondApplicantExperience eq '26-30 years'}">
                    		<option selected="selected" value="26-30 years">26-30 years</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="26-30 years">26-30 years</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	  <c:choose>
                    	  <c:when test="${aggreementDetails[0].secondApplicantExperience eq '30 years'}">
                    		<option selected="selected" value="30 years">30 years</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value=">30 years">>30 years</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 		
                  	</select>
                    <span id="secondApplicantExperienceSpan" style="color:#FF0000"></span>
			   </div>
			      	 
			        
			  <div class="col-xs-3">
                  <label>Annual Household Income(Rupees)</label> 
                  	<select class="form-control" name="secondApplicantIncome" id="secondApplicantIncome">
                  		<c:choose>
                  		  <c:when test="${aggreementDetails[0].secondApplicantIncome eq 'Default'}">
                    		<option selected="selected" value="Default">-Select Annual Household Income-</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	     <option value="Default">-Select Annual Household Income-</option>
                    	  </c:otherwise>
                    	 </c:choose>
                  		 <c:choose>
                  		  <c:when test="${aggreementDetails[0].secondApplicantIncome eq '0-5 lakhs'}">
                    		<option selected="selected" value="0-5 lakhs">0-5 lakhs</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	     <option value="0-5 lakhs">0-5 lakhs</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 
                    	 <c:choose>
                    	  <c:when test="${aggreementDetails[0].secondApplicantIncome eq '6-10 lakhs'}">
                    		<option selected="selected" value="6-10 lakhs">6-10 lakhs</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="6-10 lakhs">6-10 lakhs</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 
                    	 <c:choose>
                    	  <c:when test="${aggreementDetails[0].secondApplicantIncome eq '11-15 lakhs'}">
                    		<option selected="selected" value="11-15 lakhs">11-15 lakhs</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="11-15 lakhs">11-15 lakhs</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 	 <c:choose>
                    	  <c:when test="${aggreementDetails[0].secondApplicantIncome eq '16-20 lakhs'}">
                    		<option selected="selected" value="16-20 lakhs">16-20 lakhs</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="16-20 lakhs">16-20 lakhs</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 <c:choose>
                    	  <c:when test="${aggreementDetails[0].secondApplicantIncome eq '21-25 lakhs'}">
                    		<option selected="selected" value="21-25 lakhs">21-25 lakhs</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="21-25 lakhs">21-25 lakhs</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 <c:choose>
                    	  <c:when test="${aggreementDetails[0].secondApplicantIncome eq '26-30 lakhs'}">
                    		<option selected="selected" value="26-30 lakhs">26-30 lakhs</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="26-30 lakhs">26-30 lakhs</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 <c:choose>
                    	  <c:when test="${aggreementDetails[0].secondApplicantIncome eq '30-35 lakhs'}">
                    		<option selected="selected" value="30-35 lakhs">30-35 lakhs</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="30-35 lakhs">30-35 lakhs</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 <c:choose>
                    	  <c:when test="${aggreementDetails[0].secondApplicantIncome eq '36-40 lakhs'}">
                    		<option selected="selected" value="36-40 lakhs">36-40 lakhs</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="36-40 lakhs">36-40 lakhs</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 <c:choose>
                    	  <c:when test="${aggreementDetails[0].secondApplicantIncome eq '41-45 lakhs'}">
                    		<option selected="selected" value="41-45 lakhs">41-45 lakhs</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="41-45 lakhs">41-45 lakhs</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 <c:choose>
                    	  <c:when test="${aggreementDetails[0].secondApplicantIncome eq '56-50 lakhs'}">
                    		<option selected="selected" value="56-50 lakhs">56-50 lakhs</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="56-50 lakhs">56-50 lakhs</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 <c:choose>
                    	  <c:when test="${aggreementDetails[0].secondApplicantIncome eq '>50 lakhs'}">
                    		<option selected="selected" value=">50 lakhs">>50 lakhs</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value=">50 lakhs">>50 lakhs</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    </select>
                    <span id="secondApplicantIncomeSpan" style="color:#FF0000"></span>
			   </div>
			      	 
			     	    
              </div>
            </div>
			 --%>
									
					    
          </div>
     </div>
     </div>
 
         	  <input type="hidden" id="aggreementstatus" name="aggreementstatus" value="Aggreement">
			  <input type="hidden" id="creationDate" name="creationDate" value="">
			  <input type="hidden" id="updateDate" name="updateDate" value="">
			  <input type="hidden" id="userName" name="userName" value=""> 
			  
     <div class="box-body">
              <div class="row">
              </br>
               <div class="col-xs-4">
               </div>
                <div class="col-xs-4">
                
                  <div class="col-xs-4">
                  <div class="col-xs-4">
                	<a href="AllAgreementList"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
			     </div>
			     </div>
			     </div>
				  <!-- <div class="col-xs-4">
                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
              
			     </div>
					<div class="col-xs-2">
			  <button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
              
			     </div>  -->
			     
              </div>
			</div>
     </div>
         
    </section>
	</form>
    <!-- /.content -->
  </div>
 

  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->

<script>

function init()
{
	
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	 document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
}


</script>
</body>
</html>

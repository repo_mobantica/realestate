<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate |Architectural Section</title>
  <!-- Tell the browser to be responsive to screen width -->
   <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- Google Font -->
  <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
 
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

   <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Architectural Section:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Architectural</a></li>
        <li class="active">Architectural Section</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="architecturalform" action="${pageContext.request.contextPath}/AllProjectArchitecturalSection" method="post">
    <section class="content">

	
		<div class="box box-default">
         <div  class="panel box box-danger"></div>
         
		 <div class="box-body">
                <div class="row">
                 </br>
				  <div class="col-xs-6">
			  	
			      </div>
			     
				
             	  <div class="col-xs-2">
			      		<a  href="${pageContext.request.contextPath}/ArchitecturalSectionForm?projectId=${projectId}"> <button type="button" class="btn btn-success"><i class="fa fa-plus"></i> Add New Project Architectural Section</button></a>
           		  </div>
			       
			  </div>
		</div>
	
	</div>
	
	<div class="box box-default">	
        <div class="box-body">
           
          <h3>Project Name : ${projectName}</h3>
              <div class="table-responsive">
                <table class="table table-bordered" id="projectListTable">
                  <thead>
	                  <tr bgcolor=#4682B4>
	                    <th style="width:60px">Sr.No</th>
	                    <th>Project Name</th>
	                    <th>Architectural Firm Name</th>
	                    <th>Create Date</th>
	                    <th style="width:150px">Action</th>
	                  </tr>
                  </thead>
                  
                  <tbody>
                   <s:forEach items="${projectWiseList}" var="projectWiseList" varStatus="loopStatus">
                    <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                    		<td>${loopStatus.index+1}</td>
	                        <td>${projectWiseList.projectId}</td>
	                        <td>${projectWiseList.firmName}</td> 
	                        <td>${projectWiseList.creationDate}</td>
	                     <td><a href="${pageContext.request.contextPath}/EditArchitecturalSectionForm?projectArchitecturalId=${projectWiseList.projectArchitecturalId}" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>
                      </tr>
					</s:forEach>
                 
                 </tbody>
                </table>
              </div>
            </div>
				
      </div>
     
    </section>
	</form>
    
  </div>
 
   <%@ include file="footer.jsp" %>

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
       
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script>



//function to get country wise state list------------------------------------
function getStateList()
{
	 $("#stateId").empty();
	 var countryId = $('#countryId').val();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getStateList',
		type : 'Post',
		data : { countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select State-");
							$("#stateId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].stateId).text(result[i].stateName);
							    $("#stateId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
	// to retrive all value in table..........
	// for clear table
	$("#projectListTable tr").detach();
	
	
	 var countryId = $('#countryId').val();
	 
	 $("#stateId").empty(); // for clear the State
	 $("#cityId").empty(); // for clear city
		var option = $('<option/>');// to add select option in city
		option.attr('value',"Default").text("-Select City-");
		
		$("#cityId").append(option);
		 $("#locationareaId").empty();
		 var option = $('<option/>');
		 option.attr('value',"Default").text("-Select Location Area-");
		 $("#locationareaId").append(option);
		
	$.ajax({
		//alert("hjhjh");

		url : '${pageContext.request.contextPath}/getCountryCompanyList',
		type : 'Post',
		data : { countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
									$('#projectListTable').append('<tr style="background-color: #4682B4;"><th style="width:300px">Company Name</th><th style="width:150px">Company Type</th><th style="width:200px">City Name</th><th style="width:200px">State Name</th><th style="width:200px">Phone No.</th><th style="width:150px">Pan No.</th> <th style="width:150px">GST No.</th> <th style="width:150px">Financial Year</th><td>Action</td>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].companyId;
									$('#projectListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].companyName+'</td><td>'+result[i].companyType+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].companyPhoneno+'</td><td>'+result[i].companyPanno+'</td><td>'+result[i].companyGstno+'</td><td>'+result[i].companyFinancialyear+'</td><td><a href="${pageContext.request.contextPath}/EditCompany?companyId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
								else
								{
									$('#projectListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].companyName+'</td><td>'+result[i].companyType+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].companyPhoneno+'</td><td>'+result[i].companyPanno+'</td><td>'+result[i].companyGstno+'</td><td>'+result[i].companyFinancialyear+'</td><td><a href="${pageContext.request.contextPath}/EditCompany?companyId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
}//end of get State List



function getCityList()
{
	 $("#cityId").empty();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();
	$.ajax({

		url : '${pageContext.request.contextPath}/getCityList',
		type : 'Post',
		data : { stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select City-");
							$("#cityId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].cityId).text(result[i].cityName);
							    $("#cityId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
	
	// to retrive all value in table..........
	
	$("#projectListTable tr").detach();
	 var stateId = $('#stateId').val();
	 $("#locationareaId").empty();
	 var option = $('<option/>');
	 option.attr('value',"Default").text("-Select Location Area-");
	 $("#locationareaId").append(option);
	$.ajax({
		//alert("hjhjh");

		url : '${pageContext.request.contextPath}/getCityWiseCompanyList',
		type : 'Post',
		data : { stateId : stateId},
		dataType : 'json',
		success : function(result)
				  {

						if (result) 
						{ 
									$('#projectListTable').append('<tr style="background-color: #4682B4;"><th style="width:300px">Company Name</th><th style="width:150px">Company Type</th><th style="width:200px">City Name</th><th style="width:200px">State Name</th><th style="width:200px">Phone No.</th><th style="width:150px">Pan No.</th> <th style="width:150px">GST No.</th> <th style="width:150px">Bank A/C No.</th> <th style="width:150px">Bank Name</th><th style="width:150px">Financial Year</th><td>Action</td>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].companyId;
									$('#projectListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].companyName+'</td><td>'+result[i].companyType+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].companyPhoneno+'</td><td>'+result[i].companyPanno+'</td><td>'+result[i].companyGstno+'</td><td>'+result[i].companyBankacno+'</td><td>'+result[i].companyBankname+'</td><td>'+result[i].companyFinancialyear+'</td><td><a href="${pageContext.request.contextPath}/EditCompany?companyId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
								else
								{
									$('#projectListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].companyName+'</td><td>'+result[i].companyType+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].companyPhoneno+'</td><td>'+result[i].companyPanno+'</td><td>'+result[i].companyGstno+'</td><td>'+result[i].companyBankacno+'</td><td>'+result[i].companyBankname+'</td><td>'+result[i].companyFinancialyear+'</td><td><a href="${pageContext.request.contextPath}/EditCompany?companyId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}

				}
		});
		
}//end of get City List


function getLocationAreaList()
{
	 //to retrive all banks by city name
	 $("#projectListTable tr").detach();
	
	 $("#locationareaId").empty();
	 
	 var cityId = $('#cityId').val();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();
	
	 $.ajax({

		url : '${pageContext.request.contextPath}/getLocationAreaList',
		type : 'Post',
		data : { countryId : countryId, stateId : stateId, cityId : cityId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select City-");
							$("#locationareaId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].locationareaId).text(result[i].locationareaName);
							    $("#locationareaId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	 
	 
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getLocationAreaWiseCompanyList',
		type : 'Post',
		data : { cityId : cityId},
		dataType : 'json',
		success : function(result)
			      {

						if (result) 
						{ 
									$('#projectListTable').append('<tr style="background-color: #4682B4;"><th style="width:300px">Company Name</th><th style="width:150px">Company Type</th><th style="width:200px">City Name</th><th style="width:200px">State Name</th><th style="width:200px">Phone No.</th><th style="width:150px">Pan No.</th> <th style="width:150px">GST No.</th> <th style="width:150px">Bank A/C No.</th> <th style="width:150px">Bank Name</th><th style="width:150px">Financial Year</th><td>Action</td>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].companyId;
									$('#projectListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].companyName+'</td><td>'+result[i].companyType+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].companyPhoneno+'</td><td>'+result[i].companyPanno+'</td><td>'+result[i].companyGstno+'</td><td>'+result[i].companyBankacno+'</td><td>'+result[i].companyBankname+'</td><td>'+result[i].companyFinancialyear+'</td><td><a href="${pageContext.request.contextPath}/EditCompany?companyId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
								else
								{
									$('#projectListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].companyName+'</td><td>'+result[i].companyType+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].companyPhoneno+'</td><td>'+result[i].companyPanno+'</td><td>'+result[i].companyGstno+'</td><td>'+result[i].companyBankacno+'</td><td>'+result[i].companyBankname+'</td><td>'+result[i].companyFinancialyear+'</td><td><a href="${pageContext.request.contextPath}/EditCompany?companyId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}

				}
		});
	
}//end of get locationarea List



function getCompanyList()
{
	
	 //to retrive all banks by Location Area name
	$("#projectListTable tr").detach();
	
	 var locationareaId = $('#locationareaId').val();

	 $.ajax({

		 url : '${pageContext.request.contextPath}/getCompanyList',
		type : 'Post',
		data : { locationareaId : locationareaId},
		dataType : 'json',
		success : function(result)
			  {

					if (result) 
					{ 
								$('#projectListTable').append('<tr style="background-color: #4682B4;"><th style="width:300px">Company Name</th><th style="width:150px">Company Type</th><th style="width:200px">City Name</th><th style="width:200px">State Name</th><th style="width:200px">Phone No.</th><th style="width:150px">Pan No.</th> <th style="width:150px">GST No.</th> <th style="width:150px">Bank A/C No.</th> <th style="width:150px">Bank Name</th><th style="width:150px">Financial Year</th><td>Action</td>');
					
						for(var i=0;i<result.length;i++)
						{ 
							if(i%2==0)
							{
								var id = result[i].companyId;
								$('#projectListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].companyName+'</td><td>'+result[i].companyType+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].companyPhoneno+'</td><td>'+result[i].companyPanno+'</td><td>'+result[i].companyGstno+'</td><td>'+result[i].companyBankacno+'</td><td>'+result[i].companyBankname+'</td><td>'+result[i].companyFinancialyear+'</td><td><a href="${pageContext.request.contextPath}/EditCompany?companyId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
							}
							else
							{
								$('#projectListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].companyName+'</td><td>'+result[i].companyType+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].companyPhoneno+'</td><td>'+result[i].companyPanno+'</td><td>'+result[i].companyGstno+'</td><td>'+result[i].companyBankacno+'</td><td>'+result[i].companyBankname+'</td><td>'+result[i].companyFinancialyear+'</td><td><a href="${pageContext.request.contextPath}/EditCompany?companyId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
							}
						
						 } 
					} 
					else
					{
						alert("failure111");
					}

				}
		});
	
}//end of get locationarea List

/* function CompanyNameWiseSearch()
{
	$("#projectListTable tr").detach();
	
	 var companyName = $('#companyName').val();

	 $.ajax({

		 url : '${pageContext.request.contextPath}/searchCompanyNameWiseList',
		type : 'Post',
		data : { companyName : companyName},
		dataType : 'json',
		success : function(result)
			  {

					if (result) 
					{ 
								$('#projectListTable').append('<tr style="background-color: #4682B4;"><th style="width:300px">Company Name</th><th style="width:150px">Company Type</th><th style="width:200px">City Name</th><th style="width:200px">State Name</th><th style="width:200px">Phone No.</th><th style="width:150px">Pan No.</th> <th style="width:150px">GST No.</th> <th style="width:150px">Bank A/C No.</th> <th style="width:150px">Bank Name</th><th style="width:150px">Financial Year</th><td>Action</td>');
					
						for(var i=0;i<result.length;i++)
						{ 
							if(i%2==0)
							{
								var id = result[i].companyId;
								$('#projectListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].companyName+'</td><td>'+result[i].companyType+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].companyPhoneno+'</td><td>'+result[i].companyPanno+'</td><td>'+result[i].companyGstno+'</td><td>'+result[i].companyBankacno+'</td><td>'+result[i].companyBankname+'</td><td>'+result[i].companyFinancialyear+'</td><td><a href="${pageContext.request.contextPath}/EditCompany?companyId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
							}
							else
							{
								$('#projectListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].companyName+'</td><td>'+result[i].companyType+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].companyPhoneno+'</td><td>'+result[i].companyPanno+'</td><td>'+result[i].companyGstno+'</td><td>'+result[i].companyBankacno+'</td><td>'+result[i].companyBankname+'</td><td>'+result[i].companyFinancialyear+'</td><td><a href="${pageContext.request.contextPath}/EditCompany?companyId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
							}
						
						 } 
					} 
					else
					{
						alert("failure111");
					}

				}
		});
} */



$(function () {
    $('#projectListTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>

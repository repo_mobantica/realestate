<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Print Employee Salary Slip</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
 
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

   <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Print Employee Salary Slip:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="EmployeeSalaryDetails">Employee Salary Details</a></li>
        <li class="active">Print Employee Salary Slip</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="employeeSalaryform" action="${pageContext.request.contextPath}/PrintEmployeeSalarySlip"  target="_blank" onSubmit="return validate()" method="post">
    <section class="content">

   <div class="box box-default">
      <div  class="panel box box-danger"></div>
         
        <div class="box-body">
          <div class="row">
            
	                 <div class="col-xs-2">
					   <label for="month">Salary Month</label> <label class="text-red">* </label>
	                    <select class="form-control" id="month" name="month" onchange="getEmployeeMonthLeave(this.value)">
				    	    <option selected="selected" value="Default">-Select Salary Month-</option>
					  		<option value="1">January</option>
	                    	<option value="2">February</option>
	                    	<option value="3">March</option>
	                    	<option value="4">April</option>
	                    	<option value="5">May</option>
	                    	<option value="6">June</option>
	                    	<option value="7">July</option>
	                    	<option value="8">August</option>
	                    	<option value="9">September</option>
	                    	<option value="10">October</option>
	                    	<option value="11">November</option>
	                    	<option value="12">December</option>
	                  	</select>
	                  	
	                   <span id="monthSpan" style="color:#FF0000"></span>
	                 </div>
	                 
	                 <div class="col-xs-2">
					   <label for="year">Salary Year</label> <label class="text-red">* </label>
					   
	                    <select class="form-control" id="year" name="year">
				    	    <option selected="selected" value="Default">-Select Salary Year-</option>
					  		<option value="2011">2011</option>
					  		<option value="2012">2012</option>
					  		<option value="2013">2013</option>
					  		<option value="2014">2014</option>
					  		<option value="2015">2015</option>
					  		<option value="2016">2016</option>
					  		<option value="2017">2017</option>
	                    	<option value="2018">2018</option>
	                    	<option value="2019">2019</option>
	                    	<option value="2020">2020</option>
	                    	
	                    	<option value="2021">2021</option>
	                    	<option value="2022">2022</option>
	                    	<option value="2023">2023</option>
	                    	<option value="2024">2024</option>
	                    	<option value="2025">2025</option>
	                    	<option value="2026">2026</option>
	                    	<option value="2027">2027</option>
	                    	<option value="2028">2028</option>
	                    	<option value="2029">2029</option>
	                    	<option value="2030">2030</option>
	                    	
	                    	<option value="2031">2031</option>
	                    	<option value="2032">2032</option>
	                    	<option value="2033">2033</option>
	                    	<option value="2034">2034</option>
	                    	<option value="2035">2035</option>
	                    	<option value="2036">2036</option>
	                    	<option value="2037">2037</option>
	                    	<option value="2038">2038</option>
	                    	<option value="2039">2039</option>
	                    	<option value="2040">2040</option>
	                    	
	                    	<option value="2041">2041</option>
	                    	<option value="2042">2042</option>
	                    	<option value="2043">2043</option>
	                    	<option value="2044">2044</option>
	                    	<option value="2045">2045</option>
	                    	<option value="2046">2046</option>
	                    	<option value="2047">2047</option>
	                    	<option value="2048">2048</option>
	                    	<option value="2049">2049</option>
	                    	<option value="2050">2050</option>
	                    	
	                  	</select>
	                   <span id="yearSpan" style="color:#FF0000"></span>
	                 </div>
	                 
	                 <div class="col-xs-3">
					   <label for="employeeId">Salary Year</label> <label class="text-red">* </label>
	                    <select class="form-control" id="employeeId" name="employeeId">
				    	    <option selected="selected" value="Default">-Select Employee Name-</option>
                      		<c:forEach var="employeeList" items="${employeeList}">
	                    		<option value="${employeeList.employeeId}">${employeeList.employeefirstName} ${employeeList.employeelastName}</option>
	                  		</c:forEach>
	                  	</select>
	                   <span id="employeeIdSpan" style="color:#FF0000"></span>
	                 </div>
	                 
				<div class="col-xs-3">
				<br/>
		  			<button type="submit" class="btn btn-info" name="submit">Print</button>
			    </div>
	                 
           </div>
         </div>   
			 
		 
		    <div class="box-body">
              <div class="row">
		    <br/>
	            <div class="col-xs-1">
	            
	            </div>
	            
	            <div class="col-xs-4">
	            <div class="col-xs-2">
                	<a href="EmployeeSalaryDetails"><button type="button" class="btn btn-block btn-primary" value="reset" style="width:90px">Back</button></a>
			    </div>
			    </div>
			    
	   		    <div class="col-xs-2">
	                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
			    </div>
			    
			    
			  </div>
			</div>
              	 
			 
      </div>
     
    </section>
	</form>
    
  </div>
 
  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
       
  <script src="https://code.jquery.com/jquery-3.0.0.js"></script>
  <script src="https://code.jquery.com/jquery-migrate-3.0.1.js"></script>
  
<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script>

function clearall()
{

	$('#monthSpan').html('');
	$('#yearSpan').html('');
	$('#employeeIdSpan').html('');
}

function validate()
{
	clearall();

	if(document.employeeSalaryform.month.value=="Default")
	{
		$('#monthSpan').html('Please, select Salary Month..!');
		document.employeeSalaryform.month.focus();
		return false;
	}
	
    if(document.employeeSalaryform.year.value=="Default")
	{
		$('#yearSpan').html('Please, select Salary Year..!');
		document.employeeSalaryform.year.focus();
		return false;
	}
    
    if(document.employeeSalaryform.employeeId.value=="Default")
	{
		$('#employeeIdSpan').html('Please, select Employee Name..!');
		document.employeeSalaryform.employeeId.focus();
		return false;
	}
	
}

function init()
{
	document.employeeSalaryform.month.focus();	
}

</script>
</body>
</html>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
 
  <script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1500).fadeOut();
      });
  </script>

 <script language="JavaScript" type="text/javascript">

javascript:window.history.forward(1);

</script>      

<style>
body {
	background-image: url("${pageContext.request.contextPath}/resources/dist/img/bg.png");
	height: 94%;
	width: 100%;
	background-size: cover;
    background-position: center;
	background-repeat: no-repeat;
}
.login-box, .register-box {
    margin: 0% auto;
    position: relative;
    top: 18%;
}
</style>
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body  onload="init()">



<div class="login-box">
  <div class="login-logo">
    
  </div>
  <!-- /.login-logo -->
  
  <div class="login-box-body">
  
   <img src="${pageContext.request.contextPath}/resources/dist/img/loginlogo.png" height="139" width="320">
    <br/><br/>
    <form name="loginform" action="${pageContext.request.contextPath}/login" onsubmit="return validate()" method="post">
    
      <span id="statusSpan" style="color:#FF0000"></span>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="userName" id="userName" placeholder="Username">
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        <span id="userNameSpan" style="color:#FF0000"></span>
      </div>
      
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="passWord" id="passWord" placeholder="Password">
         <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        <span id="passWordSpan" style="color:#FF0000"></span>
      </div>
      
      <div class="box-body">
	      <div class="row">
	        <div class="col-xs-8">
	          <div class="checkbox icheck">
	         <!--    <label>
	              <input type="checkbox"> Remember Me
	            </label> -->
	          </div>
	        </div>

	        <div class="col-xs-4">
	          <input type="submit" name="submit" id="submit" class="btn btn-primary" value="Sign In">
	        </div>
	      </div>
      </div>
     		<input type="hidden" id="loginStatus" name="1111111loginStatus" value="${loginStatus}"/>
     		
    </form>
    
  </br>
  <!-- 
    <a href="#">Forgot Password?</a><br>
 -->
  </div>
  
  <!-- /.login-box-body -->
  
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>

<script>

function clearAll()
{
	$('#userNameSpan').html('');
	$('#passWordSpan').html('');
}

function init()
{
	if(document.getElementById("loginStatus").value == "Fail")
	{
		$('#statusSpan').html('Invalid username or password..!');
	}
	
	document.loginform.userName.focus();
}

function validate()
{
	clearAll();
	
    if(document.loginform.userName.value == "")
    {
    	$('#userNameSpan').html('Username cannot be empty..!');
    	document.loginform.userName.value="";
    	document.loginform.userName.focus();
    	return false;
    }
    else if(document.loginform.passWord.value == "")
    {
    	$('#passWordSpan').html('Password cannot be empty..!');
    	document.loginform.passWord.value="";
    	document.loginform.passWord.focus();
    	return false;
    }
}

  $(function () 
  {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
  
</script>
</body>
</html>

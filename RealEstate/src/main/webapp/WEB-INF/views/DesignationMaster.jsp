<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate |Designation Master</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- Google Font -->
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>

<div class="wrapper">

   <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Designation Master Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active"> Designation Master</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="designationmasterform" action="${pageContext.request.contextPath}/DesignationMaster" method="post">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              
            </div>
            
            <!-- /.col -->
			
          </div>
		   	 
			     <div class="box-body">
              <div class="row">
                 </br>
				 <div class="col-xs-3">
					<label>Select Department</label><label class="text-red">*</label>
                    <select class="form-control" id="departmentId" name="departmentId" onchange="return designationSearch(this.value)">
				      <option selected="selected" value="Default">-Select Department-</option>
                      <c:forEach var="departmentList" items="${departmentList}">
	                    <option value="${departmentList.departmentId}">${departmentList.departmentName}</option>
	                  </c:forEach>
                     </select>
                     <span id="departmentIdSpan" style="color:#FF0000"></span>
			     </div>
			     
				 <div class="col-xs-3">
			 		<br/>&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
			   		<a href="DesignationMaster">  <button type="button" class="btn btn-default" value="reset" style="width:90px"> Reset</button></a>
               	 </div>
               	  
              	 <div class="col-xs-2">
              	 	<br/>
			      	<a href="AddDesignation"> <button type="button" class="btn btn-success"><i class="fa fa-plus"></i> Add New Designation</button></a>
            	 </div>
			       
			  </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
              
         </div>
      <!-- /.box -->
	
			<div class="box box-default">
        
        <!-- /.box-header -->
          
              <!-- /.form-group -->
          
			 <div class="box-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="designationListTable">
	                  <thead>
		                  <tr bgcolor="#4682B4">
		                    <th style="width:300px">Department Name</th>
		                    <th style="width:300px">Designation Name</th>
		                    <th style="width:150px">Creation Date</th>
		                    <th style="width:150px">Last update Date</th>
		                    <th style="width:150px">User Name</th>
		                    <th style="width:50px">Action</th>
		                  </tr>
	                  </thead>
                 
                  <tbody>
                     <c:forEach items="${designationList}" var="designationList" varStatus="loopStatus">
                   
	                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
	                        <td>${designationList.departmentId}</td>
 	                        <td>${designationList.designationName}</td>
	                        <td>${designationList.creationDate}</td>
	                        <td>${designationList.updateDate}</td>
	                        <td>${designationList.userName}</td>
	                        <td><a href="${pageContext.request.contextPath}/EditDesignation?designationId=${designationList.designationId}" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>
	                      </tr>
	                      
					  </c:forEach>
                 </tbody>
                 
                </table>
              </div>
            </div>
              <!-- /.form-group -->
          <!-- /.box-body -->
        </div>	
     
       </section>
	</form>
    <!-- /.content -->
    
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script>

function designationSearch()
{
	
   $("#designationListTable tr").detach();
   var departmentId = $('#departmentId').val();
   
   if(document.designationmasterform.departmentId.value=="Default")
   {
	  $('#departmentIdSpan').value('Please, select department first');
	  document.designationmasterform.departmentId.focus();
	  return false;
   }
   
	$.ajax({

		url : '${pageContext.request.contextPath}/searchDepartmentWiseDesignation',
		type : 'Post',
		data : { departmentId : departmentId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 							
									$('#designationListTable').append('<tr style="background-color: #4682B4;"><th style="width:300px"> Department Name</th><th style="width:300px">Designation Name</th><th style="width:150px">Creation Date</th><th style="width:150px">Last update Date</th><th style="width:150px">User Name</th><th>Action</th>');
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].designationId;
									$('#designationListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].departmentId+'</td><td>'+result[i].designationName+'</td><td>'+result[i].creationDate+'</td><td>'+result[i].updateDate+'</td><td>'+result[i].userName+'</td><td><a href="${pageContext.request.contextPath}/EditDesignation?designationId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
								else
								{
									var id = result[i].designationId;
									$('#designationListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].departmentId+'</td><td>'+result[i].designationName+'</td><td>'+result[i].creationDate+'</td><td>'+result[i].updateDate+'</td><td>'+result[i].userName+'</td><td><a href="${pageContext.request.contextPath}/EditDesignation?designationId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
									
								}
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
}//end of get State List


$(function () {
    $('#designationListTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate |All Enquiry View</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">
  
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
 
  <!-- Google Font -->
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>

<div class="wrapper">

   <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        All Enquiry View:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Pre-sale</a></li>
        <li class="active">All Enquiry View</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="allenquiryviewform" action="${pageContext.request.contextPath}/AllEnquiryViews" onSubmit="return validate()" method="post">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              
       		
			  <div class="box-body">
          <div class="row">
     
                <div class="col-md-3">
                 <label >Select by Date</label>
                   <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker" name="datepicker" >
                 
              <span class="input-group-btn">
                <button type="button" name="search" id="search-btn" class="btn btn-flat" ><i class="fa fa-search" onclick="dateSearch()"></i></button>
              </span>
           
                </div>
                </div>
				</div>
				</div>
				
				
				
              <!-- /.form-group -->
            </div>
            
            <!-- /.col -->
			
          </div>
		   	 
			  <div class="box-body">
              <div class="row">
                <div class="col-xs-3">
				  <!--
				  <div class="input-group">
              	<input type="text" id="stateName" name="stateName" placeholder="State Name" class="form-control ui-autocomplete-input" autocomplete="off" oninput="stateSearch(this.value)">
                   
                 
              <span class="input-group-btn">
                <button type="button" name="search" id="search-btn" class="btn btn-flat" ><i class="fa fa-search"></i></button>
              </span>
              
            </div>
            -->
            </div>
            	  <div class="col-xs-3">
            	  
                	<a href="EnquiryMaster"><button type="button" class="btn btn-block btn-primary" value="reset" style="width:90px">Back</button></a>
			    </div>
			    
					<div class="col-xs-3">
			
			 <a href="AllEnquiryViews">  <button type="button" class="btn btn-default" value="reset" style="width:90px"> Reset</button></a>
              </div> 
              <div class="col-xs-2">
			      <a href="AddEnquiry"> <button type="button" class="btn btn-success"><i class="fa fa-plus"></i> Add New Enquiry</button></a>
            </div>
		  </div>
        </div>
    </div>
     
     <div class="box box-default">
		<div  class="panel box box-danger"></div>
			<h3><label> &nbsp &nbsp All Enquiry List</label></h3>
        	 <div class="box-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="allenquiryviewListTable">
                  <thead>
                  <tr bgcolor="#4682B4">
                  <th>Enquiry Id</th>
                  <th>Name</th>
				  <th>Mobile No</th>
                  <th>Email Id</th>
				  <th>Flat Type</th>
				  <th>Budget</th>
				  <th>Status</th>
                  </tr>
                  </thead>
                  <tbody >
                   <c:forEach items="${enquiryList}" var="enquiryList" varStatus="loopStatus">
                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                             <td>${enquiryList.enquiryId}</td>
	                        <td>${enquiryList.enqfirstName} ${enquiryList.enqmiddleName} ${enquiryList.enqlastName}</td>
	                        <td>${enquiryList.enqmobileNumber1}</td>
	                        <td>${enquiryList.enqEmail}</td>
	                     	<td>${enquiryList.flatType}</td>
	                        <td>${enquiryList.flatBudget}</td>
	                        <td>${enquiryList.enqStatus}</td>
                      </tr>
				   </c:forEach>
                 </tbody>
                </table>
              </div>
            </div>
	 </div>
   </section>
</form>
 </div>
  <!-- /.content-wrapper -->
 <%@ include file="footer.jsp" %>
  <!-- Control Sidebar -->
  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>

<!-- Bootstrap 3.3.7 -->
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- Page script -->
<script>

function getEnquiryList()
{
	 //$("#stateListTable").empty();
$("#allenquiryviewListTable tr").detach();
	 var projectName = $('#projectName').val();
	//alert(projectName);
	$.ajax({

		url : '${pageContext.request.contextPath}/getallenquirybyprojectList',
		type : 'Post',
		data : { projectName : projectName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
							
							$('#allenquiryviewListTable').append('<tr style="background-color: #4682B4;"><td>Enquiry Id</td><td>Name</td><td>Mobile No</td><td>Email Id</td> <td>Flat Type</td> <td>Budget</td><td>Status</td>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
									{
									$('#allenquiryviewListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].enquiryId+'</td><td>'+result[i].enqfirstName+" "+result[i].enqmiddleName+" "+result[i].enqlastName+'</td><td>'+result[i].enqmobileNumber1+'</td><td>'+result[i].enqEmail+'</td><td>'+result[i].flatType+'</td><td>'+result[i].flatBudget+'</td><td>'+result[i].enqStatus+'</td>');
									}
								else{
									$('#allenquiryviewListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].enquiryId+'</td><td>'+result[i].enqfirstName+" "+result[i].enqmiddleName+" "+result[i].enqlastName+'</td><td>'+result[i].enqmobileNumber1+'</td><td>'+result[i].enqEmail+'</td><td>'+result[i].flatType+'</td><td>'+result[i].flatBudget+'</td><td>'+result[i].enqStatus+'</td>');
									
								}
							
							 } 
						
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get State List

function dateSearch()
{
	
	$("#allenquiryviewListTable tr").detach();
	 var datepicker = $('#datepicker').val();
	 
	$.ajax({

		url : '${pageContext.request.contextPath}/getallenquirybyselectedDateList',
		type : 'Post',
		data : { datepicker : datepicker},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
							
							$('#allenquiryviewListTable').append('<tr style="background-color: #4682B4;"><td>Enquiry Id</td><td>Name</td><td>Mobile No</td><td>Email Id</td> <td>Flat Type</td> <td>Budget</td><td>Status</td>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
									{
									$('#allenquiryviewListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].enquiryId+'</td><td>'+result[i].enqfirstName+'</td><td>'+result[i].enqmobileNumber1+'</td><td>'+result[i].enqEmail+'</td><td>'+result[i].flatType+'</td><td>'+result[i].flatBudget+'</td><td>'+result[i].enqStatus+'</td>');
									}
								else{
									$('#allenquiryviewListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].enquiryId+'</td><td>'+result[i].enqfirstName+'</td><td>'+result[i].enqmobileNumber1+'</td><td>'+result[i].enqEmail+'</td><td>'+result[i].flatType+'</td><td>'+result[i].flatBudget+'</td><td>'+result[i].enqStatus+'</td>');
									
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}




  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
  
  
   $(function () {
    $('#allenquiryviewListTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate |Add Flat</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

  <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Flat Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Add Flat</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="flatform" action="${pageContext.request.contextPath}/AddFlat" onSubmit="return validate()" method="post">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
                 <span id="statusSpan" style="color:#FF0000"></span>
              <!-- /.form-group -->
             
			
				<div class="box-body">
              <div class="row">
                  <div class="col-xs-3">
                  <label >Flat Id</label>
                  <input type="text" class="form-control" id="flatId" name="flatId"  value="${flatCode}" readonly>
                   <span id="statusSpan" style="color:#FF0000"></span>
                </div>               
                <div class="col-xs-3">
                  <label for="flatNumber">Flat No</label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="flatNumber" placeholder="Flat Number" name="flatNumber"  style="text-transform:uppercase" onchange="getUniqueFlatNumber(this.value)">
                   <span id="flatNumberSpan" style="color:#FF0000"></span>
                </div> 
              </div>
            </div>
				   <div class="box-body">
              <div class="row">
                  <div class="col-xs-3">
			      <label>Project  </label> <label class="text-red">* </label>
                  <select class="form-control" name="projectId" id="projectId" onchange="getBuldingList(this.value)">
				  <option selected="" value="Default">-Select Project-</option>
                     <c:forEach var="projectList" items="${projectList}">
                    	<option value="${projectList.projectId}">${projectList.projectName}</option>
				     </c:forEach>
                  </select>
			        <span id="projectIdSpan" style="color:#FF0000"></span>
			     </div> 
			       <div class="col-xs-3">
			      <label>Project Building Name  </label> <label class="text-red">* </label>
                  <select class="form-control" name="buildingId" id="buildingId" onchange="getwingIdList(this.value)">
				  	<option selected="" value="Default">-Select Project Building-</option>
                  
                  </select>
			       <span id="buildingIdSpan" style="color:#FF0000"></span>
			     </div> 
			
                 <div class="col-xs-3">
			      <label>Wing </label> <label class="text-red">* </label>
              		<select class="form-control" name="wingId" id="wingId" onchange="getFloorNameList(this.value)">
				 	 	<option selected="" value="Default">-Select Wing Name-</option>
                   
                  </select>
                   <span id="wingIdSpan" style="color:#FF0000"></span>
                 </div> 
              
                  <div class="col-xs-3">
			      <label>Floor </label> <label class="text-red">* </label>
                  <select class="form-control" name="floorId" id="floorId"  onchange="getFloorRise(this.value)">
				 	 	<option selected="" value="Default">-Select Floor Name-</option>
                  
                  </select>
                   <span id="floorIdSpan" style="color:#FF0000"></span>  
			     </div> 
				 </div>
            </div>
            
             <div class="box-body">
              <div class="row">
                  <div class="col-xs-3">
			      <label>Flat Facing Type  </label> <label class="text-red">* </label>
                  <select class="form-control" name="flatfacingName">
                  <option selected="" value="Default">-Select Facing Flat Type-</option>
				  <option>East</option>
                  <option>North</option>
                  <option>North-east</option>
                  <option>North-west</option>
                  <option>South </option>
                  <option>South-east</option>
                  <option>South-west</option>
                  <option>West</option>         
                  </select>
                   <span id="flatfacingNameSpan" style="color:#FF0000"></span>
                 </div> 
                 
                  <div class="col-xs-3">
			      <label>Flat Type  </label> <label class="text-red">* </label>
                  <select class="form-control" name="flatType">
				  <option selected="" value="Default">-Select Flat Type-</option>
                    <option>RK</option>
                    <option>1BHK</option>
                    <option>2BHK</option>
                    <option>3BHK</option>
                    <option>4BHK</option>
                  </select>
			        <span id="flatTypeSpan" style="color:#FF0000"></span>
			     </div> 
				 </div>
            </div>
            <div class="box-body">
              <div class="row">
              	    <div class="col-xs-2">
			   <label for="carpetArea">Flat Carpet Area(Sq.M)</label><label class="text-red">* </label>
                <input type="text" class="form-control" id="carpetArea" placeholder="Carpet Area in SQ.FT" name="carpetArea" onchange="getflatAreawithLoading(this.value)">
                   <span id="carpetAreaSpan" style="color:#FF0000"></span>
			     </div> 
			       <div class="col-xs-2">
			    <label for="terraceArea">Open Balcony Area(Sq.M)</label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="terraceArea" placeholder="Open Balcony Area in SQ.FT" name="terraceArea" onchange="getflatAreawithLoading(this.value)">
                   <span id="terraceAreaSpan" style="color:#FF0000"></span>
			     </div> 
                 <div class="col-xs-2">
			    <label for="balconyArea">Enclose Balcony Area(Sq.M)</label><label class="text-red">* </label>
                <input type="text" class="form-control" id="balconyArea" placeholder="Enclose Balcony Area" name="balconyArea" onchange="getflatAreawithLoading(this.value)">
                  <span id="balconyAreaSpan" style="color:#FF0000"></span>
               </div>
			    <div class="col-xs-2">
			   <label for="dryterraceArea">Dry Terrace Area(Sq.M)</label><label class="text-red">* </label>
                <input type="text" class="form-control" id="dryterraceArea" placeholder="Dry Terrace Area in SQ.FT" name="dryterraceArea" onchange="getflatAreawithLoading(this.value)">
                   <span id="dryterraceAreaSpan" style="color:#FF0000"></span>
			     </div> 
			     <div class="col-xs-2">
			    <label for="flatArea">Flat Area(Sq.M)</label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="flatArea" placeholder="Flat Area in SQ.FT" name="flatArea" readonly>
			     </div>
			  </div>
            </div>
            <div class="box-body">
              <div class="row">
                 <div class="col-xs-3">
			    <label for="loading">Loading %</label> 
                  <input type="text" class="form-control" id="loading" name="loading" readonly>
			     </div>
                    <div class="col-xs-3">
			    <label for="loading">Project Loading</label> 
                  <input type="text" class="form-control" id="loadingpercentage" name="loadingpercentage" readonly>
			     </div>  
			       
                 <div class="col-xs-3">
			    <label for="flatAreawithLoadingInM">Net Flat Area(Sq.M)</label> 
                  <input type="text" class="form-control" id="flatAreawithLoadingInM" name="flatAreawithLoadingInM" readonly>
                   <span id="flatAreawithLoadingInMSpan" style="color:#FF0000"></span>
			     </div>
			    <div class="col-xs-3">
			    <label for="flatAreawithLoadingInFt">Flat Area(Sq.Ft)</label> 
                  <input type="text" class="form-control" id="flatAreawithLoadingInFt" placeholder="" name="flatAreawithLoadingInFt" onchange="getflatBasicCost(this.value)">
                   <span id="flatAreawithLoadingInFtSpan" style="color:#FF0000"></span>
			     </div> 
			       	
			</div>
            </div>
                  <div class="box-body">
              		<div class="row">
           			<div class="col-xs-2">
			 	   <label for="flatCostwithotfloorise">Flat Cost Per Sq.Ft</label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="flatCostwithotfloorise" placeholder="Flat Total Cost" name="flatCostwithotfloorise" onchange="getflatBasicCost(this.value)">
                   <span id="flatCostwithotflooriseSpan" style="color:#FF0000"></span>
			     </div> 
           			<div class="col-xs-2">
			 	   <label for="floorRise">Floor Rise</label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="floorRise" placeholder="Flat Total Cost" name="floorRise" readonly>
			     </div> 
			     	<div class="col-xs-2">
			 	   <label for="flatCost">Net Flat Cost Per Sq.Ft</label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="flatCost" placeholder="Flat Total Cost" name="flatCost"  readonly>
			     </div> 
                <div class="col-xs-2">
			    <label for="flatminimumCost">Minimum Flat Cost Per Sq.Ft</label>
                <input type="text" class="form-control" id="flatminimumCost" placeholder="Minimum flat Cost" name="flatminimumCost">
               <span id="flatminimumCostSpan" style="color:#FF0000"></span>
               </div>
                   
                 <div class="col-xs-2">
			    <label for="flatbasicCost">Total Flat Basic Cost</label>
                <input type="text" class="form-control" id="flatbasicCost" placeholder="Basic Cost" name="flatbasicCost" readonly>
               </div>
                </div>
			 <input type="hidden" id="flatstatus" name="flatstatus" value="Booking Remaninig">
			<input type="hidden" id="status" name="status" value="${Status}">	
			<input type="hidden" id="creationDate" name="creationDate" >
			<input type="hidden" id="updateDate" name="updateDate" >
			<input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>">
			</div>
           
     </div>
          
            
			
            <!-- /.col -->
			
          </div>
		   	 <div class="box-body">
              <div class="row">
              <br/><br/>
                 <div class="col-xs-3">
                 <div class="col-xs-2">
                	<a href="FlatMaster"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
			     </div>
			     </div>
				  <div class="col-xs-3">
                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
			     </div>
					<div class="col-xs-2">
			        <button type="submit" class="btn btn-info " name="submit">Submit</button>
			     </div> 
			     
              </div>
			  </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        
      </div>
      <!-- /.box -->
		
    </section>
	</form>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 

  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
 <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
    </script>
<script>
function clearall()
{
	$('#flatNumberSpan').html('');
	$('#projectIdSpan').html('');
	$('#buildingIdSpan').html('');
	$('#wingIdSpan').html('');
	$('#floorIdSpan').html('');
	$('#flatfacingNameSpan').html('');
	$('#flatTypeSpan').html('');
	$('#flatAreaSpan').html('');
	$('#carpetAreaSpan').html('');
	$('#terraceAreaSpan').html('');
	$('#dryterraceAreaSpan').html('');
	$('#balconyAreaSpan').html('');
	$('#flatminimumCostSpan').html('');
	$('#flatCostSpan').html('');
	$('#flatCostwithotflooriseSpan').html('');
}
function validate()
{ 
	clearall();
	//for flat number
	if(document.flatform.flatNumber.value=="")
	{
		$('#flatNumberSpan').html('Please, enter flat number..!');
		document.flatform.flatNumber.focus();
		return false;
	}
	else if(document.flatform.flatNumber.value.match(/^[\s]+$/))
	{
		$('#flatNumberSpan').html('Please, enter flat number..!');
		document.flatform.flatNumber.value="";
		document.flatform.flatNumber.focus();
		return false; 	
	}
	else if(!document.flatform.flatNumber.value.match(/^[a-zA-Z0-9\s]+$/))
	{
		$('#flatNumberSpan').html('Please, use only alphabets and digits for flat number..!');
		document.flatform.flatNumber.value="";
		document.flatform.flatNumber.focus();
		return false;
	}
		//for project name
		if(document.flatform.projectId.value=="Default")
		{
			$('#projectIdSpan').html('Please Select Project');
			document.flatform.projectId.focus();
			return false;
		}
		//for Building name
		
		if(document.flatform.buildingId.value=="Default")
		{
			$('#buildingIdSpan').html('Please Select Project Building');
			document.flatform.buildingId.focus();
			return false;
		}
		if(document.flatform.wingId.value=="Default")
		{
			$('#wingIdSpan').html('Please Select Project wing');
			document.flatform.wingId.focus();
			return false;
		}
		//for floor name
		if(document.flatform.floorId.value=="Default")
		{
			$('#floorIdSpan').html('Please Select Floor Name');
			document.flatform.floorId.focus();
			return false;
		}
		
		// for flat facing type
		if(document.flatform.flatfacingName.value=="Default")
		{
			$('#flatfacingNameSpan').html('Please Select Flat Facing Type');
			document.flatform.flatfacingName.focus();
			return false;
		}
		//for flat type
		if(document.flatform.flatType.value=="Default")
		{
			$('#flatTypeSpan').html('Please Select Flat Type');
			document.flatform.flatType.focus();
			return false;
		}
		
		
		//for carpet area
		
		if(document.flatform.carpetArea.value=="")
		{
			$('#carpetAreaSpan').html('Please, enter carpet area');
			document.flatform.carpetArea.focus();
			return false;
		}
		else if(document.flatform.carpetArea.value.match(/^[\s]+$/))
		{
			$('#carpetAreaSpan').html('Please, enter carpet area..!');
			document.flatform.carpetArea.value="";
			document.flatform.carpetArea.focus();
			return false; 	
		}
		else if(!document.flatform.carpetArea.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			 if(!document.flatform.carpetArea.value.match(/^[0-9]+$/))
				 {
					$('#carpetAreaSpan').html('Please, use only digit value for carpet area..! eg:21.36 OR 30');
					document.flatform.carpetArea.value="";
					document.flatform.carpetArea.focus();
					return false;
				}
		}
		
//for Terrace area
		
		if(document.flatform.terraceArea.value=="")
		{
			$('#terraceAreaSpan').html('Please, enter terrace area');
			document.flatform.terraceArea.focus();
			return false;
		}
		else if(document.flatform.terraceArea.value.match(/^[\s]+$/))
		{
			$('#terraceAreaSpan').html('Please, enter terrace area..!');
			document.flatform.terraceArea.value="";
			document.flatform.terraceArea.focus();
			return false; 	
		}
		else if(!document.flatform.terraceArea.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			 if(!document.flatform.terraceArea.value.match(/^[0-9]+$/))
				 {
					$('#terraceAreaSpan').html('Please, use only digit value for terrace area..! eg:21.36 OR 30');
					document.flatform.terraceArea.value="";
					document.flatform.terraceArea.focus();
					return false;
				}
		}
		//for balacoy area
		if(document.flatform.balconyArea.value=="")
		{
			$('#balconyAreaSpan').html('Please, enter balcony area');
			document.flatform.balconyArea.focus();
			return false;
		}
		else if(document.flatform.balconyArea.value.match(/^[\s]+$/))
		{
			$('#balconyAreaSpan').html('Please, enter balcony area..!');
			document.flatform.balconyArea.value="";
			document.flatform.balconyArea.focus();
			return false; 	
		}
		else if(!document.flatform.balconyArea.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			if(!document.flatform.balconyArea.value.match(/^[0-9]+$/))
				{
					$('#balconyAreaSpan').html('Please, use only digit value for balcony area..! eg:21.36 OR 30');
					document.flatform.balconyArea.value="";
					document.flatform.balconyArea.focus();
					return false;
				}
		}
	
		//for Dry Terrace area
				
				if(document.flatform.dryterraceArea.value=="")
				{
					$('#dryterraceAreaSpan').html('Please, enter dry terrace area');
					document.flatform.dryterraceArea.focus();
					return false;
				}
				else if(document.flatform.dryterraceArea.value.match(/^[\s]+$/))
				{
					$('#dryterraceAreaSpan').html('Please, enter dry terrace area..!');
					document.flatform.dryterraceArea.value="";
					document.flatform.dryterraceArea.focus();
					return false; 	
				}
				else if(!document.flatform.dryterraceArea.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
				{
					 if(!document.flatform.dryterraceArea.value.match(/^[0-9]+$/))
						 {
							$('#dryterraceAreaSpan').html('Please, use only digit value for dry terrace area..! eg:21.36 OR 30');
							document.flatform.dryterraceArea.value="";
							document.flatform.dryterraceArea.focus();
							return false;
						}
				}
		
		
		//for flat cost
		if(document.flatform.flatCostwithotfloorise.value=="")
		{
			$('#flatCostwithotflooriseSpan').html('Please, enter flat cost');
			document.flatform.flatCostwithotfloorise.focus();
			return false;
		}
		else if(document.flatform.flatCostwithotfloorise.value.match(/^[\s]+$/))
		{
			$('#flatCostwithotflooriseSpan').html('Please, enter flat cost..!');
			document.flatform.flatCostwithotfloorise.value="";
			document.flatform.flatCostwithotfloorise.focus();
			return false; 	
		}
		else if(!document.flatform.flatCostwithotfloorise.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			 if(!document.flatform.flatCostwithotfloorise.value.match(/^[0-9]+$/))
				 {
					$('#flatCostwithotflooriseSpan').html('Please, use only digit value for flat cost..! eg:21.36 OR 30');
					document.flatform.flatCostwithotfloorise.value="";
					document.flatform.flatCostwithotfloorise.focus();
					return false;
				}
		}
		if(document.flatform.flatminimumCost.value=="")
		{
			$('#flatminimumCostSpan').html('Please, enter flat minimum cost per sq.ft');
			document.flatform.flatminimumCost.focus();
			return false;
		}
		else if(document.flatform.flatminimumCost.value.match(/^[\s]+$/))
		{
			$('#flatminimumCostSpan').html('Please, enter flat minimum cost per sq.ft..!');
			document.flatform.flatminimumCost.value="";
			document.flatform.flatminimumCost.focus();
			return false; 	
		}
		else if(!document.flatform.flatminimumCost.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			 if(!document.flatform.flatminimumCost.value.match(/^[0-9]+$/))
				 {
					$('#flatminimumCostSpan').html('Please, use only digit value for flat minimum cost per sq.ft..! eg:21.36 OR 30');
					document.flatform.flatminimumCost.value="";
					document.flatform.flatminimumCost.focus();
					return false;
				}
		}
}
function init()
{
	clearall();
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	
	
	 if(document.flatform.status.value=="Fail")
	 {
	  	//alert("Sorry, record is present already..!");
	 }
	 else if(document.flatform.status.value=="Success")
	 {
			$('#statusSpan').html('Record added successfully..!');
	 }
  document.flatform.flatNumber.focus();
}


function getFloorRise()
{
	 var projectId =$('#projectId').val();
	 var buildingId =$('#buildingId').val();
	 var wingId =$('#wingId').val();
	 var floorId =$('#floorId').val();
	 $.ajax({

			url : '${pageContext.request.contextPath}/getFloorRise',
			type : 'Post',
			data : { projectId : projectId, buildingId : buildingId, wingId : wingId, floorId : floorId},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{
								
								for(var i=0;i<result.length;i++)
								{
									document.flatform.floorRise.value=result[i].floorRise; 
								 } 
							} 
							else
							{
								alert("failure111");
								//$("#ajax_div").hide();
							}

						}
			});
	 
	 
}
function getflatAreawithLoading()
{
	 var dryterraceArea =Number($('#dryterraceArea').val());
	 var balconyArea = Number($('#balconyArea').val());
	 var carpetArea = Number($('#carpetArea').val());
	 var terraceArea = Number($('#terraceArea').val());
	 var loading = Number($('#loading').val());
	 var t=(dryterraceArea+balconyArea+carpetArea+terraceArea);
	 var totalloading=(loading/100);
	 var flatAreawithLoadingInM=((t)*(loading/100))+t;
	 var flatAreawithLoadingInFt=flatAreawithLoadingInM*10.764;
	   document.flatform.flatAreawithLoadingInM.value=flatAreawithLoadingInM.toFixed(0);
	   document.flatform.flatAreawithLoadingInFt.value=flatAreawithLoadingInFt.toFixed(0); 
	   document.flatform.loadingpercentage.value=t*loading/100;
	   document.flatform.flatArea.value=t.toFixed(2);
	   
	   getflatBasicCost();
}

function getBuldingList()
{
	 $("#buildingId").empty();
	 $("#loading").empty();
	 var projectId = $('#projectId').val();

	 

	 $.ajax({

		url : '${pageContext.request.contextPath}/getProjectLoading',
		type : 'Post',
		data : { projectId : projectId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							
							for(var i=0;i<result.length;i++)
							{
								  document.flatform.loading.value=result[i].loading; 
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
	 
	 $.ajax({

		url : '${pageContext.request.contextPath}/getBuildingList',
		type : 'Post',
		data : { projectId : projectId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Building Name-");
							$("#buildingId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].buildingId).text(result[i].buildingName);
							    $("#buildingId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	 getflatAreawithLoading();
}//end of get Building List


function getwingIdList()
{
	 $("#wingId").empty();
	 var buildingId = $('#buildingId').val();
	 var projectId = $('#projectId').val();
	 $.ajax({

		url : '${pageContext.request.contextPath}/getprojectwingList',
		type : 'Post',
		data : { buildingId : buildingId, projectId : projectId },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Wing Name-");
							$("#wingId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].wingId).text(result[i].wingName);
							    $("#wingId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}
function getUniqueFlatNumber()
{
	if(document.flatform.projectId.value!="Default")
		{
		if(document.flatform.buildingId.value!="Default")
			{
			if(document.flatform.wingId.value!="Default")
				{
				 var wingId = $('#wingId').val();
				 var buildingId = $('#buildingId').val();
				 var projectId = $('#projectId').val();
				 var flatNumber = $('#flatNumber').val();
					$.ajax({

						url : '${pageContext.request.contextPath}/getuniqueflatnumberList',
						type : 'Post',
						dataType : 'json',
						success : function(result)
								  {
										if (result) 
										{
											for(var i=0;i<result.length;i++)
											{
												if(result[i].projectId==projectId)
													{
													if(result[i].buildingId==buildingId)
													{
														if(result[i].wingId==wingId)
														{
															if(result[i].flatNumber==flatNumber)
															{
																  document.flatform.flatNumber.focus();
																  $('#flatNumberSpan').html('This flat number is already exist..!');
																  $('#flatNumber').val("");
																  getwingIdList();
															}
														}
													}
													}
												
												
											 } 
										} 
										else
										{
											alert("failure111");
											//$("#ajax_div").hide();
										}

									}
						});	
				}
			}
		
		}
	
}

function getFloorNameList()
{
	 $("#floorId").empty();
	 var wingId = $('#wingId').val();
	 var buildingId = $('#buildingId').val();
	 var projectId = $('#projectId').val();
	 $.ajax({

		url : '${pageContext.request.contextPath}/getwingfloorNameList',
		type : 'Post',
		data : {wingId : wingId, buildingId : buildingId, projectId : projectId },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Floor Name-");
							$("#floorId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].floorId).text(result[i].floortypeName);
							    $("#floorId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
	 
	 var flatNumber = $('#flatNumber').val();
		$.ajax({

			url : '${pageContext.request.contextPath}/getuniqueflatnumberList',
			type : 'Post',
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{
								for(var i=0;i<result.length;i++)
								{
									if(result[i].projectId==projectId)
										{
										if(result[i].buildingId==buildingId)
										{
											if(result[i].wingId==wingId)
											{
												if(result[i].flatNumber==flatNumber)
												{
													  document.flatform.flatNumber.focus();
													  $('#flatNumberSpan').html('This flat number is already exist..!');
													  $('#flatNumber').val("");
													  getwingIdList();
												}
											}
										}
										}
									
									
								 } 
							} 
							else
							{
								alert("failure111");
								//$("#ajax_div").hide();
							}

						}
			});	
}//end of get Floor Type List
function getflatBasicCost()
{
	var flatCostwithotfloorise = Number($('#flatCostwithotfloorise').val());
	var floorRise = Number($('#floorRise').val());
	
	 var flatAreawithLoadingInFt = Number($('#flatAreawithLoadingInFt').val());
	 
	 var totalFlatCosrperSqft=flatCostwithotfloorise+floorRise;
	 document.flatform.flatCost.value=totalFlatCosrperSqft;
	 var flatCost = Number($('#flatCost').val());
	 var flatBasiccost=(flatAreawithLoadingInFt*flatCost).toFixed(0);
		document.flatform.flatbasicCost.value=flatBasiccost;
		
		
}

  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>

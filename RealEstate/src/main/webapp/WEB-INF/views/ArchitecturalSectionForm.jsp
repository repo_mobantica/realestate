<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Architectural</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


 <style type="text/css">
    tr.odd {background-color: #CCE5FF}
    tr.even {background-color: #F0F8FF}
 </style>

<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
 
 <script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
 </script>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
    		
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>

<div class="wrapper">

  <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
         Architectural Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Architectural</a></li>
        <li class="active">Architectural Section</li>
      </ol>
    </section>

	
<form name="architecturalSectionForm" action="${pageContext.request.contextPath}/ArchitecturalSectionForm" method="post" onSubmit="return validate()" >
    <section class="content">

    <div class="box box-default">
      <div class="box-body">
          <div class="row">
            <div class="col-md-12">
             <span id="statusSpan" style="color:#008000"></span>
				
			<div class="box-body">
              <div class="row">
                  <div class="col-xs-2">
                  <label>Id</label>
                  <input type="text" class="form-control"  id="projectArchitecturalId"name="projectArchitecturalId" value="${projectArchitecturalCode}" readonly>
                </div>               
                
                 <div class="col-xs-2">
                  <label>Project Name</label>
                  <input type="hidden" class="form-control"  id="projectId"name="projectId" value="${projectList[0].projectId}" readonly>
                  <input type="text" class="form-control" value="${projectList[0].projectName}" readonly>
                </div>               
                
                  <div class="col-xs-3">
                  <label>Architectural Name </label> <label class="text-red">* </label>
                  <select class="form-control" name="consultancyId" id="consultancyId" >
				  		<option selected="" value="Default">Select Architectural-</option>
                        <c:forEach var="consultancyList" items="${consultancyList}">
	                    <option value="${consultancyList.consultancyId}">${consultancyList.consultancyfirmName} </option>
	                    </c:forEach>
                   </select>
                  <span id="consultancyIdSpan" style="color:#FF0000"></span>
                </div> 
              </div>
            </div>
     
       <div class="panel box box-danger"></div>       
		<div class="box-body">
			<div class="row">
			
              <div class="col-xs-8">
              <div class="table-responsive">
                <table class="table table-bordered" id="CompanyListTable">
                  <thead>
	                  <tr>
	                    <th>A</th>
	                    <th>AREA STATEMENT</th>
	                    <th style="width:100px">Percentage</th>
	                    <th style="width:100px">Area in SQ.M.</th>
	                  </tr>
                  </thead>
                 
	                  <tr>
	                   <td>1a</td>
	                   <td>AREA OF PLOT AS PER 7/12 EXTRACT</td>
	                    <td> <input type="text" class="form-control"  id="aeraOfPlotAsPer7_12ExtractPer"name="aeraOfPlotAsPer7_12ExtractPer"  onchange="Calculatetotal2Cal(this.value)"></td>
	                   <td> <input type="text" class="form-control"  id="aeraOfPlotAsPer7_12ExtractArea"name="aeraOfPlotAsPer7_12ExtractArea"  onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                  
	                  <tr>
	                   <td>1b</td>
	                   <td>AREA OF PLOT AS PER PROPERTY CARD</td>
	                   
	                   <td> <input type="text" class="form-control"  id="areaOfPlotAsPerPropetyPer"name="areaOfPlotAsPerPropetyPer"></td>
	                   <td> <input type="text" class="form-control"  id="areaOfPlotAsPerPropetyArea"name="areaOfPlotAsPerPropetyArea"></td>
	                  </tr>
	                  
	                  <tr>
	                   <td>1c</td>
	                   <td>AREA OF PLOT AS PER DEMARCATION</td>
	                  
	                   <td> <input type="text" class="form-control"  id="areaOfPlotAsPerDemarcationPer"name="areaOfPlotAsPerDemarcationPer"></td>
	                   <td> <input type="text" class="form-control"  id="areaOfPlotAsPerDemarcationArea"name="areaOfPlotAsPerDemarcationArea"></td>
	                  </tr>
	                  
	                  <tr>
	                   <td>1d</td>
	                   <td>AREA AS PER SANCTIONED LAYOUT</td>
	                 
	                   <td> <input type="text" class="form-control"  id="areaAsPerSanctionedLayoutPer"name="areaAsPerSanctionedLayoutPer"></td>
	                   <td> <input type="text" class="form-control"  id="areaAsPerSanctionedLayoutArea"name="areaAsPerSanctionedLayoutArea"></td>
	                  </tr>
	                    
	                  <tr>
	                   <td>1e</td>
	                   <td>AREA AS PER U.L.C. ORDER</td>
	                 
	                   <td> <input type="text" class="form-control"  id="areaAsPerULCOrderPer"name="areaAsPerULCOrderPer"></td>
	                   <td> <input type="text" class="form-control"  id="areaAsPerULCOrderArea"name="areaAsPerULCOrderArea"></td>
	                  </tr>
	                     
	                  <tr>
	                   <td>1f</td>
	                   <td>AREA AS PER DEVELOPMENT AGREEMENT /P.O.A.H.</td>
	                   
	                   <td> <input type="text" class="form-control"  id="areaAsPerDevelopmentAgreementPer" name="areaAsPerDevelopmentAgreementPer"></td>
	                   <td> <input type="text" class="form-control"  id="areaAsPerDevelopmentAgreementArea" name="areaAsPerDevelopmentAgreementArea"></td>
	                  </tr>
	                     
	                  <tr>
	                   <td>1g</td>
	                   <td>AREA OF PLOT MINIMUM CONSIDERATION</td>
	                  
	                   <td> <input type="text" class="form-control"  id="areaOfPlotMinimumConsiderationPer"name="areaOfPlotMinimumConsiderationPer"></td>
	                   <td> <input type="text" class="form-control"  id="areaOfPlotMinimumConsiderationArea"name="areaOfPlotMinimumConsiderationArea"></td>
	                  </tr>
	                  
	               <thead>
	                  <tr>
	                    <th>2</th>
	                    <th>DEDUCTIONS FOR</th>
	                    
	                  </tr>
                  </thead> 
                     
	                  <tr>
	                   <td>2a</td>
	                   <td>AREA UNDER B.R.T. CORRIDOR</td>
	                   
	                   <td> <input type="text" class="form-control"  id="areaUnderBRTCorridorPer"name="areaUnderBRTCorridorPer" onchange="Calculatetotal2Cal(this.value)"></td>
	                   <td> <input type="text" class="form-control"  id="areaUnderBRTCorridorArea"name="areaUnderBRTCorridorArea" onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                     
	                  <tr>
	                   <td>2b</td>
	                   <td>AREA UNDER 1.50 M. ROAD WIDENING</td>
	                 
	                   <td> <input type="text" class="form-control"  id="areaUnder1_50MRoadWideningPer"name="areaUnder1_50MRoadWideningPer" onchange="Calculatetotal2Cal(this.value)"></td>
	                   <td> <input type="text" class="form-control"  id="areaUnder1_50MRoadWideningArea"name="areaUnder1_50MRoadWideningArea" onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                  
	                      
	                  <tr>
	                   <td>2c</td>
	                   <td>AREA UNDER 3.00 M. ROAD WIDENING</td>
	                   
	                   <td> <input type="text" class="form-control"  id="areaUnder3_00MRoadWideningPer"name="areaUnder3_00MRoadWideningPer" onchange="Calculatetotal2Cal(this.value)"></td>
	                   <td> <input type="text" class="form-control"  id="areaUnder3_00MRoadWideningArea"name="areaUnder3_00MRoadWideningArea" onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                      
	                  <tr>
	                   <td>2d</td>
	                   <td>AREA UNDER 4.50 M. ROAD WIDENING</td>
	                   
	                   <td> <input type="text" class="form-control"  id="areaUnder4_50MRoadWideningPer"name="areaUnder4_50MRoadWideningPer" onchange="Calculatetotal2Cal(this.value)"></td>
	                   <td> <input type="text" class="form-control"  id="areaUnder4_50MRoadWideningArea"name="areaUnder4_50MRoadWideningArea" onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                      
	                        
	                      
	                  <tr>
	                   <td>2e</td>
	                   <td>AREA UNDER 6.00 M. ROAD WIDENING</td>
	                   
	                   <td> <input type="text" class="form-control"  id="areaUnder6_00MRoadWideningPer"name="areaUnder6_00MRoadWideningPer" onchange="Calculatetotal2Cal(this.value)"></td>
	                   <td> <input type="text" class="form-control"  id="areaUnder6_00MRoadWideningArea"name="areaUnder6_00MRoadWideningArea" onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                      
	                  <tr>
	                   <td>2f</td>
	                   <td>AREA UNDER 7.50 M. ROAD WIDENING</td>
	                   
	                   <td> <input type="text" class="form-control"  id="areaUnder7_50MRoadWideningPer"name="areaUnder7_50MRoadWideningPer" onchange="Calculatetotal2Cal(this.value)"></td>
	                   <td> <input type="text" class="form-control"  id="areaUnder7_50MRoadWideningArea"name="areaUnder7_50MRoadWideningArea" onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                      
	                              
	                      
	                  <tr>
	                   <td>2g</td>
	                   <td>AREA UNDER 9.00 M. ROAD WIDENING</td>
	                   
	                   <td> <input type="text" class="form-control"  id="areaUnder9_00MRoadWideningPer"name="areaUnder9_00MRoadWideningPer" onchange="Calculatetotal2Cal(this.value)"></td>
	                   <td> <input type="text" class="form-control"  id="areaUnder9_00MRoadWideningArea"name="areaUnder9_00MRoadWideningArea" onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                      
	                  <tr>
	                   <td>2h</td>
	                   <td>AREA UNDER 12.00 M. ROAD WIDENING</td>
	                   
	                   <td> <input type="text" class="form-control"  id="areaUnder12_00MRoadWideningPer"name="areaUnder12_00MRoadWideningPer" onchange="Calculatetotal2Cal(this.value)"></td>
	                   <td> <input type="text" class="form-control"  id="areaUnder12_00MRoadWideningArea"name="areaUnder12_00MRoadWideningArea" onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                             
	                      
	                  <tr>
	                   <td>2i</td>
	                   <td>AREA UNDER 15.00 M. ROAD WIDENING</td>
	                   
	                   <td> <input type="text" class="form-control"  id="areaUnder15_00MRoadWideningPer"name="areaUnder15_00MRoadWideningPer" onchange="Calculatetotal2Cal(this.value)"></td>
	                   <td> <input type="text" class="form-control"  id="areaUnder15_00MRoadWideningArea"name="areaUnder15_00MRoadWideningArea" onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                      
	                  <tr>
	                   <td>2j</td>
	                   <td>AREA UNDER 18.00 M. ROAD WIDENING</td>
	                   
	                   <td> <input type="text" class="form-control"  id="areaUnder18_00MRoadWideningPer"name="areaUnder18_00MRoadWideningPer" onchange="Calculatetotal2Cal(this.value)"></td>
	                   <td> <input type="text" class="form-control"  id="areaUnder18_00MRoadWideningArea"name="areaUnder18_00MRoadWideningArea" onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                             
	                      
	                  <tr>
	                   <td>2k</td>
	                   <td>AREA UNDER 24.00 M. ROAD WIDENING</td>
	                   
	                   <td> <input type="text" class="form-control"  id="areaUnder24_00MRoadWideningPer"name="areaUnder24_00MRoadWideningPer" onchange="Calculatetotal2Cal(this.value)"></td>
	                   <td> <input type="text" class="form-control"  id="areaUnder24_00MRoadWideningArea"name="areaUnder24_00MRoadWideningArea" onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                      
	                  <tr>
	                   <td>2l</td>
	                   <td>AREA UNDER 30.00 M. ROAD WIDENING</td>
	                   
	                   <td> <input type="text" class="form-control"  id="areaUnder30_00MRoadWideningPer"name="areaUnder30_00MRoadWideningPer" onchange="Calculatetotal2Cal(this.value)"></td>
	                   <td> <input type="text" class="form-control"  id="areaUnder30_00MRoadWideningArea"name="areaUnder30_00MRoadWideningArea" onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                      
	                      
	                  <tr>
	                   <td>2m</td>
	                   <td>AREA UNDER SERVICE ROAD</td>
	                   
	                   <td> <input type="text" class="form-control"  id="areaUnderServiceRoadPer"name="areaUnderServiceRoadPer" onchange="Calculatetotal2Cal(this.value)"></td>
	                   <td> <input type="text" class="form-control"  id="areaUnderServiceRoadArea"name="areaUnderServiceRoadArea" onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                      
	                  <tr>
	                   <td>2n</td>
	                   <td>AREA UNDER RESERVATION</td>
	                   
	                   <td> <input type="text" class="form-control"  id="areaUnderReservationPer"name="areaUnderReservationPer" onchange="Calculatetotal2Cal(this.value)"></td>
	                   <td> <input type="text" class="form-control"  id="areaUnderReservationArea"name="areaUnderReservationArea" onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                      
	                  <tr>
	                   <td>2o</td>
	                   <td>AREA UNDER GREEN BELT</td>
	                   
	                   <td> <input type="text" class="form-control"  id="areaUnderGreenBeltPer"name="areaUnderGreenBeltPer" onchange="Calculatetotal2Cal(this.value)"></td>
	                   <td> <input type="text" class="form-control"  id="areaUnderGreenBeltArea"name="areaUnderGreenBeltArea" onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                          
	                  <tr>
	                   <td>2p</td>
	                   <td>AREA UNDER NALA</td>
	                   
	                   <td> <input type="text" class="form-control"  id="areaUnderNalaPer"name="areaUnderNalaPer" onchange="Calculatetotal2Cal(this.value)"></td>
	                   <td> <input type="text" class="form-control"  id="areaUnderNalaArea"name="areaUnderNalaArea" onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                
	                  <tr>
	                    <th></th>
	                    <th>TOTAL (a+b+c+d+e+f+g+h+i+j+k+l+m+n+o+p)</th>
	                    <th></th>
	                    <th> <input type="text" class="form-control"  id="total2Area"name="total2Area"></th>
	                  </tr>
	   <!--                              
                </table>
              </div>
              </div>
              
              <div class="col-xs-4">
              <div class="table-responsive">
                <table class="table table-bordered" id="CompanyListTable">
                  -->     
	               <thead>
	                  <tr>
	                   <th>3</th>
	                   <th>NET GROSS AREA OF PLOT (1-2)</th>
	                   
	                   <th></th>
	                   <th> <input type="text" class="form-control"  id="netGrossAreaOfPlot1_2Area"name="netGrossAreaOfPlot1_2Area"></th>
	                  </tr>
                  </thead>  
	                      
	               <thead>
	                  <tr>
	                    <th>4</th>
	                    <th>DEDUCTIONS FOR</th>
	                  </tr>
                  </thead>  
	                             
	                  <tr>
	                   <td>4a</td>
	                   <td>RECREATION GROUND 10%</td>
	                   
	                   <td> <input type="text" class="form-control"  id="recreationGround10Per"name="recreationGround10Per"></td>
	                   <td> <input type="text" class="form-control"  id="recreationGround10Area"name="recreationGround10Area"></td>
	                  </tr>
	                             
	                  <tr>
	                   <td>4b</td>
	                   <td>AMENITY SPACE (5%)</td>
	                   
	                   <td> <input type="text" class="form-control"  id="amenitySpace5Per"name="amenitySpace5Per"></td>
	                   <td> <input type="text" class="form-control"  id="amenitySpace5Area"name="amenitySpace5Area"></td>
	                  </tr>
	                             
	                  <tr>
	                   <td>4c</td>
	                   <td>INTERNAL ROADS</td>
	                   
	                   <td> <input type="text" class="form-control"  id="internalRoadsPer"name="internalRoadsPer"  onchange="Calculatetotal2Cal(this.value)"></td>
	                   <td> <input type="text" class="form-control"  id="internalRoadsArea"name="internalRoadsArea"  onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                             
	                  <tr>
	                   <td>4b</td>
	                   <td>TRANSFORMER</td>
	                   
	                   <td> <input type="text" class="form-control"  id="transformerPer"name="transformerPer" onchange="Calculatetotal2Cal(this.value)"></td>
	                   <td> <input type="text" class="form-control"  id="transformerArea"name="transformerArea" onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                                  
	                  <tr>
	                   <td></td>
	                   <td>TOTAL (a+b+c+d)</td>
	                   <td></td>
	                   <td> <input type="text" class="form-control"  id="total4Area"name="total4Area"></td>
	                  </tr>
	                      
	               <thead>
	                  <tr>
	                    <th>5</th>
	                    <th>NET AREA OF PLOT (3-4)</th>
	                    <th></th>
	                    <th> <input type="text" class="form-control"  id="netAreaOfPlot3_4Area"name="netAreaOfPlot3_4Area"></th>
	                  </tr>
                  </thead> 
                                 
	               <thead>
	                  <tr>
	                    <th>6</th>
	                    <th>ADDITION FOR</th>
	                    <th></th>
	                  </tr>
                  </thead> 
                                 
	                  <tr>
	                   <td>6a</td>
	                   <td>T.D.R.( X 40%)</td>
	                   
	                   <td> <input type="text" class="form-control"  id="tdr40Per"name="tdr40Per"  onchange="Calculatetotal2Cal(this.value)"></td>
	                   <td> <input type="text" class="form-control"  id="tdr40Area"name="tdr40Area"  onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                             
	                  <tr>
	                   <td>6b</td>
	                   <td>ADDITIONS FOR F.A.R. 4a  (RECREATION GROUND)</td>
	                   
	                   <td> <input type="text" class="form-control"  id="additionsForFAR4a_RecreationGroundPer"name="additionsForFAR4a_RecreationGroundPer"  onchange="Calculatetotal2Cal(this.value)"></td>
	                   <td> <input type="text" class="form-control"  id="additionsForFAR4a_RecreationGroundArea"name="additionsForFAR4a_RecreationGroundArea"  onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                             
	                  <tr>
	                   <td>6c</td>
	                   <td>ADDITIONS FOR F.A.R. 4b  (AMENITY SPACE)</td>
	                   
	                   <td> <input type="text" class="form-control"  id="additionForFAR4b_AmenitySpacePer"name="additionForFAR4b_AmenitySpacePer"  onchange="Calculatetotal2Cal(this.value)"></td>
	                   <td> <input type="text" class="form-control"  id="additionForFAR4b_AmenitySpaceArea"name="additionForFAR4b_AmenitySpaceArea"  onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                             
	                  <tr>
	                   <td>6d</td>
	                   <td>ADDITIONS FOR F.A.R. 4c  (INTERNALS ROAD)</td>
	                   
	                   <td> <input type="text" class="form-control"  id="additionForFAR4c_InternalsRoadPer"name="additionForFAR4c_InternalsRoadPer"  onchange="Calculatetotal2Cal(this.value)"></td>
	                   <td> <input type="text" class="form-control"  id="additionForFAR4c_InternalsRoadArea"name="additionForFAR4c_InternalsRoadArea"  onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                              
	                  <tr>
	                   <td>6e</td>
	                   <td>ADDITIONS FOR F.A.R. 4d  (TRANSFORMER)</td>
	                   
	                   <td> <input type="text" class="form-control"  id="additionForFAR4d_TransformerPer"name="additionForFAR4d_TransformerPer"  onchange="Calculatetotal2Cal(this.value)"></td>
	                   <td> <input type="text" class="form-control"  id="additionForFAR4d_TransformerArea"name="additionForFAR4d_TransformerArea"  onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                                  
	                  <tr>
	                   <td></td>
	                   <td>TOTAL (a+b+c+d+e)</td>
	                   <td></td>
	                   <td> <input type="text" class="form-control"  id="total6Area"name="total6Area"></td>
	                  </tr>
	                     
	               <thead>
	                  <tr>
	                    <th>7</th>
	                    <th>TOTAL AREA (5+6)</th>
	                    <th></th>
	                    <th> <input type="text" class="form-control"  id="totalArea5_6Area"name="totalArea5_6Area"></th>
	                  </tr>
                  </thead> 
                     
	               <thead>
	                  <tr>
	                    <th>8</th>
	                    <th>F.A.R. PERMISSIBLE</th>
	                    
	                    <th> <input type="text" class="form-control"  id="farPermissiblePer"name="farPermissiblePer"  onchange="Calculatetotal2Cal(this.value)"></th>
	                    <th> <input type="text" class="form-control"  id="farPermissibleArea"name="farPermissibleArea"  onchange="Calculatetotal2Cal(this.value)"></th>
	                  </tr>
                  </thead> 
                                            
	               <thead>
	                  <tr>
	                    <th>9</th>
	                    <th>TOTAL PERMISSIBLE FLOOR AREA (7X8)</th>
	                    <th></th>
	                    <th> <input type="text" class="form-control"  id="totalPermissibleFloorArea7_8Area"name="totalPermissibleFloorArea7_8Area" onchange="Calculatetotal2Cal(this.value)"></th>
	                  </tr>
                  </thead> 
                                          
	               <thead>
	                  <tr>
	                    <th>10</th>
	                    <th>PERMISSIBLE RESIDENTIAL AREA</th>
	                    
	                    <th> <input type="text" class="form-control"  id="permissibleResidentialPer"name="permissibleResidentialPer"></th>
	                    <th> <input type="text" class="form-control"  id="permissibleResidentialArea"name="permissibleResidentialArea"></th>
	                  </tr>
                  </thead> 
                                            
	               <thead>
	                  <tr>
	                    <th>11</th>
	                    <th>EXISTING RESIDENTIAL AREA</th>
	                    
	                    <th> <input type="text" class="form-control"  id="existingResidentialPer"name="existingResidentialPer" onchange="CalculatetotalResidentialArea11_12Area(this.value)"></th>
	                    <th> <input type="text" class="form-control"  id="existingResidentialArea"name="existingResidentialArea" onchange="CalculatetotalResidentialArea11_12Area(this.value)"></th>
	                  </tr>
                  </thead> 
                                            
	               <thead>
	                  <tr>
	                    <th>12</th>
	                    <th>PROPOSED RESIDENTIAL AREA</th>
	                    
	                    <th> <input type="text" class="form-control"  id="proposedResidentialPer"name="proposedResidentialPer" onchange="CalculatetotalResidentialArea11_12Area(this.value)"></th>
	                    <th> <input type="text" class="form-control"  id="proposedResidentialArea"name="proposedResidentialArea" onchange="CalculatetotalResidentialArea11_12Area(this.value)"></th>
	                  </tr>
                  </thead> 
                                            
	               <thead>
	                  <tr>
	                    <th>13</th>
	                    <th>TOTAL RESIDENTIAL AREA (11+12)</th>
	                    
	                    <th> </th>
	                    <th> <input type="text" class="form-control"  id="totalResidentialArea11_12Area"name="totalResidentialArea11_12Area" onchange="CalculatetotalResidentialArea11_12Area(this.value)"></th>
	                  </tr>
                  </thead> 
                                            
	               <thead>
	                  <tr>
	                    <th>14</th>
	                    <th>PERMISSIBLE COMMERCIAL/INDUSTRIAL AREA</th>
	                    
	                    <th> <input type="text" class="form-control"  id="permissibleCommercial_IndustrialPer"name="permissibleCommercial_IndustrialPer" onchange="CalculatetotalResidentialArea11_12Area(this.value)"></th>
	                    <th> <input type="text" class="form-control"  id="permissibleCommercial_IndustrialArea"name="permissibleCommercial_IndustrialArea" onchange="CalculatetotalResidentialArea11_12Area(this.value)"></th>
	                  </tr>
                  </thead> 
                                             
	               <thead>
	                  <tr>
	                    <th>15</th>
	                    <th>EXISTING COMMERCIAL/INDUSTRIAL AREA</th>
	                    
	                    <th> <input type="text" class="form-control"  id="existingCommercial_IndustrialPer"name="existingCommercial_IndustrialPer" onchange="CalculatetotalResidentialArea11_12Area(this.value)"></th>
	                    <th> <input type="text" class="form-control"  id="existingCommercial_IndustrialArea"name="existingCommercial_IndustrialArea" onchange="CalculatetotalResidentialArea11_12Area(this.value)"></th>
	                  </tr>
                  </thead> 
                                             
	               <thead>
	                  <tr>
	                    <th>16</th>
	                    <th>PROPOSED COMMERCIAL/INDUSTRIAL AREA</th>
	                    
	                    <th> <input type="text" class="form-control"  id="proposedCommercial_IndustrialPer"name="proposedCommercial_IndustrialPer" onchange="CalculatetotalResidentialArea11_12Area(this.value)"></th>
	                    <th> <input type="text" class="form-control"  id="proposedCommercial_IndustrialArea"name="proposedCommercial_IndustrialArea" onchange="CalculatetotalResidentialArea11_12Area(this.value)"></th>
	                  </tr>
                  </thead> 
                 
       <!--                              
                </table>
              </div>
              </div>
              
              <div class="col-xs-4">
              <div class="table-responsive">
                <table class="table table-bordered" id="CompanyListTable">
                 -->                             
	               <thead>
	                  <tr>
	                    <th>17</th>
	                    <th>TOTAL COMMERCIAL/INDUSTRIAL AREA  (15+16)</th>
	                    
	                    <th></th>
	                    <th> <input type="text" class="form-control"  id="totalCommercial_IndustrialArea15_16Area"name="totalCommercial_IndustrialArea15_16Area" onchange="CalculatetotalResidentialArea11_12Area(this.value)"></th>
	                  </tr>
                  </thead> 
                                             
	               <thead>
	                  <tr>
	                    <th>18</th>
	                    <th>TOTAL EXISTING AREA (11+15)</th>
	                    
	                    <th> </th>
	                    <th> <input type="text" class="form-control"  id="totalExistingArea11_15"name="totalExistingArea11_15" onchange="CalculatetotalResidentialArea11_12Area(this.value)"></th>
	                  </tr>
                  </thead> 
                                             
	               <thead>
	                  <tr>
	                    <th>19</th>
	                    <th>TOTAL PROPOSED AREA (12+16)</th>
	                    
	                    <th> </th>
	                    <th> <input type="text" class="form-control"  id="totalProposedArea12_16Area"name="totalProposedArea12_16Area" onchange="CalculatetotalResidentialArea11_12Area(this.value)"></th>
	                  </tr>
                  </thead> 
                                             
	               <thead>
	                  <tr>
	                    <th>20</th>
	                    <th>TOTAL EXISTING AREA + PROPOSED (18+19)</th>
	                    
	                    <th></th>
	                    <th> <input type="text" class="form-control"  id="totalExistingArea_Proposed18_19Area"name="totalExistingArea_Proposed18_19Area" onchange="CalculatetotalResidentialArea11_12Area(this.value)"></th>
	                  </tr>
                  </thead> 
                                             
	               <thead>
	                  <tr>
	                    <th>21</th>
	                    <th>EXCESS BALCONY AREA TAKEN IN F.S.I. </th>
	                    
	                    <th> <input type="text" class="form-control"  id="excessBalconyAreaTakenInFSIPer"name="excessBalconyAreaTakenInFSIPer" onchange="CalculatetotalResidentialArea11_12Area(this.value)"></th>
	                    <th> <input type="text" class="form-control"  id="excessBalconyAreaTakenInFSIArea"name="excessBalconyAreaTakenInFSIArea" onchange="CalculatetotalResidentialArea11_12Area(this.value)"></th>
	                  </tr>
                  </thead> 
                                             
	               <thead>
	                  <tr>
	                    <th>22</th>
	                    <th>TOTAL B'UP AREA (20+21)</th>
	                    
	                    <th></th>
	                    <th> <input type="text" class="form-control"  id="totalBupArea20_21Area"name="totalBupArea20_21Area" onchange="CalculatetotalResidentialArea11_12Area(this.value)"></th>
	                  </tr>
                  </thead> 
                                             
	               <thead>
	                  <tr>
	                    <th>23</th>
	                    <th>TOTAL F.S.I. CONSUMED</th>
	                    
	                    <th> <input type="text" class="form-control"  id="totalFSIConSumedPer"name="totalFSIConSumedPer"></th>
	                    <th> <input type="text" class="form-control"  id="totalFSIConSumedArea"name="totalFSIConSumedArea"></th>
	                  </tr>
                  </thead> 
                    
	               <thead>
	                  <tr>
	                    <th>B</th>
	                    <th>BALCONY AREA STATEMENT</th>
	                    <th></th>
	                  </tr>
                  </thead> 
                    
	                  <tr>
	                   <td>a</td>
	                   <td>PERMISSIBLE BALCONY AREA</td>
	                   
	                    <td> <input type="text" class="form-control"  id="permissibleBalconyPer"name="permissibleBalconyPer"  onchange="CalculateexcessBalconyArea(this.value)"></td>
	                    <td> <input type="text" class="form-control"  id="permissibleBalconyArea"name="permissibleBalconyArea"  onchange="CalculateexcessBalconyArea(this.value)"></td>
	                 
	                  <tr>
	                   <td>b</td>
	                   <td>PROPOSED BALCONY AREA</td>
	                   
	                    <td> <input type="text" class="form-control"  id="proposedBalconyPer"name="proposedBalconyPer" onchange="CalculateexcessBalconyArea(this.value)"></td>
	                    <td> <input type="text" class="form-control"  id="proposedBalconyArea"name="proposedBalconyArea" onchange="CalculateexcessBalconyArea(this.value)"></td>
	                 
	                  </tr>
	                   
	                  <tr>
	                   <td>c</td>
	                   <td>EXCESS BALCONY AREA (TOTAL)</td>
	                   
	                    <td> <input type="text" class="form-control"  id="excessBalconyPer"name="excessBalconyPer"></td>
	                    <td> <input type="text" class="form-control"  id="excessBalconyArea"name="excessBalconyArea"></td>
	                 
	                  </tr>
	                   	      
	               <thead>
	                  <tr>
	                    <th>C</th>
	                    <th>TENEMENT STATEMENT</th>
	                   <th></th>
	                  </tr>
                  </thead> 
                          
	                  <tr>
	                   <td>a</td>
	                   <td>NET GROSS AREA OF PLOT (A7)</td>
	                   
	                    <td> <input type="text" class="form-control"  id="netGrossAreaOfPlot_A7_Per"name="netGrossAreaOfPlot_A7_Per" onchange="CalculatetenenmentsPermissibleArea(this.value)"></td>
	                    <td> <input type="text" class="form-control"  id="netGrossAreaOfPlot_A7_Area"name="netGrossAreaOfPlot_A7_Area" onchange="CalculatetenenmentsPermissibleArea(this.value)"></td>
	                 
	                  </tr>
	                      
	                  <tr>
	                   <td>b</td>
	                   <td>LESS DEDUCTION FOR NON-RESIDENTIAL AREA</td>
	                   
	                    <td> <input type="text" class="form-control"  id="lessDeductionForNon_ResidentialPer"name="lessDeductionForNon_ResidentialPer" onchange="CalculatetenenmentsPermissibleArea(this.value)"></td>
	                    <td> <input type="text" class="form-control"  id="lessDeductionForNon_ResidentialArea"name="lessDeductionForNon_ResidentialArea" onchange="CalculatetenenmentsPermissibleArea(this.value)"></td>
	                  </tr>
	                      
	                  <tr>
	                   <td>c</td>
	                   <td>AREA OF TENEMENTS (a-b)</td>
	                   
	                    <td></td>
	                    <td> <input type="text" class="form-control"  id="areaOfTenementsa_bArea"name="areaOfTenementsa_bArea" onchange="CalculatetenenmentsPermissibleArea(this.value)"></td>
	                  </tr>
	                          
	                  <tr>
	                   <td>d</td>
	                   <td>TENEMENTS PERMISSIBLE</td>
	                   
	                    <td> <input type="text" class="form-control"  id="tenenmentsPermissiblePer"name="tenenmentsPermissiblePer" onchange="CalculatetenenmentsPermissibleArea(this.value)"></td>
	                    <td> <input type="text" class="form-control"  id="tenenmentsPermissibleArea"name="tenenmentsPermissibleArea" onchange="CalculatetenenmentsPermissibleArea(this.value)"></td>
	                 
	                  </tr>
	                         
	                  <tr>
	                   <td>e</td>
	                   <td>TOTAL TENEMENTS PROPOSED</td>
	                   
	                    <td> <input type="text" class="form-control"  id="totalTenementsProposedPer"name="totalTenementsProposedPer"></td>
	                    <td> <input type="text" class="form-control"  id="totalTenementsProposedArea"name="totalTenementsProposedArea"></td>
	                  </tr>
	                                        
	               <thead>
	                  <tr>
	                    <th>D</th>
	                    <th>GROUND COVERAGE  STATEMENT</th>
	                    <th></th>
	                  </tr>
                  </thead> 
                                             
	                  <tr>
	                   <td>a</td>
	                   <td>NET  PLOT AREA</td>
	                   
	                    <td> <input type="text" class="form-control"  id="netPlotAreaPer"name="netPlotAreaPer" onchange="CalculateexcessGroundConveraged_bArea(this.value)"></td>
	                    <td> <input type="text" class="form-control"  id="netPlotAreaArea"name="netPlotAreaArea" onchange="CalculateexcessGroundConveraged_bArea(this.value)"></td>
	                  </tr>
	                            
	                  <tr>
	                   <td>b</td>
	                   <td>PERMISSIBLE GROUND COVERAGE </td>
	                   
	                    <td> <input type="text" class="form-control"  id="permissibleGroundCoveragePer"name="permissibleGroundCoveragePer" onchange="CalculateexcessGroundConveraged_bArea(this.value)"></td>
	                    <td> <input type="text" class="form-control"  id="permissibleGroundCoverageArea"name="permissibleGroundCoverageArea" onchange="CalculateexcessGroundConveraged_bArea(this.value)"></td>
	                  </tr>
	                            
	                  <tr>
	                   <td>c</td>
	                   <td>EXISTING GROUND COVERAGE</td>
	                   
	                    <td> <input type="text" class="form-control"  id="existingGroundCoveragePer"name="existingGroundCoveragePer" onchange="CalculateexcessGroundConveraged_bArea(this.value)"></td>
	                    <td> <input type="text" class="form-control"  id="existingGroundCoverageArea"name="existingGroundCoverageArea" onchange="CalculateexcessGroundConveraged_bArea(this.value)"></td>
	                  </tr>
	                            
	                  <tr>
	                   <td>d</td>
	                   <td>PROPOSED GROUND COVERAGE</td>
	                   
	                    <td> <input type="text" class="form-control"  id="proposedGroundCoveragePer"name="proposedGroundCoveragePer" onchange="CalculateexcessGroundConveraged_bArea(this.value)"></td>
	                    <td> <input type="text" class="form-control"  id="proposedGroundCoverageArea"name="proposedGroundCoverageArea" onchange="CalculateexcessGroundConveraged_bArea(this.value)"></td>
	                  </tr>
	                            
	                  <tr>
	                   <td>e</td>
	                   <td>PERMISSIBLE GROUND COVERAGE IN PREMIUM  15% OF</td>
	                   
	                    <td> <input type="text" class="form-control"  id="permissibleGroundCoverageInPremium15Per"name="permissibleGroundCoverageInPremium15Per" onchange="CalculateexcessGroundConveraged_bArea(this.value)"></td>
	                    <td> <input type="text" class="form-control"  id="permissibleGroundCoverageInPremium15Area"name="permissibleGroundCoverageInPremium15Area" onchange="CalculateexcessGroundConveraged_bArea(this.value)"></td>
	                  </tr>
	                            
	                  <tr>
	                   <td>f</td>
	                   <td>EXCESS GROUND COVERAGE (d-b)</td>
	                   
	                    <td></td>
	                    <td> <input type="text" class="form-control"  id="excessGroundConveraged_bArea"name="excessGroundConveraged_bArea"></td>
	                  </tr>
	                          
                </table>
              </div>
              </div>
              </div>
            </div> 
             
           <div class="box-body">
            <div class="row">
              </br>
                <div class="col-xs-1">
                </div>
                   <div class="col-xs-4">
                   <div class="col-xs-4">
                	<a href="ArchitecturalSectionMaster"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
			     </div>
			     </div>
				  <div class="col-xs-4">
                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
              
			     </div>
				 <div class="col-xs-2">
			      <button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
			     </div> 
			
              </div>
		  </div>   
           
  	          <input type="hidden" id="status" name="status" value="Active">	
			  <input type="hidden" id="architecturalStatus" name="architecturalStatus" value="${architecturalStatus}">	
			  <input type="hidden" id="creationDate" name="creationDate" value="">
			  <input type="hidden" id="updateDate" name="updateDate" value="">
			  <input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>">              
 	</div>
 </div>	   
  </div>
 </div>
</section>
</form>
</div>            
              
 <%@ include file="footer.jsp" %>
<div class="control-sidebar-bg"></div>
</div> 
<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script>
function Calculatetotal2Cal()
{
	//1st
	var aeraOfPlotAsPer7_12ExtractArea = Number($('#aeraOfPlotAsPer7_12ExtractArea').val());
	
	//2nd
	 var areaUnderBRTCorridorArea =Number($('#areaUnderBRTCorridorArea').val());
	 var areaUnder1_50MRoadWideningArea = Number($('#areaUnder1_50MRoadWideningArea').val());
	 var areaUnder3_00MRoadWideningArea = Number($('#areaUnder3_00MRoadWideningArea').val());
	 var areaUnder4_50MRoadWideningArea = Number($('#areaUnder4_50MRoadWideningArea').val());
	 var areaUnder6_00MRoadWideningArea = Number($('#areaUnder6_00MRoadWideningArea').val());
	 var areaUnder7_50MRoadWideningArea =Number($('#areaUnder7_50MRoadWideningArea').val());
	 var areaUnder9_00MRoadWideningArea = Number($('#areaUnder9_00MRoadWideningArea').val());
	 var areaUnder12_00MRoadWideningArea = Number($('#areaUnder12_00MRoadWideningArea').val());
	 var areaUnder15_00MRoadWideningArea = Number($('#areaUnder15_00MRoadWideningArea').val());
	 var areaUnder18_00MRoadWideningArea = Number($('#areaUnder18_00MRoadWideningArea').val());
	 var areaUnder24_00MRoadWideningArea =Number($('#areaUnder24_00MRoadWideningArea').val());
	 var areaUnder30_00MRoadWideningArea = Number($('#areaUnder30_00MRoadWideningArea').val());
	 var areaUnderServiceRoadArea = Number($('#areaUnderServiceRoadArea').val());
	 var areaUnderReservationArea = Number($('#areaUnderReservationArea').val());
	 var areaUnderGreenBeltArea = Number($('#areaUnderGreenBeltArea').val());
	 var areaUnderNalaArea = Number($('#areaUnderNalaArea').val());
	 
	 //3th
	 var internalRoadsArea = Number($('#internalRoadsArea').val());
	 var transformerArea = Number($('#transformerArea').val());
	 
	 var total2Area=areaUnderBRTCorridorArea+areaUnder1_50MRoadWideningArea+areaUnder3_00MRoadWideningArea+areaUnder4_50MRoadWideningArea+areaUnder6_00MRoadWideningArea + areaUnder7_50MRoadWideningArea + areaUnder9_00MRoadWideningArea + areaUnder12_00MRoadWideningArea + areaUnder15_00MRoadWideningArea + areaUnder18_00MRoadWideningArea + areaUnder24_00MRoadWideningArea + areaUnder30_00MRoadWideningArea + areaUnderServiceRoadArea + areaUnderReservationArea + areaUnderGreenBeltArea + areaUnderNalaArea 
	 
	 
	 var netGrossAreaOfPlot1_2Area=aeraOfPlotAsPer7_12ExtractArea-total2Area;
	 var recreationGround10Area=(netGrossAreaOfPlot1_2Area/100)*10;
	 var amenitySpace5Area=(netGrossAreaOfPlot1_2Area/100)*5;
	 
	 var total4Area=recreationGround10Area+amenitySpace5Area+internalRoadsArea+transformerArea;
	
	 //5th
	 var netAreaOfPlot3_4Area= netGrossAreaOfPlot1_2Area-total4Area;
		 
	 document.architecturalSectionForm.total2Area.value=total2Area;
	 document.architecturalSectionForm.netGrossAreaOfPlot1_2Area.value=netGrossAreaOfPlot1_2Area;
	 document.architecturalSectionForm.recreationGround10Area.value=recreationGround10Area;
	 document.architecturalSectionForm.amenitySpace5Area.value=amenitySpace5Area;
	 document.architecturalSectionForm.total4Area.value=total4Area;
	 document.architecturalSectionForm.netAreaOfPlot3_4Area.value=netAreaOfPlot3_4Area;
	 
	 //6th
	 var tdr40Area = Number($('#tdr40Area').val());
	 var additionsForFAR4a_RecreationGroundArea = Number($('#additionsForFAR4a_RecreationGroundArea').val());
	 var additionForFAR4b_AmenitySpaceArea = Number($('#additionForFAR4b_AmenitySpaceArea').val());
	 var additionForFAR4c_InternalsRoadArea = Number($('#additionForFAR4c_InternalsRoadArea').val());
	 var additionForFAR4d_TransformerArea = Number($('#additionForFAR4d_TransformerArea').val());
	 
	 var total6Area=tdr40Area+additionsForFAR4a_RecreationGroundArea+additionForFAR4b_AmenitySpaceArea+additionForFAR4c_InternalsRoadArea+additionForFAR4d_TransformerArea;
	 document.architecturalSectionForm.total6Area.value=total6Area;
	 
	 //7th
	 var totalArea5_6Area=netAreaOfPlot3_4Area+total6Area;
	 document.architecturalSectionForm.totalArea5_6Area.value=totalArea5_6Area;
	 
	 //8th
	 var farPermissibleArea = Number($('#farPermissibleArea').val());
	 
	 //9th
	 var totalPermissibleFloorArea7_8Area=farPermissibleArea*totalArea5_6Area;
	 document.architecturalSectionForm.totalPermissibleFloorArea7_8Area.value=totalPermissibleFloorArea7_8Area;
		
	 
	 
}

function CalculatetotalResidentialArea11_12Area()
{
	//11th 
	 var existingResidentialArea = Number($('#existingResidentialArea').val());
	 //12th
	 var proposedResidentialArea = Number($('#proposedResidentialArea').val());
	 
	 //13th
	 var totalResidentialArea11_12Area=existingResidentialArea+proposedResidentialArea;
	 document.architecturalSectionForm.totalResidentialArea11_12Area.value=totalResidentialArea11_12Area;
	
	 //15th
	 var existingCommercial_IndustrialArea = Number($('#existingCommercial_IndustrialArea').val());
	 //16th
	 var proposedCommercial_IndustrialArea = Number($('#proposedCommercial_IndustrialArea').val());
	
	//17th
	var totalCommercial_IndustrialArea15_16Area=existingCommercial_IndustrialArea+proposedCommercial_IndustrialArea;
	document.architecturalSectionForm.totalCommercial_IndustrialArea15_16Area.value=totalCommercial_IndustrialArea15_16Area;
		
	//18th 
	var totalExistingArea11_15= existingResidentialArea+existingCommercial_IndustrialArea;
	document.architecturalSectionForm.totalExistingArea11_15.value=totalExistingArea11_15;
	
   //19th
	var totalProposedArea12_16Area=proposedResidentialArea+proposedCommercial_IndustrialArea;
	document.architecturalSectionForm.totalProposedArea12_16Area.value=totalProposedArea12_16Area;
	
  //20th	
	var totalExistingArea_Proposed18_19Area=totalExistingArea11_15+totalProposedArea12_16Area;
	document.architecturalSectionForm.totalExistingArea_Proposed18_19Area.value=totalExistingArea_Proposed18_19Area;
	
  //21
  var excessBalconyAreaTakenInFSIArea=Number($('#excessBalconyAreaTakenInFSIArea').val());
  
  //22th
  var totalBupArea20_21Area=totalExistingArea_Proposed18_19Area+excessBalconyAreaTakenInFSIArea;
  document.architecturalSectionForm.totalBupArea20_21Area.value=totalBupArea20_21Area;
  
  
	
}

function CalculateexcessBalconyArea()
{
	 //For BALCONY AREA STATEMENT
	 //a
	 var permissibleBalconyArea = Number($('#permissibleBalconyArea').val());
	 
	 //b
	 var proposedBalconyArea = Number($('#proposedBalconyArea').val());
	
	 var excessBalconyArea=permissibleBalconyArea+proposedBalconyArea;
	 document.architecturalSectionForm.excessBalconyArea.value=excessBalconyArea;
	  
}

function CalculatetenenmentsPermissibleArea()
{
	//TENEMENT STATEMENT
	//a
	 var netGrossAreaOfPlot_A7_Area = Number($('#netGrossAreaOfPlot_A7_Area').val());
	 //b
	 var lessDeductionForNon_ResidentialArea = Number($('#lessDeductionForNon_ResidentialArea').val());
	 //c 
	 var areaOfTenementsa_bArea = netGrossAreaOfPlot_A7_Area-lessDeductionForNon_ResidentialArea;
	 document.architecturalSectionForm.areaOfTenementsa_bArea.value=areaOfTenementsa_bArea;
	 
	 //d
	 var tenenmentsPermissibleArea = Number($('#tenenmentsPermissibleArea').val());
	 
	 var totalTenementsProposedArea=areaOfTenementsa_bArea+tenenmentsPermissibleArea;
	 document.architecturalSectionForm.totalTenementsProposedArea.value=totalTenementsProposedArea;
		
}


function CalculateexcessGroundConveraged_bArea()
{
	//GROUND COVERAGE STATEMENT
	//a
	 var netPlotAreaArea = Number($('#netPlotAreaArea').val());
	 //b
	 var permissibleGroundCoverageArea = Number($('#permissibleGroundCoverageArea').val());
	 //c 
    var existingGroundCoverageArea = Number($('#existingGroundCoverageArea').val());
	 
	 //d
	 var proposedGroundCoverageArea = Number($('#proposedGroundCoverageArea').val());
	 //e
	 var permissibleGroundCoverageInPremium15Area=(permissibleGroundCoverageArea/100)*15;
	 document.architecturalSectionForm.permissibleGroundCoverageInPremium15Area.value=permissibleGroundCoverageInPremium15Area;
	//f
	var excessGroundConveraged_bArea=proposedGroundCoverageArea - permissibleGroundCoverageArea;
	document.architecturalSectionForm.excessGroundConveraged_bArea.value=excessGroundConveraged_bArea;
		
	 
}

function SetAllZero()
{
	// for per
	
	document.getElementById("aeraOfPlotAsPer7_12ExtractPer").value =0;
	document.getElementById("areaOfPlotAsPerPropetyPer").value =0;
	document.getElementById("areaOfPlotAsPerDemarcationPer").value =0;
	document.getElementById("areaAsPerSanctionedLayoutPer").value =0;
	document.getElementById("areaAsPerULCOrderPer").value =0;
	document.getElementById("areaAsPerDevelopmentAgreementPer").value =0;
	document.getElementById("areaOfPlotMinimumConsiderationPer").value =0;
	document.getElementById("areaUnderBRTCorridorPer").value =0;
	document.getElementById("areaUnder1_50MRoadWideningPer").value =0;
	document.getElementById("areaUnder3_00MRoadWideningPer").value =0;
	document.getElementById("areaUnder4_50MRoadWideningPer").value =0;
	document.getElementById("areaUnder6_00MRoadWideningPer").value =0;
	document.getElementById("areaUnder7_50MRoadWideningPer").value =0;
	document.getElementById("areaUnder9_00MRoadWideningPer").value =0;
	document.getElementById("areaUnder12_00MRoadWideningPer").value =0;
	document.getElementById("areaUnder15_00MRoadWideningPer").value =0;
	document.getElementById("areaUnder18_00MRoadWideningPer").value =0;
	document.getElementById("areaUnder24_00MRoadWideningPer").value =0;
	document.getElementById("areaUnder30_00MRoadWideningPer").value =0;
	document.getElementById("areaUnderServiceRoadPer").value =0;
	document.getElementById("areaUnderReservationPer").value =0;
	document.getElementById("areaUnderGreenBeltPer").value =0;
	document.getElementById("areaUnderNalaPer").value =0;
	
	
	document.getElementById("recreationGround10Per").value =0;
	document.getElementById("amenitySpace5Per").value =0;
	document.getElementById("internalRoadsPer").value =0;
	document.getElementById("transformerPer").value =0;
	
	document.getElementById("tdr40Per").value =0;
	document.getElementById("additionsForFAR4a_RecreationGroundPer").value =0;
	document.getElementById("additionForFAR4b_AmenitySpacePer").value =0;
	document.getElementById("additionForFAR4c_InternalsRoadPer").value =0;
	document.getElementById("additionForFAR4d_TransformerPer").value =0;
	
	document.getElementById("farPermissiblePer").value =0;
	
	document.getElementById("permissibleResidentialPer").value =0;
	document.getElementById("existingResidentialPer").value =0;
	document.getElementById("proposedResidentialPer").value =0;
	document.getElementById("permissibleCommercial_IndustrialPer").value =0;
	document.getElementById("existingCommercial_IndustrialPer").value =0;
	document.getElementById("proposedCommercial_IndustrialPer").value =0;
	document.getElementById("excessBalconyAreaTakenInFSIPer").value =0;
	document.getElementById("totalFSIConSumedPer").value =0;
	document.getElementById("permissibleBalconyPer").value =0;
	document.getElementById("proposedBalconyPer").value =0;
	document.getElementById("excessBalconyPer").value =0;
	document.getElementById("netGrossAreaOfPlot_A7_Per").value =0;
	document.getElementById("lessDeductionForNon_ResidentialPer").value =0;
	document.getElementById("tenenmentsPermissiblePer").value =0;
	document.getElementById("totalTenementsProposedPer").value =0;
	document.getElementById("netPlotAreaPer").value =0;
	document.getElementById("permissibleGroundCoveragePer").value =0;
	document.getElementById("existingGroundCoveragePer").value =0;
	document.getElementById("proposedGroundCoveragePer").value =0;
	document.getElementById("permissibleGroundCoverageInPremium15Per").value =0;

	
	// For area
	document.getElementById("aeraOfPlotAsPer7_12ExtractArea").value =0;
	document.getElementById("areaOfPlotAsPerPropetyArea").value =0;
	document.getElementById("areaOfPlotAsPerDemarcationArea").value =0;
	document.getElementById("areaAsPerSanctionedLayoutArea").value =0;
	document.getElementById("areaAsPerULCOrderArea").value =0;
	document.getElementById("areaAsPerDevelopmentAgreementArea").value =0;
	document.getElementById("areaOfPlotMinimumConsiderationArea").value =0;
	document.getElementById("areaUnderBRTCorridorArea").value =0;
	document.getElementById("areaUnder1_50MRoadWideningArea").value =0;
	document.getElementById("areaUnder3_00MRoadWideningArea").value =0;
	document.getElementById("areaUnder4_50MRoadWideningArea").value =0;
	document.getElementById("areaUnder6_00MRoadWideningArea").value =0;
	document.getElementById("areaUnder7_50MRoadWideningArea").value =0;
	document.getElementById("areaUnder9_00MRoadWideningArea").value =0;
	document.getElementById("areaUnder12_00MRoadWideningArea").value =0;
	document.getElementById("areaUnder15_00MRoadWideningArea").value =0;
	document.getElementById("areaUnder18_00MRoadWideningArea").value =0;
	document.getElementById("areaUnder24_00MRoadWideningArea").value =0;
	document.getElementById("areaUnder30_00MRoadWideningArea").value =0;
	document.getElementById("areaUnderServiceRoadArea").value =0;
	document.getElementById("areaUnderReservationArea").value =0;
	document.getElementById("areaUnderGreenBeltArea").value =0;
	document.getElementById("areaUnderNalaArea").value =0;
	document.getElementById("total2Area").value =0;
	document.getElementById("netGrossAreaOfPlot1_2Area").value =0;
	document.getElementById("recreationGround10Area").value =0;
	document.getElementById("amenitySpace5Area").value =0;
	document.getElementById("internalRoadsArea").value =0;
	document.getElementById("transformerArea").value =0;
	document.getElementById("total4Area").value =0;
	document.getElementById("netAreaOfPlot3_4Area").value =0;
	document.getElementById("tdr40Area").value =0;
	document.getElementById("additionsForFAR4a_RecreationGroundArea").value =0;
	document.getElementById("additionForFAR4b_AmenitySpaceArea").value =0;
	document.getElementById("additionForFAR4c_InternalsRoadArea").value =0;
	document.getElementById("additionForFAR4d_TransformerArea").value =0;
	document.getElementById("total6Area").value =0;
	document.getElementById("totalArea5_6Area").value =0;
	document.getElementById("farPermissibleArea").value =0;
	document.getElementById("totalPermissibleFloorArea7_8Area").value =0;
	document.getElementById("permissibleResidentialArea").value =0;
	document.getElementById("existingResidentialArea").value =0;
	document.getElementById("proposedResidentialArea").value =0;
	document.getElementById("totalResidentialArea11_12Area").value =0;
	document.getElementById("permissibleCommercial_IndustrialArea").value =0;
	document.getElementById("existingCommercial_IndustrialArea").value =0;
	document.getElementById("proposedCommercial_IndustrialArea").value =0;
	document.getElementById("totalCommercial_IndustrialArea15_16Area").value =0;
	document.getElementById("totalExistingArea11_15").value =0;
	document.getElementById("totalProposedArea12_16Area").value =0;
	document.getElementById("totalExistingArea_Proposed18_19Area").value =0;
	document.getElementById("excessBalconyAreaTakenInFSIArea").value =0;
	document.getElementById("totalBupArea20_21Area").value =0;
	document.getElementById("totalFSIConSumedArea").value =0;
	document.getElementById("permissibleBalconyArea").value =0;
	document.getElementById("proposedBalconyArea").value =0;
	document.getElementById("excessBalconyArea").value =0;
	document.getElementById("netGrossAreaOfPlot_A7_Area").value =0;
	document.getElementById("lessDeductionForNon_ResidentialArea").value =0;
	document.getElementById("areaOfTenementsa_bArea").value =0;
	document.getElementById("tenenmentsPermissibleArea").value =0;
	document.getElementById("totalTenementsProposedArea").value =0;
	document.getElementById("netPlotAreaArea").value =0;
	document.getElementById("permissibleGroundCoverageArea").value =0;
	document.getElementById("existingGroundCoverageArea").value =0;
	document.getElementById("proposedGroundCoverageArea").value =0;
	document.getElementById("permissibleGroundCoverageInPremium15Area").value =0;
	document.getElementById("excessGroundConveraged_bArea").value =0;


}
function validate()
{
	if(document.architecturalSectionForm.consultancyId.value=="Default")
	{
		$('#consultancyIdSpan').html('Please, select architectural name..!');
		document.architecturalSectionForm.consultancyId.focus();
		return false;
	}
}
function init()
{
	//clearall();
	SetAllZero();	
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	

}



</script>
</body>
</html>


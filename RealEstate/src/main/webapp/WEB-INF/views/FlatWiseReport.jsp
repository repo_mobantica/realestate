<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Flat Wise Report</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
 
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

    <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Flat Wise Report Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Flat Wise Report</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="flatmasterform" action="${pageContext.request.contextPath}/FlatWiseReport" target="_blank" onsubmit="return validate()" method="post">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
        
          <div class="box-body">
          <div class="row">
               <div class="col-md-3">
                  <label>Project</label><label class="text-red">* </label>
                  
                  <select class="form-control" name="projectId" id="projectId" onchange="getBuldingList(this.value)">
                  	 <option selected="" value="Default">-Select Project-</option>
                  	  <c:forEach var="projectList" items="${projectList}">
                    	<option value="${projectList.projectId}">${projectList.projectName}</option>
				     </c:forEach>
				  </select>
				  <span id="projectIdSpan" style="color:#FF0000"></span>
                  </div>
                 <div class="col-xs-3">
			      <label>Project Building Name  </label> <label class="text-red">* </label>
                  <select class="form-control" name="buildingId" id="buildingId" onchange="getWingNameList(this.value)">
				  		<option selected="" value="Default">-Select Project Building-</option>
                  
                  </select>
			     </div> 
                <div class="col-xs-3">
			      <label>Wing </label> <label class="text-red">* </label>
              <select class="form-control" name="wingId" id="wingId" onchange="getFloorNameList(this.value)">
				 	 	<option selected="" value="Default">-Select Wing Name-</option>
                    <c:forEach var="projectwingList" items="${projectwingList}">
                    	<option value="${projectwingList.wingId}">${projectwingList.wingId}</option>
				     </c:forEach>
                  </select>
                 </div> 
                     <div class="col-xs-3">
			      <label>Floor </label> <label class="text-red">* </label>
              <select class="form-control" name="floorId" id="floorId" onchange="getFloorWiseFlatList(this.value)">
				 	 	<option selected="" value="Default">-Select Floor Name-</option>
                    <c:forEach var="floortypeList" items="${floortypeList}">
                    	<option value="${floortypeList.floorId}">${floortypeList.floorId}</option>
				     </c:forEach>
                  </select>
			     </div>
                  </div>
                </div>
                
				
					
				
              <!-- /.form-group -->
            </div>
            
            <!-- /.col -->
			
          </div>
		  	     <div class="box-body">
                <div class="row">
                    <br/>
                    
					<div class="col-xs-3">
				
            	 	</div>
				    
					<div class="col-xs-3">
			 			&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 
			  			<a href="FlatWiseReport">  <button type="button" class="btn btn-default" value="reset" style="width:90px"> Reset</button></a>
              		</div> 
              		
              		<div class="col-xs-3" id="submitBlock">
			      		<button type="submit" class="btn btn-success"><i class="fa fa-print"></i>&nbsp Print</button>
            		</div>
            		
			  	</div>
			</div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        
      </div>
      <!-- /.box -->
				
				<div class="box box-default">
        <div class="box-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="flatListTable">
                  <thead>
                  <tr bgcolor=#4682B4>
                     	<th style="width:10px">Sr</th>
                        <th>Flat No.</th>
	                    <th>Project Name</th>
	                    <th>Building Name</th>
	                    <th>Wing Name</th>
	                    <th>Facing</th>
                    	<th>Type</th>
                    	<th>Area(Sq.Ft.)</th>
                    	<th>Cost</th>
                    	<th>Status</th>
                  </tr>
                  </thead>
                  <tbody>
                   <c:forEach items="${flatList}" var="flatList" varStatus="loopStatus">
                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                            <td>${loopStatus.index+1}</td>
                            <td>${flatList.flatNumber}</td>
	                        <td>${flatList.projectId}</td>
	                        <td>${flatList.buildingId}</td>
	                        <td>${flatList.wingId}</td>
	                        <td>${flatList.flatfacingName}</td>
	                        <td>${flatList.flatType}</td>
	                        <td>${flatList.flatAreawithLoadingInFt}</td>
	                        <td>${flatList.flatbasicCost}</td>
	                        <td>${flatList.flatstatus}</td>
                      </tr>
					</c:forEach>
                 
                 </tbody>
                </table>
              </div>
            </div>
				
              <!-- /.form-group -->
          		<input type="hidden" id="flatdbStatus" name="flatdbStatus" value="${flatdbStatus}">
          		
        <!-- /.box-body -->
          
      </div>
     
    </section>
	</form>
    <!-- /.content -->
    
  </div>
 
  <!-- Control Sidebar -->
     <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
   
<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script>

function init()
{
   
}

function validate()
{
	$('#projectIdSpan').html('');
	var projectId   = $('#projectId').val();
	var buildingId  = $('#buildingId').val();
	var wingId      = $('#wingId').val();
	var floorId = $('#floorId').val();
	
	if(projectId=="Default")
	{
		$('#projectIdSpan').html('Please, select at least project..!');
		return false;
	}
}

function getBuldingList()
{
	 $("#buildingId").empty();
	 var projectId = $('#projectId').val();

	 $.ajax({

		 url : '${pageContext.request.contextPath}/getBuildingList',
		type : 'Post',
		data : { projectId : projectId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Building Name-");
							$("#buildingId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].buildingId).text(result[i].buildingName);
							    $("#buildingId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	 

	 
	 
		$("#flatListTable tr").detach();
		 var projectId = $('#projectId').val();
		 //$("#buildingId").empty();
		$.ajax({
			//alert("hjhjh");

			url : '${pageContext.request.contextPath}/getProjectWiseFlatList',
			type : 'Post',
			data : { projectId : projectId},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{ 
										$('#flatListTable').append('<tr style="background-color: #4682B4;"><th style="width:10px">Sr No</th><th >Flat Number</th><th>Project Name</th> <th>Building Name</th> <th>Wing Name</th><th>Facing</th><th>Type</th><th>Area(Sq.Ft.)</th><th>Cost</th><th>Status</th>');
							
								for(var i=0;i<result.length;i++)
								{ 
									if(i%2==0)
									{
										$('#flatListTable').append('<tr style="background-color: #F0F8FF;"><td>'+(i+1)+'</td><td>'+result[i].flatNumber+'</td><td>'+result[i].projectId+'</td><td>'+result[i].buildingId+'</td><td>'+result[i].wingId+'</td><td>'+result[i].flatfacingName+'</td><td>'+result[i].flatType+'</td><td>'+result[i].flatAreawithLoadingInFt+'</td><td>'+result[i].flatbasicCost+'</td><td>'+result[i].flatstatus+'</td>');
									}
									else
									{
										$('#flatListTable').append('<tr style="background-color: #CCE5FF;"><td>'+(i+1)+'</td><td>'+result[i].flatNumber+'</td><td>'+result[i].projectId+'</td><td>'+result[i].buildingId+'</td><td>'+result[i].wingId+'</td><td>'+result[i].flatfacingName+'</td><td>'+result[i].flatType+'</td><td>'+result[i].flatAreawithLoadingInFt+'</td><td>'+result[i].flatbasicCost+'</td><td>'+result[i].flatstatus+'</td>');
									}
								
								 } 
							} 
							else
							{
								alert("failure111");
								//$("#ajax_div").hide();
							}

						}
			});
		
	
}//end of get Building List



function getWingNameList()
{
	 $("#wingId").empty();
	 var buildingId = $('#buildingId').val();
	 var projectId = $('#projectId').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getprojectwingList',
		type : 'Post',
		data : { buildingId : buildingId, projectId : projectId },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Wing Name-");
							$("#wingId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].wingId).text(result[i].wingName);
							    $("#wingId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
	 
		$("#flatListTable tr").detach();
		 var buildingId = $('#buildingId').val();
		 var projectId = $('#projectId').val();
		 //$("#floorId").empty();
		$.ajax({
			//alert("hjhjh");

			url : '${pageContext.request.contextPath}/getBuildingWiseFlatList',
			type : 'Post',
			data : { buildingId : buildingId , projectId : projectId},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{ 
								$('#flatListTable').append('<tr style="background-color: #4682B4;"><th style="width:10px">Sr No</th><th >Flat Number</th><th>Project Name</th> <th>Building Name</th> <th>Wing Name</th><th>Facing</th><th>Type</th><th>Area(Sq.Ft.)</th><th>Cost</th><th>Status</th>');
							
								for(var i=0;i<result.length;i++)
								{ 
									if(i%2==0)
									{
										$('#flatListTable').append('<tr style="background-color: #F0F8FF;"><td>'+(i+1)+'</td><td>'+result[i].flatNumber+'</td><td>'+result[i].projectId+'</td><td>'+result[i].buildingId+'</td><td>'+result[i].wingId+'</td><td>'+result[i].flatfacingName+'</td><td>'+result[i].flatType+'</td><td>'+result[i].flatAreawithLoadingInFt+'</td><td>'+result[i].flatbasicCost+'</td><td>'+result[i].flatstatus+'</td>');
									}
									else
									{
										$('#flatListTable').append('<tr style="background-color: #CCE5FF;"><td>'+(i+1)+'</td><td>'+result[i].flatNumber+'</td><td>'+result[i].projectId+'</td><td>'+result[i].buildingId+'</td><td>'+result[i].wingId+'</td><td>'+result[i].flatfacingName+'</td><td>'+result[i].flatType+'</td><td>'+result[i].flatAreawithLoadingInFt+'</td><td>'+result[i].flatbasicCost+'</td><td>'+result[i].flatstatus+'</td>');
									}
								
								 } 
							} 
							else
							{
								alert("failure111");
							}

						}
			});
		
}


function getFloorNameList()
{
	 $("#floorId").empty();
	 var wingId = $('#wingId').val();
	 var buildingId = $('#buildingId').val();
	 var projectId = $('#projectId').val();
	 
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getwingfloorNameList',
		type : 'Post',
		data : {wingId : wingId, buildingId : buildingId, projectId : projectId },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Floor Name-");
							$("#floorId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].floorId).text(result[i].floortypeName);
							    $("#floorId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	 
	 
	 $("#flatListTable tr").detach();
	 var wingId = $('#wingId').val();
	 var buildingId = $('#buildingId').val();
	 var projectId = $('#projectId').val();
	 //$("#floorId").empty();
	$.ajax({
		//alert("hjhjh");

		url : '${pageContext.request.contextPath}/getWingWiseFlatList',
		type : 'Post',
		data : {wingId : wingId, buildingId : buildingId , projectId : projectId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
							$('#flatListTable').append('<tr style="background-color: #4682B4;"><th style="width:10px">Sr No</th><th >Flat Number</th><th>Project Name</th> <th>Building Name</th> <th>Wing Name</th><th>Facing</th><th>Type</th><th>Area(Sq.Ft.)</th><th>Cost</th><th>Status</th>');
							
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									$('#flatListTable').append('<tr style="background-color: #F0F8FF;"><td>'+(i+1)+'</td><td>'+result[i].flatNumber+'</td><td>'+result[i].projectId+'</td><td>'+result[i].buildingId+'</td><td>'+result[i].wingId+'</td><td>'+result[i].flatfacingName+'</td><td>'+result[i].flatType+'</td><td>'+result[i].flatAreawithLoadingInFt+'</td><td>'+result[i].flatbasicCost+'</td><td>'+result[i].flatstatus+'</td>');
								}
								else
								{
									$('#flatListTable').append('<tr style="background-color: #CCE5FF;"><td>'+(i+1)+'</td><td>'+result[i].flatNumber+'</td><td>'+result[i].projectId+'</td><td>'+result[i].buildingId+'</td><td>'+result[i].wingId+'</td><td>'+result[i].flatfacingName+'</td><td>'+result[i].flatType+'</td><td>'+result[i].flatAreawithLoadingInFt+'</td><td>'+result[i].flatbasicCost+'</td><td>'+result[i].flatstatus+'</td>');
								}
							
							 }  
						} 
						else
						{
							alert("failure111");
						}

					}
		});
}

function getFloorWiseFlatList()
{
	 
	 
	 $("#flatListTable tr").detach();
	 var floorId = $('#floorId').val();
	 var wingId = $('#wingId').val();
	 var buildingId = $('#buildingId').val();
	 var projectId = $('#projectId').val();
	 //$("#floorId").empty();
	$.ajax({
		//alert("hjhjh");

		url : '${pageContext.request.contextPath}/getFloorWiseAllFlatList',
		type : 'Post',
		data : {floorId : floorId, wingId : wingId, buildingId : buildingId , projectId : projectId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
							$('#flatListTable').append('<tr style="background-color: #4682B4;"><th style="width:10px">Sr No</th><th >Flat Number</th><th>Project Name</th> <th>Building Name</th> <th>Wing Name</th><th>Facing</th><th>Type</th><th>Area(Sq.Ft.)</th><th>Cost</th><th>Status</th>');
							
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									$('#flatListTable').append('<tr style="background-color: #F0F8FF;"><td>'+(i+1)+'</td><td>'+result[i].flatNumber+'</td><td>'+result[i].projectId+'</td><td>'+result[i].buildingId+'</td><td>'+result[i].wingId+'</td><td>'+result[i].flatfacingName+'</td><td>'+result[i].flatType+'</td><td>'+result[i].flatAreawithLoadingInFt+'</td><td>'+result[i].flatbasicCost+'</td><td>'+result[i].flatstatus+'</td>');
								}
								else
								{
									$('#flatListTable').append('<tr style="background-color: #CCE5FF;"><td>'+(i+1)+'</td><td>'+result[i].flatNumber+'</td><td>'+result[i].projectId+'</td><td>'+result[i].buildingId+'</td><td>'+result[i].wingId+'</td><td>'+result[i].flatfacingName+'</td><td>'+result[i].flatType+'</td><td>'+result[i].flatAreawithLoadingInFt+'</td><td>'+result[i].flatbasicCost+'</td><td>'+result[i].flatstatus+'</td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
}

function setFlatId()
{
	
	   var tbl = document.getElementById("flatListTable");

	   var cell_value;
	   
         if (tbl != null)
         {

            for (var i = 0; i < tbl.rows.length; i++) 
            {

               for (var j = 0; j < tbl.rows[i].cells.length; j++)

                   tbl.rows[i].cells[0].onclick = function ()
                   {
            	   		getval(this); 
            	   };
             }
         }
}


function getval(cel) 
{

    var flatId = cel.innerHTML;
    
    document.getElementById("flatId").value = flatId;
    
    document.forms["flatmasterform"].submit();
}


function SearchFlatByNumber()
{
	$("#flatListTable tr").detach();
	 var flatNumber = $('#flatNumber').val();

	 $.ajax({

		url : '/SearchFlatByNumber',
		type : 'Post',
		data : { flatNumber : flatNumber},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
							$('#flatListTable').append('<tr style="background-color: #4682B4;"><th style="width:10px">Sr No</th><th >Flat Number</th><th>Project Name</th> <th>Building Name</th> <th>Wing Name</th><th>Facing</th><th>Type</th><th>Area(Sq.Ft.)</th><th>Cost</th><th>Status</th>');
							
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									$('#flatListTable').append('<tr style="background-color: #F0F8FF;"><td>'+(i+1)+'</td><td>'+result[i].flatNumber+'</td><td>'+result[i].projectId+'</td><td>'+result[i].buildingId+'</td><td>'+result[i].wingId+'</td><td>'+result[i].flatfacingName+'</td><td>'+result[i].flatType+'</td><td>'+result[i].flatAreawithLoadingInFt+'</td><td>'+result[i].flatbasicCost+'</td><td>'+result[i].flatstatus+'</td>');
								}
								else
								{
									$('#flatListTable').append('<tr style="background-color: #CCE5FF;"><td>'+(i+1)+'</td><td>'+result[i].flatNumber+'</td><td>'+result[i].projectId+'</td><td>'+result[i].buildingId+'</td><td>'+result[i].wingId+'</td><td>'+result[i].flatfacingName+'</td><td>'+result[i].flatType+'</td><td>'+result[i].flatAreawithLoadingInFt+'</td><td>'+result[i].flatbasicCost+'</td><td>'+result[i].flatstatus+'</td>');
								}
							
							 }  
						} 
						else
						{
							alert("failure111");
						}
					}
		});
}

$(function () {
    $('#flatListTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
  
</script>
</body>
</html>

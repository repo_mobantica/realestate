<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Real Estate | Add Company Documents</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


<style type="text/css">
tr.odd {
	background-color: #CCE5FF
}

tr.even {
	background-color: #F0F8FF
}
</style>

<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#statusSpan').delay(1000).fadeOut();
	});
</script>
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control", "no-cache,no-store,must-revalidate");//HTTP 1.1
		response.setHeader("Pragma", "no-cache"); //HTTP 1.0
		response.setDateHeader("Expires", 0);

		if (session != null) {
			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null
					|| session.getAttribute("profile_img") == null) {
				response.sendRedirect("login");
			}
		}
	%>

	<div class="wrapper">

		<%@ include file="headerpage.jsp"%>
		<!-- Left side column. contains the logo and sidebar -->
		<%@ include file="menu.jsp"%>
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Add Company Documents : <small>Preview</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
					<li><a href="#">Company Document Master</a></li>
					<li class="active">Add Company Documents</li>
				</ol>
			</section>

			<!-- Main content -->

			<form name="companydocumentform" action="${pageContext.request.contextPath}/uploadCompanyDocument" method="post" onSubmit="return UploadDocument()" enctype="multipart/form-data">
				<section class="content">

					<!-- SELECT2 EXAMPLE -->
					<div class="box box-default">
						<div class="box-body">
							<div class="row">
								<div class="col-md-12">
									<span id="statusSpan" style="color: #008000"></span>

									<div class="box-body">
										<div class="row">

											<div class="col-xs-3"></div>

											<div class="col-xs-3">
												<label for="documentNumber">Document Number/Caption</label>
												<input type="hidden" class="form-control" id="companyId" name="companyId" value="${companyId}">
												<input type="text" class="form-control" id="documentNumber" placeholder="Document Number Or Caption" name="documentNumber" style="text-transform: capitalize;">
												<span id="documentNumberSpan" style="color: #FF0000"></span>
											</div>

											<div class="col-xs-3">
												<div class="form-group">

													<label>Select File</label>
													<div class="input-group">
														<div class="input-group-addon">
															<i class="fa fa-file"></i>
														</div>
														  <input type="file" id="document" name="document" accept="application/pdf,image/x-png,image/gif,image/jpeg" onchange="return getFileExtension()">
														<span class="input-group-btn">
														  <!-- <input type="submit" class="btn btn-success" value=""><i class="fa fa-upload"></i>&nbspUpload</button> -->
														  <button type="submit" class="btn btn-success" name="submit"><i class="fa fa-upload"></i>&nbspUpload</button>
														</span>
													</div>
													<p class="help-block">Only select PDF, PNG, JPEG, and JPG formats(with size maximum 500KB).</p>
													<span id="documentMsgSpan" style="color: #FF0000"></span>
												</div>
											</div>

										</div>

									</div>
								</div>

								<div class="box-body">
									<div class="row">
										<div class="col-xs-2"></div>
										<div class="col-xs-8">
											<div class="table-responsive">
												<table class="table table-bordered" id="companyDocumentListTable">
													<thead>
														<tr bgcolor=#4682B4>
															<th style="width: 10px">Sr.No</th>
															<th style="width: 150px">Document Name/Number</th>
															<th style="width: 50px">View Documents</th>
														</tr>
													</thead>
													<tbody>

														<s:forEach items="${documentList}" var="documentList" varStatus="loopStatus">
															<tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
																<td>${loopStatus.index+1}</td>
																<td>${documentList.documentNumber}</td>
																<td>
																    <a target="_blank" href="${pageContext.request.contextPath}/ViewCompanyDocument?documentId=${documentList.documentId}" class="btn btn-info btn-sm" data-toggle="tooltip" title="View Documents"><i class="glyphicon glyphicon-eye-open"></i></a>
																   |<a onclick="DeleteCompanyDocument('${documentList.documentId}')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete Documents"><i class="glyphicon glyphicon-remove"></i></a>
																</td>
															</tr>
														</s:forEach>

													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>

								<div class="box-body">
									<div class="row">
									    <input type="hidden" id="statusSpan" name="statusSpan" value="${documentStatus}">	
				 						<input type="hidden" id="creationDate" name="creationDate" value="">
				 						<input type="hidden" id="updateDate" name="updateDate" value="">
				 						<input type="hidden" id="userName" name="userName" value="">
									</div>
								</div>

								<div class="box-body">
									<div class="row">

										<div class="col-xs-3"></div>
										<div class="col-xs-2">
											<a href="CompanyDocumentMaster"><button type="button" class="btn btn-block btn-primary" value="reset" style="width: 90px">Back</button></a>
										</div>

										<div class="col-xs-2">
											<button type="reset" class="btn btn-default" value="reset" style="width: 90px">Reset</button>
										</div>

										<div class="col-xs-3">
											<a href="CompanyDocumentMaster"><button type="button" class="btn btn-success" value="Submit" style="width: 90px">Submit</button></a>
										</div>

									</div>
								</div>

							</div>
						</div>
					</div>

				</section>
			</form>
		</div>

		<%@ include file="footer.jsp"%>
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
	<!-- Page script -->

	<script>
	
	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function(e) {
				$('#document-tag').attr('src', e.target.result);
			}
			reader.readAsDataURL(input.files[0]);
		}
	}
	$("#document").change(function() {
		readURL(this);
	});
	
	
		function clearall()
		{
			$('#documentNumberSpan').html('');
			$('#documentMsgSpan').html('');
		}

		function getFileExtension()
		{
		   clearall();
		   
		   var fileName = $('#document').val();
		   var flag = 0;
		   
		   $.ajax({

				url : '${pageContext.request.contextPath}/checkDocumentFormat',
				type : 'Post',
				data : { fileName : fileName },
				dataType : 'text',
				success : function(result)
						  {
								if (result == "OK") 
								{
									flag = 1;
								} 
								else
								{
									resetFilePath();
									flag = 0;
								}
						  }
				});
		}
		
		function resetFilePath()
		{
			$('#documentMsgSpan').html('Please Select Only PDF File Or Image..!');
		    document.companydocumentform.document.value="";
			document.companydocumentform.document.focus();
		}
		
		function UploadDocument()
		{
			clearall();

			if (document.companydocumentform.documentNumber.value == "")
			{
				$('#documentNumberSpan').html('Document name or number should not be blank..!');
				document.companydocumentform.documentNumber.focus();
				return false;
			} 
			else if (document.companydocumentform.documentNumber.value.match(/^[\s]+$/))
			{
				$('#documentNumberSpan').html('Document name or number should not be blank..!');
				document.companydocumentform.documentNumber.value = "";
				document.companydocumentform.documentNumber.focus();
				return false;
			}

			if (document.companydocumentform.document.value == "")
			{
				$('#documentMsgSpan').html('Please select a document to upload..!');
				document.companydocumentform.document.value="";
				document.companydocumentform.document.focus();
				return false;
			} 
			
			/* var companyId      = $('#companyId').val();
			var documentNumber = $('#documentNumber').val();
			var creationDate   = $('#creationDate').val();
			var updateDate     = $('#updateDate').val();
			
			var form		   = $('#companydocumentform')[0];
			var file	       = new FormData(form);
			
			
			$("#companyDocumentListTable tr").detach();
			
			$.ajax({

				url : '${pageContext.request.contextPath}/uploadCompanyDocument?'+ $.param({companyId :companyId, documentNumber : documentNumber, creationDate : creationDate, updateDate : updateDate}),
				type : 'Post',
				data : file,
				dataType: 'json',
				enctype : 'multipart/form-data',
				processData: false,
	            contentType: false,
	            cache: false,
	            timeout: 600000,
				success : function(result)
						  {
								alert("Object = "+file);
										$('#companyDocumentListTable').append('<tr style="background-color: #4682B4;"><th>Sr.No.</th> <th>Document Name/Number</th><th>Action</th></tr>');
								
								for(var i=0;i<result.length;i++)
								{ 
									if(i%2==0)
									{
										var id = result[i].documentId;
										$('#companyDocumentListTable').append('<tr style="background-color: #F0F8FF;"><td>'+(i+1)+'</td><td>'+result[i].documentNumber+'</td><td><a href="${pageContext.request.contextPath}/ViewCompanyDocument?documentId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="View"><i class="glyphicon glyphicon-eye-open"></i></a>|<a onclick="DeleteCompanyBank(\''+id+'\')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td></tr>');
									}
									else
									{
										var id = result[i].documentId;
										$('#companyDocumentListTable').append('<tr style="background-color: #F0F8FF;"><td>'+(i+1)+'</td><td>'+result[i].documentNumber+'</td><td><a href="${pageContext.request.contextPath}/ViewCompanyDocument?documentId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="View"><i class="glyphicon glyphicon-eye-open"></i></a>|<a onclick="DeleteCompanyBank(\''+id+'\')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td></tr>');
									}
								
								 }
						  }
				}); */
		}
		
		function DeleteCompanyDocument(documentId)
		{
			var companyId  = $('#companyId').val();

			$("#companyDocumentListTable tr").detach();
			
			$.ajax({

				url : '${pageContext.request.contextPath}/DeleteCompanyDocument',
				type : 'Post',
				data : { documentId : documentId, companyId :companyId},
				dataType: 'json',
				success : function(result)
						  {
										$('#companyDocumentListTable').append('<tr style="background-color: #4682B4;"><th>Sr.No.</th> <th>Document Name/Number</th><th>Action</th></tr>');
								
								for(var i=0;i<result.length;i++)
								{ 
									if(i%2==0)
									{
										var id = result[i].documentId;
										$('#companyDocumentListTable').append('<tr style="background-color: #F0F8FF;"><td>'+(i+1)+'</td><td>'+result[i].documentNumber+'</td><td><a href="${pageContext.request.contextPath}/ViewCompanyDocument?documentId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="View"><i class="glyphicon glyphicon-eye-open"></i></a>|<a onclick="DeleteCompanyDocument('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td></tr>');
									}
									else
									{
										var id = result[i].documentId;
										$('#companyDocumentListTable').append('<tr style="background-color: #CCE5FF;"><td>'+(i+1)+'</td><td>'+result[i].documentNumber+'</td><td><a href="${pageContext.request.contextPath}/ViewCompanyDocument?documentId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="View"><i class="glyphicon glyphicon-eye-open"></i></a>|<a onclick="DeleteCompanyDocument('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td></tr>');
									}
								
								 }
						  }
				});
		}

		
		function init() 
		{
			clearall();
			var date  = new Date();
			var year  = date.getFullYear();
			var month = date.getMonth() + 1;
			var day   = date.getDate();

			document.getElementById("creationDate").value = day + "/" + month + "/" + year;
			document.getElementById("updateDate").value = day + "/" + month + "/" + year;

			if (document.companydocumentform.statusSpan.value == "Fail") 
			{
				//	alert("Sorry, record is present already..!");
			}
			else if (document.companydocumentform.statusSpan.value == "Success") 
			{
				$('#statusSpan').html('Document Uploaded successfully..!');
			}

			document.companydocumentform.documentNumber.focus();
	   }
		
	</script>
</body>
</html>

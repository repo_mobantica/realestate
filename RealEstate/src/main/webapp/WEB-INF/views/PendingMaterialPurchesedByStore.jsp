<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>RealEstate | Material List</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

 
  <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">
	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

    <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
    <section class="content-header">
      <h1>
        Material List:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Store</a></li>
        <li class="active"> Material List</li>
      </ol>
    </section>
    <!-- Main content -->
	<form name="storeStockform" id="storeStockform" action="${pageContext.request.contextPath}/PendingMaterialPurchesedByStore" onSubmit="return validate()" method="post">


<section class="content">
   
      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
            <div class="panel box box-danger"></div>
        <span id="statusSpan" style="color:#FF0000"></span>
   
                  <div class="box-body">
                     <div class="row">
                     
                      <div class="col-xs-2">
				      <label for="storeId">Store Id </label>
				      
     			      <input type="hidden" class="form-control" id="status" name="status" value="" readonly>
     			      
				      <input type="text" class="form-control" id="storeId" name="storeId" value="${materialpurchesDetails[0].storeId}" readonly>
     			      <input type="hidden" class="form-control" id="storeStockHistoriId" name="storeStockHistoriId" value="${storeStockHistoriId}" readonly>
     			      
     			      </div>  
     		
     				 <div class="col-xs-2">
				      <label for="grnNo">GRN No. </label><label class="text-red">* </label>
     			      <input type="text" class="form-control" id="grnNo" name="grnNo" value="${grnNo}" readonly>
     			     <span id="grnNoSpan" style="color:#FF0000"></span>
     			     </div>
     			         
     				 <div class="col-xs-2">
				      <label for="challanNo">Challan No. </label><label class="text-red">* </label>
		 		     <input type="text" class="form-control" id="challanNo" name="challanNo" placeholder="Enter challan Number">
     			     <span id="challanNoSpan" style="color:#FF0000"></span>
     			     </div>
     				 <div class="col-xs-2">
				      <label for="ewayBillNo">Eway Bill No. </label>
		 		     <input type="text" class="form-control" id="ewayBillNo" name="ewayBillNo" placeholder="Enter Eway Bill Number">
     			     <span id="ewayBillNoSpan" style="color:#FF0000"></span>
     			     </div>
     			               
     			 </div>
             </div> 
             
                  <div class="box-body">
                     <div class="row">
                     
     			      <div class="col-xs-2">
				      <label for="vehicleNumber">Vehicle No </label>
		 		     <input type="text" class="form-control" id="vehicleNumber" name="vehicleNumber" placeholder="Enter Vehicle Number">
     			     <span id="vehicleNumberSpan" style="color:#FF0000"></span>
     			     </div>  
     			     
     			      <div class="col-xs-2">
				      <label for="loadedWeight">Loaded Wt. </label>
		 		     <input type="text" class="form-control" id="loadedWeight" name="loadedWeight" placeholder="Enter loaded Weight" onchange="getTotalMaterialWeight(this.value)">
     			     <span id="loadedWeightSpan" style="color:#FF0000"></span>
     			     </div>  
     			     
     			      <div class="col-xs-2">
				      <label for="unloadedWeight">UnLoaded Wt. </label>
		 		      <input type="text" class="form-control" id="unloadedWeight" name="unloadedWeight" placeholder="Enter Unloaded Weight" onchange="getTotalMaterialWeight(this.value)">
     			       <span id="unloadedWeightSpan" style="color:#FF0000"></span>
     			      </div>  
     			      
     			    <div class="col-xs-2">
				     <label for="totalMaterialWeight">Total Material Wt</label>
		 		     <input type="text" class="form-control" id="totalMaterialWeight" name="totalMaterialWeight" placeholder="Enter Total Material Weight " readonly>
     			     <span id="totalMaterialWeightSpan" style="color:#FF0000"></span>
     			    </div> 
     			       
     			                 
     			 </div>
             </div> 
             
                  <div class="box-body">
                     <div class="row">
                     
     			    <div class="col-xs-6">
				     <h4><label> Previous Challan No : ${challanNo}</label></h4>
     			    </div> 
     			       
                         
     			 </div>
             </div> 
        <br/>
        
      
      
          <div class="box-body">
          <div class="panel box box-danger"></div>
           <div class="row"> 
                  
        	<div class="col-xs-12">
        	<h2 class="box-title">Material List</h2>
              <table id="materialDetailsTable" class="table table-bordered">
                <thead>
                <tr bgcolor="#4682B4">
               		<th >Sr.No</th>
               		<th  hidden="hidden">storeStockHistoriId</th>
               		<th>Item Id</th>
                  	<th>Item Sub-Category</th>
                  	<th>Item Name</th>
                  	<th>Size</th>
                  	<th>Unit</th>
                    <th>Brand</th>
                    <th>Qty</th>
                    <th>Previous Qty</th>
                    <th>Actual Qty</th>
                    <th>Previous Pieces</th>
                    <th>No. of Pieces</th>
                    <th>Remark</th>
                    <th hidden="hidden"></th>
                  </tr>
                </thead>
                <tbody>
                
                 <c:forEach items="${materialDetails}" var="materialDetails" varStatus="loopStatus">
                    <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                      <td>${loopStatus.index+1}</td>
                      <td  hidden="hidden">${materialDetails.storeStockHistoriId}</td>
                      <td >${materialDetails.itemId}</td>
                      <td>${materialDetails.subsuppliertypeName}</td>
                      <td>${materialDetails.itemName}</td>
                      <td>${materialDetails.itenSize}</td>
                      <td>${materialDetails.itemUnit}</td>
                      <td>${materialDetails.itemBrandName}</td>
                      <td>${materialDetails.itemQuantity}</td>
                      <td>${materialDetails.previousQuantity}</td>
                      <td contenteditable='true'></td>
                      <td>${materialDetails.previousNoOfPieces}</td>
                      <td contenteditable='true'></td>
                      <td contenteditable='true'></td>
                      <td hidden="hidden">${materialDetails.materialPurchesHistoriId}</td>
                   </tr>
				 </c:forEach>
		
                </tbody>
                	
              </table>
               
            </div>
            
            <div class="col-xs-6">
            </div>
           <div class="col-xs-6">
           <span id="checkButtonSpan" style="color:#FF0000"></span>
           </div>
		  
            
            <br/>
          
		  <div class="col-xs-1">
         </div>
         
	       <div class="col-xs-4">
	       <div class="col-xs-4">
              <a href="StoreManagement"><button type="button" class="btn btn-block btn-primary" value="reset" style="width:90px">Back</button></a>
			</div>
			</div>
			    
	   		<div class="col-xs-2">
	            <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
			</div>
			    
			<div class="col-xs-3">
		  		<button type="button" class="btn btn-info pull-right" onclick="return AddAllData()">Submit</button>
			</div>
			
			<div class="col-xs-3" hidden="hidden">
		  		<button type="submit" class="btn btn-info pull-right" name="submit" id="submit">Submit</button>
			</div> 
            
           </div>
          </div>
          
 		 		 <input type="hidden" id="materialsPurchasedId" name="materialsPurchasedId" value="${materialDetails[0].materialsPurchasedId}">
				 <input type="hidden" id="creationDate" name="creationDate" value="">
				 <input type="hidden" id="updateDate" name="updateDate" value="">
				 <input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>" >	
		  	 
     	<input type="hidden" id="itemIds" name="itemIds" value="">
     	<input type="hidden" id="itemSizes" name="itemSizes" value="">
     	<input type="hidden" id="itemUnits" name="itemUnits" value="">
     	<input type="hidden" id="itemBrandNames" name="itemBrandNames" value="">
     	<input type="hidden" id="itemQuantitys" name="itemQuantitys" value="">
     	<input type="hidden" id="actualQuantitys" name="actualQuantitys" value="">
     	<input type="hidden" id="noOfPiecess" name="noOfPiecess" value="">
     	<input type="hidden" id="remarks" name="remarks" value="">
     	<input type="hidden" id="materialPurchesHistoriIds" name="materialPurchesHistoriIds" value="">
     	<input type="hidden" id="previousQuantitys" name="previousQuantitys" value="">
     
	 </div>
  </section>

	</form>
  </div>

 <%@ include file="footer.jsp" %>
 
 <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- page script -->
<script>
function ClearAll()
{

	$('#ewayBillNoSpan').html('');
	$('#vehicleNumberSpan').html('');
	$('#loadedWeightSpan').html('');
	$('#unloadedWeightSpan').html('');
	$('#checkButtonSpan').html('');
	$('#challanNoSpan').html('');
}
function getTotalMaterialWeight()
{
	var loadedWeight = Number($('#loadedWeight').val());
	var unloadedWeight = Number($('#unloadedWeight').val());
	
	var totalMaterialWeight;
	totalMaterialWeight=loadedWeight-unloadedWeight;
	 document.getElementById("totalMaterialWeight").value=totalMaterialWeight;
}

function AddAllData()
{
	ClearAll();
 	if(document.storeStockform.challanNo.value=="")
	{
		 $('#challanNoSpan').html('Please, enter eway bill no..!');
		document.storeStockform.challanNo.focus();
		return false;
	}
	else if(document.storeStockform.challanNo.value.match(/^[\s]+$/))
	{
		$('#challanNoSpan').html('Please, enter valid eway bill no..!');
		document.storeStockform.challanNo.value="";
		document.storeStockform.challanNo.focus();
		return false; 	
	}
	
	/* 
	if(document.storeStockform.vehicleNumber.value=="")
	{
		 $('#vehicleNumberSpan').html('Please, enter vehicle no..!');
		document.storeStockform.vehicleNumber.focus();
		return false;
	}
	else if(document.storeStockform.vehicleNumber.value.match(/^[\s]+$/))
	{
		$('#vehicleNumberSpan').html('Please, enter valid vehicle no..!');
		document.storeStockform.vehicleNumber.value="";
		document.storeStockform.vehicleNumber.focus();
		return false; 	
	}
	 */
	
	if(document.storeStockform.loadedWeight.value=="")
	{
		 $('#loadedWeightSpan').html('Please, enter loaded Weight..!');
		document.storeStockform.loadedWeight.focus();
		return false;
	}
	else if(document.storeStockform.loadedWeight.value.match(/^[\s]+$/))
	{
		$('#loadedWeightSpan').html('Please, enter loaded Weight..!');
		document.storeStockform.loadedWeight.value="";
		document.storeStockform.loadedWeight.focus();
		return false; 	
	}
	else if(!document.storeStockform.loadedWeight.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
	{
		 if(!document.storeStockform.loadedWeight.value.match(/^[0-9]+$/))
			 {
				$('#loadedWeightSpan').html('Please, use only digit value for loading weight..! eg:21.36 OR 30');
				document.storeStockform.loadedWeight.value="";
				document.storeStockform.loadedWeight.focus();
				return false;
			}
	}
	if(document.storeStockform.unloadedWeight.value=="")
	{
		 $('#unloadedWeightSpan').html('Please, enter Unloaded Weight..!');
		document.storeStockform.unloadedWeight.focus();
		return false;
	}
	else if(document.storeStockform.unloadedWeight.value.match(/^[\s]+$/))
	{
		$('#unloadedWeightSpan').html('Please, enter Unloaded Weight..!');
		document.storeStockform.unloadedWeight.value="";
		document.storeStockform.unloadedWeight.focus();
		return false; 	
	}
	else if(!document.storeStockform.unloadedWeight.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
	{
		 if(!document.storeStockform.unloadedWeight.value.match(/^[0-9]+$/))
			 {
				$('#unloadedWeightSpan').html('Please, use only digit value for Unloading weight..! eg:21.36 OR 30');
				document.storeStockform.unloadedWeight.value="";
				document.storeStockform.unloadedWeight.focus();
				return false;
			}
	}
	
	var storeId = $('#storeId').val();
	var creationDate = $('#creationDate').val();
	
	var materialsPurchasedId = $('#materialsPurchasedId').val();
	var historiId=Number($('#storeStockHistoriId').val());
	var storeStockHistoriId;
	var itemId;
	var itemUnit;
	var itemSize;
	var itemBrandName;
	var itemQuantity1;
	var actualQuantity1;
	var noOfPieces1=0;

	var itemIds="";
	var itemSizes="";
	var itemUnits="";
	var itemBrandNames="";
	var itemQuantitys="";
	var actualQuantitys="";
	var noOfPiecess="";
	var remarks="";
	var materialPurchesHistoriIds="";
	var previousQuantitys="";
	
	var itemQuantity;
	var actualQuantity;
	var prevoiusQuantity;
	var currentQuantity;
	var previousNoOfPieces;
	var noOfPieces=0;
	
	var remark;
	var materialPurchesHistoriId;
	var oTable = document.getElementById('materialDetailsTable');
	var rowLength = oTable.rows.length;
	var flag=0;
	//alert(rowLength);
	for (var i = 1; i < rowLength; i++){

		  var oCells = oTable.rows.item(i).cells;

		   var cellLength = oCells.length;
		   
		   storeStockHistoriId=oCells.item(1).innerHTML;
		   itemId=oCells.item(2).innerHTML;
		   itemSize = oCells.item(5).innerHTML;
		   itemUnit =oCells.item(6).innerHTML;
   		   itemBrandName = oCells.item(7).innerHTML;
		   itemQuantity = Number(oCells.item(8).innerHTML);
		   
		   prevoiusQuantity= Number(oCells.item(9).innerHTML);
		   currentQuantity= oCells.item(10).innerHTML;
		   
			previousNoOfPieces= Number(oCells.item(11).innerHTML);

			noOfPieces1= oCells.item(12).innerHTML;
			var res1 = currentQuantity.split("<br>");
			
			actualQuantity= prevoiusQuantity+ Number(res1[0]);

			var res2 = noOfPieces1.split("<br>");
			noOfPieces=previousNoOfPieces+Number(res2[0]);
			
			remark=oCells.item(13).innerHTML;
			materialPurchesHistoriId=oCells.item(14).innerHTML;
			
			if(res1[0]=="")
			{
				$('#checkButtonSpan').html('Please, enter Actual Quantity..!');
				
				return false;
			}
			if(res2[0]=="")
			{
				$('#checkButtonSpan').html('Please, enter No. of. Pieces..!');
				return false;
			}
			if(itemQuantity>actualQuantity)
				{
				flag=1;
				document.storeStockform.status.value="InCompleted";
				}
			

	     	itemIds=itemIds+itemId+"###!###";
	     	itemSizes=itemSizes+itemSize+".###!###";
	     	itemUnits=itemUnits+itemUnit+"###!###";
	     	itemBrandNames=itemBrandNames+itemBrandName+"###!###";
	     	itemQuantitys=itemQuantitys+itemQuantity+"###!###";
	     	actualQuantitys=actualQuantitys+currentQuantity+"###!###";
	     	noOfPiecess=noOfPiecess+noOfPieces1+"###!###";
	     	remarks=remarks+remark+"###!###";
	     	materialPurchesHistoriIds=materialPurchesHistoriIds+materialPurchesHistoriId+"###!###";

	     	previousQuantitys=previousQuantitys+prevoiusQuantity+"###!###";
	     	
	     	
			document.storeStockform.itemIds.value=itemIds;
			document.storeStockform.itemSizes.value=itemSizes;
			document.storeStockform.itemUnits.value=itemUnits;
			document.storeStockform.itemBrandNames.value=itemBrandNames;
			document.storeStockform.itemQuantitys.value=itemQuantitys;
			document.storeStockform.actualQuantitys.value=actualQuantitys;
			document.storeStockform.noOfPiecess.value=noOfPiecess;
			document.storeStockform.remarks.value=remarks;
			document.storeStockform.materialPurchesHistoriIds.value=materialPurchesHistoriIds;
			document.storeStockform.previousQuantitys.value=previousQuantitys;
		 
			
			
			/* 
			 $.ajax({

				url : '${pageContext.request.contextPath}/SaveAlllStoreData',
				type : 'Post',
				data : { storeStockHistoriId : storeStockHistoriId, storeId : storeId, itemId : itemId, itemUnit : itemUnit, itemSize : itemSize, itemBrandName : itemBrandName, itemQuantity : itemQuantity, actualQuantity : actualQuantity, 
					noOfPieces : noOfPieces, remark : remark, materialPurchesHistoriId : materialPurchesHistoriId, materialsPurchasedId : materialsPurchasedId},
				dataType : 'json',
				success : function(result)
						  {

						  }
				});
		  */
		}
	submitFunction();
}
function submitFunction()
{
submit.click();
}
function init()
{
	ClearAll();
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	 document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	

}
/* 
  $(function () {
    $('#materialDetailsTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
  
   */
   $(document).ready(function() {
	  $(window).keydown(function(event){
	    if(event.keyCode == 13) {
	      event.preventDefault();
	      return false;
	    }
	  });
	});
  
</script>
</body>
</html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate |Update Payment Status</title>
  
 <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
    <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>


<div class="wrapper">

  <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Update Payment Status Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Update Payment Status</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="updatepaymentstatus" action="${pageContext.request.contextPath}/UpdatePaymentStatus" onSubmit="return validate()" method="post">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <span id="statusSpan" style="color:#FF0000"></span>
              <!-- /.form-group -->
          <div class="box-body">
            <div class="row">
               
               <div class="col-md-3">
                  <label >Receipt Id</label>
                  <input type="text" class="form-control" id="receiptId" name="receiptId" value="${paymentDetails[0].receiptId}" readonly>
               </div>
               
               <div class="col-md-3">
                  <label>Booking Id</label>
                  <input type="text" class="form-control" id="bookingId" name="bookingId" value="${paymentDetails[0].bookingId}" readonly>
               </div>
               
			 </div>
           </div>
           
           <div class="box-body">
            <div class="row">
               
              <div class="col-md-3">
                <label>Customer Name</label>
                <input type="text" class="form-control" id="customerName" name="customerName" value="${paymentDetails[0].customerName}" readonly>
              </div>
               
              <div class="col-md-3">
                <label>Payment Against</label>
                <input type="text" class="form-control" id="paymentType" name="paymentType" value="${paymentDetails[0].paymentType}" readonly>
              </div>
              
              <div class="col-md-3">
                <label>Amount</label>
                <input type="text" class="form-control" id="paymentAmount" name="paymentAmount" value="${paymentDetails[0].paymentAmount}" readonly>
              </div>
              
              <div class="col-md-3">
                <label>Payment Mode</label>
                <input type="text" class="form-control" id="paymentMode" name="paymentMode" value="${paymentDetails[0].paymentMode}" readonly>
              </div>
              
			 </div>
           </div>
           
           <div class="box-body">
            <div class="row">
            
              <div class="col-md-3">
                <label>Bank Name</label>
                <input type="text" class="form-control" id="bankName" name="bankName" value="${paymentDetails[0].bankName}" readonly>
              </div>
            
              <div class="col-md-3">
                <label>Branch</label>
                <input type="text" class="form-control" id="branchName" name="branchName" value="${paymentDetails[0].branchName}" readonly>
              </div>
            
              <div class="col-md-3">
                <label>Ref No.</label>
                <input type="text" class="form-control" id="chequeNumber" name="chequeNumber" value="${paymentDetails[0].chequeNumber}" readonly>
              </div>
              
            </div>
           </div>
           
           <div class="box-body">
            <div class="row">
             &nbsp&nbsp&nbsp&nbsp<h4>Company Bank Details In Which Amount Get Credited/Deposited</h4>
                                       
             <div class="col-md-2">
                <label>Company Account Number</label> <label class="text-red">* </label>
                <select class="form-control" id="companyBankId" name="companyBankId" onchange="getBankName()">
                  <option value="Default">-Select Bank A/c No.-</option>
                  <s:forEach items="${companyBankDetails}" var="companyBankDetails" varStatus="loopStatus">
                    <option value="${companyBankDetails.companyBankId}">${companyBankDetails.companyBankacno}</option>
                  </s:forEach>
                </select>
                <span id="companyBankIdSpan" style="color:#FF0000"></span>
              </div>
              
                 
              <div class="col-md-2">
                  <label>Bank Name</label> <label class="text-red">* </label>
                    <input  type="text" class="form-control pull-right" id="companyBankname" name="companyBankname" readonly>
                  <span id="companyBanknameSpan" style="color:#FF0000"></span>
              </div>
             
              
              <div class="col-md-2">
                <label>Select Payment Status</label> <label class="text-red">*</label>
                <input type="hidden" class="form-control" id="transactionStatus" name="transactionStatus">
                <select class="form-control" id="paymentStatus" name="paymentStatus" onchange="setTransactionStatus()">
                  <option value="Default">-Select Payment Status-</option>
                  <option value="Cleared">Cleared</option>
                  <option value="Canceled">Canceled</option>
                </select>
                <span id="paymentStatusSpan" style="color:#FF0000"></span>
              </div>
              
              <div class="col-md-3">
                <label>Payment Clearance Date</label> <label class="text-red">*</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input  type="text" class="form-control pull-right" id="paymentDate" name="paymentDate">
                </div>
                <span id="paymentDateSpan" style="color:#FF0000"></span>
              </div>
              
              <div class="col-md-3">
                  <label>Payment Status Remark</label> <label class="text-red">* </label>
                    <input  type="text" class="form-control pull-right" id="remark" name="remark" style="text-transform: capitalize;">
                  <span id="remarkSpan" style="color:#FF0000"></span>
              </div>
             
            </div>
          </div>
			         
		 </div>
        </div>
           
     <div class="box-body">
       <div class="row">
            
         <div class="col-md-3">
          <label>Chart of Account</label> <label class="text-red">* </label>
             <select class="form-control" id="chartaccountId" name="chartaccountId" onchange="getSunChartOfAccount()">
             <option value="Default">-Select Chart of Account-</option>
             <c:forEach items="${chartofaccountList}" var="chartofaccountList" varStatus="loopStatus">
             <option value="${chartofaccountList.chartaccountId}">${chartofaccountList.chartaccountName}</option>
             </c:forEach>
             </select>
              <span id="chartaccountIdSpan" style="color:#FF0000"></span>
         </div>
             
         <div class="col-md-3"> <label class="text-red">* </label>
          <label>Sub Chart of Account</label>
             <select class="form-control" id="subchartaccountId" name="subchartaccountId">
             <option value="Default">-Sub Chart of Account-</option>
           
             </select>
              <span id="subchartaccountIdSpan" style="color:#FF0000"></span>
         </div>
            					
       </div>
     </div>
	
		   	<div class="box-body">
              <div class="row">
                  <br/><br/>
                    <div class="col-xs-1">
                    </div>
                  <div class="col-xs-4">
                  <div class="col-xs-2">
                	<a href="CustomerPaymentStatusMaster"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
			     </div>
			      </div>
				  
				  <div class="col-xs-3">
                     <button type="reset" class="btn btn-default" value="reset" style="width:90px">Reset</button>
			      </div>
			      
				  <div class="col-xs-2">
			  		 <input type="submit" class="btn btn-info pull-right" name="submit" value="Submit"/>
			      </div>
			       
               </div>
			 </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        
      </div>
    
      </section>
	</form>
    <!-- /.content -->
    
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script>

function getSunChartOfAccount()
{
	var chartaccountId  = $('#chartaccountId').val();
	 $("#subchartaccountId").empty();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getSubChartAccountList',
		type : 'Post',
		data : { chartaccountId : chartaccountId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Sub chart account-");
							$("#subchartaccountId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].subchartaccountId).text(result[i].subchartaccountName);
							    $("#subchartaccountId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
	
}


function clearall()
{
	$('#companyBankIdSpan').html('');
	$('#paymentDateSpan').html('');
	$('#paymentStatusSpan').html('');
	$('#remarkSpan').html('');
	$('#chartaccountIdSpan').html('');
	$('#subchartaccountIdSpan').html('');
}

function validate()
{ 
	    clearall();
	    
		if(document.updatepaymentstatus.companyBankId.value=="Default")
		{
			$('#companyBankIdSpan').html('Please, select banka/c no..!');
			document.updatepaymentstatus.companyBankId.focus();
			return false;
		}
	
		/* if(document.updatepaymentstatus.transactionStatus.value=="Default")
		{
			$('#transactionStatusSpan').html('Please, select transactions status..!');
			document.updatepaymentstatus.transactionStatus.focus();
			return false;
		} */
		
		if(document.updatepaymentstatus.paymentStatus.value=="Default")
		{
			$('#paymentStatusSpan').html('Please, select payment status..!');
			document.updatepaymentstatus.paymentStatus.focus();
			return false;
		}
	
		if(document.updatepaymentstatus.paymentDate.value=="")
		{
			$('#paymentDateSpan').html('Please, select payment date..!');
			document.updatepaymentstatus.paymentDate.focus();
			return false;
		}
	
		if(document.updatepaymentstatus.remark.value=="")
		{
			$('#remarkSpan').html('Please, enter remark..!');
			document.updatepaymentstatus.remark.focus();
			return false;
		}
		if(document.updatepaymentstatus.chartaccountId.value=="Default")
		{
			$('#chartaccountIdSpan').html('Please, select chart account Name..!');
			document.updatepaymentstatus.chartaccountId.focus();
			return false;
		}
		
		if(document.updatepaymentstatus.subchartaccountId.value=="Default")
		{
			$('#subchartaccountIdSpan').html('Please, select sub chart account Name..!');
			document.updatepaymentstatus.subchartaccountId.focus();
			return false;
		}
		  
}

function getBankName()
{
	var companyBankId  = $('#companyBankId').val();
	$.ajax({

		url : '${pageContext.request.contextPath}/getCompanyAccountBankName',
		type : 'Post',
		data : { companyBankId : companyBankId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							for(var i=0;i<result.length;i++)
							{
								 $('#companyBankname').val(result[i].companyBankname);
							}
						} 
						else
						{
						  alert("failure111");
						}
					}
		});
	
}

function setTransactionStatus()
{
   var paymentStatus = $('#paymentStatus').val();
   
   if(paymentStatus=="Canceled")
   {
	  $('#transactionStatus').val('None');
   }
   
   if(paymentStatus=="Cleared")
   {
	  $('#transactionStatus').val('Credited');
   }
}

function setPaymentStatus()
{
   var transactionStatus = $('#transactionStatus').val();
   
   if(!(transactionStatus=="None"))
   {
	  document.getElementById("paymentStatus").options[0].selected=true;
   }
}

function init()
{
	clearall();
	
  	document.updatepaymentstatus.companyBankacno.focus();
}


$(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#paymentDate').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>

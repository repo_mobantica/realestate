<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Wing Wise Booking Report</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">
  
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
 
</head>
<body class="hold-transition skin-blue sidebar-mini">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

   <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Wing Wise Booking Report Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Wing Wise Booking Report</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="bookingmasterform" action="${pageContext.request.contextPath}/PrintWingWiseReport" target="_blank" method="post">

    <section class="content">
      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
        
          <div class="box-body">
          <div class="row">
               <div class="col-md-2">
                  <label>Project</label><label class="text-red">*</label>
                  
                  <select class="form-control" name="projectId" id="projectId" onchange="getBuldingList(this.value)">
                  	 <option selected="selected" value="Default">-Select Project-</option>
                  	  <c:forEach var="projectList" items="${projectList}">
                    	<option value="${projectList.projectId}">${projectList.projectName}</option>
				     </c:forEach>
				  </select>
                  </div>
                  
                 <div class="col-xs-3">
			      <label>Project Building Name</label> <label class="text-red">* </label>
                  <select class="form-control" name="buildingId" id="buildingId" onchange="getWingNameList(this.value)">
				  		<option selected="selected" value="Default">-Select Project Unit-</option>
                 
                  </select>
			     </div> 
			     
                <div class="col-xs-3">
			      <label>Wing </label> <label class="text-red">* </label>
              	  <select class="form-control" name="wingId" id="wingId" onchange="getFloorNameList(this.value)">
				 	 	<option selected="selected" value="Default">-Select Wing Name-</option>
                
                   </select>
                 </div> 
                 
                 <div class="col-xs-2">
			         <label>Floor </label> <label class="text-red">* </label>
	              	 <select class="form-control" name="floorId" id="floorId" onchange="getFloorWiseBookingList(this.value)">
					 	 	<option selected="selected" value="Default">-Select Floor Name-</option>
	                  
	                 </select>
			      </div>
			      
			      <div class="col-xs-2">
			      		<br/>
			      		<button type="button" class="btn btn-success" onclick="SearchFlatStatus()">Search</button>
			      		<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-print"></span>Print</button>
            	  </div>
			      
                  </div>
                </div>
                
              <!-- /.form-group -->
            </div>
            
            <!-- /.col -->
			
          </div>
		  	  <div class="box-body">
                <div class="row">
					
					  <!-- <div class="col-xs-2">
				       <label>From Date:</label>
					    <div class="input-group">
		                  <div class="input-group-addon">
		                    <i class="fa fa-calendar"></i>
		                  </div>
	                		<input type="text" class="form-control pull-right" id="datepicker" name="fromDate" value="">
	               	    </div>
	                        <span id="bookingDobSpan" style="color:#FF0000"></span>
					   </div>			
					
						
					  <div class="col-xs-2">
				       <label>To Date:</label>
					    <div class="input-group">
		                  <div class="input-group-addon">
		                    <i class="fa fa-calendar"></i>
		                  </div>
	                		<input type="text" class="form-control pull-right" id="datepicker1" name="toDate" value="">
	               	    </div>
	                        <span id="bookingDobSpan" style="color:#FF0000"></span>
					   </div> 	

					
	                	<div class="col-xs-2">
	                		<br/>
				      		<button type="button" class="btn btn-success" onclick="SearchDateWiseFlatStatus()">Search</button>
	            		</div>
            		
						<div class="col-xs-2">
							<br/>
				  			<a href="WingWiseReport"><button type="button" class="btn btn-default" value="reset" style="width:90px">Reset</button></a>
	              		</div> -->
            		
			  	</div>
			</div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        
      </div>
      <!-- /.box -->
			<div class="box box-default">
				
				<h3><label> &nbsp &nbsp Wing Wise Booking Report :</label></h3>
        	 <div class="box-body">
              <div class="table-responsive">
          <table id="WingWiseListTable" class="table table-bordered">
                  <thead>
		              <tr bgcolor="#4682B4">
		                  <th>Sr.No.</th>
		                  <th>Customer Name</th>
		                  <th>Flat No</th>
		                  <th>Project Name</th>
		                  <th>Building Name</th>
		                  <th>Wing Name</th>
						  <th>Basic Cost</th>
		                  <th>Agreement Amount</th>
						  <th>Extra Charges</th>
						  <th>Total Cost</th>
						  <th>Booking Date</th>
	                  </tr>	
                  </thead>
                  <tbody >
                   <c:forEach items="${todaysBookingList}" var="todaysBookingList" varStatus="loopStatus">
	                   <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
	                   	   <td>${loopStatus.index+1}</td>
		                   <td>${todaysBookingList.bookingfirstname} </td>
			               <td>${todaysBookingList.flatId}</td>
			               <td>${todaysBookingList.projectId}</td>
			               <td>${todaysBookingList.buildingId}</td>
			               <td>${todaysBookingList.wingId}</td>
			               <td>${todaysBookingList.flatbasicCost}</td>
			               <td>${todaysBookingList.aggreementValue1}</td>
			               <td>${todaysBookingList.infrastructureCharge}</td>
			               <td>${todaysBookingList.grandTotal1}</td>
			               <td>${todaysBookingList.stringDate}</td>
	                   </tr>
				   </c:forEach>
                 </tbody>
                </table>
              </div>
            </div>
	 </div>
     
    </section>
	</form>
    <!-- /.content -->
    
  </div>
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
       
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>

<!-- Bootstrap 3.3.7 -->
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- Page script -->

<script>

function getBuldingList()
{
	 $("#buildingId").empty();
	 var projectId = $('#projectId').val();

	 $.ajax({

		 url : '${pageContext.request.contextPath}/getBuildingList',
		type : 'Post',
		data : { projectId : projectId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Building Name-");
							$("#buildingId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].buildingId).text(result[i].buildingName);
							    $("#buildingId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get Building List



function getWingNameList()
{
	 $("#wingId").empty();
	 var buildingId = $('#buildingId').val();
	 var projectId = $('#projectId').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getprojectwingList',
		type : 'Post',
		data : { buildingId : buildingId, projectId : projectId },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Wing Name-");
							$("#wingId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].wingId).text(result[i].wingName);
							    $("#wingId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
}


function getFloorNameList()
{
	 $("#floorId").empty();
	 var wingId = $('#wingId').val();
	 var buildingId = $('#buildingId').val();
	 var projectId = $('#projectId').val();
	 
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getwingfloorNameList',
		type : 'Post',
		data : {wingId : wingId, buildingId : buildingId, projectId : projectId },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Floor Name-");
							$("#floorId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].floorId).text(result[i].floortypeName);
							    $("#floorId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
}


//----------------------------------------------------
function SearchFlatStatus()
{
	 $("#WingWiseListTable tr").detach();
	 var floorId = $('#floorId').val();
	 var wingId = $('#wingId').val();
	 var buildingId = $('#buildingId').val();
	 var projectId = $('#projectId').val();
	 
	 $.ajax({
		 
		url : '${pageContext.request.contextPath}/getCatagoryWiseFlatBookingList',
		type : 'Post',
		data : {floorId : floorId, wingId : wingId, buildingId : buildingId , projectId : projectId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
								   $('#WingWiseListTable').append('<tr style="background-color: #4682B4;"><th>Sr.No.</th><th>Customer Name</th><th>Flat No</th><th>Basic Cost</th><th>Agreement Amount</th><th>Extra Charges</th><th>Total Cost</th><th>Booking Date</th>');
								   
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									$('#WingWiseListTable').append('<tr style="background-color: #F0F8FF;"><td>'+(i+1)+'</td><td>'+result[i].bookingfirstname+'</td><td>'+result[i].flatId+'</td><td>'+result[i].flatbasicCost+'</td><td>'+result[i].aggreementValue1+'</td><td>'+result[i].infrastructureCharge+'</td><td>'+result[i].grandTotal1+'</td><td>'+result[i].stringDate+'</td>');
							
								}
								else
								{
									$('#WingWiseListTable').append('<tr style="background-color: #CCE5FF;"><td>'+(i+1)+'</td><td>'+result[i].bookingfirstname+'</td><td>'+result[i].flatId+'</td><td>'+result[i].flatbasicCost+'</td><td>'+result[i].aggreementValue1+'</td><td>'+result[i].infrastructureCharge+'</td><td>'+result[i].grandTotal1+'</td><td>'+result[i].stringDate+'</td>');
								}
							
							 } 
						}  
						else
						{
							alert("failure111");
						}

					}
		});
}


function SearchDateWiseFlatStatus()
{
	 $("#WingWiseListTable tr").detach();
	 var fromDate = $('#datepicker').val();
	 var toDate = $('#datepicker1').val();

	 alert(fromDate);
	 
	 $.ajax({
		 
		url : '${pageContext.request.contextPath}/getDateWiseFlatBookingList',
		type : 'Post',
		data : {fromDate : fromDate, toDate : toDate},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
								   $('#WingWiseListTable').append('<tr style="background-color: #4682B4;"><th>Sr.No.</th><th>Customer Name</th><th>Flat No</th><th>Basic Cost</th><th>Agreement Amount</th><th>Extra Charges</th><th>Total Cost</th><th>Booking Amount</th><th>Booking Date</th>');
								   
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									$('#WingWiseListTable').append('<tr style="background-color: #F0F8FF;"><td>'+(i+1)+'</td><td>'+result[i].bookingfirstname+'</td><td>'+result[i].flatId+'</td><td>'+result[i].flatbasicCost+'</td><td>'+result[i].aggreementValue1+'</td><td>'+result[i].infrastructureCharge+'</td><td>'+result[i].grandTotal1+'</td><td>'+result[i].bookingAmount1+'</td><td>'+result[i].creationDate+'</td>');
							
								}
								else
								{
									$('#WingWiseListTable').append('<tr style="background-color: #CCE5FF;"><td>'+(i+1)+'</td><td>'+result[i].bookingfirstname+'</td><td>'+result[i].flatId+'</td><td>'+result[i].flatbasicCost+'</td><td>'+result[i].aggreementValue1+'</td><td>'+result[i].infrastructureCharge+'</td><td>'+result[i].grandTotal1+'</td><td>'+result[i].bookingAmount1+'</td><td>'+result[i].creationDate+'</td>');
								}
							
							 } 
						}  
						else
						{
							alert("failure111");
						}

					}
		});
}

$(function () 
 {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    
    //Date picker
    $('#datepicker1').datepicker({
      autoclose: true
    })
 
    $('#datepicker').datepicker({
      autoclose: true
    })
    
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })	  
		  
   $(function () {
    $('#WingWiseListTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
  
  
</script>
</body>
</html>
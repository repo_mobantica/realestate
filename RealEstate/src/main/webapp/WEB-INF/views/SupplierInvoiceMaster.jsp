<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>RealEstate | Pending Vender Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


  <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">
	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

    <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
    <section class="content-header">
      <h1>
        Pending Vender Invoices:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Account</a></li>
        <li class="active"> Pending Vender Invoices</li>
      </ol>
    </section>
    <!-- Main content -->
	<form name="supplierpaymentbillform" action="${pageContext.request.contextPath}/SupplierInvoiceMaster" onSubmit="return validate()" method="post">

    <section class="content">
    <div class="box box-default">
      
      <div class="box-body">
							<div class="row">
								<div class="col-md-12">

									<div class="box-body">
										<div class="row">
											<div class="col-md-3">
												<label>Supplier</label> <select class="form-control"
													id="supplierId" name="supplierId"
													onchange="getSupplierWiseInvoiceList(this.value)">
													<option selected="selected" value="Default">Select
														Vender-</option>

													<s:forEach var="supplierList" items="${supplierList}">
														<option value="${supplierList.supplierId}">${supplierList.supplierfirmName}</option>
													</s:forEach>

												</select>
											</div>
										
								
								
										</div>
									</div>

								</div>

							</div>
					
						</div>
      
		<div class="box box-default">
		    <div  class="panel box box-danger"></div>
        	 <div class="box-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="venderPaymentListTable">
                  <thead>
	                  <tr bgcolor=#4682B4>
	                 	<th>#</th>
	                 	<th>Purchase Id</th>
	                 	<th>Supplier Firm Name</th>
	                 	<th>Store Name</th>
	                    <th>PO Date</th>
	                    <th>Requisition Id</th>
	                 	<!-- 
	                    <th>Total</th>
	                    <th>Discount</th>
	                    
	                    <th>GST</th>
	            		<th>Grand Amount</th> 
	            		-->
	                    <th style="width:70px">Action</th>
	                  </tr>
                  </thead>
                  
             <tbody>
                  <c:forEach items="${purchesedList}" var="purchesedList" varStatus="loopStatus">
                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                        <td>${purchesedList.number}</td>
                        <td>${purchesedList.materialsPurchasedId}</td>
                        <td>${purchesedList.supplierId}</td>
                        <td>${purchesedList.storeId}</td>
                        <td>${purchesedList.creationDate}</td>
                        <td>${purchesedList.requisitionId}</td>
                        <%-- 
                        <td>${purchesedList.totalPrice}</td>
                        <td>${purchesedList.totalDiscount}</td>
                        
                        <td>${purchesedList.totalGstAmount}</td>
                        <td>${purchesedList.totalAmount}</td>
                        --%>
                        <td>
                        <!-- 
                        AddSupplierBillPayment
                         -->
                      <a href="${pageContext.request.contextPath}/SupplierInvoiceEntry?materialsPurchasedId=${purchesedList.materialsPurchasedId}" class="btn btn-success btn-sm" data-toggle="tooltip" title="View"><i class="glyphicon glyphicon-eye-open"></i></a>
                      |&nbsp<a target="_blank" href="${pageContext.request.contextPath}/MaterialPurchaseBillReceipt?materialsPurchasedId=${purchesedList.materialsPurchasedId}" class="btn btn-info btn-sm" data-toggle="tooltip" title="Print Bill"><span class="glyphicon glyphicon-print"></span></a>
                          </td>
                      </tr>
					</c:forEach> 
                 </tbody>
                </table>
              </div>
            </div>
	 </div>
      
  </div>
    
    
  </section>
	</form>
    <!-- /.content -->
  </div>


  <!-- Control Sidebar -->
 <%@ include file="footer.jsp" %>

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- page script -->
<script>
function getSupplierWiseInvoiceList()
{
	 //to retrive all banks by Location Area name
	$("#venderPaymentListTable tr").detach();
	
	 var supplierId = $('#supplierId').val();

	 $.ajax({

		url : '${pageContext.request.contextPath}/getSupplierWiseInvoiceList',
		type : 'Post',
		data : { supplierId : supplierId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
							$('#venderPaymentListTable').append('<tr style="background-color: #4682B4;"><th>#</th><th>Purchase Id</th><th>Supplier Firm Name</th><th>Store Name</th><th>PO Date</th><th>Requisition Id</th> <th>Action</th>');
						
							for(var i=result.length-1;i>=0;i--)
							{ 
								if(i%2==0)
								{
									 var id = result[i].materialsPurchasedId;
									 $('#venderPaymentListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].number+'</td><td>'+result[i].materialsPurchasedId+'</td><td>'+result[i].supplierId+'</td><td>'+result[i].storeId+'</td><td>'+result[i].creationDate+'</td><td>'+result[i].requisitionId+'</td><td><a href="${pageContext.request.contextPath}/SupplierInvoiceEntry?materialsPurchasedId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="View"><i class="glyphicon glyphicon-eye-open"></i></a><a href="${pageContext.request.contextPath}/MaterialPurchaseBillReceipt?materialsPurchasedId='+id+'" class="btn success btn-sm"  target="_blank" data-toggle="tooltip" title="Print Bill"><i class="glyphicon glyphicon-print"></i></a></td>');
											
								}
								else
								{
									var id = result[i].materialsPurchasedId;
									$('#venderPaymentListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].number+'</td><td>'+result[i].materialsPurchasedId+'</td><td>'+result[i].supplierId+'</td><td>'+result[i].storeId+'</td><td>'+result[i].creationDate+'</td><td>'+result[i].requisitionId+'</td><td><a href="${pageContext.request.contextPath}/SupplierInvoiceEntry?materialsPurchasedId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="View"><i class="glyphicon glyphicon-eye-open"></i></a><a href="${pageContext.request.contextPath}/MaterialPurchaseBillReceipt?materialsPurchasedId='+id+'" class="btn success btn-sm"  target="_blank" data-toggle="tooltip" title="Print Bill"><i class="glyphicon glyphicon-print"></i></a></td>');
										
								}
							 } 
						} 
						else
						{
							alert("failure111");
						}
					}
		});
	
}


  $(function () {
    $('#venderPaymentListTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
  
  
</script>
</body>
</html>

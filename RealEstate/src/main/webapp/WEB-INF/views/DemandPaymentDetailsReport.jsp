<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Demand Payment Details Report</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">
  
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
 
</head>
<body class="hold-transition skin-blue sidebar-mini">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

   <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Demand Payment Details Report :
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Sale</a></li>
        <li class="active">Demand Payment Details Report</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="DemandPaymentDetailsReport" action="${pageContext.request.contextPath}/DemandPaymentDetailsReport" target="_blank" onSubmit="return validate()" method="post">

    <section class="content">
      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
        
          <div class="box-body">
          <div class="row">
               <div class="col-md-3">
                  <label>Project Name</label><label class="text-red">*</label>
                  
                  <select class="form-control" name="projectId" id="projectId" onchange="getBuldingList(this.value)">
                  	 <option selected="selected" value="Default">-Select Project-</option>
                  	  <c:forEach var="projectList" items="${projectList}">
                    	<option value="${projectList.projectId}">${projectList.projectName}</option>
				     </c:forEach>
				  </select>
				  <span id="projectIdSpan" style="color:#FF0000"></span>
                  </div>
                  
                 <div class="col-xs-2">
			      <label>Building Name</label> <label class="text-red">* </label>
                  <select class="form-control" name="buildingId" id="buildingId" onchange="getWingNameList(this.value)">
				  		<option selected="selected" value="Default">-Select Project Building-</option>
                  
                  </select>
                  <span id="buildingIdSpan" style="color:#FF0000"></span>
			     </div> 
			     
                <div class="col-xs-2">
			      <label>Wing Name</label> <label class="text-red">* </label>
              	  <select class="form-control" name="wingId" id="wingId">
				 	 	<option selected="selected" value="Default">-Select Wing Name-</option>
                  
                   </select>
                   <span id="wingIdSpan" style="color:#FF0000"></span>
                 </div> 
                 
                 <%-- <div class="col-xs-2">
			         <label>Floor </label> <label class="text-red">* </label>
	              	 <select class="form-control" name="floortypeName" id="floortypeName" onchange="getFloorWiseBookingList(this.value)">
					 	 	<option selected="selected" value="Default">-Select Floor Name-</option>
	                     <c:forEach var="floortypeList" items="${floortypeList}">
	                    	<option value="${floortypeList.floortypeName}">${floortypeList.floortypeName}</option>
					     </c:forEach>
	                 </select>
			      </div> --%>
			      
			      <div class="col-xs-4">
			      		<br/>
			      		<button type="button" class="btn btn-success" onclick="return SearchFlatStatus()">Search</button>
			      	&nbsp &nbsp 	<a href="DemandPaymentDetailsReport">  <button type="button" class="btn btn-default" value="reset" style="width:90px">Reset</button></a>
			      	&nbsp &nbsp 	<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-print"></span>Print</button>
			      		<button type="button" class="btn btn-success" onclick="return SendMailForDemandPayment()">Send Mail</button>
            	  </div>
			      
                  </div>
                </div>
                
              <!-- /.form-group -->
            </div>
            
            <!-- /.col -->
			
          </div>
		  	  <div class="box-body">
                <div class="row">
					
					  <!-- <div class="col-xs-2">
				       <label>From Date:</label>
					    <div class="input-group">
		                  <div class="input-group-addon">
		                    <i class="fa fa-calendar"></i>
		                  </div>
	                		<input type="text" class="form-control pull-right" id="datepicker" name="fromDate" value="">
	               	    </div>
	                        <span id="bookingDobSpan" style="color:#FF0000"></span>
					   </div>			
					
						
					  <div class="col-xs-2">
				       <label>To Date:</label>
					    <div class="input-group">
		                  <div class="input-group-addon">
		                    <i class="fa fa-calendar"></i>
		                  </div>
	                		<input type="text" class="form-control pull-right" id="datepicker1" name="toDate" value="">
	               	    </div>
	                        <span id="bookingDobSpan" style="color:#FF0000"></span>
					   </div> 	

					
	                	<div class="col-xs-2">
	                		<br/>
				      		<button type="button" class="btn btn-success" onclick="SearchDateWiseFlatStatus()">Search</button>
	            		</div>
            		
						<div class="col-xs-2">
							<br/>
				  			<a href="WingWiseReport"><button type="button" class="btn btn-default" value="reset" style="width:90px">Reset</button></a>
	              		</div> -->
            		
			  	</div>
			</div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        
      </div>
      <!-- /.box -->
			<div class="box box-default">
				
				<h3><label> &nbsp &nbsp Demand Payment Report :</label></h3>
        	 <div class="box-body">
              <div class="table-responsive">
          <table id="WingWiseListTable" class="table table-bordered">
                  <thead>
		              <tr bgcolor="#4682B4">
		                  <th>Sr.No.</th>
		                  <th>Booking Id</th>
		                  <th>Customer Name</th>
		                  <th>Wing</th>
		                  <th>Flat No</th>
		                  <th>Agg. Amount</th>
		                  <th>Demand Amount</th>
		                  <th>Paid Till Date</th>
						  <th>Remaining Amount</th>
	                  </tr>	
                  </thead>
                  <tbody >
                   <%-- <c:forEach items="${todaysBookingList}" var="todaysBookingList" varStatus="loopStatus">
	                   <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
	                   	   <td>${loopStatus.index+1}</td>
		                   <td>${todaysBookingList.bookingfirstname} ${todaysBookingList.bookingmiddlename} ${todaysBookingList.bookinglastname}</td>
			               <td>${todaysBookingList.flatId}</td>
			               <td>${todaysBookingList.projectId}</td>
			               <td>${todaysBookingList.buildingId}</td>
			               <td>${todaysBookingList.wingId}</td>
			               <td>${todaysBookingList.flatbasicCost}</td>
			               <td>${todaysBookingList.aggreementValue1}</td>
			               <td>${todaysBookingList.infrastructureCharge}</td>
			               <td>${todaysBookingList.grandTotal1}</td>
			               <td>${todaysBookingList.creationDate}</td>
	                   </tr>
				   </c:forEach> --%>
                 </tbody>
                </table>
              </div>
            </div>
	 </div>
     
    </section>
	</form>
    <!-- /.content -->
    
  </div>
 
  <!-- Control Sidebar -->
  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
       
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>

<!-- Bootstrap 3.3.7 -->
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- Page script -->

<script>

function clearall()
{

	$('#projectIdSpan').html('');
	$('#buildingIdSpan').html('');
	$('#wingIdSpan').html('');
}

function getBuldingList()
{
	 $("#buildingId").empty();
	 var projectId = $('#projectId').val();

	 $.ajax({

		 url : '${pageContext.request.contextPath}/getBuildingList',
		type : 'Post',
		data : { projectId : projectId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Building Name-");
							$("#buildingId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].buildingId).text(result[i].buildingName);
							    $("#buildingId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	 
	 
	
}//end of get Building List



function getWingNameList()
{
	 $("#wingId").empty();
	 var buildingId = $('#buildingId').val();
	 var projectId = $('#projectId').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getprojectwingList',
		type : 'Post',
		data : { buildingId : buildingId, projectId : projectId },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Wing Name-");
							$("#wingId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].wingId).text(result[i].wingName);
							    $("#wingId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
}


/*function getFloorNameList()
{
	 $("#floortypeName").empty();
	 var wingId = $('#wingId').val();
	 var buildingId = $('#buildingId').val();
	 var projectId = $('#projectId').val();
	 
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getwingfloorNameList',
		type : 'Post',
		data : {wingId : wingId, buildingId : buildingId, projectId : projectId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Floor Name-");
							$("#floortypeName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].floortypeName).text(result[i].floortypeName);
							    $("#floortypeName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
} */


//----------------------------------------------------
function SearchFlatStatus()
{
	 clearall();
	 
	 //validation for filters
	 if(document.DemandPaymentDetailsReport.projectId.value=="Default")
	 {
		$('#projectIdSpan').html('Please, select project name..!');
		document.DemandPaymentDetailsReport.projectId.focus();
		return false;
	 }
	 
	 if(document.DemandPaymentDetailsReport.buildingId.value=="Default")
	 {
		$('#buildingIdSpan').html('Please, select building name..!');
		document.DemandPaymentDetailsReport.buildingId.focus();
		return false;
	 }
	 
	 if(document.DemandPaymentDetailsReport.wingId.value=="Default")
	 {
		$('#wingIdSpan').html('Please, select building name..!');
		document.DemandPaymentDetailsReport.wingId.focus();
		return false;
	 }
	 
	 $("#WingWiseListTable tr").detach();
	 
	 //var floortypeName = $('#floortypeName').val();
	 var wingId = $('#wingId').val();
	 var buildingId = $('#buildingId').val();
	 var projectId = $('#projectId').val();
	 
	 
	 $.ajax({
		 
		url : '${pageContext.request.contextPath}/GetDemandPaymentDetailsList',
		type : 'Post',
		data : {projectId : projectId, buildingId : buildingId, wingId : wingId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
						 $('#WingWiseListTable').append('<tr style="background-color: #4682B4;"><th>Sr.No.</th><th>Booking Id</th><th>Customer Name</th><th>Wing</th><th>Flat No</th><th>Agg. Amount</th><th>Demand Amount</th><th>Agg. Paid Till Date</th><th>Remaining Demand Amt.</th><th>Remaining Agg. Amount</th></tr>');
								   
							for(var i=0;i<result.length;i++)	
							{ 
								if(i%2==0)
								{
									var id=result[i].bookingId;
									$('#WingWiseListTable').append('<tr style="background-color: #F0F8FF;"><td>'+(i+1)+'</td><td>'+result[i].bookingId+'</td><td>'+result[i].customerName+'</td><td>'+result[i].wingId+'</td><td>'+result[i].flatId+'</td><td>'+result[i].agreementAmount+'</td><td>'+result[i].demandAmount+'</td><td>'+result[i].paidTillDate+'</td><td>'+result[i].remainingDemandAmount+'</td><td>'+result[i].remainingAmount+'</td>');
							
									//<td><a onclick="sendRemainderLatter('+result[i].bookingId+')"  class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a> </td>
									
								}
								else
								{
									var id=result[i].bookingId;
									$('#WingWiseListTable').append('<tr style="background-color: #CCE5FF;"><td>'+(i+1)+'</td><td>'+result[i].bookingId+'</td><td>'+result[i].customerName+'</td><td>'+result[i].wingId+'</td><td>'+result[i].flatId+'</td><td>'+result[i].agreementAmount+'</td><td>'+result[i].demandAmount+'</td><td>'+result[i].paidTillDate+'</td><td>'+result[i].remainingDemandAmount+'</td><td>'+result[i].remainingAmount+'</td>');
								}
							
							 } 
						}  
						else
						{
							alert("failure111");
						}

					}
		});
}

function sendRemainderLatter(id)
{

	alert("ABCD");
}

//----------------------------------------------------
function SendMailForDemandPayment()
{
	 clearall();
	 
	 //validation for filters
	 if(document.DemandPaymentDetailsReport.projectId.value=="Default")
	 {
		$('#projectIdSpan').html('Please, select project name..!');
		document.DemandPaymentDetailsReport.projectId.focus();
		return false;
	 }
	 
	 if(document.DemandPaymentDetailsReport.buildingId.value=="Default")
	 {
		$('#buildingIdSpan').html('Please, select building name..!');
		document.DemandPaymentDetailsReport.buildingId.focus();
		return false;
	 }
	 
	 if(document.DemandPaymentDetailsReport.wingId.value=="Default")
	 {
		$('#wingIdSpan').html('Please, select building name..!');
		document.DemandPaymentDetailsReport.wingId.focus();
		return false;
	 }
	 
	 //var floortypeName = $('#floortypeName').val();
	 var wingId = $('#wingId').val();
	 var buildingId = $('#buildingId').val();
	 var projectId = $('#projectId').val();
	 
	 
	 $.ajax({
		 
		url : '${pageContext.request.contextPath}/SendMailForDemandPayment',
		type : 'Post',
		data : {projectId : projectId, buildingId : buildingId, wingId : wingId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
						
						}  
						else
						{
							alert("failure111");
						}

					}
		});
}


function validate()
{
	
 clearall();
	 
	 //validation for filters
	 if(document.DemandPaymentDetailsReport.projectId.value=="Default")
	 {
		$('#projectIdSpan').html('Please, select project name..!');
		document.DemandPaymentDetailsReport.projectId.focus();
		return false;
	 }
	 
	 if(document.DemandPaymentDetailsReport.buildingId.value=="Default")
	 {
		$('#buildingIdSpan').html('Please, select building name..!');
		document.DemandPaymentDetailsReport.buildingId.focus();
		return false;
	 }
	 
	 if(document.DemandPaymentDetailsReport.wingId.value=="Default")
	 {
		$('#wingIdSpan').html('Please, select building name..!');
		document.DemandPaymentDetailsReport.wingId.focus();
		return false;
	 }
	 
}
$(function () 
 {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    
    //Date picker
    $('#datepicker1').datepicker({
      autoclose: true
    })
 
    $('#datepicker').datepicker({
      autoclose: true
    })
    
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })	  
		  
   $(function () {
    $('#WingWiseListTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
  
  
</script>
</body>
</html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Edit Material Transfer To Contractor</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


  <!-- Google Font -->
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#enquirydbStatusSpan').delay(1000).fadeOut();
      });
    </script>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

  <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Edit Material Transfer To Contractor:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Store</a></li>
        <li class="active">Edit Material Transfer To Contractor</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="materialTransferform" action="${pageContext.request.contextPath}/EditMaterialTransferToContractor" onSubmit="return validate()" method="post">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <span id="statusSpan" style="color:#FF0000"></span>
			
			<div class="box-body">
              <div class="row">
              
                  <div class="col-xs-2">
                  <label >Transfer Id</label>
                  <input type="text" class="form-control" id="materialTransferId" name="materialTransferId" value="${transferList[0].materialTransferId}" readonly>
                   <span id="materialTransferIdSpan" style="color:#FF0000"></span>
                </div>               
            
              </div>
            </div>
			
			<div class="box-body">
              <div class="row">
     
        	     <div class="col-md-2">
	                 <label>From Store Name</label><label class="text-red">* </label>
                      <select class="form-control" id="storeId" name="storeId">
                      
                      	<%-- <option value="${transferList[0].storeId}">${transferList[0].storeId}</option> --%>
                      		<c:forEach var="storeList" items="${storeList}">
                       		 <c:choose>
                                <c:when test="${transferList[0].storeId eq storeList.storeId}">
	                    		    <option selected="selected" value="${transferList[0].storeId}">${storeList.storeName}</option>
	                            </c:when>
	                         </c:choose>
                       		<%--  <c:choose>
                                <c:when test="${transferList[0].storeId ne storeList.storeId}">
	                    		    <option value="${storeList.storeId}">${storeList.storeName}</option>
	                            </c:when>
	                         </c:choose> --%>
	                    	</c:forEach>
                      </select>
                      <span id="storeIdSpan" style="color:#FF0000"></span>
                  </div>
                
                 <div class="col-xs-2">
			      <label>To Contractor Firm Name  </label> <label class="text-red">* </label>
                  <select class="form-control" name="contractorId" id="contractorId" onchange="getContratorDetails(this.value)">
                  <option value="${transferList[0].contractorId}">${transferList[0].contractorfirmName}</option>
                      		<c:forEach var="contractorList" items="${contractorList}">
                       		 <c:choose>
                                <c:when test="${transferList[0].contractorId ne contractorList.contractorId}">
	                    		    <option value="${contractorList.contractorId}">${contractorList.contractorfirmName}</option>
	                            </c:when>
	                         </c:choose>
	                    	</c:forEach>
                 
                  </select>
			      <span id="contractorIdSpan" style="color:#FF0000"></span>
			     </div> 
		
			     <div class="col-xs-2">
			       <label>Contractor Firm Emp Name </label> <label class="text-red">* </label>
                   <select class="form-control" name="contractorEmployeeId" id="contractorEmployeeId" >
                    <option value="${transferList[0].contractorEmployeeId}">${transferList[0].employeeName}</option>
                   </select>
			       <span id="contractorEmployeeIdSpan" style="color:#FF0000"></span>
			     </div> 
			<!--     
              </div>
            </div>
			
			<div class="box-body">
              <div class="row">
               -->
                  <div class="col-xs-2">
			      <label>Project  </label> <label class="text-red">* </label>
                  <select class="form-control" name="projectName" id="projectName" onchange="getBuldingList(this.value)">
                     <option value="${transferList[0].projectName}">${projectName}</option>
                     <c:forEach var="projectList" items="${projectList}">
                       <c:choose>
                       <c:when test="${transferList[0].projectName ne projectList.projectName}">
	                   <option value="${projectList.projectName}">${projectList.projectName}</option>
	                   </c:when>
	                   </c:choose>
	                 </c:forEach>
                  </select>
			        <span id="projectNameSpan" style="color:#FF0000"></span>
			     </div> 
			     
			      <div class="col-xs-2">
			      <label>Project Building Name  </label> <label class="text-red">* </label>
                  <select class="form-control" name="buildingName" id="buildingName" onchange="getWingNameList(this.value)">
				  <option value="${transferList[0].buildingName}">${buildingName}</option>
                  </select>
			       <span id="buildingNameSpan" style="color:#FF0000"></span>
			     </div> 
			
                 <div class="col-xs-2">
			      <label>Wing </label> <label class="text-red">* </label>
              		<select class="form-control" name="wingName" id="wingName" >
				 	  <option value="${transferList[0].wingName}">${wingName}</option>
                    </select>
                   <span id="wingNameSpan" style="color:#FF0000"></span>
                 </div> 
              
			  </div>
            </div> 
            
       <div class="box-body">
       <h4><label>Available Item Details  </label></h4>
           <div class="row">
                  
                <div class="col-xs-2">
			      <label>Item Main Category</label> <label class="text-red">* </label>
                  <select class="form-control" name="suppliertypeId" id="suppliertypeId" onchange="getMainSubItemCategory(this.value)">
				  <option selected="" value="Default">-Select Category-</option>
                   <c:forEach var="suppliertypeList" items="${suppliertypeList}" >
					<option value="${suppliertypeList.suppliertypeId}">${suppliertypeList.supplierType}</option>
					 </c:forEach>
                  </select>
			         <span id="suppliertypeIdSpan" style="color:#FF0000"></span>
			     </div> 
			     
			      <div class="col-xs-2">
			      <label>Item Sub-Category</label> <label class="text-red">* </label>
                  <select class="form-control" name="subsuppliertypeId" id="subsuppliertypeId" onchange="getAllItem(this.value)">
				  <option selected="" value="Default">-Select Sub Item Category-</option>
                 
                  </select>
			         <span id="subsuppliertypeIdSpan" style="color:#FF0000"></span>
			     </div>  
           
			    <div class="col-xs-2">
				  <label for="itemId">Item Name </label> <label class="text-red">* </label>
  				   <select class="form-control" name="itemId" id="itemId" onchange="getItemDetails(this.value)">
				   <option selected="" value="Default">-Select Item Name-</option>
                  </select>
                   <span id="itemIdSpan" style="color:#FF0000"></span>
                </div>
                    
                   
              	<div class="col-xs-1">
			       <label for="availableQuantity">Stock</label><label class="text-red">* </label>
                   <input type="text" class="form-control" id="availableQuantity" name="availableQuantity"  placeholder="Available Stock" readonly>
                   <span id="availableQuantitySpan" style="color:#FF0000"></span>
			    </div> 
			     
              	<div class="col-xs-1">
			       <label for="availablePieces">Pieces</label><label class="text-red">* </label>
                   <input type="text" class="form-control" id="availablePieces" name="availablePieces"  placeholder="Available Pieces" readonly>
                   <span id="availablePiecesSpan" style="color:#FF0000"></span>
			    </div>   
			     
              	<div class="col-xs-2">
			       <label for="transferStock">Transfer Stock</label><label class="text-red">* </label>
                   <input type="text" class="form-control" id="transferStock" name="transferStock"  placeholder="transfer Stocks" >
                   <span id="transferStockSpan" style="color:#FF0000"></span>
			    </div>    
			     
				 <div class="col-xs-2">
			      <label for="transferPieces">Transfer Pieces</label><label class="text-red">* </label>
				  <div class="input-group">
                
                  <input type="text" class="form-control" id="transferPieces" name="transferPieces"  placeholder="Transfer Quantity" onchange="getCheckAmount(this.value)">
                   <span class="input-group-btn">
            	    <button type="button" class="btn btn-success" onclick="return AddTransferQuantity()"><i class="fa fa-plus"></i>Add</button>
              	   </span>
                  </div>
                <span id="transferPiecesSpan" style="color:#FF0000"></span>
			   </div>
			  
           </div>
        </div>    
            
           
           <div class="box-body">
           <h4><label>Selected Items </label></h4>
              <div class="row">
              <br/>
              
              <div class="col-xs-12">
                <table class="table table-bordered" id="transferMaterialListTable">
	              <tr bgcolor=#4682B4>
	               	  <th>Item Sub-Category</th>
		              <th>Item Name</th>
		              <th>Quantity</th>
		              <th>Quantity Pieces</th>
		              <th>Action</th>
	               </tr>
	                 <c:forEach items="${contractorMaterialList}" var="contractorMaterialList" varStatus="loopStatus">
                    <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                        <td>${contractorMaterialList.subsuppliertypeId}</td>
                        <td>${contractorMaterialList.itemName}</td>
                        <td>${contractorMaterialList.transferStock}</td>
                        <td>${contractorMaterialList.transferPieces}</td>
                        <td><a onclick="DeleteSelectedContractorMaterial(${contractorMaterialList.materialTransferHistoryId})" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td>
                     </tr>
                    </c:forEach>
                </table>
              </div>
              
			  </div>
            </div>      
          
			<input type="hidden" id="creationDate" name="creationDate" >
			<input type="hidden" id="updateDate" name="updateDate" >
			<input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>" >
		</div>	
      </div>
      
		   	 <div class="box-body">
              <div class="row">
              <br/><br/>
              <div class="col-xs-1">
              </div>
                 <div class="col-xs-4">
	            	 <div class="col-xs-2">	
	             		<a href="MaterialTransferToContractorMaster"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
				     </div> 
			     </div>
				  <div class="col-xs-4">
                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
			     </div>
					<div class="col-xs-2">
			        <button type="submit" class="btn btn-info " name="submit">Submit</button>
			     </div> 
			     
              </div>
			  </div>
          <!-- /.row -->
        </div>
        
      </div>
		
    </section>
	</form>
    <!-- /.content -->
  </div>
  
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>

 <script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
</script>
 
<script>

function AddTransferQuantity()
{

	clearall();
	
	 var storeId=$('#storeId').val();
	 var materialTransferId = $('#materialTransferId').val();
	 var itemId = $('#itemId').val();
	 

		if(document.materialTransferform.suppliertypeId.value=="Default")
		{
			$('#suppliertypeIdSpan').html('Please Select item category Name');
			document.materialTransferform.suppliertypeId.focus();
			return false;
		}

		if(document.materialTransferform.subsuppliertypeId.value=="Default")
		{
			$('#subsuppliertypeIdSpan').html('Please Select item sub category Name');
			document.materialTransferform.subsuppliertypeId.focus();
			return false;
		}

		if(document.materialTransferform.itemId.value=="Default")
		{
			$('#itemIdSpan').html('Please Select  Item Name');
			document.materialTransferform.itemId.focus();
			return false;
		}
		

		if(document.materialTransferform.transferStock.value=="")
		{
			$('#transferStockSpan').html('Please, enter transfer quantity');
			document.materialTransferform.transferStock.focus();
			return false;
		}
		else if(document.materialTransferform.transferStock.value.match(/^[\s]+$/))
		{
			$('#transferStockSpan').html('Please, enter transfer quantity..!');
			document.materialTransferform.transferStock.value="";
			document.materialTransferform.transferStock.focus();
			return false; 	
		}
		else if(!document.materialTransferform.transferStock.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			 if(!document.materialTransferform.transferStock.value.match(/^[0-9]+$/))
				 {
					$('#transferStockSpan').html('Please, use only digit value for transfer quantity..! eg:21.36 OR 30');
					document.materialTransferform.transferStock.value="";
					document.materialTransferform.transferStock.focus();
					return false;
				}
		}
		
		if(document.materialTransferform.transferPieces.value=="")
		{
			$('#transferPiecesSpan').html('Please, enter transfer quantity');
			document.materialTransferform.transferPieces.focus();
			return false;
		}
		else if(document.materialTransferform.transferPieces.value.match(/^[\s]+$/))
		{
			$('#transferPiecesSpan').html('Please, enter transfer quantity..!');
			document.materialTransferform.transferPieces.value="";
			document.materialTransferform.transferPieces.focus();
			return false; 	
		}
		else if(!document.materialTransferform.transferPieces.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			 if(!document.materialTransferform.transferPieces.value.match(/^[0-9]+$/))
				 {
					$('#transferPiecesSpan').html('Please, use only digit value for transfer quantity..! eg:21.36 OR 30');
					document.materialTransferform.transferPieces.value="";
					document.materialTransferform.transferPieces.focus();
					return false;
				}
		}

		 var availableQuantity = Number($('#availableQuantity').val());
		 var transferStock = Number($('#transferStock').val());
		 
		 var availablePieces = Number($('#availablePieces').val());
		 var transferPieces = Number($('#transferPieces').val());
		 

		 if(availableQuantity<transferStock)
			 {
			    $('#transferStockSpan').html('Please enter transfer Quantity less then available Quantity');
				document.materialTransferform.transferStock.focus();
				return false;
			 }
		 
	 if(availablePieces<transferPieces)
		 {
		    $('#transferPiecesSpan').html('Please enter transfer Quantity less then available Quantity');
			document.materialTransferform.transferPieces.focus();
			return false;
		 }
	 
	 $('#transferMaterialListTable tr').detach();
	 $.ajax({

		url : '${pageContext.request.contextPath}/AddTransferQuantity',
		type : 'Post',
		data : { storeId : storeId, materialTransferId : materialTransferId, itemId : itemId, transferPieces : transferPieces, transferStock : transferStock},
		dataType : 'json',
		success : function(result)
				  {
			
				   if (result) 
				   { 
						$('#transferMaterialListTable').append('<tr style="background-color: #4682B4;"><th>Item Sub Category</th><th>Item Name</th><th>Quantity</th><th>Quantity Pieces</th><th>Action</th>');
					
						for(var i=0;i<result.length;i++)
						{ 
							if(i%2==0)
							{
								var id = result[i].materialTransferHistoryId;
								$('#transferMaterialListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].subsuppliertypeId+'</td><td>'+result[i].itemName+'</td><td>'+result[i].transferStock+'</td><td>'+result[i].transferPieces+'</td><td><a onclick="DeleteSelectedContractorMaterial('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
							}
							else
							{
								var id = result[i].materialTransferHistoryId;
								$('#transferMaterialListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].subsuppliertypeId+'</td><td>'+result[i].itemName+'</td><td>'+result[i].transferStock+'</td><td>'+result[i].transferPieces+'</td><td><a onclick="DeleteSelectedContractorMaterial('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
							}
						
						 } 
					} 
					else
					{
						alert("failure111");
					}
				
				  }
		});
  
  
}
function getAllItem()
{
	 $("#itemId").empty();
	 var suppliertypeId = $('#suppliertypeId').val();
	 var subsuppliertypeId = $('#subsuppliertypeId').val();
	// var subItemCategoryName = $('#subItemCategoryName').val();
	
	 $.ajax({

		url : '${pageContext.request.contextPath}/getAllItemList',
		type : 'Post',
		data : { suppliertypeId : suppliertypeId, subsuppliertypeId : subsuppliertypeId},
		dataType : 'json',
		success : function(result)
				  {
			
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Item Name-");
							$("#itemId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].itemId).text(result[i].itemName);
							    $("#itemId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
						}
		
		}
		});

		if(document.materialTransferform.storeId.value=="Default")
		{
			$('#storeIdSpan').html('Please Select Select Store Name');
			document.materialTransferform.storeId.focus();
			return false;
		}

}
function getMainSubItemCategory()
{
	$('#subsuppliertypeId').empty();
	var suppliertypeId = $('#suppliertypeId').val();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getSubSupplierTypeList',
		type : 'Post',
		data : { suppliertypeId : suppliertypeId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select Sub Item Category-");
								$("#subsuppliertypeId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].subsuppliertypeId).text(result[i].subsupplierType);
							    $("#subsuppliertypeId").append(option);
							} 
						} 
						else
						{
							alert("failure111");
						}

					}
		});	
	
}

function getItemDetails()
{

	clearall();
	if(document.materialTransferform.storeId.value=="Default")
	{
		$('#storeIdSpan').html('Please Select Store Name');
		document.materialTransferform.storeId.focus();
		return false;
	}

	var itemId = $('#itemId').val();
	var storeId = $('#storeId').val();
	//availableQuantity
	document.materialTransferform.availableQuantity.value=0;
	document.materialTransferform.availablePieces.value=0;
	$.ajax({

		url : '${pageContext.request.contextPath}/GetStoreWiseItemQunatity',
		type : 'Post',
		data : { storeId : storeId, itemId : itemId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							
							for(var i=0;i<result.length;i++)
							{
								 document.materialTransferform.availableQuantity.value=result[i].currentStock; 
								 document.materialTransferform.availablePieces.value=result[i].noOfPieces;  
								// document.materialTransferform.availableQuantity.value=result[i].currentStock; 
							} 
						} 
						else
						{
							alert("failure111");
						}

					}
		});	
	
	
}



function getContratorDetails()
{
	$("#contractorEmployeeId").empty();
	 var contractorId = $('#contractorId').val();

	 $.ajax({

		url : '${pageContext.request.contextPath}/getContratorEmployeeDetails',
		type : 'Post',
		data : { contractorId : contractorId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							

							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Contractor Firm Emp Name-");
							$("#contractorEmployeeId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].contractorEmployeeId).text(result[i].employeeName);
							    $("#contractorEmployeeId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		}); 
}

 
function DeleteSelectedContractorMaterial(id)
{

	var storeId=$('#storeId').val();
	var materialTransferId = $('#materialTransferId').val();
    var materialTransferHistoryId=Number(id);
	 $('#transferMaterialListTable tr').detach();
	 $.ajax({

		url : '${pageContext.request.contextPath}/DeleteSelectedContractorMaterial',
		type : 'Post',
		data : { storeId : storeId, materialTransferHistoryId : materialTransferHistoryId, materialTransferId : materialTransferId},
		dataType : 'json',
		success : function(result)
				  {
			
				   if (result) 
				   { 
						$('#transferMaterialListTable').append('<tr style="background-color: #4682B4;"><th>Item Sub Category</th><th>Item Name</th><th>Quantity</th><th> Quantity Pieces</th><th>Action</th>');
					
						for(var i=0;i<result.length;i++)
						{ 
							if(i%2==0)
							{
								var id = result[i].materialTransferHistoryId;
								$('#transferMaterialListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].subsuppliertypeId+'</td><td>'+result[i].itemName+'</td><td>'+result[i].transferStock+'</td><td>'+result[i].transferPieces+'</td><td><a onclick="DeleteSelectedContractorMaterial('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
							}
							else
							{
								var id = result[i].materialTransferHistoryId;
								$('#transferMaterialListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].subsuppliertypeId+'</td><td>'+result[i].itemName+'</td><td>'+result[i].transferStock+'</td><td>'+result[i].transferPieces+'</td><td><a onclick="DeleteSelectedContractorMaterial('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
							}
						
						 } 
					} 
					else
					{
						alert("failure111");
					}
				
				  }
		});


} 
function getTotalAmount()
{
	 var totalBuildupArea = Number($('#totalBuildupArea').val());
	 var workRate = Number($('#workRate').val());
	 var totalAmount=totalBuildupArea*workRate;
	 
	 document.materialTransferform.totalAmount.value=totalAmount.toFixed(0);
}

function clearall()
{
	$('#projectIdSpan').html('');
	$('#buildingIdSpan').html('');
	$('#wingIdSpan').html('');
	$('#suppliertypeIdSpan').html('');
	$('#subsuppliertypeIdSpan').html('');
	$('#availableQuantitySpan').html('');
	$('#transferQuantitySpan').html('');
	$('#materialTransferHistoryIdSpan').html('');
	$('#materialTransferIdSpan').html('');
	$('#itemIdSpan').html('');
	$('#transferQuantitySpan').html('');
	$('#storeIdSpan').html('');
	$('#contractorIdSpan').html('');
	$('#contractorEmployeeIdSpan').html('');
	
}
function validate()
{ 
	clearall();
	
	if(document.materialTransferform.storeId.value=="Default")
	{
		$('#storeIdSpan').html('Please Select Store name');
		document.materialTransferform.storeId.focus();
		return false;
	}

	if(document.materialTransferform.contractorId.value=="Default")
	{
		$('#contractorIdSpan').html('Please Select contractor name');
		document.materialTransferform.contractorId.focus();
		return false;
	}

	if(document.materialTransferform.contractorEmployeeId.value=="Default")
	{
		$('#contractorEmployeeIdSpan').html('Please Select firm employee name');
		document.materialTransferform.contractorEmployeeId.focus();
		return false;
	}
	
	if(document.materialTransferform.projectId.value=="Default")
	{
		$('#projectIdSpan').html('Please Select project name');
		document.materialTransferform.projectId.focus();
		return false;
	}

	if(document.materialTransferform.buildingId.value=="Default")
	{
		$('#buildingIdSpan').html('Please Select building name');
		document.materialTransferform.buildingId.focus();
		return false;
	}

	if(document.materialTransferform.wingId.value=="Default")
	{
		$('#wingIdSpan').html('Please Select firm wing name');
		document.materialTransferform.wingId.focus();
		return false;
	}
	
	
}
function init()
{
	clearall();
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	
/* 	
	 if(document.materialTransferform.status.value=="Fail")
	 {
	  	//alert("Sorry, record is present already..!");
	 }
	 else if(document.materialTransferform.status.value=="Success")
	 {
			$('#statusSpan').html('Record added successfully..!');
	 } */
}



function getBuldingList()
{
	 $("#buildingId").empty();
	 var projectId = $('#projectId').val();
	 
	 $.ajax({

		url : '${pageContext.request.contextPath}/getBuildingList',
		type : 'Post',
		data : { projectId : projectId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Building Name-");
							$("#buildingId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].buildingId).text(result[i].buildingName);
							    $("#buildingId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
}//end of get Building List


function getWingNameList()
{
	 $("#wingId").empty();
	 var buildingId = $('#buildingId').val();
	 var projectId = $('#projectId').val();
	 $.ajax({

		url : '${pageContext.request.contextPath}/getprojectwingList',
		type : 'Post',
		data : { buildingId : buildingId, projectId : projectId },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Wing Name-");
							$("#wingId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].wingId).text(result[i].wingName);
							    $("#wingId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}

/* 
function getFloorNameList()
{
	 $("#floortypeName").empty();
	 var wingName = $('#wingName').val();
	 var buildingName = $('#buildingName').val();
	 var projectName = $('#projectName').val();
	 
	 $.ajax({

		url : '${pageContext.request.contextPath}/getwingfloorNameList',
		type : 'Post',
		data : {wingName : wingName, buildingName : buildingName, projectName : projectName },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"All").text("All");
							$("#floortypeName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].floortypeName).text(result[i].floortypeName);
							    $("#floortypeName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	

}//end of get Floor Type List

 */
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
     $('#workStartDate').datepicker({
      autoclose: true
    })

    $('#workEndDate').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>

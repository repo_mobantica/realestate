<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Edit Task</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

   <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Task Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Edit Task</li>
      </ol>
    </section>

    <!-- Main content -->
    <form name="taskform" action="${pageContext.request.contextPath}/EditTask" onSubmit="return validate()" method="post">

    
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          
          <div class="box-body">
              <div class="row">
               
                <div class="col-xs-3">
                  <label for="taskId">Task Id</label>
                  <input type="text" class="form-control" id="taskId" placeholder="ID" name="taskId"  value="${taskDetails[0].taskId}" readonly>
                </div>
                
              </div>
          </div>
          
          <div class="box-body">
              <div class="row">
               
               <div class="col-xs-2">
                  <label>Project </label> <label class="text-red">* </label>
                 <select class="form-control" name="projectId" id="projectId" onchange="getBuldingList(this.value)">
				        <option value="Default">-Select Project-</option>
				        <option value="${taskDetails[0].projectId}" selected="selected">${taskDetails[0].projectName}</option>
                     <c:forEach var="projectList" items="${projectList}">
                       <c:choose>
                         <c:when test="${taskDetails[0].projectId ne projectList.projectId}">
                    	   <option value="${projectList.projectId}">${projectList.projectName}</option>
                         </c:when>
                       </c:choose>
				     </c:forEach>
                  </select>
                  <span id="projectNameSpan" style="color:#FF0000"></span>
                </div>               
                <div class="col-xs-2">
				 <label>Project Building </label> <label class="text-red">* </label>
                    <select class="form-control" name="buildingId" id="buildingId" onchange="getWingNameList(this.value)">
				  		<option value="Default">-Select a Project Unit-</option>
				  		<option value="${taskDetails[0].buildingId}" selected="selected">${taskDetails[0].buildingName}</option>
                     <c:forEach var="projectbuildingList" items="${projectbuildingList}">
                      <c:choose>
                       <c:when test="${projectbuildingList.buildingId ne taskDetails[0].buildingId}">
                    	 <option value="${projectbuildingList.buildingId}">${projectbuildingList.buildingName}</option>
                       </c:when>
                      </c:choose>
				     </c:forEach>
                  </select>
                  <span id="buildingNameSpan" style="color:#FF0000"></span>
				</div>
               
                <div class="col-xs-2">
			         <label>Wing </label> <label class="text-red">* </label>
                     <select class="form-control" name="wingId" id="wingId" onchange="getFloorNameList()">
				 	 	  <option value="Default">-Select Wing Name-</option>
				 	 	  <option value="${taskDetails[0].wingId}" selected="selected">${taskDetails[0].wingName}</option>
                       <c:forEach var="projectwingList" items="${projectwingList}">
                         <c:choose>
                          <c:when test="${projectwingList.wingId ne taskDetails[0].wingId}">
                    	    <option value="${projectwingList.wingId}">${projectwingList.wingName}</option>
                    	  </c:when>
                    	 </c:choose>
				       </c:forEach>
                     </select>
                     <span id="wingNameSpan" style="color:#FF0000"></span>
                  </div>
                   
                  <div class="col-xs-2">
                  <label>Floor </label> <label class="text-red">* </label>
                  <select class="form-control" name="floorId" id="floorId" onchange="getFlatNumberList(this.value)">
				 		 <option value="Default">-Select Floor-</option>
				 		 <option selected="selected" value="${taskDetails[0].floorId}">${taskDetails[0].floorName}</option>
                      <c:forEach var="floortypeList" items="${floortypeList}">
                        <c:choose>
                          <c:when test="${floortypeList.floorId ne taskDetails[0].floorId}">
                    		<option value="${floortypeList.floorId}">${floortypeList.floortypeName}</option>
                    	  </c:when>
                    	</c:choose>
				      </c:forEach>
                  </select>
                  <span id="floortypeNameSpan" style="color:#FF0000"></span>
                </div> 
          <!--      
              </div>
          </div>
          
          <div class="box-body">
              <div class="row">
               -->
               <div class="col-xs-2">
				   <label>Flat </label> <label class="text-red">* </label>
                   <select class="form-control" id="flatId" name="flatId">
				         <option value="Default">-Select Flat Number-</option>
				         <option selected="selected" value="${taskDetails[0].flatId}">${taskDetails[0].flatNumber}</option>
                   </select>
                   <span id="flatNumberSpan" style="color:#FF0000"></span>
			   </div> 
                  
               <div class="col-xs-2">
			      <label>Task Name </label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="taskName" placeholder="Task Name" style="text-transform: capitalize;" name="taskName" value="${taskDetails[0].taskName}">
                  <span id="taskNameSpan" style="color:#FF0000"></span>
			   </div>
			   
			   <!-- <div class="col-xs-4">
			     <label for="taskDescription">Task Description </label> <label class="text-red">* </label>
                 <textarea class="form-control" rows="2" id="taskDescription" placeholder="Task Description" style="text-transform: capitalize;" name="taskDescription"></textarea>
			   </div> --> 
               
              </div>
          </div>
          
          <div class="box-body">
              <div class="row">
              
                <div class="col-xs-3">
                  <label>Task Assign From </label> <label class="text-red">* </label>
                  <select class="form-control" id="taskassignFrom" name="taskassignFrom" onchange="getEmployeeList()">
				        <option value="Default">-Select Name-</option>
				        <option selected="selected" value="${taskDetails[0].taskassignFrom}">${taskDetails[0].assignFrom}</option>
                     <c:forEach var="employeeList" items="${employeeList}">
                      <c:choose>
                        <c:when test="${employeeList.employeeId ne taskDetails[0].taskassignFrom}">
                    	  <option value="${employeeList.employeeId}">${employeeList.employeefirstName} ${employeeList.employeemiddleName} ${employeeList.employeelastName}</option>
                        </c:when>
                      </c:choose>
				     </c:forEach>
                  </select>
                  <span id="taskassignFromSpan" style="color:#FF0000"></span>
                </div> 
                              
                <div class="col-xs-3">
				  <label>Task Assign To </label> <label class="text-red">* </label>
                  <select class="form-control" id="taskassignTo" name="taskassignTo">
				    <option value="Default">-Select Emp Name-</option>
				    <option selected="selected" value="${taskDetails[0].taskassignTo}">${taskDetails[0].assignTo}</option>
                  </select>
                  <span id="taskassignToSpan" style="color:#FF0000"></span>
				</div>
				
				<div class="col-xs-2">
                  <label for="emailid">Task Assign Date</label> <label class="text-red">* </label>
				  <div class="input-group date">
                   <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                   </div>
                   <input type="text" class="form-control pull-right" id="datepicker1" name="taskassingDate" value="${taskDetails[0].taskassingDate1}">
                 </div>
                 <span id="taskassingDateSpan" style="color:#FF0000"></span>
			    </div>
			    
				<div class="col-xs-2">
			     <label>Task End Date</label> <label class="text-red">* </label>
			     <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker" name="taskendDate" value="${taskDetails[0].taskendDate1}">
                 </div>
                 <span id="taskendDateSpan" style="color:#FF0000"></span>
				</div> 
		<!-- 		
              </div>
          </div>
          
          <div class="box-body">
            <div class="row">
                -->
               <div class="col-xs-2">
                  <label>Status</label> <label class="text-red">* </label>
                  <select class="form-control" id="taskStatus" name="taskStatus">
                  <c:choose>
                   <c:when test="${taskDetails[0].taskStatus eq 'Not Started'}">
                    <option selected="selected">Not Started</option>
                   </c:when>
                   <c:otherwise>
                     <option>Not Started</option>
                   </c:otherwise>                 
                  </c:choose>
                  
                  <c:choose>
                   <c:when test="${taskDetails[0].taskStatus eq 'In-Process'}">
                      <option selected="selected">In-Process</option>
                    </c:when>
                    <c:otherwise>
                      <option>In-Process</option>
                    </c:otherwise>
                  </c:choose>
                  
                  <c:choose>
                   <c:when test="${taskDetails[0].taskStatus eq 'In-Complete'}">
					 <option selected="selected">In-Complete</option>
					</c:when>
					<c:otherwise>
					 <option>In-Complete</option>
					</c:otherwise>
				  </c:choose>
				  
				  <c:choose>
                   <c:when test="${taskDetails[0].taskStatus eq 'Complete'}">
                      <option selected="selected">Complete</option>
                   </c:when>
                   <c:otherwise>
                      <option>Complete</option>
                   </c:otherwise>
                  </c:choose>
                  </select>
                  <span id="taskStatusSpan" style="color:#FF0000"></span>
                </div> 
                
                <input type="hidden" id="creationDate" name="creationDate" value="">
			    <input type="hidden" id="updateDate" name="updateDate" value="">
			    <input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>"> 
                  
            </div>
          </div>
          
        </div>
        
        <div class="box">
        
          <div class="box-header">
              <h3 class="box-title">Add Multiple Tasks:</h3>
            </div>
          <div class="box-body">
            <div class="row">
              
             <div class="col-xs-3">
                  <input type="hidden" id="taskHistoryId" name="taskHistoryId" value="">
			      <label>Task Description </label> <label class="text-red">* </label>
			      <textarea class="form-control" rows="1" id="taskDescription" placeholder="Enter Task Description" name="taskDescription"></textarea>
			      <span id="taskDescriptionSpan" style="color:#FF0000"></span>
			 </div>
			 
			 <div class="col-xs-2">
			      <label>Status</label> <label class="text-red">* </label>
                  <select class="form-control" id="otherTaskStatus" name="otherTaskStatus">
                    <option selected="selected" value="Default">-Select Status-</option>
                    <option>Not Started</option>
                    <option>In-Process</option>
					 <option>In-Complete</option>
                    <option>Complete</option>
                  </select>
                  <span id="otherTaskStatusSpan" style="color:#FF0000"></span>
			 </div>
			 
			 <div class="col-xs-2">
                  <label for="startDate">Assign Dt.</label> <label class="text-red">* </label>
				  <div class="input-group date">
                   <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                   </div>
                   <input type="text" class="form-control pull-right" id="startDate" name="startDate">
                 </div>
                 <span id="startDateSpan" style="color:#FF0000"></span>
			 </div>
			 
			 <div class="col-xs-2">
                  <label for="endDate">Exp. End Dt.</label> <label class="text-red">* </label>
				  <div class="input-group date">
                   <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                   </div>
                   <input type="text" class="form-control pull-right" id="expectedEndDate" name="expectedEndDate" onchange="copyEndDate()">
                 </div>
                 <span id="expectedEndDateSpan" style="color:#FF0000"></span>
			 </div>
			 
			 <div class="col-xs-2">
                  <label for="endDate">Act. End Dt.</label>
				  <div class="input-group date">
                   <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                   </div>
                   <input type="text" class="form-control pull-right" id="actualEndDate" name="actualEndDate">
                   <span id="AddCharge" class="input-group-btn">
	            	 <button type="button" class="btn btn-success" onclick="return AddOtherTasks()"><i class="fa fa-plus"></i>Add</button>
	               </span>
	               <span id="UpdateCharge" class="input-group-btn">
	            	 <button type="button" class="btn btn-success" onclick="return UpdateOtherTask()"><i class="fa fa-plus"></i>Update</button>
	               </span>
                   
                 </div>
                 <span id="actualEndDateSpan" style="color:#FF0000"></span>
			 </div>
		   
		   </div>
		 </div>
            
            <div class="box-header">
              <h3 class="box-title">Previus Tasks</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             <div class="table-responsive">
              <table id="taskHistoryTable" class="table table-bordered">
                <thead>
	                <tr bgcolor=#4682B4>
	                  <th>Sr No.</th>
	                  <th>Task Description</th>
	                  <th>Task Status</th>
	                  <th>Start Date</th>
	                  <th>End Date</th>
					  <th>Actual Completion Date</th>
	                  <th>Action</th>
	                </tr>
                </thead>
				
                <tbody>
                   <c:forEach items="${taskHistoryDetails}" var="taskHistoryDetails" varStatus="loopStatus">
                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                            <td>${loopStatus.index+1}</td>
                            <td>${taskHistoryDetails.taskDescription}</td>
	                        <td>${taskHistoryDetails.otherTaskStatus}</td>
	                        <td>${taskHistoryDetails.startDate1}</td>
	                        <td>${taskHistoryDetails.expectedEndDate1}</td>
	                        <td>${taskHistoryDetails.actualEndDate1}</td>
	                        <td><a onclick="EditOtherTask('${taskHistoryDetails.taskHistoryId}')" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>
                       </tr>
					</c:forEach>
             
                </tbody>
               
              </table>
             </div>
            </div>
            
            <div class="box-body">
              <div class="row">
                  <br/>
                  <div class="col-xs-3">
                  
                  </div>
                  
                  <div class="col-xs-3">
                  <div class="col-xs-2">
                     <a href="TaskMaster"><button type="button" class="btn btn-info" value="Task Master" style="width:90px">Back</button></a>
			      </div>
			      </div>
			      
				  <div class="col-xs-3">
                     <button type="reset" class="btn btn-default" value="reset"> Reset</button>
			      </div>
			      
			      <div class="col-xs-3">
			         <button type="submit" class="btn btn-info" name="submit">Submit</button>
			      </div> 
			      
              </div>
		 </div>
            
            <!-- /.box-body -->
          </div>
          
      </div>
      <!-- /.box -->

     
    </section>
	</form>
    <!-- /.content -->
  </div>
  
  <!-- /.content-wrapper -->
  

  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script>
function getBuldingList()
{
	 $("#buildingId").empty();
	 var projectId = $('#projectId').val();

	 $.ajax({

		url : '${pageContext.request.contextPath}/getBuildingList',
		type : 'Post',
		data : { projectId : projectId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Building Name-");
							$("#buildingId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].buildingId).text(result[i].buildingName);
							    $("#buildingId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
}//end of get Building List

function getWingNameList()
{
	 $("#wingId").empty();
	 var buildingId = $('#buildingId').val();
	 var projectId = $('#projectId').val();
	 
	
		 $.ajax({
	
			url : '${pageContext.request.contextPath}/getprojectwingList',
			type : 'Post',
			data : { buildingId : buildingId, projectId : projectId },
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select Wing Name-");
								$("#wingId").append(option);
								
								var option = $('<option/>');
								option.attr('value',"ALL").text("ALL");
								$("#wingId").append(option);
								
								for(var i=0;i<result.length;i++)
								{
									var option = $('<option />');
								    option.attr('value',result[i].wingId).text(result[i].wingName);
								    $("#wingId").append(option);
								 } 
							} 
							else
							{
								alert("failure111");
							}
	
						}
			});
}

function getFloorNameList()
{
	 $("#floorId").empty();
	 
	 var wingId = $('#wingId').val();
	 var buildingId = $('#buildingId').val();
	 var projectId = $('#projectId').val();
	 
	 if(wingId=="ALL")
	 {
		 var option = $('<option/>');
		 option.attr('value',"ALL").text("ALL");
		 $("#floorId").append(option);
		 
		 $("#flatId").empty();
		 var option = $('<option/>');
		 option.attr('value',"ALL").text("ALL");
		 $("#flatId").append(option);
	 }
	 else
	 {
		 $.ajax({
	
			url : '${pageContext.request.contextPath}/getwingfloorNameList',
			type : 'Post',
			data : {wingId : wingId, buildingId : buildingId, projectId : projectId },
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select Floor Name-");
								$("#floorId").append(option);
								
								option = $('<option/>');
								option.attr('value',"ALL").text("ALL");
								$("#floorId").append(option);
								
								for(var i=0;i<result.length;i++)
								{
									var option = $('<option />');
								    option.attr('value',result[i].floorId).text(result[i].floortypeName);
								    $("#floorId").append(option);
								 } 
							} 
							else
							{
								alert("failure111");
							}
	
						}
			});
	  }
}

function getFlatNumberList()
{
	 var wingId      = $('#wingId').val();
	 var buildingId  = $('#buildingId').val();
	 var projectId   = $('#projectId').val();
	 var floorId     = $('#floorId').val();
	 
	 $("#flatId").empty();
	 
		 $.ajax({
	
				url : '${pageContext.request.contextPath}/getAllFlatNumbers',
				type : 'Post',
				data : {floorId: floorId ,wingId : wingId, buildingId : buildingId, projectId : projectId},
				dataType : 'json',
				success : function(result)
						  {
								if (result) 
								{
									var option = $('<option/>');
									option.attr('value',"Default").text("-Select Flat Number-");
									$("#flatId").append(option);
									
									option = $('<option/>');
									option.attr('value',"ALL").text("ALL");
									$("#flatId").append(option);
									
									for(var i=0;i<result.length;i++)
									{
										var option = $('<option />');
									    option.attr('value',result[i].flatId).text(result[i].flatNumber);
									    $("#flatId").append(option);
									 } 
								} 
								else
								{
									alert("failure111");
								}
	
							}
				});
}

function getEmployeeList()
{
   var employeeId = $('#taskassignFrom').val();

   $("#taskassignTo").empty();
	 
	 $.ajax({

			url : '${pageContext.request.contextPath}/getEmployeeListByDepartment',
			type : 'Post',
			data : {employeeId : employeeId},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select Employee Name-");
								$("#taskassignTo").append(option);
								
								for(var i=0;i<result.length;i++)
								{
									var option = $('<option />');
								    option.attr('value',result[i].employeeId).text(result[i].employeefirstName+" "+result[i].employeemiddleName+" "+result[i].employeelastName);
								    $("#taskassignTo").append(option);
								} 
							} 
							else
							{
								alert("failure111");
							}
						}
			});
}

function validate()
{ 
	if(document.taskform.projectId.value=="Default")
	{
		$('#projectNameSpan').html('Please, select project name..!');
		document.taskform.projectId.focus();
		
		return false;
	}
	
	if(document.taskform.buildingId.value=="Default")
	{
		$('#buildingNameSpan').html('Please, select building name..!');
		document.taskform.buildingId.focus();
		
		return false;
	}
	
	if(document.taskform.wingId.value=="Default")
	{
		$('#wingNameSpan').html('Please, select wing name..!');
		document.taskform.wingId.focus();
		
		return false;
	}
	
	if(document.taskform.floorId.value=="Default")
	{
		$('#floortypeNameSpan').html('Please, select floor..!');
		document.taskform.floorId.focus();
		
		return false;
	}
	
	if(document.taskform.flatId.value=="Default")
	{
		$('#flatNumberSpan').html('Please, select flat number..!');
		document.taskform.flatId.focus();
		
		return false;
	}
	
	if(document.taskform.taskName.value=="")
	{
		$('#taskNameSpan').html('Please, enter task name..!');
		document.taskform.taskName.value="";
		document.taskform.taskName.focus();
		
		return false;
	}
	
	if(document.taskform.taskassignFrom.value=="Default")
	{
		$('#taskassignFromSpan').html('Please, select employee Name..!');
		document.taskform.taskassignFrom.focus();
		
		return false;
	}
	
	if(document.taskform.taskassignTo.value=="Default")
	{
		$('#taskassignToSpan').html('Please, select employee Name..!');
		document.taskform.taskassignTo.focus();
		
		return false;
	}
	
	if(document.taskform.taskassingDate.value=="")
	{
		$('#taskassingDateSpan').html('Please, select assign date..!');
		document.taskform.taskassingDate.value="";
		document.taskform.taskassingDate.focus();
		
		return false;
	}
	
	if(document.taskform.taskendDate.value=="")
	{
		$('#taskendDateSpan').html('Please, select end  date..!');
		document.taskform.taskendDate.value="";
		document.taskform.taskendDate.focus();
		
		return false;
	}
	
	if(document.taskform.taskStatus.value=="")
	{
		$('#taskStatusSpan').html('Please, select task status..!');
		document.taskform.taskStatus.value="";
		document.taskform.taskStatus.focus();
		
		return false;
	}
}

function init()
{
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
  
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
    $("#UpdateCharge").removeAttr("style").hide();
  
    document.taskform.projectId.focus();
}

function clearAll()
{
	$('#taskDescriptionSpan').html('');
	$('#otherTaskStatusSpan').html('');
	$('#startDateSpan').html('');
	$('#expectedEndDateSpan').html('');
	$('#actualEndDateSpan').html('');
}

function AddOtherTasks()
{

	clearAll();
	
	//validation for employee name
	if(document.taskform.taskDescription.value=="")
	{
		$('#taskDescriptionSpan').html('Description should not be empty..!');
		document.taskform.taskDescription.value="";
		document.taskform.taskDescription.focus();
		
		return false;
	}
	
	//validation for employee email id
	if(document.taskform.otherTaskStatus.value=="Default")
	{
		$('#otherTaskStatusSpan').html('Please, select status..!');
		document.taskform.otherTaskStatus.focus();
		return false;
	}
	
	if(document.taskform.startDate.value=="")
	{
		$('#startDateSpan').html('Please, select start date..!');
		document.taskform.startDate.focus();
		return false;
	}
	
	if(document.taskform.expectedEndDate.value=="")
	{
		$('#expectedEndDateSpan').html('Please, select expected end date..!');
		document.taskform.expectedEndDate.focus();
		return false;
	}
	
	/* if(document.taskform.actualEndDate.value=="")
	{
		$('#actualEndDateSpan').html('Please, select actal end date..!');
		document.taskform.actualEndDate.focus();
		return false;
	} */
	 
	$('#taskHistoryTable tr').detach();
	 
	 var taskId          = $('#taskId').val();
	 var taskDescription = $('#taskDescription').val();
	 var otherTaskStatus = $('#otherTaskStatus').val();
	 var startDate       = $('#startDate').val();
	 var expectedEndDate = $('#expectedEndDate').val();
	 var actualEndDate   = $('#actualEndDate').val();
	 
	 $.ajax({

		 url : '${pageContext.request.contextPath}/AddOtherTasks',
		type : 'Post',
		data : { taskId : taskId, taskDescription : taskDescription, otherTaskStatus : otherTaskStatus, startDate : startDate, expectedEndDate : expectedEndDate, actualEndDate : actualEndDate },
		dataType : 'json',
		success : function(result)
				  {
					   if (result) 
					   { 
									$('#taskHistoryTable').append('<tr style="background-color: #4682B4;"><th style="width:300px">Task Description</th><th style="width:150px">Status</th><th style="width:30px">Start Date</th><th style="width:30px">Expected End Date</th><th style="width:30px">Actual End Date</th><th style="width:30px">Action</th>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].taskHistoryId;
									$('#taskHistoryTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].taskDescription+'</td><td>'+result[i].otherTaskStatus+'</td><td>'+result[i].startDate1+'</td><td>'+result[i].expectedEndDate1+'</td><td>'+result[i].actualEndDate1+'</td><td><a onclick="EditOtherTask('+id+')" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a> </td>');
								}
								else
								{
									var id = result[i].taskHistoryId;
									$('#taskHistoryTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].taskDescription+'</td><td>'+result[i].otherTaskStatus+'</td><td>'+result[i].startDat1e+'</td><td>'+result[i].expectedEndDate1+'</td><td>'+result[i].actualEndDate1+'</td><td><a onclick="EditOtherTask('+id+')" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a> </td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}
				  } 

		});
	 
	 $('#taskDescription').val("");
	 $('#startDate').val("");
	 $('#expectedEndDate').val("");
	 $('#actualEndDate').val("");
}


function EditOtherTask(taskHistoryId)
{
	var taskId = $('#taskId').val();
	$.ajax({

		 url : '${pageContext.request.contextPath}/EditOtherTask',
		type : 'Post',
		data : { taskId : taskId, taskHistoryId : taskHistoryId},
		dataType : 'json',
		success : function(result)
				  {
					   if (result) 
					   { 
							 for(var i=0; i < result.length; i++)
							 {
							   $('#taskHistoryId').val(result[i].taskHistoryId);
							   $('#taskDescription').val(result[i].taskDescription);
							   $('#startDate').val(result[i].startDate1);
							   $('#expectedEndDate').val(result[i].expectedEndDate1);
							   $('#actualEndDate').val(result[i].actualEndDate1);
							 }
						   
						   $("#AddCharge").removeAttr("style").hide();
						   $("#UpdateCharge").show();
						} 
						else
						{
							alert("failure111");
						}
				  } 

		}); 
	
}

function UpdateOtherTask()
{
	clearAll();
	
	//validation for employee name
	if(document.taskform.taskDescription.value=="")
	{
		$('#taskDescriptionSpan').html('Description should not be empty..!');
		document.taskform.taskDescription.value="";
		document.taskform.taskDescription.focus();
		
		return false;
	}
	
	//validation for employee email id
	if(document.taskform.otherTaskStatus.value=="Default")
	{
		$('#otherTaskStatusSpan').html('Please, select status..!');
		document.taskform.otherTaskStatus.focus();
		return false;
	}
	
	if(document.taskform.startDate.value=="")
	{
		$('#startDateSpan').html('Please, select start date..!');
		document.taskform.startDate.focus();
		return false;
	}
	
	if(document.taskform.expectedEndDate.value=="")
	{
		$('#expectedEndDateSpan').html('Please, select expected end date..!');
		document.taskform.expectedEndDate.focus();
		return false;
	}
	
	
	$('#taskHistoryTable tr').detach();
	
	var taskId              = $('#taskId').val();
	var taskHistoryId		= $('#taskHistoryId').val();
	var taskDescription 	= $('#taskDescription').val();
	var otherTaskStatus     = $('#otherTaskStatus').val();
	var startDate 			= $('#startDate').val();
	var expectedEndDate     = $('#expectedEndDate').val();
	var actualEndDate 		= $('#actualEndDate').val();
	
	$.ajax({

		 url : '${pageContext.request.contextPath}/UpdateOtherTasks',
		type : 'Post',
		data : { taskId : taskId, taskHistoryId : taskHistoryId, taskDescription : taskDescription, otherTaskStatus : otherTaskStatus, startDate : startDate, expectedEndDate : expectedEndDate, actualEndDate : actualEndDate},
		dataType : 'json',
		success : function(result)
				  {
					   if (result) 
					   { 
									$('#taskHistoryTable').append('<tr style="background-color: #4682B4;"><th style="width:300px">Task Description</th><th style="width:150px">Status</th><th style="width:30px">Start Date</th><th style="width:30px">Expected End Date</th><th style="width:30px">Actual End Date</th><th style="width:30px">Action</th>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].taskHistoryId;
									$('#taskHistoryTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].taskDescription+'</td><td>'+result[i].otherTaskStatus+'</td><td>'+result[i].startDate1+'</td><td>'+result[i].expectedEndDate1+'</td><td>'+result[i].actualEndDate1+'</td><td><a onclick="EditOtherTask('+id+')" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a> </td>');
								}
								else
								{
									var id = result[i].taskHistoryId;
									$('#taskHistoryTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].taskDescription+'</td><td>'+result[i].otherTaskStatus+'</td><td>'+result[i].startDat1e+'</td><td>'+result[i].expectedEndDate1+'</td><td>'+result[i].actualEndDate1+'</td><td><a onclick="EditOtherTask('+id+')" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a> </td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}
				  } 

		});
	
	 $('#taskDescription').val("");
	 $('#startDate').val("");
	 $('#expectedEndDate').val("");
	 $('#actualEndDate').val("");
} 

function copyEndDate()
{
   var expectedEndDate = $('#expectedEndDate').val();
   
   $('#actualEndDate').val(expectedEndDate);
}

$(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
 
    $('#datepicker1').datepicker({
      autoclose: true
    })
    
    $('#startDate').datepicker({
      autoclose: true
    })
    
    $('#expectedEndDate').datepicker({
      autoclose: true
    })
    
    $('#actualEndDate').datepicker({
      autoclose: true
    })
    
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate |Project Building Master</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- Google Font -->
    <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>

<div class="wrapper">

      <%@ include file="headerpage.jsp" %>
  
  <!-- Left side column. contains the logo and sidebar -->
    <%@ include file="menu.jsp" %>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Project Building Master Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active"> Project Building Master</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="projectbuildingmasterform" action="${pageContext.request.contextPath}/ProjectBuildingMaster" onSubmit="return validate()" method="post">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              
       		
			  <div class="box-body">
          <div class="row">
            <div class="col-md-6">
                  <label>Project Name</label>
                  <select class="form-control" name="projectId" id="projectId" onchange="getProjectBuildingList(this.value)">
				  <option selected="" value="Default">-Select Project-</option>
				    <c:forEach var="projectList" items="${projectList}">
	                    <option value="${projectList.projectId}">${projectList.projectName}</option>
	                  </c:forEach>
                  </select>
                </div>
				</div>
				</div>
				
				
				
              <!-- /.form-group -->
            </div>
            
            <!-- /.col -->
			
          </div>
		   	 
			     <div class="box-body">
              <div class="row">
                 
				  <div class="col-xs-3">
				  <!--
				  <div class="input-group">
              	<input type="text" id="stateName" name="stateName" placeholder="State Name" class="form-control ui-autocomplete-input" autocomplete="off" oninput="stateSearch(this.value)">
                   
                 
              <span class="input-group-btn">
                <button type="button" name="search" id="search-btn" class="btn btn-flat" ><i class="fa fa-search"></i></button>
              </span>
              
            </div>
            -->
            </div>
					<div class="col-xs-3">
			 &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 
			 <a href="ProjectBuildingMaster">  <button type="button" class="btn btn-default" value="reset" style="width:90px"> Reset</button></a>
              </div> 
              <div class="col-xs-2">
			      <a href="AddProjectBuilding"> <button type="button" class="btn btn-success"><i class="fa fa-plus"></i> Add New Project Building</button></a>
            </div>
			   
			       <div class="col-xs-3">
			       <a href="ImportNewProjectBuilding">  <button type="button" class="btn btn-info pull-right" style="background:#48D1CC"> Import From Excel File</button></a>
                </div>    
			  </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
              
         </div>
      <!-- /.box -->
	
			<div class="box box-default">
			 <div  class="panel box box-danger"></div>
        	 <div class="box-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="projectbuildingListTable">
                  <thead>
	                  <tr bgcolor="#4682B4">
	                     <th style="width:300px">Project Name</th>
	                    <th style="width:300px">Building Name</th>
	                    <th style="width:300px">Building Status</th>
	                    <th style="width:150px">No. Of Wing</th>
	                    <th style="width:50px">Action</th>
	                  </tr>
                  </thead>
                  <tbody >
                   <c:forEach items="${buildingList}" var="buildingList" varStatus="loopStatus">
                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                       		<td>${buildingList.projectId}</td>
	                        <td>${buildingList.buildingName}</td>
	                        <td>${buildingList.buildingStatus}</td>
	                        <td>${buildingList.numberofWing}</td>
	                        <td><a href="${pageContext.request.contextPath}/EditProjectBuilding?buildingId=${buildingList.buildingId}" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>
                      </tr>
				   </c:forEach>
                 </tbody>
                </table>
              </div>
            </div>
	 </div>
   </div>  
  </section>
</form>
</div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
       <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script>

function getProjectBuildingList()
{
	 //$("#stateListTable").empty();
	 $("#projectbuildingListTable tr").detach();
	 var projectId = $('#projectId').val();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getbuildingList',
		type : 'Post',
		data : { projectId : projectId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
							
									$('#projectbuildingListTable').append('<tr style="background-color: #4682B4;"><td style="width:150px"><b>Building Id</b></td><td style="width:300px"><b>Building Name</b></td><td style="width:300px"><b>Project Name</b></td><td style="width:300px"><b>Building Status</b></td><td style="width:150px"><b>No. Of Wings</b></td><td><b>Action</b></td>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].buildingId;
									$('#projectbuildingListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].buildingId+'</td><td>'+result[i].buildingName+'</td><td>'+result[i].projectId+'</td><td>'+result[i].buildingStatus+'</td><td>'+result[i].numberofWing+'</td><td><a href="${pageContext.request.contextPath}/EditProjectBuilding?buildingId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
								else
								{
									var id = result[i].buildingId;
									$('#projectbuildingListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].buildingId+'</td><td>'+result[i].buildingName+'</td><td>'+result[i].projectId+'</td><td>'+result[i].buildingStatus+'</td><td>'+result[i].numberofWing+'</td><td><a href="${pageContext.request.contextPath}/EditProjectBuilding?buildingId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
							
							 } 
						
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
}//end of get State List


$(function () {
    $('#projectbuildingListTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>

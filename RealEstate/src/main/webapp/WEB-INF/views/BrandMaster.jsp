<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>RealEstate | Brand Master</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

    <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
    <section class="content-header">
      <h1>
        Brand Master Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active"> Brand Master</li>
      </ol>
    </section>
    <!-- Main content -->
	<form name="brandmasterform" action="${pageContext.request.contextPath}/BrandMaster" onSubmit="return validate()" method="post">

    <section class="content">
    
    <div class="box box-default">
        
                <div class="box-body">
			<div class="box-body">
			
			  <div class="row">
                
             
               <div class="col-xs-3">
			      <label>Item Main Category</label> <label class="text-red">* </label>
                  <select class="form-control" name="suppliertypeId" id="suppliertypeId" onchange="getMainSubItemCategory(this.value)">
				  <option selected="" value="Default">-Select Category-</option>
                   <c:forEach var="suppliertypeList" items="${suppliertypeList}" >
					<option value="${suppliertypeList.suppliertypeId}">${suppliertypeList.supplierType}</option>
					 </c:forEach>
                  </select>
			         <span id="suppliertypeIdSpan" style="color:#FF0000"></span>
			     </div> 
                   <div class="col-xs-3">
			      <label>Item Main Sub-Category</label> <label class="text-red">* </label>
                  <select class="form-control" name="subsuppliertypeId" id="subsuppliertypeId" onchange="getBrandList(this.value)">
				  <option selected="" value="Default">-Select Sub Item Category-</option>
                   <c:forEach var="itemcategoryList" items="${itemcategoryList}" >
					<option value="${itemcategoryList.subsuppliertypeId}">${itemcategoryList.itemcategoryName}</option>
					 </c:forEach>
                  </select>
			         <span id="subsuppliertypeIdSpan" style="color:#FF0000"></span>
			     </div>
               </div>
			
			  </div>
			  
			  <div class="box-body">
	              <div class="row">
				  
						<div class="col-xs-3">
			 		&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 
			 		<a href="BrandMaster">  <button type="button" class="btn btn-default" value="reset" style="width:90px"> Reset</button></a>
                </div> 
              
              	<div class="col-xs-2">
			        <a href="AddBrand"> <button type="button" class="btn btn-success"><i class="fa fa-plus"></i> Add New Brand</button></a>
            	</div>
				  </div>
			  </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
      </div>
      <div class="box box-default">
			<div  class="panel box box-danger"></div>
        	 <div class="box-body">
              <div class="table-responsive">
                <table id="brandListTable" class="table table-bordered">
                <thead>
                <tr bgcolor="#4682B4">
                <th style="width:150px">Category Name</th>
                <th style="width:150px">Sub-Category Name</th>
                <th style="width:300px">Brand Name</th>
                <th style="width:50px">Action</th>
                </tr>
                </thead>
                <tbody>
             
                  <c:forEach items="${brandList}" var="brandList" varStatus="loopStatus">
                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                      <td>${brandList.suppliertypeId}</td>
                      <td>${brandList.subsuppliertypeId}</td>
                        <td>${brandList.brandName}</td> 
                        <td><a href="${pageContext.request.contextPath}/EditBrand?brandId=${brandList.brandId}" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>
                      </tr>
				   </c:forEach>
                </tbody>
              </table>
              </div>
            </div>
	 </div>
     
  </section>
	</form>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <!-- Control Sidebar -->
 <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- page script -->
<script>

function getMainSubItemCategory()
{
	$('#subsuppliertypeId').empty();
	var suppliertypeId = $('#suppliertypeId').val();
	var suppliertypeId=$('#suppliertypeId').val();
	$.ajax({

		url : '${pageContext.request.contextPath}/getSubSupplierTypeList',
		type : 'Post',
		data : { suppliertypeId : suppliertypeId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select Sub Item Category-");
								$("#subsuppliertypeId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].subsuppliertypeId).text(result[i].subsupplierType);
							    $("#subsuppliertypeId").append(option);
							} 
						} 
						else
						{
							alert("failure111");
						}

					}
		});	
	
	
$("#brandListTable tr").detach();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getItemCategoryWiseBrandList',
		type : 'Post',
		data : { suppliertypeId : suppliertypeId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
							$('#brandListTable').append('<tr style="background-color: #4682B4;"><td>Item Category Name</td><td>Item Sub Category</td><td>Brand Name</td><td>Action</td>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].brandId;
									$('#brandListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].suppliertypeId+'</td><td>'+result[i].subsuppliertypeId+'</td><td>'+result[i].brandName+'</td><td><a href="${pageContext.request.contextPath}/EditBrand?brandId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
								else
								{
									var id = result[i].brandId;
									$('#brandListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].suppliertypeId+'</td><td>'+result[i].subsuppliertypeId+'</td><td>'+result[i].brandName+'</td><td><a href="${pageContext.request.contextPath}/EditBrand?brandId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		}); 
	
}
function getBrandList()
{
	var subsuppliertypeId = $('#subsuppliertypeId').val();
	var suppliertypeId=$('#suppliertypeId').val();
	
	$("#brandListTable tr").detach();
		
		$.ajax({

			url : '${pageContext.request.contextPath}/getItemSubCategoryWiseBrandList',
			type : 'Post',
			data : { suppliertypeId : suppliertypeId, subsuppliertypeId : subsuppliertypeId},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{ 
								$('#brandListTable').append('<tr style="background-color: #4682B4;"><td>Item Category Name</td><td>Item Sub Category</td><td>Brand Name</td><td>Action</td>');
							
								for(var i=0;i<result.length;i++)
								{ 
									if(i%2==0)
									{
										var id = result[i].brandId;
										$('#brandListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].suppliertypeId+'</td><td>'+result[i].subsuppliertypeId+'</td><td>'+result[i].brandName+'</td><td><a href="${pageContext.request.contextPath}/EditBrand?brandId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
									}
									else
									{
										var id = result[i].brandId;
										$('#brandListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].suppliertypeId+'</td><td>'+result[i].subsuppliertypeId+'</td><td>'+result[i].brandName+'</td><td><a href="${pageContext.request.contextPath}/EditBrand?brandId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
									}
								 } 
							} 
							else
							{
								alert("failure111");
							}

						}
			}); 
}
  $(function () {
    $('#brandListTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>

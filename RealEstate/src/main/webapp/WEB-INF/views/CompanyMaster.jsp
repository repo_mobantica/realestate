<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Company Master</title>
  <!-- Tell the browser to be responsive to screen width -->
   <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- Google Font -->
  <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
 
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

   <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Company Master Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Company Master</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="Companymasterform" action="${pageContext.request.contextPath}/CompanyMaster" method="post">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
        
          <div class="box-body">
          <div class="row">
            <div class="col-md-3">
                  <label>Country</label> 
                  <select class="form-control" id="countryId" name="countryId" onchange="getStateList(this.value)">
				  <option selected="" value="Default">Select Country-</option>
				  
				  <s:forEach var="countryList" items="${countryList}">
				    <option value="${countryList.countryId}">${countryList.countryName}</option>
                  </s:forEach> 
                    
                  </select>
                </div>
                   <div class="col-md-3">
                  <label>State</label>
                  <select class="form-control" id="stateId" name="stateId" onchange="getCityList(this.value)">
				  
				  	<option selected="" value="Default">-Select State-</option>
				  <s:forEach var="stateList" items="${stateList}">
                    <option value="${stateList.stateId}">${stateList.stateId}</option>
				  </s:forEach>
                  </select>
                </div>
                  <div class="col-xs-3">
				  <label>City</label> 
				   <select class="form-control" name="cityId" id="cityId" onchange="getLocationAreaList(this.value)">
				  		<option selected="" value="Default">Select City-</option>
                     <s:forEach var="cityList" items="${cityList}">
                    	<option value="${cityList.cityId}">${cityList.cityId}</option>
				    </s:forEach>       
                  </select>
                  </div>
                  
                  <div class="col-xs-3">
				    <label>Area</label> 
				   <select class="form-control" name="locationareaId" id="locationareaId" onchange="getCompanyList(this.value)">
				  		<option selected="" value="Default">Select a Area-</option>
                     <s:forEach var="locationareaList" items="${locationareaList}">
                    	<option value="${locationareaList.locationareaId}">${locationareaList.locationareaId}</option>
				     </s:forEach>
                   </select>
                  </div> 
                </div>
               </div>
                
		 </div>
    </div>
		  	     <div class="box-body">
                <div class="row">
                 <br/>
                 
				  <div class="col-xs-3">
			  		
			      </div>
			     
				 <div class="col-xs-3">
			 		&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
			  		<a href="CompanyMaster"><button type="button" class="btn btn-default" value="reset" style="width:90px">Reset</button></a>
                 </div> 
              	 <div class="col-xs-2">
			      	<a href="AddCompany"> <button type="button" class="btn btn-success"><i class="fa fa-plus"></i>Add New Company</button></a>
            	 </div>
			  </div>
			</div>
          <!-- /.row -->
        </div>
        
      </div>
      <!-- /.box -->
				
				<div class="box box-default">
         <div  class="panel box box-danger"></div>
          
              <!-- /.form-group -->
          
			 <div class="box-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="CompanyListTable">
                  <thead>
	                  <tr bgcolor=#4682B4>
	                    <th style="width:300px">Company Name</th>
	                    <th style="width:150px">Company Type</th>
	                    <th style="width:200px">City Name</th>
	                    <th style="width:200px">State Name</th>
	                    <th style="width:200px">Phone No.</th>
	                    <th style="width:150px">Pan No.</th>
	                    <th style="width:150px">GST No.</th>
	                    <th style="width:150px">Financial Year</th>
	                    <th style="width:50px">Action</th> 
	                  </tr>
                  </thead>
                  <tbody>
                   <s:forEach items="${companyList}" var="companyList" varStatus="loopStatus">
                    <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
	                        <td>${companyList.companyName}</td>
	                        <td>${companyList.companyType}</td>
	                        <td>${companyList.cityId}</td>
	                        <td>${companyList.stateId}</td>
	                        <td>${companyList.companyPhoneno}</td>
	                        <td>${companyList.companyPanno}</td>
	                        <td>${companyList.companyGstno}</td>
	                        <td>${companyList.companyFinancialyear}</td>
	                        <td><a href="${pageContext.request.contextPath}/EditCompany?companyId=${companyList.companyId}" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>
                      </tr>
					</s:forEach>
                 
                 </tbody>
                </table>
              </div>
            </div>
				
              <!-- /.form-group -->
          
        <!-- /.box-body -->
          
      </div>
     
    </section>
	</form>
    <!-- /.content -->
    
  </div>
 
  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
       
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script>



//function to get country wise state list------------------------------------
function getStateList()
{
	 $("#stateId").empty();
	 var countryId = $('#countryId').val();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getStateList',
		type : 'Post',
		data : { countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select State-");
							$("#stateId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].stateId).text(result[i].stateName);
							    $("#stateId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
	// to retrive all value in table..........
	// for clear table
	$("#CompanyListTable tr").detach();
	
	
	 var countryId = $('#countryId').val();
	 
	 $("#stateId").empty(); // for clear the State
	 $("#cityId").empty(); // for clear city
		var option = $('<option/>');// to add select option in city
		option.attr('value',"Default").text("-Select City-");
		
		$("#cityId").append(option);
		 $("#locationareaId").empty();
		 var option = $('<option/>');
		 option.attr('value',"Default").text("-Select Location Area-");
		 $("#locationareaId").append(option);
		
	$.ajax({
		//alert("hjhjh");

		url : '${pageContext.request.contextPath}/getCountryCompanyList',
		type : 'Post',
		data : { countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
									$('#CompanyListTable').append('<tr style="background-color: #4682B4;"><th style="width:300px">Company Name</th><th style="width:150px">Company Type</th><th style="width:200px">City Name</th><th style="width:200px">State Name</th><th style="width:200px">Phone No.</th><th style="width:150px">Pan No.</th> <th style="width:150px">GST No.</th> <th style="width:150px">Financial Year</th><td>Action</td>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].companyId;
									$('#CompanyListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].companyName+'</td><td>'+result[i].companyType+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].companyPhoneno+'</td><td>'+result[i].companyPanno+'</td><td>'+result[i].companyGstno+'</td><td>'+result[i].companyFinancialyear+'</td><td><a href="${pageContext.request.contextPath}/EditCompany?companyId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
								else
								{
									$('#CompanyListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].companyName+'</td><td>'+result[i].companyType+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].companyPhoneno+'</td><td>'+result[i].companyPanno+'</td><td>'+result[i].companyGstno+'</td><td>'+result[i].companyFinancialyear+'</td><td><a href="${pageContext.request.contextPath}/EditCompany?companyId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
}//end of get State List



function getCityList()
{
	 $("#cityId").empty();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();
	$.ajax({

		url : '${pageContext.request.contextPath}/getCityList',
		type : 'Post',
		data : { stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select City-");
							$("#cityId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].cityId).text(result[i].cityName);
							    $("#cityId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
	
	// to retrive all value in table..........
	
	$("#CompanyListTable tr").detach();
	 var stateId = $('#stateId').val();
	 $("#locationareaId").empty();
	 var option = $('<option/>');
	 option.attr('value',"Default").text("-Select Location Area-");
	 $("#locationareaId").append(option);
	$.ajax({
		//alert("hjhjh");

		url : '${pageContext.request.contextPath}/getCityWiseCompanyList',
		type : 'Post',
		data : { stateId : stateId},
		dataType : 'json',
		success : function(result)
				  {

						if (result) 
						{ 
									$('#CompanyListTable').append('<tr style="background-color: #4682B4;"><th style="width:300px">Company Name</th><th style="width:150px">Company Type</th><th style="width:200px">City Name</th><th style="width:200px">State Name</th><th style="width:200px">Phone No.</th><th style="width:150px">Pan No.</th> <th style="width:150px">GST No.</th> <th style="width:150px">Bank A/C No.</th> <th style="width:150px">Bank Name</th><th style="width:150px">Financial Year</th><td>Action</td>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].companyId;
									$('#CompanyListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].companyName+'</td><td>'+result[i].companyType+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].companyPhoneno+'</td><td>'+result[i].companyPanno+'</td><td>'+result[i].companyGstno+'</td><td>'+result[i].companyBankacno+'</td><td>'+result[i].companyBankname+'</td><td>'+result[i].companyFinancialyear+'</td><td><a href="${pageContext.request.contextPath}/EditCompany?companyId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
								else
								{
									$('#CompanyListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].companyName+'</td><td>'+result[i].companyType+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].companyPhoneno+'</td><td>'+result[i].companyPanno+'</td><td>'+result[i].companyGstno+'</td><td>'+result[i].companyBankacno+'</td><td>'+result[i].companyBankname+'</td><td>'+result[i].companyFinancialyear+'</td><td><a href="${pageContext.request.contextPath}/EditCompany?companyId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}

				}
		});
		
}//end of get City List


function getLocationAreaList()
{
	 //to retrive all banks by city name
	 $("#CompanyListTable tr").detach();
	
	 $("#locationareaId").empty();
	 
	 var cityId = $('#cityId').val();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();
	
	 $.ajax({

		url : '${pageContext.request.contextPath}/getLocationAreaList',
		type : 'Post',
		data : { countryId : countryId, stateId : stateId, cityId : cityId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select City-");
							$("#locationareaId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].locationareaId).text(result[i].locationareaName);
							    $("#locationareaId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	 
	 
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getLocationAreaWiseCompanyList',
		type : 'Post',
		data : { cityId : cityId},
		dataType : 'json',
		success : function(result)
			      {

						if (result) 
						{ 
									$('#CompanyListTable').append('<tr style="background-color: #4682B4;"><th style="width:300px">Company Name</th><th style="width:150px">Company Type</th><th style="width:200px">City Name</th><th style="width:200px">State Name</th><th style="width:200px">Phone No.</th><th style="width:150px">Pan No.</th> <th style="width:150px">GST No.</th> <th style="width:150px">Bank A/C No.</th> <th style="width:150px">Bank Name</th><th style="width:150px">Financial Year</th><td>Action</td>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].companyId;
									$('#CompanyListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].companyName+'</td><td>'+result[i].companyType+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].companyPhoneno+'</td><td>'+result[i].companyPanno+'</td><td>'+result[i].companyGstno+'</td><td>'+result[i].companyBankacno+'</td><td>'+result[i].companyBankname+'</td><td>'+result[i].companyFinancialyear+'</td><td><a href="${pageContext.request.contextPath}/EditCompany?companyId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
								else
								{
									$('#CompanyListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].companyName+'</td><td>'+result[i].companyType+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].companyPhoneno+'</td><td>'+result[i].companyPanno+'</td><td>'+result[i].companyGstno+'</td><td>'+result[i].companyBankacno+'</td><td>'+result[i].companyBankname+'</td><td>'+result[i].companyFinancialyear+'</td><td><a href="${pageContext.request.contextPath}/EditCompany?companyId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}

				}
		});
	
}//end of get locationarea List



function getCompanyList()
{
	
	 //to retrive all banks by Location Area name
	$("#CompanyListTable tr").detach();
	
	 var locationareaId = $('#locationareaId').val();

	 $.ajax({

		 url : '${pageContext.request.contextPath}/getCompanyList',
		type : 'Post',
		data : { locationareaId : locationareaId},
		dataType : 'json',
		success : function(result)
			  {

					if (result) 
					{ 
								$('#CompanyListTable').append('<tr style="background-color: #4682B4;"><th style="width:300px">Company Name</th><th style="width:150px">Company Type</th><th style="width:200px">City Name</th><th style="width:200px">State Name</th><th style="width:200px">Phone No.</th><th style="width:150px">Pan No.</th> <th style="width:150px">GST No.</th> <th style="width:150px">Bank A/C No.</th> <th style="width:150px">Bank Name</th><th style="width:150px">Financial Year</th><td>Action</td>');
					
						for(var i=0;i<result.length;i++)
						{ 
							if(i%2==0)
							{
								var id = result[i].companyId;
								$('#CompanyListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].companyName+'</td><td>'+result[i].companyType+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].companyPhoneno+'</td><td>'+result[i].companyPanno+'</td><td>'+result[i].companyGstno+'</td><td>'+result[i].companyBankacno+'</td><td>'+result[i].companyBankname+'</td><td>'+result[i].companyFinancialyear+'</td><td><a href="${pageContext.request.contextPath}/EditCompany?companyId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
							}
							else
							{
								$('#CompanyListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].companyName+'</td><td>'+result[i].companyType+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].companyPhoneno+'</td><td>'+result[i].companyPanno+'</td><td>'+result[i].companyGstno+'</td><td>'+result[i].companyBankacno+'</td><td>'+result[i].companyBankname+'</td><td>'+result[i].companyFinancialyear+'</td><td><a href="${pageContext.request.contextPath}/EditCompany?companyId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
							}
						
						 } 
					} 
					else
					{
						alert("failure111");
					}

				}
		});
	
}//end of get locationarea List

/* function CompanyNameWiseSearch()
{
	$("#CompanyListTable tr").detach();
	
	 var companyName = $('#companyName').val();

	 $.ajax({

		 url : '${pageContext.request.contextPath}/searchCompanyNameWiseList',
		type : 'Post',
		data : { companyName : companyName},
		dataType : 'json',
		success : function(result)
			  {

					if (result) 
					{ 
								$('#CompanyListTable').append('<tr style="background-color: #4682B4;"><th style="width:300px">Company Name</th><th style="width:150px">Company Type</th><th style="width:200px">City Name</th><th style="width:200px">State Name</th><th style="width:200px">Phone No.</th><th style="width:150px">Pan No.</th> <th style="width:150px">GST No.</th> <th style="width:150px">Bank A/C No.</th> <th style="width:150px">Bank Name</th><th style="width:150px">Financial Year</th><td>Action</td>');
					
						for(var i=0;i<result.length;i++)
						{ 
							if(i%2==0)
							{
								var id = result[i].companyId;
								$('#CompanyListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].companyName+'</td><td>'+result[i].companyType+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].companyPhoneno+'</td><td>'+result[i].companyPanno+'</td><td>'+result[i].companyGstno+'</td><td>'+result[i].companyBankacno+'</td><td>'+result[i].companyBankname+'</td><td>'+result[i].companyFinancialyear+'</td><td><a href="${pageContext.request.contextPath}/EditCompany?companyId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
							}
							else
							{
								$('#CompanyListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].companyName+'</td><td>'+result[i].companyType+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].companyPhoneno+'</td><td>'+result[i].companyPanno+'</td><td>'+result[i].companyGstno+'</td><td>'+result[i].companyBankacno+'</td><td>'+result[i].companyBankname+'</td><td>'+result[i].companyFinancialyear+'</td><td><a href="${pageContext.request.contextPath}/EditCompany?companyId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
							}
						
						 } 
					} 
					else
					{
						alert("failure111");
					}

				}
		});
} */



$(function () {
    $('#CompanyListTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>

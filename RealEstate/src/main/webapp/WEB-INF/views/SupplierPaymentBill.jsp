<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>RealEstate | Supplier Payment Bill</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


  <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">
	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

    <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
    <section class="content-header">
      <h1>
        Supplier Payment Bill Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Account</a></li>
        <li class="active"> Supplier Payment Bill</li>
      </ol>
    </section>
    <!-- Main content -->
	<form name="supplierpaymentbillform" action="${pageContext.request.contextPath}/SupplierPaymentBill" onSubmit="return validate()" method="post">

    <section class="content">
    <div class="box box-default">
      
      <div class="box-body">
      <div class="row">
     
        <div class="col-xs-12">
       
        </br> </br>
        
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#RemainingPaymentList" data-toggle="tab">Remaining Bill Payment List</a></li>
              <li><a href="#CompletedBillList" data-toggle="tab">Completed Bill Payment</a></li>
            </ul>
            
            
            
            <div class="tab-content">
              <div class="tab-pane active" id="RemainingPaymentList">
                <section >
              <div  class="panel box box-danger">
              </br>
              
              
              <div class="table-responsive">
                <table class="table table-bordered" id="contractorPaymentListTable">
                  <thead>
	                  <tr bgcolor=#4682B4>
	                 	<th>#</th>
	                 	<th>Purchase Id</th>
	                 	<th>Supplier Firm Name</th>
	                 	<th>Store Name</th>
	                    <th>PO Date</th>
	                    <th>Requisition Id</th>
	                 	<!-- 
	                    <th>Total</th>
	                    <th>Discount</th>
	                    
	                    <th>GST</th>
	            		<th>Grand Amount</th> 
	            		-->
	                    <th style="width:70px">Action</th>
	                  </tr>
                  </thead>
                  
             <tbody>
                  <c:forEach items="${purchesedList}" var="purchesedList" varStatus="loopStatus">
                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                        <td>${purchesedList.number}</td>
                        <td>${purchesedList.materialsPurchasedId}</td>
                        <td>${purchesedList.supplierId}</td>
                        <td>${purchesedList.storeId}</td>
                        <td>${purchesedList.creationDate}</td>
                        <td>${purchesedList.requisitionId}</td>
                        <%-- 
                        <td>${purchesedList.totalPrice}</td>
                        <td>${purchesedList.totalDiscount}</td>
                        
                        <td>${purchesedList.totalGstAmount}</td>
                        <td>${purchesedList.totalAmount}</td>
                        --%>
                        <td>
                        <!-- 
                        AddSupplierBillPayment
                         -->
                      <a href="${pageContext.request.contextPath}/AddSupplierBillPayment?materialsPurchasedId=${purchesedList.materialsPurchasedId}" class="btn btn-success btn-sm" data-toggle="tooltip" title="View"><i class="glyphicon glyphicon-eye-open"></i></a>
                      |&nbsp<a target="_blank" href="${pageContext.request.contextPath}/MaterialPurchaseBillReceipt?materialsPurchasedId=${purchesedList.materialsPurchasedId}" class="btn btn-info btn-sm" data-toggle="tooltip" title="Print Bill"><span class="glyphicon glyphicon-print"></span></a>
                          </td>
                      </tr>
					</c:forEach> 
                 </tbody>
                </table>
              </div>
            </div> 
           </section>
		</div>
		    
                      
       <div class="tab-pane" id="CompletedBillList">
        <section >
          <div  class="panel box box-danger">
          </br>
          <div class="table-responsive">
                <table class="table table-bordered" id="CompletedBilltable">
                  <thead>
	                  <tr bgcolor=#4682B4>
	                 	<th style="width:100px">Purchase Id</th>
	                 	<th>Supplier Firm Name</th>
	                 	<th>Store Name</th>
	                 	
	                    <th>Total</th>
	                    <th>Discount</th>
	                    
	                    <th>GST</th>
	            		<th>Grand Amount</th>
	                    <th style="width:70px">Bill Print</th>
	                  </tr>
                  </thead>
                  
             <tbody>
                   <c:forEach items="${purchesedClearPaymentList}" var="purchesedClearPaymentList" varStatus="loopStatus">
                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                        <td>${purchesedClearPaymentList.materialsPurchasedId}</td>
                        <td>${purchesedClearPaymentList.supplierId}</td>
                        <td>${purchesedClearPaymentList.storeId}</td>
                        
                        <td>${purchesedClearPaymentList.totalPrice}</td>
                        <td>${purchesedClearPaymentList.totalDiscount}</td>
                        
                        <td>${purchesedClearPaymentList.totalGstAmount}</td>
                        <td>${purchesedClearPaymentList.totalAmount}</td>
                       
                        <td>
                      |&nbsp<a target="_blank" href="${pageContext.request.contextPath}/MaterialPurchaseBillReceipt?materialsPurchasedId=${purchesedClearPaymentList.materialsPurchasedId}" class="btn btn-info btn-sm" data-toggle="tooltip" title="Print Bill"><span class="glyphicon glyphicon-print"></span></a>
                         </td>
                      </tr>
					</c:forEach>
                 </tbody>
                </table>
              </div>
              </div> 
             </section>
		</div>
 
	</div>
   </div>
 </div>
  </div>
  </div>
  </div>
    
    
  </section>
	</form>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <!-- Control Sidebar -->
 <%@ include file="footer.jsp" %>

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- page script -->
<script>

function calcuate(i)
{
	var oTable = document.getElementById('materialDetailsTable');

	var oCells = oTable.rows.item(i).cells;
	
	var quantity=0;
	quantity=Number(oCells.item(4).innerHTML); 
	
	var rate=0;
	rate=Number(oCells.item(8).innerHTML); 

	var discountPer1=0;
	discountPer1=Number(oCells.item(10).innerHTML); 
	var gstPer1=0;
	gstPer1=Number(oCells.item(12).innerHTML); 
	
	var discountAmount1=0;
	var gstAmount1=0;
	var grandTotal1=0;
	
	var total1=0;
	total1=quantity*rate;
	
	oCells.item(9).innerHTML=total1.toFixed(1);
	discountAmount1=(total1/100)*discountPer1;
	
	var ab=discountAmount1.toFixed(0);
	oCells.item(11).innerHTML=ab;
	var a=0;
	a=total1-discountAmount1;
	gstAmount1=(a/100)*gstPer1;
	
	var xy=gstAmount1.toFixed(0);
	oCells.item(13).innerHTML=xy;
	grandTotal1=a+gstAmount1;
	var pq=grandTotal1.toFixed(0);
	oCells.item(14).innerHTML=pq;
	
	var rowLength = oTable.rows.length;
	var grandTotal=0;
	var total=0;
	var discountAmount=0;
	var gstAmount=0;
	
	for (var j = 1; j < rowLength-1; j++){

		   var oCells = oTable.rows.item(j).cells;

		   var cellLength = oCells.length;
		  
		   total=total+Number(oCells.item(9).innerHTML);
		   discountAmount=discountAmount+Number(oCells.item(11).innerHTML);
		   gstAmount=gstAmount+Number(oCells.item(13).innerHTML);
		   grandTotal=grandTotal+Number(oCells.item(14).innerHTML);
			
			
	}		
	document.getElementById('total1').innerHTML = total;
	document.materialform.total.value=total;
	
	document.getElementById('discountAmount1').innerHTML = discountAmount;
	document.materialform.discountAmount.value=discountAmount;
	document.getElementById('gstAmount1').innerHTML = gstAmount;
	document.materialform.gstAmount.value=gstAmount;
	document.getElementById('grandTotal1').innerHTML = grandTotal;
	document.materialform.grandTotal.value=grandTotal; 
	//document.getElementById('grandTotal').innerHTML = grandTotal;
}



  $(function () {
    $('#contractorPaymentListTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
  
  
  $(function () {
	    $('#CompletedBilltable').DataTable()
	    $('#example2').DataTable({
	      'paging'      : true,
	      'lengthChange': false,
	      'searching'   : false,
	      'ordering'    : true,
	      'info'        : true,
	      'autoWidth'   : false
	    })
	  })
	  
  
</script>
</body>
</html>

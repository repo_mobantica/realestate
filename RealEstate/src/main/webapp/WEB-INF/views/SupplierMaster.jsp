<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Supplier Master</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- Google Font -->
  <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
 
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

     <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Supplier Master Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Supplier Master</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="suppliermasterform" action="${pageContext.request.contextPath}/SupplierMaster" method="post">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
        
          <div class="box-body">
          
                </div>
                
              <!-- /.form-group -->
            </div>
            
            <!-- /.col -->
			
          </div>
		  	     <div class="box-body">
                <div class="row">
                
                 <div class="col-xs-5">
			
           
            	 </div>
            	
      
            	
            	<br/>
				<div class="col-xs-3">
			 		&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
			  		<a href="SupplierMaster">  <button type="button" class="btn btn-default" value="reset" style="width:90px"> Reset</button></a>
               </div> 
               <div class="col-xs-3">
			      <a href="AddSupplier"> <button type="button" class="btn btn-success"><i class="fa fa-plus"></i> Add New Supplier</button></a>
               </div>
			       
			  </div>
			</div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        
      </div>
      <!-- /.box -->
				
		<div class="box box-default">
        
         <div  class="panel box box-danger"></div>
          
			 <div class="box-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="supplierListTable">
                  <thead>
                  <tr bgcolor=#4682B4>
	                 	<th style="width:150px">Supplier Id</th>
	                    <th style="width:300px">Firm Name</th>
	                    <th style="width:150px">Firm Type</th>
	                    <th style="width:150px">Address</th>
	                    <th style="width:150px">GST Number</th>
	                    <th style="width:200px">Supplier Type</th>
	                    <th style="width:50px">Action</th>
	                  </tr>
                  </thead>
                  <tbody>
                   <s:forEach items="${supplierList}" var="supplierList" varStatus="loopStatus">
                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                            <td>${supplierList.supplierId}</td>
	                        <td>${supplierList.supplierfirmName}</td>
	                        <td>${supplierList.supplierfirmType}</td>
	                        <td>${supplierList.supplierfirmAddress}</td>
	                        <td>${supplierList.firmgstNumber}</td>
	                        <td>${supplierList.suppliertypeId}</td>
	                        <td><a href="${pageContext.request.contextPath}/EditSupplier?supplierId=${supplierList.supplierId}" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>
                      </tr>
					</s:forEach>
                 
                 </tbody>
                </table>
              </div>
            </div>
				
              <!-- /.form-group -->
          
        <!-- /.box-body -->
          
      </div>
     
    </section>
	</form>
    <!-- /.content -->
    
  </div>
 
  <!-- Control Sidebar -->
       <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
       
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>

<script>

//function to get country wise state list------------------------------------
function getStateList()
{
	 $("#stateId").empty();
	 var countryId = $('#countryId').val();
	
	 $.ajax({

		url : '${pageContext.request.contextPath}/getStateList',
		type : 'Post',
		data : { countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select State-");
							$("#stateId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].stateId).text(result[i].stateName);
							    $("#stateId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	 
	    //Get country wise suppliers list
	     $("#supplierListTable tr").detach();
		
		 $.ajax({
	
			 url : '${pageContext.request.contextPath}/getCountryWiseSupplierList',
			type : 'Post',
			data : { countryId : countryId},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{ 
								$('#supplierListTable').append('<tr style="background-color: #4682B4;"><th style="width:150px">Supplier Id</th><th style="width:300px">Firm Name</th><th style="width:150px">Firm Type</th><th style="width:150px">Address</th><th style="width:150px">City</th><th style="width:150px">State</th><th style="width:150px">Country</th><th style="width:150px">GST Number</th><th style="width:200px">Supplier Type</th><th style="width:50px">Action</th>');
					
					      	   for(var i=0;i<result.length;i++)
						  	   { 
							  		if(i%2==0)
							  		{
							  			var id = result[i].supplierId;
										$('#supplierListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].supplierId+'</td><td>'+result[i].supplierfirmName+'</td><td>'+result[i].supplierfirmType+'</td><td>'+result[i].supplierfirmAddress+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].countryId+'</td><td>'+result[i].firmgstNumber+'</td><td>'+result[i].suppliertypeId+'</td><td><a href="${pageContext.request.contextPath}/EditSupplier?supplierId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
							  		}
							  		else
							  		{
							  			var id = result[i].supplierId;
							    		$('#supplierListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].supplierId+'</td><td>'+result[i].supplierfirmName+'</td><td>'+result[i].supplierfirmType+'</td><td>'+result[i].supplierfirmAddress+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].countryId+'</td><td>'+result[i].firmgstNumber+'</td><td>'+result[i].suppliertypeId+'</td><td><a href="${pageContext.request.contextPath}/EditSupplier?supplierId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
						      		}
						   		} 
							 }
							 else
							 {
								alert("failure111");
							 }
	
						}
			});
	
}//end of get State List



function getCityList()
{
	 $("#supplierListTable tr").detach();
	 $("#cityId").empty();
	 
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();
	 
	 $.ajax({

		url : '${pageContext.request.contextPath}/getCityList',
		type : 'Post',
		data : { stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select City-");
							$("#cityId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].cityId).text(result[i].cityName);
							    $("#cityId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
		 //Country & State Wise Supplier List
	     $("#supplierListTable tr").detach();
		
		 $.ajax({
	
			 url : '${pageContext.request.contextPath}/getStateWiseSupplierList',
			type : 'Post',
			data : { countryId : countryId, stateId : stateId},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{ 
										$('#supplierListTable').append('<tr style="background-color: #4682B4;"><th style="width:150px">Supplier Id</th><th style="width:300px">Firm Name</th><th style="width:150px">Firm Type</th><th style="width:150px">Address</th><th style="width:150px">City</th><th style="width:150px">State</th><th style="width:150px">Country</th><th style="width:150px">GST Number</th><th style="width:200px">Supplier Type</th><th style="width:50px">Action</th>');
					
					      	   for(var i=0;i<result.length;i++)
						  	   { 
							  		if(i%2==0)
							  		{
							  			var id = result[i].supplierId;
										$('#supplierListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].supplierId+'</td><td>'+result[i].supplierfirmName+'</td><td>'+result[i].supplierfirmType+'</td><td>'+result[i].supplierfirmAddress+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].countryId+'</td><td>'+result[i].firmgstNumber+'</td><td>'+result[i].suppliertypeId+'</td><td><a href="${pageContext.request.contextPath}/EditSupplier?supplierId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
							  		}
							  		else
							  		{
							  			var id = result[i].supplierId;
							    		$('#supplierListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].supplierId+'</td><td>'+result[i].supplierfirmName+'</td><td>'+result[i].supplierfirmType+'</td><td>'+result[i].supplierfirmAddress+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].countryId+'</td><td>'+result[i].firmgstNumber+'</td><td>'+result[i].suppliertypeId+'</td><td><a href="${pageContext.request.contextPath}/EditSupplier?supplierId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
						      		}
						   		} 
							 }
							 else
							 {
								alert("failure111");
							 }
	
						}
			});
		
}//end of get City List


function getLocationAreaList()
{
	 $("#locationareaId").empty();
	 
	 var cityId = $('#cityId').val();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();
	 
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getLocationAreaList',
		type : 'Post',
		data : { countryId : countryId , stateId : stateId, cityId : cityId },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Location Area-");
							$("#locationareaId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].locationareaId).text(result[i].locationareaName);
							    $("#locationareaId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
	 
	 	//get Country State And City Wise Suppliers
		 $("#supplierListTable tr").detach();
			
		 $.ajax({
	
			 url : '${pageContext.request.contextPath}/getCityWiseSupplierList',
			type : 'Post',
			data : { countryId : countryId, stateId : stateId, cityId : cityId},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{ 
										$('#supplierListTable').append('<tr style="background-color: #4682B4;"><th style="width:150px">Supplier Id</th><th style="width:300px">Firm Name</th><th style="width:150px">Firm Type</th><th style="width:150px">Address</th><th style="width:150px">City</th><th style="width:150px">State</th><th style="width:150px">Country</th><th style="width:150px">GST Number</th><th style="width:200px">Supplier Type</th><th style="width:50px">Action</th>');
					
					      	   for(var i=0;i<result.length;i++)
						  	   { 
							  		if(i%2==0)
							  		{
							  			var id = result[i].supplierId;
										$('#supplierListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].supplierId+'</td><td>'+result[i].supplierfirmName+'</td><td>'+result[i].supplierfirmType+'</td><td>'+result[i].supplierfirmAddress+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].countryId+'</td><td>'+result[i].firmgstNumber+'</td><td>'+result[i].suppliertypeId+'</td><td><a href="${pageContext.request.contextPath}/EditSupplier?supplierId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
							  		}
							  		else
							  		{
							  			var id = result[i].supplierId;
							    		$('#supplierListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].supplierId+'</td><td>'+result[i].supplierfirmName+'</td><td>'+result[i].supplierfirmType+'</td><td>'+result[i].supplierfirmAddress+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].countryId+'</td><td>'+result[i].firmgstNumber+'</td><td>'+result[i].suppliertypeId+'</td><td><a href="${pageContext.request.contextPath}/EditSupplier?supplierId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
						      		}
						   		} 
							 }
							 else
							 {
								alert("failure111");
							 }
	
						}
			});
	 	
}//end of get locationarea List


function getLocationAreaWiseSupplierList()
{
	
	 //to retrive all banks by Location Area name
	$("#supplierListTable tr").detach();
	
	 var countryId = $('#countryId').val();
	 var stateId = $('#stateId').val();
	 var cityId = $('#cityId').val();
	 var locationareaId = $('#locationareaId').val();

	 $.ajax({

		 url : '${pageContext.request.contextPath}/getLocationAreaWiseSupplierList',
		type : 'Post',
		data : { countryId : countryId , stateId : stateId, cityId : cityId ,locationareaId : locationareaId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
									$('#supplierListTable').append('<tr style="background-color: #4682B4;"><th style="width:150px">Supplier Id</th><th style="width:300px">Firm Name</th><th style="width:150px">Firm Type</th><th style="width:150px">Address</th><th style="width:150px">City</th><th style="width:150px">State</th><th style="width:150px">Country</th><th style="width:150px">GST Number</th><th style="width:200px">Supplier Type</th><th style="width:50px">Action</th>');
				
				      	   for(var i=0;i<result.length;i++)
					  	   { 
						  		if(i%2==0)
						  		{
						  			var id = result[i].supplierId;
									$('#supplierListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].supplierId+'</td><td>'+result[i].supplierfirmName+'</td><td>'+result[i].supplierfirmType+'</td><td>'+result[i].supplierfirmAddress+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].countryId+'</td><td>'+result[i].firmgstNumber+'</td><td>'+result[i].suppliertypeId+'</td><td><a href="${pageContext.request.contextPath}/EditSupplier?supplierId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
						  		}
						  		else
						  		{
						  			var id = result[i].supplierId;
						    		$('#supplierListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].supplierId+'</td><td>'+result[i].supplierfirmName+'</td><td>'+result[i].supplierfirmType+'</td><td>'+result[i].supplierfirmAddress+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].countryId+'</td><td>'+result[i].firmgstNumber+'</td><td>'+result[i].suppliertypeId+'</td><td><a href="${pageContext.request.contextPath}/EditSupplier?supplierId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
					      		}
					   		} 
						 }
						 else
						 {
							alert("failure111");
						 }

					}
		});
	
}//end of get locationarea List  

//Search Supplier
function SearchSuppliers()
{

	$("#supplierListTable tr").detach();
	
	var countryId = $('#countryId').val();
	var stateId = $('#stateId').val();
	var cityId = $('#cityId').val();
	var locationareaId = $('#locationareaId').val();
		
	$.ajax({

		url : '${pageContext.request.contextPath}/SearchSuppliers',
		type : 'Post',
		data : { countryId : countryId, stateId : stateId, cityId : cityId, locationareaId : locationareaId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
									$('#supplierListTable').append('<tr style="background-color: #4682B4;"><th style="width:150px">Supplier Id</th><th style="width:300px">Firm Name</th><th style="width:150px">Firm Type</th><th style="width:150px">Address</th><th style="width:150px">City</th><th style="width:150px">State</th><th style="width:150px">Country</th><th style="width:150px">GST Number</th><th style="width:200px">Supplier Type</th><th style="width:50px">Action</th>');
							
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									$('#supplierListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].supplierId+'</td><td>'+result[i].supplierfirmName+'</td><td>'+result[i].supplierfirmType+'</td><td>'+result[i].supplierfirmAddress+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].countryId+'</td><td>'+result[i].firmgstNumber+'</td><td>'+result[i].suppliertypeId+'</td><td><a href="${pageContext.request.contextPath}/EditSupplier?supplierId=${'+result[i].supplierId+'}" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
								else
								{
									$('#supplierListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].supplierId+'</td><td>'+result[i].supplierfirmName+'</td><td>'+result[i].supplierfirmType+'</td><td>'+result[i].supplierfirmAddress+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].countryId+'</td><td>'+result[i].firmgstNumber+'</td><td>'+result[i].suppliertypeId+'</td><td><a href="${pageContext.request.contextPath}/EditSupplier?supplierId=${'+result[i].supplierId+'}" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
							} 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
}


function supplierTypeSearch()
{

	$("#supplierListTable tr").detach();
	 var suppliertypeId = $('#suppliertypeId').val();
		
	$.ajax({

		url : '${pageContext.request.contextPath}/searchSupplierTypeWiseSupplierList',
		type : 'Post',
		data : { suppliertypeId : suppliertypeId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
									$('#supplierListTable').append('<tr style="background-color: #4682B4;"><th style="width:150px">Supplier Id</th> <th style="width:300px">Supplier Name</th> <th style="width:150px">Supplier Type</th> <th style="width:150px">Address</th><th style="width:150px">City</th><th style="width:150px">State</th><th style="width:150px">Country</th> <th style="width:200px">Aadhar Number</th> <th style="width:150px">GST Number</th> <th style="width:150px">Mobile Number</th>');
							
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									$('#supplierListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].supplierId+'</td><td>'+result[i].supplierfirstName+' '+result[i].suppliermiddleName+' '+result[i].supplierlastName+'</td><td>'+result[i].suppliertypeId+'</td><td>'+result[i].supplierAddress+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].countryId+'</td><td>'+result[i].supplieraadharcardNumber+'</td><td>'+result[i].suppliergstNumber+'</td><td>'+result[i].suppliermobileNumber+'</td>');
								}
								else
								{
									$('#supplierListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].supplierId+'</td><td>'+result[i].supplierfirstName+' '+result[i].suppliermiddleName+' '+result[i].supplierlastName+'</td><td>'+result[i].suppliertypeId+'</td><td>'+result[i].supplierAddress+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].countryId+'</td><td>'+result[i].supplieraadharcardNumber+'</td><td>'+result[i].suppliergstNumber+'</td><td>'+result[i].suppliermobileNumber+'</td>');
								}
							} 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
}


$(function () {
    $('#supplierListTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>

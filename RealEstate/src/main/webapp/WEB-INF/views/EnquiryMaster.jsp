<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate |Enquiry Master</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>

<div class="wrapper">

     <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Enquiry Master Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Pre-sale</a></li>
        <li class="active"> Enquiry Master</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="enquirymasterform" action="${pageContext.request.contextPath}/EnquiryMaster" onSubmit="return validate()" method="post">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              
       		
			  <div class="box-body">
          <div class="row">
          
                 <div class="col-xs-6">
			    <label>Flat Type  </label><label class="text-red">* </label>
                  <select class="form-control" name="flatType" id="flatType" onchange="getEnquiryList(this.value)">
				  <option selected="" value="Default">-Select Flat Type-</option>
                    <option>RK</option>
                    <option>1 BHK</option>
                    <option>2 BHK</option>
                    <option>3 BHK</option>
                    <option>4 BHK</option>
                  </select>
                    <span id="flatTypeSpan" style="color:#FF0000"></span>
			    </div> 
           
				</div>
				</div>
		 </div>
      </div>
		   	 
		<div class="box-body">
            <div class="row">
            
             <div class="col-xs-2">
             </div>
             
			<div class="col-xs-2">
			 &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 
			 <a href="EnquiryMaster">  <button type="button" class="btn btn-default" value="reset" style="width:90px"> Reset</button></a>
              </div> 
              
              <div class="col-xs-2">
			      <a href="AddEnquiry"> <button type="button" class="btn btn-success"><i class="fa fa-plus"></i> Add New Enquiry</button></a>
            </div>
			   <div class="col-xs-2">
			      <a href="AllEnquiryViews"> <button type="button" class="btn btn-success"> All Enquiry Views</button></a>
            </div>  
            
			 <div class="col-xs-2">
			    <a href="ImportNewEnquiry">  <button type="button" class="btn btn-info pull-right" style="background:#48D1CC"> Import From Excel File</button></a>
             </div> 
                 
			  </div>
          <!-- /.row -->
        </div>
      </div>
     <div class="box box-default">
				 <div  class="panel box box-danger"></div>
				  <h4 class="page-header"> &nbsp &nbsp  All Enquiry List</h4>
        	 <div class="box-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="enquiryListTable">
                  <thead>
                  <tr bgcolor="#4682B4">
                  <th>Enquiry Id</th>
                  <th>Name</th>
                  <th>Mobile No</th>
                  <th>Email Id</th>
				  <th>Flat Type</th>
				  <th>Sub Enq Source</th>
				  <th>Status</th>
                  </tr>
                  </thead>
                  <tbody >
                   <c:forEach items="${enquiryList}" var="enquiryList" varStatus="loopStatus">
                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                           <td>${enquiryList.enquiryId}</td>
	                       <td>${enquiryList.enqfirstName} ${enquiryList.enqmiddleName} ${enquiryList.enqlastName}</td>
	                       <td>${enquiryList.enqmobileNumber1}</td>
	                        <td>${enquiryList.enqEmail}</td>
	                      
	                     	<td>${enquiryList.flatType}</td>
	                        <td>${enquiryList.subsourceId}</td>
	                        <td>${enquiryList.enqStatus}</td>
                      </tr>
				   </c:forEach>
                 </tbody>
                </table>
              </div>
            </div>
	 </div>
     
       </section>
	</form>
    <!-- /.content -->
    
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
     <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script>

function getEnquiryList()
{
	 //$("#stateListTable").empty();
$("#enquiryListTable tr").detach();
	 var flatType = $('#flatType').val();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getenquiryList',
		type : 'Post',
		data : { flatType : flatType},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
							
							$('#enquiryListTable').append('<tr style="background-color: #4682B4;"><td>Enquiry Id</td><td>Name</td><td>Email Id</td><td>Mobile No</td><td>Flat Type</td><th>Sub Enq Source</th><td>Status</td>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
									{
								$('#enquiryListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].enquiryId+'</td><td>'+result[i].enqfirstName+'</td><td>'+result[i].enqEmail+'</td><td>'+result[i].enqmobileNumber1+'</td><td>'+result[i].flatType+'</td><td>'+result[i].subsourceId+'</td><td>'+result[i].enqStatus+'</td>');
									}
								else{
									$('#enquiryListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].enquiryId+'</td><td>'+result[i].enqfirstName+'</td><td>'+result[i].enqEmail+'</td><td>'+result[i].enqmobileNumber1+'</td><td>'+result[i].flatType+'</td><td>'+result[i].subsourceId+'</td><td>'+result[i].enqStatus+'</td>');
									
								}
							
							 } 
						
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get State List



$(function () {
    $('#enquiryListTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Add Contractor</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


  <!-- Google Font -->
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
    </script>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
	
<div class="wrapper">

   <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
  <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Contractor Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Add Contractor</li>
      </ol>
    </section>

    <!-- Main content -->
	<form name="contractorform" action="${pageContext.request.contextPath}/AddContractor" onSubmit="return validate()" method="post">

    <section class="content">
   
      <div class="box box-default">
        <div class="panel box box-danger"></div>
       
        <span id="statusSpan" style="color:#FF0000"></span>
        <!-- /.box-header -->
        
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
                  <span id="statusSpan" style="color:#FF0000"></span>
                   
                  <div class="box-body">
                     <div class="row">
                       <div class="col-xs-3">
		                  <label for="contractorId">Contractor Id </label>
		                  <input type="text" class="form-control" id="contractorId" name="contractorId" value="${contractorCode}" readonly>
                       </div>               
              		 </div>
            	  </div> 
            	  
            	 <div class="box-body">
	               <div class="row">
	                 <div class="col-xs-3">
					   <label for="contractorfirstName">Contractor Firm Name</label> <label class="text-red">* </label>
	                   <input type="text" class="form-control" id="contractorfirmName" placeholder="First Name" name="contractorfirmName"  style="text-transform: capitalize;" onchange="getclearstatus()">
	                   <span id="contractorfirmNameSpan" style="color:#FF0000"></span>
	                 </div>
	                 
	                 <div class="col-xs-3">
                  		<label>Select Firm Type</label> <label class="text-red">* </label>
                  		<select class="form-control" name="contractorfirmType" id="contractorfirmType">
                    		<option value="Private">Private</option>
                    		<option value="Public">Public</option>
                    		<option value="Proprietary">Proprietary</option>
                    		<option value="Partner">Partner</option>
                    		<option value="Other">Other</option>
                  		</select>
                    	<span id="contractorfirmTypeSpan" style="color:#FF0000"></span>
			     	 </div>
			     	 
			     	 
			     	 <div class="col-xs-2">
			    	  <label for="contractorpanNumber">Firm PAN  No</label> 
                  	  <input type="text" class="form-control" id="firmpanNumber" placeholder="Contractor PAN No" name="firmpanNumber" style="text-transform:uppercase" onchange="getpannounique(this.value)">
                	  <span id="firmpanNumberSpan" style="color:#FF0000"></span>
			        </div>
			        
			        <div class="col-xs-2">
			     	  <label for="contractorgstNumber">Firm GST No</label> 
                      <input type="text" class="form-control" id="firmgstNumber" placeholder="Contractor GST No" name="firmgstNumber" style="text-transform:uppercase" onchange="getgstnounique(this.value)">
                      <span id="firmgstNumberSpan" style="color:#FF0000"></span>
			        </div>
			     	
			        <div class="col-xs-2">
			     	  <label for="tdspercentage">TDS Per</label> <label class="text-red">* </label>
                      <input type="text" class="form-control" id="tdspercentage" placeholder="TDS per" name="tdspercentage" >
                      <span id="tdspercentageSpan" style="color:#FF0000"></span>
			        </div>
			     	  
	               </div>	
            	</div>
            	
              <div class="box-body">
               <div class="row">
                
                  <div class="col-xs-2">
			        <label for="insuranceStatus">Insurance</label><label class="text-red">* </label>   <span id="insuranceStatusSpan" style="color:#FF0000"></span></br>
   				    <input type="radio" name="insuranceStatus" id="insuranceStatus" value="Yes" onclick = "InsuranceStatusYes()"> Yes
				    &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
  				    <input type="radio" name="insuranceStatus" id="insuranceStatus" value="No" onclick = "InsuranceStatusNo()"> No
                </div>
                 
			     <div class="col-xs-2">
			          <label>Contractor Type</label> <label class="text-red">* </label>
                      <select class="form-control" name="contractortypeId" id="contractortypeId" onchange="getSubContractorType(this.value)">
				           <option selected="selected" value="Default">-Select Contractor Type-</option>
                  		<c:forEach var="contractortypeList" items="${contractortypeList}">
	                        <option value="${contractortypeList.contractortypeId}">${contractortypeList.contractorType}</option>
	                    </c:forEach>
                      </select>
                      <span id="contractortypeIdSpan" style="color:#FF0000"></span>
			     </div>
			     
			     <div class="col-xs-3">
			          <label>Sub-Contractor Type</label> <label class="text-red">* </label>
                      <select class="form-control" name="subcontractortypeId" id="subcontractortypeId">
				           <option selected="selected" value="Default">-Select Sub-Contractor Type-</option>
                  		<c:forEach var="subcontractortypeList" items="${subcontractortypeList}">
	                        <option value="${subcontractortypeList.subcontractortypeId}">${subcontractortypeList.subcontractorType}</option>
	                    </c:forEach>
                      </select>
                      <span id="subcontractortypeIdSpan" style="color:#FF0000"></span>
			     </div>
		         
                   <div class="col-xs-3">
			     	<label for="checkPrintingName">Check Printing Name</label>
                    <input type="text" class="form-control" id="checkPrintingName" placeholder="Check Holder Name" name="checkPrintingName" style="text-transform:uppercase">
                    <span id="checkPrintingNameSpan" style="color:#FF0000"></span>
			     </div>
			       	  
                <div class="col-xs-2">
			        <label for="contractorpaymentTerms">Payment Terms</label> <label class="text-red">* </label>
                    <select class="form-control" id="contractorpaymentTerms" name="contractorpaymentTerms">
				  		<option selected="selected">1</option>
                    	<option>2</option>
                    	<option>3</option>
                    	<option>4</option>
                    	<option>5</option>
                    	<option>6</option>
                    	<option>7</option>
                    	<option>8</option>
                    	<option>9</option>
                    	<option>10</option>
                    	<option>11</option>
                    	<option>12</option>
                    	<option>13</option>
                    	<option>14</option>
                    	<option>15</option>
                  	</select>
                    <span id="contractorpaymentTermsSpan" style="color:#FF0000"></span>
			     </div>
			      	  
	               </div>	
            	</div>
            	
              <div class="box-body">
               <div class="row">
               
                    <div class="col-xs-3">
			            <label for="contractorAddress">Firm Address </label> <label class="text-red">* </label>
                        <textarea class="form-control" rows="1" id="contractorfirmAddress" placeholder="Address" name="contractorfirmAddress"></textarea>
                        <span id="contractorfirmAddressSpan" style="color:#FF0000"></span>
			         </div> 
	                 
	                 <div class="col-xs-2">
                        <label>Country</label> <label class="text-red">*</label>
                  		<select class="form-control" id="countryId" name="countryId"  onchange="getStateList(this.value)">
				  				<option selected="selected" value="Default">-Select Country-</option>
                      		<c:forEach var="countryList" items="${countryList}">
	                    		<option value="${countryList.countryId}">${countryList.countryName}</option>
	                  		</c:forEach>
                  		</select>
                   		<span id="countryIdSpan" style="color:#FF0000"></span>
                	 </div>
                	 
                	 <div class="col-xs-2">
				       <label>State </label> <label class="text-red">* </label>
                       <select class="form-control" id="stateId" name="stateId" onchange="getCityList(this.value)">
				    	  <option selected="selected" value="Default">-Select State-</option>
                        <c:forEach var="stateList" items="${stateList}">
                          <option value="${stateList.stateId}">${stateList.stateId}</option>
				        </c:forEach>
                       </select>
                       <span id="stateIdSpan" style="color:#FF0000"></span>
				     </div>
				  
				     <div class="col-xs-2">
				       <label>City </label> <label class="text-red">* </label>
                       <select class="form-control" id="cityId" name ="cityId" onchange="getLocationAreaList(this.value)">
				    	  <option selected="selected" value="Default">-Select City-</option>
                        <c:forEach var="cityList" items="${cityList}">
                    	  <option value="${cityList.cityId}">${cityList.cityId}</option>
				        </c:forEach>
                       </select>
                       <span id="cityIdSpan" style="color:#FF0000"></span>
		             </div>
                	 
         <!--        
               </div>
              </div>
            	
               <div class="box-body">
                <div class="row">
		        -->
		         <div class="col-xs-2">
				    <label>Area</label> <label class="text-red">* </label>
                    <select class="form-control" id="locationareaId" name="locationareaId" onchange="getpinCode(this.value)">
				         <option selected="selected" value="Default">-Select Area-</option>
                      <c:forEach var="locationareaList" items="${locationareaList}">
                    	 <option value="${locationareaList.locationareaId}">${locationareaList.locationareaId}</option>
				      </c:forEach>
                    </select>
                    <span id="locationareaIdSpan" style="color:#FF0000"></span>
				 </div>
		         
		         <div class="col-xs-1">
                  <label for="contractorPincode">Pin Code</label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="contractorPincode" placeholder="Pin Code" name="contractorPincode" readonly="readonly">
                   <span id="contractorPincodeSpan" style="color:#FF0000"></span>
			     </div>
			
			    </div>
		      </div>
		      
		      <div class="box-body"><!-- Firm Pan, GST, Bank Name, Branch -->
                <div class="row">
                
			     	<div class="col-xs-3">
			          <label>Firm Bank Name</label> 
                      <input type="text" class="form-control" id="bankName" placeholder="bank Name "name="bankName" >
                   	  <span id="firmbankNameSpan" style="color:#FF0000"></span>
			        </div> 
			        
			        <div class="col-xs-3">
			    	  <label for="contractorbranchName">Firm Bank Branch </label> 
                      <input type="text" class="form-control" id="branchName" placeholder="branch Name "name="branchName" >
                      <span id="firmbankBranchSpan" style="color:#FF0000"></span>
			        </div> 
			        
			        <div class="col-xs-3">
			    	  <label for="contractorbankifscCode">Firm Bank Branch IFSC</label> 
                      <input type="text" class="form-control" id="bankifscCode" placeholder="IFSC "name="bankifscCode">
                      <span id="bankifscCodeSpan" style="color:#FF0000"></span>
			        </div> 
                  
                    <div class="col-xs-3">
			    	  <label for="contractorbankacNumber">Firm Bank A/C No</label> 
                      <input type="text" class="form-control" id="firmbankacNumber" placeholder="Bank A/C No "name="firmbankacNumber">
                      <span id="contractorbankacNumberSpan" style="color:#FF0000"></span>
			        </div>
			        
                </div>
              </div>
              
            </div>
          </div>
        </div><!-- Col-12 end -->
        
        <div class="box-body">
          <div class="row">
			
			<div class="col-md-12">
			 <div class="box-body">
			    <h4>Contractor Employee Details</h4>
              <div class="row">
                  <div class="col-xs-3">
			    <label for="">Employee Name</label> 
                <input type="text" class="form-control" id="employeeName" placeholder="Enter Employee Name" name="employeeName" style="text-transform: capitalize;">
                 <span id="employeeNameSpan" style="color:#FF0000"></span>
                 </div> 
                    <div class="col-xs-3">
			    <label for="">Designation</label>
			    
                    <select class="form-control" id="employeeDesignation" name="employeeDesignation">
						  <option selected="selected" value="Default">-Select Designation-</option>
		               <c:forEach var="designationList" items="${designationList}">
                    	  <option value="${designationList.designationName}">${designationList.designationName}</option>
				        </c:forEach> 
		             </select>
		            <span id="employeeDesignationSpan" style="color:#FF0000"></span>    
                  </div> 
              
           
                  <div class="col-xs-3">
			   
                  <label for="emailid">Email ID </label> 
				   <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                    <input type="text" class="form-control" placeholder="Email" id="employeeEmail" name="employeeEmail">
                    <span id="employeeEmailSpan" style="color:#FF0000"></span>
                   </div>
			     </div> 
				  
				 <div class="col-xs-3">
			     <label>Mobile No </label> 
				  <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask='"mask": "9999999999"' data-mask name="employeeMobileno" id="employeeMobileno">
                   <span class="input-group-btn">
            	    <button type="button" class="btn btn-success" onclick="return AddContractorEmployee()"><i class="fa fa-plus"></i>Add</button>
              	   </span>
                </div>
                  <span id="employeeMobilenoSpan" style="color:#FF0000"></span>
			   </div>
			 
		  </div>
        </div>		

          <div class="box-body">
           <div class="row">        
        	<div class="col-xs-12">
              <table class="table table-bordered" id="contractorEmployeeListTable">
	              <tr bgcolor=#4682B4>
		              <th>Name</th>
		              <th>Designation</th>
		              <th>E-Mail Id</th>
		              <th>Mobile No.</th>
		              <th>Action</th>
	               </tr>

               	   <%-- <c:forEach items="${bankEmployeesList}" var="bankEmployeesList" varStatus="loopStatus">
                    <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                        <td>${bankEmployeesList.employeeName}</td>
                        <td>${bankEmployeesList.employeeDesignation}</td>
                        <td>${bankEmployeesList.employeeEmail}</td>
                        <td>${bankEmployeesList.employeeMobileno}</td>
                        <td>
                        	<a href="${pageContext.request.contextPath}/EditBankEmployee?bankId=${bankEmployeesList.bankId}" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>
                           |<a onclick="DeleteBankEmployee()" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a>
                        </td>
                     </tr>
                    </c:forEach> --%>
              </table>
            </div>
           </div>
          </div>
        	
        		 <input type="hidden" id="status" name="status" value="${Status}">	
				 <input type="hidden" id="creationDate" name="creationDate" value="">
				 <input type="hidden" id="updateDate" name="updateDate" value="">
				 <input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>">	 
				 
		  </div>
		  
		  <br/><br/><br/>
		  
		    <div class="box-body">
              <div class="row">
               <div class="col-xs-1">
               </div>
	            <div class="col-xs-4">
	             <div class="col-xs-2">
                	<a href="ContractorMaster"><button type="button" class="btn btn-block btn-primary" value="reset" style="width:90px">Back</button></a>
			     </div>
			    </div>
			    
	   		    <div class="col-xs-2">
	                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
			    </div>
			    
				<div class="col-xs-3">
		  			<button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
			    </div>
			    
			    <!-- 
			     
			       <div class="col-xs-6">
			         <button type="button" class="btn btn-info pull-right" style="background:#48D1CC"><a href="ImportNewBank"> Import From Excel File</a></button>
              
                  </div>
                  
			        -->
			  </div>
			</div>
		    
            <!-- /.col -->
       </div>
	 </div>
	 </div>
     </section>
	</form>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

 <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script>

function clearall()
{

	$('#contractorfirmNameSpan').html('');
	$('#contractorfirmTypeSpan').html('');
	$('#firmpanNumberSpan').html('');
	$('#firmgstNumberSpan').html('');
	$('#contractorfirmAddressSpan').html('');
	$('#countryIdSpan').html('');
	$('#contractorGenderSpan').html('');
	$('#contractorAddressSpan').html('');
	$('#countryIdSpan').html('');
	$('#stateIdSpan').html('');
	$('#cityIdSpan').html('');
	$('#locationareaIdSpan').html('');
	$('#contractorPincodeSpan').html('');
	$('#contractortypeIdSpan').html('');
	$('#subcontractortypeIdSpan').html('');
	$('#firmbankNameSpan').html('');
	$('#firmbankBranchSpan').html('');
	$('#bankifscCodeSpan').html('');
	$('#contractorbankacNumberSpan').html('');
	$('#contractorpaymentTermsSpan').html('');
	
	$('#employeeNameSpan').html('');
	$('#employeeDesignationSpan').html();
	$('#employeeEmailSpan').html('');
	$('#employeeMobilenoSpan').html('');
	
	$('#statusSpan').html('');
}

function validate()
{
	clearall();

	//validation for Contractor firm name
	if(document.contractorform.contractorfirmName.value=="")
	{
		 $('#contractorfirmNameSpan').html('Please, enter contractor firm name..!');
		document.contractorform.contractorfirmName.focus();
		return false;
	}
	else if(document.contractorform.contractorfirmName.value.match(/^[\s]+$/))
	{
		$('#contractorfirmNameSpan').html('Please, enter valid contractor firm name..!');
		document.contractorform.contractorfirmName.value="";
		document.contractorform.contractorfirmName.focus();
		return false; 	
	}

	/* 
	//validation for PAN number
	if(document.contractorform.firmpanNumber.value=="")
	{
		$('#firmpanNumberSpan').html('Please, enter Pancard number..!');
		document.contractorform.firmpanNumber.focus();
		return false;
	}
	 */
	/*
	else if(!document.contractorform.firmpanNumber.value.match(/^[A-Za-z]{5}[0-9]{4}[A-z]{1}$/))
	{
		$('#firmpanNumberSpan').html('PAN number must start with 5 alphabets follwed by 4 digit number and 1 alphabet..!');
		//document.employeeform.firmpanNumber.value="";
		document.contractorform.firmpanNumber.focus();
		return false;
	}
	*/
	//validation for GST number
	/* 
	if(document.contractorform.firmgstNumber.value=="")
	{
		$('#firmgstNumberSpan').html('Please, Enter GST number..!');
		document.contractorform.firmgstNumber.focus();
		return false;
	}
	 */
	/*
	else if(!document.contractorform.firmgstNumber.value.match(/^[0-9]{2}[A-Za-z]{5}[0-9]{4}[A-z]{1}[0-9]{1}[Zz]{1}[0-9]{1}$/))
	{
		$('#firmgstNumberSpan').html('GST number must match format like(22AAAAA0000A1Z5)..!');
		//document.contractorform.firmgstNumber.value="";
		document.contractorform.firmgstNumber.focus();
		return false;
	}
	*/
	//validation for contractor firm address--------------------------------
	
	
	
	if(document.contractorform.tdspercentage.value=="")
	{
		$('#tdspercentageSpan').html('Please, enter flat cost');
		document.contractorform.tdspercentage.focus();
		return false;
	}
	else if(document.contractorform.tdspercentage.value.match(/^[\s]+$/))
	{
		$('#tdspercentageSpan').html('Please, enter TDS Per..!');
		document.contractorform.tdspercentage.value="";
		document.contractorform.tdspercentage.focus();
		return false; 	
	}
	else if(!document.contractorform.tdspercentage.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
	{
		 if(!document.contractorform.tdspercentage.value.match(/^[0-9]+$/))
			 {
				$('#tdspercentageSpan').html('Please, use only digit value for TDS Per..! eg:21.36 OR 30');
				document.contractorform.tdspercentage.value="";
				document.contractorform.tdspercentage.focus();
				return false;
			}
	}

    if(( document.contractorform.insuranceStatus[0].checked == false ) && ( document.contractorform.insuranceStatus[1].checked == false ) )
	{
    	$('#insuranceStatusSpan').html('Please, insurance Status..!');
		document.contractorform.insuranceStatus[0].focus();
		return false;
	}

	if(document.contractorform.contractortypeId.value=="Default")
	{
		$('#contractortypeIdSpan').html('Please, select type name..!');
		document.contractorform.contractortypeId.focus();
		return false;
	}
	
	
	if(document.contractorform.subcontractortypeId.value=="Default")
	{
		$('#subcontractortypeIdSpan').html('Please, select sub type name..!');
		document.contractorform.subcontractortypeId.focus();
		return false;
	}
	
	if(document.contractorform.contractorfirmAddress.value=="")
	{
		 $('#contractorfirmAddressSpan').html('Please, enter contractor firm address..!');
		document.contractorform.contractorfirmAddress.focus();
		return false;
	}
	else if(document.contractorform.contractorfirmAddress.value.match(/^[\s]+$/))
	{
		$('#contractorfirmAddressSpan').html('Please, enter valid contractor firm address..!');
		document.contractorform.contractorfirmAddress.value="";
		document.contractorform.contractorfirmAddress.focus();
		return false; 	
	}

	
	//validation for countryId
	if(document.contractorform.countryId.value=="Default")
	{
		$('#countryIdSpan').html('Please, select country name..!');
		document.contractorform.countryId.focus();
		return false;
	}
	
	//validation for stateId
    if(document.contractorform.stateId.value=="Default")
	{
		$('#stateIdSpan').html('Please, select state name..!');
		document.contractorform.stateId.focus();
		return false;
	}
	
	//validation for cityId
	if(document.contractorform.cityId.value=="Default")
	{
		$('#cityIdSpan').html('Please, select city name..!');
		document.contractorform.cityId.focus();
		return false;
	}
	
	//validation for location area name
	if(document.contractorform.locationareaId.value=="Default")
	{
		$('#locationareaIdSpan').html('Please, select area name..!');
		document.contractorform.locationareaId.focus();
		return false;
	}
	/* 
	//validation for bankName---------------------------------------
	if(document.contractorform.bankName.value=="Default")
	{
		$('#firmbankNameSpan').html('Please, select bank name..!');
		document.contractorform.bankName.focus();
		return false;
	}
	
	//validation for branchName
	if(document.contractorform.branchName.value=="Default")
	{
		$('#firmbankBranchSpan').html('Please, select bank branch name..!');
		document.contractorform.branchName.focus();
		return false;
	}
	
	//validation for bank account number
	if(document.contractorform.firmbankacNumber.value.length==0)
	{
		$('#contractorbankacNumberSpan').html('Please, enter Bank a/c number..!');
		//document.contractorform.employeeBankacno.value="";
		document.contractorform.firmbankacNumber.focus();
		return false;
	}
	else if(document.contractorform.firmbankacNumber.value.length!=0)
	{
		if(!document.contractorform.firmbankacNumber.value.match(/^[0-9]{6,18}$/))
		{
			$('#contractorbankacNumberSpan').html('Please, enter valid Bank a/c number..!');
			//document.contractorform.employeeBankacno.value="";
			document.contractorform.firmbankacNumber.focus();
			return false;
		}
	} */
}

function init()
{
	clearall();
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	
	 if(document.contractorform.status.value=="Fail")
	 {
		// $('#statusSpan').html('Sorry, record is present already..!');
	 }
	 else if(document.contractorform.status.value=="Success")
	 {
		 $('#statusSpan').html('Record added successfully..!');
	 }
  
	 document.contractorform.contractorfirmName.focus();
}


function getpinCode()
{

	 $("#contractorPincode").empty();
	 var locationareaId = $('#locationareaId').val();
	 var cityId = $('#cityId').val();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();
	 $.ajax({

		url : '${pageContext.request.contextPath}/getallAreaList',
		type : 'Post',
		data : { locationareaId : locationareaId, cityId : cityId, stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							
							for(var i=0;i<result.length;i++)
							{
							$('#contractorPincode').val(result[i].pinCode);
								
							 } 
						
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}

function getStateList()
{
	 $("#stateId").empty();
	 var countryId = $('#countryId').val();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getStateList',
		type : 'Post',
		data : { countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select State-");
							$("#stateId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].stateId).text(result[i].stateName);
							    $("#stateId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get State List


 function getCityList()
{
	 $("#cityId").empty();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();
	$.ajax({

		url : '${pageContext.request.contextPath}/getCityList',
		type : 'Post',
		data : { stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select City-");
							$("#cityId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].cityId).text(result[i].cityName);
							    $("#cityId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
}//end of get City List



function getLocationAreaList()
{
	 $("#locationareaId").empty();
	 var cityId = $('#cityId').val();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();

	 $.ajax({

		url : '${pageContext.request.contextPath}/getLocationAreaList',
		type : 'Post',
		data : { cityId : cityId, stateId : stateId, countryId:countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Location Area-");
							$("#locationareaId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].locationareaId).text(result[i].locationareaName);
							    $("#locationareaId").append(option);
							} 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
}//end of get Location Area List


function getBranchList()
{
	 $("#branchName").empty();
	 var bankName = $('#bankName').val();
	
	 $.ajax({

		url : '${pageContext.request.contextPath}/getBranchList',
		type : 'Post',
		data : { bankName : bankName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Branch Name-");
							$("#branchName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].branchName).text(result[i].branchName);
							    $("#branchName").append(option);
							} 
						} 
						else
						{
							alert("failure111");
						}

					}
		});	
}

function getBranchIfsc()
{
	 //$("#bankifscCode").val('');
	 var bankName = $('#bankName').val();
     var branchName = $('#branchName').val();
	 
	 $.ajax({

		url : '${pageContext.request.contextPath}/getBranchIfsc',
		type : 'Post',
		data : { bankName : bankName, branchName : branchName },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							$('#bankifscCode').val(result[0].bankifscCode);
						} 
						else
						{
							alert("failure111");
						}

					}
		});
}

	
function AddContractorEmployee()
{

	clearall();
	
	//validation for employee name
	if(document.contractorform.employeeName.value=="")
	{
		$('#employeeNameSpan').html('Employee name should not be empty..!');
		document.contractorform.employeeName.value="";
		document.contractorform.employeeName.focus();
		return false;
	}
	else if(document.contractorform.employeeName.value.match(/^[\s]+$/))
	{
		$('#employeeNameSpan').html('Employee name must contains alphabets only..!');
		document.contractorform.employeeName.value="";
		document.contractorform.employeeName.focus();
		return false;
	}
	else if(!document.contractorform.employeeName.value.match(/^[a-zA-Z\s]+$/))
	{
		$('#employeeNameSpan').html('Employee name must contains alphabets only..!');
		document.contractorform.employeeName.value="";
		document.contractorform.employeeName.focus();
		return false;
	}
	
	//validation for employee designation
	if(document.contractorform.employeeDesignation.value=="Default")
	{
		$('#employeeDesignationSpan').html('Please, select designation..!');
		document.contractorform.employeeDesignation.focus();
		return false;
	}
	
	//validation for employee email id
	if(document.contractorform.employeeEmail.value=="")
	{
		$('#employeeEmailSpan').html('Please, enter email id..!');
		document.contractorform.employeeEmail.focus();
		return false;
	}
	else if(!document.contractorform.employeeEmail.value.match(/^([a-z0-9_\.\-])+\@(([a-z0-9\-])+\.)+([a-z0-9]{2,4})+$/))
	{
		$('#employeeEmailSpan').html('Please, enter valid email id..!');
		document.contractorform.employeeEmail.value="";
		document.contractorform.employeeEmail.focus();
		return false;
	}
	
	//validation for employee mobile number
	if(document.contractorform.employeeMobileno.value=="")
	{
		$('#employeeMobilenoSpan').html('Please, enter mobile number..!');
		document.contractorform.employeeMobileno.focus();
		return false;
	}
	else if(!document.contractorform.employeeMobileno.value.match(/^[0-9]{10}$/))
	{
		$('#employeeMobilenoSpan').html('mobile number must be 10 digit numbers only with correct format..!');
		document.contractorform.employeeMobileno.value="";
		document.contractorform.employeeMobileno.focus();
		return false;
	}
	
	 
	$('#contractorEmployeeListTable tr').detach();
	 
	 var contractorId = $('#contractorId').val();
	 var employeeName = $('#employeeName').val();
	 var employeeDesignation = $('#employeeDesignation').val();
	 var employeeEmail = $('#employeeEmail').val();
	 var employeeMobileno = $('#employeeMobileno').val();
	 
	 $.ajax({

		 url : '${pageContext.request.contextPath}/AddContractorEmployee',
		type : 'Post',
		data : { contractorId : contractorId, employeeName : employeeName, employeeDesignation : employeeDesignation, employeeEmail : employeeEmail, employeeMobileno : employeeMobileno},
		dataType : 'json',
		success : function(result)
				  {
					   if (result) 
					   { 
									$('#contractorEmployeeListTable').append('<tr style="background-color: #4682B4;"><th style="width:300px">Name</th><th style="width:150px">Designation</th><th style="width:150px">Email Id</th><th style="width:150px">Mobile No.</th><th style="width:100px">Action</th>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].contractorEmployeeId;
									$('#contractorEmployeeListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].employeeName+'</td><td>'+result[i].employeeDesignation+'</td><td>'+result[i].employeeEmail+'</td><td>'+result[i].employeeMobileno+'</td><td><a onclick="DeleteContractorEmployee('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
								else
								{
									var id = result[i].contractorEmployeeId;
									$('#contractorEmployeeListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].employeeName+'</td><td>'+result[i].employeeDesignation+'</td><td>'+result[i].employeeEmail+'</td><td>'+result[i].employeeMobileno+'</td><td><a onclick="DeleteContractorEmployee('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}
				  } 

		});
	 
	 $('#employeeName').val("")
	 $('#employeeDesignation').val("");
	 $('#employeeEmail').val("");
	 $('#employeeMobileno').val("");

}

function DeleteContractorEmployee(employeeId)
{
	var contractorId = $('#contractorId').val();
    var contractorEmployeeId = employeeId;
    
    $('#contractorEmployeeListTable tr').detach();
    
    $.ajax({

		 url : '${pageContext.request.contextPath}/DeleteContractorEmployee',
		type : 'Post',
		data : { contractorId : contractorId, contractorEmployeeId : contractorEmployeeId },
		dataType : 'json',
		success : function(result)
				  {
					   if (result) 
					   { 
									$('#contractorEmployeeListTable').append('<tr style="background-color: #4682B4;"><th style="width:300px">Name</th><th style="width:150px">Designation</th><th style="width:150px">Email Id</th><th style="width:150px">Mobile No.</th><th style="width:100px">Action</th>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].contractorEmployeeId;
									$('#contractorEmployeeListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].employeeName+'</td><td>'+result[i].employeeDesignation+'</td><td>'+result[i].employeeEmail+'</td><td>'+result[i].employeeMobileno+'</td><td><a onclick="DeleteContractorEmployee('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
								else
								{
									var id = result[i].contractorEmployeeId;
									$('#contractorEmployeeListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].employeeName+'</td><td>'+result[i].employeeDesignation+'</td><td>'+result[i].employeeEmail+'</td><td>'+result[i].employeeMobileno+'</td><td><a onclick="DeleteContractorEmployee('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}
				  } 

		});

}

function getSubContractorType()
{
	$('#subcontractortypeId').empty();
	var contractortypeId = $('#contractortypeId').val();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getSubContractorTypeList',
		type : 'Post',
		data : { contractortypeId : contractortypeId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select Sub Contractor-");
								$("#subcontractortypeId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].subcontractortypeId).text(result[i].subcontractorType);
							    $("#subcontractortypeId").append(option);
							} 
						} 
						else
						{
							alert("failure111");
						}

					}
		});	
	
}

$(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>
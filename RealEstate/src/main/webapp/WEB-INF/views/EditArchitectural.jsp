<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Add Architectural</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>

<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
    </script>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
    		
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>

<div class="wrapper">

  <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Architectural Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Add Architectural</li>
      </ol>
    </section>

	
<form name="architecturalform" action="${pageContext.request.contextPath}/EditArchitectural" method="post" onSubmit="return validate()" enctype="multipart/form-data">
    <section class="content">
  <div class="box box-default">
  <div class="box-body">
          <div class="row">
            <div class="col-md-12">
	            <span id="statusSpan" style="color:#008000"></span>
					
				<div class="box-body">
	              <div class="row">
	              
	                  <div class="col-xs-2">
	                  <label>Architectural Id</label>
	                  <input type="text" class="form-control"  id="architecturalId"name="architecturalId" value="${architecturalDetails[0].architecturalId}" readonly>
	                </div>               
	                
	              </div>
	            </div>
				
	            
			<div class="box-body">
              <div class="row">
              
                <div class="col-xs-3">
				<label for="architecturalfirmName">Firm Name</label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="architecturalfirmName" placeholder="Firm Name" name="architecturalfirmName" style="text-transform: capitalize;"value="${architecturalDetails[0].architecturalfirmName}">
                 <span id="architecturalfirmNameSpan" style="color:#FF0000"></span>
                </div>
                   
	            <div class="col-xs-2">
                  <label>Select Firm Type</label> <label class="text-red">* </label>
                   <select class="form-control" name="firmType" id="firmType">
                   <c:choose>
                  		  <c:when test="${architecturalDetails[0].firmType eq 'Private'}">
                    		<option selected="selected" value="Private">Private</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	     <option value="Private">Private</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 
                    	 <c:choose>
                    	  <c:when test="${architecturalDetails[0].firmType eq 'Public'}">
                    		<option selected="selected" value="Public">Public</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="Public">Public</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 
                    	 <c:choose>
                    	  <c:when test="${architecturalDetails[0].firmType eq 'Proprietary'}">
                    		<option selected="selected" value="Proprietary">Proprietary</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="Proprietary">Proprietary</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 
                    	 <c:choose>
                    	  <c:when test="${architecturalDetails[0].firmType eq 'Other'}">
                    		<option selected="selected" value="Other">Other</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="Other">Other</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    		
                  	</select>
                    <span id="firmTypeSpan" style="color:#FF0000"></span>
			     </div>
			     	 
			     <div class="col-xs-2">
			    	<label for="firmpanNumber">Firm PAN  No</label> 
                  	<input type="text" class="form-control" id="firmpanNumber" placeholder="Firm PAN No" name="firmpanNumber"  style="text-transform:uppercase" value="${architecturalDetails[0].firmpanNumber}">
                	<span id="firmpanNumberSpan" style="color:#FF0000"></span>
			     </div>
			        
			     <div class="col-xs-2">
			       <label for="firmgstNumber">Firm GST No</label>
                   <input type="text" class="form-control" id="firmgstNumber" placeholder="Firm GST No" name="firmgstNumber" style="text-transform:uppercase"value="${architecturalDetails[0].firmgstNumber}">
                   <span id="firmgstNumberSpan" style="color:#FF0000"></span>
			     </div>
			       
			     <div class="col-xs-3">
			     	<label for="checkPrintingName">Check Printing Name</label>
                    <input type="text" class="form-control" id="checkPrintingName" placeholder="Check Holder Name" name="checkPrintingName" style="text-transform:uppercase" value="${architecturalDetails[0].checkPrintingName}">
                    <span id="checkPrintingNameSpan" style="color:#FF0000"></span>
			     </div>
                   
              </div>
            </div>
            
		<!-- 	     
           <div class="box-body">
              <div class="row">
              
			     <div class="col-xs-3">
				     <label>Architectural RERA Number</label> 
	                 <input type="text" class="form-control" id="architecturalreraNumber" placeholder="Rera No:" name="architecturalreraNumber"  style="text-transform:uppercase">
	                 <span id="reraNumberSpan" style="color:#FF0000"></span>
			     </div>  
			       
			    </div>
		      </div>
		    -->   
		    
			<div class="box-body">
              <div class="row">
                  <div class="col-xs-3">
			      <label for="architecturaladdress">Address </label> <label class="text-red">* </label>
                  <textarea class="form-control" rows="1" id="architecturalAddress" placeholder="Address" name ="architecturalAddress">${architecturalDetails[0].architecturalAddress}</textarea>
			      <span id="architecturalAddressSpan" style="color:#FF0000"></span>
			      </div> 
			
                  <div class="col-xs-2">
                  <label>Country </label> <label class="text-red">* </label>
                    
                  		<select class="form-control" id="countryId" name="countryId"  onchange="getStateList(this.value)">
				  			<option selected="selected" value="${architecturalDetails[0].countryId}">${architecturalDetails[0].countryId}</option>
                      		<c:forEach var="countryList" items="${countryList}">
                       		 <c:choose>
                                <c:when test="${architecturalDetails[0].countryId ne countryList.countryId}">
	                    		    <option value="${countryList.countryId}">${countryList.countryName}</option>
	                            </c:when>
	                         </c:choose>
	                    	</c:forEach>
                  		</select>
                  <span id="countryIdSpan" style="color:#FF0000"></span>
                </div>             
                  
                <div class="col-xs-2">
				 <label>State </label> <label class="text-red">* </label>
                  <select class="form-control" name="stateId" id="stateId" onchange="getCityList(this.Value)">
				 	  <option selected="selected" value="${architecturalDetails[0].stateId}">${architecturalDetails[0].stateId}</option>  
                  </select>
                 <span id="stateIdSpan" style="color:#FF0000"></span>
				</div> 
				
                 <div class="col-xs-2">
				  <label>City </label> <label class="text-red">* </label>
                   <select class="form-control" name ="cityId" id="cityId" onchange="getLocationAreaList(this.value)">
				  		<option selected="selected" value="${architecturalDetails[0].cityId}">${architecturalDetails[0].cityId}</option>
                   </select>
                  <span id="cityIdSpan" style="color:#FF0000"></span>
				  </div> 
				  
				 <div class="col-xs-2">
				    <label>Area </label> <label class="text-red">* </label>
                     <select class="form-control" name="locationareaId" id="locationareaId"onchange="getpinCode(this.value)" >
				  	  <option selected="selected" value="${architecturalDetails[0].locationareaId}">${architecturalDetails[0].locationareaId}</option>
                     </select>
                    <span id="locationareaIdSpan" style="color:#FF0000"></span>
				  </div> 
           
                  <div class="col-xs-1">
                   <label for="pincode">Pin Code </label> <label class="text-red">* </label>
                   <input type="text" class="form-control" id="areaPincode" placeholder="Pin Code" name="areaPincode" value="${architecturalDetails[0].areaPincode}">
			       <span id="areaPincodeSpan" style="color:#FF0000"></span>
			     </div> 
        
              </div>
            </div>
            
			<div class="box-body">
              <div class="row">
                   
				<div class="col-xs-3">
			    <label>Bank Name </label>
			      <input type="text" class="form-control" id="architecturalBankName" placeholder="Bak Name" name="architecturalBankName" style="text-transform: capitalize;" value="${architecturalDetails[0].architecturalBankName}">
                  <span id="architecturalBankNameSpan" style="color:#FF0000"></span>
			     </div> 
			     
			    <div class="col-xs-3">
			     <label>Bank Branch Name </label>
			     <input type="text" class="form-control" id="branchName" placeholder="Branch Name" name="branchName" style="text-transform: capitalize;" value="${architecturalDetails[0].branchName}"> 
				 <span id="bankBranchNameSpan" style="color:#FF0000"></span>
			     </div> 
			  
                <div class="col-xs-3">
			     <label>IFSC </label>
                 <input type="text" class="form-control" id="bankifscCode" placeholder="Bank IFSC No " name="bankifscCode" style="text-transform:uppercase" value="${architecturalDetails[0].bankifscCode}">
			     <span id="bankifscCodeSpan" style="color:#FF0000"></span>
			     </div> 
			     
                <div class="col-xs-3">
			     <label for="bankacno">Bank A/C No </label> 
                 <input type="text" class="form-control" id="architecturalBankacno" placeholder="Bank A/C No "name="architecturalBankacno" value="${architecturalDetails[0].architecturalBankacno}">
                 <span id="architecturalBankacnoSpan" style="color:#FF0000"></span>
			    </div> 
			  
              </div>
            </div>
				     
  	          <input type="hidden" id="status" name="status" value="${architecturalDetails[0].status}">	
			  <input type="hidden" id="creationDate" name="creationDate"  value="${architecturalDetails[0].creationDate}">
			  <input type="hidden" id="updateDate" name="updateDate" value="">
			  <input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>">              
      
	          
				  		
            </div>
          </div>
        </div>
        
        <div class="box-body">
          <div class="row">
			
			<div class="col-md-12">
			 <div class="box-body">
			    <h4>Architectural Employee Details</h4>
              <div class="row">
                  <div class="col-xs-3">
			    <label for="">Employee Name</label> 
                <input type="text" class="form-control" id="employeeName" placeholder="Enter Employee Name" name="employeeName" style="text-transform: capitalize;">
                 <span id="employeeNameSpan" style="color:#FF0000"></span>
                 </div> 
                    <div class="col-xs-3">
			        <label for="">Designation</label>
                    <select class="form-control" id="employeeDesignation" name="employeeDesignation">
						  <option selected="selected" value="Default">-Select Designation-</option>
		               <c:forEach var="designationList" items="${designationList}">
                    	  <option value="${designationList.designationName}">${designationList.designationName}</option>
				        </c:forEach> 
		             </select>
		            <span id="employeeDesignationSpan" style="color:#FF0000"></span>    
                  </div> 
              
           
                  <div class="col-xs-3">
			   
                  <label for="emailid">Email ID </label> 
				   <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                    <input type="text" class="form-control" placeholder="Email" id="employeeEmail" name="employeeEmail">
                   </div>
                   <span id="employeeEmailSpan" style="color:#FF0000"></span>
			     </div> 
				  
				 <div class="col-xs-3">
			     <label>Mobile No </label> 
				  <div class="input-group">
                  <div class="input-group-addon">
                   <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask='"mask": "9999999999"' data-mask name="employeeMobileno" id="employeeMobileno">
                   <span class="input-group-btn">
            	    <button type="button" class="btn btn-success" onclick="return AddArchitecturalEmployee()"><i class="fa fa-plus"></i>Add</button>
              	   </span>
                  </div>
                  <span id="employeeMobilenoSpan" style="color:#FF0000"></span>
			   </div>
			 
		  </div>
        </div>		

          <div class="box-body">
           <div class="row">        
        	<div class="col-xs-12">
              <table class="table table-bordered" id="architecturalEmployeeListTable">
	              <tr bgcolor=#4682B4>
		              <th>Name</th>
		              <th>Designation</th>
		              <th>E-Mail Id</th>
		              <th>Mobile No.</th>
		              <th>Action</th>
	               </tr>
         
               	   <c:forEach items="${architecturalEmployeesList}" var="architecturalEmployeesList" varStatus="loopStatus">
                    <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                        <td>${architecturalEmployeesList.employeeName}</td>
                        <td>${architecturalEmployeesList.employeeDesignation}</td>
                        <td>${architecturalEmployeesList.employeeEmail}</td>
                        <td>${architecturalEmployeesList.employeeMobileno}</td>
                        <td><a onclick="DeleteArchitecturalEmployee(${architecturalEmployeesList.architecturalEmployeeId})" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td>
                     </tr>
                    </c:forEach>
              </table>
            </div>
           </div>
          </div> 
				 
		  </div>
		  
		  <br/><br/><br/>
		  
		  <div class="col-md-12">
    
	          <div class="box-body">
	             <div class="row">
	              </br>
	             <div class="col-xs-1">
	             </div>
	             
	             <div class="col-xs-4">
	               <div class="col-xs-3">
	              	<a href="ArchitecturalMaster"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
				</div>
				 </div>
				
				 <div class="col-xs-4">
	               <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
			     </div>
	
				<div class="col-xs-2">
				  <button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
	    	    </div> 
				 
	            </div>
			 </div>
		 </div>
		    
            <!-- /.col -->
       </div>
	 </div>
      
    </div>
 
</section>
</form>
</div>
  
<%@ include file="footer.jsp" %>
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->

<script>


function AddArchitecturalEmployee()
{

	clearall();
	
	//validation for employee name
	if(document.architecturalform.employeeName.value=="")
	{
		$('#employeeNameSpan').html('Employee name should not be empty..!');
		document.architecturalform.employeeName.value="";
		document.architecturalform.employeeName.focus();
		return false;
	}
	else if(document.architecturalform.employeeName.value.match(/^[\s]+$/))
	{
		$('#employeeNameSpan').html('Employee name must contains alphabets only..!');
		document.architecturalform.employeeName.value="";
		document.architecturalform.employeeName.focus();
		return false;
	}
	else if(!document.architecturalform.employeeName.value.match(/^[a-zA-Z\s]+$/))
	{
		$('#employeeNameSpan').html('Employee name must contains alphabets only..!');
		document.architecturalform.employeeName.value="";
		document.architecturalform.employeeName.focus();
		return false;
	}
	
	//validation for employee designation
	if(document.architecturalform.employeeDesignation.value=="Default")
	{
		$('#employeeDesignationSpan').html('Please, select designation..!');
		document.architecturalform.employeeDesignation.focus();
		return false;
	}
	
	//validation for employee email id
	if(document.architecturalform.employeeEmail.value=="")
	{
		$('#employeeEmailSpan').html('Please, enter email id..!');
		document.architecturalform.employeeEmail.focus();
		return false;
	}
	else if(!document.architecturalform.employeeEmail.value.match(/^([a-z0-9_\.\-])+\@(([a-z0-9\-])+\.)+([a-z0-9]{2,4})+$/))
	{
		$('#employeeEmailSpan').html('Please, enter valid email id..!');
		document.architecturalform.employeeEmail.value="";
		document.architecturalform.employeeEmail.focus();
		return false;
	}
	
	//validation for employee mobile number
	if(document.architecturalform.employeeMobileno.value=="")
	{
		$('#employeeMobilenoSpan').html('Please, enter mobile number..!');
		document.architecturalform.employeeMobileno.focus();
		return false;
	}
	else if(!document.architecturalform.employeeMobileno.value.match(/^[0-9]{10}$/))
	{
		$('#employeeMobilenoSpan').html('mobile number must be 10 digit numbers only with correct format..!');
		document.architecturalform.employeeMobileno.value="";
		document.architecturalform.employeeMobileno.focus();
		return false;
	}
	
	 
	$('#architecturalEmployeeListTable tr').detach();
	 
	 var architecturalId = $('#architecturalId').val();
	 var employeeName = $('#employeeName').val();
	 var employeeDesignation = $('#employeeDesignation').val();
	 var employeeEmail = $('#employeeEmail').val();
	 var employeeMobileno = $('#employeeMobileno').val();
	 
	 $.ajax({

		 url : '${pageContext.request.contextPath}/AddArchitecturalEmployee',
		type : 'Post',
		data : { architecturalId : architecturalId, employeeName : employeeName, employeeDesignation : employeeDesignation, employeeEmail : employeeEmail, employeeMobileno : employeeMobileno},
		dataType : 'json',
		success : function(result)
				  {
					   if (result) 
					   { 
							$('#architecturalEmployeeListTable').append('<tr style="background-color: #4682B4;"><th style="width:300px">Name</th><th style="width:150px">Designation</th><th style="width:150px">Email Id</th><th style="width:150px">Mobile No.</th><th style="width:100px">Action</th>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].architecturalEmployeeId;
									$('#architecturalEmployeeListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].employeeName+'</td><td>'+result[i].employeeDesignation+'</td><td>'+result[i].employeeEmail+'</td><td>'+result[i].employeeMobileno+'</td><td><a onclick="DeleteArchitecturalEmployee('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
								else
								{
									var id = result[i].architecturalEmployeeId;
									$('#architecturalEmployeeListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].employeeName+'</td><td>'+result[i].employeeDesignation+'</td><td>'+result[i].employeeEmail+'</td><td>'+result[i].employeeMobileno+'</td><td><a onclick="DeleteArchitecturalEmployee('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}
				  } 

		});
	 
	 $('#employeeName').val("");
	 $('#employeeEmail').val("");
	 $('#employeeMobileno').val("");
}

function DeleteArchitecturalEmployee(employeeId)
{
	var architecturalId = $('#architecturalId').val();
    var architecturalEmployeeId = employeeId;
    
    $('#architecturalEmployeeListTable tr').detach();
    
    $.ajax({

		 url : '${pageContext.request.contextPath}/DeleteArchitecturalEmployee',
		type : 'Post',
		data : { architecturalId : architecturalId, architecturalEmployeeId : architecturalEmployeeId },
		dataType : 'json',
		success : function(result)
				  {
					   if (result) 
					   { 
									$('#architecturalEmployeeListTable').append('<tr style="background-color: #4682B4;"><th style="width:300px">Name</th><th style="width:150px">Designation</th><th style="width:150px">Email Id</th><th style="width:150px">Mobile No.</th><th style="width:100px">Action</th>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].architecturalEmployeeId;
									$('#architecturalEmployeeListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].employeeName+'</td><td>'+result[i].employeeDesignation+'</td><td>'+result[i].employeeEmail+'</td><td>'+result[i].employeeMobileno+'</td><td><a onclick="DeleteArchitecturalEmployee('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
								else
								{
									var id = result[i].architecturalEmployeeId;
									$('#architecturalEmployeeListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].employeeName+'</td><td>'+result[i].employeeDesignation+'</td><td>'+result[i].employeeEmail+'</td><td>'+result[i].employeeMobileno+'</td><td><a onclick="DeleteArchitecturalEmployee('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}
				  } 

		});

}
	
function clearall()
{

	$('#architecturalfirmNameSpan').html('');
	$('#firmpanNumberSpan').html('');
	$('#firmgstNumberSpan').html('');
	
	$('#checkPrintingNameSpan').html('');
	
	$('#architecturalAddressSpan').html('');
	$('#countryIdSpan').html('');
	$('#stateIdSpan').html('');
	$('#cityIdSpan').html('');
	$('#locationareaIdSpan').html('');
	$('#areaPincodeSpan').html('');
	$('#architecturalBankNameSpan').html('');
	$('#branchNameSpan').html('');
	$('#bankifscCodeSpan').html('');
	$('#architecturalBankacnoSpan').html('');
	$('#statusSpan').html('');

	$('#creationDateSpan').html('');
	$('#updateDateSpan').html('');
	$('#userNameSpan').html('');
}

    function getpinCode()
    {

    	 $("#areaPincode").empty();
    	 var locationareaId = $('#locationareaId').val();
    	 var cityId = $('#cityId').val();
    	 var stateId = $('#stateId').val();
    	 var countryId = $('#countryId').val();
    	 $.ajax({

    		 url : '${pageContext.request.contextPath}/getallAreaList',
    		type : 'Post',
    		//data : { locationareaId : locationareaId, cityId : cityId, stateId : stateId, countryId : countryId},
    		dataType : 'json',
    		success : function(result)
    				  {
    						if (result) 
    						{
    							
    							for(var i=0;i<result.length;i++)
    							{
    								if(result[i].countryId==countryId)
    									{
    									if(result[i].stateId==stateId)
    										{
    											if(result[i].cityId==cityId)
    											{
    												if(result[i].locationareaId==locationareaId)
    												{
    													 $('#areaPincode').val(result[i].pinCode);
    												}
    											}
    										}
    									}
    								
    							 } 
    						
    						} 
    						else
    						{
    							alert("failure111");
    							//$("#ajax_div").hide();
    						}

    					}
    		});
    	
    }


 function validate()
 {
	    clearall();
	    
	    //validation for architectural first name
		if(document.architecturalform.architecturalfirmName.value=="")
		{
			$('#architecturalfirmNameSpan').html('Firm name should not be blank..!');
			document.architecturalform.architecturalfirmName.focus();
			return false;
		}
		else if(document.architecturalform.architecturalfirmName.value.match(/^[\s]+$/))
		{
			$('#architecturalfirmNameSpan').html('Firm name should not be blank..!');
			document.architecturalform.architecturalfirmName.value="";
			document.architecturalform.architecturalfirmName.focus();
			return false;
		}
	/* 
		
	    //validation for architectural email address
		if(document.architecturalform.architecturalEmailId.value=="")
		{
			$('#architecturalEmailIdSpan').html('Email Id should not be blank..!');
			document.architecturalform.architecturalEmailId.focus();
			return false;
		}
		else if(!document.architecturalform.architecturalEmailId.value.match(/^(([\-\w]+)\.?)+@(([\-\w]+)\.?)+\.[a-z]{2,4}$/))
		{
			$('#architecturalEmailIdSpan').html(' enter valid email id..!');
			document.architecturalform.architecturalEmailId.value="";
			document.architecturalform.architecturalEmailId.focus();
			return false;
		}
		
		//validation for architectural mobile number------------------------------
		if(document.architecturalform.architecturalMobileno.value=="")
		{
			$('#architecturalMobilenoSpan').html('Please, enter architectural mobile number..!');
			document.architecturalform.architecturalMobileno.value="";
			document.architecturalform.architecturalMobileno.focus();
			return false;
		}
		else if(!document.architecturalform.architecturalMobileno.value.match(/^[0-9]{10}$/))
		{
			$('#architecturalMobilenoSpan').html(' enter valid architectural mobile number..!');
			document.architecturalform.architecturalMobileno.value="";
			document.architecturalform.architecturalMobileno.focus();
			return false;	
		}
		  */
		
		  
	    //validation for architectural address----------------------
		if(document.architecturalform.architecturalAddress.value=="")
		{
			$('#architecturalAddressSpan').html('Please, enter architectural address name..!');
			document.architecturalform.architecturalAddress.focus();
			return false;
		}
		else if(document.architecturalform.architecturalAddress.value.match(/^[\s]+$/))
		{
			$('#architecturalAddressSpan').html('Please, enter architectural address ...!');
			document.architecturalform.architecturalAddress.focus();
			return false;
		}
	    
	    //validation for architectural country
		if(document.architecturalform.countryId.value=="Default")
		{
			$('#countryIdSpan').html('Please, select architectural country name..!');
			document.architecturalform.countryId.focus();
			return false;
		}
	    
	    //validation for architectural state
		if(document.architecturalform.stateId.value=="Default")
		{
			$('#stateIdSpan').html('Please, select architectural state name..!');
			document.architecturalform.stateId.focus();
			return false;
		}
	    
	    //validation for architectural city
		if(document.architecturalform.cityId.value=="Default")
		{
			$('#cityIdSpan').html('Please, select architectural city name..!');
			document.architecturalform.cityId.focus();
			return false;
		}
	    
	    //validation for architectural location area
		if(document.architecturalform.locationareaId.value=="Default")
		{
			$('#locationareaIdSpan').html('Please, select architectural area name..!');
			document.architecturalform.locationareaId.focus();
			return false;
		}

		if(document.architecturalform.architecturalBankName.value=="")
		{
			$('#architecturalBankNameSpan').html('Please, enter bank name..!');
			document.architecturalform.architecturalBankName.focus();
			return false;
		}
		if(document.architecturalform.branchName.value=="")
		{
			$('#branchNameSpan').html('Please, enter branch name..!');
			document.architecturalform.branchName.focus();
			return false;
		}
		if(document.architecturalform.bankifscCode.value=="")
		{
			$('#bankifscCodeSpan').html('Please, enter IFSC code..!');
			document.architecturalform.bankifscCode.focus();
			return false;
		}
		if(document.architecturalform.architecturalBankacno.value=="")
		{
			$('#architecturalBankacnoSpan').html('Please, enter Account number..!');
			document.architecturalform.architecturalBankacno.focus();
			return false;
		}
		else if(!document.architecturalform.architecturalBankacno.value.match(/^[0-9]+$/))
			{
				$('#architecturalBankacnoSpan').html('Please, enter valid architectural bank Ac/No..!');
				document.architecturalform.architecturalBankacno.value="";
				document.architecturalform.architecturalBankacno.focus();
				return false;
			}
	/* 	
		 if(document.architecturalform.architecturalBankacno.value.length!=0)
			{ 
			 if(!document.architecturalform.architecturalBankacno.value.match(/^[0-9]+$/))
				{
					$('#architecturalBankacnoSpan').html('Please, enter valid architectural bank Ac/No..!');
					document.architecturalform.architecturalBankacno.value="";
					document.architecturalform.architecturalBankacno.focus();
					return false;
				}
			} */ 
		 
}
 
function init()
{
	//clearall();
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	
	 
	 if(document.architecturalform.architecturalStatus.value == "Fail")
	 {
	  //	alert("Sorry, record is present already..!");
	 }
	 else if(document.architecturalform.architecturalStatus.value == "Success")
	 {
		 $('#statusSpan').html('Record added successfully..!');
	 }
	 
     document.architecturalform.architecturalfirmName.focus();
}


function getStateList()
{
	 $("#stateId").empty();
	 var countryId = $('#countryId').val();
	 
	$.ajax({

		url : '${pageContext.request.contextPath}/getStateList',
		type : 'Post',
		data : { countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select State-");
							$("#stateId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].stateId).text(result[i].stateName);
							    $("#stateId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get State List


function getCityList()
{
	 $("#cityId").empty();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();
	$.ajax({

		url : '${pageContext.request.contextPath}/getCityList',
		type : 'Post',
		data : { stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select City-");
							$("#cityId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].cityId).text(result[i].cityName);
							    $("#cityId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get City List

function getLocationAreaList()
{
	 $("#locationareaId").empty();
	 var cityId = $('#cityId').val();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getLocationAreaList',
		type : 'Post',
		data : { cityId : cityId, stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Location Area-");
							$("#locationareaId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].locationareaId).text(result[i].locationareaName);
							    $("#locationareaId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
	
}//end of get locationarea List

function getBranchList()
{
	 
	 $("#branchName").empty();
	 var bankName = $('#architecturalBankacname').val();
	
	 $.ajax({

		url : '${pageContext.request.contextPath}/getBranchList',
		type : 'Post',
		data : { bankName : bankName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Branch Name-");
							$("#branchName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].branchName).text(result[i].branchName);
							    $("#branchName").append(option);
							} 
						} 
						else
						{
							alert("failure111");
						}

					}
		});	
}

function getBranchIfsc()
{
	 //$("#bankifscCode").val('');
	 var bankName = $('#architecturalBankacname').val();
     var branchName = $('#branchName').val();
	 $.ajax({

		url : '${pageContext.request.contextPath}/getBranchIfsc',
		type : 'Post',
		data : { bankName : bankName, branchName : branchName },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							$('#bankifscCode').val(result[0].bankifscCode);
							/* for(var i=0;i<result.length;i++)
							{
								 var option = $('<option />');
							    option.attr('value',result[i].branchName).text(result[i].branchName);
							    $("#bankifscCode").append(option); 
							}   */
						} 
						else
						{
							alert("failure111");
						}

					}
		});
}


  $(function ()
  {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
     $('#architecturalDob').datepicker({
      autoclose: true
    })
 $('#architecturalJoiningdate').datepicker({
      autoclose: true
    })

$('#architecturalAnniversaryDate').datepicker({
    autoclose: true
  })
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>

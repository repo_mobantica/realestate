<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Add Item</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
    </script>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>


<div class="wrapper">

  <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
  <%@ include file="menu.jsp" %>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Item Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Add Item</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="itemform" action="${pageContext.request.contextPath}/AddItem" onSubmit="return validate()" method="post">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
             <span id="statusSpan" style="color:#008000"></span>
           
				<div class="box-body">
              <div class="row">
                  <div class="col-xs-2">
                  <label >Item Id</label>
                  <input type="text" class="form-control"  id="itemId" placeholder="ID" name="itemId" value="${itemCode}" readonly>
                </div>               
              </div>
            </div>
				
				
			<div class="box-body">
              <div class="row">
              
                <div class="col-xs-3">
			      <label>Item Main Category</label> <label class="text-red">* </label>
                  <select class="form-control" name="suppliertypeId" id="suppliertypeId" onchange="getMainSubItemCategory(this.value)">
				  <option selected="" value="Default">-Select Category-</option>
                   <c:forEach var="suppliertypeList" items="${suppliertypeList}" >
					<option value="${suppliertypeList.suppliertypeId}">${suppliertypeList.supplierType}</option>
					 </c:forEach>
                  </select>
			         <span id="suppliertypeIdSpan" style="color:#FF0000"></span>
			     </div> 
			     
			       <div class="col-xs-3">
			      <label>Item Main Sub-Category</label> <label class="text-red">* </label>
                  <select class="form-control" name="subsuppliertypeId" id="subsuppliertypeId" onchange="getSubItemCategory(this.value)">
				  <option selected="" value="Default">-Select Sub Item Category-</option>
                  </select>
			         <span id="subsuppliertypeIdSpan" style="color:#FF0000"></span>
			     </div>
			  <!-- 
			     <div class="col-xs-3">
			      <label>Item Sub-Category</label> <label class="text-red">* </label>
                  <select class="form-control" name="subItemCategoryName" id="subItemCategoryName">
				  <option selected="" value="Default">-Select Sub Item Category-</option>
               
                  </select>
			         <span id="subItemCategoryNameSpan" style="color:#FF0000"></span>
			     </div>
			     -->
			     <div class="col-xs-3">
				<label for="itemName">Item Name </label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="itemName" name="itemName" placeholder="Item Name"  style="text-transform: capitalize;">
                   <span id="itemNameSpan" style="color:#FF0000"></span>
                </div>
                  	 <div class="col-xs-3">
			    <label for="intemInitialPrice">Item Initial Price</label> <label class="text-red">* </label>
                <input type="text" class="form-control" id="intemInitialPrice" placeholder="intem Initial Price" name="intemInitialPrice" >
               <span id="intemInitialPriceSpan" style="color:#FF0000"></span>
                 </div>
               </div>
            </div>
					<div class="box-body">
              <div class="row">
              
			
                  <div class="col-xs-3">
			    <label for="itemInitialDiscount">Item Initial Discount %</label> <label class="text-red">* </label>
                <input type="text" class="form-control" id="itemInitialDiscount" placeholder="item Initial Discount %" name="itemInitialDiscount" >
               <span id="itemInitialDiscountSpan" style="color:#FF0000"></span>
                 </div>
                  <div class="col-xs-3">
			    <label for="minimumStock">Minimum Stock Qty</label> <label class="text-red">* </label>
                <input type="text" class="form-control" id="minimumStock" placeholder="Minimum Stock Qty" name="minimumStock" >
               <span id="minimumStockSpan" style="color:#FF0000"></span>
                 </div>
                  <div class="col-xs-3">
			    <label for="currentStock">Current Stock Qty</label> <label class="text-red">* </label>
                <input type="text" class="form-control" id="currentStock" placeholder="Current Stock Qty" name="currentStock" >
               <span id="currentStockSpan" style="color:#FF0000"></span>
                 </div>
                  <div class="col-xs-3">
			    <label for="hsnCode">HSN Code</label> <label class="text-red">* </label>
                <input type="text" class="form-control" id="hsnCode" placeholder="HSN Code" name="hsnCode"  style="text-transform:uppercase">
               <span id="minimumStockSpan" style="color:#FF0000"></span>
                 </div>
              <!-- 
                  <div class="col-xs-3">
			    <label for="itemSize">Item Size</label> 
                <input type="text" class="form-control" id="itemSize" placeholder="item Size" name="itemSize" >
               <span id="itemSizeSpan" style="color:#FF0000"></span>
                 </div>
                  -->
                 </div>
                 </div>
			<div class="box-body">
              <div class="row">
              
			  	
                  <div class="col-xs-3">
			    <label for="gstPer">GST %</label> <label class="text-red">* </label>
                <input type="text" class="form-control" id="gstPer" placeholder="GST %" name="gstPer" >
               <span id="currentStockSpan" style="color:#FF0000"></span>
                 </div>
              </div>
            </div>
           
             	
             <input type="hidden" id="itemStatus" name="itemStatus" value="Active">	
         		 <input type="hidden" id="status" name="status" value="${Status}">	
				<input type="hidden" id="creationDate" name="creationDate" >
				<input type="hidden" id="updateDate" name="updateDate" >
				<input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>">
				
            </div>
          
            
			
          </div>
			      <div class="box-body">
              <div class="row">
              </br>
                     <div class="col-xs-3">
                	<div class="col-xs-2">
                	<a href="ItemMaster"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
			     </div>
			     </div>
				  <div class="col-xs-2">
                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
              
			     </div>
					<div class="col-xs-2">
			  <button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
              
			     </div> 
		   
              </div>
			  </div>
          <!-- /.row -->
        </div>   
      </div>
    </section>
	</form>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script>

function getSubItemCategory()
{
	$('#subItemCategoryName').empty();
	var suppliertypeId = $('#suppliertypeId').val();
	var subsuppliertypeId = $('#subsuppliertypeId').val();
	$.ajax({

		url : '${pageContext.request.contextPath}/getSubItemCategoryList',
		type : 'Post',
		data : { suppliertypeId : suppliertypeId, subsuppliertypeId : subsuppliertypeId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select Sub Item Category-");
								$("#subItemCategoryName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].subItemCategoryName).text(result[i].subItemCategoryName);
							    $("#subItemCategoryName").append(option);
							} 
						} 
						else
						{
							alert("failure111");
						}

					}
		});	
	
}

function getMainSubItemCategory()
{
	$('#subsuppliertypeId').empty();
	var suppliertypeId = $('#suppliertypeId').val();
	$.ajax({

		url : '${pageContext.request.contextPath}/getSubSupplierTypeList',
		type : 'Post',
		data : { suppliertypeId : suppliertypeId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select Sub Item Category-");
								$("#subsuppliertypeId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].subsuppliertypeId).text(result[i].subsupplierType);
							    $("#subsuppliertypeId").append(option);
							} 
						} 
						else
						{
							alert("failure111");
						}

					}
		});	
	
}
function clearall()
{
	$('#itemNameSpan').html('');
	$('#suppliertypeIdSpan').html('');
	$('#subsuppliertypeIdSpan').html('');
	$('#minimumStockSpan').html('');
	$('#currentStockSpan').html('');
	$('#hsnCodeSpan').html('');
	$('#gstPerSpan').html('');
	$('#intemInitialPrice').html('');
	$('#itemInitialDiscount').html('');
}
function validate()
{ 
	clearall();
	if(document.itemform.suppliertypeId.value=="Default")
	{
		$('#suppliertypeIdSpan').html('Please, Select Item Category Name');
		document.itemform.suppliertypeId.focus();
		return false;
	}
	
	if(document.itemform.subsuppliertypeId.value=="Default")
	{
		$('#subsuppliertypeIdSpan').html('Please, Select Item Sub Category Name');
		document.itemform.subsuppliertypeId.focus();
		return false;
	}
	
		if(document.itemform.itemName.value=="")
		{
			$('#itemNameSpan').html('Please, enter item Name');
			document.itemform.itemName.focus();
			return false;
		}
		else if(document.itemform.itemName.value.match(/^[\s]+$/))
		{
			$('#itemNameSpan').html('Please, enter item name..!');
			document.itemform.itemName.value="";
			document.itemform.itemName.focus();
			return false; 	
		}
	
		if(document.itemform.intemInitialPrice.value=="")
		{
			$('#intemInitialPriceSpan').html('Please intem Initial Price');
			document.itemform.intemInitialPrice.focus();
			return false;
		}
		else if(document.itemform.intemInitialPrice.value.match(/^[\s]+$/))
		{
			$('#minimumStockSpan').html('Please, enter intem Initial Price..!');
			document.itemform.intemInitialPrice.value="";
			document.itemform.intemInitialPrice.focus();
			return false; 	
		}
		else if(!document.itemform.intemInitialPrice.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			if(!document.itemform.intemInitialPrice.value.match(/^[0-9]+$/))
			{
				$('#intemInitialPriceSpan').html('Please, use only digit for intemInitialPrice..!');
				document.itemform.intemInitialPrice.value="";
				document.itemform.intemInitialPrice.focus();
				return false;
			}
		}
		if(document.itemform.minimumStock.value=="")
		{
			$('#minimumStockSpan').html('Please Fill Minimum Stock');
			document.itemform.minimumStock.focus();
			return false;
		}
		else if(document.itemform.minimumStock.value.match(/^[\s]+$/))
		{
			$('#minimumStockSpan').html('Please, enter minimum stock..!');
			document.itemform.minimumStock.value="";
			document.itemform.minimumStock.focus();
			return false; 	
		}
		else if(!document.itemform.minimumStock.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
		if(!document.itemform.minimumStock.value.match(/^[0-9]+$/))
			{
				$('#minimumStockSpan').html('Please, use only digit for minimum stock..!');
				document.itemform.minimumStock.value="";
				document.itemform.minimumStock.focus();
				return false;
			}
		}
		
		if(document.itemform.currentStock.value=="")
		{
			$('#currentStockSpan').html('Please Fill current Stock');
			document.itemform.currentStock.focus();
			return false;
		}
		else if(document.itemform.currentStock.value.match(/^[\s]+$/))
		{
			$('#currentStockSpan').html('Please, enter current stock..!');
			document.itemform.currentStock.value="";
			document.itemform.currentStock.focus();
			return false; 	
		}
		else if(!document.itemform.currentStock.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			if(!document.itemform.currentStock.value.match(/^[0-9]+$/))
			{
				$('#currentStockSpan').html('Please, use only digit for current stock..!');
				document.itemform.currentStock.value="";
				document.itemform.currentStock.focus();
				return false;
			}
		}
		
		if(document.itemform.hsnCode.value=="")
		{
			$('#hsnCodeSpan').html('Please, enter HSC Code');
			document.itemform.hsnCode.focus();
			return false;
		}
		else if(document.itemform.hsnCode.value.match(/^[\s]+$/))
		{
			$('#hsnCodeSpan').html('Please, enter HSN code %..!');
			document.itemform.hsnCode.value="";
			document.itemform.hsnCode.focus();
			return false; 	
		}
		
		if(document.itemform.gstPer.value=="")
		{
			$('#gstPerSpan').html('Please, enter GST Per');
			document.itemform.gstPer.focus();
			return false;
		}
		else if(document.itemform.gstPer.value.match(/^[\s]+$/))
		{
			$('#gstPerSpan').html('Please, enter GST %..!');
			document.itemform.gstPer.value="";
			document.itemform.gstPer.focus();
			return false; 	
		}
		else if(!document.itemform.gstPer.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			if(!document.itemform.gstPer.value.match(/^[0-9]+$/))
			{
				$('#gstPerSpan').html('Please, use only digit for GST %..!');
				document.itemform.gstPer.value="";
				document.itemform.gstPer.focus();
				return false;
			}
		}
}
function init()
{
	clearall();
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	
	
	
	 if(document.itemform.status.value=="Fail")
	 {
	  	alert("Sorry, record is present already..!");
	 }
	 else if(document.itemform.status.value=="Success")
	 {
		 $('#statusSpan').html('Record added successfully..!');
	 }
	document.itemform.itemName.focus();
}



</script>
</body>
</html>

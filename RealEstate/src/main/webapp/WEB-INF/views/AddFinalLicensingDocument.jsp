<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Real Estate | Add Licensing Documents</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


<style type="text/css">
tr.odd {
	background-color: #CCE5FF
}

tr.even {
	background-color: #F0F8FF
}
</style>

<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#statusSpan').delay(1000).fadeOut();
	});
</script>
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control", "no-cache,no-store,must-revalidate");//HTTP 1.1
		response.setHeader("Pragma", "no-cache"); //HTTP 1.0
		response.setDateHeader("Expires", 0);

		if (session != null) {
			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null
					|| session.getAttribute("profile_img") == null) {
				response.sendRedirect("login");
			}
		}
	%>

	<div class="wrapper">

		<%@ include file="headerpage.jsp"%>
		<!-- Left side column. contains the logo and sidebar -->
		<%@ include file="menu.jsp"%>
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Add Licensing Documents : <small>Preview</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
					<li><a href="#">Licensing Document Master</a></li>
					<li class="active">Add Licensing Documents</li>
				</ol>
			</section>

			<!-- Main content -->

				<section class="content">

					<!-- SELECT2 EXAMPLE -->
					<div class="box box-default">
						<div class="box-body">
							<div class="row">
							
							<form name="licensingFinalDocumentform" action="${pageContext.request.contextPath}/AddFinalLicensingDocument" method="post" onSubmit="return UploadDocument1()" enctype="multipart/form-data">
			
								<div class="col-md-12">
									<span id="statusSpan" style="color: #008000"></span>

									<div class="box-body">
										<div class="row">
											<input type="hidden" class="form-control" id="projectId" name="projectId" value="${projectId}">
											<div class="col-xs-2">
												<label for="description">Description</label><label class="text-red">* </label>
												<input type="text" class="form-control" id="description" placeholder="Description" name="description" style="text-transform: capitalize;">
												<span id="description1Span" style="color: #FF0000"></span>
											</div>
											 	 	
											 	
											<div class="col-xs-2">
												<label for="invertedDate">Inverted Date</label><label class="text-red">* </label>
												<input type="text" class="form-control" id="invertedDate" placeholder="Inverted Date" name="invertedDate" style="text-transform: capitalize;">
												<span id="invertedDate1Span" style="color: #FF0000"></span>
											</div>
											 	 	
											<div class="col-xs-1">
												<label for="tokenNumber">Token No</label><label class="text-red">* </label>
												<input type="text" class="form-control" id="tokenNumber" placeholder="Tokan Number" name="tokenNumber" style="text-transform: capitalize;">
												<span id="tokenNumber1Span" style="color: #FF0000"></span>
											</div>
											
											<div class="col-xs-2">
												<div class="form-group">

													<label>Token Select File</label><label class="text-red">* </label>
													<div class="input-group">
														<div class="input-group-addon">
															<i class="fa fa-file"></i>
														</div>
														  <input type="file" id="tokenDocument" name="tokenDocument" accept="application/pdf,image/x-png,image/gif,image/jpeg" onchange="return getFileExtension()">
													</div>
													<p class="help-block">Only select PDF, PNG, JPEG, and JPG formats.</p>
													<span id="tokenDocumentMsg1Span" style="color: #FF0000"></span>
												</div>
											</div>
		         
											<div class="col-xs-2">
												<label for="challanAmount">Challan Amt</label><label class="text-red">* </label>
												<input type="text" class="form-control" id="challanAmount" placeholder="Challan Amount" name="challanAmount" style="text-transform: capitalize;">
												<span id="challanAmount1Span" style="color: #FF0000"></span>
											</div>
										 
											<div class="col-xs-2">
												<div class="form-group">

													<label>NOC Select File</label><label class="text-red">* </label>
													<div class="input-group">
														<div class="input-group-addon">
															<i class="fa fa-file"></i>
														</div>
														  <input type="file" id="nocDocument" name="nocDocument" accept="application/pdf,image/x-png,image/gif,image/jpeg" onchange="return getFileExtension()">
														<span class="input-group-btn">
														  <!-- <input type="submit" class="btn btn-success" value=""><i class="fa fa-upload"></i>&nbspUpload</button> -->
														 		</span>
													</div>
													<p class="help-block">Only select PDF, PNG, JPEG, and JPG formats.</p>
													<span id="nocDocumentMsg1Span" style="color: #FF0000"></span>
												</div>
											</div>

 	
											<div class="col-xs-1">
												<label for="challanAmount">.</label>
										 <button type="submit" class="btn btn-success" name="submit"><i class="fa fa-upload"></i>&nbsp Add</button>
												
											</div>



										</div>

									</div>
								</div>

							</form>


								<div class="box-body">
									<div class="row">
									 
										<div class="col-xs-12">
											<div class="table-responsive">
												<table class="table table-bordered" id="licensingFinalDocumentListTable">
													<thead>
														<tr bgcolor=#4682B4>
															<th style="width: 10px">Sr.No</th>
															<th>Description</th>
															<th>Inverted Date</th>
															<th>Token Number</th>
															<th>Challan Amount</th>
															<th>View Documents</th>
														</tr>
													</thead>
													<tbody>

														<s:forEach items="${finalDocumentList}" var="finalDocumentList" varStatus="loopStatus">
															<tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
															 
																<td>${loopStatus.index+1}</td>
																<td>${finalDocumentList.description}</td>
																<td>${finalDocumentList.invertedDate}</td>
																<td>${finalDocumentList.tokenNumber}</td>
																<td>${finalDocumentList.challanAmount}</td>
																
																<td>
																    <a target="_blank" href="${pageContext.request.contextPath}/ViewLicensingTokenDocument?documentId=${finalDocumentList.documentId}" class="btn btn-info btn-sm" data-toggle="tooltip" title="View Token Documents"><i class="glyphicon glyphicon-eye-open"></i></a>
																  
																   | <a target="_blank" href="${pageContext.request.contextPath}/ViewLicensingnocDocument?documentId=${finalDocumentList.documentId}" class="btn btn-info btn-sm" data-toggle="tooltip" title="View NOC Documents"><i class="glyphicon glyphicon-eye-open"></i></a>
																   
																    |<a onclick="DeleteLicensingFinalDocument('${finalDocumentList.documentId}')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete Documents"><i class="glyphicon glyphicon-remove"></i></a>
																</td>
															</tr>
														</s:forEach>

													</tbody>
												</table>
											</div>
										</div>
										
										
									</div>
								</div>

								<div class="box-body">
									<div class="row">
									    <input type="hidden" id="statusSpan" name="statusSpan" value="${documentStatus}">	
				 						<input type="hidden" id="creationDate" name="creationDate" value="">
				 						<input type="hidden" id="updateDate" name="updateDate" value="">
				 						<input type="hidden" id="userName" name="userName" value="">
									</div>
								</div>

								<div class="box-body">
									<div class="row">

										<div class="col-xs-3"></div>
										<div class="col-xs-2">
											<a href="FinalLicensingDepartmentMaster"><button type="button" class="btn btn-block btn-primary" value="reset" style="width: 90px">Back</button></a>
										</div>

										<div class="col-xs-2">
											<button type="reset" class="btn btn-default" value="reset" style="width: 90px">Reset</button>
										</div>

										<div class="col-xs-3">
											<a href="LicensingDepartmentMaster"><button type="button" class="btn btn-success" value="Submit" style="width: 90px">Submit</button></a>
										</div>

									</div>
								</div>

							</div>
						</div>
					</div>

				</section>
		</div>

		<%@ include file="footer.jsp"%>
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

	<!-- jQuery 3 -->
	<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
	<!-- Page script -->

	<script>
	
	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function(e) {
				$('#document-tag').attr('src', e.target.result);
			}
			reader.readAsDataURL(input.files[0]);
		}
	}
	$("#nocDocument").change(function() {
		readURL(this);
	});

	$("#tokenDocument").change(function() {
		readURL(this);
	});
	
	
		function clearall()
		{
		//	$('#provisionalNocSpan').html('');
			$('#descriptionSpan').html('');
			$('#invertedDateSpan').html('');
			$('#tokenNumberSpan').html('');
			$('#challanNumberSpan').html('');
			$('#challanAmountSpan').html('');
			
			$('#nocDocumentMsgSpan').html('');
			$('#tokenDocumentMsgSpan').html('');

			$('#description1Span').html('');
			$('#invertedDate1Span').html('');
			$('#tokenNumber1Span').html('');
			$('#challanNumber1Span').html('');
			$('#challanAmount1Span').html('');
			
			$('#nocDocumentMsg1Span').html('');
			$('#tokenDocumentMsg1Span').html('');
		}

		function getFileExtension()
		{
		   clearall();
		   
		   var fileName = $('#nocDocument').val();
		   var flag = 0;
		   
		   $.ajax({

				url : '${pageContext.request.contextPath}/checkDocumentExtension',
				type : 'Post',
				data : { fileName : fileName },
				dataType : 'text',
				success : function(result)
						  {
								if (result == "OK") 
								{
									flag = 1;
								} 
								else
								{
									resetFilePath();
									flag = 0;
								}
						  }
				});
		}
		/* 
		function resetFilePath()
		{
			$('#documentMsgSpan').html('Please Select Only PDF File Or Image..!');
		    document.licensingProvisionalDocumentform.document.value="";
			document.licensingProvisionalDocumentform.document.focus();
		}
		 */
		 
		function UploadDocument()
		{
			clearall();
/* 
			if (document.licensingProvisionalDocumentform.provisionalNoc.value == "")
			{
				$('#provisionalNocSpan').html('provisional Noc should not be blank..!');
				document.licensingProvisionalDocumentform.provisionalNoc.focus();
				return false;
			} 
			else if (document.licensingProvisionalDocumentform.provisionalNoc.value.match(/^[\s]+$/))
			{
				$('#provisionalNocSpan').html('Provisional Noc should not be blank..!');
				document.licensingProvisionalDocumentform.provisionalNoc.value = "";
				document.licensingProvisionalDocumentform.provisionalNoc.focus();
				return false;
			}
 */
			if (document.licensingProvisionalDocumentform.description.value == "")
			{
				$('#descriptionSpan').html('Description should not be blank..!');
				document.licensingProvisionalDocumentform.description.focus();
				return false;
			} 
			else if (document.licensingProvisionalDocumentform.description.value.match(/^[\s]+$/))
			{
				$('#descriptionSpan').html('Description should not be blank..!');
				document.licensingProvisionalDocumentform.description.value = "";
				document.licensingProvisionalDocumentform.description.focus();
				return false;
			}
			
			if (document.licensingProvisionalDocumentform.invertedDate.value == "")
			{
				$('#invertedDateSpan').html('Inverted Date should not be blank..!');
				document.licensingProvisionalDocumentform.invertedDate.focus();
				return false;
			} 
			else if (document.licensingProvisionalDocumentform.invertedDate.value.match(/^[\s]+$/))
			{
				$('#invertedDateSpan').html('Inverted Date should not be blank..!');
				document.licensingProvisionalDocumentform.invertedDate.value = "";
				document.licensingProvisionalDocumentform.invertedDate.focus();
				return false;
			}

			if (document.licensingProvisionalDocumentform.tokenNumber.value == "")
			{
				$('#tokenNumberSpan').html('Token Number should not be blank..!');
				document.licensingProvisionalDocumentform.tokenNumber.focus();
				return false;
			} 
			else if (document.licensingProvisionalDocumentform.tokenNumber.value.match(/^[\s]+$/))
			{
				$('#tokenNumberSpan').html('Token Date should not be blank..!');
				document.licensingProvisionalDocumentform.tokenNumber.value = "";
				document.licensingProvisionalDocumentform.tokenNumber.focus();
				return false;
			}
			
/* 
			if (document.licensingProvisionalDocumentform.challanNumber.value == "")
			{
				$('#challanNumberSpan').html('Challan Number should not be blank..!');
				document.licensingProvisionalDocumentform.challanNumber.focus();
				return false;
			} 
			else if (document.licensingProvisionalDocumentform.challanNumber.value.match(/^[\s]+$/))
			{
				$('#challanNumberSpan').html('Challan Number should not be blank..!');
				document.licensingProvisionalDocumentform.challanNumber.value = "";
				document.licensingProvisionalDocumentform.challanNumber.focus();
				return false;
			}
 */
			if (document.licensingProvisionalDocumentform.challanAmount.value == "")
			{
				$('#challanAmountSpan').html('Challan Amount should not be blank..!');
				document.licensingProvisionalDocumentform.challanAmount.focus();
				return false;
			} 
			else if (document.licensingProvisionalDocumentform.challanAmount.value.match(/^[\s]+$/))
			{
				$('#challanAmountSpan').html('Challan Amount should not be blank..!');
				document.licensingProvisionalDocumentform.challanAmount.value = "";
				document.licensingProvisionalDocumentform.challanAmount.focus();
				return false;
			}
			
			if (document.licensingProvisionalDocumentform.tokenDocument.value == "")
			{
				$('#tokenDocumentMsgSpan').html('Please select a Token document to upload..!');
				document.licensingProvisionalDocumentform.tokenDocument.value="";
				document.licensingProvisionalDocumentform.tokenDocument.focus();
				return false;
			}

			if (document.licensingProvisionalDocumentform.nocDocument.value == "")
			{
				$('#nocDocumentMsgSpan').html('Please select a NOC document to upload..!');
				document.licensingProvisionalDocumentform.nocDocument.value="";
				document.licensingProvisionalDocumentform.nocDocument.focus();
				return false;
			}
			
		}

			function UploadDocument1()
			{
				clearall();
	
				if (document.licensingFinalDocumentform.description.value == "")
				{
					$('#description1Span').html('Description should not be blank..!');
					document.licensingFinalDocumentform.description.focus();
					return false;
				} 
				else if (document.licensingFinalDocumentform.description.value.match(/^[\s]+$/))
				{
					$('#description1Span').html('Description should not be blank..!');
					document.licensingFinalDocumentform.description.value = "";
					document.licensingFinalDocumentform.description.focus();
					return false;
				}
				
				if (document.licensingFinalDocumentform.invertedDate.value == "")
				{
					$('#invertedDate1Span').html('Inverted Date should not be blank..!');
					document.licensingFinalDocumentform.invertedDate.focus();
					return false;
				} 
				else if (document.licensingFinalDocumentform.invertedDate.value.match(/^[\s]+$/))
				{
					$('#invertedDate1Span').html('Inverted Date should not be blank..!');
					document.licensingFinalDocumentform.invertedDate.value = "";
					document.licensingFinalDocumentform.invertedDate.focus();
					return false;
				}

				if (document.licensingFinalDocumentform.tokenNumber.value == "")
				{
					$('#tokenNumber1Span').html('Token Number should not be blank..!');
					document.licensingFinalDocumentform.tokenNumber.focus();
					return false;
				} 
				else if (document.licensingFinalDocumentform.tokenNumber.value.match(/^[\s]+$/))
				{
					$('#tokenNumber1Span').html('Token Date should not be blank..!');
					document.licensingFinalDocumentform.tokenNumber.value = "";
					document.licensingFinalDocumentform.tokenNumber.focus();
					return false;
				}
				
	
				if (document.licensingFinalDocumentform.challanAmount.value == "")
				{
					$('#challanAmount1Span').html('Challan Amount should not be blank..!');
					document.licensingFinalDocumentform.challanAmount.focus();
					return false;
				} 
				else if (document.licensingFinalDocumentform.challanAmount.value.match(/^[\s]+$/))
				{
					$('#challanAmount1Span').html('Challan Amount should not be blank..!');
					document.licensingFinalDocumentform.challanAmount.value = "";
					document.licensingFinalDocumentform.challanAmount.focus();
					return false;
				}
				
				if (document.licensingFinalDocumentform.tokenDocument.value == "")
				{
					$('#tokenDocumentMsg1Span').html('Please select a Token document to upload..!');
					document.licensingFinalDocumentform.tokenDocument.value="";
					document.licensingFinalDocumentform.tokenDocument.focus();
					return false;
				}

				if (document.licensingFinalDocumentform.nocDocument.value == "")
				{
					$('#nocDocumentMsg1Span').html('Please select a NOC document to upload..!');
					document.licensingFinalDocumentform.nocDocument.value="";
					document.licensingProvisionalDocumentform.nocDocument.focus();
					return false;
				}
				
			}
			
		function DeleteLicensingProvisinalDocument(documentId)
		{
			var projectId  = $('#projectId').val();

			$("#licensingDocumentListTable tr").detach();
			
			$.ajax({

				url : '${pageContext.request.contextPath}/DeleteLicensingProvisinalDocument',
				type : 'Post',
				data : { documentId : documentId, projectId : projectId},
				dataType: 'json',
				success : function(result)
						  {
								$('#licensingDocumentListTable').append('<tr style="background-color: #4682B4;"><th style="width: 10px">Sr.No</th><th>Description</th><th>Inverted Date</th><th>Token Number</th><th>Challan Amount</th><th>View Documents</th></tr>');
								
								for(var i=0;i<result.length;i++)
								{ 

									if(i%2==0)
									{
										var id = result[i].documentId;
										$('#licensingDocumentListTable').append('<tr style="background-color: #F0F8FF;"><td>'+(i+1)+'</td><td>'+result[i].description+'</td><td>'+result[i].invertedDate+'</td><td>'+result[i].tokenNumber+'</td><td>'+result[i].challanAmount+'</td><td><a href="${pageContext.request.contextPath}/ViewLicensingDocument?documentId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="View"><i class="glyphicon glyphicon-eye-open"></i></a>|<a onclick="DeleteLicensingProvisinalDocument('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td></tr>');
									}
									else
									{
										var id = result[i].documentId;
										$('#licensingDocumentListTable').append('<tr style="background-color: #CCE5FF;"><td>'+(i+1)+'</td><td>'+result[i].description+'</td><td>'+result[i].invertedDate+'</td><td>'+result[i].tokenNumber+'</td><td>'+result[i].challanAmount+'</td><td><a href="${pageContext.request.contextPath}/ViewLicensingDocument?documentId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="View"><i class="glyphicon glyphicon-eye-open"></i></a>|<a onclick="DeleteLicensingProvisinalDocument('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td></tr>');
									}
								
								 }
						  }
				});
		}
		

		function DeleteLicensingFinalDocument(documentId)
		{
			var projectId  = $('#projectId').val();

			$("#licensingFinalDocumentListTable tr").detach();
			
			$.ajax({

				url : '${pageContext.request.contextPath}/DeleteLicensingFinalDocument',
				type : 'Post',
				data : { documentId : documentId, projectId : projectId},
				dataType: 'json',
				success : function(result)
						  {
								$('#licensingFinalDocumentListTable').append('<tr style="background-color: #4682B4;"><th style="width: 10px">Sr.No</th><th>Description</th><th>Inverted Date</th><th>Token Number</th><th>Challan Amount</th><th>View Documents</th></tr>');
								
								for(var i=0;i<result.length;i++)
								{ 

									if(i%2==0)
									{
										var id = result[i].documentId;
										$('#licensingFinalDocumentListTable').append('<tr style="background-color: #F0F8FF;"><td>'+(i+1)+'</td><td>'+result[i].description+'</td><td>'+result[i].invertedDate+'</td><td>'+result[i].tokenNumber+'</td><td>'+result[i].challanAmount+'</td><td><a href="${pageContext.request.contextPath}/ViewLicensingDocument?documentId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="View"><i class="glyphicon glyphicon-eye-open"></i></a>|<a onclick="DeleteLicensingFinalDocument('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td></tr>');
									}
									else
									{
										var id = result[i].documentId;
										$('#licensingFinalDocumentListTable').append('<tr style="background-color: #CCE5FF;"><td>'+(i+1)+'</td><td>'+result[i].description+'</td><td>'+result[i].invertedDate+'</td><td>'+result[i].tokenNumber+'</td><td>'+result[i].challanAmount+'</td><td><a href="${pageContext.request.contextPath}/ViewLicensingDocument?documentId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="View"><i class="glyphicon glyphicon-eye-open"></i></a>|<a onclick="DeleteLicensingFinalDocument('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td></tr>');
									}
								
								 }
						  }
				});
		}
		
		function init() 
		{
			clearall();
			var date  = new Date();
			var year  = date.getFullYear();
			var month = date.getMonth() + 1;
			var day   = date.getDate();

			document.getElementById("creationDate").value = day + "/" + month + "/" + year;
			document.getElementById("updateDate").value = day + "/" + month + "/" + year;

			if (document.licensingProvisionalDocumentform.statusSpan.value == "Fail") 
			{
				//	alert("Sorry, record is present already..!");
			}
			else if (document.licensingProvisionalDocumentform.statusSpan.value == "Success") 
			{
				$('#statusSpan').html('Document Uploaded successfully..!');
			}

			document.licensingProvisionalDocumentform.description.focus();
	   }
		
	</script>
</body>
</html>
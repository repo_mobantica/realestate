<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate |Add Tax</title>
  <!-- Tell the browser to be responsive to screen width -->
   <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  
  <!-- Google Font -->
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
    </script>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

   <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Tax Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Add Tax</li>
      </ol>
    </section>

    
<form name="taxform" action="${pageContext.request.contextPath}/AddTax" onSubmit="return validate()" method="post">
    
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
                <span id="statusSpan" style="color:#008000"></span>
              <!-- /.form-group -->
          <div class="box-body">
          <div class="row">
            <div class="col-md-2">
                  <label >Tax Id</label>
                  <input type="text" class="form-control" id="taxId" name="taxId" value="${taxCode}" readonly>
                </div>
			 </div>
          </div>
          
		<div class="box-body">
          <div class="row">
          
              <div class="col-md-3">
                  <label for="taxName">Tax Type</label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="taxType" placeholder="Tax Type" name="taxType"  style="text-transform:uppercase" >
                  <span id="taxTypeSpan" style="color:#FF0000"></span>
               </div>
                
              <div class="col-md-3">
                  <label for="taxName">Tax Name</label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="taxName" placeholder="Tax Name" name="taxName" style="text-transform:uppercase">
                  <span id="taxNameSpan" style="color:#FF0000"></span>
               </div>
               
            <div class="col-md-2">
                  <label for="cgstPercentage">CGST Percentage</label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="cgstPercentage" placeholder="CGST Tax Percentage" name="cgstPercentage" onchange="CalcuateSGST(this.value)" >
                <span id="cgstPercentageSpan" style="color:#FF0000"></span>
            </div>
                    
            <div class="col-md-2">
                  <label for="sgstPercentage">SGST Percentage</label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="sgstPercentage" placeholder="SGST Tax Percentage" name="sgstPercentage" onchange="CalcuateSGST(this.value)" >
                <span id="taxPercentageSpan" style="color:#FF0000"></span>
            </div>
            
            <div class="col-md-2">
                  <label for="taxPercentage">Total GST Tax Percentage</label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="taxPercentage" placeholder="Tax Percentage" name="taxPercentage" readonly>
                <span id="taxPercentageSpan" style="color:#FF0000"></span>
            </div>
             
        </div>
     </div>
			  
          <div class="box-body">
           <div class="row">
                   
                <div class="col-md-3">
                  <label>Currency</label> <label class="text-red">* </label>
                  <select class="form-control" name="currencyName">
				  <option selected="" value="Default">-Select Currency-</option>
                    <c:forEach var="currencyList" items="${currencyList}" >
					<option value="${currencyList.currencyName}">${currencyList.currencyName}</option>
					 </c:forEach>
                  </select>
                    <span id="currencyNameSpan" style="color:#FF0000"></span>
                </div>
               
            <div class="col-md-3">
                <label for="wefDate">W.E.F Date</label> <label class="text-red">* </label>
                 <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="wefDate" name="wefDate">
                </div>
                <span id="wefDateSpan" style="color:#FF0000"></span>
            </div>
           </div>
          </div>
               
                  
               <input type="hidden" id="status" name="status" value="${Status}">	
				<input type="hidden" id="creationDate" name="creationDate" >
				<input type="hidden" id="updateDate" name="updateDate" >
				<input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>">     
	
          <div class="box-body">
              <div class="row">
             
                 <div class="col-xs-4">
                 <div class="col-xs-2">
                	<a href="TaxMaster"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
			     </div>
			     </div>
			     
				  <div class="col-xs-2">
                  <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
			     </div>
			     
				<div class="col-xs-3">
			    <button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
              
			    </div> 
			     
			 </div>
		</div>	
        
     </div>
  </div>
        
</div>
         
 </div>
   	
</section>
</form>
    <!-- /.content -->
</div>

  <!-- /.content-wrapper -->
  

  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script>
function CalcuateSGST()
{
	 var sgstPercentage = Number($('#sgstPercentage').val());
	 var cgstPercentage = Number($('#cgstPercentage').val());
	 var taxPercentage=sgstPercentage+cgstPercentage;
	 $('#taxPercentage').val(taxPercentage);
}

function clearall()
{
	$('#taxTypeSpan').html('');
	$('#taxNameSpan').html('');
	$('#sgstPercentageSpan').html('');
	$('#cgstPercentageSpan').html('');
	$('#taxPercentageSpan').html('');
	$('#currencyNameSpan').html('');
	$('#wefDateSpan').html('');
}
function validate()
{ 
	clearall();
	//validation for Tax Name
	
	if(document.taxform.taxType.value=="")
	{
		 $('#taxTypeSpan').html('Please, enter tax type..!');
		document.taxform.taxType.focus();
		return false;
	}
	else if(document.taxform.taxType.value.match(/^[\s]+$/))
	{
		 $('#taxTypeSpan').html('Please, enter tax type..!');
		document.taxform.taxType.value="";
		document.taxform.taxType.focus();
		return false;	
	}

	if(document.taxform.taxName.value=="")
	{
		 $('#taxNameSpan').html('Please, enter tax name..!');
		document.taxform.taxName.focus();
		return false;
	}
	else if(document.taxform.taxName.value.match(/^[\s]+$/))
	{
		 $('#taxNameSpan').html('Please, enter tax name..!');
		document.taxform.taxName.value="";
		document.taxform.taxName.focus();
		return false;	
	}
	
	//validation for Tax Percentage
		if(document.taxform.cgstPercentage.value=="")
		{
			 $('#cgstPercentageSpan').html('Please, enter CGST tax percentage..!');
			document.taxform.cgstPercentage.focus();
			return false;
		}
		else if(document.taxform.cgstPercentage.value.match(/^[\s]+$/))
		{
			 $('#cgstPercentageSpan').html('Please, enter CGST tax percentage..!');
			document.taxform.cgstPercentage.value="";
			document.taxform.cgstPercentage.focus();
			return false;	
		}
		else if(!document.taxform.cgstPercentage.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			 if(!document.taxform.cgstPercentage.value.match(/^[0-9]+$/))
				 {
					$('#cgstPercentageSpan').html('Please, use only digit value for CGST tax percentage..!');
					document.taxform.cgstPercentage.value="";
					document.taxform.cgstPercentage.focus();
					return false;
				}
		}
	 
	 
	//validation for Tax Percentage
	if(document.taxform.sgstPercentage.value=="")
	{
		 $('#sgstPercentageSpan').html('Please, enter SGST tax percentage..!');
		document.taxform.sgstPercentage.focus();
		return false;
	}
	else if(document.taxform.sgstPercentage.value.match(/^[\s]+$/))
	{
		 $('#sgstPercentageSpan').html('Please, enter SGST tax percentage..!');
		document.taxform.sgstPercentage.value="";
		document.taxform.sgstPercentage.focus();
		return false;	
	}
	else if(!document.taxform.sgstPercentage.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
	{
		 if(!document.taxform.sgstPercentage.value.match(/^[0-9]+$/))
			 {
				$('#sgstPercentageSpan').html('Please, use only digit value for SGST tax percentage..!');
				document.taxform.sgstPercentage.value="";
				document.taxform.sgstPercentage.focus();
				return false;
			}
	}

	
	//validation for Tax currency
	if(document.taxform.currencyName.value=="Default")
	{
		 $('#currencyNameSpan').html('Please Select Currency');
		document.taxform.currencyName.focus();
		return false;
	}
	
	if(document.taxform.wefDate.value=="")
	{
		 $('#wefDateSpan').html('Please, select tax W.E.F date');
		document.taxform.wefDate.focus();
		return false;
	}
}
function init()
{
	clearall();
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	
	 if(document.taxform.status.value=="Fail")
	 {
	  //	alert("Sorry, record is present already..!");
	 }
	 else if(document.taxform.status.value=="Success")
	 {
		 $('#statusSpan').html('Record added successfully..!');
	 }
  document.taxform.taxType.focus();
}
$(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#wefDate').datepicker({
      autoclose: true
    })
	
	 

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>

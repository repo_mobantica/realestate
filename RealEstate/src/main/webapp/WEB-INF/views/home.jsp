<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onload="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null || session.getAttribute("designationName") == null || session.getAttribute("projectList") == null || session.getAttribute("userName") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

   <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
    <%@ include file="menu.jsp" %>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>



<form name="homePage" >


    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>${totalenquiry}</h3>

              <p>Today's Enquirys</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="EnquiryMaster" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>${totalbooking}</h3>

              <p>Today's Booking</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
        <a href="BookingMaster" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>${totalsitevisited}</h3>

              <p>Today's Site Visited</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
               <h3>${totalCancel}</h3>

              <p>Today's Booking Cancel</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
           <a href="AllBookingCancelList" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
     </div>
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-7 connectedSortable">
          <!-- Custom tabs (Charts with tabs)-->
          
         <div class="box box-success">
          <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs pull-right">
              <li class="pull-left header"><i class="fa fa-inbox"></i><font size="5"> Sales</font></li>
            </ul>
            <div class="tab-content no-padding">

   				 <div id="chart_div" style="width: 100%; height: 100%;"></div>
                  
            </div>
          </div>
		</div>
			
          <div class="box box-success">
            <div class="box-header">
              <i class="ion ion-clipboard"></i>

              <h3 class="box-title" ><font size="5">To Do Task</font></h3>

            </div>
            
            <div class="box-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="flatListTable">
                  <thead>
                  <tr bgcolor=#4682B4>
                        <th style="width:10px">#</th>
                     	<th>To</th>
                        <th>Project</th>
	                    <th>Building</th>
	                    <th>Wing</th>
	                    <th>Flat</th>
	                    <th>Task Name</th>
	                    <th>From</th>
                    	<th>Status</th>
                  </tr>
                  </thead>
                  
                  <tbody>
                   <c:forEach items="${taskDetails}" var="taskDetails" varStatus="loopStatus">
                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                            <td>${loopStatus.index+1}</td>
                            <td>${taskDetails.assignTo}</td>
	                        <td>${taskDetails.projectName}</td>
	                        <td>${taskDetails.buildingName}</td>
	                        <td>${taskDetails.wingName}</td>
	                        <td>${taskDetails.flatNumber}</td>
	                        <td>${taskDetails.taskName}</td>
	                        <td>${taskDetails.assignFrom}</td>
	                        <td>${taskDetails.taskStatus}</td>
                       </tr>
					</c:forEach>
					
                 </tbody>
                </table>
              </div>
            </div>
            
            <!-- /.box-body -->
            <div class="box-footer clearfix no-border">
           <a href="TaskMaster"> <button type="button" class="btn btn-default pull-right"><i class="fa fa-plus"></i> Add Task</button></a>
            </div>
          </div> 
          
          
          	  
     
 <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title"><font size="5">Latest Project</font></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            
            <!-- /.box-header -->
            
            <div class="box box-default">
        	 <div class="box-body">
              <div class="table-responsive">
                <table class="table table-bordered" >
                  <thead>
                  <tr bgcolor="#4682B4">
                   <th style="width:80px"> Project Id </th>
                    <th>Project Name</th>
                    <th>Area Name</th>
                    <th>City Name</th>
                    <th>State Name</th>
                    <th>No. of Bulidings</th>
                    <th>Project Land Area</th>
                  </tr>
                  </thead>
                  <tbody>
                   <c:forEach items="${projectList}" var="projectList" varStatus="loopStatus">
                   <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
	                        <td>${projectList.projectId}</td>
	                        <td>${projectList.projectName}</td>
	                        <td>${projectList.locationareaId}</td>
	                        <td>${projectList.cityId}</td>
	                        <td>${projectList.stateId}</td>
	                        <td>${projectList.numberofBuildings}</td>
	                        <td>${projectList.projectlandArea}</td>
	                    </tr>
	                  </c:forEach>
                </tbody>
                </table>
              </div>
            </div>
		</div>
     
		</div>  
		  
          
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-5 connectedSortable">

          <!-- Map box -->
              
         <div class="box box-success">
          <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs pull-right">
              <li class="pull-left header"><i class="fa fa-inbox"></i><font size="5">Total Sales</font></li>
            </ul>
            <div class="tab-content no-padding">

               <div id="piechart" style="width: 100%; height: 100%;"></div>
                  
            </div>
          </div>
		</div>
		
          
          <!-- <div class="box box-solid bg-green-gradient">
            <div class="box-header">
              <i class="fa fa-calendar"></i>

              <h3 class="box-title">Calendar</h3>
              tools box
              <div class="pull-right box-tools">
                button with a dropdown
                <div class="btn-group">
                  <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-bars"></i></button>
                  <ul class="dropdown-menu pull-right" role="menu">
                    <li><a href="#">Add new event</a></li>
                    <li><a href="#">Clear events</a></li>
                    <li class="divider"></li>
                    <li><a href="#">View calendar</a></li>
                  </ul>
                </div>
                <button type="button" class="btn btn-success btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-success btn-sm" data-widget="remove"><i class="fa fa-times"></i>
                </button>
              </div>
              /. tools
            </div>
            /.box-header
            <div class="box-body no-padding">
              The calendar
              <div id="calendar" style="width: 100%"></div>
            </div>
            /.box-body
        
          </div> -->
		  
		  
	 <div class="box box-info">
            <div class="box-header">
              <i class="fa fa-envelope"></i>

              <h3 class="box-title">Quick Email</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip"
                        title="Remove">
                  <i class="fa fa-times"></i></button>
              </div>
              <!-- /. tools -->
            </div>
            <div class="box-body">
                <div class="form-group">
                  <input type="email" class="form-control" id="emailto" name="emailto" placeholder="Email to:">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" id="mailSubject" name="mailSubject" placeholder="Subject">
                </div>
                <div>
                  <textarea class="textarea" placeholder="Message"
                            style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" id="mailBody" name="mailBody"></textarea>
                </div>
            </div>
            <div class="box-footer clearfix">
              <button type="button" class="pull-right btn btn-default" id="sendEmail"  onclick="return SendMail()" >Send
                <i class="fa fa-arrow-circle-right"></i></button>
            </div>
          </div>
	
		  
        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    </form>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  

  <!-- Control Sidebar -->
     <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<script>
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawVisualization);
google.charts.setOnLoadCallback(drawChart);

function drawChart() {


	 $.ajax({

		 url : '${pageContext.request.contextPath}/getProjectGraghDetails',
		type : 'Post',
		data : { },
		dataType : 'json',
		success : function(result)
				  {
							  
						 for(var i=0;i<result.length;i++)
						 {

							  var myArr = new Array();

							  myArr[1] = new Array("Booking", result[i].totalFlatBooking);
							  myArr[2] = new Array("Agreement", result[i].totalFlatAgreement);
							  myArr[3] = new Array("Remaining", result[i].totalFlatRemaining);
							  
							  var data = google.visualization.arrayToDataTable([
							    ['Details', 'Flat'],
							    myArr[1],
							    myArr[2],
							    myArr[3]
							  ]);
							  
							  var options = {
							    title: ' '
							  };

							  var chart = new google.visualization.PieChart(document.getElementById('piechart'));
							  chart.draw(data, options);
							 
						 }
									  
				  } 

		});
	 
}


function drawVisualization() {
  // Some raw data (not necessarily accurate)

	 $.ajax({

		 url : '${pageContext.request.contextPath}/getGraghDetails',
		type : 'Post',
		data : { },
		dataType : 'json',
		success : function(result)
				  {

						  var myArr = new Array();

						  myArr[0] = new Array('Week Days');
						  myArr[1] = new Array("Sunday");
						  myArr[2] = new Array("Monday");
						  myArr[3] = new Array("Tuesday");
						  myArr[4] = new Array("Wednesday");
						  myArr[5] = new Array("Thursday");
						  myArr[6] = new Array("Friday");
						  myArr[7] = new Array("Saturday");
							  
						 for(var k=0;k<result.length;k++)
						 {
							 myArr[0].push(result[k].projectName);
							 myArr[1].push(result[k].bookingSaleInSunday);
							 myArr[2].push(result[k].bookingSaleInMonday);
							 myArr[3].push(result[k].bookingSaleInTuesday);
							 myArr[4].push(result[k].bookingSaleInWednesday);
							 myArr[5].push(result[k].bookingSaleInThursday);
							 myArr[6].push(result[k].bookingSaleInFriday);
							 myArr[7].push(result[k].bookingSaleInSaturday);
							 
						 }
							 
					  var data = google.visualization.arrayToDataTable([
					    myArr[0],
					    myArr[1],
					    myArr[2],
					    myArr[3],
					    myArr[4],
					    myArr[5],
					    myArr[6],
					    myArr[7],
					  ]);
		
					  var options = {
					    vAxis: {title: 'No. Of Flats'},
					    hAxis: {title: 'Week Days'},
					    seriesType: 'bars',
					    series: {5: {type: 'line'}}
					  };
		
					  var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
					  chart.draw(data, options);
					  
				  } 

		});
	 
}


	function SendMail()
	{

		if(document.homePage.emailto.value=="")
		{
			document.homePage.emailto.focus();
			return false;
		}
		else if(document.homePage.emailto.value.match(/^[\s]+$/))
		{
			document.homePage.emailto.value="";
			document.homePage.emailto.focus();
			return false; 	
		}


		if(document.homePage.mailSubject.value=="")
		{
			document.homePage.mailSubject.focus();
			return false;
		}
		else if(document.homePage.mailSubject.value.match(/^[\s]+$/))
		{
			document.homePage.mailSubject.value="";
			document.homePage.mailSubject.focus();
			return false; 	
		}
	
		 var emailto = $('#emailto').val();
		 var mailSubject = $('#mailSubject').val();
		 var mailBody = $('#mailBody').val();
	

		 $.ajax({

			 url : '${pageContext.request.contextPath}/HomeMailSending',
			type : 'Post',
			data : { emailto : emailto, mailSubject : mailSubject, mailBody : mailBody},
			dataType : 'json',
			success : function(result)
					  {
						   
					  } 

			});
		 
		 
		 $('#emailto').val("");
		 $('#mailSubject').val("");
		  document.getElementById("mailBody").value = "";
		 //$('#mailBody').val(""); 
	}
	
	
</script>

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="${pageContext.request.contextPath}/resources/bower_components/raphael/raphael.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="${pageContext.request.contextPath}/resources/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="${pageContext.request.contextPath}/resources/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="${pageContext.request.contextPath}/resources/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- ChartJS -->
<script src="${pageContext.request.contextPath}/resources/bower_components/chart.js/Chart.js"></script>


</body>
</html>

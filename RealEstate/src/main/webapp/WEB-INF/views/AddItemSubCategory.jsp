<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Add Item Sub Category</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
    </script>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>


<div class="wrapper">

  <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
  <%@ include file="menu.jsp" %>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Item Sub Category:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Add Item Sub Category</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="itemform" action="${pageContext.request.contextPath}/AddItemSubCategory" onSubmit="return validate()" method="post">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
             <span id="statusSpan" style="color:#008000"></span>
           
				<div class="box-body">
              <div class="row">
                  <div class="col-xs-2">
                  <label >Sub Item Id</label>
                  <input type="text" class="form-control"  id="subItemCategoryId" placeholder="ID" name="subItemCategoryId" value="${subItemId}" readonly>
                </div>               
              </div>
            </div>
				
				
				<div class="box-body">
              <div class="row">
                
                <div class="col-xs-3">
				      <label>Main Item Category</label> <label class="text-red">* </label>
	                  <select class="form-control" name="itemMainCategoryName" id="itemMainCategoryName" onchange="getSubItemCategory(this.value)">
					  <option selected="" value="Default">-Select Main Category-</option>
	                   <c:forEach var="suppliertypeList" items="${suppliertypeList}" >
						<option value="${suppliertypeList.supplierType}">${suppliertypeList.supplierType}</option>
						 </c:forEach>
	                  </select>
			         <span id="itemMainCategoryNameSpan" style="color:#FF0000"></span>
			     </div> 
			     
			     <div class="col-xs-3">
				      <label>Item Category</label> <label class="text-red">* </label>
	                  <select class="form-control" name="itemCategoryName" id="itemCategoryName">
					      <option selected="" value="Default">-Select Sub Item Category-</option>
	                    <c:forEach var="itemcategoryList" items="${itemcategoryList}" >
						  <option value="${itemcategoryList.itemcategoryName}">${itemcategoryList.itemcategoryName}</option>
					    </c:forEach>
	                   </select>
				         <span id="itemCategoryNameSpan" style="color:#FF0000"></span>
			     </div>
			     
			     <div class="col-xs-3">
				   <label for="subItemCategoryName">Sub Item Category Name </label> <label class="text-red">* </label>
                   <input type="text" class="form-control" id="subItemCategoryName" name="subItemCategoryName" placeholder="Item Name" style="text-transform: capitalize;" >
                   <span id="subItemCategoryNameSpan" style="color:#FF0000"></span>
                 </div>
                 
               </div>
            </div>
			
                <input type="hidden" id="itemStatus" name="itemStatus" value="Active">	
         		<input type="hidden" id="status" name="status" value="${Status}">	
				<input type="hidden" id="creationDate" name="creationDate" >
				<input type="hidden" id="updateDate" name="updateDate" >
				<input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>">
				
            </div>
          
            
			
          </div>
			      <div class="box-body">
              <div class="row">
              </br>
                 
                 <div class="col-xs-3">
	                  <div class="col-xs-2">
	                	<a href="ItemSubCategoryMaster"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
				     </div>
			     </div>
				 
				 <div class="col-xs-2">
                	<button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
			     </div>
					
				 <div class="col-xs-2">
			  		<button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
              
			     </div> 
		        
		       
                 
              </div>
			  </div>
          <!-- /.row -->
        </div>   
      </div>
    </section>
	</form>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script>

/*
function getuniqeItemName()
{
	
	 var itemName = $('#itemName').val();
		$.ajax({

			url : '${pageContext.request.contextPath}/getallItemList',
			type : 'Post',
			data : { itemName : itemName},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{
								 
								for(var i=0;i<result.length;i++)
								{
									
										if(result[i].itemName==itemName)
											{
											 //document.stateform.stateName.value="";
											  document.itemform.itemName.focus();
											  $('#itemNameSpan').html('This item name is already exist..!');
											 $('#itemName').val("");
										 //return (false);
											}
										
									
								 } 
							} 
							else
							{
								alert("failure111");
								//$("#ajax_div").hide();
							}

						}
			});	
}
*/

function getSubItemCategory()
{
	$('#itemCategoryName').empty();
	
	var supplierType = $('#itemMainCategoryName').val();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getSubSupplierTypeList',
		type : 'Post',
		data : { supplierType : supplierType},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select Item Category-");
								$("#itemCategoryName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].subsupplierType).text(result[i].subsupplierType);
							    $("#itemCategoryName").append(option);
							} 
						} 
						else
						{
							alert("failure111");
						}

					}
		});	
	
}

function clearall()
{
	$('#itemMainCategoryNameSpan').html('');
	$('#itemCategoryNameSpan').html('');
	$('#subItemCategoryNameSpan').html('');
}

function validate()
{ 
	clearall();
	
	if(document.itemform.itemMainCategoryName.value=="Default")
	{
		$('#itemMainCategoryNameSpan').html('Please, Select Item Main Category Name');
		document.itemform.itemMainCategoryName.focus();
		return false;
	}
	
	if(document.itemform.itemCategoryName.value=="Default")
	{
		$('#itemCategoryNameSpan').html('Please, Select Item Category Name');
		document.itemform.itemCategoryName.focus();
		return false;
	}
	
	if(document.itemform.subItemCategoryName.value=="")
	{
		$('#subItemCategoryNameSpan').html('Please, enter sub item category');
		document.itemform.subItemCategoryName.focus();
		return false;
	}
	else if(document.itemform.subItemCategoryName.value.match(/^[\s]+$/))
	{
		$('#subItemCategoryNameSpan').html('Please, enter valid sub item category..!');
		document.itemform.subItemCategoryName.value="";
		document.itemform.subItemCategoryName.focus();
		return false; 	
	}
	else if(document.itemform.subItemCategoryName.value.match(/^[\s]+$/))
	{
		$('#subItemCategoryNameSpan').html('Please, enter sub item category..!');
		document.itemform.subItemCategoryName.value="";
		document.itemform.subItemCategoryName.focus();
		return false; 	
	}

}

function init()
{
	clearall();
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	
	
	 if(document.itemform.status.value=="Fail")
	 {
	  	alert("Sorry, record is present already..!");
	 }
	 else if(document.itemform.status.value=="Success")
	 {
		 $('#statusSpan').html('Record added successfully..!');
	 }
	 
	 document.itemform.itemMainCategoryName.focus();
}

</script>
</body>
</html>

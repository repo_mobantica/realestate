<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Contractor Work Payment</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


  <!-- Google Font -->
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#enquirydbStatusSpan').delay(1000).fadeOut();
      });
    </script>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

  <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Contractor Work Payment:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Account</a></li>
        <li class="active">Contractor Work Payment</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="contractorworkpaymentform" action="${pageContext.request.contextPath}/AddContractorWorkPayment" onSubmit="return validate()" method="post">
    <section class="content">

 
      <div class="box box-default">
      <div class="box box-danger"> </div>  
        
         <div class="box-body">
          <div class="row">
       <div class="col-md-12">
              <span id="statusSpan" style="color:#FF0000"></span>
			
			<div class="box-body">
              <div class="row">
              
                  <div class="col-xs-2">
                  <label >Work Order Id</label>
                  <input type="text" class="form-control" id="workOrderId" name="workOrderId" value="${contractorworkorderDetails[0].workOrderId}" readonly>
                   <span id="statusSpan" style="color:#FF0000"></span>
                </div>               
            
                  <div class="col-xs-2">
                  <label >Quotation Id</label>
                   <input type="hidden" class="form-control" id="workId" name="workId" value="${contractorworkorderDetails[0].workId}" readonly>
                  <input type="text" class="form-control" id="quotationId" name="quotationId" value="${contractorworkorderDetails[0].quotationId}" readonly>
                   <span id="statusSpan" style="color:#FF0000"></span>
                </div>               
            
              </div>
            </div>
          
            <div class="box-body">
              <div class="row">
              
                  <div class="col-xs-2">
			      <label>Project  </label> 
			      <input type="text" class="form-control" value="${projectName}" readonly>
				  <input type="hidden" class="form-control" id=projectId name="projectId" value="${contractorworkorderDetails[0].projectId}" readonly>
			        <span id="projectIdSpan" style="color:#FF0000"></span>
			     </div> 
			     
			      <div class="col-xs-2">
			      <label>Building </label> 
			      <input type="text" class="form-control" value="${buildingName}" readonly>
				   <input type="hidden" class="form-control" id=buildingId name="buildingId" value="${contractorworkorderDetails[0].buildingId}" readonly>
			     </div> 
			
                 <div class="col-xs-1">
			      <label>Wing </label> 
			      <input type="text" class="form-control" value="${wingName}" readonly>
				 	<input type="hidden" class="form-control" id=wingId name="wingId" value="${contractorworkorderDetails[0].wingId}" readonly>
                 </div> 
             
                 <div class="col-xs-2">
			      <label>Work/Contractor Type </label> 
			      <input type="text" class="form-control" value="${contractorType}" readonly>
                  <input type="hidden" class="form-control" id=contractortypeId name="contractortypeId" value="${contractorworkorderDetails[0].contractortypeId}" readonly>  
			     </div> 
			      
                  <div class="col-xs-3">
			      <label>Contractor Firm Name  </label> <label class="text-red">* </label>
                  <input type="hidden" class="form-control" id="contractorId" name="contractorId" value="${contractorworkorderDetails[0].contractorId}" readonly>
                   <input type="text" class="form-control" id="contractorfirmName" name="contractorfirmName"  value="${contractorworkorderDetails[0].contractorfirmName}" readonly>
			     </div> 
			     
			      <div class="col-xs-2">
			      <label>Total Amount </label> <label class="text-red">* </label>
                   <input type="text" class="form-control" id="totalAmount" name="totalAmount"  value="${contractorworkorderDetails[0].totalAmount}" readonly>
			     </div> 
			
			       
           </div>
            </div>
            
            <div class="box-body">
              <div class="row">
               
			     <div class="col-xs-2">
			       <label>Total No.of Male Labour</label> <label class="text-red">* </label>
                   <input type="text" class="form-control" id="noofMaleLabour" name="noofMaleLabour" placeholder="No.of Male Labour" value="${contractorworkorderDetails[0].noofMaleLabour}" readonly>
			       <span id="noofMaleLabourSpan" style="color:#FF0000"></span>
			     </div> 
			
			     <div class="col-xs-2">
			       <label>Total No.of Female Labour </label> <label class="text-red">* </label>
                   <input type="text" class="form-control" id="noofFemaleLabour" name="noofFemaleLabour"  placeholder="No.of Female Labour" value="${contractorworkorderDetails[0].noofFemaleLabour}" readonly>
			       <span id="noofFemaleLabourSpan" style="color:#FF0000"></span>
			     </div> 
			    
			     <div class="col-xs-2">
			       <label>No. Of Insured Labour</label> <label class="text-red">* </label>
                   <input type="text" class="form-control" id="noofInsuredLabour" name="noofInsuredLabour" placeholder="No of Insured Labour" value="${contractorworkorderDetails[0].noofInsuredLabour}" readonly>
			       <span id="noofInsuredLabourSpan" style="color:#FF0000"></span>
			     </div> 
			          
			     <div class="col-xs-2">
			      <label for="retentionPer">Retention %</label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="retentionPer" name="retentionPer" placeholder="Retention Per" value="${contractorworkorderDetails[0].retentionPer}" readonly>
                   <span id="retentionAmountSpan" style="color:#FF0000"></span> 
			     </div>
			         
			     <div class="col-xs-2">
			      <label for="retentionAmount">Retention Amount</label> 
                  <input type="text" class="form-control" id="retentionAmount" name="retentionAmount" placeholder="Retention Amount" value="${contractorworkorderDetails[0].retentionAmount}" readonly>
                   <span id="retentionAmountSpan" style="color:#FF0000"></span> 
			     </div>
			         
			     <div class="col-xs-2">
			      <label for="retentionAmount">Previous Paid Amount </label> 
                  <input type="text" class="form-control" id="retentionAmount" name="retentionAmount" placeholder="Retention Amount" value="${totalPaidAmount}" readonly>
                   <span id="retentionAmountSpan" style="color:#FF0000"></span> 
			     </div>
              </div>
            </div>
          
          <div class="box-body">
              <div class="row">
                    
                  <div class="col-xs-12">
		           <table class="table table-bordered" id="contractorWorkListTable">
	              <tr bgcolor=#4682B4 style="color: white;">
		              <td>Sub-Contractor Type</td>
		              <td>Work Type</td>
		              <td>Type</td>
		              <td>Unit</td>
		              <td>Rate</td>
		              <td>G.AMT</td>
	               </tr>

               	   <c:forEach items="${contractorworkhistoryList}" var="contractorworkhistoryList" varStatus="loopStatus">
                    <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                        <td>${contractorworkhistoryList.subcontractortypeId}</td>
                        <td>${contractorworkhistoryList.workType}</td>
                        <td>${contractorworkhistoryList.type}</td>
                        <td>${contractorworkhistoryList.unit}</td>
                        <td>${contractorworkhistoryList.workRate}</td>
                        <td>${contractorworkhistoryList.grandTotal}</td>
                      
                     </tr>
                    </c:forEach> 
              </table>
            </div>
			        
			  </div>
            </div> 
         
		</div>	
            
           
            
         <div class="col-xs-12">
         <div class="box-body">
            <div class="row">
              <h4><label> Add Payment</label></h4>
		       
		      <div class="col-xs-3">
				  <label>Work</label> <label class="text-red">*</label>
			       <select class="form-control" id="paymentDecription" name="paymentDecription">
					<option selected="selected" value="Default">-Select work-</option>
			        <c:forEach var="contractorpaymentscheduleList1" items="${contractorpaymentscheduleList1}">
				    <option value="${contractorpaymentscheduleList1.paymentDecription}">${contractorpaymentscheduleList1.paymentDecription}</option>
				    </c:forEach>
			        </select>
			       <span id="paymentDecriptionSpan" style="color:#FF0000"></span> 
				</div>
					    
	   		  <div class="col-xs-2">
			      <label for="billNumber">Bill No</label><label class="text-red">*</label>
				  <input type="text" class="form-control" id="billNumber" name="billNumber">
				  <span id="billNumberSpan" style="color:#FF0000"></span>
			   </div>
			    		    
	   		  <div class="col-xs-2">
			      <label for="billDate">Bill Date</label><label class="text-red">*</label>
				  <input type="text" class="form-control" id="billDate" name="billDate">
				  <span id="billDateSpan" style="color:#FF0000"></span>
			   </div>
			    		    
	   		  <div class="col-xs-2">
			      <label for="paymentAmount">Amount</label><label class="text-red">*</label>
				  <input type="text" class="form-control" id="paymentAmount" name="paymentAmount">
				  <span id="paymentAmountSpan" style="color:#FF0000"></span>
			   </div>
			    
			<div class="col-xs-3">
			      <label for="narration">Narration</label><label class="text-red">*</label>
				  <input type="text" class="form-control" id="narration" name="narration">
				  <span id="narrationSpan" style="color:#FF0000"></span>
			 </div>
			    	     
          </div>
	     </div> 
   
	 </div>
		   
          <div class="col-xs-12">
          <br/> <br/> 
		  <h4><label> Work & Payment Details :</label></h4>
                <table id="customerPaymentListTable" class="table table-bordered">
	              <tr bgcolor=#4682B4>
	                  <th style="width:90px">Sr. No</th>
		              <th>Work Details</th>
		              <th>Amount</th>
		              <th>GST Amount</th>
		              <th>Total Amount</th>
		              <th>TDS Amount</th>
		              <th>Grand Amount</th> 
		              <th>Work Status</th>
		              <th>Paid Amount</th>
		             <!--  <th style="width:150px">Change Status</th> -->
	               </tr>
     	         <c:forEach items="${contractorpaymentscheduleList1}" var="contractorpaymentscheduleList1" varStatus="loopStatus">
                    <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                        <td>${loopStatus.index+1}</td>
                        <td>${contractorpaymentscheduleList1.paymentDecription}</td>
                        <td>${contractorpaymentscheduleList1.paymentAmount}</td>
                        <td>${contractorpaymentscheduleList1.gstAmount}</td>
                        <td>${contractorpaymentscheduleList1.paymentTotalAmount}</td>
                        <td>${contractorpaymentscheduleList1.tdsAmount}</td>
                        <td>${contractorpaymentscheduleList1.grandAmount}</td> 
                        <td>${contractorpaymentscheduleList1.status} </td>
                        <td>${contractorpaymentscheduleList1.paidPaymentAmount} </td>
                   <%--     <td> 
                        <a onclick="ToCompleteSaveStatus(${contractorpaymentscheduleList1.contractorrschedulerId})" class="btn btn-primary btn-sm"" data-toggle="tooltip" title="Complete"><i class="glyphicon glyphicon-edit"></i></a>
                        -|<a onclick="ToInCompleteSaveStatus(${contractorpaymentscheduleList1.contractorrschedulerId})" class="btn btn-success btn-sm"" data-toggle="tooltip" title="Incompleted"><i class="glyphicon glyphicon-edit"></i></a>
                        </td> --%>
                     </tr>
                    </c:forEach>
                </table>
		  </div>              
            
		      
	      <div class="box-body">
            <div class="row">
             <br/><br/>
               <div class="col-xs-1">
               </div>
		        <div class="col-xs-4">
		            <div class="col-xs-2">	
		            <a href="ContractorPaymentBill"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
					</div> 
				</div>
					    
	   		    <div class="col-xs-2">
	                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
			    </div>
			    
				<div class="col-xs-3">
		  			<button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
			    </div>
			    	     
          </div>
	     </div> 

   </div>
  </div>   
          
</div>
</section>
</form>
</div>

   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>

<script>
function clearall()
{
	$('#paymentAmountSpan').html('');
	$('#paymentDecriptionSpan').html('');
	$('#narrationSpan').html('');
	
}

function validate()
{
	clearall();


	if(document.contractorworkpaymentform.paymentDecription.value=="Default")
	{
		$('#paymentDecriptionSpan').html('Please, select work..!');
		document.contractorworkpaymentform.paymentDecription.focus();
		return false;
	}
	
	if(document.contractorworkpaymentform.billNumber.value=="")
	{
		 $('#billNumberSpan').html('Please, enter Bill Number..!');
		document.contractorworkpaymentform.billNumber.focus();
		return false;
	}
	else if(document.contractorworkpaymentform.billNumber.value.match(/^[\s]+$/))
	{
		$('#billNumberSpan').html('Please, enter valid Bill number..!');
		document.contractorworkpaymentform.billNumber.value="";
		document.contractorworkpaymentform.billNumber.focus();
		return false; 	
	}
	if(document.contractorworkpaymentform.billDate.value=="")
	{
		 $('#billDateSpan').html('Please, enter Bill Date..!');
		document.contractorworkpaymentform.billDate.focus();
		return false;
	}
	else if(document.contractorworkpaymentform.billDate.value.match(/^[\s]+$/))
	{
		$('#billDateSpan').html('Please, enter valid Bill datet..!');
		document.contractorworkpaymentform.billDate.value="";
		document.contractorworkpaymentform.billDate.focus();
		return false; 	
	}
	
	if(document.contractorworkpaymentform.paymentAmount.value=="")
	{
		 $('#paymentAmountSpan').html('Please, enter payment amount..!');
		document.contractorworkpaymentform.paymentAmount.focus();
		return false;
	}
	else if(document.contractorworkpaymentform.paymentAmount.value.match(/^[\s]+$/))
	{
		$('#paymentAmountSpan').html('Please, enter valid payment amount..!');
		document.contractorworkpaymentform.paymentAmount.value="";
		document.contractorworkpaymentform.paymentAmount.focus();
		return false; 	
	}
	else if(!document.contractorworkpaymentform.paymentAmount.value.match(/^[0-9]+$/))
	{
		$('#paymentAmountSpan').html('Please, enter valid payment amount..!');
		//document.contractorworkpaymentform.employeeBankacno.value="";
		document.contractorworkpaymentform.paymentAmount.focus();
		return false;
	}

	if(document.contractorworkpaymentform.narration.value=="")
	{
		 $('#narrationSpan').html('Please, enter narration..!');
		document.contractorworkpaymentform.narration.focus();
		return false;
	}
	else if(document.contractorworkpaymentform.narration.value.match(/^[\s]+$/))
	{
		$('#narrationSpan').html('Please, enter valid narration..!');
		document.contractorworkpaymentform.narration.value="";
		document.contractorworkpaymentform.narration.focus();
		return false; 	
	}
	
	
	
	
}


</script>
</body>
</html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | View Contractor Work Orders</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


  <!-- Google Font -->
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#enquirydbStatusSpan').delay(1000).fadeOut();
      });
    </script>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

  <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        View Contractor Work Order Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Contractor</a></li>
        <li class="active">View Contractor Work Order</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="contractorworkorderform" action="${pageContext.request.contextPath}/ViewContractorWorkOrderPaymentScheduler" onSubmit="return validate()" method="post">
    <section class="content">

 
      <div class="box box-default">
      <div class="box box-danger"> </div>  
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <span id="statusSpan" style="color:#FF0000"></span>
			
			<div class="box-body">
              <div class="row">
              
                  <div class="col-xs-2">
                  <label >Work Order Id</label>
                  <input type="text" class="form-control" id="workOrderId" name="workOrderId" value="${contractorworkorderDetails[0].workOrderId}" readonly>
                   <span id="statusSpan" style="color:#FF0000"></span>
                </div>               
            
                  <div class="col-xs-2">
                  <label >Quotation Id</label>
                   <input type="hidden" class="form-control" id="workId" name="workId" value="${contractorworkorderDetails[0].workId}" readonly>
                  <input type="text" class="form-control" id="quotationId" name="quotationId" value="${contractorworkorderDetails[0].quotationId}" readonly>
                   <span id="statusSpan" style="color:#FF0000"></span>
                </div>               
            
              </div>
            </div>
          
            <div class="box-body">
              <div class="row">
              
                  <div class="col-xs-2">
			      <label>Project  </label> 
			        <input type="text" class="form-control" value="${projectName}" readonly>
				  <input type="hidden" class="form-control" id=projectId name="projectId" value="${contractorworkorderDetails[0].projectId}" readonly>
			        <span id="projectIdSpan" style="color:#FF0000"></span>
			     </div> 
			     
			      <div class="col-xs-2">
			      <label>Building </label> 
			       <input type="text" class="form-control" value="${buildingName}" readonly>
				   <input type="hidden" class="form-control" id=buildingId name="buildingId" value="${contractorworkorderDetails[0].buildingId}" readonly>
			     </div> 
			
                 <div class="col-xs-1">
			      <label>Wing </label>
			       <input type="text" class="form-control" value="${wingName}" readonly> 
				 	<input type="hidden" class="form-control" id=wingId name="wingId" value="${contractorworkorderDetails[0].wingId}" readonly>
                 </div> 
             
                 <div class="col-xs-2">
			      <label>Work/Contractor Type </label> 
			       <input type="text" class="form-control" value="${contractorType}" readonly>
                  <input type="hidden" class="form-control" id=contractortypeId name="contractortypeId" value="${contractorworkorderDetails[0].contractortypeId}" readonly>  
			     </div> 
			       
                  <div class="col-xs-3">
			      <label>Contractor Firm Name  </label> <label class="text-red">* </label>
                  <input type="hidden" class="form-control" id="contractorId" name="contractorId" value="${contractorworkorderDetails[0].contractorId}" readonly>
                   <input type="text" class="form-control" id="contractorfirmName" name="contractorfirmName"  value="${contractorworkorderDetails[0].contractorfirmName}" readonly>
			     </div> 
			              
                  <div class="col-xs-2">
			      <label>Total Amount </label> <label class="text-red">* </label>
                   <input type="text" class="form-control" id="totalAmount" name="totalAmount"  value="${contractorworkorderDetails[0].totalAmount}" readonly>
			     </div> 
           </div>
            </div>
            
            <div class="box-body">
              <div class="row">
                 
		
			     <div class="col-xs-2">
			       <label>Total No.of Male Labour</label> <label class="text-red">* </label>
                   <input type="text" class="form-control" id="noofMaleLabour" name="noofMaleLabour" placeholder="No.of Male Labour" value="${contractorworkorderDetails[0].noofMaleLabour}" readonly>
			       <span id="noofMaleLabourSpan" style="color:#FF0000"></span>
			     </div> 
			
			     <div class="col-xs-2">
			       <label>Total No.of Female Labour </label> <label class="text-red">* </label>
                   <input type="text" class="form-control" id="noofFemaleLabour" name="noofFemaleLabour"  placeholder="No.of Female Labour" value="${contractorworkorderDetails[0].noofFemaleLabour}" readonly>
			       <span id="noofFemaleLabourSpan" style="color:#FF0000"></span>
			     </div> 
			    
			     <div class="col-xs-2">
			       <label>No. Of Insured Labour</label> <label class="text-red">* </label>
                   <input type="text" class="form-control" id="noofInsuredLabour" name="noofInsuredLabour" placeholder="No of Insured Labour" value="${contractorworkorderDetails[0].noofInsuredLabour}" readonly>
			       <span id="noofInsuredLabourSpan" style="color:#FF0000"></span>
			     </div> 
			   
			     <div class="col-xs-2">
			      <label for="retentionPer">Retention %</label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="retentionPer" name="retentionPer" placeholder="Retention Per" value="${contractorworkorderDetails[0].retentionPer}"  readonly>
                   <span id="retentionAmountSpan" style="color:#FF0000"></span> 
			     </div>
			         
			     <div class="col-xs-2">
			      <label for="retentionAmount">Retention Amount</label> 
                  <input type="text" class="form-control" id="retentionAmount" name="retentionAmount" placeholder="Retention Amount" value="${contractorworkorderDetails[0].retentionAmount}"readonly>
                   <span id="retentionAmountSpan" style="color:#FF0000"></span> 
			     </div>
			     
              </div>
            </div>
     
            <div class="box-body">
              <div class="row">
                    
                  <div class="col-xs-12">
		           <table class="table table-bordered" id="contractorWorkListTable">
	              <tr bgcolor=#4682B4 style="color: white;">
		              <td>Sub-Contractor Type</td>
		              <td>Work Type</td>
		              <td>Type</td>
		              <td>Unit</td>
		              <td>Rate</td>
		              <td>G.AMT</td>
	               </tr>

               	   <c:forEach items="${contractorworkhistoryList}" var="contractorworkhistoryList" varStatus="loopStatus">
                    <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                        <td>${contractorworkhistoryList.subcontractortypeId}</td>
                        <td>${contractorworkhistoryList.workType}</td>
                        <td>${contractorworkhistoryList.type}</td>
                        <td>${contractorworkhistoryList.unit}</td>
                        <td>${contractorworkhistoryList.workRate}</td>
                        <td>${contractorworkhistoryList.grandTotal}</td>
                      
                     </tr>
                    </c:forEach> 
              </table>
            </div>
			        
			  </div>
            </div> 
         
       
           <div class="box-body">
              <div class="row">
              <div class="col-md-12">
		
			<div class="box-body">
              <div class="row">
             <div class="col-xs-1">
             </div>  
          <div class="col-xs-8">
		  <h4><label> Work & Payment Details :</label></h4>
                <table id="customerPaymentListTable" class="table table-bordered">
	              <tr bgcolor=#4682B4>
	              <th style="width:90px">Sr. No</th>
		              <th>Work Details</th>
		            <!--   <th>Amount</th>
		              <th>GST Amount</th>
		              <th>Total Amount</th>
		              <th>TDS Amount</th>
		              <th>Grand Amount</th> -->
		              <th>Work Status</th>
		              <th style="width:150px">Change Status</th>
	               </tr>
     	         <c:forEach items="${contractorpaymentscheduleList}" var="contractorpaymentscheduleList" varStatus="loopStatus">
                    <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                    <td>${loopStatus.index+1}</td>
                        <td>${contractorpaymentscheduleList.paymentDecription}</td>
                      <%--   <td>${contractorpaymentscheduleList.paymentAmount}</td>
                        <td>${contractorpaymentscheduleList.gstAmount}</td>
                        <td>${contractorpaymentscheduleList.paymentTotalAmount}</td>
                        <td>${contractorpaymentscheduleList.tdsAmount}</td>
                        <td>${contractorpaymentscheduleList.grandAmount}</td> --%>
                        <td>${contractorpaymentscheduleList.status} </td>
                        <td> 
                        <a onclick="ToCompleteSaveStatus(${contractorpaymentscheduleList.contractorrschedulerId})" class="btn btn-primary btn-sm"" data-toggle="tooltip" title="Complete"><i class="glyphicon glyphicon-edit"></i></a>
                        -|<a onclick="ToInCompleteSaveStatus(${contractorpaymentscheduleList.contractorrschedulerId})" class="btn btn-success btn-sm"" data-toggle="tooltip" title="Incompleted"><i class="glyphicon glyphicon-edit"></i></a>
                        </td>
                     </tr>
                    </c:forEach>
                </table>
		  </div>              
            
        </div>
      </div>
		
		</div>
              
			  </div>
            </div>      
       
		</div>	
		
      </div>
      
		   	 <div class="box-body">
              <div class="row">
              <br/><br/>
              <div class="col-xs-1">
              </div>
                 <div class="col-xs-4">
                 <div class="col-xs-2">	<a href="ContractorPaymentScheduler"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
			     </div> 
			     </div>
			     
              </div>
			  </div>
          <!-- /.row -->
        </div>
        
      </div>
    </section>
	</form>
  </div>

   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>

 <script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
</script>
 
<script>
function ToInCompleteSaveStatus(contractorrschedulerId)
{
	var workOrderId = $('#workOrderId').val();
    var status="Incompleted";
   $('#customerPaymentListTable tr').detach();
   
   $.ajax({

		 url : '${pageContext.request.contextPath}/TochangesContrctorPaymentStatus',
		type : 'Post',
		data : { contractorrschedulerId : contractorrschedulerId, workOrderId : workOrderId, status : status },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
							$('#customerPaymentListTable').append('<tr style="background-color: #4682B4;"><th style="width:90px">Sr.No</th> <th>Work Details</th> <th>Work Status</th> <th style="width:150px">Change Status</th>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].contractorrschedulerId;
									$('#customerPaymentListTable').append('<tr style="background-color: #F0F8FF;"><td>'+(i+1)+'</td><td>'+result[i].paymentDecription+'</td><td>'+result[i].status+'</td><td><a onclick="ToCompleteSaveStatus('+id+')" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Completed"><i class="glyphicon glyphicon-edit"></i></a>-|<a onclick="ToInCompleteSaveStatus('+id+')" class="btn btn-success btn-sm" data-toggle="tooltip" title="Incompleted"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
								else
								{
									var id = result[i].contractorrschedulerId;
									$('#customerPaymentListTable').append('<tr style="background-color: #CCE5FF;"><td>'+(i+1)+'</td><td>'+result[i].paymentDecription+'</td><td>'+result[i].status+'</td><td><a onclick="ToCompleteSaveStatus('+id+')" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Completed"><i class="glyphicon glyphicon-edit"></i></a>-|<a onclick="ToInCompleteSaveStatus('+id+')" class="btn btn-success btn-sm" data-toggle="tooltip" title="Incompleted"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
							 } 
							} 
							else
							{
								alert("failure111");
							}
				  } 

		});		
}
function ToCompleteSaveStatus(contractorrschedulerId)
{
	var workOrderId = $('#workOrderId').val();
     var status="Completed";
    $('#customerPaymentListTable tr').detach();
    
    $.ajax({

		 url : '${pageContext.request.contextPath}/TochangesContrctorPaymentStatus',
		type : 'Post',
		data : { contractorrschedulerId : contractorrschedulerId, workOrderId : workOrderId, status : status },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
							$('#customerPaymentListTable').append('<tr style="background-color: #4682B4;"><th style="width:90px">Sr.No</th> <th>Work Details</th> <th>Work Status</th> <th style="width:150px">Change Status</th>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].contractorrschedulerId;
									$('#customerPaymentListTable').append('<tr style="background-color: #F0F8FF;"><td>'+(i+1)+'</td><td>'+result[i].paymentDecription+'</td><td>'+result[i].status+'</td><td><a onclick="ToCompleteSaveStatus('+id+')" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Completed"><i class="glyphicon glyphicon-edit"></i></a>-|<a onclick="ToInCompleteSaveStatus('+id+')" class="btn btn-success btn-sm" data-toggle="tooltip" title="Incompleted"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
								else
								{
									var id = result[i].contractorrschedulerId;
									$('#customerPaymentListTable').append('<tr style="background-color: #CCE5FF;"><td>'+(i+1)+'</td><td>'+result[i].paymentDecription+'</td><td>'+result[i].status+'</td><td><a onclick="ToCompleteSaveStatus('+id+')" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Completed"><i class="glyphicon glyphicon-edit"></i></a>-|<a onclick="ToInCompleteSaveStatus('+id+')" class="btn btn-success btn-sm" data-toggle="tooltip" title="Incompleted"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
							 } 
							} 
							else
							{
								alert("failure111");
							}
				  } 

		});	
}

  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
     $('#workStartDate').datepicker({
      autoclose: true
    })

    $('#workEndDate').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Add Labour</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>

<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
    </script>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
    		
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>

<div class="wrapper">

  <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Labour Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Add Labour</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="labourform" action="${pageContext.request.contextPath}/AddLabour" method="post" onSubmit="return validate()" >
    <section class="content">

      <div class="box box-default">
      <div class="box-body">
          <div class="row">
            <div class="col-md-12">
             <span id="statusSpan" style="color:#008000"></span>
				
			<div class="box-body">
              <div class="row">
                  <div class="col-xs-2">
                  <label >Labour Id</label>
                  <input type="text" class="form-control"  id="labourId"name="labourId" value="${labourCode}" readonly>
                </div>               
                
              </div>
            </div>
			
			<div class="box-body">
              <div class="row">
              
                <div class="col-xs-2">
				<label for="labourfirstname">First Name</label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="labourfirstName" placeholder="First Name" name="labourfirstName" style="text-transform: capitalize;">
                 <span id="labourfirstNameSpan" style="color:#FF0000"></span>
                </div>
                
                <div class="col-xs-2">
				<label for="labourmiddlename">Middle Name </label>
                  <input type="text" class="form-control" id="labourmiddleName" placeholder="Middle Name" name="labourmiddleName" style="text-transform: capitalize;">
                  <span id="labourmiddleNameSpan" style="color:#FF0000"></span>
                </div>
                
                <div class="col-xs-2">
				<label for="labourlastname">Last Name </label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="labourlastName" placeholder="Last Name" name="labourlastName" style="text-transform: capitalize;">
                  <span id="labourlastNameSpan" style="color:#FF0000"></span>
                </div>
                      
		 	  <div class="col-xs-2">
			   <label for="radiobutton">Gender</label><label class="text-red">* </label></br>
                 <input type="radio" class="minimal" name="labourGender" id="labourGender" value="Male"> Male
				  &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
  				 <input type="radio" class="minimal" name="labourGender" id="labourGender" value="Female"> Female
                 <span id="labourGenderSpan" style="color:#FF0000"></span>
              </div>
               
              <div class="col-xs-2">
				<label for="labourAge">Age </label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="labourAge" placeholder="Age" name="labourAge">
                  <span id="labourAgeSpan" style="color:#FF0000"></span>
               </div>
                
                <div class="col-xs-2">
			     <label>Project Name </label> <label class="text-red">* </label>
                  <select class="form-control" name="projectId" id="projectId">
				  		<option selected="" value="Default">-Select Project-</option>
                   	 <c:forEach var="projectList" items="${projectList}">
                    	<option value="${projectList.projectId}">${projectList.projectName}</option>
				     </c:forEach>
                  </select>
                     <span id="projectIdSpan" style="color:#FF0000"></span>
			     </div>   
			      
              </div>
            </div>
			
		
			<div class="box-body">
              <div class="row">
                  <div class="col-xs-3">
			      <label for="labouraddress">Address </label> <label class="text-red">* </label>
                  <textarea class="form-control" rows="1" id="labourAddress" placeholder="Address" name ="labourAddress"></textarea>
			      <span id="labourAddressSpan" style="color:#FF0000"></span>
			      </div> 
			
                  <div class="col-xs-2">
                  <label>Country </label> <label class="text-red">* </label>
                  <select class="form-control" name="countryId" id="countryId" onchange="getStateList(this.value)">
				  		<option selected="" value="Default">Select Country-</option>
                        <c:forEach var="countryList" items="${countryList}">
	                    <option value="${countryList.countryId}">${countryList.countryName}</option>
	                    </c:forEach>
                   </select>
                  <span id="countryIdSpan" style="color:#FF0000"></span>
                </div>               
                <div class="col-xs-2">
				 <label>State </label> <label class="text-red">* </label>
                  <select class="form-control" name="stateId" id="stateId" onchange="getCityList(this.Value)">
				 		<option selected="" value="Default">Select State-</option>
                        <c:forEach var="stateList" items="${stateList}">
                        <option value="${stateList.stateId}">${stateList.stateId}</option>
				        </c:forEach>  
                  </select>
                 <span id="stateIdSpan" style="color:#FF0000"></span>
				</div> 
				
                 <div class="col-xs-2">
				  <label>City </label> <label class="text-red">* </label>
                   <select class="form-control" name ="cityId" id="cityId" onchange="getLocationAreaList(this.value)">
				  		<option selected="" value="Default">Select City-</option>
                        <c:forEach var="cityList" items="${cityList}">
                    	<option value="${cityList.cityId}">${cityList.cityId}</option>
				        </c:forEach>  
                   </select>
                  <span id="cityIdSpan" style="color:#FF0000"></span>
				  </div> 
				  
            
		
			<div class="col-xs-2">
			   <label>Area </label> <label class="text-red">* </label>
                <select class="form-control" name="locationareaId" id="locationareaId"onchange="getpinCode(this.value)" >
			 	  <option selected="" value="Default">Select Area-</option>
               	  <c:forEach var="locationareaList" items="${locationareaList}">
                  <option value="${locationareaList.locationareaId}">${locationareaList.locationareaId}</option>
			      </c:forEach>
               </select>
               <span id="locationareaIdSpan" style="color:#FF0000"></span>
			</div> 
           
           <div class="col-xs-1">
            <label for="pincode">Pin Code </label> <label class="text-red">* </label>
            <input type="text" class="form-control" id="areaPincode" placeholder="Pin Code" name="areaPincode" >
			<span id="areaPincodeSpan" style="color:#FF0000"></span>
		  </div> 
        		
    	</div>
     </div>
 
 	
			<div class="box-body">
              <div class="row">
              
				 <div class="col-xs-3">
			     <label>Personal Mobile No </label> <label class="text-red">* </label>
				  <div class="input-group">
                  <div class="input-group-addon">
                  <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask='"mask": "(+99) 9999999999"' data-mask name="labourMobileno" id="labourMobileno">
                  <span id="labourMobilenoSpan" style="color:#FF0000"></span>
                  </div>
				 </div> 
			     
				  <div class="col-xs-3">
			        <label for="adharcardno">Aadhar Card No</label>
                    <input type="text" class="form-control" id="labourAadharcardno" placeholder="Adhar Card No " name="labourAadharcardno" style="text-transform:uppercase" >
			        <span id="labourAadharcardnoSpan" style="color:#FF0000"></span>
			      </div> 
			     	   
			    <div class="col-xs-3">
			    <label for="joiningdate">Joining Date</label> <label class="text-red">* </label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                <input type="text" class="form-control pull-right" id="labourJoiningdate" name="labourJoiningdate" >
                <span id="labourJoiningdateSpan" style="color:#FF0000"></span>
                </div>
				</div> 
               
                <div class="col-xs-3">
			       <label for="labourBasicpay">Basic Pay/Day </label> <label class="text-red">* </label>
                   <input type="text" class="form-control" id="labourBasicpay" placeholder="Basic Pay" name="labourBasicpay">
                   <span id="labourBasicpaySpan" style="color:#FF0000"></span>
			    </div> 
			    
			     
          </div>
       </div>
       
	<div class="box-body">
       <div class="row">
       <br/>
           <div class="col-xs-1">
           </div>
           <div class="col-xs-4">
           <div class="col-xs-2">
           	<a href="LabourMaster"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
			</div>
			</div>
			<div class="col-xs-4">
             <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
			</div>
			<div class="col-xs-2">
			  <button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
			</div> 
        </div>
	</div>		
	
	<input type="hidden" id="status" name="status" value="Active">	
	<input type="hidden" id="employeeStatus" name="employeeStatus" value="${employeeStatus}">	
	<input type="hidden" id="creationDate" name="creationDate" value="">
	<input type="hidden" id="updateDate" name="updateDate" value="">
	<input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>">              
 
  
   </div>
  </div>		 
 </div>
</div>			 

</section>
</form>
</div>
  
<%@ include file="footer.jsp" %>
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->

<script>
function clearall()
{
	$('#labourfirstNameSpan').html('');
	$('#labourmiddleNameSpan').html('');
	$('#labourlastNameSpan').html('');
	$('#labourGenderSpan').html('');
	$('#labourAgeSpan').html('');
	$('#labourAddressSpan').html('');
	$('#countryIdSpan').html('');
	$('#stateIdSpan').html('');
	$('#cityIdSpan').html('');
	$('#locationareaIdSpan').html('');
	$('#areaPincodeSpan').html('');
	$('#labourMobilenoSpan').html('');
	$('#labourAadharcardnoSpan').html('');
	$('#labourJoiningdateSpan').html('');
	$('#labourBasicpaySpan').html('');
	$('#projectIdSpan').html('');
}

    function getpinCode()
    {

    	 $("#areaPincode").empty();
    	 var locationareaId = $('#locationareaId').val();
    	 var cityId = $('#cityId').val();
    	 var stateId = $('#stateId').val();
    	 var countryId = $('#countryId').val();
    	 $.ajax({

    		 url : '${pageContext.request.contextPath}/getallAreaList',
    		type : 'Post',
    		data : { locationareaId : locationareaId, cityId : cityId, stateId : stateId, countryId : countryId},
    		dataType : 'json',
    		success : function(result)
    				  {
    						if (result) 
    						{
    							
    							for(var i=0;i<result.length;i++)
    							{
    							$('#areaPincode').val(result[i].pinCode);
    								
    							 } 
    						
    						} 
    						else
    						{
    							alert("failure111");
    							//$("#ajax_div").hide();
    						}

    					}
    		});
    	
    }


 function validate()
 {
	     clearall();
	    
	    //validation for  first name
		if(document.labourform.labourfirstName.value=="")
		{
			$('#labourfirstNameSpan').html('First name should not be blank..!');
			document.labourform.labourfirstName.focus();
			return false;
		}
		else if(document.labourform.labourfirstName.value.match(/^[\s]+$/))
		{
			$('#labourfirstNameSpan').html('First name should not be blank..!');
			document.labourform.labourfirstName.value="";
			document.labourform.labourfirstName.focus();
			return false;
		}
	
/* 	    //validation for  middle name
	    if(document.labourform.labourmiddleName.value.length!=0)
	    {
	    	if(document.labourform.labourmiddleName.value.match(/^[\s]+$/))
	    	{
	    		document.labourform.labourmiddleName.value="";
	    	}
	    	else if(!document.labourform.labourmiddleName.value.match(/^[A-Za-z\s.]+$/))
			{
	    		$('#labourmiddleNameSpan').html('Middle name should contain alphabets only..!');
				document.labourform.labourmiddleName.value="";
				document.labourform.labourmiddleName.focus();
				return false;
			}
	    }
	     */
	    //validation for  last name
		if(document.labourform.labourlastName.value=="")
		{
			$('#labourlastNameSpan').html('Last name should not be blank..!');
			document.labourform.labourlastName.focus();
			return false;
		}
		else if(document.labourform.labourlastName.value.match(/^[\s]+$/))
		{
			$('#labourlastNameSpan').html('Last name should not be blank..!');
			document.labourform.labourlastName.value="";
			document.labourform.labourlastName.focus();
			return false;
		}

		//validation for gender selection
	    if(( document.labourform.labourGender[0].checked == false ) && ( document.labourform.labourGender[1].checked == false ) )
		{
	    	$('#labourGenderSpan').html('Please, choose emp. Gender: Male or Female..!');
			document.labourform.labourGender[0].focus();
			return false;
		}
		
		if(document.labourform.labourAge.value=="")
		{
			$('#labourAgeSpan').html('Please, enter age..!');
			document.labourform.labourAge.focus();
			return false;
		}
		else if(document.labourform.labourAge.value.match(/^[\s]+$/))
		{
			$('#labourAgeSpan').html('Please, enter age ...!');
			document.labourform.labourAge.focus();
			return false;
		}
		else if(!document.labourform.labourAge.value.match(/^[0-9]+$/))
		{
			$('#labourAgeSpan').html(' enter valid age..!');
			document.labourform.labourAge.value="";
			document.labourform.labourAge.focus();
			return false;	
		}
		
		if(document.labourform.projectId.value=="Default")
		{
			$('#projectIdSpan').html('Please, select Project name..!');
			document.labourform.projectId.focus();
			return false;
		} 
	    //validation for employee address----------------------
		if(document.labourform.labourAddress.value=="")
		{
			$('#labourAddressSpan').html('Please, enter address name..!');
			document.labourform.labourAddress.focus();
			return false;
		}
		else if(document.labourform.labourAddress.value.match(/^[\s]+$/))
		{
			$('#labourAddressSpan').html('Please, enter address ...!');
			document.labourform.labourAddress.focus();
			return false;
		}
		
	    
	    //validation for employee country
		if(document.labourform.countryId.value=="Default")
		{
			$('#countryIdSpan').html('Please, select employee country name..!');
			document.labourform.countryId.focus();
			return false;
		}
	    
	    //validation for employee state
		if(document.labourform.stateId.value=="Default")
		{
			$('#stateIdSpan').html('Please, select employee state name..!');
			document.labourform.stateId.focus();
			return false;
		}
	    
	    //validation for employee city
		if(document.labourform.cityId.value=="Default")
		{
			$('#cityIdSpan').html('Please, select employee city name..!');
			document.labourform.cityId.focus();
			return false;
		}
	    
	    //validation for employee location area
		if(document.labourform.locationareaId.value=="Default")
		{
			$('#locationareaIdSpan').html('Please, select employee area name..!');
			document.labourform.locationareaId.focus();
			return false;
		}
	    
		if(document.labourform.labourMobileno.value=="")
		{
			$('#labourMobilenoSpan').html('Please, enter mobile number..!');
			document.labourform.labourMobileno.value="";
			document.labourform.labourMobileno.focus();
			return false;
		}
		else if(!document.labourform.labourMobileno.value.match(/^[(]{1}[+]{1}[0-9]{2}[)]{1}[\s]{1}[0-9]{10}$/))
		{
			$('#labourMobilenoSpan').html(' enter valid employee mobile number..!');
			document.labourform.labourMobileno.value="";
			document.labourform.labourMobileno.focus();
			return false;	
		}
		
		   if(document.labourform.labourAadharcardno.value.length!=0)
			{
			 if(!document.labourform.labourAadharcardno.value.match(/^[0-9]+$/))
				{
					$('#labourAadharcardnoSpan').html(' enter valid Aadhar number..!');
					document.labourform.labourAadharcardno.value="";
					document.labourform.labourAadharcardno.focus();
					return false;	
				}
	 		}
			
		   
/* 		
		//validation for  aadhar card number
		if(document.labourform.labourAadharcardno.value=="")
		{
			$('#labourAadharcardnoSpan').html('Please, enter Aadhar card number..!');
			document.labourform.labourAadharcardno.focus();
			return false;
		}
		else if(!document.labourform.labourAadharcardno.value.match(/^\d{4}\d{4}\d{4}$/))
		{
			$('#labourAadharcardnoSpan').html('Aadhar card number should be 12 digit number only..!');
			document.labourform.labourAadharcardno.value="";
			document.labourform.labourAadharcardno.focus();
			return false;
		}
	    */

		//validation for joining date
		if(document.labourform.labourJoiningdate.value=="")
		{
			$('#labourJoiningdateSpan').html('Please, select joining date..!');
			document.labourform.labourJoiningdate.focus();
			return false;
		}
		
		
		//validation for  basic pay
		if(document.labourform.labourBasicpay.value=="")
		{
			$('#labourBasicpaySpan').html('Please, enter basic pay amount..!');
			document.labourform.labourBasicpay.focus();
			return false;
		}
		else if(!document.labourform.labourBasicpay.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			 if(!document.labourform.labourBasicpay.value.match(/^[0-9]+$/))
				 {
					$('#labourBasicpaySpan').html('Please, use only digit value for basic pay/per..! eg:21.36 OR 30');
					document.labourform.labourBasicpay.value="";
					document.labourform.labourBasicpay.focus();
					return false;
				}
		}
		
	
}
 
function init()
{
	clearall();
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	

	 
    document.labourform.labourfirstName.focus();
}


function getStateList()
{
	 $("#stateId").empty();
	 var countryId = $('#countryId').val();
	 
	$.ajax({

		url : '${pageContext.request.contextPath}/getStateList',
		type : 'Post',
		data : { countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select State-");
							$("#stateId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].stateId).text(result[i].stateName);
							    $("#stateId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get State List


function getCityList()
{
	 $("#cityId").empty();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();
	$.ajax({

		url : '${pageContext.request.contextPath}/getCityList',
		type : 'Post',
		data : { stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select City-");
							$("#cityId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].cityId).text(result[i].cityName);
							    $("#cityId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get City List

function getLocationAreaList()
{
	 $("#locationareaId").empty();
	 var cityId = $('#cityId').val();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getLocationAreaList',
		type : 'Post',
		data : { cityId : cityId, stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Location Area-");
							$("#locationareaId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].locationareaId).text(result[i].locationareaName);
							    $("#locationareaId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
	
}//end of get locationarea List


  $(function ()
  {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
     $('#labourJoiningdate').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>

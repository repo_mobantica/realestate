<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Payment Scheduler</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
    
 	<script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
    </script>

  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>

<div class="wrapper">

   <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Payment Scheduler Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Add State</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="paymentschedulerform" action="${pageContext.request.contextPath}/GeneratePaymentScheduler" onSubmit="return validate()" method="post">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
     
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
                <span id="statusSpan" style="color:#008000"></span>
              <!-- /.form-group -->
              <div class="box-body">
          		 <div class="row">
          		 
	                
	                <div class="col-md-3">
	                  <label for="bookingId">Booking Id</label>
	                  <input type="text" class="form-control" id="bookingId" name="bookingId"  value="${bookingList[0].bookingId}" readonly/>
	                </div>
	                
	                <div class="col-md-3">
	                  <label for="projectName">Project Name</label>
	                  <input type="text" class="form-control" id="projectName" name="projectName"  value="${projectName}" readonly/>
	                </div>
	                
	                <div class="col-md-3">
	                  <label for="buildingName">Building Name</label>
	                  <input type="text" class="form-control" id="buildingName" name="buildingName"  value="${buildingName}" readonly/>
	                </div>
	                	<div class="col-md-3">
	                  <label for="wingName">Wing Name</label>
	                  <input type="text" class="form-control" id="wingName" name="wingName"  value="${wingName}" readonly/>
	                </div>
				 </div>
			  </div>
			  
			  <div class="box-body">
          		 <div class="row">
          		 	
          		 	<div class="col-md-3">
	                  <label for="floortypeName">Floor Name</label>
	                  <input type="text" class="form-control" id="floortypeName" name=floortypeName  value="${floortypeName}" readonly/>
	                </div>
          		 
	            	<div class="col-md-3">
	                  <label for="flatType">Flat Type</label>
	                  <input type="text" class="form-control" id="flatType" name="flatType"  value="${bookingList[0].flatType}" readonly/>
	                </div>
	                
	                <div class="col-md-3">
	                  <label for="flatNumber">Flat No.</label>
	                  <input type="text" class="form-control" id="flatNumber" name="flatNumber" value="${flatNumber}" readonly/>
	                </div>
	                	<div class="col-md-3">
	                  <label for="flatbasicCost">Flat Cost</label>
	                  <input type="text" class="form-control" id="flatbasicCost" name="flatbasicCost" value="${bookingList[0].flatbasicCost}" readonly/>
	                </div>
				 </div>
			  </div>
								
			  <div class="box-body">
          		 <div class="row">

					<div class="col-md-3">
	                  <label for="aggreementAmount1">Agreement Value</label>
	                  <input type="text" class="form-control" id="aggreementAmount1" name="aggreementAmount1" value="${bookingList[0].aggreementValue1}" readonly/>
	                </div>
					
					<div class="col-md-3">
	                  <label for="loanAmount">Loan Amount</label>
	                  <input type="text" class="form-control" id="loanAmount" name="loanAmount" value="${customerLoanList[0].loanAmount}" readonly/>
	                </div>	
					      
					<div class="col-md-3">
	                  <label for="remainingAmount">Remaining Amount </label>
	                  <input type="text" class="form-control" id="remainingAmount" name="remainingAmount" value="${customerLoanList[0].remainingAmount}" readonly/>
	                </div>      
					
				 </div>
			  </div>
			
            </div>
            
            <!-- /.col -->
			
          </div>
		   	 
			<div class="box-body">
              <div class="row">
                 
             
                 
				  <div class="col-xs-3">
				  	<br/>
	                <a href="BookingMaster"><button type="button" class="btn btn-block btn-primary" value="back" style="width:90px">Back</button></a>
			      </div>
			     
				  <div class="col-xs-2">
				  	<br/>
			  		 <a target="_blank" href="CustomerPaymentSchedule?bookingId=${bookingList[0].bookingId}" ><button type="button" class="btn btn-info pull-right" name="submit">Print</button></a>
			      </div> 
			       
			  </div>
          <!-- /.row -->
           </div>
        <!-- /.box-body -->
        
         </div>
      <!-- /.box -->
	 </div>	
	 
	 	<div class="box" id="hidefirst">
          <div class="box box-danger">
                <div class="box-header with-border">
                  <h3 class="box-title" id="box_header">Payment Scheduler</h3>
                </div><!-- /.box-header -->
                <div class="box-body" style="height:500px;overflow:scroll;overflow-x:scroll;overflow-y:scroll;">
            
          
               	<table id="paymentSchedulerListTable" class="table table-bordered table-striped">
                    <thead>
                      <tr bgcolor="#4682B4">
	                    <th style="width:80px">Sr.No.</th>
	                    <th style="width:150px">Installments</th>
	                    <th style="width:300px">Payment Scheduler Description</th>
	                    <th style="width:50px">Per(%)</th>
                      </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${paymentscheduleList}" var="paymentscheduleList" varStatus="loopStatus">
                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                  			<td>${loopStatus.index+1}</td>
                  			<td>${paymentscheduleList.installmentNumber}</td>
                  			<td>${paymentscheduleList.paymentDecription}</td>
                  			<td>${paymentscheduleList.percentage}</td>
                  			
                  		</tr>
                  	</c:forEach>
                  </tbody>
                 </table>
                
                </div><!-- /.box-body -->
              </div>
          </div>
	 
       </section>
	</form>
 </div>
    <%@ include file="footer.jsp" %>
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>

<script>

$(function ()
{
				    //Datemask dd/mm/yyyy
				    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
				    //Datemask2 mm/dd/yyyy
				    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
				    //Money Euro
				    $('[data-mask]').inputmask()

				    //Date range picker
				    $('#reservation').daterangepicker()
				    //Date range picker with time picker
				    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
				    //Date range as a button
				    $('#daterange-btn').daterangepicker(
				      {
				        ranges   : {
				          'Today'       : [moment(), moment()],
				          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
				          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
				          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
				        },
				        startDate: moment().subtract(29, 'days'),
				        endDate  : moment()
				      },
				      function (start, end) {
				        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
				      }
				    )

				    //Date picker
				    $('#datepicker0').datepicker({
				      autoclose: true
				    })
				    
				    $('#datepicker1').datepicker({
				      autoclose: true
				    })
				    
				    $('#datepicker2').datepicker({
				      autoclose: true
				    })
				    
				    $('#datepicker3').datepicker({
				      autoclose: true
				    })
				    
				    $('#datepicker4').datepicker({
				      autoclose: true
				    })
				    
				    $('#datepicker5').datepicker({
				      autoclose: true
				    })
				    
				    $('#datepicker6').datepicker({
				      autoclose: true
				    })
				    
				    $('#datepicker7').datepicker({
				      autoclose: true
				    })
				    
				    $('#datepicker8').datepicker({
				      autoclose: true
				    })
				    
				    $('#datepicker9').datepicker({
				      autoclose: true
				    })
				    
				    $('#datepicker10').datepicker({
				      autoclose: true
				    })
				    
				    $('#datepicker11').datepicker({
				      autoclose: true
				    })
				    
				    $('#datepicker12').datepicker({
				      autoclose: true
				    })
				    
				    $('#datepicker13').datepicker({
				      autoclose: true
				    })
				    
				    $('#datepicker14').datepicker({
				      autoclose: true
				    })
				    
				    $('#datepicker15').datepicker({
				      autoclose: true
				    })
				    
				    $('#datepicker16').datepicker({
				      autoclose: true
				    })
				    
				    $('#datepicker17').datepicker({
				      autoclose: true
				    })
				    
				    $('#datepicker18').datepicker({
				      autoclose: true
				    })
				    
				    $('#datepicker19').datepicker({
				      autoclose: true
				    })
				    
				    $('#datepicker20').datepicker({
				      autoclose: true
				    })

				    //iCheck for checkbox and radio inputs
				    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
				      checkboxClass: 'icheckbox_minimal-blue',
				      radioClass   : 'iradio_minimal-blue'
				    })
				    //Red color scheme for iCheck
				    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
				      checkboxClass: 'icheckbox_minimal-red',
				      radioClass   : 'iradio_minimal-red'
				    })
				    //Flat red color scheme for iCheck
				    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
				      checkboxClass: 'icheckbox_flat-green',
				      radioClass   : 'iradio_flat-green'
				    })

				    //Colorpicker
				    $('.my-colorpicker1').colorpicker()
				    //color picker with addon
				    $('.my-colorpicker2').colorpicker()

				    //Timepicker
				    $('.timepicker').timepicker({
				      showInputs: false
				    })
})
		
  
  /*
  	 //Date picker
    $('#datepicker1').datepicker({
      autoclose: true
    })
    
    $('#datepicker2').datepicker({
      autoclose: true
    })
    
    $('#datepicker3').datepicker({
      autoclose: true
    })
    
    $('#datepicker4').datepicker({
      autoclose: true
    })
    
    $('#datepicker5').datepicker({
      autoclose: true
    })
    
    $('#datepicker6').datepicker({
      autoclose: true
    })
    
    $('#datepicker7').datepicker({
      autoclose: true
    })
    
    $('#datepicker8').datepicker({
      autoclose: true
    })
    
    $('#datepicker9').datepicker({
      autoclose: true
    })
    
    $('#datepicker10').datepicker({
      autoclose: true
    })
    
    $('#datepicker11').datepicker({
      autoclose: true
    })
    
    $('#datepicker12').datepicker({
      autoclose: true
    })
    
    $('#datepicker13').datepicker({
      autoclose: true
    })
    
    $('#datepicker14').datepicker({
      autoclose: true
    })
    
    $('#datepicker15').datepicker({
      autoclose: true
    })
    
    $('#datepicker16').datepicker({
      autoclose: true
    })
    
    $('#datepicker17').datepicker({
      autoclose: true
    })
    
    $('#datepicker18').datepicker({
      autoclose: true
    })
    
    $('#datepicker19').datepicker({
      autoclose: true
    })
    
    $('#datepicker20').datepicker({
      autoclose: true
    })
  */
  
</script>
</body>
</html>
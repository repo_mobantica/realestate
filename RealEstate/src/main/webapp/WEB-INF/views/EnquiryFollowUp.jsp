

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Add Enquiry Followup</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
     <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

   <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Enquiry Follow-up:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Enquiry Follow-up</li>
      </ol>
    </section>

    <!-- Main content -->
    
<form name="enquiryfollowupform" action="${pageContext.request.contextPath}/EnquiryFollowUp" method="post" onSubmit="return validate()">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
                <span id="statusSpan" style="color:#FF0000"></span>
              <!-- /.form-group -->
			
				<div class="box-body">
              <div class="row">
                  <div class="col-xs-2">
                  <label for="enquairyid">Enquiry Id</label>
                  <input type="text" class="form-control" id="enquiryId" placeholder="ID" name="enquiryId" value="${enquiryDetails[0].enquiryId}" readonly>
                </div>               
                <div class="col-xs-2">
                  <label>Enquiry Date </label>
                     <input type="text" class="form-control" id="enquirydate" value="${enquiryDetails[0].creationDate}" readonly>
                </div>  
              </div>
            </div>
				
				
			<div class="box-body">
              <div class="row">
                <div class="col-xs-6">
				<label for="enquairyfirstname">Full Name</label>
                  <input type="text" class="form-control" id="enquiryName" placeholder="Full Name" name="enquiryName" value="${enquiryDetails[0].enqfirstName}  ${enquiryDetails[0].enqmiddleName}  ${enquiryDetails[0].enqlastName}" readonly>
                </div>
                
              </div>
            </div>
	
				
				
	
				
			
		
			
			 <div class="box-body">
              <div class="row">
                      <div class="col-xs-3">
			    <label for="mobileno1">Mobile No(Primary)</label>
                    <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" id="enqmobileNumber1" name="enqmobileNumber1" value="${enquiryDetails[0].enqmobileNumber1}" readonly>
                </div>
			     </div> 
				 
				  <div class="col-xs-3">
			      <label for="officephoneno">Mobile Number</label>
				  <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" id="enqmobileNumber2" name="enqmobileNumber2" value="${enquiryDetails[0].enqmobileNumber2}" readonly>
                </div>
			     </div> 
				 <div class="col-xs-3">
                  <label>Email Id </label>
                    <input type="text" class="form-control" id="enqEmail" placeholder="" name="enqEmail" value="${enquiryDetails[0].enqEmail}" readonly>
			     </div> 
			
              </div>
            </div>
            
              <div class="box-body">
              <div class="row">
              	    <div class="col-xs-3">
			    <label>Project Name </label>
                     <input type="text" class="form-control" id="projectName" name="projectName" value="${enquiryDetails[0].projectName}" readonly>
			     </div> 
              	    <div class="col-xs-3">
			    <label>Flat Type  </label>
                     <input type="text" class="form-control" id="flatType" placeholder="Flat Type" name="flatType" value="${enquiryDetails[0].flatType}" readonly>
			     </div> 
                   <div class="col-xs-3">
				     <label for="flatRemark">Flat Remark </label>
	                 <textarea class="form-control" rows="1" id="flatRemark" placeholder="flatRemark" name="flatRemark"  value="${enquiryDetails[0].flatRemark}"></textarea>
				     <span id="flatRemarkSpan" style="color:#FF0000"></span>
			     </div> 
              </div>
              </div>
              
			<div class="box-body">
              <div class="row">
				  <div class="col-xs-3">
			    <label>Budget</label>
                <input type="text" class="form-control" id="flatBudget" placeholder="Budget" name="flatBudget" value="${enquiryDetails[0].flatBudget}" readonly>
			     </div> 
			  
				  <div class="col-xs-3">
			   	 <label for="preferbanks">Enquiry Source </label>
				   <input type="text" class="form-control" id="enquirysourceId" placeholder="Enquiry Source" name="enquirysourceId" value="${enquiryDetails[0].enquirysourceId}" readonly>
			     </div>
			          <div class="col-xs-3">
			   	 <label for="sl">Sub-Enquiry Source </label><label class="text-red">* </label>
			  <input type="text" class="form-control" id="subsourceId"  name="subsourceId" value="${enquiryDetails[0].subsourceId}" readonly>
                   <span id="subsourceIdSpan" style="color:#FF0000"></span>
			     </div>
			     </div>
			     </div>
			     
			     
			   <div class="box-body">
              <div class="row">
			  <div class="col-xs-3">
			    <label>Next Follow-up Date</label><label class="text-red">* </label>
              <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                   <input type="text" class="form-control pull-right" id="datepicker" name="followupDate">
               
                </div>
                  <span id="followupDateSpan" style="color:#FF0000"></span>
			     </div> 
			      <div class="col-xs-3">
			   <label for="enquairyremark">Remark </label><label class="text-red">* </label>
                 <textarea class="form-control" rows="1" id="enqRemark" placeholder="Remark" name="enqRemark"></textarea>
			     </div> 
				  <span id="enqRemarkSpan" style="color:#FF0000"></span> 
			     
			     <div class="col-xs-3">
			   <label for="status">Status </label><label class="text-red">* </label>
			   <select class="form-control" id="enqStatus" name="enqStatus">
				  <option selected="" value="Default">-Status-</option>
                  <option>In-Process</option>
                  <option>Site Visited 1</option>
                  <option>Site Visited 2</option>
                  <option>Site Visited 3</option>
                  <option>Site Visited 4</option>
                  <option>Site Visited 5</option>
                  <option>Site Visited 6</option>
                  <option>Site Visited 7</option>
                  <option>Site Visited 8</option>
                  <option>Site Visited 9</option>
                  <option>Booking</option>
                  <option>Cancel</option>
                  </select>
			     </div> 
				    <span id="enqStatusSpan" style="color:#FF0000"></span>
				    
				   <input type="hidden" id="followupTakenBy" name="followupTakenBy" value="">
			  <input type="hidden" id="updateDate" name="updateDate" value="">
			  <input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>" >  
              </div>
            </div>
	
		
	     
            </div>
          
      	
          </div>
          
		  	     <div class="box-body">
              <div class="row">
              </br>
                    <div class="col-xs-4">
                          <div class="col-xs-2">
                			<a href="TodayEnquiryFollowUp"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
                		</div>
			     </div>
				  <div class="col-xs-4">
                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
              
			     </div>
					<div class="col-xs-2">
			  <button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
              
			     </div> 
              </div>
			  </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        
        
        
          
          <div class="box box-default">
            <div class="box-header">
              <h3 class="box-title">Previus Follow-up</h3>
            </div>
        	 <div class="box-body">
              <div class="table-responsive">
                <table class="table table-bordered" >
                  <thead>
                  <tr bgcolor="#4682B4">
                    <th style="width:80px">Follow Up Date</th>
                    <th style="width:300px">Remark</th>
                    <th style="width:150px">Status</th>
                    <th style="width:150px">Follow-Up Taken Username</th>
                  </tr>
                  </thead>
                  <tbody>
                   <c:forEach items="${followupList}" var="followupList" varStatus="loopStatus">
	               <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
	               <td>${followupList.followupDate}</td>
	               <td>${followupList.enqRemark}</td>
	               <td>${followupList.enqStatus}</td>
	               <td>${followupList.userName}</td>
	               </tr>
	               </c:forEach>
                </tbody>
                </table>
              </div>
            </div>
		</div> 
    </div>
     </section>
    </form>
    <!-- /.content -->
  </div>
  
  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script>
function clearall()
{
	$('#followupDateSpan').html('');
	$('#enqRemarkSpan').html('');
	$('#enqStatusSpan').html('');
}
function validate()
{ 
	clearall();
		//validation for followup date
		if(document.enquiryfollowupform.followupDate.value=="")
		{
			$('#followupDateSpan').html('Please, select next follow-up date..!');
			document.enquiryfollowupform.followupDate.focus();
			return false;
		}
		else if(document.enquiryfollowupform.followupDate.value.length!=0)
		{
			 var n = new Date();
			 var y = n.getFullYear();
			 var m = n.getMonth()+1;
			 var d = n.getDate();
			 //var t=d+"/"+m+"/"+y;
			//var todayDate =  new Date();
			//alert(t);
			var selectedDate = document.enquiryfollowupform.followupDate.value;
			var res = selectedDate.split('/');
			if(res[2] <y)
			{
				$('#followupDateSpan').html('Please, select next valid follow-up date..!');
				
				document.enquiryfollowupform.followupDate.value="";
				document.enquiryfollowupform.followupDate.focus();
				
				return false;
			}
			else if(res[2]==y)
			{
				if(res[1]<m)
					{
					$('#followupDateSpan').html('Please, select next valid follow-up date..!');
					document.enquiryfollowupform.followupDate.value="";
					document.enquiryfollowupform.followupDate.focus();
					return false;
					}
				else if(res[1]==m)
					{
					if(res[0]<d)
						{
						$('#followupDateSpan').html('Please, select next valid follow-up date..!');
						document.enquiryfollowupform.followupDate.value="";
						document.enquiryfollowupform.followupDate.focus();
						return false;
						}
					}
			}
			
		 }
		
		//validation for enquiry remark
		if(document.enquiryfollowupform.enqRemark.value=="")
		{
			$('#enqRemarkSpan').html('Please Fill Remark');
			document.enquiryfollowupform.enqRemark.focus();
			return false;
		}
		else if(document.enquiryfollowupform.enqRemark.value.match(/^[\s]+$/))
		{
			$('#enqRemarkSpan').html('Please, enter enquiry remark..!');
			document.enquiryfollowupform.enqRemark.value="";
			document.enquiryfollowupform.enqRemark.focus();
			return false;
		}
		else if(!document.enquiryfollowupform.enqRemark.value.match(/^[A-Za-z0-9\s./]+$/))
		{
			$('#enqRemarkSpan').html('Please, enter valid enquiry remark..!');
			document.enquiryfollowupform.enqRemark.value="";
			document.enquiryfollowupform.enqRemark.focus();
			return false;
		}
		
		//validation for next enquiry status
		if(document.enquiryfollowupform.enqStatus.value=="Default")
		{
			$('#enqStatusSpan').html('Please, select next status..!');
			document.enquiryfollowupform.enqStatus.focus();
			return false;
		}
		
}

function init()
{
	clearall();
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	 document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
}

$(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker1').datepicker({
      autoclose: true
    })
 $('#datepicker').datepicker({
      autoclose: true
    })
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Customer Daily Report</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">
  
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
 
  <!-- Google Font -->
  <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
 
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
	
<div class="wrapper">

   <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Customer Monthly Report Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Customer Daily Report</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="CustomerMonthlyReport" action="${pageContext.request.contextPath}/ExportMonthlyReport" target="_blank" method="post">
    <section class="content">
<div class="box box-default">
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              
       	 	
	<div class="box-body">
          <div class="row">
              
                 <div class="col-xs-3">
			  		<label>Select Start Date </label> <label class="text-red">*</label>
				    <div class="input-group date">
                     <div class="input-group-addon">
                       <i class="fa fa-calendar"></i>
                     </div>
                       <input  type="text" class="form-control pull-right" id="startDate" name="startDate">
                      
                    </div>
                    <span id="startDateSpan" style="color:#FF0000"></span>
			      </div>
			     
                 <div class="col-xs-3">
			  		<label>Select End Date </label> <label class="text-red">*</label>
				    <div class="input-group date">
                     <div class="input-group-addon">
                       <i class="fa fa-calendar"></i>
                     </div>
                       <input  type="text" class="form-control pull-right" id="endDate" name="endDate">
                       <span class="input-group-btn">
	                     <button type="button" class="btn btn-success" onclick="return searchReportDateWise()"><i class="fa fa-search"></i></button></a>
	                   </span>
                    </div>
                    <span id="endDateSpan" style="color:#FF0000"></span>
			      </div>
			     
			     <%-- 
			      <div class="col-xs-3">
			         <br/>
			          <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-print"></span>Print</button>
			      </div>
			      --%>
				  <div class="col-xs-3">
			 			<br/>
			  			<a href="CustomerMonthlyReport"><button type="button" class="btn btn-default" value="reset" style="width:90px"> Reset</button></a>
                  </div>  
                  
					<div class="col-xs-2">
			        <button type="submit" class="btn btn-info " name="submit">Submit</button>
			     </div> 
			     <!-- 
									<div class="col-xs-3">
										<a target="_blank" ><button
												type="button" class="btn btn-info" onclick="return ExportMonthlyReport()"
												style="background: #48D1CC">Export Monthly Report</button></a>
									</div> -->
           </div>
	</div>
		 
<div  class="panel box box-danger"></div>
				
	
      <div class="box-body">
      <div class="row">
       <div class="col-md-12">
       
       
       <div class="box-body">
              <div class="row">
              
                   <div class="col-xs-3">
                   	<div class="input-group">
                 	<h4><span class="fa fa-university"></span> 
                   	<label id="totalEnquiry">Enquiry in Month: ${totalMonthlyEnquiry}</label>
                   	</h4> 
       		 		</div>
			      </div>  
			     
			     <div class="col-xs-3">
                   <div class="input-group">
                 	<h4><span class="fa fa-certificate"></span> 
                  <label id="totalBooking"> Booking in Month: ${totaMonthlyBooking} </label>
                   </h4> 
       		 	  </div>
			     </div>  
			     
			     <div class="col-xs-3">
                   <div class="input-group">
                 	<h4><span class="fa fa-certificate"></span> 
                 	<label id="todayTotalFlatAgreement">Agg in Month :  ${TotalMonthlyFlatAgreement} </label>
                    </h4> 
       		 	   </div>
			     </div>  
			     
			     <div class="col-xs-3">
                   <div class="input-group">
                 	<h4><span class="fa fa-money"></span> 
                   <label id="totalCollection">Payment Collection in Month:  ${totalMonthlyCustomerPayment} </label>
                    </h4> 
       		 	   </div>
			     </div>  
			     
			     
                   <div class="col-xs-3">
                   	<div class="input-group">
                 	<h4><span class="fa fa-university"></span> 
                 	<label id="totalFlats">Total Flats :  ${totalFlats} </label> 
                   	</h4> 
       		 		</div>
			      </div>  
			     
			     <div class="col-xs-3">
                   <div class="input-group">
                 	<h4><span class="fa fa-certificate"></span>
                 	<label id="totalFlatBooking">Total Booking :  ${totalFlatBooking} </label> 
                   </h4> 
       		 	  </div>
			     </div>  
			     
			     <div class="col-xs-3">
                   <div class="input-group">
                 	<h4><span class="fa fa-certificate"></span> 
                 	<label id="totalFlatAgreement">Total Agg :  ${totalFlatAgreement} </label>
                    </h4> 
       		 	   </div>
			     </div>  

              </div>
            </div>
				
			       
       
          <div class="box-body">
              <div class="row">
              
                   <div class="col-xs-6">
                   <h4>Enquiry Table</h4>
                 <div class="table-responsive" >
	        	  <table id="enquiryListTable" class="table table-bordered">
	                  <thead>
	                  <tr bgcolor="#4682B4">
	                  <th>Enquiry Id</th>
	                  <th>Name</th>
					 
	                  <th>Email Id</th>
					  <th>Mobile No</th>
					
					  <th>Type</th>
					  <th>Status</th>
	                  </tr>
	                  </thead>
	                  <tbody >
	                   <c:forEach items="${enquiryList}" var="enquiryList" varStatus="loopStatus">
	                  	<tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
		                   <td>${enquiryList.enquiryId}</td>
			               <td>${enquiryList.enqfirstName}</td>
			               
			               <td>${enquiryList.enqEmail}</td>
			               <td>${enquiryList.enqmobileNumber1}</td>
			              
			               <td>${enquiryList.flatType}</td>
			               <td>${enquiryList.enqStatus}</td>
	                     </tr>
					   </c:forEach>
	                 </tbody>
	                </table>
	              </div>
                 
                 </div>
                 
                 <div class="col-xs-6">
                 <h4>Booking Table</h4>
	              <div class="table-responsive">
	                <table id="bookingListTable" class="table table-bordered">
	                  <thead>
		                  <tr bgcolor="#4682B4">
			                  <th>Booking Id</th>
			                  <th>Name</th>
							  <th>Building</th>
							  <th>Wing</th>
							  <th>Flat</th>
							  <th>Type</th>
		                  </tr>
	                  </thead>
	                  <tbody>
	                  <c:forEach items="${bookingList}" var="bookingList" varStatus="loopStatus">
	                    <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
		                   <td>${bookingList.bookingId}</td>
		                   <td>${bookingList.bookingfirstname} </td>
			               <td>${bookingList.buildingId}</td>
			               <td>${bookingList.wingId}</td>
			               <td>${bookingList.flatId}</td>
			               <td>${bookingList.flatType}</td>
	                     </tr>
					  </c:forEach>
	                 </tbody>
	                </table>
	              </div>
			      </div>  
			 
			     
              </div>
            </div>
             
	 </div>
	 
	</div>
	</div>
	</div> 
   </div>  
   </div>
   </div>


    </section>
	</form>
    <!-- /.content -->
    
  </div>
 
  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  
<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>

function clearAll()
{
	$("#startDateSpan").html('');
	$("#endDateSpan").html('');
}

function ExportMonthlyReport()
{

alert("fgf");
	var startDate = $("#startDate").val();
	var endDate = $("#endDate").val();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/ExportMonthlyReport',
		type : 'Post',
		data : { startDate : startDate, endDate : endDate},
		dataType : 'json',
		success : function(result)
				  {

					}
		});
	
}
function searchReportDateWise()
{
	clearAll();
	
	if(document.CustomerMonthlyReport.startDate.value == "")
	{
		$("#startDateSpan").html('Please, Select Start Date..!');
		document.CustomerMonthlyReport.startDate.focus();
		return false;
	}

	if(document.CustomerMonthlyReport.endDate.value == "")
	{
		$("#endDateSpan").html('Please, Select End Date..!');
		document.CustomerMonthlyReport.endDate.focus();
		return false;
	}

	var startDate = $("#startDate").val();
	var endDate = $("#endDate").val();
	
	$("#enquiryListTable tr").detach();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/searchEnquiryReportDateWise',
		type : 'Post',
		data : { startDate : startDate, endDate : endDate},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
							$('#enquiryListTable').append('<tr style="background-color: #4682B4;">	<th>Enquiry Id</th><th> Name</th><th>Email Id</th><th>Mobile No</th><th>Type</th><th>Status</th></tr>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									$('#enquiryListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].enquiryId+'</td><td>'+result[i].enqfirstName+'</td><td>'+result[i].enqEmail+'</td><td>'+result[i].enqmobileNumber1+'</td><td>'+result[i].flatType+'</td><td>'+result[i].enqStatus+'</td>');
								}
								else
								{
									$('#enquiryListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].enquiryId+'</td><td>'+result[i].enqfirstName+'</td><td>'+result[i].enqEmail+'</td><td>'+result[i].enqmobileNumber1+'</td><td>'+result[i].flatType+'</td><td>'+result[i].enqStatus+'</td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	

	$("#bookingListTable tr").detach();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/searchBookingReportDateWise',
		type : 'Post',
		data : { startDate : startDate, endDate : endDate},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
							$('#bookingListTable').append('<tr style="background-color: #4682B4;">	<th>Booking Id</th><th> Name</th><th>Building</th><th>Wing</th><th>Flat</th><th>Type</th></tr>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									$('#bookingListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].bookingId+'</td><td>'+result[i].bookingfirstname+'</td><td>'+result[i].buildingId+'</td><td>'+result[i].wingId+'</td><td>'+result[i].flatId+'</td><td>'+result[i].flatType+'</td>');
								}
								else
								{
									$('#bookingListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].bookingId+'</td><td>'+result[i].bookingfirstname+'</td><td>'+result[i].buildingId+'</td><td>'+result[i].wingId+'</td><td>'+result[i].flatId+'</td><td>'+result[i].flatType+'</td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});

	$.ajax({

		url : '${pageContext.request.contextPath}/searchCollectionReportDateWise',
		type : 'Post',
		data : { startDate : startDate, endDate : endDate},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
						
							for(var i=0;i<result.length;i++)
							{ 
								document.getElementById('totalEnquiry').innerHTML = 'Total Enquiry : '+result[0].totalEnquiry;
								document.getElementById('totalBooking').innerHTML = 'Total Booking in Month: '+result[0].totalBooking;
								document.getElementById('totalCollection').innerHTML = 'Collection in Month : '+result[0].totalCollection;
								
								//document.getElementById('todayTotalFlatBooking').innerHTML = 'Booking in Month: '+result[0].todayTotalFlatBooking;
								document.getElementById('todayTotalFlatAgreement').innerHTML = 'Agg in Month: '+result[0].todayTotalFlatAgreement;
								/* 
								document.getElementById('totalFlatAgreement').innerHTML = 'Total Agg : '+result[0].totalFlatAgreement;
								document.getElementById('totalFlatBooking').innerHTML = 'Total Booking : '+result[0].totalFlatBooking;
								document.getElementById('totalFlats').innerHTML = 'Total Flats : '+result[0].totalFlats;
							 */
							} 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
}

  $(function ()
  {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
     $('#startDate').datepicker({
      autoclose: true
    })
    
    //Date picker
     $('#endDate').datepicker({
      autoclose: true
    })
    $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  
  })
   
    $('#bookingListTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    
    $('#enquiryListTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
      
</script>
</body>
</html>

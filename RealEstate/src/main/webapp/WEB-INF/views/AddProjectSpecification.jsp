<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Add Project Specification List</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


  <!-- Google Font -->
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#enquirydbStatusSpan').delay(1000).fadeOut();
      });
    </script>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

  <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Project Specification List :
        <small>Preview</small>
      </h1>
      
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Project Specification</a></li>
        <li class="active">Add Project Specification List</li>
      </ol>
    </section>

<form name="projectspecificationlist" action="${pageContext.request.contextPath}/AddProjectSpecification" onSubmit="return validate()" method="post">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="panel box box-danger"></div>

        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <span id="statusSpan" style="color:#FF0000"></span>
			
			<div class="box-body">
              <div class="row">
              
                 <div class="col-xs-2">
                  <label>Project Name</label>
                  <input type="text" class="form-control" id="projectName" name="projectName" value="${projectList[0].projectName}" readonly>
                  <input type="hidden" class="form-control" id="projectId" name="projectId" value="${projectList[0].projectId}" readonly>
                  <input type="hidden" class="form-control" id="projectspecificationId" name="projectspecificationId" readonly>
                  <span id="statusSpan" style="color:#FF0000"></span>
                </div>               
            
              </div>
            </div>
			
			<div class="box-body">
              <div class="row">
                
			     <div class="col-xs-2">
			      <label>Item Category</label> <label class="text-red">* </label>
                  <select class="form-control" name="suppliertypeId" id="suppliertypeId" onchange="getMainSubItemCategory(this.value)">
				     <option selected="selected" value="Default">-Select Category-</option>
                   <c:forEach var="supplierTypeList" items="${supplierTypeList}" >
					 <option value="${supplierTypeList.suppliertypeId}">${supplierTypeList.supplierType}</option>
				   </c:forEach>
                  </select>
			         <span id="suppliertypeIdSpan" style="color:#FF0000"></span>
			     </div>
		
                 <div class="col-xs-2">
			      <label>Item Sub-Category</label> <label class="text-red">* </label>
                  <select class="form-control" name="subsuppliertypeId" id="subsuppliertypeId" onchange="getItemAndBrandList(this.value)">
				  <option selected="selected" value="Default">-Select Sub Item Category-</option>
                  </select>
			         <span id="subsuppliertypeIdSpan" style="color:#FF0000"></span>
			     </div> 
			      
                 <div class="col-xs-2">
				  <label for="itemName">Item Name </label> <label class="text-red">* </label>
                  <select class="form-control" name="itemId" id="itemId">
				     <option selected="selected" value="Default">-Select Item-</option>
                  </select>
			         <span id="itemIdSpan" style="color:#FF0000"></span>
                </div> 
			     
              	<div class="col-xs-2">
			       <label for="workRate">Brand 1</label><label class="text-red">* </label>
                   <select class="form-control" id="brand1" name="brand1">
                   	<option selected="selected" value="Default">-Select Brand Name-</option>
                   </select>
                   <span id="brand1Span" style="color:#FF0000"></span>
			    </div>
			    
			    <div class="col-xs-2">
			       <label for="workRate">Brand 2</label><label class="text-red">* </label>
                   <select class="form-control" id="brand2" name="brand2">
                    <option selected="selected" value="Default">-Select Brand Name-</option>
                   </select>
                   <span id="brand2Span" style="color:#FF0000"></span>
			    </div>
     		  
				 <div class="col-xs-2">
			     <label for="grandTotal">Brand 3</label><label class="text-red">* </label>
				  <div class="input-group">
                   <select class="form-control" id="brand3" name="brand3">
                    <option selected="selected" value="Default">-Select Brand Name-</option>
                   </select>
                   <span id="addSpecification" class="input-group-btn">
            	     <button type="button" class="btn btn-success" onclick="return AddMultipleSpecifications()"><i class="fa fa-plus"></i>Add</button>
              	   </span>
              	   <span id="updateSpecification" class="input-group-btn">
            	     <button type="button" class="btn btn-success" onclick="return UpdateMultipleSpecifications()"><i class="fa fa-plus"></i>Update</button>
              	   </span>
                  </div>
                  <span id="brand3Span" style="color:#FF0000"></span>
			   </div>
			 
                
                
			  </div>
            </div> 
     
          <div class="box-body">
           <div class="row">        
        	<div class="col-xs-12">
              <table class="table table-bordered" id="projectspecificationTable">
	              <tr bgcolor=#4682B4 style="color: white;">
		              <td>Item Category</td>
		              <td>Item Sub-Category</td>
		              <td>Item Name</td>
		              <td>Brand 1</td>
		              <td>Brand 2</td>
		              <td>Brand 3</td>
		              <td>Action</td>
	               </tr>

               	   <c:forEach items="${projectSpecificationsList}" var="projectSpecificationsList" varStatus="loopStatus">
                    <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                        <td>${projectSpecificationsList.supplierType}</td>
                        <td>${projectSpecificationsList.subsupplierType}</td>
                        <td>${projectSpecificationsList.itemName}</td>
                        <td>${projectSpecificationsList.brandName1}</td>
                        <td>${projectSpecificationsList.brandName2}</td>
                        <td>${projectSpecificationsList.brandName3}</td>
                        <td>
                        	<a onclick="DeleteProjectSpecification('${projectSpecificationsList.projectspecificationId}')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a>
                        </td>
                     </tr>
                    </c:forEach>
              </table>
            </div>
           </div>
          </div>
                
			<input type="hidden" id="creationDate" name="creationDate" >
			<input type="hidden" id="updateDate" name="updateDate" >
			<input type="hidden" id="status" name="status" value="Applied">
			
			<input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>">
		</div>	
      </div>
      
		   	 <div class="box-body">
              <div class="row">
              <br/><br/>
              <div class="col-xs-1">
              </div>
                 <div class="col-xs-4">
	            	 <div class="col-xs-2">	
	             		<a href="ProjectSpecificationMaster"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
				     </div> 
			      </div>
			     
				  <div class="col-xs-4">
                		<button type="reset" class="btn btn-default" value="reset" style="width:90px">Reset</button>
			      </div>
				 
				  <div class="col-xs-2">
			        	<button type="submit" class="btn btn-info " name="submit">Submit</button>
			       </div> 
			     
              </div>
			  </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        
      </div>
      <!-- /.box -->
		
    </section>
	</form>
    <!-- /.content -->
  </div>

 <%@ include file="footer.jsp" %>
   
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>

 <script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
</script>
 
<script>

function clearAll()
{
	$('#suppliertypeIdSpan').html('');
	$('#subsuppliertypeIdSpan').html('');
	$('#itemIdSpan').html('');
	$('#brand1Span').html('');
	$('#brand2Span').html('');
	$('#brand3Span').html('');
}

function AddMultipleSpecifications()
{
	clearAll();
	var projectId 	      = $('#projectId').val();
	var suppliertypeId    = $('#suppliertypeId').val();
	var subsuppliertypeId = $('#subsuppliertypeId').val();
	var itemId            = $('#itemId').val();
	var brand1 			  = $('#brand1').val();
	var brand2 			  = $('#brand2').val();
	var brand3 			  = $('#brand3').val();

	if(suppliertypeId == "Default")
	{
		$('#suppliertypeIdSpan').html('Please, select Item Category..!');
		document.projectspecificationlist.suppliertypeId.focus();
		return false;
	}
	
	if(subsuppliertypeId == "Default")
	{
	  $('#subsuppliertypeIdSpan').html('Please, select Item Sub-Category..!');
	  document.projectspecificationlist.subsuppliertypeId.focus();
	  return false;
	}
	
	if(itemId == "Default")
	{
	  $('#itemIdSpan').html('Please, select Item Name..!');
	  document.projectspecificationlist.itemId.focus();
	  return false;
	}
	
	if(brand1 == "Default")
	{
	  $('#brand1Span').html('Please, select Brand Name..!');
	  document.projectspecificationlist.brand1.focus();
	  return false;
	}
	
	if(brand2 == "Default")
	{
	  $('#brand2Span').html('Please, select Brand Name..!');
	  document.projectspecificationlist.brand2.focus();
	  return false;
	}
	
	if(brand3 == "Default")
	{
	  $('#brand3Span').html('Please, select Brand Name..!');
	  document.projectspecificationlist.brand3.focus();
	  return false;
	}
	
	$('#projectspecificationTable tr').detach();
	
	$.ajax({

		 url : '${pageContext.request.contextPath}/AddMultipleSpecifications',
		type : 'Post',
		data : { projectId : projectId, suppliertypeId : suppliertypeId, subsuppliertypeId : subsuppliertypeId, itemId : itemId, brand1 : brand1, brand2 : brand2, brand3 : brand3 },
		dataType : 'json',
		success : function(result)
				  {
					   if (result) 
					   { 
									$('#projectspecificationTable').append('<tr style="background-color: #4682B4;"><th>Item Category</th><th>Item Sub-Category</th><th>Item Name</th><th>Brand1</th><th>Brand2</th><th>Brand3</th><th style="width:30px">Action</th></tr>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].projectspecificationId;
									$('#projectspecificationTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].supplierType+'</td><td>'+result[i].subsupplierType+'</td><td>'+result[i].itemName+'</td><td>'+result[i].brandName1+'</td><td>'+result[i].brandName2+'</td><td>'+result[i].brandName3+'</td><td><a onclick="DeleteProjectSpecification('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
								else
								{
									var id = result[i].projectspecificationId;
									$('#projectspecificationTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].supplierType+'</td><td>'+result[i].subsupplierType+'</td><td>'+result[i].itemName+'</td><td>'+result[i].brandName1+'</td><td>'+result[i].brandName2+'</td><td>'+result[i].brandName3+'</td><td><a onclick="DeleteProjectSpecification('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}
				  } 

		});
	
	 $('#suppliertypeId').val("Default");
	 $('#subsuppliertypeId').val("Default");
	 $('#itemId').val("Default");
	 $('#brand1').val("Default");
	 $('#brand2').val("Default");
	 $('#brand3').val("Default");
}

function DeleteProjectSpecification(projectspecificationId)
{
   //alert("Project Specification Id = "+projectspecificationId);
	
   var projectId = $('#projectId').val();
   
   $('#projectspecificationTable tr').detach();
   
   $.ajax({

		 url : '${pageContext.request.contextPath}/DeleteProjectSpecification',
		type : 'Post',
		data : { projectspecificationId : projectspecificationId, projectId : projectId },
		dataType : 'json',
		success : function(result)
				  {
					   if (result) 
					   { 
									$('#projectspecificationTable').append('<tr style="background-color: #4682B4;"><th>Item Category</th><th>Item Sub-Category</th><th>Item Name</th><th>Brand1</th><th>Brand2</th><th>Brand3</th><th style="width:30px">Action</th></tr>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].projectspecificationId;
									$('#projectspecificationTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].supplierType+'</td><td>'+result[i].subsupplierType+'</td><td>'+result[i].itemName+'</td><td>'+result[i].brandName1+'</td><td>'+result[i].brandName2+'</td><td>'+result[i].brandName3+'</td><td><a onclick="DeleteProjectSpecification('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
								else
								{
									var id = result[i].projectspecificationId;
									$('#projectspecificationTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].supplierType+'</td><td>'+result[i].subsupplierType+'</td><td>'+result[i].itemName+'</td><td>'+result[i].brandName1+'</td><td>'+result[i].brandName2+'</td><td>'+result[i].brandName3+'</td><td><a onclick="DeleteProjectSpecification('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}
				  } 

		});
}

function getSubItemCategory()
{
	$('#subItemCategoryName').empty();
	
	var suppliertypeId = $('#suppliertypeId').val();
	var subsuppliertypeId = $('#subsuppliertypeId').val();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getSubItemCategoryList',
		type : 'Post',
		data : { suppliertypeId : suppliertypeId, subsuppliertypeId : subsuppliertypeId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select Sub Item Category-");
								$("#subItemCategoryName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].subItemCategoryName).text(result[i].subItemCategoryName);
							    $("#subItemCategoryName").append(option);
							} 
						} 
						else
						{
							alert("failure111");
						}

					}
		});	
	
}

function getMainSubItemCategory()
{
	$('#subsuppliertypeId').empty();
	var suppliertypeId = $('#suppliertypeId').val();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getSubSupplierTypeList',
		type : 'Post',
		data : { suppliertypeId : suppliertypeId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select Sub Item Category-");
								$("#subsuppliertypeId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].subsuppliertypeId).text(result[i].subsupplierType);
							    $("#subsuppliertypeId").append(option);
							} 
						} 
						else
						{
							alert("failure111");
						}

					}
		});	
	
}

function getItemAndBrandList()
{
	$('#itemId').empty();
	$('#brand1').empty();
	$('#brand2').empty();
	$('#brand3').empty();
	
	var suppliertypeId = $('#suppliertypeId').val();
	var subsuppliertypeId = $('#subsuppliertypeId').val();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getAllItemList',
		type : 'Post',
		data : { suppliertypeId : suppliertypeId, subsuppliertypeId : subsuppliertypeId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select Item Name-");
								$("#itemId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].itemId).text(result[i].itemName);
							    $("#itemId").append(option);
							} 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
	 $.ajax({

		url : '${pageContext.request.contextPath}/getAllBrandList',
		type : 'Post',
		data : { suppliertypeId : suppliertypeId, subsuppliertypeId : subsuppliertypeId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select Brand Name-");
								$("#brand1").append(option);
								
								option = $('<option/>');
								option.attr('value',"Default").text("-Select Brand Name-");
								$("#brand2").append(option);
								
								option = $('<option/>');
								option.attr('value',"Default").text("-Select Brand Name-");
								$("#brand3").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].brandId).text(result[i].brandName);
							    $("#brand1").append(option);
							    
							    option = $('<option />');
							    option.attr('value',result[i].brandId).text(result[i].brandName);
							    $("#brand2").append(option);
							    
							    option = $('<option />');
							    option.attr('value',result[i].brandId).text(result[i].brandName);
							    $("#brand3").append(option);
							} 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
}

function init()
{
	//clearall();
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	 $("#updateSpecification").removeAttr("style").hide();
}


  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
     $('#workStartDate').datepicker({
      autoclose: true
    })

    $('#workEndDate').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
  
  $(function () {
    $('#projectspecificationTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
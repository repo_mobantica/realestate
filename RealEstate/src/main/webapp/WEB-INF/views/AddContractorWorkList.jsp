<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Add Contractor Work List</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


  <!-- Google Font -->
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#enquirydbStatusSpan').delay(1000).fadeOut();
      });
    </script>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

  <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Contractor Work List :
        <small>Preview</small>
      </h1>
      
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Contractor</a></li>
        <li class="active">Add Contractor Work List</li>
      </ol>
    </section>

<form name="contractorworkorderform" action="${pageContext.request.contextPath}/AddContractorWorkList" onSubmit="return validate()" method="post">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="panel box box-danger"></div>

        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <span id="statusSpan" style="color:#FF0000"></span>
			
			<div class="box-body">
              <div class="row">
              
                 <div class="col-xs-2">
                  <label>Work Id</label>
                  <input type="text" class="form-control" id="workId" name="workId" value="${workListCode}" readonly>
                   <span id="statusSpan" style="color:#FF0000"></span>
                </div>               
            
                  <div class="col-xs-2">
			      <label>Project  </label> <label class="text-red">* </label>
                  <select class="form-control" name="projectId" id="projectId" onchange="getBuldingList(this.value)">
				    <option selected="" value="Default">-Select Project-</option>
                    <c:forEach var="projectList" items="${projectList}">
                    <option value="${projectList.projectId}">${projectList.projectName}</option>
				    </c:forEach>
                  </select>
			        <span id="projectIdSpan" style="color:#FF0000"></span>
			     </div> 
			          
			      <div class="col-xs-2">
			      <label>Building Name </label> <label class="text-red">* </label>
                    <select class="form-control" name="buildingId" id="buildingId" onchange="getWingNameList(this.value)">
				  	<option selected="" value="Default">-Select Project Building-</option>
                    </select>
			       <span id="buildingIdSpan" style="color:#FF0000"></span>
			     </div> 
			
                 <div class="col-xs-2">
			      <label>Wing Name</label> <label class="text-red">* </label>
              		<select class="form-control" name="wingId" id="wingId">
				 	   <option selected="" value="Default">-Select Wing Name-</option>
                       <c:forEach var="projectwingList" items="${projectwingList}">
                       <option value="${projectwingList.wingId}">${projectwingList.wingId}</option>
				       </c:forEach>
                    </select>
                   <span id="wingIdSpan" style="color:#FF0000"></span>
                 </div> 
                    
                 <div class="col-xs-3">
			      <label>Work/Contractor Type </label> <label class="text-red">* </label>
                   <select class="form-control" name="contractortypeId" id="contractortypeId" onchange="getSubContractorType(this.value)">
				 	 <option selected="" value="Default">-Select Contractor Type-</option>
                     <c:forEach var="contractorTypeList" items="${contractorTypeList}">
                     <option value="${contractorTypeList.contractortypeId}">${contractorTypeList.contractorType}</option>
				     </c:forEach>
                   </select>
                   <span id="contractortypeIdSpan" style="color:#FF0000"></span>  
			     </div> 
			      
              </div>
            </div>
			
			<div class="box-body">
              <div class="row">
                
			     <div class="col-xs-2">
			          <label>Sub-Contractor Type</label> <label class="text-red">* </label>
                      <select class="form-control" name="subcontractortypeId" id="subcontractortypeId">
				           <option selected="selected" value="Default">-Select Sub-Contractor Type-</option>
                  		   <c:forEach var="subcontractortypeList" items="${subcontractortypeList}">
	                        <option value="${subcontractortypeList.subcontractortypeId}">${subcontractortypeList.subcontractorType}</option>
	                    </c:forEach>
                      </select>
                      <span id="subcontractortypeIdSpan" style="color:#FF0000"></span>
			     </div>
		
                 <div class="col-xs-2">
			      <label>Work Type</label> <label class="text-red">* </label>
                  <select class="form-control" name="workType" id="workType">
				    <option selected="selected" value="Default">-Select Work Type-</option>
				    <option value="With Material">With Material</option>
				    <option value="Without Material">W/O Material</option>
                  </select>
			      <span id="workTypeSpan" style="color:#FF0000"></span>
			     </div> 
			      
			      
                 <div class="col-xs-2">
			      <label>Type</label> <label class="text-red">* </label>
                  <select class="form-control" name="type" id="type">
				    <option selected="selected" value="Default">-Select Type-</option>
				    <option value="Area">Area</option>
				    <option value="Pieces">Pieces</option>
                  </select>
			      <span id="typeSpan" style="color:#FF0000"></span>
			     </div> 
			     
			     
              	<div class="col-xs-2">
			       <label for="unit">Unit</label><label class="text-red">* </label>
                   <input type="text" class="form-control" id="unit" name="unit"  placeholder="Unit" onchange="getTotalAmount(this.value)">
                   <span id="unitSpan" style="color:#FF0000"></span>
			    </div>
			    
              	<div class="col-xs-2">
			       <label for="workRate">Rate</label><label class="text-red">* </label>
                   <input type="text" class="form-control" id="workRate" name="workRate"  placeholder="work Rate" onchange="getTotalAmount(this.value)">
                   <span id="workRateSpan" style="color:#FF0000"></span>
			    </div>
     		  
				 <div class="col-xs-2">
			     <label for="grandTotal">G.AMT</label><label class="text-red">* </label>
				  <div class="input-group">
                 
                  <input type="text" class="form-control" id="grandTotal" name="grandTotal"  placeholder="Grand Total" readonly>
                   <span class="input-group-btn">
            	    <button type="button" class="btn btn-success" onclick="return AddContractorWorkList()"><i class="fa fa-plus"></i>Add</button>
              	   </span>
                  </div>
                  <span id="grandTotalSpan" style="color:#FF0000"></span>
			   </div>
			 
                
                
			  </div>
            </div> 
     
          <div class="box-body">
           <div class="row">        
        	<div class="col-xs-12">
              <table class="table table-bordered" id="contractorWorkListTable">
	              <tr bgcolor=#4682B4 style="color: white;">
		              <td>Sub-Contractor Type</td>
		              <td>Work Type</td>
		              <td>Type</td>
		              <td>Unit</td>
		              <td>Rate</td>
		              <td>G.AMT</td>
		              <td>Action</td>
	               </tr>

               	   <%-- <c:forEach items="${bankEmployeesList}" var="bankEmployeesList" varStatus="loopStatus">
                    <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                        <td>${bankEmployeesList.employeeName}</td>
                        <td>${bankEmployeesList.employeeDesignation}</td>
                        <td>${bankEmployeesList.employeeEmail}</td>
                        <td>${bankEmployeesList.employeeMobileno}</td>
                        <td>
                        	<a href="${pageContext.request.contextPath}/EditBankEmployee?bankId=${bankEmployeesList.bankId}" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>
                           |<a onclick="DeleteBankEmployee()" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a>
                        </td>
                     </tr>
                    </c:forEach> --%>
              </table>
            </div>
           </div>
          </div>
                
			<input type="hidden" id="creationDate" name="creationDate" >
			<input type="hidden" id="updateDate" name="updateDate" >
			<input type="hidden" id="status" name="status" value="Applied">
			
			<input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>">
		</div>	
      </div>
      
		   	 <div class="box-body">
              <div class="row">
              <br/><br/>
              <div class="col-xs-1">
              </div>
                 <div class="col-xs-4">
	            	 <div class="col-xs-2">	
	             		<a href="ContractorWorkListMaster"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
				     </div> 
			     </div>
				  <div class="col-xs-4">
                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
			     </div>
					<div class="col-xs-2">
			        <button type="submit" class="btn btn-info " name="submit">Submit</button>
			     </div> 
			     
              </div>
			  </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        
      </div>
      <!-- /.box -->
		
    </section>
	</form>
    <!-- /.content -->
  </div>

 <%@ include file="footer.jsp" %>
   
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>

 <script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
</script>
 
<script>

function clearall()
{
	$('#projectIdSpan').html('');
	$('#buildingIdSpan').html('');
	$('#wingIdSpan').html('');
	$('#contractortypeIdSpan').html('');
	$('#subcontractortypeIdSpan').html('');
	$('#workTypeSpan').html('');
	$('#typeSpan').html('');
	$('#unitSpan').html('');
	$('#workRateSpan').html('');
	
}
function getTotalAmount()
{
	var workRate = Number($('#workRate').val());
	var unit = Number($('#unit').val());
	var grandTotal=workRate*unit;

	document.contractorworkorderform.grandTotal.value=grandTotal.toFixed(0);
}


function AddContractorWorkList()
{
	clearall();
	if(document.contractorworkorderform.contractortypeId.value=="Default")
	{
		$('#contractortypeIdSpan').html('Please Select Contractor Type');
		document.contractorworkorderform.contractortypeId.focus();
		return false;
	}
	if(document.contractorworkorderform.subcontractortypeId.value=="Default")
	{
		$('#subcontractortypeIdSpan').html('Please Select Contractor Sub-Type');
		document.contractorworkorderform.subcontractortypeId.focus();
		return false;
	}
	if(document.contractorworkorderform.workType.value=="Default")
	{
		$('#workTypeSpan').html('Please Select Work Type');
		document.contractorworkorderform.workType.focus();
		return false;
	}
	if(document.contractorworkorderform.type.value=="Default")
	{
		$('#typeSpan').html('Please Select Type');
		document.contractorworkorderform.type.focus();
		return false;
	}
	
	if(document.contractorworkorderform.unit.value.length==0)
	{
		$('#unitSpan').html('Please, enter unit..!');
		//document.contractorworkorderform.employeeBankacno.value="";
		document.contractorworkorderform.unit.focus();
		return false;
	}
	else if(!document.contractorworkorderform.unit.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
	{
		 if(!document.contractorworkorderform.unit.value.match(/^[0-9]+$/))
			 {
				$('#unitSpan').html('Please, use only digit value for unit..! eg:21.36 OR 30');
				document.contractorworkorderform.unit.value="";
				document.contractorworkorderform.unit.focus();
				return false;
			}
	}

	if(document.contractorworkorderform.workRate.value.length==0)
	{
		$('#workRateSpan').html('Please, enter unit..!');
		//document.contractorworkorderform.employeeBankacno.value="";
		document.contractorworkorderform.workRate.focus();
		return false;
	}
	else if(!document.contractorworkorderform.workRate.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
	{
		 if(!document.contractorworkorderform.workRate.value.match(/^[0-9]+$/))
			 {
				$('#workRateSpan').html('Please, use only digit value for unit..! eg:21.36 OR 30');
				document.contractorworkorderform.workRate.value="";
				document.contractorworkorderform.workRate.focus();
				return false;
			}
	}
	
	var workId = $('#workId').val();
	var contractortypeId = $('#contractortypeId').val();
	var subcontractortypeId = $('#subcontractortypeId').val();
	var workType = $('#workType').val();
	var type = $('#type').val();
	
	var unit = Number($('#unit').val());
	var workRate = Number($('#workRate').val());
	var grandTotal = Number($('#grandTotal').val());
	
	$("#contractorWorkListTable tr").detach();
	$.ajax({

		url : '${pageContext.request.contextPath}/AddContractorWorkListHistory',
		type : 'Post',
		data : { workId : workId, contractortypeId : contractortypeId, subcontractortypeId : subcontractortypeId, workType : workType, type : type, unit : unit, workRate : workRate, grandTotal : grandTotal},
		dataType : 'json',
		success : function(result)
				  {
					   if (result) 
					   { 
							$('#contractorWorkListTable').append('<tr style="background-color: #4682B4;"><td style="color: white;">Sub-Contractor Type</td><td style="color: white;">Work Type</td><td style="color: white;">Type</td><td style="color: white;">Unit</th><td style="color: white;">Rate</td><td style="color: white;">G.AMT</td></td><td style="color: white;">Action</td></tr>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].workHistoryId;
									$('#contractorWorkListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].subcontractortypeId+'</td><td>'+result[i].workType+'</td><td>'+result[i].type+'</td><td>'+result[i].unit+'</td><td>'+result[i].workRate+'</td><td>'+result[i].grandTotal+'</td><td><a onclick="DeleteContractorWorkList('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
								else
								{
									var id = result[i].workHistoryId;
									$('#contractorWorkListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].subcontractortypeId+'</td><td>'+result[i].workType+'</td><td>'+result[i].type+'</td><td>'+result[i].unit+'</td><td>'+result[i].workRate+'</td><td>'+result[i].grandTotal+'</td><td><a onclick="DeleteContractorWorkList('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});	
}

function DeleteContractorWorkList(workHistoryId)
{
var workId = $('#workId').val();
	
	$("#contractorWorkListTable tr").detach();
	$.ajax({

		url : '${pageContext.request.contextPath}/DeleteContractorWorkListHistory',
		type : 'Post',
		data : { workId : workId, workHistoryId : workHistoryId},
		dataType : 'json',
		success : function(result)
				  {
					   if (result) 
					   { 
							$('#contractorWorkListTable').append('<tr style="background-color: #4682B4;"><td style="color: white;">Sub-Contractor Type</td><td style="color: white;">Work Type</td><td style="color: white;">Type</td><td style="color: white;">Unit</th><td style="color: white;">Rate</td><td style="color: white;">G.AMT</td></td><td style="color: white;">Action</td></tr>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].workHistoryId;
									$('#contractorWorkListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].subcontractortypeId+'</td><td>'+result[i].workType+'</td><td>'+result[i].type+'</td><td>'+result[i].unit+'</td><td>'+result[i].workRate+'</td><td>'+result[i].grandTotal+'</td><td><a onclick="DeleteContractorWorkList('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
								else
								{
									var id = result[i].workHistoryId;
									$('#contractorWorkListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].subcontractortypeId+'</td><td>'+result[i].workType+'</td><td>'+result[i].type+'</td><td>'+result[i].unit+'</td><td>'+result[i].workRate+'</td><td>'+result[i].grandTotal+'</td><td><a onclick="DeleteContractorWorkList('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});	
	
}
function getSubContractorType()
{
	$('#subcontractortypeId').empty();
	var contractortypeId = $('#contractortypeId').val();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getSubContractorTypeList',
		type : 'Post',
		data : { contractortypeId : contractortypeId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select Sub Contractor-");
								$("#subcontractortypeId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].subcontractortypeId).text(result[i].subcontractorType);
							    $("#subcontractortypeId").append(option);
							} 
						} 
						else
						{
							alert("failure111");
						}

					}
		});	
	
}

function validate()
{ 
	clearall();

	if(document.contractorworkorderform.projectId.value=="Default")
	{
		$('#projectIdSpan').html('Please Select Project');
		document.contractorworkorderform.projectId.focus();
		return false;
	}
	//for Building name
	
	if(document.contractorworkorderform.buildingId.value=="Default")
	{
		$('#buildingIdSpan').html('Please Select Project Building');
		document.contractorworkorderform.buildingId.focus();
		return false;
	}
	if(document.contractorworkorderform.wingId.value=="Default")
	{
		$('#wingIdSpan').html('Please Select Project wing');
		document.contractorworkorderform.wingId.focus();
		return false;
	}
	
	if(document.contractorworkorderform.contractortypeId.value=="Default")
	{
		$('#contractortypeIdSpan').html('Please Select work/contractor type');
		document.contractorworkorderform.contractortypeId.focus();
		return false;
	}

}
function init()
{
	clearall();
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	

}



function getBuldingList()
{
	 $("#buildingId").empty();
	 var projectId = $('#projectId').val();
	 
	 $.ajax({

		url : '${pageContext.request.contextPath}/getBuildingList',
		type : 'Post',
		data : { projectId : projectId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Building Name-");
							$("#buildingId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].buildingId).text(result[i].buildingName);
							    $("#buildingId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
}//end of get Building List


function getWingNameList()
{
	 $("#wingId").empty();
	 var buildingId = $('#buildingId').val();
	 var projectId = $('#projectId').val();
	 $.ajax({

		url : '${pageContext.request.contextPath}/getprojectwingList',
		type : 'Post',
		data : { buildingId : buildingId, projectId : projectId },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Wing Name-");
							$("#wingId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].wingId).text(result[i].wingName);
							    $("#wingId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}


function getFloorNameList()
{
	 $("#floorId").empty();
	 var wingId = $('#wingId').val();
	 var buildingId = $('#buildingId').val();
	 var projectId = $('#projectId').val();
	 
	 $.ajax({

		url : '${pageContext.request.contextPath}/getwingfloorNameList',
		type : 'Post',
		data : {wingId : wingId, buildingId : buildingId, projectId : projectId },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"All").text("All");
							$("#floorId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].floorId).text(result[i].floortypeName);
							    $("#floorId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	

}//end of get Floor Type List


  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
     $('#workStartDate').datepicker({
      autoclose: true
    })

    $('#workEndDate').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>

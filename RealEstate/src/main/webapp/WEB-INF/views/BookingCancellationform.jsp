<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Booking Cancellation form</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        
  <script src="https://code.jquery.com/jquery-3.0.0.js"></script>
  <script src="https://code.jquery.com/jquery-migrate-3.0.1.js"></script>
          
</head>
<body class="hold-transition skin-blue sidebar-mini"  onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

  <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
  <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Booking Cancellation form:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Sale</a></li>
        <li class="active">Booking Cancellation form</li>
      </ol>
    </section>

<form name="bookingcancelForm" action="${pageContext.request.contextPath}/BookingCancellationform" onSubmit="return validate()" method="POST">
    <!-- Main content -->
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              
			  <div class="box-body">
              <div class="row">
                <div class="col-xs-2">
			     <label for="cancelId">Cancel Id</label>
                  <input type="text" class="form-control" id="cancelId" name="cancelId"  value="${cancelCode}" readonly>
                 </div>
                  <div class="col-xs-2">
			     <label for="bookingId">Booking Id</label>
                  <input type="text" class="form-control" id="bookingId" name="bookingId" value="${bookingList[0].bookingId}" readonly>
                 </div>
                    <div class="col-xs-2">
			     <label for="bookingDate">Booking Date</label>
                  <input type="text" class="form-control" id="bookingDate" name="bookingDate"  value="${bookingList[0].creationDate}" readonly>
                 </div>
                      <div class="col-xs-2">
			     <label for="cancelDate">Cancellation Date</label>
                  <input type="text" class="form-control" id="cancelDate" name="cancelDate" readonly>
                 </div>  
				</div>
            </div>
		 </div>
		 
		 <div class="col-md-12">
              
			  <div class="box-body">
              <div class="row">
                <div class="col-xs-2">
			     <label for="customerName">Customer Name</label>
                  <input type="text" class="form-control" id="customerName" name="customerName"  value="${bookingList[0].bookingfirstname} ${bookingList[0].bookingmiddlename} ${bookingList[0].bookinglastname}" readonly>
                 </div>
                  <div class="col-xs-2">
			     <label for="customerEmail">Email Id</label>
                  <input type="text" class="form-control" id="customerEmail" name="customerEmail" value="${bookingList[0].bookingEmail}" readonly>
                 </div>
                    <div class="col-xs-2">
			     <label for="customerMobile">Mobile Number</label>
                  <input type="text" class="form-control" id="customerMobile" name="customerMobile"  value="${bookingList[0].bookingmobileNumber1}" readonly>
                 </div>
                     
				</div>
            </div>
		 </div>
     </div>
	  </div>
	  
	  
           <div class="box-body">
          <div class="row">
            <div class="col-md-6">
               
               
                 <div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
			     <label for="projectName">Project Name</label>
                  <input type="text" class="form-control" id="projectId" name="projectId" value="${bookingList[0].projectId}" readonly>
                 </div>
                    <div class="col-xs-6">
			     <label for="buildingName">Building Name</label>
                  <input type="text" class="form-control" id="buildingId" name="buildingId" value="${bookingList[0].buildingId}" readonly>
                 </div>
				</div>
            </div>
               
                       <div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
			     <label for="wingName">Wing Name</label>
                  <input type="text" class="form-control" id="wingId" name="wingId" value="${bookingList[0].wingId}" readonly>
                 </div>
                 <div class="col-xs-6">
			     <label for="floortypeName">Floor Name</label>
                  <input type="text" class="form-control" id="floorId" name="floorId" value="${bookingList[0].floorId}" readonly>
                 </div>
				</div>
            </div>
                    <div class="box-body">
              <div class="row">
                    <div class="col-xs-6">
			     <label for="flatType">Flat Type</label>
                  <input type="text" class="form-control" id="flatType" name="flatType" value="${bookingList[0].flatType}" readonly>
                 </div>
                    <div class="col-xs-6">
			     <label for="flatNumber">Flat Number</label>
                  <input type="text" class="form-control" id="flatId" name="flatId" value="${bookingList[0].flatId}" readonly>
                 </div>
				</div>
            </div>
                    <div class="box-body">
              <div class="row">
               
                    <div class="col-xs-6">
			     <label for="flatFacing">Flat Facing type</label>
                  <input type="text" class="form-control" id="flatFacing" name="flatFacing" value="${bookingList[0].flatFacing}" readonly>
                 </div>
                      <div class="col-xs-6">
			     <label for="flatareainSqFt">Flat Area</label>
                  <input type="text" class="form-control" id="flatareainSqFt" name="flatareainSqFt" value="${bookingList[0].flatareainSqFt}" readonly>
                 </div>
				</div>
            </div>
            
                     <div class="box-body">
              <div class="row">
                    <div class="col-xs-6">
			     <label for="flatbasicCost">Flat Basic Cost </label>
                  <input type="text" class="form-control" id="flatbasicCost" name="flatbasicCost"  value="${bookingList[0].flatbasicCost}" readonly>
                 </div>
				</div>
            </div>
        
     
               </div>
  				<div class="col-md-6">
           	      <div class="box-body">
           	      </br></br>
           	         <div class="form-group">
                  <label for="aggreementValue1" class="col-sm-4 control-label">Agreement Value </label>
                  <div class="col-sm-5">
					<input type="text" class="form-control" id="aggreementValue1" name="aggreementValue1" value="${bookingList[0].aggreementValue1}" readonly>      
				  </div>
                </div>
             
                   </br></br>
             
	                <div class="form-group">
	                  <label for="cancelCharge" class="col-sm-4 control-label">Cancellation Charges <label class="text-red">* </label></label>
	
	                  <div class="col-sm-5">
					<input type="text" class="form-control" id="cancelCharge" name="cancelCharge" onchange="calculate(this.value)">  
					<span id="cancelChargeSpan" style="color:#FF0000"></span>         
					  </div>
	                </div>
                   </br></br>
                <div class="form-group">
                  <label for="gstTaxes" class="col-sm-4 control-label"> GST Taxes<label class="text-red">* </label></label>

                  <div class="col-sm-5">
				<input type="text" class="form-control" id="gstTaxes" name="gstTaxes"  onchange="calculate(this.value)">   
				<span id="gstTaxesSpan" style="color:#FF0000"></span>       
				  </div>
                </div>
                   </br></br>
                <div class="form-group">
                  <label for="netAmount" class="col-sm-4 control-label">Net Amount<label class="text-red">* </label></label>

                  <div class="col-sm-5">
					<input type="text" class="form-control" id="netAmount" name="netAmount" readonly>  
					<span id="netAmountSpan" style="color:#FF0000"></span>        
				  </div>
                </div>
            
	  </div> 
            
            
             </div>
             
               </div>
                   <div class="box-body">
              <div class="row">
              </br>
                
                 <div class="col-xs-4">
	            	 <div class="col-xs-3">	
	             		<a href="BookingMaster"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
				     </div> 
			     </div>
				  <div class="col-xs-4">
                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
              
			     </div>
					<div class="col-xs-2">
			  <button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
              </div> 
			 </div>
			  </div>
                    <div class="col-md-6">
			
			</br>
			</br>
		  <input type="hidden" id="creationDate" name="creationDate" value="">
			  <input type="hidden" id="updateDate" name="updateDate" value="">
			  <input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>">
		  
            </div>
                  
               </div>
        <!-- /.box-body -->
        
      </div>
      <!-- /.box -->

     
    </section>
	</form>
    <!-- /.content -->
  </div>
 
 <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->

<script>
function clearall()
{
	$('#cancelChargeSpan').html('');
	$('#gstTaxesSpan').html('');
	$('#netAmountSpan').html('');
	
}
function calculate()
{
	 var cancelCharge = $('#cancelCharge').val();
	 var gstTaxes = $('#gstTaxes').val();
	 var netAmount = parseInt(cancelCharge)+parseInt(gstTaxes);
	 document.getElementById("netAmount").value = netAmount ; 
	 
}
function init()
{
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	
	document.getElementById("cancelDate").value =  day + "/" + month + "/" + year;
	

  document.bookingcancelForm.cancelCharge.focus();
}
function validate()
{
	clearall();
	if(document.bookingcancelForm.cancelCharge.value=="")
	{
		$('#cancelChargeSpan').html('Please, enter valid digit booking cancel charge..!');
		document.bookingcancelForm.cancelCharge.value="";
		document.bookingcancelForm.cancelCharge.focus();
		return false;
	}
	else if(!document.bookingcancelForm.cancelCharge.value.match(/^[0-9]+$/))
	{
		$('#cancelChargeSpan').html(' booking cancel charge must be digit numbers only..!');
		document.bookingcancelForm.cancelCharge.value="";
		document.bookingcancelForm.cancelCharge.focus();
		return false;
	}
	if(document.bookingcancelForm.gstTaxes.value=="")
	{
		$('#gstTaxesSpan').html('Please, enter valid digit GST Tax charge..!');
		document.bookingcancelForm.gstTaxes.value="";
		document.bookingcancelForm.gstTaxes.focus();
		return false;
	}
	else if(!document.bookingcancelForm.gstTaxes.value.match(/^[0-9]+$/))
	{
		$('#gstTaxesSpan').html(' GST Tax charge must be digit numbers only..!');
		document.bookingcancelForm.gstTaxes.value="";
		document.bookingcancelForm.gstTaxes.focus();
		return false;
	}
	
}

</script>
</body>
</html>

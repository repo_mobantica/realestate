<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | User Profile</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

    <%@ include file="headerpage.jsp" %>
   <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
   
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Profile
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Profile</a></li>
        <li class="active">User profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="GetImage?profileImgPath=<%=session.getAttribute("profile_img")%>"  height="250px" width="250px" alt="User profile picture">

              <h3 class="profile-username text-center">${employeeDetails[0].employeefirstName} ${employeeDetails[0].employeelastName}</h3>

              <p class="text-muted text-center">${designationName}</p>

			
			<br/><br/><br/>
			
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-8">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#activity" data-toggle="tab">About</a></li>
              <li><a href="#settings" data-toggle="tab">Settings</a></li>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="activity">
                <!-- Post -->
                <div class="post">
                  
			      <div class="box-body">
	                   <strong><i class="fa fa-book margin-r-5"></i> Full Name</strong>
	                   <p class="text-muted">
	                      <label>${employeeDetails[0].employeefirstName} ${employeeDetails[0].employeemiddleName} ${employeeDetails[0].employeelastName}</label>
	                   </p>
	
	                   <hr>
	
	                   <strong><i class="fa fa-map-marker margin-r-5"></i> Address</strong>
	                   <p class="text-muted"><label>${employeeDetails[0].employeeAddress}, ${employeeDetails[0].areaPincode}</label></p>
	 
	                   <hr>
			  		   
			  		   <strong><i class="fa fa-birthday-cake margin-r-5"></i> Date Of Birthday</strong>
	                   <p class="text-muted"><label>${employeeDetails[0].employeeDob}</label></p>
	
	                   <hr>
                  </div>
				  
                </div>
                <!-- /.post -->

              </div>
            
              <div class="tab-pane" id="settings">
                <form class="form-horizontal" name="profileform" action="${pageContext.request.contextPath}/profile" method="POST">
				<div class="form-group">
                </div>
                <div class="box-body">
                <div class="row">
                 <div class="col-md-1">
                 </div>
               <div class="col-md-11">
                  <span id="resultSpan" style="color:#008000"></span>
                  
                  <div class="form-group">
                    <label for="userName" class="col-sm-3">Change Username</label>
                    <div class="col-sm-4">
                      <input type="text" class="form-control" id="userName" name="userName" value="${loginDetails[0].userName}" placeholder="Enter username" onchange="return checkUserName()">
                    </div>
                    <span id="userNameSpan" style="color:#FF0000" hidden="true"><img src="${pageContext.request.contextPath}/resources/dist/img/ok.gif" border="0"/></span>
                    <span id="userNameMsgSpan" style="color:#FF0000"></span>
                  </div>
                  
                  <div class="form-group">
                    <label for="OldpassWord" class="col-sm-3">Old Password</label>
                    <div class="col-sm-4">
                      <input type="password" class="form-control" id="OldpassWord" name="OldpassWord" placeholder="**********" onchange="return CheckOldPassword()">
                    </div>
                    <span id="OldpassWordSpan" style="color:#FF0000" hidden="true"><img src="${pageContext.request.contextPath}/resources/dist/img/ok.gif" border="0"/></span>
                    <span id="OldpassWordMsgSpan" style="color:#FF0000"></span>
                  </div>
                  
                  <div class="form-group">
                    <label for="passWord" class="col-sm-3">New Password</label>
                    <div class="col-sm-4">
                      <input type="password" class="form-control" id="passWord" name="passWord" placeholder="************" onchange="return CheckNewPassowrd()">
                    </div>
                    <span id="passWordSpan" style="color:#FF0000" hidden="true"><img src="${pageContext.request.contextPath}/resources/dist/img/ok.gif" border="0"/></span>
                    <span id="passWordMsgSpan" style="color:#FF0000"></span>
                  </div>
                  
                  <div class="form-group">
                    <label for="confirmPassword" class="col-sm-3">Confirm Password</label>
                    <div class="col-sm-4">
                      <input type="password" class="form-control" id="confirmPassword" name="confirmPassword" placeholder="************" onchange="return CheckConfirmPassword()">
                    </div>
                    <span id="confirmPasswordSpan" style="color:#FF0000" hidden="true"><img src="${pageContext.request.contextPath}/resources/dist/img/ok.gif" border="0"/></span>
                    <span id="confirmPasswordMsgSpan" style="color:#FF0000"></span>
                  </div>
                  
                  <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-10">
                     &nbsp &nbsp &nbsp &nbsp  &nbsp &nbsp 
					  <button type="button" class="btn btn-danger" onclick="Clear()">RESET</button>
					  &nbsp &nbsp &nbsp &nbsp  &nbsp &nbsp  &nbsp &nbsp
                      <input type="button" class="btn btn-success" onclick="return updateProfile()" value="SUBMIT">
                    </div>
                  </div>
              </div>
              </div> 
               </div>
          </form>
                
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
 
  <!-- /.content-wrapper -->
    <!-- Control Sidebar -->
  <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>

<script>

function clearAll()
{
   document.getElementById("userNameSpan").style.display="none";
   $('#userNameMsgSpan').html('');
}

function clearAll2()
{
  document.getElementById("OldpassWordSpan").style.display = "none";
  $('#OldpassWordMsgSpan').html('');
}

function clearAll3()
{
  document.getElementById("passWordSpan").style.display = "none";
  $('#passWordMsgSpan').html('');
}

function clearAll4()
{
  document.getElementById("confirmPasswordSpan").style.display = "none";
  $('#confirmPasswordMsgSpan').html('');
}

function checkUserName()
{
	clearAll();
	
	var userName = "";
	
	if(document.profileform.userName.value == "")
	{
		document.profileform.userName.focus();
		$('#userNameMsgSpan').html('Username can not be empty..!');
		return false;
	}
	else if(document.profileform.userName.value.match(/^[\s]+$/))
	{
		document.profileform.userName.value="";
		$('#userNameMsgSpan').html('Username can not be empty..!');
		document.profileform.userName.focus();
		return false;
	}/* 
	else if(!document.profileform.userName.value.match(/^[A-z0-9_@.]{3,15}$/))
	{
		$('#userNameMsgSpan').html('Invalid Username..!');
		document.profileform.userName.focus();
		return false;
	} */
	else
	{
		userName = document.profileform.userName.value;
	}
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getUserName',
		type : 'Post',
		data : { userName : userName},
		dataType : 'json',
		success : function(result)
				  {
						if (result.length!=0) 
						{
							$('#userNameMsgSpan').html('Username already exist..!');
						} 
						else
						{
							document.getElementById("userNameSpan").style.display = "block";
						}

			      }
		});

}

function CheckOldPassword()
{
	clearAll2();
	
	var OldPassword = "";
	
	if(document.profileform.OldpassWord.value == "")
	{
		document.profileform.OldpassWord.focus();
		$('#OldpassWordMsgSpan').html('Old Password Could Not Be Empty..!');
		return false;
	}
	else
	{
		OldPassword = document.profileform.OldpassWord.value;
	}
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getExistingPassword',
		type : 'Post',
		data : { OldPassword : OldPassword},
		dataType : 'json',
		success : function(result)
				  {
			         if(result.length!=0)
			         {
			        	 if (result[0].passWord == OldPassword) 
						 {
							document.getElementById("OldpassWordSpan").style.display = "block";
						 }
			        	 else
			        	 {
			        		$('#OldpassWordMsgSpan').html('Password does not match with existing password..!'); 
			        	 }
			         }
 					 else
					 {
						 $('#OldpassWordMsgSpan').html('Password does not match with existing password..!');
					 }

			      }
		});
}

function CheckNewPassowrd()
{
	clearAll3();
	
	var NewPassword = "";
	
	if(document.profileform.passWord.value == "")
	{
		document.profileform.passWord.value="";
		document.profileform.passWord.focus();
		$('#passWordMsgSpan').html('New Password can not be empty..!');
		return false;
	}
	else if(!(document.profileform.passWord.value.length>=8))
	{
		document.profileform.passWord.value="";
		document.profileform.passWord.focus();
		$('#passWordMsgSpan').html('Password length must be between 8 to 12 character long ..!');
		return false;
	}
	else if(!document.profileform.passWord.value.match(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{7,15}$/))
	{
		document.profileform.passWord.value="";
		document.profileform.passWord.focus();
		$('#passWordMsgSpan').html('Password should contain At least one upper case letter,one lower case letter, one digit & special symbol..!');
		return false;
	}
	else
	{
		NewPassword = document.profileform.passWord.value;
	}
	
	$.ajax({

		url : '${pageContext.request.contextPath}/CheckNewAndOldPassword',
		type : 'Post',
		data : { NewPassword : NewPassword},
		dataType : 'json',
		success : function(result)
				  {
			         if(result.length!=0)
			         {
			        	 if (result[0].passWord == NewPassword) 
						 {
			        		 $('#passWordMsgSpan').html('New Password cannot be same as existing password..!');
						 }
			        	 else
			        	 {
			        		 document.getElementById("passWordSpan").style.display = "block";			        		 
			        	 }
			         }
 					 else
					 {
 						document.getElementById("passWordSpan").style.display = "block";
					 }

			      }
		});
}

function CheckConfirmPassword()
{
	clearAll4();
	
	var NewPassWord = document.profileform.passWord.value;
	var ConfirmPassword = "";
	
	if(document.profileform.confirmPassword.value == "")
	{
		document.profileform.confirmPassword.value="";
		document.profileform.confirmPassword.focus();
		$('#confirmPasswordMsgSpan').html('Confirm Password can not be empty..!');
		return false;
	}
	else if(NewPassWord != document.profileform.confirmPassword.value)
	{
		document.profileform.confirmPassword.value="";
		document.profileform.confirmPassword.focus();
		$('#confirmPasswordMsgSpan').html('Confirm password does not match with new password..!');
		return false;
	}
	else
	{
		document.getElementById("confirmPasswordSpan").style.display = "block";
	}
}

function updateProfile()
{
  if(document.profileform.OldpassWord.value == "")
  {
	  document.profileform.OldpassWord.value = "";
	  document.profileform.OldpassWord.focus();
	  $('#OldpassWordMsgSpan').html('Old Password Could Not Be Empty..!');
	  return false;
  }
  else if(document.profileform.passWord.value == "")
  {
	  document.profileform.passWord.value="";
	  document.profileform.passWord.focus();
	  $('#passWordMsgSpan').html('New Password can not be empty..!');
	  return false;
  }
  else if(document.profileform.confirmPassword.value == "")
  {
	  document.profileform.confirmPassword.value="";
	  document.profileform.confirmPassword.focus();
	  $('#confirmPasswordMsgSpan').html('Confirm Password can not be empty..!');
	  return false;
  }
	
	
  var userName = document.profileform.userName.value;
  var passWord = document.profileform.passWord.value;
  
  $.ajax({

		url : '${pageContext.request.contextPath}/UpdateProfile',
		type : 'Post',
		data : { userName : userName, passWord : passWord},
		dataType : 'json',
		success : function(result)
				  {
			         if (result[0].userName == userName && result[0].passWord == passWord) 
					 {
			        	 $('#resultSpan').html('Profile updated successfully..!');
			        	 $('#resultSpan').delay(2000).fadeOut();
					 }
			         else
			         {
			        	 $('#resultSpan').html('Failed to update profile..!'); 
			        	 $('#resultSpan').delay(2000).fadeOut();
			         }
			      }
		});
  
  document.profileform.OldpassWord.value = "";
  document.profileform.passWord.value = "";
  document.profileform.confirmPassword.value = "";
  
  clearAll();
  clearAll2();
  clearAll3();
  clearAll4();
  
}

function Clear()
{
	document.profileform.OldpassWord.value = "";
	document.profileform.passWord.value = "";
	document.profileform.confirmPassword.value = "";
	  
	clearAll();
	clearAll2();
	clearAll3();
	clearAll4(); 
}

</script>

</body>
</html>
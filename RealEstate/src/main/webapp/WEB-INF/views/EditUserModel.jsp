<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate |User Model</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


  <!-- Google Font -->
    <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>


<div class="wrapper">

  <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Model Access Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">User Management</a></li>
        <li class="active">User Model Access</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="userModelForm" action="${pageContext.request.contextPath}/EditUserModel" onSubmit="return validate()" method="POST">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <span id="statusSpan" style="color:#FF0000"></span>
              <!-- /.form-group -->
          
           <div class="box-body">
              <div class="row">
                 
                <div class="col-md-2">
                  <label>User Model Id</label>
                  <input type="text" class="form-control" id="userModelId" name="userModelId" value="${userDetail[0].userModelId}" readonly>
                </div>
                  
              </div>
           </div>
          
							
		 <div class="box-body">
              <div class="row">
               
        	    <div class="col-md-3">
        	      <label>Employee Name</label>
        	      <input type="text"  class="form-control" id="employeeName" name="employeeName" value="${userDetail[0].employeeName}">
                  <input type="hidden" class="form-control" id="employeeId" name="employeeId" value="${userDetail[0].employeeId}">
        	    </div>
			     
			     <div class="col-md-3">
                   <label>Assign Role</label>
                   <select class="form-control" id="role" name="role">
                        <option value="Default">-Select Role-</option>
                        <option selected="selected" value="${userDetail[0].role}">${userDetail[0].role}</option>
                         
                         <c:choose>
                    	<c:when test="${userDetail[0].role eq 'Admin'}">
                    		<option selected="selected">Admin</option>
                    	</c:when>
                    	<c:otherwise>
                    		<option>Admin</option>
                    	</c:otherwise>
                    </c:choose>
                    
                    <c:forEach var="departmentList" items="${departmentList}">
                     <c:choose>
                       <c:when test="${departmentList.departmentName ne userDetail[0].role}">
                         <option value="${departmentList.departmentName}">${departmentList.departmentName}</option>
                       </c:when>
                     </c:choose>
				    </c:forEach>
                   </select>
                 </div>
                 
                 <div class="col-md-1">
                 </div>
                 
                 <div class="col-md-3">
                   <label>Project Name</label>
                   <div class="input-group">
					 <div class="input-group-addon">
						<i class="fa fa-archive"></i>
					 </div>
                     <select class="form-control" id="projectId" name="projectId">
                        <option value="Default">-select Project Name-</option>
                        <option value="ALL">ALL</option>
                       <c:forEach var="projectList" items="${projectList}">
                        <option value="${projectList.projectId}">${projectList.projectName}</option>
				       </c:forEach>
                      </select>
                      <span class="input-group-btn">
						 <a onclick="return AddProject()" class="btn btn-success" data-toggle="tooltip" title="Add Project"><i class="glyphicon glyphicon-plus"></i>&nbsp ADD</a>
					  </span>
					</div>
                   <span id="projectIdSpan" style="color:#ff0000"></span>
                 </div>
       
              </div>
              </div>
     
        	<div class="box-body">
        	
              <div class="row">
                
              <div class="col-xs-7">
              <div class="table-responsive">
              <table id="checklisttable" class="table table-bordered">

                  <thead>
	                  <tr bgcolor=#4682B4>
	                  	<th>Sr.No</th>
	                    <th>Module Name</th>
	                    <th>Module Access Rights</th>
	                  </tr>
                  </thead>
                  
                  <tbody>
                      <tr>
	                      <td>1</td>
	                      <td>Administrator Master</td>
	                      
	                      <td>
	                        <c:choose>
	                          <c:when test="${userDetail[0].administrator eq 'OK'}">
	                            <input type="checkbox" name="administrator" id="administrator" value="OK" checked="checked">&nbsp Administrator
	                          </c:when>
	                          <c:otherwise>
	                            <input type="checkbox" name="administrator" id="administrator" value="OK">&nbsp Administrator
	                          </c:otherwise>
	                        </c:choose>
	                      </td>
	                      
	               	  </tr>
	               	  
	               	  <tr>
	                      <td>2</td>
	                      <td>Sales Management</td>
	                      
	                      <td>
	                        <c:choose>
	                          <c:when test="${userDetail[0].preSaleaManagment eq 'OK'}">
	                          <input type="checkbox" name="preSaleaManagment" id="preSaleaManagment" value="OK" checked="checked">&nbsp Pre-Sale Management
	                          </c:when>
	                          <c:otherwise>
	                            <input type="checkbox" name="preSaleaManagment" id="preSaleaManagment" value="OK">&nbsp Pre-Sale Management
	                          </c:otherwise>
	                        </c:choose>
	                     &nbsp &nbsp &nbsp
	                        <c:choose>
	                          <c:when test="${userDetail[0].salesManagment eq 'OK'}">
	                         <input type="checkbox" name="salesManagment" id="salesManagment" value="OK" checked="checked">&nbsp Sales Management
	                          </c:when>
	                          <c:otherwise>
	                         <input type="checkbox" name="salesManagment" id="salesManagment" value="OK">&nbsp Sales Management
	                          </c:otherwise>
	                        </c:choose>
	                      </td>
	                      
	               	  </tr>
	               	  <tr>
	                      <td>3</td>
	                      <td>Reports</td>
	                      <td>
	                      
	                      
	                        <c:choose>
	                          <c:when test="${userDetail[0].flatStatusReport eq 'OK'}">
	                          <input type="checkbox" name="flatStatusReport" id="flatStatusReport" value="OK" checked="checked">&nbsp Flat Status
	                          </c:when>
	                          <c:otherwise>
	                          <input type="checkbox" name="flatStatusReport" id="flatStatusReport" value="OK">&nbsp Flat Status
	                          </c:otherwise>
	                        </c:choose>
	                          &nbsp &nbsp &nbsp
	                        <c:choose>
	                          <c:when test="${userDetail[0].enquiryReport eq 'OK'}">
			                  <input type="checkbox" name="enquiryReport" id="enquiryReport" value="OK" checked="checked">&nbsp Enquiry Report
	                          </c:when>
	                          <c:otherwise>
			                  <input type="checkbox" name="enquiryReport" id="enquiryReport" value="OK">&nbsp Enquiry Report
	                          </c:otherwise>
	                        </c:choose>
	                          &nbsp &nbsp &nbsp
	                        <c:choose>
	                          <c:when test="${userDetail[0].bookingReport eq 'OK'}">
			                  <input type="checkbox" name="bookingReport" id="bookingReport" value="OK" checked="checked">&nbsp Booking Report
	                          </c:when>
	                          <c:otherwise>
			                  <input type="checkbox" name="bookingReport" id="bookingReport" value="OK">&nbsp Booking Report
	                          </c:otherwise>
	                        </c:choose>
	                          &nbsp &nbsp &nbsp
	                        <c:choose>
	                          <c:when test="${userDetail[0].customerPaymentReport eq 'OK'}">
			                  <input type="checkbox" name="customerPaymentReport" id="customerPaymentReport" value="OK" checked="checked">&nbsp Customer Payment Report
	                          </c:when>
	                          <c:otherwise>
			                  <input type="checkbox" name="customerPaymentReport" id="customerPaymentReport" value="OK">&nbsp Customer Payment Report
	                          </c:otherwise>
	                        </c:choose>
	                          &nbsp &nbsp &nbsp
	                        <c:choose>
	                          <c:when test="${userDetail[0].demandPaymentReport eq 'OK'}">
			                  <input type="checkbox" name="demandPaymentReport" id="demandPaymentReport" value="OK" checked="checked">&nbsp Demand Payment Report
	                          </c:when>
	                          <c:otherwise>
			                  <input type="checkbox" name="demandPaymentReport" id="demandPaymentReport" value="OK">&nbsp Demand Payment Report
	                          </c:otherwise>
	                        </c:choose>
	                          &nbsp &nbsp &nbsp
	                        <c:choose>
	                          <c:when test="${userDetail[0].parkingReport eq 'OK'}">
			                  <input type="checkbox" name="parkingReport" id="parkingReport" value="OK" checked="checked">&nbsp Parking Report
	                          </c:when>
	                          <c:otherwise>
			                  <input type="checkbox" name="parkingReport" id="parkingReport" value="OK">&nbsp Parking Report
	                          </c:otherwise>
	                        </c:choose>
	                         &nbsp &nbsp &nbsp 
	                        <c:choose>
	                          <c:when test="${userDetail[0].agentReport eq 'OK'}">
			                  <input type="checkbox" name="agentReport" id="agentReport" value="OK" checked="checked">&nbsp Agent Report
	                          </c:when>
	                          <c:otherwise>
			                  <input type="checkbox" name="agentReport" id="agentReport" value="OK">&nbsp Agent Report
	                          </c:otherwise>
	                        </c:choose>
	                          &nbsp &nbsp &nbsp
	                        <c:choose>
	                          <c:when test="${userDetail[0].customerReport eq 'OK'}">
			                  <input type="checkbox" name="customerReport" id="customerReport" value="OK" checked="checked">&nbsp Customer Report
	                          </c:when>
	                          <c:otherwise>
			                  <input type="checkbox" name="customerReport" id="customerReport" value="OK">&nbsp Customer Report
	                          </c:otherwise>
	                        </c:choose>
	                      
	                      </td>
	               	  </tr>
	               	  <tr>
	                      <td>4</td>
	                      <td>Assign Task</td>
	                      <td>
	                      
	                        <c:choose>
	                          <c:when test="${userDetail[0].addTask eq 'OK'}">
			                 <input type="checkbox" name="addTask" id="addTask" value="OK" checked="checked">&nbsp Add Task
	                          </c:when>
	                          <c:otherwise>
			                 <input type="checkbox" name="addTask" id="addTask" value="OK">&nbsp Add Task
	                          </c:otherwise>
	                        </c:choose>
	                          
	                      </td>
	               	  </tr>
	               	  
	               	  <tr>
	                      <td>5</td>
	                      <td>HR & PayRoll Management</td>
	                      <td>
	                        <c:choose>
	                          <c:when test="${userDetail[0].hrANDpayRollManagement eq 'OK'}"> 
	                          <input type="checkbox" name="hrANDpayRollManagement" id="hrANDpayRollManagement" value="OK" checked="checked">&nbsp HR AND PayRoll Management
	                          </c:when>
	                          <c:otherwise> 
	                          <input type="checkbox" name="hrANDpayRollManagement" id="hrANDpayRollManagement" value="OK">&nbsp HR AND PayRoll Management
	                          </c:otherwise>
	                        </c:choose>
	                         
	                      </td>
	               	  </tr>
	               	  
	               	  <tr>
	                      <td>6</td>
	                      <td>Architectural Section</td>
	                      <td>
	                        <c:choose>
	                          <c:when test="${userDetail[0].consultancyMaster eq 'OK'}"> 
			                  <input type="checkbox" name="consultancyMaster" id="consultancyMaster" value="OK" checked="checked">&nbsp Consultancy Master
	                          </c:when>
	                          <c:otherwise> 
			                  <input type="checkbox" name="consultancyMaster" id="consultancyMaster" value="OK">&nbsp Consultancy Master
	                          </c:otherwise>
	                        </c:choose>
	                         
			                    &nbsp &nbsp &nbsp
			                    
	                        <c:choose>
	                          <c:when test="${userDetail[0].architecturalSection eq 'OK'}"> 
			                  <input type="checkbox" name="architecturalSection" id="architecturalSection" value="OK" checked="checked">&nbsp Architectural Section
	                          </c:when>
	                          <c:otherwise> 
			                  <input type="checkbox" name="architecturalSection" id="architecturalSection" value="OK">&nbsp Architectural Section
	                          </c:otherwise>
	                        </c:choose>
	                         
	                      </td>
	               	  </tr>
	               	  
	               	  <tr>
	                      <td>7</td>
	                      <td>Engineering Management</td>
	                      <td>
	                      
	                        <c:choose>
	                          <c:when test="${userDetail[0].contractorMagagement eq 'OK'}"> 
			                  <input type="checkbox" name="contractorMagagement" id="contractorMagagement" value="OK" checked="checked">&nbsp Contractor Management
	                          </c:when>
	                          <c:otherwise> 
			                  <input type="checkbox" name="contractorMagagement" id="contractorMagagement" value="OK">&nbsp Contractor Management
	                          </c:otherwise>
	                        </c:choose>
	                           &nbsp &nbsp &nbsp
	                        <c:choose>
	                          <c:when test="${userDetail[0].projectEngineering eq 'OK'}"> 
			                  <input type="checkbox" name="projectEngineering" id="projectEngineering" value="OK" checked="checked">&nbsp Project Engineering
	                          </c:when>
	                          <c:otherwise> 
			                  <input type="checkbox" name="projectEngineering" id="projectEngineering" value="OK">&nbsp Project Engineering
	                          </c:otherwise>
	                        </c:choose>
	                           &nbsp &nbsp &nbsp
	                        <c:choose>
	                          <c:when test="${userDetail[0].jrEngineering eq 'OK'}"> 
			                  <input type="checkbox" name="jrEngineering" id="jrEngineering" value="OK" checked="checked">&nbsp Jr.Engineering
	                          </c:when>
	                          <c:otherwise> 
			                  <input type="checkbox" name="jrEngineering" id="jrEngineering" value="OK">&nbsp Jr.Engineering
	                          </c:otherwise>
	                        </c:choose>
	                           &nbsp &nbsp &nbsp
	                        <c:choose>
	                          <c:when test="${userDetail[0].qualityEngineering eq 'OK'}"> 
			                  <input type="checkbox" name="qualityEngineering" id="qualityEngineering" value="OK" checked="checked">&nbsp Quality Engineering
	                          </c:when>
	                          <c:otherwise> 
			                  <input type="checkbox" name="qualityEngineering" id="qualityEngineering" value="OK">&nbsp Quality Engineering
	                          </c:otherwise>
	                        </c:choose>
	                           &nbsp &nbsp &nbsp
	                        <c:choose>
	                          <c:when test="${userDetail[0].srSupervisor eq 'OK'}"> 
			                  <input type="checkbox" name=srSupervisor id="srSupervisor" value="OK" checked="checked">&nbsp Sr.Supervisor
	                          </c:when>
	                          <c:otherwise> 
			                  <input type="checkbox" name=srSupervisor id="srSupervisor" value="OK">&nbsp Sr.Supervisor
	                          </c:otherwise>
	                        </c:choose>
	                           &nbsp &nbsp &nbsp
	                        <c:choose>
	                          <c:when test="${userDetail[0].jrSupervisor eq 'OK'}"> 
			                  <input type="checkbox" name="jrSupervisor" id="jrSupervisor" value="OK" checked="checked">&nbsp Jr.Supervisor
	                          </c:when>
	                          <c:otherwise> 
			                  <input type="checkbox" name="jrSupervisor" id="jrSupervisor" value="OK">&nbsp Jr.Supervisor
	                          </c:otherwise>
	                        </c:choose>
	                         
	                      </td>
	               	  </tr>
	               	  <tr>
	                      <td>8</td>
	                      <td>Purchase Management</td>
	                      <td>
	                        <c:choose>
	                          <c:when test="${userDetail[0].materialRequisitionList eq 'OK'}"> 
	                          <input type="checkbox" name="materialRequisitionList" id="materialRequisitionList" value="OK" checked="checked">&nbsp Material Requisition List
	                          </c:when>
	                          <c:otherwise> 
	                          <input type="checkbox" name="materialRequisitionList" id="materialRequisitionList" value="OK">&nbsp Material Requisition List
	                          </c:otherwise>
	                        </c:choose>
	                        
	                      </td>
	               	  </tr>
	               	  <tr>
	                      <td>9</td>
	                      <td>Site Management</td>
	                      <td>
	                      
	                        <c:choose>
	                          <c:when test="${userDetail[0].storeManagement eq 'OK'}"> 
			                  <input type="checkbox" name="storeManagement" id="storeManagement" value="OK" checked="checked">&nbsp Store Management
	                          </c:when>
	                          <c:otherwise> 
			                  <input type="checkbox" name="storeManagement" id="storeManagement" value="OK">&nbsp Store Management
	                          </c:otherwise>
	                        </c:choose>
	                         &nbsp &nbsp &nbsp
	                        <c:choose>
	                          <c:when test="${userDetail[0].storeStocks eq 'OK'}"> 
			                  <input type="checkbox" name="storeStocks" id="storeStocks" value="OK" checked="checked">&nbsp Store Stocks
	                          </c:when>
	                          <c:otherwise> 
			                  <input type="checkbox" name="storeStocks" id="storeStocks" value="OK">&nbsp Store Stocks
	                          </c:otherwise>
	                        </c:choose>
	                         &nbsp &nbsp &nbsp
	                        <c:choose>
	                          <c:when test="${userDetail[0].materialTransferToContractor eq 'OK'}"> 
			                  <input type="checkbox" name="materialTransferToContractor" id="materialTransferToContractor" value="OK" checked="checked">&nbsp Material Transfer to contractor
	                          </c:when>
	                          <c:otherwise> 
			                  <input type="checkbox" name="materialTransferToContractor" id="materialTransferToContractor" value="OK">&nbsp Material Transfer to contractor
	                          </c:otherwise>
	                        </c:choose>
	                        
	                      </td>
	               	  </tr>
	               	  <tr>
	                      <td>10</td>
	                      <td>Account Management</td>
	                      <td>
	                      
	                        <c:choose>
	                          <c:when test="${userDetail[0].customerPaymentStatus eq 'OK'}"> 
	                          <input type="checkbox" name="customerPaymentStatus" id="customerPaymentStatus" value="OK" checked="checked">&nbsp Customer Payment Status
	                          </c:when>
	                          <c:otherwise> 
	                          <input type="checkbox" name="customerPaymentStatus" id="customerPaymentStatus" value="OK">&nbsp Customer Payment Status
	                          </c:otherwise>
	                        </c:choose>
	                          &nbsp &nbsp &nbsp
	                        <c:choose>
	                          <c:when test="${userDetail[0].supplierPaymentBill eq 'OK'}"> 
		                   	  <input type="checkbox" name="supplierPaymentBill" id="supplierPaymentBill" value="OK" checked="checked">&nbsp Supplier Payment Bill
	                          </c:when>
	                          <c:otherwise> 
		                   	  <input type="checkbox" name="supplierPaymentBill" id="supplierPaymentBill" value="OK">&nbsp Supplier Payment Bill
	                          </c:otherwise>
	                        </c:choose>
	                        
	                          &nbsp &nbsp &nbsp
	                        <c:choose>
	                          <c:when test="${userDetail[0].agentPayment eq 'OK'}"> 
		                   	  <input type="checkbox" name="agentPayment" id="agentPayment" value="OK" checked="checked">&nbsp Agent payment
	                          </c:when>
	                          <c:otherwise> 
		                   	  <input type="checkbox" name="agentPayment" id="agentPayment" value="OK">&nbsp Agent payment
	                          </c:otherwise>
	                        </c:choose>
	                        
	                      </td>
	               	   </tr>
                       <tr>
	                      <td>11</td>
	                      <td>Rera</td>
	                      <td>
	                      
	                        <c:choose>
	                          <c:when test="${userDetail[0].projectDevelopmentWork eq 'OK'}"> 
	                          <input type="checkbox" name="projectDevelopmentWork" id="projectDevelopmentWork" value="OK" checked="checked">&nbsp Project Development Work
	                          </c:when>
	                          <c:otherwise> 
		                   	  <input type="checkbox" name="projectDevelopmentWork" id="projectDevelopmentWork" value="OK">&nbsp Project Development Work
	                          </c:otherwise>
	                        </c:choose>
	                        
	                          &nbsp &nbsp &nbsp
	                        <c:choose>
	                          <c:when test="${userDetail[0].architectsCertificate eq 'OK'}"> 
		                   	  <input type="checkbox" name="architectsCertificate" id="architectsCertificate" value="OK" checked="checked">&nbsp Architect's Certificate
	                          </c:when>
	                          <c:otherwise> 
		                   	  <input type="checkbox" name="architectsCertificate" id="architectsCertificate" value="OK">&nbsp Architect's Certificate
	                          </c:otherwise>
	                        </c:choose>
	                        
	                          &nbsp &nbsp &nbsp
	                        <c:choose>
	                          <c:when test="${userDetail[0].projectSpecificationMaster eq 'OK'}"> 
		                   	  <input type="checkbox" name="projectSpecificationMaster" id="projectSpecificationMaster" value="OK" checked="checked">&nbsp Project Specification Master
	                          </c:when>
	                          <c:otherwise> 
		                   	  <input type="checkbox" name="projectSpecificationMaster" id="projectSpecificationMaster" value="OK">&nbsp Project Specification Master
	                          </c:otherwise>
	                        </c:choose>
		                   	  
	                      </td>
	               	    </tr>
	               	    <tr>
	                      <td>12</td>
	                      <td>User Management</td>
	                      <td>
	                      
	                        <c:choose>
	                          <c:when test="${userDetail[0].userManagement eq 'OK'}"> 
	                          <input type="checkbox" name="userManagement" id="userManagement" value="OK" checked="checked">&nbsp User Management
	                          </c:when>
	                          <c:otherwise> 
	                          <input type="checkbox" name="userManagement" id="userManagement" value="OK">&nbsp User Management
	                          </c:otherwise>
	                        </c:choose>
		                   	  
	                      </td>
	               	    </tr>
	               	    <tr>
	                      <td>13</td>
	                      <td>Licensing Department</td>
	                      <td>
	                      
	                        <c:choose>
	                          <c:when test="${userDetail[0].licensingDepartment eq 'OK'}"> 
	                          <input type="checkbox" name="licensingDepartment" id="licensingDepartment" value="OK" checked="checked">&nbsp Licensing Department
	                          </c:when>
	                          <c:otherwise> 
	                          <input type="checkbox" name="licensingDepartment" id="licensingDepartment" value="OK">&nbsp Licensing Department
	                          </c:otherwise>
	                        </c:choose>
		                   	  
	                      </td>
	               	    </tr>
	               	    <tr>
	                      <td>14</td>
	                      <td>Documentation</td>
	                      <td>
	                      
	                        <c:choose>
	                          <c:when test="${userDetail[0].companyDocuments eq 'OK'}"> 
		                   	  <input type="checkbox" name="companyDocuments" id="companyDocuments" value="OK" checked="checked">&nbsp Company Documents
	                          </c:when>
	                          <c:otherwise> 
		                   	  <input type="checkbox" name="companyDocuments" id="companyDocuments" value="OK">&nbsp Company Documents
	                          </c:otherwise>
	                        </c:choose>
		                   	  
		                   	    &nbsp &nbsp &nbsp
	                        <c:choose>
	                          <c:when test="${userDetail[0].reraDocuments eq 'OK'}"> 
		                   	  <input type="checkbox" name="reraDocuments" id="reraDocuments" value="OK" checked="checked">&nbsp Rera Documents
	                          </c:when>
	                          <c:otherwise> 
		                   	  <input type="checkbox" name="reraDocuments" id="reraDocuments" value="OK">&nbsp Rera Documents
	                          </c:otherwise>
	                        </c:choose>
		                   	  
	                      </td>
	               	    </tr>
                 </tbody>
                  
                  
                </table>
                </div>
                </div>
                
                <div class="col-xs-5">
              <div class="table-responsive">

               <table id="userProjectListTable" class="table table-bordered">
                  <thead>
                  
	                  <tr bgcolor=#4682B4>
	                  	<th>Sr.No.</th>
	                    <th>Project Name</th>
	                    <th>Action</th>
	                  </tr>
	                  
                  </thead>

                  <tbody>
                  
                    <s:forEach items="${assignedProjectList}" var="assignedProjectList" varStatus="loopStatus">
					  <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
						<td>${loopStatus.index+1}</td>
						<td>${assignedProjectList.projectName}</td>
						<td>
						   <a onclick="DeleteAssignedProject('${assignedProjectList.projectassignId}')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete Project"><i class="glyphicon glyphicon-remove"></i></a>
						</td>
					  </tr>
					</s:forEach>
	               	  
                 </tbody>
                </table>
                 <span id="assignProjectListSpan" style="color:#ff0000"></span>
                </div>
              </div>
                
              </div>
            </div>
        	
            <div class="panel box box-danger"></div>
           
            <div class="box-body">
            <div class="row">
            
                <input type="hidden" id="status" name="status" value="Active">   
				<input type="hidden" id="creationDate" name="creationDate" value="${userDetail[0].creationDate}">
				<input type="hidden" id="updateDate" name="updateDate" value="">
				<input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>">
				<input type="hidden" id="userStatus" name="userStatus" value="${userStatus}">
              <!-- /.form-group -->
            </div>
            </div>
             </div>
    
            
            <!-- /.col -->
			
          </div>
          
		   	 <div class="box-body">
              <div class="row">
                  <div class="col-xs-4">
                  <div class="col-xs-2">
                	<a href="UserMaster"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
			       </div>
			       </div>
				  <div class="col-xs-3">
                   <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
   			      </div>
				 <div class="col-xs-2">
			       <button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
			     </div> 
              </div>
			</div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        
      </div>
    
      </section>
	</form>
    <!-- /.content -->
    
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    
<script>

function clearAll()
{
	$('#employeeIdSpan').html('');
	$('#roleSpan').html('');
	$('#accessMenuSpan').html('');
} 

function validate()
{ 
	 clearAll();
	
	 if(document.userModelForm.employeeId.value=="Default")
	 {
			$('#employeeIdSpan').html('Please, select employee name..!');
			document.userModelForm.employeeId.focus();
			return false;
	 }
	
	 if(document.userModelForm.role.value=="Default")
	 {
			$('#roleSpan').html('Please, select role..!');
			document.userModelForm.role.focus();
			return false;
	 }
	 
}

function init()
{
	//clearAll()
	 
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	 document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	 if(document.userModelForm.userStatus.value == "Fail")
	 {
		 $('#statusSpan').html('Failed to add user..!');
	 }
	 
  	 document.userModelForm.employeeId.focus();
}

	function AddProject()
	{
		  $('#employeeIdSpan').html('');
		  $('#projectIdSpan').html('');
	
		  var userModelId = $('#userModelId').val();
		  var employeeId = $('#employeeId').val();
		  var projectId = $('#projectId').val();
		  
		  if(employeeId == "Default")	  
		  {
			  $('#employeeIdSpan').html('Please, select employee name..!');
			  document.userModelForm.employeeId.focus(); 
			  return false;  
		  }
		  
		  if(projectId == "Default")
		  {
			 $('#projectIdSpan').html('Please, select project name..!');
			 document.userModelForm.projectId.focus(); 
			 return false;
		  }
		  
		  $("#userProjectListTable tr").detach();
		  $.ajax({
	
				url : '${pageContext.request.contextPath}/AssignProject',
				type : 'Post',
				data : { userModelId : userModelId, projectId : projectId, employeeId : employeeId },
				dataType : 'json',
				success : function(result)
						  {
								        $('#userProjectListTable').append('<tr style="background-color: #4682B4;"><th>Sr.No.</th> <th>Project Name</th><th>Action</th></tr>');
								
								for(var i=0;i<result.length;i++)
								{ 
									if(i%2==0)
									{
										var id = result[i].projectassignId;
										$('#userProjectListTable').append('<tr style="background-color: #F0F8FF;"><td>'+(i+1)+'</td><td>'+result[i].projectName+'</td><td><a onclick="DeleteAssignedProject('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td></tr>');
									}
									else
									{
										var id = result[i].projectassignId;
										$('#userProjectListTable').append('<tr style="background-color: #CCE5FF;"><td>'+(i+1)+'</td><td>'+result[i].projectName+'</td><td><a onclick="DeleteAssignedProject('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td></tr>');
									}
								
								 }
						  }
				});
	}
	
	function DeleteAssignedProject(projectassignId)
	{
		  var userModelId = $('#userModelId').val();
		  
		  $("#userProjectListTable tr").detach();
		  $.ajax({
	
				url : '${pageContext.request.contextPath}/DeleteAssignedProject',
				type : 'Post',
				data : { projectassignId : projectassignId, userModelId : userModelId },
				dataType : 'json',
				success : function(result)
						  {
								        $('#userProjectListTable').append('<tr style="background-color: #4682B4;"><th>Sr.No.</th> <th>Project Name</th><th>Action</th></tr>');
								
								for(var i=0;i<result.length;i++)
								{ 
									if(i%2==0)
									{
										var id = result[i].projectassignId;
										$('#userProjectListTable').append('<tr style="background-color: #F0F8FF;"><td>'+(i+1)+'</td><td>'+result[i].projectId+'</td><td><a onclick="DeleteAssignedProject('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td></tr>');
									}
									else
									{
										var id = result[i].projectassignId;
										$('#userProjectListTable').append('<tr style="background-color: #CCE5FF;"><td>'+(i+1)+'</td><td>'+result[i].projectId+'</td><td><a onclick="DeleteAssignedProject('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td></tr>');
									}
								
								 }
						  }
				});
		  
	}

</script>
</body>
</html>

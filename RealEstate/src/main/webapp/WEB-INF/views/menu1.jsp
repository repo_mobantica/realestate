<!-- Left side column. contains the logo and sidebar -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
.responsive {
	max-width: auto;
	height: auto;
}
</style>
</head>

<aside class="main-sidebar">

	<section class="sidebar">

		<img
			src="${pageContext.request.contextPath}/resources/dist/img/loginlogo.png"
			alt="Nature" class="responsive">
			
	</section>
</aside>

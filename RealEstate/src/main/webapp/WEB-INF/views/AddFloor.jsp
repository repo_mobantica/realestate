<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate |Add Floor</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
     <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

  <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Floor Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Add Floor</li>
      </ol>
    </section>

    <!-- Main content -->
    
<form name="floorform" action="${pageContext.request.contextPath}/AddFloor" method="Post" onSubmit="return validate()">
    <section class="content">
      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
                  <span id="statusSpan" style="color:#FF0000"></span>
              <!-- /.form-group -->
             <div class="box-body">
          <div class="row">
            <div class="col-md-3">
                  <label >Floor Id</label>
                  <input type="text" class="form-control"  id="floorId" name="floorId"  value="${floorCode}" readonly>
                </div>
			</div>
			</div>
											
		 <div class="box-body">
          <div class="row">
            <div class="col-md-3">
                  <label>Project</label><label class="text-red">* </label>
                  <select class="form-control" name="projectId" id="projectId" onchange="getBuldingList(this.value)">
                  	 <option selected="" value="Default">-Select a Project-</option>
                   	  <c:forEach var="projectList" items="${projectList}">
                    	<option value="${projectList.projectId}">${projectList.projectName}</option>
				     </c:forEach>
				  </select>
				      <span id="projectIdSpan" style="color:#FF0000"></span>
                  </div>
                    <div class="col-md-3">
                  <label>Project Building</label><label class="text-red">* </label>                  
                  <select class="form-control" name="buildingId" id="buildingId" onchange="getWingList(this.value)">
				  		<option selected="" value="Default">-Select Project Building-</option>
                     <c:forEach var="projectbuildingList" items="${projectbuildingList}">
                    	<option value="${projectbuildingList.buildingId}">${projectbuildingList.buildingName}</option>
				     </c:forEach>
                  </select>
                      <span id="buildingIdSpan" style="color:#FF0000"></span>
                  </div>
                  
                  <div class="col-md-3">
                  <label>Project Wing</label><label class="text-red">* </label>                  
                  <select class="form-control" name="wingId" id="wingId" onchange="getFloorList(this.value)">
				  		<option selected="" value="Default">-Select Project Wing-</option>
                     <c:forEach var="wingList" items="${wingList}">
                    	<option value="${wingList.wingId}">${wingList.wingName}</option>
				     </c:forEach>
                  </select>
                      <span id="wingIdSpan" style="color:#FF0000"></span>
                  </div>
                  
                  <div class="col-md-3">
                     <label>Floor Name </label><label class="text-red">* </label>
                     <select class="form-control" name="floortypeName" id="floortypeName" onchange="getuniqueFloorList(this.value)">
				 	 	<option selected="" value="Default">-Select Floor Type-</option>
                      <c:forEach var="floortypeList" items="${floortypeList}">
                    	<option value="${floortypeList.floortypeName}">${floortypeList.floortypeName}</option>
				      </c:forEach>
                    </select>
                      <span id="floortypeNameSpan" style="color:#FF0000"></span>
                </div>
                  
				</div>
                </div>
				 
                
                 </div>
            <!-- /.col -->
            <div class="col-md-12">
            <!-- For space -->
				
			 <div class="box-body">
              <div class="row">
                 
                 <div class="col-md-3" id="floorRise">
		            <label for="floorRise">Floor Rise</label><label class="text-red">* </label>
		            <input type="text" class="form-control" name="floorRise" id="floorRise" placeholder="Enter Floor Rise">
		            <span id="floorRiseSpan" style="color:#FF0000"></span>
	             </div>
                 
                 <div class="col-xs-6">
			   		<label for="radiobutton">Floor Zone Type</label><label class="text-red">* </label> <span id="floorzoneTypeSpan" style="color:#FF0000"></span></br>
                	<input type="radio"  name="floorzoneType" id="floorzoneType" value="Parking" onclick = "addparking()" > Parking
					&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
  					<input type="radio"name="floorzoneType" id="floorzoneType" value="Shops"  onclick = "addshop()"> Shops
                   	&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
                	<input type="radio" name="floorzoneType" id="floorzoneType" value="Flats"  onclick = "addflat()"> Flats
                 </div> 
				 
			 </div>
            </div>	
			
		<div class="box-body">
          <div class="row">
            <div class="col-md-3" id="numberofOpenParking1">
	            <label for="numberofOpenParking">No. of Open Parking </label><label class="text-red">* </label>
	            <input type="text" class="form-control" name="numberofOpenParking" id="numberofOpenParking" placeholder="Enter No of Open Parking ">
	            <span id="numberofOpenParkingSpan" style="color:#FF0000"></span>
	        </div>
            
            <div class="col-md-3" id="numberofCloseParking1">
	            <label for="numberofCloseParking">No. of Close Parking </label><label class="text-red">* </label>
	            <input type="text" class="form-control" name="numberofCloseParking" id="numberofCloseParking" placeholder="Enter No of Close Parking ">
	            <span id="numberofCloseParkingSpan" style="color:#FF0000"></span>
	        </div>
            
            <div class="col-md-3" id="numberofShops1">
                  <label for="numberofShops">No. of Shops </label><label class="text-red">* </label>
                  <input type="text" class="form-control" name="numberofShops" id="numberofShops" placeholder="Enter No of Shops ">
                  <span id="numberofShopsSpan" style="color:#FF0000"></span>
	        </div>
	            
	        <div class="col-md-3" id="numberofFlats1">
	               <label for="numberofFlats">No. of Flats </label><label class="text-red">* </label>
	               <input type="text" class="form-control" name="numberofFlats" id="numberofFlats" placeholder="Enter No of Flats ">
	               <span id="numberofFlatsSpan" style="color:#FF0000"></span>
	        </div>
            
		 </div>
		</div>	
				
              <!-- /.form-group -->
            </div>
              <input type="hidden" id="floorStatus" name="floorStatus" value="${floorStatus}">	
			  <input type="hidden" id="creationDate" name="creationDate" value="">
			  <input type="hidden" id="updateDate" name="updateDate" value="">
			  <input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>">
			  
            <!-- /.col -->
			
          </div>
		  	     <div class="box-body">
              <div class="row">
                 <div class="col-xs-5">
                   <div class="col-xs-2">
		                <a href="FloorMaster"><button type="button" class="btn btn-block btn-primary" style="width:90px">Back</button></a>
		           </div>
			     </div>
			     
				  <div class="col-xs-4">
                		<button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
			      </div>
			      
				  <div class="col-xs-2">
			       		<button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
			      </div>
			       
              </div>
			 </div>
          <!-- /.row -->
        </div>
      </div>
    </section>
    </form>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
    </script>
<script>
function addparking()
{
	document.getElementById("numberofFlats").disabled = true;	
	document.getElementById("numberofShops").disabled = true; 
	
	document.getElementById("numberofOpenParking").disabled = false;
	document.getElementById("numberofCloseParking").disabled = false;
	
	document.getElementById("numberofFlats").value = "";	
	document.getElementById("numberofShops").value = "";
}

function addshop()
{
	document.getElementById("numberofShops").disabled = false;
	
	document.getElementById("numberofFlats").disabled = true;
	document.getElementById("numberofOpenParking").disabled = true;
	document.getElementById("numberofCloseParking").disabled = true;
	
	document.getElementById("numberofFlats").value = "";
	document.getElementById("numberofOpenParking").value ="";
	document.getElementById("numberofCloseParking").value ="";
}

function addflat()
{
   document.getElementById("numberofFlats").disabled = false;
   
   document.getElementById("numberofShops").disabled = true;
   document.getElementById("numberofOpenParking").disabled = true;
   document.getElementById("numberofCloseParking").disabled = true;

   document.getElementById("numberofShops").value = "";
   document.getElementById("numberofOpenParking").value = "";
   document.getElementById("numberofCloseParking").value = "";
}

function clearall()
{
	$('#projectIdSpan').html('');
	$('#buildingIdSpan').html('');
	$('#wingIdSpan').html('');
	$('#floortypeNameSpan').html('');
	$('#floorRiseSpan').html('');
	$('#floorzoneTypeSpan').html('');
	$('#numberofShopsSpan').html('');
	$('#numberofFlatsSpan').html('');
	$('#numberofOpenParkingSpan').html('');
	$('#numberofCloseParkingSpan').html('');
}

function validate()
{ 
	 clearall();
		//validation for project name
	//	alert('hi');
		if(document.floorform.projectId.value=="Default")
		{
			$('#projectIdSpan').html('Please, Select project name..!');
			document.floorform.projectId.focus();
			return false;
		}
		
		//validation for project building
		if(document.floorform.buildingId.value=="Default")
		{
			$('#buildingIdSpan').html('Please, select building name..!');
			document.floorform.buildingId.focus();
			return false;
		}
		//validation for project building
		if(document.floorform.wingId.value=="Default")
		{
			$('#wingIdSpan').html('Please, select wing name..!');
			document.floorform.wingId.focus();
			return false;
		}
		//validation for floor type name
		if(document.floorform.floortypeName.value=="Default")
		{
			$('#floortypeNameSpan').html('Please, select floor type..!');
			document.floorform.floortypeName.focus();
			return false;
		}
		
		if(document.floorform.floorRise.value=="")
		{
			$('#floorRiseSpan').html('Please, enter floor rise..!');
			document.floorform.floorRise.focus();
			return false;
		}
		else if(document.floorform.floorRise.value.match(/^[\s]+$/))
		{
			$('#floorRiseSpan').html('Please, enter floor rise..!');
			document.floorform.floorRise.value="";
			document.floorform.floorRise.focus();
			return false; 	
		}
		else if(!document.floorform.floorRise.value.match(/^[0-9]+$/))
		{
			$('#floorRiseSpan').html('Please, use only digit value for floor rise..! eg: 30');
			document.floorform.floorRise.value="";
			document.floorform.floorRise.focus();
			return false;			
		}
		
		if(( document.floorform.floorzoneType[0].checked == false ) && ( document.floorform.floorzoneType[1].checked == false ) && ( document.floorform.floorzoneType[2].checked == false ) )
		{
	    	$('#floorzoneTypeSpan').html('Please, choose your floor zone type..!');
			document.floorform.floorzoneType[0].focus();
			return false;
		}
	    else if(document.floorform.floorzoneType[0].checked == true)
	    {
	    	if(document.floorform.numberofOpenParking.value=="")
			{
				$('#numberofOpenParkingSpan').html('Please, enter number of open parking..!');
				document.floorform.numberofOpenParking.focus();
				return false;
			}
			else if(!document.floorform.numberofOpenParking.value.match(/^[0-9]+$/))
			{
				$('#numberofOpenParkingSpan').html('Number of parking should be numbers only..!');
				document.floorform.numberofOpenParking.value="";
				document.floorform.numberofOpenParking.focus();
				return false;
			}
	    	
	    	if(document.floorform.numberofCloseParking.value=="")
			{
				$('#numberofCloseParkingSpan').html('Please, enter number of close parking..!');
				document.floorform.numberofCloseParking.focus();
				return false;
			}
			else if(!document.floorform.numberofCloseParking.value.match(/^[0-9]+$/))
			{
				$('#numberofCloseParkingSpan').html('Number of parking should be numbers only..!');
				document.floorform.numberofCloseParking.value="";
				document.floorform.numberofCloseParking.focus();
				return false;
			}
	    	
	    }
	 	else if( document.floorform.floorzoneType[1].checked == true )
		{
				if(document.floorform.numberofShops.value=="")
				{
					$('#numberofShopsSpan').html('Please, enter number of shop..!');
					document.floorform.numberofShops.focus();
					return false;
				}
				else if(!document.floorform.numberofShops.value.match(/^[0-9]+$/))
				{
					$('#numberofShopsSpan').html('Number of shop should be digits only..!');
					document.floorform.numberofShops.value="";
					document.floorform.numberofShops.focus();
					return false;
				}
		 }
	 	 else if( document.floorform.floorzoneType[2].checked == true )
		 {
			if(document.floorform.numberofFlats.value=="")
			{
				$('#numberofFlatsSpan').html('Please, enter number of flats..!');
				document.floorform.numberofFlats.focus();
				return false;
			}
			else if(!document.floorform.numberofFlats.value.match(/^[0-9]+$/))
			{
				$('#numberofFlatsSpan').html('Number of flats should be digits only..!');
				document.floorform.numberofFlats.value="";
				document.floorform.numberofFlats.focus();
				return false;
			}
		 } 
		
}
function init()
{
	 clearall();
	 var date =  new Date();
		var year = date.getFullYear();
		var month = date.getMonth() + 1;
		var day = date.getDate();
		
		document.getElementById("creationDate").value = day + "/" + month + "/" + year;
		document.getElementById("updateDate").value = day + "/" + month + "/" + year;
		
		
	 
	 if(document.floorform.floorStatus.value == "Fail")
	 {
	  	//alert("Sorry, record is present already..!");
	 }
	 else if(document.floorform.floorStatus.value == "Success")
	 {
		 $('#statusSpan').html('Record added successfully..!');
	 }
    
	 document.floorform.projectId.focus();
}


function getBuldingList()
{
	 $("#buildingId").empty();
	 var projectId = $('#projectId').val();

	 $.ajax({

		url : '${pageContext.request.contextPath}/getBuildingList',
		type : 'Post',
		data : { projectId : projectId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Building Name-");
							$("#buildingId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].buildingId).text(result[i].buildingName);
							    $("#buildingId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get Building List


function getWingList()
{
	 $("#wingId").empty();
	 var buildingId = $('#buildingId').val();
	 var projectId = $('#projectId').val();
	 $.ajax({

		url : '${pageContext.request.contextPath}/getWingList',
		type : 'Post',
		data : { buildingId : buildingId, projectId : projectId },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Wing Name-");
							$("#wingId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].wingId).text(result[i].wingName);
							    $("#wingId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get Floor Type List

function getFloorList()
{
	 $("#floortypeName").empty();
	 var wingId = $('#wingId').val();
	 var buildingId = $('#buildingId').val();
	 var projectId = $('#projectId').val();
	 
	 var noofBasement;
	 var noofStilts;
	 var noofPodiums;
	 var noofFloors;
	 var total;
	 
	 $.ajax({

		url : '${pageContext.request.contextPath}/getFloorList',
		type : 'Post',
		data : { buildingId : buildingId, projectId : projectId ,wingId : wingId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Floor Name-");
							$("#floortypeName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								noofBasement = result[i].noofBasement;
								noofStilts = result[i].noofStilts;
								noofPodiums = result[i].noofPodiums;
								noofFloors = result[i].noofFloors;
								total = result[i].total;
							 } 
						
							for(var i=1;i<=noofBasement;i++)
							{
								var option = $('<option />');
							    option.attr('value',i+"st Basement").text(i+"st Basement");
							    $("#floortypeName").append(option);
							 }
							for(var i=1;i<=noofStilts;i++)
							{
								var option = $('<option />');
							    option.attr('value',i+"st Stils").text(i+"st Stils");
							    $("#floortypeName").append(option);
							 }
							for(var i=1;i<=noofPodiums;i++)
							{
								var option = $('<option />');
							    option.attr('value',"P"+i).text("p"+i);
							    $("#floortypeName").append(option);
							 }
							
								for(var i=1;i<=noofFloors;i++)
								{
									
									
									var option = $('<option />');
									if(i==1||i==21||i==31||i==41||i==51||i==61||i==81||i==91)
										{
										  option.attr('value',i+"st Floor").text(i+"st Floor");
										}
									else if(i==3||i==23||i==33||i==43||i==53||i==73||i==83||i==93)
										{
										 option.attr('value',i+"rd Floor").text(i+"rd Floor");
										}
									else if(i==2||i==22||i==32||i==42||i==52||i==62||i==72||i==82||i==92)
										{
										 option.attr('value',i+"nd Floor").text(i+"nd Floor");
										}
									else
										{
										 option.attr('value',i+"th Floor").text(i+"th Floor");
										}
									  $("#floortypeName").append(option);
								 }
							 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
}

function getuniqueFloorList()
{
	 var floortypeName = $('#floortypeName').val();
	 var wingId = $('#wingId').val();
	 var buildingId = $('#buildingId').val();
	 var projectId = $('#projectId').val();
			$.ajax({

			url : '${pageContext.request.contextPath}/getuniquefloorList',
			type : 'Post',
		//	data : { floortypeName : floortypeName, buildingId : buildingId, projectId : projectId ,wingId : wingId},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{
								 
								for(var i=0;i<result.length;i++)
								{
									if(result[i].projectId==projectId)
										{
										if(result[i].buildingId==buildingId)
											{
												if(result[i].wingId==wingId)
												{
													if(result[i].floortypeName==floortypeName)
														{
												  			document.floorform.floortypeName.focus();
															$('#floortypeNameSpan').html('This floor is already exist..!');
															getFloorList();
															  // $('#stateName').val("");
														}
													else
														{
														$('#floortypeNameSpan').html('');
														}
												}
											}
										}
									
									
								 } 
							} 
							else
							{
								alert("failure111");
								//$("#ajax_div").hide();
							}

						}
			});	
}


$(function () 
  {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Location Area Master</title>
  <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
 
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
    	
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

     <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Location Area Master Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Location Area Master</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="locationareamasterform" action="${pageContext.request.contextPath}/LocationAreaMaster" method="post">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
        
          <div class="box-body">
          <div class="row">
            <div class="col-md-3">
                  <label>Country</label> <label class="text-red">* </label>
                  <select class="form-control" id="countryId" name="countryId" onchange="getStateList(this.value)">
				  <option selected="" value="Default">Select Country-</option>
				  
				  <s:forEach var="countryList" items="${countryList}">
				    <option value="${countryList.countryId}">${countryList.countryName}</option>
                  </s:forEach> 
                    
                  </select>
                </div>
                   <div class="col-md-3">
                  <label>State</label> <label class="text-red">* </label>
                  <select class="form-control" id="stateId" name="stateId" onchange="getCityList(this.value)">
				  
				  	<option selected="" value="Default">-Select State-</option>
				  <s:forEach var="stateList" items="${stateList}">
                    <option value="${stateList.stateId}">${stateList.stateId}</option>
				  </s:forEach>
                  </select>
                </div>
                  <div class="col-xs-3">
				  <label>City</label> <label class="text-red">* </label>
				  
                  <select class="form-control" name="cityId" id="cityId" onchange="getLocationAreaList(this.value)">
				  		<option selected="" value="Default">Select City-</option>
                     <s:forEach var="cityList" items="${cityList}">
                    	<option value="${cityList.cityId}">${cityList.cityId}</option>
				    </s:forEach>       
                  </select>
                  </div>
                  </div>
                </div>
                
				
					
				
              <!-- /.form-group -->
            </div>
            
            <!-- /.col -->
			
          </div>
		  	     <div class="box-body">
                <div class="row">
                 </br>
                 
				    <div class="col-xs-3">
			  		
			        </div>
			        
					<div class="col-xs-3">
			 			&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
			  			<a href="LocationAreaMaster">  <button type="button" class="btn btn-default" value="reset" style="width:90px"> Reset</button></a>
              		</div> 
              		<div class="col-xs-2">
			      		<a href="AddLocationArea"> <button type="button" class="btn btn-success"><i class="fa fa-plus"></i> Add New Locations</button></a>
            		</div>
			       <div class="col-xs-3">
			       <a href="ImportNewLocationArea">  <button type="button" class="btn btn-info pull-right" style="background:#48D1CC"> Import Excel File</button></a>
                </div>  
			  </div>
			</div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        
      </div>
      <!-- /.box -->
				
				<div class="box box-default">
        
       <div  class="panel box box-danger"></div>
          
			 <div class="box-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="locationListTable">
                  <thead>
                  <tr bgcolor=#4682B4>
                    <th style="width:300px"> Location Name</th>
                    <th style="width:300px">City Name</th>
                    <th style="width:300px">State Name</th>
                    <th style="width:150px">Country Name</th>
                    <th style="width:300px"> Pin Code</th>
                    <th style="width:50px">Action</th>
                  </tr>
                  </thead>
                  <tbody>
                   <s:forEach items="${locationareaList}" var="locationareaList" varStatus="loopStatus">
                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                        <td id="locationareaName">${locationareaList.locationareaName}</td>
                        <td id="cityId">${locationareaList.cityId}</td>
                        <td id="stateId">${locationareaList.stateId}</td>
                        <td id="countryId">${locationareaList.countryId}</td>
                        <td id="locationareaName">${locationareaList.pinCode}</td>
                        <td><a href="${pageContext.request.contextPath}/EditLocationArea?locationareaId=${locationareaList.locationareaId}" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>
                      </tr>
					</s:forEach>
                 
                 </tbody>
                </table>
              </div>
            </div>
				
              <!-- /.form-group -->
          
        <!-- /.box-body -->
          
      </div>
     
    </section>
	</form>
    <!-- /.content -->
    
  </div>
 
  <!-- Control Sidebar -->
       <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
 
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<!-- Page script -->
<script>



//function to get country wise state list------------------------------------
function getStateList()
{
	 $("#stateId").empty();
	 var countryId = $('#countryId').val();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getStateList',
		type : 'Post',
		data : { countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select State-");
							$("#stateId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].stateId).text(result[i].stateName);
							    $("#stateId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
	// to retrive all value in table..........
	// for clear table
	$("#locationListTable tr").detach();
	
	
	 var countryId = $('#countryId').val();
	 
	 $("#stateId").empty(); // for clear the State
	 $("#cityId").empty(); // for clear city
		var option = $('<option/>');// to add select option in city
		option.attr('value',"Default").text("-Select City-");
		$("#cityId").append(option);
		
		
	$.ajax({

		url : '${pageContext.request.contextPath}/getCountryLocationList',
		type : 'Post',
		data : { countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
										$('#locationListTable').append('<tr style="background-color: #4682B4;"><th>Location Name</th><th>City Name</th><th>State Name</th><th>Country Name</th><th>Action</th>');
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].locationareaId;
										$('#locationListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].locationareaName+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].countryId+'</td><td><a href="${pageContext.request.contextPath}/EditLocationArea?locationareaId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
								else
								{
									var id = result[i].locationareaId;
										$('#locationListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].locationareaName+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].countryId+'</td><td><a href="${pageContext.request.contextPath}/EditLocationArea?locationareaId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
}//end of get State List



function getCityList()
{
	 $("#cityId").empty();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();
	$.ajax({

		url : '${pageContext.request.contextPath}/getCityList',
		type : 'Post',
		data : { stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select City-");
							$("#cityId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].cityId).text(result[i].cityName);
							    $("#cityId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
// to retrive all value in table..........
	
	$("#locationListTable tr").detach();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();
	 $("#cityId").empty();
	$.ajax({
		//alert("hjhjh");

		url : '${pageContext.request.contextPath}/getStateLocationList',
		type : 'Post',
		data : { countryId : countryId, stateId : stateId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
										$('#locationListTable').append('<tr style="background-color: #4682B4;"><td>Location Name</td><td>City Name</td><td>State Name</td><td>Country Name</td><td>Action</td>');
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].locationareaId;
										$('#locationListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].locationareaName+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].countryId+'</td><td><a href="${pageContext.request.contextPath}/EditLocationArea?locationareaId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
								else
								{
									var id = result[i].locationareaId;
										$('#locationListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].locationareaName+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].countryId+'</td><td><a href="${pageContext.request.contextPath}/EditLocationArea?locationareaId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
		
}//end of get City List


function getLocationAreaList()
{
	$("#locationListTable tr").detach();
	
	 var countryId = $('#countryId').val();
	 var stateId = $('#stateId').val();
	 var cityId = $('#cityId').val();

	 $.ajax({

		 url : '${pageContext.request.contextPath}/searchLocationAreaNameWiseList',
		type : 'Post',
		data : { countryId : countryId, stateId : stateId, cityId : cityId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
										$('#locationListTable').append('<tr style="background-color: #4682B4;"><td>Location Name</td><td>City Name</td><td>State Name</td><td>Country Name</td><td>Action</td>');
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].locationareaId;
										$('#locationListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].locationareaName+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].countryId+'</td><td><a href="${pageContext.request.contextPath}/EditLocationArea?locationareaId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
								else
								{
									var id = result[i].locationareaId;
										$('#locationListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].locationareaName+'</td><td>'+result[i].cityId+'</td><td>'+result[i].stateId+'</td><td>'+result[i].countryId+'</td><td><a href="${pageContext.request.contextPath}/EditLocationArea?locationareaId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
}

$(function () {
    $('#locationListTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Item Master</title>
  <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
 
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
	
<div class="wrapper">

   <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Item Master Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Item Master</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="itemmasterform" action="${pageContext.request.contextPath}/ItemMaster" method="post">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
        
            <div class="col-md-3">
               <label>Item Category</label> 
                  <select class="form-control" name="suppliertypeId" id="suppliertypeId" onchange="getItemList(this.value)">
				  <option selected="" value="Default">-Select Category-</option>
              		 <c:forEach var="suppliertypeList" items="${suppliertypeList}" >
					<option value="${suppliertypeList.suppliertypeId}">${suppliertypeList.supplierType}</option>
					 </c:forEach>
                  </select>
                </div>
          
          </div>
          </div>
		  	     <div class="box-body">
                <div class="row">
                 </br>
			     
					<div class="col-xs-3">
			 &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 
			 <a href="ItemMaster">  <button type="button" class="btn btn-default" value="reset" style="width:90px"> Reset</button></a>
              </div> 
              <div class="col-xs-2">
			      <a href="AddItem"> <button type="button" class="btn btn-success"><i class="fa fa-plus"></i> Add New Item</button></a>
            </div>
			     <div class="col-xs-3">
			         <button type="button" class="btn btn-info pull-right" style="background:#48D1CC"><a href="ImportNewItem"> Import From Excel File</a></button>
              
                  </div>   
			  </div>
			</div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        
      </div>
      <!-- /.box -->
				
     
		<div class="box box-default">
		    <div  class="panel box box-danger"></div>
        	 <div class="box-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="itemListTable">
                  <thead>
                  <tr bgcolor="#4682B4">
                  		<th>Category Name</th>
                  		<th>Sub-Category Name</th>
	                    <th>Item Name</th>
	                     <th>HSN Code</th>
	                    <th>GST %</th>
	                    <th>Status</th>
	                    <th style="width:50px">Action</th>
                  </tr>
                  </thead>
                  <tbody >
                   <c:forEach items="${itemList}" var="itemList" varStatus="loopStatus">
                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                            <td>${itemList.suppliertypeId}</td>
                            <td>${itemList.subsuppliertypeId}</td>
	                        <td>${itemList.itemName}</td>
	                        <td>${itemList.hsnCode}</td>
	                        <td>${itemList.gstPer}</td>
	                        <td>${itemList.itemStatus}</td>
	                        <td><a href="${pageContext.request.contextPath}/EditItem?itemId=${itemList.itemId}" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>
                      </tr>
				   </c:forEach>
                 </tbody>
                </table>
              </div>
 </div>
     
</section>
</form>
    <!-- /.content -->
    
  </div>
 
  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
 
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<script>


function getItemList()
{
	 //$("#stateListTable").empty();
$("#itemListTable tr").detach();
	 var suppliertypeId = $('#suppliertypeId').val();
	//alert(suppliertypeId)
	$.ajax({

		url : '${pageContext.request.contextPath}/getCategoryWiseItemList',
		type : 'Post',
		data : { suppliertypeId : suppliertypeId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
							$('#itemListTable').append('<tr style="background-color: #4682B4;"><td >Main Category Name</td><td >Sub-Category Name</td> <td>Item Name</td><td>HSN Code</td><td >GST %</td><td>Status</td><td style="width:50px">Aciton</td>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
									{
									var id = result[i].itemId;
								$('#itemListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].suppliertypeId+'</td><td>'+result[i].subsuppliertypeId+'</td><td>'+result[i].itemName+'</td><td>'+result[i].hsnCode+'</td><td>'+result[i].gstPer+'</td><td>'+result[i].itemStatus+'</td><td><a href="${pageContext.request.contextPath}/EditItem?itemId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
									}
								else{
									var id = result[i].itemId;
								$('#itemListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].suppliertypeId+'</td><td>'+result[i].subsuppliertypeId+'</td><td>'+result[i].itemName+'</td><td>'+result[i].hsnCode+'</td><td>'+result[i].gstPer+'</td><td>'+result[i].itemStatus+'</td><td><a href="${pageContext.request.contextPath}/EditItem?itemId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
									
								}
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get State List


$(function () {
    $('#itemListTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })

</script>
</body>
</html>

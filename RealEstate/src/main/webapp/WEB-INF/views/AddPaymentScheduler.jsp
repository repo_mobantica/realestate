<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Add Payment Scheduler</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
    
 <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>

    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
    </script>

  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>

<div class="wrapper">

   <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Payment Scheduler Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Add State</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="paymentschedulerform" action="${pageContext.request.contextPath}/AddPaymentScheduler" onSubmit="return validate()" method="post">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
     
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
                <span id="statusSpan" style="color:#008000"></span>
              <!-- /.form-group -->
              <div class="box-body">
          		 <div class="row">
	            	<div class="col-md-2">
	                  <label for="stateId">Payment Scheduler Id</label>
	                  <input type="text" class="form-control" id="paymentschedulerId" name="paymentschedulerId"  value="${schedulerId}" readonly/>
	                </div>
				 </div>
			  </div>
			  
			  <div class="box-body">
          		 <div class="row">
          		<div class="col-xs-3">
				<label>Project Name</label><label class="text-red">* </label>
				   <select class="form-control" name="projectId" id="projectId" onchange="getBuildingList(this.value)">
				  		<option selected="" value="Default">-Select Project-</option>
                     <c:forEach var="projectList" items="${projectList}">
                    	<option value="${projectList.projectId}">${projectList.projectName}</option>
				     </c:forEach>
                  </select>
                    <span id="projectIdSpan" style="color:#FF0000"></span>
			     </div> 
	              <div class="col-xs-3">
			     <label>Project Building</label><label class="text-red">* </label>
	                  <select class="form-control" name="buildingId" id="buildingId" onchange="getwingIdList(this.value)">
					  		<option selected="" value="Default">-Select Project Building-</option>
	                  </select>
	                   <span id="buildingIdSpan" style="color:#FF0000"></span>
			     </div> 
                 <div class="col-xs-3">
			      <label>Wing Name</label> <label class="text-red">* </label>
            	  <select class="form-control" name="wingId" id="wingId" onchange="getInstallmentNumber(this.value)">
				 		<option selected="" value="Default">-Select Wing Name-</option>
                  </select>
                   <span id="wingIdSpan" style="color:#FF0000"></span>
                 </div>
	            	<div class="col-md-3">
	                  <label for="">Installment</label>
	                  <input type="text" class="form-control" id="installmentNumber" name="installmentNumber"  value="" readonly/>
	                </div>
	             </div>
			  </div>
			  
			  <div class="box-body">
          		 <div class="row">
          		 
          		 <div class="col-md-3">
	                  <label for="">Percentage(%)</label><label class="text-red">* </label>
	                  <input type="text" class="form-control" id="percentage" placeholder="Enter percentage" name="percentage"  onchange="CheckPercentage(this.value)">
                 	  <span id="percentageStatusSpan" style="color:#FF0000"></span>
                  </div>
	            	<div class="col-md-3">
	                  <label for="">Payment Description</label><label class="text-red">* </label>
	                  <input type="text" class="form-control" style="width: 300px;" id="paymentDecription" name="paymentDecription"  style="text-transform: capitalize;" placeholder="Enter Payment Description"  value="" />
	                  <span id="paymentDescriptionSpan" style="color:#FF0000"></span>
	                </div>
	                <div class="col-xs-3">
					   <label for="joiningdate">DueDate</label><label class="text-red">*</label>
        	        <div class="input-group date">
            	      <div class="input-group-addon">
                	    <i class="fa fa-calendar"></i>
                  	  </div>
                	  <input type="text" class="form-control pull-right" id="dueDate" name="dueDate" />
                	  <span id="dueDateSpan" style="color:#FF0000"></span>
                	</div>
				</div>
	                
	            <div class="col-xs-3">
					   <label for="joiningdate">Paid Date</label><label class="text-red">*</label>
        	        <div class="input-group date">
            	      <div class="input-group-addon">
                	    <i class="fa fa-calendar"></i>
                  	  </div>
                	  <input type="text" class="form-control pull-right" id="paidDate" name="paidDate" >
                	  <span id="paidDateSpan" style="color:#FF0000"></span>
                	</div>
				</div>
          		 </div>
          		 </div>
			  
			   <input type="hidden" id="architectureSign" name="architectureSign" value="Incompleted">
			  <input type="hidden" id="engineerSign" name="engineerSign" value="Incompleted">
			  <input type="hidden" id="meSign" name="meSign" value="Incompleted">
			  
			  <input type="hidden" id="architectureSignDate" name="architectureSignDate">
			  <input type="hidden" id="engineerSignDate" name="engineerSignDate" >
			  <input type="hidden" id="meSignDate" name="meSignDate" >
			  
			  <input type="hidden" id="slabStatus" name="slabStatus" value="Incompleted">
				<input type="hidden" id="schedulerStatus" name="schedulerStatus" value="${schedulerStatus}">
				<input type="hidden" id="creationDate" name="creationDate" value="">
				<input type="hidden" id="updateDate" name="updateDate" value="">
				<input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>">
				
	  </div>
				
 </div>
		   	 
			 <div class="box-body">
              <div class="row">
                 <br/> <br/>
                  <div class="col-xs-3">
                  <div class="col-xs-2">
	                <a href="PaymentSchedulerMaster"><button type="button" class="btn btn-block btn-primary" value="reset" style="width:90px">Back</button></a>
			     </div>
			     </div>
                 
				  <div class="col-xs-3">
	                <button type="reset" class="btn btn-default" value="reset" style="width:90px">Reset</button>
			     </div>
			     
				 <div class="col-xs-3">
			  		<button type="submit" class="btn btn-info btn-primary" name="submit">Submit</button>
			     </div> 
			  </div>
          <!-- /.row -->
           </div>
        <!-- /.box-body -->
        
         </div>
      <!-- /.box -->
	 </div>	
       </section>
	</form>
 </div>
  <%@ include file="footer.jsp" %>
    <!-- /.content -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->


    
<script>



function getBuildingList()
{
	 $("#buildingId").empty();
	 $("#wingId").empty();
	 var projectId = $('#projectId').val();
	 
	$.ajax({

		url : '${pageContext.request.contextPath}/getBuildingList',
		type : 'Post',
		data : { projectId : projectId },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{

							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Wing Name-");
							$("#wingId").append(option);
							
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Building-");
							$("#buildingId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].buildingId).text(result[i].buildingName);
							    $("#buildingId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get Building List  



function getwingIdList()
{
	 $("#wingId").empty();
	 var buildingId = $('#buildingId').val();
	 var projectId = $('#projectId').val();
	 $.ajax({

		url : '${pageContext.request.contextPath}/getprojectwingList',
		type : 'Post',
		data : { buildingId : buildingId, projectId : projectId },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Wing Name-");
							$("#wingId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].wingId).text(result[i].wingName);
							    $("#wingId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}



function getInstallmentNumber()
{
	clearall();
	document.paymentschedulerform.installmentNumber.value="";
	document.paymentschedulerform.percentage.value="";
	document.paymentschedulerform.paymentDecription.value="";
	 var projectId = $('#projectId').val();
	 var buildingId = $('#buildingId').val();
	 var wingId = $('#wingId').val();
	 var totalPercentage=0;
	 var count=1;
	$.ajax({

		url : '${pageContext.request.contextPath}/getInstallmentNumbers',
		type : 'Post',
		data : { wingId : wingId, buildingId : buildingId, projectId : projectId},
		dataType : 'json',
		success : function(result)
				  {
			
						if (result) 
						{
							
							for(var i=0;i<result.length;i++)
							{
								totalPercentage=totalPercentage+result[i].percentage;
								count++;
							 } 
							if(totalPercentage>=100)
								{
								$('#wingIdSpan').html(' for this wing payment schedule is exit..!');
								 $("#wingId").empty();
								 
								 
								 $.ajax({

										url : '${pageContext.request.contextPath}/getprojectwingList',
										type : 'Post',
										data : { buildingId : buildingId, projectId : projectId },
										dataType : 'json',
										success : function(result)
												  {
														if (result) 
														{
															var option = $('<option/>');
															option.attr('value',"Default").text("-Select Wing Name-");
															$("#wingId").append(option);
															
															for(var i=0;i<result.length;i++)
															{
																var option = $('<option />');
															    option.attr('value',result[i].wingId).text(result[i].wingName);
															    $("#wingId").append(option);
															 } 
														} 
														else
														{
															alert("failure111");
															//$("#ajax_div").hide();
														}

													}
										});
									
								
								}
							else if(count==1||count==21)
								{
								document.paymentschedulerform.installmentNumber.value=count+"st Installment";
								}
							else if(count==3||count==23)
								{
								document.paymentschedulerform.installmentNumber.value=count+"rd Installment";
								}
							else if(count==2||count==22)
								{
								document.paymentschedulerform.installmentNumber.value=count+"nd Installment";
								}
							else
								{
								document.paymentschedulerform.installmentNumber.value=count+"th Installment";
								}
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}

function CheckPercentage()
{
	 var percentage = parseInt($('#percentage').val());
	 var projectId = $('#projectId').val();
	 var buildingId = $('#buildingId').val();
	 var wingId = $('#wingId').val();
	 var totalPercentage=0;
	      $.ajax({

			url : '${pageContext.request.contextPath}/getInstallmentNumbers',
			type : 'Post',
			data : {wingId : wingId, buildingId : buildingId, projectId : projectId},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{
								
								for(var i=0;i<result.length;i++)
								{
									totalPercentage=totalPercentage+result[i].percentage;
								 } 
								if(totalPercentage==0)
								{
									//$('#percentageStatusSpan').html('please add 100 or less than 100..!');
									//document.paymentschedulerform.percentage.focus();
									//document.paymentschedulerform.percentage.value="";
								}
								else if((totalPercentage+percentage)>100)
								{
									$('#percentageStatusSpan').html('Already add '+totalPercentage+'..!');
									document.paymentschedulerform.percentage.focus();
									document.paymentschedulerform.percentage.value="";
									//return false;
								}
								else
								{
									$('#percentageStatusSpan').html('');
								}
							} 
							else
							{
								alert("failure111");
							}

						}
			});	
}

function validate()
{ 
		clearall();
		
		if(document.paymentschedulerform.projectId.value=="Default")
		{
			 $('#projectIdSpan').html('Please, select project name..!');
			document.paymentschedulerform.projectId.focus();
			return false;
		}
		//Validation for percentage
		if(document.paymentschedulerform.percentage.value=="")
		{
			$('#percentageStatusSpan').html('Please, enter percentage..!');
			document.paymentschedulerform.percentage.focus();
			
			return false;
		}
		else if(!document.paymentschedulerform.percentage.value.match(/^[0-9]+$/))
		{
			if(!document.paymentschedulerform.percentage.value.match(/^[0-9]+[.]{0,1}[0-9]{0,2}$/))
				{
				$('#percentageStatusSpan').html('Please, enter valid percentage..!');
				document.paymentschedulerform.percentage.focus();
				
				return false; 
				}
		}
		
		//Validation for Payment Description
		if(document.paymentschedulerform.paymentDecription.value=="")
		{
			$('#paymentDescriptionSpan').html('Please, enter description..!');
			document.paymentschedulerform.paymentDecription.focus();
			return false;
		}
		else
		{
			$('#paymentDescriptionSpan').html('');
		}
		
		//validation for dueDate
		if(document.paymentschedulerform.dueDate.value=="")
		{
			$('#dueDateSpan').html('Please, select due date..!');
			document.paymentschedulerform.dueDate.focus();
			return false;
		}
		else
		{
			$('#dueDateSpan').html('');
		}
		
		//validation for paidDate
		if(document.paymentschedulerform.paidDate.value=="")
		{
			$('#paidDateSpan').html('Please, select paid date..!');
			document.paymentschedulerform.paidDate.focus();
			return false;
		}
		else
		{
			$('#paidDateSpan').html('');
		}
		if(document.paymentschedulerform.slabStatus.value=="Default")
		{
			$('#slabStatusSpan').html('Please, select slab status name..!');
			document.paymentschedulerform.slabStatus.focus();
			return false;
		}
}

function clearall()
{
	$('#paymentDescriptionSpan').html('');
	$('#percentageStatusSpan').html('');
}
function init()
{
	 clearall();
	 var date =  new Date();
		var year = date.getFullYear();
		var month = date.getMonth() + 1;
		var day = date.getDate();
		
		document.getElementById("creationDate").value = day + "/" + month + "/" + year;
		document.getElementById("updateDate").value = day + "/" + month + "/" + year;
		
		
	 
	 if(document.paymentschedulerform.schedulerStatus.value == "Fail")
	 {
		 $('#statusSpan').html('Reord Already Present..!');
	 }
	 else if(document.paymentschedulerform.schedulerStatus.value == "Success")
	 {
		 $('#statusSpan').html('Record saved successfully..!');
	 }
	 
	document.paymentschedulerform.percentage.focus();
}


$(function ()
  {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
     $('#paidDate').datepicker({
      autoclose: true
    })
 
    $('#dueDate').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>
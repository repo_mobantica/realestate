<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Project Wing Master</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
 
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

  
  <!-- Left side column. contains the logo and sidebar -->
    <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Project Wing Master Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Project Wing Master</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="projectwingform" action="${pageContext.request.contextPath}/ProjectWingMaster" onSubmit="return validate()" method="post">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
        
          <div class="box-body">
          <div class="row">
              
                 <div class="col-md-3">
                  <label>Project</label><label class="text-red">* </label>
                   <select class="form-control" name="projectId" id="projectId" onchange="getBuldingList(this.value)">
                  	 <option selected="selected" value="Default">-Select Project-</option>
                   	  <c:forEach var="projectList" items="${projectList}">
                    	<option value="${projectList.projectId}">${projectList.projectName}</option>
				     </c:forEach>
				   </select>
                  </div>
                  
                   <div class="col-md-3">
                     <label>Project Building</label><label class="text-red">* </label>
                     <select class="form-control" name="buildingId" id="buildingId" onchange="getAllWingList(this.value)">
				  		 <option selected="selected" value="Default">-Select Project Building-</option>
                       <c:forEach var="projectbuildingList" items="${projectbuildingList}">
                    	 <option value="${projectbuildingList.buildingId}">${projectbuildingList.buildingName}</option>
				       </c:forEach>
                     </select>
                  </div>
                  
                  <div class="col-xs-3">
			     	 <label>Wing </label> <label class="text-red">* </label>
             		 <select class="form-control" name="wingId" id="wingId" onchange="getWingList(this.value)">
				 	 	<option selected="selected" value="Default">-Select Wing Name-</option>
                   	   <c:forEach var="projectwingList" items="${projectwingList}">
                    	<option value="${projectwingList.wingId}">${projectwingList.wingName}</option>
				       </c:forEach>
                    </select>
                   </div>
                  
                </div>
              </div>
                
		  </div>
            
       </div>
		  	     <div class="box-body">
		  	     </br>
                <div class="row">
                 
				    <div class="col-xs-3">
			  		
			        </div>
			      
					<div class="col-xs-3">
			 			&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
			 			<a href="ProjectWingMaster">  <button type="button" class="btn btn-default" value="reset" style="width:90px">Reset</button></a>
              		</div> 
              		<div class="col-xs-2">
			      		<a href="AddProjectWing"> <button type="button" class="btn btn-success"><i class="fa fa-plus"></i>Add New Project Wing</button></a>
            		</div>
            		
			       <div class="col-xs-3">
			       <a href="ImportNewProjectWing">  <button type="button" class="btn btn-info pull-right" style="background:#48D1CC"> Import From Excel File</button></a>
                </div> 
			  </div>
			</div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        
      </div>
      <!-- /.box -->
				
		<div class="box box-default">
         <div  class="panel box box-danger"></div>
     	 <div class="box-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="wingListTable">
                  <thead>
	                  <tr bgcolor=#4682B4>
	                 	<th style="width:300px">Project Name</th>
	                    <th style="width:300px">Building Name</th>
	                    <th style="width:150px">Wing Name</th>
	                    <th style="width:150px">Basement Floor</th>
	                    <th style="width:150px">Stilts Floor</th>
	                    <th style="width:150px">Podium/Parking Floor</th>
	                    <th style="width:150px">Floors</th>
	                    <th style="width:150px">Total Floors</th>
	                    <th style="width:100px">Action</th>
	                  </tr>
                  </thead>
                  
                  <tbody>
                   <c:forEach items="${wingList}" var="wingList" varStatus="loopStatus">
                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                        <td>${wingList.projectId}</td>
                        <td>${wingList.buildingId}</td>
                        <td>${wingList.wingName}</td>
                        <td>${wingList.noofBasement}</td>
                        <td>${wingList.noofStilts}</td>
                        <td>${wingList.noofPodiums}</td>  
                        <td>${wingList.noofFloors}</td>
                        <td>${wingList.total}</td>  
                        <td><a href="${pageContext.request.contextPath}/EditProjectWing?wingId=${wingList.wingId}" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>
                      </tr>
					</c:forEach>
                 </tbody>
                </table>
              </div>
            </div>

        </div>
      </section>
	</form>
    
  </div>
 
  <!-- Control Sidebar -->
       <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
     
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script>


function getBuldingList()
{
	 $("#buildingId").empty();
	 var projectId = $('#projectId').val();

	 $.ajax({

		 url : '${pageContext.request.contextPath}/getBuildingList',
		type : 'Post',
		data : { projectId : projectId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Building Name-");
							$("#buildingId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].buildingId).text(result[i].buildingName);
							    $("#buildingId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
	 
	 
	 $("#wingListTable tr").detach();
	 var projectId = $('#projectId').val();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getProjectwiseWingList',
		type : 'Post',
		data : { projectId : projectId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
							$('#wingListTable').append('<tr style="background-color: #4682B4;"> <th style="width:150px">Wing Name</th> <th style="width:150px">Basement Floor</th><th style="width:150px">Stils Floor</th><th style="width:150px">Podium Floor</th><th style="width:150px">Floor</th><th style="width:150px">Total Floor</th> <th style="width:300px">Building Name</th><th style="width:300px">Project Name</th><th>Action</th>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].wingId;
									$('#wingListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].wingName+'</td><td>'+result[i].noofBasement+'</td><td>'+result[i].noofStilts+'</td><td>'+result[i].noofPodiums+'</td><td>'+result[i].noofFloors+'</td><td>'+result[i].total+'</td><td>'+result[i].buildingId+'</td><td>'+result[i].projectId+'</td><td><a href="${pageContext.request.contextPath}/EditProjectWing?wingId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
								else
								{
									var id = result[i].wingId;
									$('#wingListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].wingName+'</td><td>'+result[i].noofBasement+'</td><td>'+result[i].noofStilts+'</td><td>'+result[i].noofPodiums+'</td><td>'+result[i].noofFloors+'</td><td>'+result[i].total+'</td><td>'+result[i].buildingId+'</td><td>'+result[i].projectId+'</td><td><a href="${pageContext.request.contextPath}/EditProjectWing?wingId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	 
}//end of get Building List


function getAllWingList()
{
	  $("#wingId").empty();
	  var buildingId = $('#buildingId').val();
	  var projectId = $('#projectId').val();
	    
	  $.ajax({

		 url : '${pageContext.request.contextPath}/getprojectwingList',
		type : 'Post',
		data : { buildingId : buildingId, projectId : projectId },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Wing Name-");
							$("#wingId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].wingId).text(result[i].wingName);
							    $("#wingId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
	 
	 $("#wingListTable tr").detach();
	 
	 $.ajax({

		url : '${pageContext.request.contextPath}/getBuildingwiseWingList',
		type : 'Post',
		data : { buildingId : buildingId , projectId : projectId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
							$('#wingListTable').append('<tr style="background-color: #4682B4;"> <th style="width:150px">Wing Name</th> <th style="width:150px">Basement Floor</th><th style="width:150px">Stils Floor</th><th style="width:150px">Podium Floor</th><th style="width:150px">Floor</th><th style="width:150px">Total Floor</th> <th style="width:300px">Building Name</th><th style="width:300px">Project Name</th><th>Action</th>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].wingId;
									$('#wingListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].wingName+'</td><td>'+result[i].noofBasement+'</td><td>'+result[i].noofStilts+'</td><td>'+result[i].noofPodiums+'</td><td>'+result[i].noofFloors+'</td><td>'+result[i].total+'</td><td>'+result[i].buildingId+'</td><td>'+result[i].projectId+'</td><td><a href="${pageContext.request.contextPath}/EditProjectWing?wingId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
								else
								{
									var id = result[i].wingId;
									$('#wingListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].wingName+'</td><td>'+result[i].noofBasement+'</td><td>'+result[i].noofStilts+'</td><td>'+result[i].noofPodiums+'</td><td>'+result[i].noofFloors+'</td><td>'+result[i].total+'</td><td>'+result[i].buildingId+'</td><td>'+result[i].projectId+'</td><td><a href="${pageContext.request.contextPath}/EditProjectWing?wingId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
}


function getWingList()
{
	 $("#wingListTable tr").detach();
	 var projectId = $('#projectId').val();
	 var buildingId = $('#buildingId').val();
	 var wingId = $('#wingId').val();
	 
	 $.ajax({

		url : '${pageContext.request.contextPath}/getWingwiseWingList',
		type : 'Post',
		data : { wingId : wingId, buildingId : buildingId , projectId : projectId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
									$('#wingListTable').append('<tr style="background-color: #4682B4;"> <th style="width:150px">Wing Name</th> <th style="width:150px">Basement Floor</th><th style="width:150px">Stils Floor</th><th style="width:150px">Podium Floor</th><th style="width:150px">Floor</th><th style="width:150px">Total Floor</th> <th style="width:300px">Building Name</th><th style="width:300px">Project Name</th><th>Action</th>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].wingId;
									$('#wingListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].wingName+'</td><td>'+result[i].noofBasement+'</td><td>'+result[i].noofStilts+'</td><td>'+result[i].noofPodiums+'</td><td>'+result[i].noofFloors+'</td><td>'+result[i].total+'</td><td>'+result[i].buildingId+'</td><td>'+result[i].projectId+'</td><td><a href="${pageContext.request.contextPath}/EditProjectWing?wingId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
								else
								{
									var id = result[i].wingId;
									$('#wingListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].wingName+'</td><td>'+result[i].noofBasement+'</td><td>'+result[i].noofStilts+'</td><td>'+result[i].noofPodiums+'</td><td>'+result[i].noofFloors+'</td><td>'+result[i].total+'</td><td>'+result[i].buildingId+'</td><td>'+result[i].projectId+'</td><td><a href="${pageContext.request.contextPath}/EditProjectWing?wingId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
}


$(function () {
  $('#wingListTable').DataTable()
  $('#example2').DataTable({
    'paging'      : true,
    'lengthChange': false,
    'searching'   : false,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : false
  })
})
</script>
</body>
</html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Add Employee Leave</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>

<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
    </script>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
    		
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>

<div class="wrapper">

  <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Employee Leave Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="EmployeeLeaveDetails">Employee Leave Details</a></li>
        <li class="active">Add Employee Leave</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="employeeleaveform" action="${pageContext.request.contextPath}/AddEmployeeLeave" method="post" onSubmit="return validate()">
   
<section class="content">

<div class="box box-default">

        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
            
                  <span id="statusSpan" style="color:#FF0000"></span>
                  <input type="hidden" class="form-control" id="employeeLeaveId" name="employeeLeaveId" value="${employeeLeaveId}" readonly>
            	  
            	 <div class="box-body">
	               <div class="row">
	               
	                 <div class="col-xs-3">
                        <label>Employee Name</label> <label class="text-red">*</label>
                  		<select class="form-control" id="employeeId" name="employeeId" onchange="getAllYearllyDetials(this.value)">
				  				<option selected="selected" value="Default">-Select Employee Name-</option>
                      		<c:forEach var="empolyeeList" items="${empolyeeList}">
	                    		<option value="${empolyeeList.employeeId}">${empolyeeList.employeefirstName} ${empolyeeList.employeelastName}</option>
	                  		</c:forEach>
                  		</select>
                   		<span id="employeeIdSpan" style="color:#FF0000"></span>
                	 </div>
	                 
	                 <div class="col-xs-3">
					      <label>From Date</label> <label class="text-red">* </label>
						 <div class="input-group date">
		                  <div class="input-group-addon">
		                    <i class="fa fa-calendar"></i>
		                  </div>
		                  <input  type="text" class="form-control pull-right" id="leaveFromDate" name="leaveFromDate">
		                </div>
		                <span id="leaveFromDateSpan" style="color:#FF0000"></span>
			     	 </div>
			     	 
			     <div class="col-xs-3">
			      <label>To Date </label>
				  <div class="input-group">
                 	 <div class="input-group-addon">
                    	<i class="fa fa-calendar"></i>
                  	 </div>
	               <input type="text" class="form-control pull-right" id="leaveToDate" name="leaveToDate" >
	               </div>
	               <span id="leaveToDateSpan" style="color:#FF0000"></span>
			     </div>
			        
	                 <div class="col-xs-3">
                        <label>Leave Type</label> <label class="text-red">*</label>
                  		    <select class="form-control" id="leaveType" name="leaveType">
				  				<option selected="selected" value="Default">-Select Leave Type-</option>
								<option value="Personal Leave">Personal Leave</option>
								<option value="Sick Leave">Sick Leave</option>
								<option value="Paid Leave">Paid Leave</option>
								<option value="Unpaid Leave">Unpaid Leave</option>
								<option value="Holiday Leave">Holiday Leave</option>
				  				<option value="Optional Leave">Optional Leave</option>
                  		   </select>
                   		<span id="leaveTypeSpan" style="color:#FF0000"></span>
                	 </div>
	                 
			     </div>
			    </div>
			    
			    <div class="box-body">
	               <div class="row">
	               
			        <div class="col-xs-3">
			     	  <label for="purposeOfLeave">Purpose Of Leave</label> <label class="text-red">*</label>
                      <input type="text" class="form-control" id="firmgstNumber" placeholder="purpose Of Leave" name="purposeOfLeave" >
                      <span id="purposeOfLeaveSpan" style="color:#FF0000"></span>
			        </div>
			     </div>
			    </div>
			    
			<br/>
			
			<input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>">
				 
              <div class="box-body">
              <div class="row">
              
	            <div class="col-xs-1">
	            
	            </div>
	            <div class="col-xs-4">
	            <div class="col-xs-2">
                	<a href="EmployeeLeaveDetails"><button type="button" class="btn btn-block btn-primary" value="reset" style="width:90px">Back</button></a>
			    </div>
			    </div>
			    
	   		    <div class="col-xs-2">
	                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
			    </div>
			    
				<div class="col-xs-3">
		  			<button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
			    </div>
			    
			  </div>
			</div>
             
          <div class="box-body">
           <div class="row">  .
           <h4>This Year Employee Leave Details</h4>      
        	<div class="col-xs-12">
              <table class="table table-bordered" id="employeeLeaveListTable">
	              <tr bgcolor=#4682B4>
		              <th>From Date</th>
		              <th>To Date</th>
		              <th>Leave Type</th>
		              <th>Purpose of Leave</th>
		              <th>Apply Date</th>
	               </tr>

              </table>
            </div>
           </div>
          </div> 
              
            </div>
          </div>
        </div><!-- Col-12 end -->
        
        
	 </div>

</section>
</form>
</div>
  
<%@ include file="footer.jsp" %>
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->

<script>
function clearall()
{
	$('#employeeIdSpan').html('');
	$('#leaveFromDateSpan').html('');
	$('#leaveToDateSpan').html('');
	$('#leaveTypeSpan').html('');
	$('#purposeOfLeaveSpan').html('');
}


 function validate()
 {
	   clearall();
	     
		if(document.employeeleaveform.employeeId.value=="Default")
		{
			$('#employeeIdSpan').html('Please, select employee Name..!');
			document.employeeleaveform.employeeId.focus();
			return false;
		}
	 
		if(document.employeeleaveform.leaveFromDate.value=="")
		{
			$('#leaveFromDateSpan').html('Please, enter from date');
			document.employeeleaveform.leaveFromDate.value="";
			document.employeeleaveform.leaveFromDate.focus();
			return false;
		}

		if(document.employeeleaveform.leaveToDate.value=="")
		{
			$('#leaveToDateSpan').html('Please, enter to date');
			document.employeeleaveform.leaveToDate.value="";
			document.employeeleaveform.leaveToDate.focus();
			return false;
		} 
		 
		if(document.employeeleaveform.leaveType.value=="Default")
		{
			$('#leaveTypeSpan').html('Please, select leave type..!');
			document.employeeleaveform.leaveType.focus();
			return false;
		}

		if(document.employeeleaveform.purposeOfLeave.value=="")
		{
			$('#purposeOfLeaveSpan').html('Please, enter purpose of leave');
			document.employeeleaveform.purposeOfLeave.value="";
			document.employeeleaveform.purposeOfLeave.focus();
			return false;
		} 
}
 
function init()
{
	clearall();
	
     document.employeeleaveform.employeeId.focus();
}


 
function getAllYearllyDetials()
{
	
	 var employeeId = $('#employeeId').val();

		$('#employeeLeaveListTable tr').detach();
		
			$.ajax({

			url : '${pageContext.request.contextPath}/getAllYearllyDetials',
			type : 'Post',
			data : { employeeId : employeeId},
			dataType : 'json',
			success : function(result)
					  {
					   if (result) 
					   { 
							$('#employeeLeaveListTable').append('<tr style="background-color: #4682B4;"><th>From Date</th><th>To Date</th><th>Leave Type</th><th>Purpose Of Leave</th><th>Apply Date</th></tr>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									$('#employeeLeaveListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].fromDate+'</td><td>'+result[i].toDate+'</td><td>'+result[i].leaveType+'</td><td>'+result[i].purposeOfLeave+'</td><td>'+result[i].createDate+'</td>');
								}
								else
								{
									$('#employeeLeaveListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].fromDate+'</td><td>'+result[i].toDate+'</td><td>'+result[i].leaveType+'</td><td>'+result[i].purposeOfLeave+'</td><td>'+result[i].createDate+'</td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}
						}
			});	
}
 

  $(function ()
  {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
     $('#leaveFromDate').datepicker({
      autoclose: true
    })
 $('#employeeJoiningdate').datepicker({
      autoclose: true
    })

$('#leaveToDate').datepicker({
    autoclose: true
  })
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>

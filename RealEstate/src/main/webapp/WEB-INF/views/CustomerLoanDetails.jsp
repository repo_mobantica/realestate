<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Customer Loan Details</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
    </script>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        
  <script src="https://code.jquery.com/jquery-3.0.0.js"></script>
  <script src="https://code.jquery.com/jquery-migrate-3.0.1.js"></script>
          
</head>
<body class="hold-transition skin-blue sidebar-mini"  onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

  <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
  <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Customer Loan Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Sale</a></li>
        <li class="active">Customer Loan Details</li>
      </ol>
    </section>
    
<form name="CustomerLoanDetailsform" action="${pageContext.request.contextPath}/CustomerLoanDetails" onSubmit="return validate()" method="POST">
    <!-- Main content -->
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              
              <!-- /.form-group -->
			  
			   <span id="statusSpan" style="color:#FF0000"></span>
			   
		   <div class="box-body">
              <div class="row">
               
                 <div class="col-xs-2">
			       <label for="customerloanId">Loan Id</label>
			       <input type="text" class="form-control" id="customerloanId" name="customerloanId" value="${loanCode}" readonly>
                 </div> 
                 
                 <div class="col-xs-2">
			       <label for="bookingId">Booking Id</label>
                   <input type="text" class="form-control" id="bookingId" name="bookingId" value="${bookingList[0].bookingId}" readonly>
                 </div>
                  
			  </div>
            </div>
			  
			  
			<div class="box-body">
              <div class="row">
                  
                  <div class="col-xs-4">
                    <label for="customerName">Customer Name  </label>  <label class="text-red">* </label>
                    <input type="text" class="form-control" id="customerName" name="customerName"  value="${bookingList[0].bookingfirstname} ${bookingList[0].bookingmiddlename} ${bookingList[0].bookinglastname}" readonly>
                    <span id="customerNameSpan" style="color:#FF0000"></span>
			      </div> 
			      
			      <div class="col-xs-2">
			        <br/>
                    <label for="againstAggreement">Agreement</label>
                    <input type="checkbox" name="againstAggreement" id="againstAggreement" value="AggreementAgainst" onclick="Calculate()">
                    <span id="againstAggreementSpan" style="color:#FF0000"></span>
                 </div>
                 
                 <div class="col-xs-2">
                 	<br/>
                    <label for="againstStampDuty">Stamp Duty</label>
                    <input type="checkbox" name="againstStampDuty" id="againstStampDuty" value="StampDutyAgainst" onclick="Calculate()">
                    <span id="againstStampDutySpan" style="color:#FF0000"></span>
                 </div>
                 
                 <div class="col-xs-2">
                    <br/>
                    <label for="againstRegistration">Registration</label>
                    <input type="checkbox" name="againstRegistration" id="againstRegistration" value="RegistrationAgainst" onclick="Calculate()">
                    <span id="againstRegistrationSpan" style="color:#FF0000"></span>
                 </div>
                 
                 <div class="col-xs-2">
                    <br/>
                    <label for="againstGST">GST</label>
                    <input type="checkbox" name="againstGST" id="againstGST" value="GSTAgainst" onclick="Calculate()">
                    <span id="againstGST" style="color:#FF0000"></span>
                 </div>
			      
				</div>
            </div>
			  
			<div class="box-body">
              <div class="row">
              
                <div class="col-xs-2">
			       <label for="aggreementValue1">Agreement Amount </label>  <label class="text-red">*</label>
                   <input type="text" class="form-control" id="aggreementValue1" name="aggreementValue1" value="${bookingList[0].aggreementValue1}" readonly>
			       <span id="aggreementValue1Span" style="color:#FF0000"></span>
			    </div>
			    
			    <div class="col-xs-2">
			       <label for="stampDuty1">Stamp Duty </label>  <label class="text-red">*</label>
                   <input type="text" class="form-control" id="stampDuty1" name="stampDuty1"  value="${bookingList[0].stampDuty1}" readonly>
			       <span id="stampDuty1Span" style="color:#FF0000"></span>
			    </div>
			    
			    <div class="col-xs-2">
			       <label for="registrationCost1">Registration Amount </label>  <label class="text-red">*</label>
                   <input type="text" class="form-control" id="registrationCost1" name="registrationCost1"  value="${bookingList[0].registrationCost1}" readonly>
			       <span id="registrationCost1Span" style="color:#FF0000"></span>
			    </div>
			    
			    <div class="col-xs-2">
			       <label for="gstAmount1">GST Amount </label>  <label class="text-red">*</label>
                   <input type="text" class="form-control" id="gstAmount1" name="gstAmount1"  value="${bookingList[0].gstAmount1}" readonly>
			       <span id="gstAmount1Span" style="color:#FF0000"></span>
			    </div>
              
                <div class="col-xs-2">
			      <label for="totalAmount">Total Amount </label>  <label class="text-red">*</label>
                  <input type="text" class="form-control" id="totalAmount" name="totalAmount"  value="${bookingList[0].grandTotal1}" readonly>
			      <span id="totalAmountSpan" style="color:#FF0000"></span>
			     </div>
              
              </div>
            </div>
			
		    <div class="box-body">
              <div class="row">
				      
			    <div class="col-xs-3">
                  <label>Loan Amount</label>  <label class="text-red">* </label>
        		  <input type="text" class="form-control" id="loanAmount" name="loanAmount" onchange="Calculate()">
                  <span id="loanAmountSpan" style="color:#FF0000"></span>
                </div>               
                
                <div class="col-xs-3">
				  <label>Own Contribution Amount</label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="remainingAmount" name="remainingAmount" readonly>
                  <span id="remainingAmountSpan" style="color:#FF0000"></span>
				</div>
				
              </div>
             </div>
			
		    <div class="box-body">
              <div class="row">
				
				<div class="col-xs-3">
				  <label for="bankName">Prefer Loan Bank </label><label class="text-red">* </label>
		          <input class="form-control" id="bankName" name="bankName" placeholder="Prefer Bank Name" style="text-transform: capitalize;" />
                  <span id="bankNameSpan" style="color:#FF0000"></span>
			    </div> 
				
				<div class="col-xs-2">
				  <label for="fileNumber">File Number</label><label class="text-red">* </label>
		          <input class="form-control" id="fileNumber" name="fileNumber" placeholder="File Name"  style="text-transform: capitalize;"/>
                  <span id="fileNumberSpan" style="color:#FF0000"></span>
			    </div> 
				
				<div class="col-xs-3">
				  <label for="bankPersonName">Bank Person Name</label><label class="text-red">* </label>
		          <input class="form-control" id="bankPersonName" name="bankPersonName" placeholder="Bank person Name" style="text-transform: capitalize;" />
                  <span id="bankPersonNameSpan" style="color:#FF0000"></span>
			    </div> 
				
				<div class="col-xs-2">
				  <label for="bankPersonMobNo">Person Mo. No </label><label class="text-red">* </label>
		          <div class="input-group">
                   <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                   </div>
                  <input type="text" class="form-control" data-inputmask='"mask": "(+99) 9999999999"' data-mask name="bankPersonMobNo" data-mask id="bankPersonMobNo" >
                  </div>
                  <span id="bankPersonMobNoSpan" style="color:#FF0000"></span>
			    </div> 
				
				<div class="col-xs-2">
				  <label for="pl">Person Email-Id </label><label class="text-red">* </label>
		           <div class="input-group">
	                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
	                <input type="text" class="form-control" placeholder="Email" name ="bankPersonEmailId" id="bankPersonEmailId" >
	              </div>
                  <span id="bankPersonEmailIdSpan" style="color:#FF0000"></span>
			    </div> 
				
			  </div>
			</div>	
						
            </div>
          </div>
        </div>
          
		   <div class="box-body">
              <div class="row">
                
                <br/>
                <div class="col-xs-4">
                <div class="col-xs-4">
                	<a href="BookingMaster"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
			    </div>
			    </div>
				
				<div class="col-xs-4">
                    <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
			    </div>
				 
				<div class="col-xs-2">
			        <button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
			    </div> 
			     
              </div>
		    </div>
          <!-- /.row -->
		  
		  
        </div>
        <!-- /.box-body -->
        
      <!-- /.box -->

     
    </section>
	</form>
    <!-- /.content -->
  </div>
 

  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->

<script>

function clearAll()
{
	$('#againstAggreementSpan').html('');
	$('#loanAmountSpan').html('');
	$('#bankNameSpan').html('');
}


function validate()
{
	clearAll();
	
	if((document.getElementById("againstAggreement").checked == false) && (document.getElementById("againstStampDuty").checked == false) && (document.getElementById("againstRegistration").checked == false) && (document.getElementById("againstGST").checked == false))
	{
		$('#againstAggreementSpan').html('Please, select at least one checkbox..!');
		document.CustomerLoanDetailsform.againstAggreement.focus();
		return false;
	}
	
	if(document.CustomerLoanDetailsform.loanAmount.value=="")
	{
		$('#loanAmountSpan').html('Please, enter valid digit loan amount..!');
		document.CustomerLoanDetailsform.loanAmount.focus();
		return false;
	}
	else if(!document.CustomerLoanDetailsform.loanAmount.value.match(/^[0-9]+$/))
	{
		$('#loanAmountSpan').html(' loan amount must be digit numbers only..!');
		document.CustomerLoanDetailsform.loanAmount.value="";
		document.CustomerLoanDetailsform.loanAmount.focus();
		return false;
	}
	
	//validation for bank name	
	if(document.CustomerLoanDetailsform.bankName.value == "")
	{
		$('#bankNameSpan').html('Please, enter bank name..!');
		document.CustomerLoanDetailsform.bankName.focus();
		return false;
	}
	else if(document.CustomerLoanDetailsform.bankName.value.match(/^[\s]+$/))
	{
		$('#bankNameSpan').html('Please, enter bank name..!');
		document.CustomerLoanDetailsform.bankName.value="";
		document.CustomerLoanDetailsform.bankName.focus();
		return false;
	}

}

function init()
{
	clearAll();
	 
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	/* 
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	 */
	 
     document.CustomerLoanDetailsform.loanAmount.focus();
}


function Calculate()
{
	var loanAmount = Number($('#loanAmount').val());
	var againstAggreement = "";
	var againstStampDuty = "";
	var againstRegistration ="";
	var againstGST = "";

	var totalAmount = Number($('#totalAmount').val());
	var total = 0;
	var remainingValue = 0;
	/* 
	if(document.getElementById("againstAggreement").checked == true)
	{
		total = total + Number($('#aggreementValue1').val());	
	}
	else
	{
		total = total + 0;
	}
		
	if(document.getElementById("againstStampDuty").checked == true)
	{
		total = total + Number($('#stampDuty1').val());	
	}
	else
	{
		total = total + 0;
	}
	
	if(document.getElementById("againstRegistration").checked == true)
	{
		total = total + Number($('#registrationCost1').val());	
	}
	else
	{
		total = total + 0;
	}
	
	if(document.getElementById("againstGST").checked == true)
	{
		total = total + Number($('#gstAmount1').val());	
	}
	else
	{
		total = total + 0;
	}
	
	 */
	remainingValue = totalAmount - loanAmount;
	
	$('#remainingAmount').val(remainingValue.toFixed(0));
}

$(function () 
 {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    
    //Date picker
    $('#secondDob').datepicker({
      autoclose: true
    })
 $('#datepicker').datepicker({
      autoclose: true
    })
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>

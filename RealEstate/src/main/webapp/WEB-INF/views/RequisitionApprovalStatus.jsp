<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate |Add Material Purchase Requisition</title>
  <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">
 <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
    
 

  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>

<div class="wrapper">

   <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Material Purchase Requisition Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Site</a></li>
        <li class="active">Add Material Purchase Requisition</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="materialform" action="${pageContext.request.contextPath}/RequisitionApprovalStatus" onSubmit="return validate()" method="post">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
     
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
                <span id="statusSpan" style="color:#FF0000"></span>
              <!-- /.form-group -->
               <div class="box-body">
        	  <div class="row">
            <div class="col-md-2">
                  <label for="requisitionId"> Id</label>
                  <input type="text" class="form-control" id="requisitionId" name="requisitionId"  value="${materialpurchaserequisition[0].requisitionId}" readonly>
                </div>
			
                    <div class="col-xs-2">
			      <label>Employee Name </label> <label class="text-red">* </label>
                  <select class="form-control" name="employeeId" id="employeeId">
				  
                  <option selected="selected" value="${materialpurchaserequisition[0].employeeId}">${materialpurchaserequisition[0].employeeName}</option>
           
				 
                  </select>
			        <span id="employeeIdSpan" style="color:#FF0000"></span>
			     </div> 
                  <div class="col-xs-2">
			      <label>Project  </label> <label class="text-red">* </label>
                  <select class="form-control" name="projectId" id="projectId" onchange="getBuldingList(this.value)">
                  
                  <option selected="selected" value="${materialpurchaserequisition[0].projectId}">${projectName}</option>
			
                  </select>
			        <span id="projectIdSpan" style="color:#FF0000"></span>
			     </div> 
			       <div class="col-xs-2">
			      <label>Building Name  </label> <label class="text-red">* </label>
                  <select class="form-control" name="buildingId" id="buildingId" onchange="getWingNameList(this.value)">
                    	<option value="${materialpurchaserequisition[0].buildingId}">${buildingName}</option>
                  </select>
			       <span id="buildingIdSpan" style="color:#FF0000"></span>
			     </div> 
			
                 <div class="col-xs-2">
			      <label>Wing </label> <label class="text-red">* </label>
             	 <select class="form-control" name="wingId" id="wingId">
                    	<option value="${materialpurchaserequisition[0].wingId}">${wingName}</option>
                  </select>
                   <span id="wingIdSpan" style="color:#FF0000"></span>
                 </div> 
                 
                 <div class="col-xs-2">
			      <label>Required Date</label> <label class="text-red">* </label>
				 <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input  type="text" class="form-control pull-right" id="requiredDate" name="requiredDate"  value="${materialpurchaserequisition[0].requiredDate}">
                </div>
                <span id="requiredDateSpan" style="color:#FF0000"></span>
				   </div>
				   
				   
                 <div class="col-xs-2">
			      <label>Status </label> <label class="text-red">* </label>
             	 <select class="form-control" name="status" id="status">
                    	<option value="Approved">Approved</option>
                    	<option value="Reject">Reject</option>
                  </select>
                   <span id="statusSpan" style="color:#FF0000"></span>
                 </div> 
                 
                 
				 </div>
				</div>
		
            <br/>	
             <div class="panel box box-danger"></div>		
	
				<div class="box-body">
             	 <div class="row">
                  <div class="col-xs-5">
             
			    
			  
        </div>
       
     <div class="box-body">
     	<div class="row">
     	<br/>
     	<div class="col-md-1">
     	</div>
          <div class="col-md-10">
                  <div class="table-responsive">
                <table class="table table-bordered" id="ItemListTable">
                  <thead>
	                  <tr bgcolor=#4682B4>
	                  <th style="width:50px">Sr. No</th>
	                    <th style="width:300px">Item Name</th>
	                    <th style="width:150px">Item Unit</th>
	                    <th style="width:150px">Item Size</th>
	                  	<th style="width:150px">Quantity</th>
	                  <!--   <th style="width:50px">Remove</th>  -->
	                  </tr>
                  </thead>
                    <tbody>
                   <c:forEach items="${materialpurchaserequisitionhistory}" var="materialpurchaserequisitionhistory" varStatus="loopStatus">
                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                      		<td>${loopStatus.index+1}</td>
                     		<%-- <td>${materialpurchaserequisitionhistory.requisitionHistoryId}</td> --%>
	                        <td>${materialpurchaserequisitionhistory.itemId}</td>
	                        <td>${materialpurchaserequisitionhistory.itemunitName}</td>
	                        <td>${materialpurchaserequisitionhistory.itemSize}</td>
	                        <td>${materialpurchaserequisitionhistory.itemQuantity}</td>
	                        <%-- <td><a onclick="DeleteSelectedItem1(${materialpurchaserequisitionhistory.requisitionHistoryId})" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td> --%>
                      </tr>
					</c:forEach>
                 
                 </tbody>
                </table>
              </div>
                  </div>
                  </div>
       </div>
       </div>		  
			  
              <!-- /.form-group -->
            </div>
            <br/>
         	<div class="box-body">
              <div class="row">
              <div class="col-xs-1">
              </div>
                  <div class="col-xs-4">
                  <div class="col-xs-2">
                	<a href="MaterialPurchaseRequisitionApprovalMaster"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
			     </div>
			     </div>
				  <div class="col-xs-4">
                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
			     </div>
					<div class="col-xs-2">
			  <button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
              
			     </div> 
			       
			  </div>
        </div>
          </div>
		   	 

        <!-- /.box-body -->
        
         </div>
      <!-- /.box -->
	</div>
		</div>	
       </section>
	</form>
    <!-- /.content -->
    
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Page script -->


<script>
function clearall()
{
	$('#itemIdSpan').html('');
	$('#itemunitNameSpan').html('');
	$('#itemQuantitySpan').html('');
	$('#employeeIdSpan').html('');
	$('#projectIdSpan').html('');
	$('#buildingIdSpan').html('');
	$('#wingIdSpan').html('');
	$('#suppliertypeIdSpan').html('');
	$('#subsuppliertypeIdSpan').html('');
	$('#requiredDateSpan').html('');
}


function validate()
{ 

	if(document.materialform.employeeId.value=="Default")
	{
		$('#employeeIdSpan').html('Please, select employee name..!');
		document.materialform.employeeId.focus();
		return false;
	}

	
}

function init()
{
	
}

  $(function ()
  {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
     $('#requiredDate').datepicker({
      autoclose: true
    })


    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>

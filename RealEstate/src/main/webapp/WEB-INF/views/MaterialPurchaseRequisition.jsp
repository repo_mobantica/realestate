<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate |Add Material Purchase Requisition</title>
  <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">
 <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
    
 

  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>

<div class="wrapper">

   <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Material Purchase Requisition Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Site</a></li>
        <li class="active">Add Material Purchase Requisition</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="materialform" action="${pageContext.request.contextPath}/MaterialPurchaseRequisition" onSubmit="return validate()" method="post">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
     
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
                <span id="statusSpan" style="color:#FF0000"></span>
              <!-- /.form-group -->
               <div class="box-body">
        	  <div class="row">
            <div class="col-md-2">
                  <label for="requisitionId"> Id</label>
                  <input type="text" class="form-control" id="requisitionId" name="requisitionId"  value="${requisitionCode}" readonly>
                </div>
			
                    <div class="col-xs-2">
			      <label>Employee Name </label> <label class="text-red">* </label>
                  <select class="form-control" name="employeeId" id="employeeId">
				  <option selected="" value="Default">-Select Employee Name-</option>
                     <c:forEach var="employeeList" items="${employeeList}">
                    	<option value="${employeeList.employeeId}">${employeeList.employeefirstName} ${employeeList.employeemiddleName} ${employeeList.employeelastName}</option>
				     </c:forEach>
                  </select>
			        <span id="employeeIdSpan" style="color:#FF0000"></span>
			     </div> 
                  <div class="col-xs-2">
			      <label>Project  </label> <label class="text-red">* </label>
                  <select class="form-control" name="projectId" id="projectId" onchange="getBuldingList(this.value)">
				  <option selected="" value="Default">-Select Project-</option>
                     <c:forEach var="projectList" items="${projectList}">
                    	<option value="${projectList.projectId}">${projectList.projectName}</option>
				     </c:forEach>
                  </select>
			        <span id="projectIdSpan" style="color:#FF0000"></span>
			     </div> 
			       <div class="col-xs-2">
			      <label>Building Name  </label> <label class="text-red">* </label>
                  <select class="form-control" name="buildingId" id="buildingId" onchange="getWingNameList(this.value)">
				  		<option selected="" value="Default">-Select Project Building-</option>
                 
                  </select>
			       <span id="buildingIdSpan" style="color:#FF0000"></span>
			     </div> 
			
                 <div class="col-xs-2">
			      <label>Wing </label> <label class="text-red">* </label>
             	 <select class="form-control" name="wingId" id="wingId">
				 	 	<option selected="" value="Default">-Select Wing Name-</option>
                  </select>
                   <span id="wingIdSpan" style="color:#FF0000"></span>
                 </div> 
                 
                 <div class="col-xs-2">
			      <label>Required Date</label> <label class="text-red">* </label>
				 <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input  type="text" class="form-control pull-right" id="requiredDate" name="requiredDate">
                </div>
                <span id="requiredDateSpan" style="color:#FF0000"></span>
				   </div>
				 </div>
				</div>

            <br/>	
             <div class="panel box box-danger"></div>		
	
			  <div class="box-body">
              <div class="row">
                <div class="col-xs-2">
			      <label>Item Main Category</label> <label class="text-red">* </label>
                  <select class="form-control" name="suppliertypeId" id="suppliertypeId" onchange="getMainSubItemCategory(this.value)">
				  <option selected="" value="Default">-Select Category-</option>
                   <c:forEach var="suppliertypeList" items="${suppliertypeList}" >
					<option value="${suppliertypeList.suppliertypeId}">${suppliertypeList.supplierType}</option>
					 </c:forEach>
                  </select>
			         <span id="suppliertypeIdSpan" style="color:#FF0000"></span>
			     </div> 
			     
			     <div class="col-xs-2">
			      <label>Item Sub-Category</label> <label class="text-red">* </label>
                  <select class="form-control" name="subsuppliertypeId" id="subsuppliertypeId" onchange="getAllItem(this.value)">
				  <option selected="" value="Default">-Select Sub Item Category-</option>
               
                  </select>
			         <span id="subsuppliertypeIdSpan" style="color:#FF0000"></span>
			     </div>
			     
			    <div class="col-xs-2">
				 <label for="itemId">Item Name </label> <label class="text-red">* </label>
  				 <select class="form-control" name="itemId" id="itemId">
				 <option selected="" value="Default">-Select Item Name-</option>
                 </select>
                 <span id="itemIdSpan" style="color:#FF0000"></span>
                </div>
                
               <div class="col-md-2">
              <label for="itemunitName"> Item Unit</label><label class="text-red">* </label>
              <select class="form-control" name="itemunitName" id="itemunitName" >
				  <option selected="" value="Default">-Select Item Unit-</option>
                     <c:forEach var="itemUnitList" items="${itemUnitList}">
                    	<option value="${itemUnitList.itemunitSymbol}">${itemUnitList.itemunitSymbol}</option>
				     </c:forEach>
                  </select>
                  <span id="itemunitNameSpan" style="color:#FF0000"></span>
         		 </div>
         		<div class="col-md-2">
              <label for="itemSize">Item Size</label>
              <input type="text" class="form-control" id="itemSize" name="itemSize">
         	 </div>
               <div class="col-md-2">
	              <label for="itemQuantity"> Quantity</label><label class="text-red">* </label>
	               <div class="input-group">
	              <input type="text" class="form-control" id="itemQuantity" name="itemQuantity" >
	              	 <span class="input-group-btn">
		             <a onclick="AddNewItem()"><button type="button" class="btn btn-success"><i class="fa fa-plus"></i> Add</button></a>
		          </span>
	          </div>
	           <span id="itemQuantitySpan" style="color:#FF0000"></span> 
	          </div>

		
                
               </div>
            </div>
			
			     
				<div class="box-body">
             	 <div class="row">
                  <div class="col-xs-5">
             
			    
			   <!--  
			     <div class="box-body">
             	 <div class="row">
			     <div class="col-xs-12">
                <div class="table-responsive">
                <table class="table table-bordered" id="AllItemListTable">
                  <thead>
	                  <tr bgcolor=#4682B4>
	                    <th style="width:300px">Item Name</th>
	                    <th style="width:50px">Action</th> 
	                  </tr>
                  </thead>
                  <tbody>
                <c:forEach items="${itemList}" var="itemList" varStatus="loopStatus">
                    <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
	                        <td>${itemList.itemName}</td>
	                       <td><a onclick="AddItem('${itemList.itemId}','${itemList.itemName}')" class="btn btn-info btn-sm" data-toggle="tooltip" title="Add Item"><i class="glyphicon glyphicon-edit"></i></a></td>
                        </tr>
					</c:forEach>
               
                 </tbody>
                </table>
              </div>
              </div>
              </div>
              </div>
              -->
        </div>
       
     <div class="box-body">
     	<div class="row">
     	<br/>
     	<div class="col-md-1">
     	</div>
          <div class="col-md-10">
                  <div class="table-responsive">
                <table class="table table-bordered" id="ItemListTable">
                  <thead>
	                  <tr bgcolor=#4682B4>
	                  <th style="width:50px">Sr. No</th>
	                    <th style="width:300px">Item Name</th>
	                    <th style="width:150px">Item Unit</th>
	                    <th style="width:150px">Item Size</th>
	                  	<th style="width:150px">Quantity</th>
	                    <th style="width:50px">Remove</th> 
	                  </tr>
                  </thead>
                </table>
              </div>
                  </div>
                  </div>
       </div>
       </div>		  
			  
			  
			  <input type="hidden" id="totalNoItem" name="totalNoItem" >
			  <input type="hidden" id="totalQuantity" name="totalQuantity" >
			  
				<input type="hidden" id="status" name="status" value="Applied">
				<input type="hidden" id="creationDate" name="creationDate" value="">
				<input type="hidden" id="updateDate" name="updateDate" value="">
				<input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>" >
				
              <!-- /.form-group -->
            </div>
            <br/>
         	<div class="box-body">
              <div class="row">
              <div class="col-xs-1">
              </div>
                  <div class="col-xs-4">
                  <div class="col-xs-4">
                	<a href="MaterialPurchaseRequisitionMaster"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
			     </div>
			     </div>
				  <div class="col-xs-4">
                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
			     </div>
					<div class="col-xs-2">
			  <button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
              
			     </div> 
			       
			  </div>
        </div>
          </div>
		   	 

        <!-- /.box-body -->
        
         </div>
      <!-- /.box -->
	</div>
		</div>	
       </section>
	</form>
    <!-- /.content -->
    
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Page script -->


<script>
function clearall()
{
	$('#itemIdSpan').html('');
	$('#itemunitNameSpan').html('');
	$('#itemQuantitySpan').html('');
	$('#employeeIdSpan').html('');
	$('#projectIdSpan').html('');
	$('#buildingIdSpan').html('');
	$('#wingIdSpan').html('');
	$('#suppliertypeIdSpan').html('');
	$('#subsuppliertypeIdSpan').html('');
	$('#requiredDateSpan').html('');
}
function validate()
{ 
	clearall();
	if(document.materialform.employeeId.value=="Default")
	{
		$('#employeeIdSpan').html('Please, select employee name..!');
		document.materialform.employeeId.focus();
		return false;
	}
	if(document.materialform.projectId.value=="Default")
	{
		$('#projectIdSpan').html('Please, select project name..!');
		document.materialform.projectId.focus();
		return false;
	}
	if(document.materialform.buildingId.value=="Default")
	{
		$('#buildingIdSpan').html('Please, select building name..!');
		document.materialform.buildingId.focus();
		return false;
	}
	if(document.materialform.wingId.value=="Default")
	{
		$('#wingIdSpan').html('Please, select wing name..!');
		document.materialform.wingId.focus();
		return false;
	}
	if(document.materialform.requiredDate.value=="")
	{
		$('#requiredDateSpan').html('Please,select Date..!');
		document.materialform.requiredDate.focus();
		return false;
	} 

	TotalNoOfItem();
		
}
/* 
function getItemDetails()
{
	// $("#itemName").empty();
	 var suppliertypeId = $('#suppliertypeId').val();
	 var subsuppliertypeId = $('#subsuppliertypeId').val();
	// var subItemCategoryName = $('#subItemCategoryName').val();
	 var itemName = $('#itemName').val();
	 $('#AllItemListTable tr').detach();
	 $.ajax({

		url : '${pageContext.request.contextPath}/getItemDetails',
		type : 'Post',
		data : { suppliertypeId : suppliertypeId, subsuppliertypeId : subsuppliertypeId,itemId : itemId},
		dataType : 'json',
		success : function(result)
				  {
			
						if (result) 
						{
							for(var i=0;i<result.length;i++)
							{
								document.materialform.itemId.value=result[i].itemId;
							//	document.materialform.itemSize.value=result[i].itemSize;
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}
		
		}
		});
	
}
 */
/*

function AddItem(itemId)
{
	var a = itemId.split(",");
	var employeeId = $('#employeeId').val();
	document.materialform.itemName.value=a[1];
	document.materialform.itemId.value=a[0];
}
*/

function AddNewItem()
{
	
	clearall();
	if(document.materialform.suppliertypeId.value=="Default")
	{
		$('#suppliertypeIdSpan').html('Please, select Item Main Category..!');
		document.materialform.suppliertypeId.focus();
		return false;
	}
	if(document.materialform.subsuppliertypeId.value=="Default")
	{
		$('#subsuppliertypeIdSpan').html('Please, select Item main Sub category..!');
		document.materialform.subsuppliertypeId.focus();
		return false;
	}

	if(document.materialform.itemId.value=="Default")
	{
		$('#itemIdSpan').html('Please, select Item Name..!');
		document.materialform.itemId.focus();
		return false;
	}
	if(document.materialform.itemunitName.value=="Default")
	{
		$('#itemunitNameSpan').html('Please, select Item Unit..!');
		document.materialform.itemunitName.focus();
		return false;
	}
	
	if(document.materialform.itemQuantity.value=="")
	{
		$('#itemQuantitySpan').html('Please, enter item Quantity..!');
		document.materialform.itemQuantity.focus();
		return false;
	}
	/* 
	else if(!document.materialform.itemQuantity.value.match(/^[0-9]+$/))
	{
		$('#itemQuantitySpan').html('item quantity must be digit..!');
		document.materialform.itemQuantity.value="";
		document.materialform.itemQuantity.focus();
		return false;
	}
	 */
	
	var requisitionId = $('#requisitionId').val();
	var itemId = $('#itemId').val();
	var itemunitName = $('#itemunitName').val();
	var itemSize = $('#itemSize').val();
	var itemQuantity = parseFloat($('#itemQuantity').val());
	var totalQuantity = parseFloat($('#totalQuantity').val());
	
	totalQuantity=parseFloat(totalQuantity)+parseFloat(itemQuantity);
	document.materialform.totalQuantity.value=totalQuantity.toString();
	 $('#ItemListTable tr').detach();
	$.ajax({

		 url : '${pageContext.request.contextPath}/AddItemRequisition',
		type : 'Post',
		data : { requisitionId : requisitionId, itemId : itemId, itemunitName : itemunitName, itemSize : itemSize, itemQuantity : itemQuantity},
		dataType : 'json',
		success : function(result)
				  {
					if (result) 
						{
							$('#ItemListTable').append('<tr style="background-color: #4682B4;"><th style="width:50px">Sr. No</th><th style="width:150px">Item Name</th><th style="width:150px">Item Unit</th><th style="width:150px">Item Size</th><th style="width:50px">Quantity</th><th style="width:100px">Action</th>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].requisitionHistoryId;
									var quantity=result[i].itemQuantity;
									$('#ItemListTable').append('<tr style="background-color: #F0F8FF;"><td>'+(i+1)+'</td><td>'+result[i].itemId+'</td><td>'+result[i].itemunitName+'</td><td>'+result[i].itemSize+'</td><td>'+result[i].itemQuantity+'</td><td><a onclick="DeleteSelectedItem(\''+id+'\')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
								else
								{
									var id = result[i].requisitionHistoryId;
									var quantity=result[i].itemQuantity;
									$('#ItemListTable').append('<tr style="background-color: #CCE5FF;"><td>'+(i+1)+'</td><td>'+result[i].itemId+'</td><td>'+result[i].itemunitName+'</td><td>'+result[i].itemSize+'</td><td>'+result[i].itemQuantity+'</td><td><a onclick="DeleteSelectedItem(\''+id+'\')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
								document.materialform.totalNoItem.value=i+1;
							 }
							document.materialform.totalNoItem.value=i;
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	document.materialform.itemSize.value="";
	document.materialform.itemQuantity.value="";
	getAllItemName();
}



function TotalNoOfItem()
{

var oTable = document.getElementById('ItemListTable');
var rowLength = oTable.rows.length;

   var totalNoItem=0;
	var totalQuantity=0;
	
	for (var j = 1; j < rowLength; j++){

		   var oCells = oTable.rows.item(j).cells;

		   var cellLength = oCells.length;
		  
		   totalQuantity=totalQuantity+Number(oCells.item(4).innerHTML);
	}		
	document.materialform.totalQuantity.value=totalQuantity;
	document.materialform.totalNoItem.value=rowLength-1;
	
}


function DeleteSelectedItem(id)
{

var requisitionId = $('#requisitionId').val();
var requisitionHistoryId = id;
$('#ItemListTable tr').detach();
$.ajax({

	 url : '${pageContext.request.contextPath}/DeleteSelectedItem',
	type : 'Post',
	data : { requisitionHistoryId : requisitionHistoryId, requisitionId : requisitionId },
	dataType : 'json',
	success : function(result)
			  {
				   if (result) 
				   { 
					   $('#ItemListTable').append('<tr style="background-color: #4682B4;"><th style="width:50px">Sr. No</th><th style="width:150px">Item Name</th><th style="width:150px">Item Unit</th><th style="width:150px">Item Size</th><th style="width:100px">Quantity</th><th style="width:100px">Action</th>');
						
						for(var i=0;i<result.length;i++)
						{ 
							if(i%2==0)
							{
								var id = result[i].requisitionHistoryId;
								$('#ItemListTable').append('<tr style="background-color: #F0F8FF;"><td>'+(i+1)+'</td><td>'+result[i].itemId+'</td><td>'+result[i].itemunitName+'</td><td>'+result[i].itemSize+'</td><td>'+result[i].itemQuantity+'</td><td><a onclick="DeleteSelectedItem(\''+id+'\')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
							}
							else
							{
								var id = result[i].requisitionHistoryId;
								$('#ItemListTable').append('<tr style="background-color: #CCE5FF;"><td>'+(i+1)+'</td><td>'+result[i].itemId+'</td><td>'+result[i].itemunitName+'</td><td>'+result[i].itemSize+'</td><td>'+result[i].itemQuantity+'</td><td><a onclick="DeleteSelectedItem(\''+id+'\')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
							}
						
						 }
					} 
					else
					{
						alert("failure111");
					}
			  } 

	});
}

/*
function getAllItem()
{
	$('#itemName').empty();
	var suppliertypeId = $('#suppliertypeId').val();
	var subsuppliertypeId = $('#subsuppliertypeId').val();
	$.ajax({

		url : '${pageContext.request.contextPath}/getSubItemCategoryList',
		type : 'Post',
		data : { suppliertypeId : suppliertypeId, subsuppliertypeId : subsuppliertypeId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select Sub Item Name-");
								$("#itemName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].subItemCategoryName).text(result[i].subItemCategoryName);
							    $("#itemName").append(option);
							} 
						} 
						else
						{
							alert("failure111");
						}

					}
		});	
	
}
*/
function getMainSubItemCategory()
{
	$('#subsuppliertypeId').empty();
	var suppliertypeId = $('#suppliertypeId').val();
	$.ajax({

		url : '${pageContext.request.contextPath}/getSubSupplierTypeList',
		type : 'Post',
		data : { suppliertypeId : suppliertypeId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select Sub Item Category-");
								$("#subsuppliertypeId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].subsuppliertypeId).text(result[i].subsupplierType);
							    $("#subsuppliertypeId").append(option);
							} 
						} 
						else
						{
							alert("failure111");
						}

					}
		});	
	
}

function getAllItem()
{
	 $("#itemId").empty();
	 var suppliertypeId = $('#suppliertypeId').val();
	 var subsuppliertypeId = $('#subsuppliertypeId').val();
	// var subItemCategoryName = $('#subItemCategoryName').val();
	 $('#AllItemListTable tr').detach();
	 $.ajax({

		url : '${pageContext.request.contextPath}/getAllItemList',
		type : 'Post',
		data : { suppliertypeId : suppliertypeId, subsuppliertypeId : subsuppliertypeId},
		dataType : 'json',
		success : function(result)
				  {
			
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Item Name-");
							$("#itemId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].itemId).text(result[i].itemName);
							    $("#itemId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}
		
		
		
		/*
		if (result) 
		   { 
			   $('#AllItemListTable').append('<tr style="background-color: #4682B4;"><th style="width:300px">Item Name</th><th style="width:50px">Action</th>');
				
				for(var i=0;i<result.length;i++)
				{ 
					if(i%2==0)
					{
						var id = result[i].itemId;
						var name = result[i].itemName;
						$('#AllItemListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].itemName+'</td><td><a onclick="AddItem(\''+id+','+name+'\')" class="btn btn-info btn-sm" data-toggle="tooltip" title="Add"><i class="glyphicon glyphicon-edit"></i></a> </td>');
					}
					else
					{
						var id = result[i].itemId;
						var name = result[i].itemName;
						$('#AllItemListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].itemName+'</td><td><a onclick="AddItem(\''+id+','+name+'\')" class="btn btn-info btn-sm" data-toggle="tooltip" title="Add"><i class="glyphicon glyphicon-edit"></i></a> </td>');
					}
				
				 }
			} 
			else
			{
				alert("failure111");
			}	
					   */
		}
		});
	
}
function init()
{
	clearall();
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	 document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	/*  if(document.materialform.stateStatus.value == "Fail")
	 {
	  	alert("Sorry, record is present already..!");
	 }
	 else if(document.materialform.stateStatus.value == "Success")
	 {
		 $('#statusSpan').html('Record added successfully..!');
	 }
 */
	
	document.materialform.totalNoItem.value=0;
	document.materialform.totalQuantity.value=0;
	document.materialform.employeeId.focus();
}

function getBuldingList()
{
	 $("#buildingId").empty();
	 $("#loading").empty();
	 var projectId = $('#projectId').val();
 
	 $.ajax({

		url : '${pageContext.request.contextPath}/getBuildingList',
		type : 'Post',
		data : { projectId : projectId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Building Name-");
							$("#buildingId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].buildingId).text(result[i].buildingName);
							    $("#buildingId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
}//end of get Building List


function getWingNameList()
{
	 $("#wingId").empty();
	 var buildingId = $('#buildingId').val();
	 var projectId = $('#projectId').val();
	 $.ajax({

		url : '${pageContext.request.contextPath}/getprojectwingList',
		type : 'Post',
		data : { buildingId : buildingId, projectId : projectId },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Wing Name-");
							$("#wingId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].wingId).text(result[i].wingName);
							    $("#wingId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}
/*   
 $(function () {
 $('#AllItemListTable').DataTable()
 $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
  */
  
 $(function () {
 $('#ItemListTable').DataTable()
 $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
   
  $(function ()
  {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
     $('#requiredDate').datepicker({
      autoclose: true
    })


    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>

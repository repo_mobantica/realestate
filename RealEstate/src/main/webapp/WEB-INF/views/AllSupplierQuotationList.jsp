<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>RealEstate | Supplier Quotation List</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

 
  <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

    <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
    <section class="content-header">
      <h1>
        Supplier Quotation  List:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Purchase</a></li>
        <li class="active"> Supplier Quotation  List</li>
      </ol>
    </section>

<form name="StorePurchesform" id="StorePurchesform" action="${pageContext.request.contextPath}/AllSupplierQuotationList" onSubmit="return validate()" method="post">

<section class="content">
   
<div class="box box-default">
  <span id="statusSpan" style="color:#FF0000"></span>
    		  
	 <div class="box-body">
	   <div class="row">
	   <div class="col-xs-5">
	   </div>
		 <div class="col-xs-2">
			 <a href="${pageContext.request.contextPath}/AddSupplierQuotation?requisitionId=${requisitionId}"> <button type="button" class="btn btn-success"><i class="fa fa-plus"></i> Add New Quotation</button></a>
	     </div>
	 </div>
	</div>    
	
<div class="panel box box-danger"></div>
    <div class="box-body">
      <div class="row">
		<div class="col-md-12">
        
              <table id="materialDetailsTable" class="table table-bordered">
                <thead>
                <tr bgcolor="#4682B4">
               		<th style="width:30px">Sr.No</th>
               		<th>Quotation Id</th>
               		<th>Requisition Id</th>
               		<th>Supplier Id</th>
               		<th>Total</th>
               		<th>Discount Amount</th>
               		<th>GST Amount</th>
                  	<th>Grand Total</th>
                  	<th>Action</th>
                  </tr>
                </thead>
               <tbody>
              <c:forEach items="${supplierquotationList}" var="supplierquotationList" varStatus="loopStatus">
                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                      <td>${loopStatus.index+1}</td>
                      <td >${supplierquotationList.quotationId}</td>
                      <td>${supplierquotationList.requisitionId}</td> 
                      <td>${supplierquotationList.supplierId}</td>
                      <td>${supplierquotationList.total}</td>
                      <td>${supplierquotationList.discountAmount}</td>
                      <td>${supplierquotationList.gstAmount}</td>
                      <td>${supplierquotationList.grandTotal}</td>
                      <td>
                      <a href="${pageContext.request.contextPath}/MaterialPurchaseForm?quotationId=${supplierquotationList.quotationId}" class="btn btn-info btn-sm" data-toggle="tooltip" title="To purchase"><i class="glyphicon glyphicon-edit"></i></a>
	                       |<a target="_blank" href="${pageContext.request.contextPath}/PrintSupplierQuotation?quotationId=${supplierquotationList.quotationId}" class="btn btn-success btn-sm" data-toggle="tooltip" title="Print Quotation"><i class="glyphicon glyphicon-print"></i></a>
	                 
	                     </td>
                     </tr>
				 </c:forEach>
              </tbody>
        </table>
    </div>
    
	 <div class="col-md-12">
	 <div class="box-body">
        <div class="row">
         <div class="col-xs-1">
         </div>
	       <div class="col-xs-4">
	         <div class="col-xs-2">
              <a href="MaterialPurchaseRequisitionList"><button type="button" class="btn btn-block btn-primary" value="reset" style="width:90px">Back</button></a>
			</div>
			</div>
			    
		 </div>
	  </div>
	</div>
</div>
</div>
</div>
</section>
</form>
</div>

 <%@ include file="footer.jsp" %>
 
 <div class="control-sidebar-bg"></div>
</div>

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- page script -->
<script>
function AddAllData()
{
	
	
	var storeId = $('#storeId').val();
	var creationDate = $('#creationDate').val();
	
	var materialsPurchasedId = $('#materialsPurchasedId').val();
	
	var itemId;
	var itemUnit;
	var itemSize;
	var itemBrandName;
	var itemQuantity;
	var actualQuantity;
	var checkStatus;
	var remark;
	var materialPurchesHistoriId;
	var oTable = document.getElementById('materialDetailsTable');
	var rowLength = oTable.rows.length;
	//alert(rowLength);
	for (var i = 1; i < rowLength; i++){

		   var oCells = oTable.rows.item(i).cells;

		   var cellLength = oCells.length;
		   
			itemId=oCells.item(1).innerHTML;
			itemUnit =oCells.item(5).innerHTML;
			itemSize = oCells.item(6).innerHTML;
			itemBrandName = oCells.item(7).innerHTML;
			itemQuantity = oCells.item(8).innerHTML;
			actualQuantity= oCells.item(9).innerHTML;
			checkStatus= oCells.item(10).innerHTML;
			remark=oCells.item(11).innerHTML;
			materialPurchesHistoriId=oCells.item(12).innerHTML;
			
			 $.ajax({

				url : '${pageContext.request.contextPath}/SaveAlllStoreDate',
				type : 'Post',
				data : { storeId : storeId, itemId : itemId, itemUnit : itemUnit, itemSize : itemSize, itemBrandName : itemBrandName, itemQuantity : itemQuantity, actualQuantity : actualQuantity, checkStatus : checkStatus, remark : remark, materialPurchesHistoriId : materialPurchesHistoriId},
				dataType : 'json',
				success : function(result)
						  {

						  }
				});
		
		}
	submitFunction();
}
function submitFunction()
{
submit.click();
}
function init()
{
	clearall();
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	

}

  $(function () {
    $('#materialDetailsTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>

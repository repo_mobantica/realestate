<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate |Edit Currency</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
 <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
    
  <style>
.alert {
    padding: 20px;
    background-color: #f44336;
    color: white;
    opacity: 1;
    transition: opacity 0.6s;
    margin-bottom: 15px;
}

.alert.success {background-color: #4CAF50;}
.alert.info {background-color: #2196F3;}
.alert.warning {background-color: #ff9800;}

.closebtn {
    margin-left: 15px;
    color: white;
    font-weight: bold;
    float: right;
    font-size: 22px;
    line-height: 20px;
    cursor: pointer;
    transition: 0.3s;
}

.closebtn:hover {
    color: black;
}
</style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

	<%@ include file="headerpage.jsp" %>
  
  	<%@ include file="menu.jsp" %>
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Currency Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Edit Currency</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="currencyform" action="${pageContext.request.contextPath}/EditCurrency" onSubmit="return validate()" method="post">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <span id="statusSpan" style="color:#FF0000"></span>
              <!-- /.form-group -->
             <div class="box-body">
          <div class="row">
            <div class="col-md-4">
                  <label for="currencyId">Currency Id</label>
                  <input type="text" class="form-control" id="currencyId" name="currencyId"  value="${currencyDetails[0].currencyId}" readonly>
             </div>
			</div>
			</div>
								
			<div class="box-body">
          <div class="row">
            <div class="col-md-6">
                  <label>Country</label> <label class="text-red">* </label>
                  <select class="form-control" name="countryId" id="countryId" onchange="getuniquecountryId(this.value)">
				 	<option selected="selected" value="${currencyDetails[0].countryId}">${countryName}</option>
				 	
				  	<c:forEach var="countryList" items="${countryList}" >
				  	  <c:choose>
				  	  	 <c:when test="${currencyDetails[0].countryId ne countryList.countryId}">
							<option value="${countryList.countryId}">${countryList.countryName}</option>
						 </c:when>
					  </c:choose>
					 </c:forEach>
                  </select>
                  
                  <span id="countryIdSpan" style="color:#FF0000"></span>
                </div>
                
            <div class="col-md-6">
                  <label for="currencyName">Currency Name</label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="currencyName" placeholder="Currency Name" name="currencyName" style="text-transform: capitalize;" value="${currencyDetails[0].currencyName}">
                <span id="currencyNameSpan" style="color:#FF0000"></span>
                </div>
		
				</div>
			</div>
			</div>
		
              	<input type="hidden" id="status" name="status" value="${Status}">	
				<input type="hidden" id="creationDate" name="creationDate" value="${currencyDetails[0].creationDate}">
				<input type="hidden" id="updateDate" name="updateDate" >
				<input type="hidden" id="userName" name="userName" placeholder="<%= session.getAttribute("user")%>">
				
				
            <!-- /.col -->
			
          </div>
          </br>
		     <div class="box-body">
              <div class="row">
                   <div class="col-xs-2">
                   <div class="col-xs-2">
           		 <a href="CurrencyMaster">   <button type="button" class="btn btn-block btn-primary" style="width:90px">  Back</button></a>
                </div>
                </div>
				  <div class="col-xs-3">
				  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
                </div>
					<div class="col-xs-1">
			  <button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
              
			     </div> 
              </div>
			  </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        
        </div>
      <!-- /.box -->

     
      </section>
	</form>
    <!-- /.content -->
    
  </div>
  

  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>

<script type="text/javascript"> 

      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
      
</script>
    
<script>

function getuniquecountryId()
{
	
	 var countryId = $('#countryId').val();
			$.ajax({

				url : '${pageContext.request.contextPath}/getcurrencyList',
			type : 'Post',
			data : { countryId : countryId},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{
								 
								for(var i=0;i<result.length;i++)
								{
									
									if(result[i].countryId==countryId)
										{
											  document.currencyform.countryId.focus();
											  $('#countryIdSpan').html('For this country the currency is already exist..!');
										}
									
									
								 } 
							} 
							else
							{
								alert("failure111");
							}

						}
			});	
}


function validate()
{ 
		$('#countryIdSpan').html('');
		$('#currencyNameSpan').html('');
		
		if(document.currencyform.countryId.value=="Default")
		{
			$('#countryIdSpan').html('Please, select country name..!');
			document.currencyform.currencyName.focus();
			return false;
		}
		
		//validation for currency name
		if(document.currencyform.currencyName.value=="")
		{
			$('#currencyNameSpan').html('Please, enter currency name..!');
			document.currencyform.currencyName.focus();
			return false;
		}
		else if(document.currencyform.currencyName.value.match(/^[\s]+$/))
		{
			$('#currencyNameSpan').html('Please, enter currency name..!');
			document.currencyform.currencyName.value="";
			document.currencyform.currencyName.focus();
			return false;
		}
		
}

function init()
{
	$('#countryIdSpan').html('');
	$('#currencyNameSpan').html('');
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	/* document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	 */document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	 if(document.currencyform.status.value=="Fail")
	 {
	  	alert("Sorry, record is present already..!");
	 }
	 else if(document.currencyform.status.value=="Success")
	 {
		 $('#statusSpan').html('Record saved successfully..!');
	 }
	 
  	 document.currencyform.countryId.focus();
}

</script>
</body>
</html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Edit Employee</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
     
  <style>
.alert {
    padding: 20px;
    background-color: #f44336;
    color: white;
    opacity: 1;
    transition: opacity 0.6s;
    margin-bottom: 15px;
}

.alert.success {background-color: #4CAF50;}
.alert.info {background-color: #2196F3;}
.alert.warning {background-color: #ff9800;}

.closebtn {
    margin-left: 15px;
    color: white;
    font-weight: bold;
    float: right;
    font-size: 22px;
    line-height: 20px;
    cursor: pointer;
    transition: 0.3s;
}

.closebtn:hover {
    color: black;
}
</style>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
    </script>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>

<div class="wrapper">

	<%@ include file="headerpage.jsp" %>
  
  	<%@ include file="menu.jsp" %>
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Employee Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Edit Employee</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="employeeform" action="${pageContext.request.contextPath}/EditEmployee" method="post" onSubmit="return validate()" enctype="multipart/form-data">
<section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
      <div class="box-body">
          <div class="row">
            <div class="col-md-7">
             <span id="statusSpan" style="color:#FF0000"></span>
				
			<div class="box-body">
              <div class="row">
                  <div class="col-xs-3">
                  <label >Employee Id</label>
                  <input type="text" class="form-control"  id="employeeId"name="employeeId" value="${employeeDetails[0].employeeId}" readonly>
                </div>               
                
              </div>
            </div>
			
			<div class="box-body">
              <div class="row">
                <div class="col-xs-4">
				<label for="employeefirstname">First Name</label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="employeefirstName" placeholder="First Name" name="employeefirstName" style="text-transform: capitalize;" value="${employeeDetails[0].employeefirstName}">
                 <span id="employeefirstNameSpan" style="color:#FF0000"></span>
                </div>
                <div class="col-xs-4">
				<label for="employeemiddlename">Middle Name </label>
                  <input type="text" class="form-control" id="employeemiddleName" placeholder="Middle Name" name="employeemiddleName" style="text-transform: capitalize;" value="${employeeDetails[0].employeemiddleName}">
                  <span id="employeemiddleNameSpan" style="color:#FF0000"></span>
                </div>
                <div class="col-xs-4">
				<label for="employeelastname">Last Name </label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="employeelastName" placeholder="Last Name" name="employeelastName" style="text-transform: capitalize;" value="${employeeDetails[0].employeelastName}">
                  <span id="employeelastNameSpan" style="color:#FF0000"></span>
                </div>
              </div>
            </div>
			
			   
			<div class="box-body">
              <div class="row">
			       
			  <div class="col-xs-4">
			   <label for="radiobutton">Gender</label></br>
                
                <c:choose>
			     <c:when test="${employeeDetails[0].employeeGender eq 'Male'}">
                 	<input type="radio" class="minimal" name="employeeGender" id="employeeGender" value="Male" checked="checked">Male
                 </c:when>
                 <c:otherwise>  
                 	<input type="radio" class="minimal" name="employeeGender" id="employeeGender" value="Male">Male
                 </c:otherwise>
			   </c:choose>
			    		&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
				  		
			   <c:choose>
				  <c:when test="${employeeDetails[0].employeeGender eq 'Female'}">
  					<input type="radio" class="minimal" name="employeeGender" id="employeeGender" value="Female" checked="checked">Female
  				  </c:when>
  				  <c:otherwise>
  				  	<input type="radio" class="minimal" name="employeeGender" id="employeeGender" value="Female">Female
  				  </c:otherwise>
  			   </c:choose>
  			   
                   <span id="employeeGenderSpan" style="color:#FF0000"></span>
                 </div>
                 
                  <div class="col-xs-4">
				   <label for="employeeMarried">Marital Status</label><label class="text-red">* </label>   <span id="employeeMarriedSpan" style="color:#FF0000"></span></br>
	   				
                <c:choose>
			     <c:when test="${employeeDetails[0].employeeMarried eq 'Married'}">
                 	<input type="radio" class="minimal" name="employeeMarried" id="employeeMarried" value="Married" checked="checked">Married
                 </c:when>
                 <c:otherwise>  
                 	<input type="radio" class="minimal" name="employeeMarried" id="employeeMarried" value="Married">Married
                 </c:otherwise>
			   </c:choose>
			    		&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
				  		
			   <c:choose>
				  <c:when test="${employeeDetails[0].employeeMarried eq 'Unmarried'}">
  					<input type="radio" class="minimal" name="employeeMarried" id="employeeMarried" value="Unmarried" checked="checked">Unmarried
  				  </c:when>
  				  <c:otherwise>
  				  	<input type="radio" class="minimal" name="employeeMarried" id="employeeMarried" value="Unmarried">Unmarried
  				  </c:otherwise>
  			   </c:choose>
  			   
                 </div>
                 
			     
				  <div class="col-xs-4">
			      <label>Date Of Birth </label> <label class="text-red">* </label>
				 <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input  type="text" class="form-control pull-right" id="employeeDob" name="employeeDob" value="${employeeDetails[0].employeeDob}">
                </div>
                <span id="employeeDobSpan" style="color:#FF0000"></span>
				   </div>
				 
				 </div>
            </div>
	</div>
          
            <div class="col-md-4">
			<div class="box-body">
              <div class="row">
               <div class="col-xs-3">
			   </div>
              </div>
              </div>
			 
			   <div class="box-body">
              <div class="row">
			<!--    <div class="col-xs-2">
			   </div> -->
			   <br/><br/>
                <div class="col-xs-4">
			   	  
			   <div class="pull-left image">
          		<img src="GetImage?profileImgPath=${profileImgPath}" id="profile-img-tag" class="img-circle" height="200px" width="200px alt="User Image">
        		</div>
			     </div> 
			     <div class="col-xs-3">
			   	
			     </div>
				<div class="col-xs-5">
			    <div class="form-group">
				 <br/>
                 <label >Select Photograph</label>
				 <input type="file" id="profile_img" name="profile_img" accept="image/x-png,image/gif,image/jpeg">

                <p class="help-block">Only select PNG, JPEG, and JPG formats(with size maximum 500KB).</p> 
                </div>
			    </div> 
			    
              </div>
            </div>
	 </div>
   </div>
<!--  </div> 
       
  <div class="box-body"> -->
          <div class="row">
            <div class="col-md-12">
              
            <div class="box-body">
              <div class="row">
                 
				  <div class="col-xs-3">
			      <label>Anniversary Date </label>
				  <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
	                <input type="text" class="form-control pull-right" id="employeeAnniversaryDate" name="employeeAnniversaryDate" value="${employeeDetails[0].employeeAnniversaryDate}">
	               </div>
	                <span id="employeeAnniversaryDateSpan" style="color:#FF0000"></span>
				   </div>
				
                 <div class="col-xs-3">
				 <label for="employeeSpuseName">Spouse Name </label>
                  <input type="text" class="form-control" id="employeeSpuseName" placeholder="Spouse Name" name="employeeSpuseName" style="text-transform: capitalize;" value="${employeeDetails[0].employeeSpuseName}">
                 <span id="employeeSpuseNameSpan" style="color:#FF0000"></span>
                </div>
               
				   <div class="col-xs-2">
				    <label>Education Qualification </label><label class="text-red">* </label>
				     <input type="text" class="form-control" id="employeeEducation" placeholder="Education Qualifaction" name="employeeEducation" style="text-transform:uppercase" value="${employeeDetails[0].employeeEducation}">
                    <span id="employeeEducationSpan" style="color:#FF0000"></span> 
                 </div>  
                 
                  <div class="col-xs-2">
			      <label>Department</label> <label class="text-red">* </label>
                  <select class="form-control" name="departmentId" id="departmentId" onchange="getDesignationList(this.value)">
				  	   <option selected="" value="${employeeDetails[0].departmentId}">${departmentName}</option>
                        <c:forEach var="departmentList" items="${departmentList}">
	                    <c:choose>
	                     <c:when test="${employeeDetails[0].departmentId ne departmentList.departmentId}">
	                 	  	<option value="${departmentList.departmentId}">${departmentList.departmentName}</option>
	                     </c:when>
	                    </c:choose>
				     </c:forEach>
                  </select>
                   <span id="departmentIdSpan" style="color:#FF0000"></span>  
			     </div> 
			     
                  <div class="col-xs-2">
			      <label>Designation  </label> <label class="text-red">* </label>
                  <select class="form-control" name="designationId" id="designationId">
				  		<option selected="" value="${employeeDetails[0].designationId}">${designationName}</option>
                  </select>
                   <span id="designationIdSpan" style="color:#FF0000"></span>  
			     </div>      
			 </div>
            </div>
			
			<div class="box-body">
              <div class="row">
                  <div class="col-xs-3">
			      <label for="employeeaddress">Address </label> <label class="text-red">* </label>
                  <textarea class="form-control" rows="1" id="employeeAddress" placeholder="Address" name ="employeeAddress">${employeeDetails[0].employeeAddress}</textarea>
			      <span id="employeeAddressSpan" style="color:#FF0000"></span>
			      </div> 
			
       
                  <div class="col-xs-3">
                   <label for="pincode">Pin Code </label> <label class="text-red">* </label>
                   <input type="text" class="form-control" id="areaPincode" placeholder="Pin Code" name="areaPincode"  value="${employeeDetails[0].areaPincode}">
			       <span id="areaPincodeSpan" style="color:#FF0000"></span>
			     </div> 
        
                  <div class="col-xs-3">
			       <label for="pancardno">Pan Card No </label> <label class="text-red">* </label>
                   <input type="text" class="form-control" id="employeePancardno" placeholder="Pan Card No " name="employeePancardno" style="text-transform:uppercase" value="${employeeDetails[0].employeePancardno}">
                   <span id="employeePancardnoSpan" style="color:#FF0000"></span>
			     </div> 
			     
				  <div class="col-xs-3">
			        <label for="adharcardno">Aadhar Card No</label> <label class="text-red">* </label>
                    <input type="text" class="form-control" id="employeeAadharcardno" placeholder="Adhar Card No " name="employeeAadharcardno" style="text-transform:uppercase" value="${employeeDetails[0].employeeAadharcardno}" >
			        <span id="employeeAadharcardnoSpan" style="color:#FF0000"></span>
			      </div> 
			     
              </div>
            </div>
			  
		<div class="box-body">
          <div class="row">
             
                  <div class="col-xs-3">
                    <label for="employeeEmailId">Personal Email ID </label> <label class="text-red">* </label>
				    <div class="input-group">
                	 <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                	 <input type="text" class="form-control" placeholder="Email" id="employeeEmailId" name="employeeEmailId" value="${employeeDetails[0].employeeEmailId}" onchange="CheckEmailId(this.value)" >
                     <span id="employeeEmailIdSpan" style="color:#FF0000"></span>
                    </div>
			     </div> 
			     
				 <div class="col-xs-3">
			     <label>Personal Mobile No </label> <label class="text-red">* </label>
				  <div class="input-group">
                  <div class="input-group-addon">
                  <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask='"mask": "9999999999"' data-mask name="employeeMobileno" id="employeeMobileno" value="${employeeDetails[0].employeeMobileno}">
                  <span id="employeeMobilenoSpan" style="color:#FF0000"></span>
                  </div>
				 </div> 
				  
                  <div class="col-xs-3">
                    <label for="companyEmailId">Company Email ID </label>
				    <div class="input-group">
                	 <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                	 <input type="text" class="form-control" placeholder="Email" id="companyEmailId" name="companyEmailId" value="${employeeDetails[0].companyEmailId}">
                     <span id="companyEmailIdSpan" style="color:#FF0000"></span>
                    </div>
			     </div> 
			     
				 <div class="col-xs-3">
			      <label>Company Mobile No </label> 
				  <div class="input-group">
                  <div class="input-group-addon">
                  <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask='"mask": "9999999999"' data-mask name="companyMobileno" id="companyMobileno" value="${employeeDetails[0].companyMobileno}">
                  <span id="companyMobilenoSpan" style="color:#FF0000"></span>
                  </div>
				 </div> 
              
          </div>
        </div>
           
		  <div class="box-body">
              <div class="row">
               
			    <div class="col-xs-3">
			    <label for="joiningdate">Joining Date</label> <label class="text-red">* </label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                <input type="text" class="form-control pull-right" id="employeeJoiningdate" name="employeeJoiningdate" value="${employeeDetails[0].employeeJoiningdate}">
                <span id="employeeJoiningdateSpan" style="color:#FF0000"></span>
                </div>
				</div> 
              
                <div class="col-xs-3">
			       <label for="basicpay">Basic Pay </label> <label class="text-red">* </label>
                   <input type="text" class="form-control" id="employeeBasicpay" placeholder="Basic Pay" name="employeeBasicpay" value="${employeeDetails[0].employeeBasicpay}">
                   <span id="employeeBasicpaySpan" style="color:#FF0000"></span>
			     </div> 
			     
				<div class="col-xs-2">
			      <label for="hra">HRA </label>
                  <input type="text" class="form-control" id="employeeHra" placeholder="HRA " name="employeeHra" value="${employeeDetails[0].employeeHra}">
                  <span id="employeeHraSpan" style="color:#FF0000"></span>
			     </div> 
			    
                 <div class="col-xs-2">
			       <label for="da">DA </label>
                   <input type="text" class="form-control" id="employeeDa" placeholder="DA" name="employeeDa" value="${employeeDetails[0].employeeDa}">
                   <span id="employeeDaSpan" style="color:#FF0000"></span>
			     </div> 
				 
               <div class="col-xs-2">
			      <label for="ca">CA </label>
                  <input type="text" class="form-control" id="employeeCa" placeholder="CA" name="employeeCa" value="${employeeDetails[0].employeeCa}">
                  <span id="employeeCaSpan" style="color:#FF0000"></span>
			     </div>
			     
			   </div>
            </div>
            
    	 <div class="box-body">
              <div class="row">  
              
               <div class="col-xs-3">
			    <label for="pfno">P. F. A/C No</label>
                <input type="text" class="form-control" id="employeePfacno" placeholder="P. F. A/C No " name="employeePfacno" style="text-transform:uppercase" value="${employeeDetails[0].employeePfacno}">
                <span id="employeePfacnoSpan" style="color:#FF0000"></span>
			   </div> 
			   
			  <div class="col-xs-3">
			    <label for="estno">ESI No </label>
                <input type="text" class="form-control" id="employeeEsino" placeholder="ESI No" name="employeeEsino" style="text-transform:uppercase" value="${employeeDetails[0].employeeEsino}">
                <span id="employeeEsinoSpan" style="color:#FF0000"></span>
			  </div>
			 
               <div class="col-xs-2">
			     <label for="pl">PL </label>
                 <input type="text" class="form-control" id="employeePLleaves" placeholder="PL " name="employeePLleaves" value="${employeeDetails[0].employeePLleaves}">
                 <span id="employeePLleavesSpan" style="color:#FF0000"></span>
			    </div> 
			     
				 <div class="col-xs-2">
			   	 <label for="sl">SL </label>
                 <input type="text" class="form-control" id="employeeSLleaves" placeholder="SL " name="employeeSLleaves" value="${employeeDetails[0].employeeSLleaves}">
                 <span id="employeeSLleavesSpan" style="color:#FF0000"></span>
			     </div>
			     
				<div class="col-xs-2">
			   	 <label for="cl">CL </label>
                 <input type="text" class="form-control" id="employeeCLleaves" placeholder="CL " name="employeeCLleaves" value="${employeeDetails[0].employeeCLleaves}">
                 <span id="employeeCLleavesSpan" style="color:#FF0000"></span>
			     </div> 
              </div>
            </div>          
             
			 <div class="box-body">
              <div class="row">
                   
				  <div class="col-xs-3">
			    <label>Bank Name </label>
			      <input type="text" class="form-control" id="employeeBankName" placeholder="Bak Name" name="employeeBankName" value="${employeeDetails[0].employeeBankName}" style="text-transform: capitalize;">
			   <%--    
              <select class="form-control" name="employeeBankacname" id="employeeBankacname" onchange="getBranchList(this.value)">
				 	<option selected="" value="Default">-Select Bank-</option>
                    <c:forEach var="bankList" items="${bankList}">
                    <option value="${bankList.bankName}">${bankList.bankName}</option>
				    </c:forEach> 
                  </select>
                   --%>
                    <span id="employeeBankNameSpan" style="color:#FF0000"></span>
			     </div> 
			     
			     <div class="col-xs-3">
			    <label>Bank Branch Name </label> 
			    <input type="text" class="form-control" id="branchName" placeholder="Branch Name" name="branchName" value="${employeeDetails[0].branchName}" style="text-transform: capitalize;"> 
			         <%-- 
			         <select class="form-control" id="branchName" name="branchName" onchange="getBranchIfsc(this.value)">
                     <option selected="selected" value="Default">-Select Bank Branch-</option>
                     <c:forEach var="locationareaList" items="${bankBranchList}">
                     <option value="${bankBranchList.branchName}">${bankBranchList.branchName}</option>
				     </c:forEach>
                     </select>
                         --%>
				 <span id="bankBranchNameSpan" style="color:#FF0000"></span>
			     </div> 
			  
                 <div class="col-xs-3">
			     <label>IFSC </label>
                 <input type="text" class="form-control" id="bankifscCode" placeholder="Bank IFSC No " name="bankifscCode" style="text-transform:uppercase" value="${employeeDetails[0].bankifscCode}">
			     <span id="bankifscCodeSpan" style="color:#FF0000"></span>
			     </div> 
			     
                 <div class="col-xs-3">
			     <label for="bankacno">Bank A/C No </label>
                 <input type="text" class="form-control" id="employeeBankacno" placeholder="Bank A/C No "name="employeeBankacno" value="${employeeDetails[0].employeeBankacno}">
                 <span id="employeeBankacnoSpan" style="color:#FF0000"></span>
			     </div> 
			  
              </div>
            </div>
       
          <div class="box-body">
              <div class="row">
                  <div class="col-xs-3">
			   	<label for="">Employee Status </label>
                    <select class="form-control" name="status" id="status" >
				  		<option value="Active">Active</option>
				  		<option value="InActive">InActive</option>
                  </select>
                 <span id="StatusSpan" style="color:#FF0000"></span>
			     </div> 
						
              </div>
            </div>  
            	 
      <div class="box-body">
              <div class="row">
              </br>
                <div class="col-xs-1">
                </div>
                   <div class="col-xs-4">
                   <div class="col-xs-2">
                	<a href="EmployeeMaster"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
			     </div>
			     </div>
				  <div class="col-xs-4">
                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
              
			     </div>
					<div class="col-xs-2">
			  <button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
              
			     </div> 
			     <!-- 
			       <div class="col-xs-6">
			         <button type="button" class="btn btn-info pull-right" style="background:#48D1CC"><a href="ImportNewEmployee"> Import From Excel File</a></button>
             </div>
              -->
              </div>
	   </div>
			  			         
         </div>  
  			 
			  <input type="hidden" id="employeeStatus" name="employeeStatus" value="${employeeStatus}">	
			  <input type="hidden" id="creationDate" name="creationDate" value="${employeeDetails[0].creationDate}">
			  <input type="hidden" id="updateDate" name="updateDate" value="">
			  <input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>">            
 
 </div>
          
</div>
 </div>
</section>   
</form>
 </div>
  

  <!-- Control Sidebar -->
  <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->

<script>
function clearall()
{
	$('#employeefirstNameSpan').html('');
	$('#employeemiddleNameSpan').html('');
	$('#employeelastNameSpan').html('');
	$('#employeeGenderSpan').html('');
	$('#employeeMarriedSpan').html('');
	$('#employeeDobSpan').html('');
	$('#employeeAnniversaryDateSpan').html('');
	$('#employeeSpuseNameSpan').html('');
	$('#employeeEducationSpan').html('');
	$('#departmentIdSpan').html('');
	$('#designationIdSpan').html('');
	$('#employeeAddressSpan').html('');
	$('#countryIdSpan').html('');
	$('#stateIdSpan').html('');
	$('#cityIdSpan').html('');
	$('#locationareaIdSpan').html('');
	$('#areaPincodeSpan').html('');
	$('#employeePancardnoSpan').html('');
	$('#employeeAadharcardnoSpan').html('');
	$('#employeeEmailIdSpan').html('');
	$('#employeeMobilenoSpan').html('');
	$('#companyEmailIdSpan').html('');
	$('#companyMobilenoSpan').html('');
	$('#employeeJoiningdateSpan').html('');
	$('#employeeBasicpaySpan').html('');
	$('#employeeHraSpan').html('');
	$('#employeeDaSpan').html('');
	$('#employeeCaSpan').html('');
	$('#employeePfacnoSpan').html('');
	$('#employeeEsinoSpan').html('');
	$('#employeePLleavesSpan').html('');
	$('#employeeSLleavesSpan').html('');
	$('#employeeCLleavesSpan').html('');
	$('#employeeBankNameSpan').html('');
	$('#branchNameSpan').html('');
	$('#bankifscCodeSpan').html('');
	$('#employeeBankacnoSpan').html('');
}
function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#profile-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#profile_img").change(function(){
        readURL(this);
    });


    function CheckEmailId()
    {
   	 var employeeEmailId = $('#employeeEmailId').val();
   	 $('#employeeEmailIdSpan').html('');
   		
   		$.ajax({

   			url : '${pageContext.request.contextPath}/CheckEmailId',
   			type : 'Post',
   			data : { employeeEmailId : employeeEmailId},
   			dataType : 'json',
   			success : function(result)
   					  {
   							if (result) 
   							{
   									$('#employeeEmailIdSpan').html(' This emailid already present..!');
   									document.employeeform.employeeEmailId.value="";
   									document.employeeform.employeeEmailId.focus();
   							} 
   							else
   							{
   								alert("failure111");
   								//$("#ajax_div").hide();
   							}

   						}
   			});
   	 
    }
       
    function getDesignationList()
    {
    	 $("#designationId").empty();
    	 var departmentId = $('#departmentId').val();
    	 
    	$.ajax({

    		url : '${pageContext.request.contextPath}/getDesignationList',
    		type : 'Post',
    		data : { departmentId : departmentId},
    		dataType : 'json',
    		success : function(result)
    				  {
    						if (result) 
    						{
    							var option = $('<option/>');
    							option.attr('value',"Default").text("-Select Designation-");
    							$("#designationId").append(option);
    							
    							for(var i=0;i<result.length;i++)
    							{
    								var option = $('<option />');
    							    option.attr('value',result[i].designationId).text(result[i].designationName);
    							    $("#designationId").append(option);
    							 } 
    						} 
    						else
    						{
    							alert("failure111");
    							//$("#ajax_div").hide();
    						}

    					}
    		});

    }
       
 function validate()
 {

	 clearall();
	    //validation for employee first name
	 
	    //validation for employee first name
		if(document.employeeform.employeefirstName.value=="")
		{
			$('#employeefirstNameSpan').html('First name should not be blank..!');
			document.employeeform.employeefirstName.focus();
			return false;
		}
		else if(document.employeeform.employeefirstName.value.match(/^[\s]+$/))
		{
			$('#employeefirstNameSpan').html('First name should not be blank..!');
			document.employeeform.employeefirstName.value="";
			document.employeeform.employeefirstName.focus();
			return false;
		}
	
	    //validation for employee middle name
	    if(document.employeeform.employeemiddleName.value.length!=0)
	    {
	    	if(document.employeeform.employeemiddleName.value.match(/^[\s]+$/))
	    	{
	    		document.employeeform.employeemiddleName.value="";
	    	}
	    	else if(!document.employeeform.employeemiddleName.value.match(/^[A-Za-z\s.]+$/))
			{
	    		$('#employeemiddleNameSpan').html('Middle name should contain alphabets only..!');
				document.employeeform.employeemiddleName.value="";
				document.employeeform.employeemiddleName.focus();
				return false;
			}
	    }
	    
	    //validation for employee last name
		if(document.employeeform.employeelastName.value=="")
		{
			$('#employeelastNameSpan').html('Last name should not be blank..!');
			document.employeeform.employeelastName.focus();
			return false;
		}
		else if(document.employeeform.employeelastName.value.match(/^[\s]+$/))
		{
			$('#employeelastNameSpan').html('Last name should not be blank..!');
			document.employeeform.employeelastName.value="";
			document.employeeform.employeelastName.focus();
			return false;
		}

		//validation for gender selection
	    if(( document.employeeform.employeeGender[0].checked == false ) && ( document.employeeform.employeeGender[1].checked == false ) )
		{
	    	$('#employeeGenderSpan').html('Please, choose emp. Gender: Male or Female..!');
			document.employeeform.employeeGender[0].focus();
			return false;
		}
	    
	  //validation for gender selection
	    if(( document.employeeform.employeeMarried[0].checked == false ) && ( document.employeeform.employeeMarried[1].checked == false ) )
		{
	    	$('#employeeMarriedSpan').html('Please, choose emp. marital..!');
			document.employeeform.employeeMarried[0].focus();
			return false;
		} 
	    
	    
		 //validation employee for date of birth
		if(document.employeeform.employeeDob.value=="")
		{
			$('#employeeDobSpan').html('Please, enter employee date of birth');
			document.employeeform.employeeDob.value="";
			document.employeeform.employeeDob.focus();
			return false;
		}
		else if(document.employeeform.employeeDob.value!="")
		{
			var today = new Date();
			var dd1 = today.getDate();
			var mm1 = today.getMonth()+1;
			var yy1 = today.getFullYear();
			
	    	var dob= document.employeeform.employeeDob.value;
	   		var dob2=dob.split("/");
	   
	    	var yydiff=parseInt(yy1)-parseInt(dob2[2]);
		    if(yydiff<=18)
		    {
		    		 $('#employeeDobSpan').html(' date of birth should be 18 or 18+ years..!');
		    		document.employeeform.employeeDob.value="";
		    		document.employeeform.employeeDob.focus();
		    		return false;
		    }
		}
	    
		if(document.employeeform.employeeEducation.value=="")
		{
			$('#employeeEducationSpan').html('Please, enter employee Education ..!');
			document.employeeform.employeeEducation.focus();
			return false;
		}
		else if(document.employeeform.employeeEducation.value.match(/^[\s]+$/))
		{
			$('#employeeEducationSpan').html('Please, enter employee Education  ...!');
			document.employeeform.employeeEducation.focus();
			return false;
		}
		 
		if(document.employeeform.departmentId.value=="Default")
		{
			$('#departmentIdSpan').html('Please, select employee Department..!');
			document.employeeform.departmentId.focus();
			return false;
		}
	 
	    //validation for employee designation
		if(document.employeeform.designationId.value=="Default")
		{
			$('#designationIdSpan').html('Please, select employee designation..!');
			document.employeeform.designationId.focus();
			return false;
		}
	    
	    //validation for employee address----------------------
		if(document.employeeform.employeeAddress.value=="")
		{
			$('#employeeAddressSpan').html('Please, enter employee address name..!');
			document.employeeform.employeeAddress.focus();
			return false;
		}
		else if(document.employeeform.employeeAddress.value.match(/^[\s]+$/))
		{
			$('#employeeAddressSpan').html('Please, enter employee address ...!');
			document.employeeform.employeeAddress.focus();
			return false;
		}
		
	    
	    //validation for employee country
		if(document.employeeform.countryId.value=="Default")
		{
			$('#countryIdSpan').html('Please, select employee country name..!');
			document.employeeform.countryId.focus();
			return false;
		}
	    
	    //validation for employee state
		if(document.employeeform.stateId.value=="Default")
		{
			$('#stateIdSpan').html('Please, select employee state name..!');
			document.employeeform.stateId.focus();
			return false;
		}
	    
	    //validation for employee city
		if(document.employeeform.cityId.value=="Default")
		{
			$('#cityIdSpan').html('Please, select employee city name..!');
			document.employeeform.cityId.focus();
			return false;
		}
	    
	    //validation for employee location area
		if(document.employeeform.locationareaId.value=="Default")
		{
			$('#locationareaIdSpan').html('Please, select employee area name..!');
			document.employeeform.locationareaId.focus();
			return false;
		}
	    
		//validation for employee pan card
		if(document.employeeform.employeePancardno.value=="")
		{
			$('#employeePancardnoSpan').html('Please, enter Pancard number..!');
			document.employeeform.employeePancardno.focus();
			return false;
		}
		else if(!document.employeeform.employeePancardno.value.match(/^[A-Za-z]{5}[0-9]{4}[A-z]{1}$/))
		{
			$('#employeePancardnoSpan').html('PAN number must start with 5 alphabets follwed by 4 digit number and 1 alphabet..!');
			document.employeeform.employeePancardno.value="";
			document.employeeform.employeePancardno.focus();
			return false;
		}
		
		//validation for employee aadhar card number
		if(document.employeeform.employeeAadharcardno.value=="")
		{
			$('#employeeAadharcardnoSpan').html('Please, enter Aadhar card number..!');
			document.employeeform.employeeAadharcardno.focus();
			return false;
		}
		else if(!document.employeeform.employeeAadharcardno.value.match(/^\d{4}\d{4}\d{4}$/))
		{
			$('#employeeAadharcardnoSpan').html('Aadhar card number should be 12 digit number only..!');
			document.employeeform.employeeAadharcardno.value="";
			document.employeeform.employeeAadharcardno.focus();
			return false;
		}
	   
	    //validation for employee email address
		if(document.employeeform.employeeEmailId.value=="")
		{
			$('#employeeEmailIdSpan').html('Email Id should not be blank..!');
			document.employeeform.employeeEmailId.focus();
			return false;
		}
		else if(!document.employeeform.employeeEmailId.value.match(/^(([\-\w]+)\.?)+@(([\-\w]+)\.?)+\.[a-z]{2,4}$/))
		{
			$('#employeeEmailIdSpan').html(' enter valid email id..!');
			document.employeeform.employeeEmailId.value="";
			document.employeeform.employeeEmailId.focus();
			return false;
		}
		
		//validation for employee mobile number------------------------------
		if(document.employeeform.employeeMobileno.value=="")
		{
			$('#employeeMobilenoSpan').html('Please, enter employee mobile number..!');
			document.employeeform.employeeMobileno.value="";
			document.employeeform.employeeMobileno.focus();
			return false;
		}
		else if(!document.employeeform.employeeMobileno.value.match(/^[0-9]{10}$/))
		{
			$('#employeeMobilenoSpan').html(' enter valid employee mobile number..!');
			document.employeeform.employeeMobileno.value="";
			document.employeeform.employeeMobileno.focus();
			return false;	
		}
		
		//validation for employee email address
		if(document.employeeform.companyEmailId.value.length!=0)
		{
		  if(!document.employeeform.companyEmailId.value.match(/^(([\-\w]+)\.?)+@(([\-\w]+)\.?)+\.[a-z]{2,4}$/))
			{
				$('#companyEmailIdSpan').html(' enter valid email id..!');
				document.employeeform.companyEmailId.value="";
				document.employeeform.companyEmailId.focus();
				return false;
			}
		}
		//validation for employee mobile number------------------------------
	   if(document.employeeform.companyMobileno.value.length!=0)
		{
		 if(!document.employeeform.companyMobileno.value.match(/^[0-9]{10}$/))
			{
				$('#companyMobilenoSpan').html(' enter valid employee mobile number..!');
				document.employeeform.companyMobileno.value="";
				document.employeeform.companyMobileno.focus();
				return false;	
			}
		}
		
		//validation for joining date
		if(document.employeeform.employeeJoiningdate.value=="")
		{
			$('#employeeJoiningdateSpan').html('Please, select joining date..!');
			document.employeeform.employeeJoiningdate.focus();
			return false;
		}
		
		
		//validation for employee basic pay
		if(document.employeeform.employeeBasicpay.value=="")
		{
			$('#employeeBasicpaySpan').html('Please, enter basic pay amount..!');
			document.employeeform.employeeBasicpay.focus();
			return false;
		}
		if(!document.employeeform.employeeBasicpay.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			 if(!document.employeeform.employeeBasicpay.value.match(/^[0-9]+$/))
				 {
					$('#employeeBasicpaySpan').html('Please, enter valid Basic Pay amount..!');
					document.employeeform.employeeBasicpay.value="";
					document.employeeform.employeeBasicpay.focus();
					return false;
				}
		}
		
		//validation for employee HRA
		if(document.employeeform.employeeHra.value.length!=0)
		{
			if(!document.employeeform.employeeHra.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
			{
				 if(!document.employeeform.employeeHra.value.match(/^[0-9]+$/))
					 {
						$('#employeeHraSpan').html('Please, enter valid HRA amount..!');
						document.employeeform.employeeHra.value="";
						document.employeeform.employeeHra.focus();
						return false;
					}
			}
		}
		
		//validation for employee DA
		if(document.employeeform.employeeDa.value.length!=0)
		{
			if(!document.employeeform.employeeDa.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
			{
				 if(!document.employeeform.employeeDa.value.match(/^[0-9]+$/))
					 {
						$('#employeeDaSpan').html('Please, enter valid DA amount..!');
						document.employeeform.employeeDa.value="";
						document.employeeform.employeeDa.focus();
						return false;
					}
			}
		}
		
		//validation for employee CA
		if(document.employeeform.employeeCa.value.length!=0)
		{

			if(!document.employeeform.employeeCa.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
			{
				 if(!document.employeeform.employeeCa.value.match(/^[0-9]+$/))
					 {
						$('#employeeCaSpan').html('Please, enter valid CA amount..!');
						document.employeeform.employeeCa.value="";
						document.employeeform.employeeCa.focus();
						return false;
					}
			}
		}
		
		//validation for employee PF a/c no
		if(document.employeeform.employeePfacno.value.length!=0)
		{
			if(!document.employeeform.employeePfacno.value.match(/^[A-Za-z0-9/]+$/))
			{
				$('#employeePfacnoSpan').html('Please, enter valid P.F. a/c number..!');
				document.employeeform.employeePfacno.value="";
				document.employeeform.employeePfacno.focus();
				return false;
			}
		}
		
		//validation for employee ESI no
		if(document.employeeform.employeeEsino.value.length!=0)
		{
			if(!document.employeeform.employeeEsino.value.match(/^[0-9]{2}[-]{1}[0-9]{2}[-]{1}[0-9]{6}[-]{1}[0-9]{3}[-]{1}[0-9]{4}$/))
			{
				$('#employeeEsinoSpan').html('Please, enter valid ESI number..!');
				document.employeeform.employeeEsino.value="";
				document.employeeform.employeeEsino.focus();
				return false;
			}
		}
		
		//validation for PL leaves
		if(document.employeeform.employeePLleaves.value.length!=0)
		{
			if(!document.employeeform.employeePLleaves.value.match(/^[0-9]{1,2}$/))
			{
				$('#employeePLleavesSpan').html('Please, enter valid PL leaves..!');
				document.employeeform.employeePLleaves.value="";
				document.employeeform.employeePLleaves.focus();
				return false;
			}
		}
		
		//validation for SL leaves 
		if(document.employeeform.employeeSLleaves.value.length!=0)
		{
			if(!document.employeeform.employeeSLleaves.value.match(/^[0-9]{1,2}$/))
			{
				$('#employeeSLleavesSpan').html('Please, enter valid SL leaves..!');
				document.employeeform.employeeSLleaves.value="";
				document.employeeform.employeeSLleaves.focus();
				return false;
			}
		}
		
		//validation for CL leaves
		if(document.employeeform.employeeCLleaves.value.length!=0)
		{
			if(!document.employeeform.employeeCLleaves.value.match(/^[0-9]{1,2}$/))
			{
				$('#employeeCLleavesSpan').html('Please, enter valid CL leaves..!');
				document.employeeform.employeeCLleaves.value="";
				document.employeeform.employeeCLleaves.focus();
				return false;
			}
		}
		
		 if(document.employeeform.employeeBankacno.value.length!=0)
			{ 
			 if(!document.employeeform.employeeBankacno.value.match(/^[0-9]+$/))
				{
					$('#employeeBankacnoSpan').html('Please, enter valid employee bank Ac/No..!');
					document.employeeform.employeeBankacno.value="";
					document.employeeform.employeeBankacno.focus();
					return false;
				}
			}
		 
		 
}
 
function init()
{
	 clearall();
		var date =  new Date();
		var year = date.getFullYear();
		var month = date.getMonth() + 1;
		var day = date.getDate();
		
		/* document.getElementById("creationDate").value = day + "/" + month + "/" + year;
		 */document.getElementById("updateDate").value = day + "/" + month + "/" + year;
		
	 if(document.employeeform.employeeStatus.value == "Fail")
	 {
	  	alert("Sorry, record is present already..!");
	 }
	 else if(document.employeeform.employeeStatus.value == "Success")
	 {
		 $('#statusSpan').html('Record saved successfully..!');
	 }
	 
     document.employeeform.employeefirstName.focus();
}


function getpinCode()
{

	 $("#areaPincode").empty();
	 var locationareaId = $('#locationareaId').val();
	 var cityId = $('#cityId').val();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getallAreaList',
		type : 'Post',
		data : { locationareaId : locationareaId, cityId : cityId, stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							
							for(var i=0;i<result.length;i++)
							{
							 $('#areaPincode').val(result[i].pinCode);
								
							 } 
						
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}

function getStateList()
{
	 $("#stateId").empty();
	 $("#cityId").empty();
	 $("#locationareaId").empty();
	 
	 var countryId = $('#countryId').val();
	 
	$.ajax({

		url : '${pageContext.request.contextPath}/getStateList',
		type : 'Post',
		data : { countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select State-");
							$("#stateId").append(option);
							
							var option1 = $('<option/>');
							option.attr('value',"Default").text("-Select City-");
							$("#cityId").append(option1);
							
							var option2 = $('<option/>');
							option.attr('value',"Default").text("-Select Location-");
							$("#locationareaId").append(option2);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].stateId).text(result[i].stateName);
							    $("#stateId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
}//end of get State List


function getCityList()
{
	 $("#cityId").empty();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();
	$.ajax({

		url : '${pageContext.request.contextPath}/getCityList',
		type : 'Post',
		data : { stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select City-");
							$("#cityId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].cityId).text(result[i].cityName);
							    $("#cityId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
}//end of get City List

function getLocationAreaList()
{
	 $("#locationareaId").empty();
	 var cityId = $('#cityId').val();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getLocationAreaList',
		type : 'Post',
		data : { cityId : cityId, stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Location Area-");
							$("#locationareaId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].locationareaId).text(result[i].locationareaName);
							    $("#locationareaId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get locationarea List


function getuniqueaadharnumberName()
{
	
	 var employeeAadharcardno = $('#employeeAadharcardno').val();
	 
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getaadharList',
			type : 'Post',
			data : { employeeAadharcardno : employeeAadharcardno},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{
								 
								for(var i=0;i<result.length;i++)
								{
									
									if(result[i].employeeAadharcardno==employeeAadharcardno)
										{
										
											  document.employeeform.employeeAadharcardno.focus();
											  $('#employeeAadharcardnoSpan').html('This aadhar number is already exist..!');
										}
									
									
								 } 
							} 
							else
							{
								alert("failure111");
							}

						}
			});	
}

function getBranchList()
{
	 
	 $("#branchName").empty();
	 var bankName = $('#employeeBankacname').val();
	
	 $.ajax({

		url : '${pageContext.request.contextPath}/getBranchList',
		type : 'Post',
		data : { bankName : bankName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Branch Name-");
							$("#branchName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].branchName).text(result[i].branchName);
							    $("#branchName").append(option);
							} 
						} 
						else
						{
							alert("failure111");
						}

					}
		});	
}

function getBranchIfsc()
{
	 //$("#bankifscCode").val('');
	 var bankName = $('#employeeBankacname').val();
     var branchName = $('#branchName').val();
	 $.ajax({

		url : '${pageContext.request.contextPath}/getBranchIfsc',
		type : 'Post',
		data : { bankName : bankName, branchName : branchName },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							$('#bankifscCode').val(result[0].bankifscCode);
							/* for(var i=0;i<result.length;i++)
							{
								 var option = $('<option />');
							    option.attr('value',result[i].branchName).text(result[i].branchName);
							    $("#bankifscCode").append(option); 
							}   */
						} 
						else
						{
							alert("failure111");
						}

					}
		});
}



  $(function ()
  {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    
 $('#employeeDob').datepicker({
    autoclose: true
    })
    
 $('#employeeJoiningdate').datepicker({
      autoclose: true
    })

$('#employeeAnniversaryDate').datepicker({
    autoclose: true
  })
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>
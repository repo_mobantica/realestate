<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>RealEstate | Material List</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

 
  <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">
	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

    <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
    <section class="content-header">
      <h1>
        Material List:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Store</a></li>
        <li class="active"> Material List</li>
      </ol>
    </section>
    <!-- Main content -->
	<form name="supplierquotationForm" id="StorePurchesform" action="${pageContext.request.contextPath}/AddSupplierQuotation" onSubmit="return validate()" method="post">


<section class="content">
   
      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <span id="statusSpan" style="color:#FF0000"></span>
        <!-- /.box-header -->
        
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
                  <span id="statusSpan" style="color:#FF0000"></span>
                   
                  <div class="box-body">
                     <div class="row">
                     
                      <div class="col-xs-2">
				      <label for="quotationId">Quotation Id </label>
				      <input type="text" class="form-control" id="quotationId" name="quotationId" value="${quotationId}" readonly>
				      <input type="hidden" class="form-control" id="quotationHistoriId" name="quotationHistoriId" value="${quotationHistoriId}" readonly>
				      
     			    </div>  
     			    
     			        
                   <div class="col-md-3">
	                  <label>Supplier Name</label><label class="text-red">* </label>
                      <select class="form-control" id="supplierId" name="supplierId">
				      		<option selected="selected" value="Default">-Select Supplier Name-</option>
                      	<c:forEach var="supplierList" items="${supplierList}">
	                    	<option value="${supplierList.supplierId}">${supplierList.supplierfirmName}</option>
	                  	</c:forEach>
                      </select>
                      <span id="supplierIdSpan" style="color:#FF0000"></span>
                   </div> 
     			                 
              	    </div>
            	 </div> 
              
            </div>
          </div>
        </div>
        
        <div class="panel box box-danger"></div>
   
              
          <div class="box-body">
           <div class="row">        
        	<div class="col-xs-12">
        	<h2 class="box-title">Material List</h2>
              <table id="materialDetailsTable" class="table table-bordered">
                <thead>
                <tr bgcolor="#4682B4">
               		<th style="width:20px">Sr.No</th>
               		<td hidden="hidden"> </td> 
                  	<th>Sub-Category</th>
                  	<th>Item Name</th>
                  	<th>Quantity</th>
                  	<th>Size</th>
                  	<th>Unit</th>
                    <th>Brand</th>
                   
                    <th>Rate</th>
                    <th>Total</th>
                    <th>Discount%</th>
                    <th>Discount Amount</th>
                    <th>GST%</th>
                    <th>GST Amount</th>
                    <th>Grand Total</th>
                  </tr>
                </thead>
                <tbody>
            
                  <c:forEach items="${materialpurchaserequisitionhistory}" var="materialpurchaserequisitionhistory" varStatus="loopStatus">
                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                      <td>${loopStatus.index+1}</td>
                      <td hidden="hidden">${materialpurchaserequisitionhistory.itemId}</td>
                      <td>${materialpurchaserequisitionhistory.subsuppliertypeId}</td>
                      <td>${materialpurchaserequisitionhistory.itemName}</td>
                      <td>${materialpurchaserequisitionhistory.itemQuantity}</td> 
                      <td>${materialpurchaserequisitionhistory.itemSize}</td>
                      <td>${materialpurchaserequisitionhistory.itemunitName}</td>
                      <td contenteditable='true' onkeyup="javascript:calcuate(${loopStatus.index+1});"></td>
                      
                      <td contenteditable='true' onkeyup="javascript:calcuate(${loopStatus.index+1});">${materialpurchaserequisitionhistory.itemInitialPrice}</td>
                      <td>0</td>
                  	  <td contenteditable='true' onkeyup="javascript:calcuate(${loopStatus.index+1});">${materialpurchaserequisitionhistory.itemInitialDiscount}</td>
                      <td>0</td>
                      <td contenteditable='true' onkeyup="javascript:calcuate(${loopStatus.index+1});">${materialpurchaserequisitionhistory.gstPer}</td>
                      <td>0</td>
                      <td>0</td>
                      </tr>
				 </c:forEach>
		       <tr>
			          <td></td>
                      <td hidden="hidden"></td>
                      <td></td>
                      <td></td> 
                      <td></td> 
                      <td></td>
                      <td></td>
                      <td></td>
                      <td><label>Total</label></td>
                      <td id="total1"></td> 
                      <td></td> 
                      <td id="discountAmount1"></td>
                      <td></td> 
                      <td id="gstAmount1"></td> 
                      <td id="grandTotal1"><label> </label></td>
			   
			    </tr>
                </tbody>
         
              </table>
            </div>
            <div class="col-xs-5">
            </div>
            	<div class="col-xs-3">
            	
            	 <span id="itemBrandNameSpan" style="color:#FF0000"></span>
            	</div>
   	
			   	<div class="col-xs-4">
			   	
			   	</div>
   	<br/> <br/> <br/> <br/>
   	<div class="col-xs-1">
         </div>
	       <div class="col-xs-4">
	       <div class="col-xs-2">
              <a href="AllSupplierQuotationList?requisitionId=${requisitionId}"><button type="button" class="btn btn-block btn-primary" value="reset" style="width:90px">Back</button></a>
			</div>
			</div>
			    
	   		<div class="col-xs-2">
	            <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
			</div>
			    
			<div class="col-xs-3">
		  		<button type="button" class="btn btn-info pull-right" onclick="return AddAllData()">Submit</button>
			</div>
			
			   <div class="col-xs-3" hidden="hidden">
		  			<button type="submit" class="btn btn-info pull-right" name="submit" id="submit">Submit</button>
			    </div> 
			    
   	         
         <input type="hidden" id="requisitionId" name="requisitionId" value="${requisitionId}" >
     	<input type="hidden" id="total" name="total">
     	<input type="hidden" id="discountAmount" name="discountAmount">
     	<input type="hidden" id="gstAmount" name="gstAmount">
     	<input type="hidden" id="grandTotal" name="grandTotal">		
     	
				 <input type="hidden" id="creationDate" name="creationDate" value="">
				 <input type="hidden" id="updateDate" name="updateDate" value="">
				 <input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>">			 
		
		
     	<input type="hidden" id="itemIds" name="itemIds" value="">
     	<input type="hidden" id="itemSizes" name="itemSizes" value="">
     	<input type="hidden" id="itemUnits" name="itemUnits" value="">
     	<input type="hidden" id="itemBrandNames" name="itemBrandNames" value="">
     	<input type="hidden" id="quantitys" name="quantitys" value="">
     	<input type="hidden" id="rates" name="rates" value="">
     	<input type="hidden" id="totals" name="totals" value="">
     	<input type="hidden" id="discountPers" name="discountPers" value="">
     	<input type="hidden" id="discountAmounts" name="discountAmounts" value="">
     	<input type="hidden" id="gstPers" name="gstPers" value="">
     	<input type="hidden" id="gstAmounts" name="gstAmounts" value="">
     	<input type="hidden" id="grandTotals" name="grandTotals" value="">
     
     	
     </div>
  </div>
          

</div>
  </section>

</form>
</div>

 <%@ include file="footer.jsp" %>
 
 <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- page script -->
<script>

function calcuate(i)
{
	var oTable = document.getElementById('materialDetailsTable');

	var oCells = oTable.rows.item(i).cells;
	
	var quantity=0;
	quantity=Number(oCells.item(4).innerHTML); 
	
	var rate=0;
	rate=Number(oCells.item(8).innerHTML); 

	var discountPer1=0;
	discountPer1=Number(oCells.item(10).innerHTML); 
	var gstPer1=0;
	gstPer1=Number(oCells.item(12).innerHTML); 
	
	var discountAmount1=0;
	var gstAmount1=0;
	var grandTotal1=0;
	
	var total1=0;
	total1=quantity*rate;
	
	oCells.item(9).innerHTML=total1.toFixed(1);
	discountAmount1=(total1/100)*discountPer1;
	
	var ab=discountAmount1.toFixed(0);
	oCells.item(11).innerHTML=ab;
	var a=0;
	a=total1-discountAmount1;
	gstAmount1=(a/100)*gstPer1;
	
	var xy=gstAmount1.toFixed(0);
	oCells.item(13).innerHTML=xy;
	grandTotal1=a+gstAmount1;
	var pq=grandTotal1.toFixed(0);
	oCells.item(14).innerHTML=pq;
	
	var rowLength = oTable.rows.length;
	var grandTotal=0;
	var total=0;
	var discountAmount=0;
	var gstAmount=0;
	
	for (var j = 1; j < rowLength-1; j++){

		   var oCells = oTable.rows.item(j).cells;

		   var cellLength = oCells.length;
		  
		   total=total+Number(oCells.item(9).innerHTML);
		   discountAmount=discountAmount+Number(oCells.item(11).innerHTML);
		   gstAmount=gstAmount+Number(oCells.item(13).innerHTML);
		   grandTotal=grandTotal+Number(oCells.item(14).innerHTML);
			
			
	}		
	document.getElementById('total1').innerHTML = total;
	document.supplierquotationForm.total.value=total;
	
	document.getElementById('discountAmount1').innerHTML = discountAmount;
	document.supplierquotationForm.discountAmount.value=discountAmount;
	document.getElementById('gstAmount1').innerHTML = gstAmount;
	document.supplierquotationForm.gstAmount.value=gstAmount;
	
	document.getElementById('grandTotal1').innerHTML = grandTotal;
	document.supplierquotationForm.grandTotal.value=grandTotal; 
	//document.getElementById('grandTotal').innerHTML = grandTotal;
}




function AddAllData()
{
	$('#itemBrandNameSpan').html('');
	$('#supplierIdSpan').html('');
	
	
	if(document.supplierquotationForm.supplierId.value=="Default")
	{
		$('#supplierIdSpan').html('Please, select Supplier name..!');
		document.supplierquotationForm.supplierId.focus();
		return false;
	}
	
	var quotationId = $('#quotationId').val();
	var quotationHistoriId = Number($('#quotationHistoriId').val());
	var itemId;
	var itemUnit;
	var itemSize;
	var itemBrandName;
	var quantity;
	var rate;
	var total;
	var discountPer;
	var discountAmount;
	var gstPer;
	var gstAmount;
	var grandTotal;
	
	var itemIds="";
	var itemSizes="";
	var itemUnits="";
	var itemBrandNames="";
	var quantitys="";
	var rates="";
	var totals="";
	var discountPers="";
	var discountAmounts="";
	var gstPers="";
	var gstAmounts="";
	var grandTotals="";
	
	var oTable = document.getElementById('materialDetailsTable');
	var rowLength = oTable.rows.length;

	for (var i = 1; i < rowLength-1; i++){
		
		calcuate(i);
		   var oCells = oTable.rows.item(i).cells;

		   var cellLength = oCells.length;
		   
			itemId=oCells.item(1).innerHTML;
			quantity = oCells.item(4).innerHTML;
			itemSize= oCells.item(5).innerHTML;
			itemUnit= oCells.item(6).innerHTML;
			itemBrandName = oCells.item(7).innerHTML;
			rate = oCells.item(8).innerHTML;
			total= oCells.item(9).innerHTML;
			
			discountPer= oCells.item(10).innerHTML;
			discountAmount= oCells.item(11).innerHTML;
			gstPer= oCells.item(12).innerHTML;
			gstAmount= oCells.item(13).innerHTML;
			grandTotal= oCells.item(14).innerHTML;
			
			
			if(itemBrandName=="")
				{
				$('#itemBrandNameSpan').html('Please, select item Brand name..!');
				return false;
				}
			
		        itemIds=itemIds+itemId+"###!###";
		        itemSizes=itemSizes+itemSize+".###!###";
		  	 	itemUnits=itemUnits+itemUnit+"###!###";
		  	 	itemBrandNames=itemBrandNames+itemBrandName+"###!###";
		  	 	quantitys=quantitys+quantity+"###!###";
		  	 	rates=rates+rate+"###!###";
		  	 	totals=totals+total+"###!###";
		  	 	discountPers=discountPers+discountPer+"###!###";
		  	 	discountAmounts=discountAmounts+discountAmount+"###!###";
		  		gstPers=gstPers+gstPer+"###!###";
		  		gstAmounts=gstAmounts+gstAmount+"###!###";
		  	 	grandTotals=grandTotals+grandTotal+"###!###";
		      

			
			/* 
			quotationHistoriId =quotationHistoriId+i-1;
			
			 $.ajax({

				url : '${pageContext.request.contextPath}/SaveSupplierQuotation',
				type : 'Post',
				data : { quotationHistoriId : quotationHistoriId, quotationId : quotationId, itemId : itemId, itemSize : itemSize, itemUnit : itemUnit, itemBrandName : itemBrandName, quantity: quantity, rate : rate, total : total, discountPer : discountPer, discountAmount : discountAmount, gstPer : gstPer, gstAmount : gstAmount, grandTotal : grandTotal},
				dataType : 'json',
				success : function(result)
						  {

						  }
				});
			 

				  */
		}

		document.supplierquotationForm.itemIds.value=itemIds;
		document.supplierquotationForm.itemSizes.value=itemSizes;
		document.supplierquotationForm.itemUnits.value=itemUnits;
		document.supplierquotationForm.itemBrandNames.value=itemBrandNames;
		document.supplierquotationForm.quantitys.value=quantitys;
		document.supplierquotationForm.rates.value=rates;
		document.supplierquotationForm.totals.value=totals;
		document.supplierquotationForm.discountPers.value=discountPers;
		document.supplierquotationForm.discountAmounts.value=discountAmounts;
		document.supplierquotationForm.gstPers.value=gstPers;
		document.supplierquotationForm.gstAmounts.value=gstAmounts;
		document.supplierquotationForm.grandTotals.value=grandTotals;
	 	
	submitFunction();
}

	
function submitFunction()
{
submit.click();
}
function init()
{
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	

}

 /*  $(function () {
    $('#materialDetailsTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  }) */
  
  $(document).ready(function() {
	  $(window).keydown(function(event){
	    if(event.keyCode == 13) {
	      event.preventDefault();
	      return false;
	    }
	  });
	});
 
 
 
 
 
 
</script>
</body>
</html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | supplier Work Payment</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


  <!-- Google Font -->
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#enquirydbStatusSpan').delay(1000).fadeOut();
      });
    </script>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

  <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Supplier Work Payment:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Account</a></li>
        <li class="active">Supplier Payment Bill</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="supplierbillpaymentform" action="${pageContext.request.contextPath}/AddSupplierBillPayment" onSubmit="return validate()" method="post">
    <section class="content">

 
      <div class="box box-default">
      <div class="box box-danger"> </div>  
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
		
	<div class="box-body">
       <div class="row">
       
         <input type="hidden" id="supplierId" name="supplierId" value="${purchesedList[0].supplierId}">
          <input type="hidden" id="materialsPurchasedId" name="materialsPurchasedId" value="${purchesedList[0].materialsPurchasedId}">
          <input type="hidden" id="storeId" name="storeId" value="${purchesedList[0].storeId}">
            <input type="hidden" id="supplierBillPaymentHistoriId" name="supplierBillPaymentHistoriId" value="${supplierBillPaymentHistoriId}">
           
          <input type="hidden" id="totalAmount" name="totalAmount" value="${purchesedList[0].totalAmount}">
          <input type="hidden" id="supplierPaidAmount" name="supplierPaidAmount" value="${supplierPaidAmount}">
               
           <div class="col-xs-6">
               <table>
                  <tr>
                   <td style="width:10px"></td>
                   <td style="width:130px"> <h4><label>Purchase Id : </label></h4></td>
                   <td><h4>${purchesedList[0].materialsPurchasedId}</h4></td>
				
				
					<td style="width:30px"></td>
					<td style="width:130px"> <h4><label>Store Name : </label></h4></td>
					<td><h4>${purchesedList[0].storeName}</h4></td>
                  </tr>
                  
              </table>
                  
          </div>               
            
         <div class="col-xs-12">	
		 <table>
			<tr>
			    <td style="width:10px"></td>
				<td style="width:130px"> <h4><label> Supplier Firm : </label></h4></td>
				<td><h4>${purchesedList[0].supplierfirmName}</h4></td>
				
				<td  style="width:30px"></td>
				
				<td style="width:130px"><h4> <label> Firm PAN No : </label></h4></td>
				<td><h4>${firmpanNumber}</h4></td>
				
				<td style="width:30px"></td>
				
				<td style="width:130px"><h4> <label> Firm GST No : </label></h4></td>
				<td><h4>${firmgstNumber}</h4></td>
				
						
			</tr>	
			</table>
			
			<table>
			<tr>
			
				<td style="width:10px"></td>
				<td  style="width:150px"> <h4><label>Item Total Price : </label></h4></td>
				<td><h4>${purchesedList[0].totalPrice}</h4></td>
				
				<td style="width:30px"></td>
				<td style="width:150px"> <h4><label>Total Item Price : </label></h4></td>
				<td><h4>${purchesedList[0].totalPrice}</h4></td>
				
				<td style="width:30px"></td>
				<td style="width:150px"> <h4><label>Total Disct Price : </label></h4></td>
				<td><h4>${purchesedList[0].totalDiscount}</h4></td>
				
				<td style="width:30px"></td>
				<td style="width:150px"> <h4><label>Total GST Price : </label></h4></td>
				<td><h4>${purchesedList[0].totalGstAmount}</h4></td>
				
				<td style="width:30px"></td>
				<td style="width:150px"> <h4><label>Grand Total Price : </label></h4></td>
				<td><h4>${purchesedList[0].totalAmount}</h4></td>
				
			</tr>
			
			<tr>
				<td style="width:10px"></td>
				<td style="width:200px"> <h4><label>Previous Total Advance : </label></h4></td>
				<td><h4>${supplierPaidAmount}</h4></td>
				
				<td style="width:10px"></td>
				<td style="width:150px"> <h4><label>Challan No : </label></h4></td>
				<td><h4>${challanNo}</h4></td>
				
			</tr>
			
		 </table>
		</div>
		  
	  </div>
	</div>	
	
</div>	

  <div class="col-md-12">
		
	 <div class="box-body">
        <div class="row">
          
          <div class="col-xs-12">
		  <h4><label>Item Details :</label></h4>
                <table id="customerPaymentListTable" class="table table-bordered">
	              <tr bgcolor=#4682B4>
	              <th style="width:90px">Sr. No</th>
	                  <th>Item Category Name</th>
		              <th>Item Name</th>
		              <th>Size</th>
		              <th>Unit</th>
		              <th>Quantity</th>
		              <th>Rate</th> 
		              <th>Total</th>
		              <th>Discount</th> 
		              <th>GST %</th>
		              <th>GST AMT</th>
		              <th>Grand Total</th>
	               </tr>
     	         <c:forEach items="${purchesedMaterialList}" var="purchesedMaterialList" varStatus="loopStatus">
                    <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                    <td>${loopStatus.index+1}</td>
                        <td>${purchesedMaterialList.subsuppliertypeName}</td>
                        <td>${purchesedMaterialList.itemName}</td>
                        <td>${purchesedMaterialList.itenSize}</td>
                        <td>${purchesedMaterialList.itemUnit}</td>
                        <td>${purchesedMaterialList.itemQuantity}</td>
                        <td>${purchesedMaterialList.rate}</td>
                        <td>${purchesedMaterialList.total}</td> 
                        <td>${purchesedMaterialList.discountAmount} </td>
                        <td>${purchesedMaterialList.gstPer} </td>
                        <td>${purchesedMaterialList.gstAmount} </td>
                        <td>${purchesedMaterialList.grandTotal} </td>
                     </tr>
                    </c:forEach>
                </table>
		  </div>              
            
        </div>
      </div>
			
	 <div class="box-body">
            <div class="row">
             &nbsp&nbsp&nbsp&nbsp<h4>Company Bank Details In Which Amount Get Credited/Deposited</h4>
                                       
             <div class="col-md-2">
                <label>Company Account Number</label> <label class="text-red">* </label>
                <select class="form-control" id="companyBankId" name="companyBankId" onchange="getBankName()">
                  <option value="Default">-Select Bank A/c No.-</option>
                  <c:forEach items="${companyBankDetails}" var="companyBankDetails" varStatus="loopStatus">
                    <option value="${companyBankDetails.companyBankId}">${companyBankDetails.companyBankacno}</option>
                  </c:forEach>
                </select>
                <span id="companyBankIdSpan" style="color:#FF0000"></span>
              </div>
              
                 
              <div class="col-md-2">
                  <label>Bank Name</label> <label class="text-red">* </label>
                    <input  type="text" class="form-control pull-right" id="companyBankname" name="companyBankname" readonly>
                  <span id="companyBanknameSpan" style="color:#FF0000"></span>
              </div>
                      
              <div class="col-md-2">
                  <label>Amount</label> <label class="text-red">* </label>
                  <input  type="text" class="form-control pull-right" id="amount" name="amount" placeholder="Enter Amount" onchange="CheckAmount(this.value)"/>
                  <span id="amountSpan" style="color:#FF0000"></span>
              </div>
              
	            <div class="col-xs-2">
                    <label>Payment Type</label> <label class="text-red">*</label>
                  	<select class="form-control" id="paymentMode" name="paymentMode">
				  	<option selected="selected" value="Default">-Select Type-</option>
				  	<option value="Cash">Cash</option>
				  	<option  value="Cheque">Cheque</option>
				  	<option value="DD">DD</option>
				  	<option value="RTGS">RTGS</option>	
				  	<option value="NEFT">NEFT</option>
				  	<option value="IMPS">IMPS</option>	
                  	</select>
                   <span id="paymentModeSpan" style="color:#FF0000"></span>
               </div>

		       <div class="col-md-2">
               <label for="refNumber">REF No. </label><label class="text-red">* </label>
    		   <input type="text" class="form-control" name="refNumber" id="refNumber" placeholder="Enter REF No.">                  
		       <span id="refNumberSpan" style="color:#FF0000"></span>
		       </div>
		       
		      <div class="col-md-2">
               <label for="narration">Narration</label><label class="text-red">* </label>
    		   <input type="text" class="form-control" name="narration" id="narration" placeholder="Enter Narration" style="text-transform: capitalize;">                  
		       <span id="narrationSpan" style="color:#FF0000"></span>
		       </div>
		
            </div>
          </div> 
          
             
     <div class="box-body">
       <div class="row">
            
         <div class="col-md-3">
          <label>Chart of Account</label> <label class="text-red">* </label>
             <select class="form-control" id="chartaccountId" name="chartaccountId" onchange="getSunChartOfAccount()">
             <option value="Default">-Select Chart of Account-</option>
             <c:forEach items="${chartofaccountList}" var="chartofaccountList" varStatus="loopStatus">
             <option value="${chartofaccountList.chartaccountId}">${chartofaccountList.chartaccountName}</option>
             </c:forEach>
             </select>
              <span id="chartaccountNameSpan" style="color:#FF0000"></span>
         </div>
             
         <div class="col-md-3"> <label class="text-red">* </label>
          <label>Sub Chart of Account</label>
             <select class="form-control" id="subchartaccountId" name="subchartaccountId">
             <option value="Default">-Sub Chart of Account-</option>
           
             </select>
              <span id="subchartaccountIdSpan" style="color:#FF0000"></span>
         </div>
            					
       </div>
     </div>
	
          	  <input type="hidden" id="creationDate" name="creationDate" value="">
			  <input type="hidden" id="updateDate" name="updateDate" value="">
			  <input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>">     	
  </div>
</div>
      
	<div class="box-body">
      <div class="row">
      <br/><br/>
        <div class="col-xs-1">
        </div>
        <div class="col-xs-4">
            <div class="col-xs-2">	<a href="SupplierPaymentBill"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
			</div> 
		</div>
			    
	   		    <div class="col-xs-2">
	                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
			    </div>
			    
				<div class="col-xs-3">
		  			<button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
			    </div>
			    	     
       </div>
	 </div>
 </div>
        
</div>
</section>
</form>
</div>

   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>

<script>

function getSunChartOfAccount()
{
	var chartaccountId  = $('#chartaccountId').val();
	 $("#subchartaccountId").empty();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getSubChartAccountList',
		type : 'Post',
		data : { chartaccountId : chartaccountId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Sub chart account-");
							$("#subchartaccountId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].subchartaccountId).text(result[i].subchartaccountName);
							    $("#subchartaccountId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
	
}

function CheckAmount()
{
	var totalAmount  = Number($('#totalAmount').val());
	var supplierPaidAmount  = Number($('#supplierPaidAmount').val());
	var amount  = Number($('#amount').val());
	
	var checkAmount=supplierPaidAmount+amount;
	if(totalAmount<checkAmount)
		{
		$('#amountSpan').html('Please, enter less than total amount..!');
		document.supplierbillpaymentform.amount.value="";
		document.supplierbillpaymentform.amount.focus();
		return false;
		}
	
	
}

function getBankName()
{
	var companyBankId  = $('#companyBankId').val();
	$.ajax({

		url : '${pageContext.request.contextPath}/getCompanyAccountBankName',
		type : 'Post',
		data : { companyBankId : companyBankId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							for(var i=0;i<result.length;i++)
							{
								 $('#companyBankname').val(result[i].companyBankname);
							}
						} 
						else
						{
						  alert("failure111");
						}
					}
		});
	
}

function clearall()
{
	$('#companyBankIdSpan').html('');
	$('#amountSpan').html('');
	$('#paymentModeSpan').html('');
	$('#refNumberSpan').html('');
	$('#narrationSpan').html('');

}

function validate()
{
	clearall();

	if(document.supplierbillpaymentform.companyBankId.value=="Default")
	{
		$('#companyBankIdSpan').html('Please, select company A/c No..!');
		document.supplierbillpaymentform.companyBankId.focus();
		return false;
	}

	if(document.supplierbillpaymentform.amount.value=="")
	{
		 $('#amountSpan').html('Please, enter payment amount..!');
		document.supplierbillpaymentform.amount.focus();
		return false;
	}
	else if(document.supplierbillpaymentform.amount.value.match(/^[\s]+$/))
	{
		$('#amountSpan').html('Please, enter valid payment amount..!');
		document.supplierbillpaymentform.amount.value="";
		document.supplierbillpaymentform.amount.focus();
		return false; 	
	}
	else if(!document.supplierbillpaymentform.amount.value.match(/^[0-9]+$/))
	{
		$('#amountSpan').html('Please, enter valid payment amount..!');
		//document.supplierbillpaymentform.employeeBankacno.value="";
		document.supplierbillpaymentform.amount.focus();
		return false;
	}
	

	if(document.supplierbillpaymentform.paymentMode.value=="Default")
	{
		$('#paymentModeSpan').html('Please, select payment mode..!');
		document.supplierbillpaymentform.paymentMode.focus();
		return false;
	}
	
		
	if(document.supplierbillpaymentform.refNumber.value=="")
	{
		 $('#refNumberSpan').html('Please, enter ref. no..!');
		document.supplierbillpaymentform.refNumber.focus();
		return false;
	}
	else if(document.supplierbillpaymentform.refNumber.value.match(/^[\s]+$/))
	{
		$('#refNumberSpan').html('Please, enter valid ref. no..!');
		document.supplierbillpaymentform.refNumber.value="";
		document.supplierbillpaymentform.refNumber.focus();
		return false; 	
	}
	
	if(document.supplierbillpaymentform.narration.value=="")
	{
		 $('#narrationSpan').html('Please, enter narration..!');
		document.supplierbillpaymentform.narration.focus();
		return false;
	}
	else if(document.supplierbillpaymentform.narration.value.match(/^[\s]+$/))
	{
		$('#narrationSpan').html('Please, enter valid narration..!');
		document.supplierbillpaymentform.narration.value="";
		document.supplierbillpaymentform.narration.focus();
		return false; 	
	}
	if(document.supplierbillpaymentform.chartaccountId.value=="Default")
	{
		$('#chartaccountNameSpan').html('Please, select chart account Name..!');
		document.supplierbillpaymentform.chartaccountId.focus();
		return false;
	}
	
	if(document.supplierbillpaymentform.subchartaccountId.value=="Default")
	{
		$('#subchartaccountIdSpan').html('Please, select sub chart account Name..!');
		document.supplierbillpaymentform.subchartaccountId.focus();
		return false;
	}
	
}

function init()
{
	clearall();
	
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
}



</script>
</body>
</html>

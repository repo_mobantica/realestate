<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Edit Bank</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
   <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
    </script>
    
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        
  <script src="https://code.jquery.com/jquery-3.0.0.js"></script>
  <script src="https://code.jquery.com/jquery-migrate-3.0.1.js"></script>
          
</head>
<body class="hold-transition skin-blue sidebar-mini"  onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

  
  	<%@ include file="headerpage.jsp" %>

   	<%@ include file="menu.jsp" %>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Bank Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Edit Bank</li>
      </ol>
    </section>

<form name="bankform" action="${pageContext.request.contextPath}/EditBank" onSubmit="return validate()" method="POST">
    <!-- Main content -->
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              
              <!-- /.form-group -->
			  
			   <span id="statusSpan" style="color:#FF0000"></span>
			     <div class="box-body">
              <div class="row">
                  <div class="col-xs-3">
			     <label for="bankId">Bank Id</label>
                  <input type="text" class="form-control" id="bankId" name="bankId" value="${bankDetails[0].bankId}" readonly>
                 </div> 
                 <%-- 
                 <div class="col-xs-6">
			       <label for="bankId">Bank Type</label>
			       </br>
			       <c:choose>
			     <c:when test="${bankDetails[0].bankType eq 'Housing Loan Bank'}">
                 	<input type="radio"  name="bankType" id="bankType" value="Housing Loan Bank" checked="checked"> &nbsp &nbsp Housing Loan Bank
                 </c:when>
                 <c:otherwise>  
                 	<input type="radio"  name="bankType" id="bankType" value="Housing Loan Bank" onclick = "HousingLoanBank()"> &nbsp &nbsp Housing Loan Bank
                 </c:otherwise>
			   </c:choose>
                 
                 &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
                 
                 <c:choose>
				  <c:when test="${bankDetails[0].bankType eq 'Other'}">
  					<input type="radio" name="bankType" id="bankType" value="Other" checked="checked"> &nbsp &nbsp Other
  				  </c:when>
  				  <c:otherwise>
  				  	<input type="radio" name="bankType" id="bankType" value="Other" onclick = "OtherBank()"> &nbsp &nbsp Other
  				  </c:otherwise>
  			    </c:choose>
  			   <span id="bankTypeSpan" style="color:#FF0000"></span>
                 </div> --%> 
				</div>
            </div>
			  
			  
		   <div class="box-body">
             <div class="row">
             
             
			      <div class="col-xs-3">
                  <label for="bankName">Bank Name  </label>  <label class="text-red">* </label>
                  <input type="text" class="form-control" id="bankName" name="bankName" placeholder="Enter Bank Name"  value="${bankDetails[0].bankName}" style="text-transform: capitalize;">
                  <span id="bankNameSpan" style="color:#FF0000"></span>
			     </div> 
			     
             <%-- 
                  <div class="col-xs-3">
                     <label for="bankName">Bank Name  </label>  <label class="text-red">* </label>
                     <select class="form-control" id="bankName" name="bankName" >
                        <option selected="" value="${bankDetails[0].bankName}">${bankDetails[0].bankName}</option>
                     </select>
				     <span id="bankNameSpan" style="color:#FF0000"></span>
			      </div> 
			       --%>
			      <div class="col-xs-3">
                    <label for="branchName">Branch Name  </label>  <label class="text-red">* </label>
                    <input type="text" class="form-control" id="branchName" name="branchName" placeholder="Enter Branch Name" value="${bankDetails[0].branchName}" style="text-transform: capitalize;">
                    <span id="branchNameSpan" style="color:#FF0000"></span>
			      </div>
			      <%-- 
			      <div class="col-xs-3">
			       <label for="ifsccode">NEFT No/RTGS No/IFSC code</label> <label class="text-red">* </label>
                   <input type="text" class="form-control" id="bankifscCode" placeholder="IFSC No " name="bankifscCode" onchange="getuniqueifsc(this.value)" style="text-transform:uppercase" value="${bankDetails[0].bankifscCode}">
                   <span id="bankifscCodeSpan" style="color:#FF0000"></span>
			      </div> 
			      --%>
			     <div class="col-xs-3">
			       <label for="apfnumber">APF Number</label> 
                   <input type="text" class="form-control" id="apfnumber" placeholder="APF No " name="apfnumber" value="${bankDetails[0].apfnumber}">
                   <span id="apfnumberSpan" style="color:#FF0000"></span>
			     </div>
			       
			 </div>
            </div>
			  
				
	        <div class="box-body">
              <div class="row">
                  
                 <div class="col-xs-3">
			        <label for="bankaddress">Address </label>  <label class="text-red">*</label>
                    <textarea class="form-control" rows="1" id="bankAddress" name="bankAddress" placeholder="Enter Address">${bankDetails[0].bankAddress}</textarea>
			        <span id="bankAddressSpan" style="color:#FF0000"></span>
			     </div> 
				  
				 <div class="col-xs-3">
                  <label>Country</label>  <label class="text-red">* </label>
                   <select class="form-control" id="countryId" name="countryId"  onchange="getStateList(this.value)">
                    <option selected="" value="${bankDetails[0].countryId}">${countryName}</option>
                    <c:forEach var="countryList" items="${countryList}">
                      <c:choose>
                       <c:when test="${bankDetails[0].countryId ne countryList.countryId}">
	                    	<option value="${countryList.countryId}">${countryList.countryName}</option>
	                   </c:when>
	                  </c:choose>
	                </c:forEach>
                  </select>
                   <span id="countryIdSpan" style="color:#FF0000"></span>
                </div>
				  
				<div class="col-xs-3">
				 <label>State</label> <label class="text-red">* </label>
                  <select class="form-control" id="stateId" name="stateId" onchange="getCityList(this.value)">
                  <option selected="" value="${bankDetails[0].stateId}">${stateName}</option>
				  	<c:forEach var="stateList" items="${stateList}">
				      <c:choose>
				         <c:when test="${bankDetails[0].stateId ne stateList.stateId}">
                       		<option value="${stateList.stateId}">${stateList.stateId}</option>
                       	 </c:when>
                      </c:choose>
				    </c:forEach>                      
                  </select>
                   <span id="stateIdSpan" style="color:#FF0000"></span>
				</div>
				
				<div class="col-xs-3">
				  <label>City</label> <label class="text-red">* </label>
                  <select class="form-control" id="cityId" name="cityId"  onchange="getLocationAreaList(this.value)">
                  <option selected="" value="${bankDetails[0].cityId}">${cityName}</option>
					<c:forEach var="cityList" items="${cityList}">
					   <c:choose>
					     <c:when test="${bankDetails[0].cityId ne cityList.cityId}">
                    		<option value="${cityList.cityId}">${cityList.cityId}</option>
                    	 </c:when>
                       </c:choose>
				    </c:forEach>                    
                  </select>
                   <span id="cityIdSpan" style="color:#FF0000"></span>
				</div>
				  
              </div>
            </div>
				
				
			<div class="box-body">
              <div class="row">
              
              	  <div class="col-xs-3">
				    <label>Area</label> <label class="text-red">* </label>
                   <select class="form-control" id="locationareaId" name="locationareaId" onchange="getpinCode(this.value)">
					<option selected="" value="${bankDetails[0].locationareaId}">${locationareaName}</option>
					<c:forEach var="locationareaList" items="${locationareaList}">
					  <c:choose>
					    <c:when test="${bankDetails[0].locationareaId ne locationareaList.locationareaId}">
                    		<option value="${locationareaList.locationareaId}">${locationareaList.locationareaId}</option>
                    	</c:when>
                      </c:choose>
				    </c:forEach>    
				   </select>
				  <span id="locationareaIdSpan" style="color:#FF0000"></span>
				  </div>
              
                  <div class="col-xs-3">
                    <label for="pincode">Pin Code</label> <label class="text-red">* </label>
                    <input type="text" class="form-control" id="areaPincode" name="areaPincode" placeholder="Enter Pin Code" value="${bankDetails[0].areaPincode}" readonly="readonly">
			        <span id="areaPincodeSpan" style="color:#FF0000"></span>
			     </div>
			     
			      
				  <div class="col-xs-3">
			    <label for="bankPhoneno">Bank Phone No</label> 

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask='"mask": "(999) 999-99999"' data-mask id="bankPhoneno" name="bankPhoneno" value="${bankDetails[0].bankPhoneno}">
              <span id="bankPhonenoSpan" style="color:#FF0000"></span>
                </div>
			     </div> 
              </div>
            </div>
	     </div>
	    </div>
	   </div>
	   
	    <div class="panel box box-danger"></div>
        <div class="box-body">
          <div class="row">
			<div class="col-md-12">
			 <div class="box-body">
			    <h4>Bank Employee Details</h4>
              <div class="row">
                  <div class="col-xs-3">
			    <label for="">Employee Name</label> 
                <input type="text" class="form-control" id="employeeName" placeholder="Enter Employee Name" name="employeeName" style="text-transform: capitalize;">
                 <span id="employeeNameSpan" style="color:#FF0000"></span>
                 </div> 
                    <div class="col-xs-3">
			    <label for="">Designation</label>
			    
                    <select class="form-control" id="employeeDesignation" name="employeeDesignation">
						  <option selected="selected" value="Default">-Select Designation-</option>
		               <c:forEach var="designationList" items="${designationList}">
                    	  <option value="${designationList.designationName}">${designationList.designationName}</option>
				        </c:forEach> 
		             </select>
		            <span id="employeeDesignationSpan" style="color:#FF0000"></span>    
                  </div> 
              
           
                  <div class="col-xs-3">
			   
                  <label for="emailid">Email ID </label> 
				   <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                    <input type="text" class="form-control" placeholder="Email" id="employeeEmail" name="employeeEmail">
                    <span id="employeeEmailSpan" style="color:#FF0000"></span>
                   </div>
			     </div> 
				  
				 <div class="col-xs-3">
			     <label>Mobile No </label> 
				  <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask='"mask": "9999999999"' data-mask name="employeeMobileno" id="employeeMobileno">
                   <span class="input-group-btn">
            	    <button type="button" class="btn btn-success" onclick="return AddBankEmployee()"><i class="fa fa-plus"></i>Add</button>
              	   </span>
                </div>
                  <span id="employeeMobilenoSpan" style="color:#FF0000"></span>
			   </div>
			 
		  </div>
        </div>		

          <div class="box-body">
           <div class="row">        
        	<div class="col-xs-12">
              <table class="table table-bordered" id="bankEmployeeListTable">
	           <tr bgcolor=#4682B4  style="color: white;">
		              <td>Name</td>
		              <td>Designation</td>
		              <td>E-Mail Id</td>
		              <td>Mobile No.</td>
		              <td>Action</td>
	               </tr>

               	   <c:forEach items="${bankEmployeesList}" var="bankEmployeesList" varStatus="loopStatus">
                    <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                        <td>${bankEmployeesList.employeeName}</td>
                        <td>${bankEmployeesList.employeeDesignation}</td>
                        <td>${bankEmployeesList.employeeEmail}</td>
                        <td>${bankEmployeesList.employeeMobileno}</td>
                        <td> <a onclick="DeleteBankEmployee(${bankEmployeesList.bankEmployeeId})" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>
                     </tr>
                    </c:forEach>
              </table>
            </div>
           </div>
          </div>
        	
        	  <input type="hidden" id="bankStatus" name="bankStatus" value="${bankStatus}">	
			  <input type="hidden" id="creationDate" name="creationDate" value="${bankDetails[0].creationDate}">
			  <input type="hidden" id="updateDate" name="updateDate" value="">
			  <input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>">	 
				 
		  </div>
		  
		  <br/><br/><br/>
		  
		  <div class="col-md-12">
		    <div class="box-body">
              <div class="row">
	            <div class="col-xs-4">
	            
	            <div class="col-xs-2">
                	<a href="BankMaster"><button type="button" class="btn btn-block btn-primary" value="reset" style="width:90px">Back</button></a>
			    </div>
			    </div>
	   		    <div class="col-xs-2">
	                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
			    </div>
			    
				<div class="col-xs-3">
		  			<button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
			    </div>
			    
			    <!-- 
			     
			       <div class="col-xs-6">
			         <button type="button" class="btn btn-info pull-right" style="background:#48D1CC"><a href="ImportNewBank"> Import From Excel File</a></button>
              
                  </div>
                  
			        -->
			  </div>
			</div>
		   </div>
         </div>
	  </div>
      <!-- /.box -->
    </div>
  
    </section>
	</form>
    <!-- /.content -->
  </div>
 

  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
 
<script>

function OtherBank()
{

	var bankType="Other";
	 $("#bankName").empty();
	$.ajax({

		url : '${pageContext.request.contextPath}/getAllBankList',
		type : 'Post',
		data : { bankType : bankType},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Bank Name-");
							$("#bankName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].bankName).text(result[i].bankName);
							    $("#bankName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
}
function HousingLoanBank()
{
	var bankType="Housing Loan Bank";
	 $("#bankName").empty();
	$.ajax({

		url : '${pageContext.request.contextPath}/getAllBankList',
		type : 'Post',
		data : { bankType : bankType},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Bank Name-");
							$("#bankName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].bankName).text(result[i].bankName);
							    $("#bankName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}


function getpinCode()
{

	 $("#areaPincode").empty();
	 var locationareaId = $('#locationareaId').val();
	 var cityId = $('#cityId').val();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();
	 
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getallAreaList',
		type : 'Post',
		data : { locationareaId : locationareaId, cityId : cityId, stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							
							for(var i=0;i<result.length;i++)
							{
								$('#areaPincode').val(result[i].pinCode);
								
							} 
						
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
}

function getuniqueifsc()
{

	 var bankifscCode = $('#bankifscCode').val();
		$.ajax({

			url : '${pageContext.request.contextPath}/getallifscList',
			type : 'Post',
			data : { bankifscCode : bankifscCode},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{
								 
								for(var i=0;i<result.length;i++)
								{
									
									if(result[i].bankifscCode==bankifscCode)
										{
											 //document.stateform.stateId.value="";
											  document.bankform.bankifscCode.focus();
											  $('#bankifscCodeSpan').html('This IFSC is already exist..!');
											  $('#bankifscCodeSpan').val("");
										
										}
									
									
								 } 
							} 
							else
							{
								alert("failure111");
								//$("#ajax_div").hide();
							}

						}
			});	
}
function clearall()
{
	//$('#bankTypeSpan').html('');
	$('#bankNameSpan').html('');
	$('#bankAddressSpan').html('');
	$('#countryIdSpan').html('');
	$('#stateIdSpan').html('');
	$('#cityIdSpan').html('');
	$('#locationareaIdSpan').html('');
	$('#areaPincodeSpan').html('');
	$('#bankPhonenoSpan').html('');
	$('#bankifscCodeSpan').html('');
	$('#employeeNameSpan').html('');
	$('#employeeDesignationSpan').html('');
	$('#employeeEmailSpan').html('');
	$('#employeeMobilenoSpan').html('');
}
function validate()
{
	
	clearall();
/* 
	if(( document.bankform.bankType[0].checked == false ) && ( document.bankform.bankType[1].checked == false ) )
	{
    	$('#bankTypeSpan').html('Please, choose bank Type..!');
		document.bankform.bankType[0].focus();
		return false;
	} 
     */
	if(document.bankform.bankName.value=="")
	{
		$('#bankNameSpan').html('Please, enter Bank name..!');
		document.bankform.bankName.focus();
		return false;
	}
	else if(document.bankform.bankName.value.match(/^[\s]+$/))
	{
		$('#bankNameSpan').html('Please, enter Bank name..!');
		document.bankform.bankName.value="";
		document.bankform.bankName.focus();
		return false;	
	}

	
	if(document.bankform.branchName.value=="")
	{
		$('#branchNameSpan').html('Please, enter branch name..!');
		document.bankform.branchName.focus();
		return false;
	}
	else if(document.bankform.branchName.value.match(/^[\s]+$/))
	{
		$('#branchNameSpan').html('Please, enter branch name..!');
		document.bankform.branchName.value="";
		document.bankform.branchName.focus();
		return false;	
	}
	
	//validation for ifsc code
	if(document.bankform.bankifscCode.value=="")
	{
		$('#bankifscCodeSpan').html('Please, enter bank IFSC code..!');
		document.bankform.bankifscCode.focus();
		return false;
	}
	else if(!document.bankform.bankifscCode.value.match(/^\(?([A-Z]{4})\)?[. ]?([0-9]{7})$/))
	{
		$('#bankifscCodeSpan').html(' bank IFSC code must contain 4 capital alphabets and 7 digit numbers only..!');
		document.bankform.bankifscCode.value="";
		document.bankform.bankifscCode.focus();
		return false;
	}
	
	//validation for bank address
	if(document.bankform.bankAddress.value=="")
	{
		$('#bankAddressSpan').html('Please, enter bank address..!');
		document.bankform.bankAddress.focus();
		return false;
	}
	else if(document.bankform.bankAddress.value.match(/^[\s]+$/))
	{
		$('#bankAddressSpan').html('Please, enter bank address..!');
		document.bankform.bankAddress.value="";
		document.bankform.bankAddress.focus();
		return false;	
	}
	
	//validation for bank country
	if(document.bankform.countryId.value=="Default")
	{
		$('#countryIdSpan').html('Please, select bank country name..!');
		document.bankform.countryId.focus();
		return false;
	}
	
	//validation for bank state
	if(document.bankform.stateId.value=="Default")
	{
		$('#stateIdSpan').html('Please, select bank state name..!');
		document.bankform.stateId.focus();
		return false;
	}
	
	//validation for bank city
	if(document.bankform.cityId.value=="Default")
	{
		$('#cityIdSpan').html('Please, select bank city name..!');
		document.bankform.cityId.focus();
		return false;
	}
	
	//validation for bank area name	
	if(document.bankform.locationareaId.value=="Default")
	{
		$('#locationareaIdSpan').html('Please, select bank area name..!');
		document.bankform.locationareaId.focus();
		return false;
	}
	/*
	//validation for bank phone num
	if(document.bankform.bankPhoneno.value=="")
	{
		$('#bankPhonenoSpan').html('Please, enter bank phone number..!');
		document.bankform.bankPhoneno.value="";
		document.bankform.bankPhoneno.focus();
		return false;
	}
	else if(!document.bankform.bankPhoneno.value.match(/^\(?([0-9]{3})\)?[\s. ]?([0-9]{3})[-. ]?([0-9]{5})$/))
	{
		$('#bankPhonenoSpan').html('phone number must be 10 digit numbers only..!');
		document.bankform.bankPhoneno.value="";
		document.bankform.bankPhoneno.focus();
		return false;	
	}
	*/
	//validation for bank employee 1 mobile number
	if(document.bankform.employeeName1.value.length!=0)
	{
		if(document.bankform.employeeMobileno1.value=="")
		{
			$('#employeeMobileno1Span').html('Please, enter mobile number of this employee..!');
			document.bankform.employeeMobileno1.focus();
			return false;
		}
		else if(!document.bankform.employeeMobileno1.value.match(/^[0-9]{10}$/))
		{
			$('#employeeMobileno1Span').html(' mobile number must be 10 digit numbers only with correct format..!');
			document.bankform.employeeMobileno1.value="";
			document.bankform.employeeMobileno1.focus();
			return false;
		}
	}
		
}

function init()
{
	 clearall();
	 var date =  new Date();
		var year = date.getFullYear();
		var month = date.getMonth() + 1;
		var day = date.getDate();
		
		/* document.getElementById("creationDate").value = day + "/" + month + "/" + year;
		 */document.getElementById("updateDate").value = day + "/" + month + "/" + year;
		
	 if(document.bankform.bankStatus.value == "Fail")
	 {
	  	alert("Sorry, record is present already..!");
	 }
	 else if(document.bankform.bankStatus.value == "Success")
	 {
		$('#statusSpan').html('Record saved successfully..!');
	 }
	
     document.bankform.bankName.focus();
}


function AddBankEmployee()
{
	clearall();
	
	//validation for employee name
	if(document.bankform.employeeName.value=="")
	{
		$('#employeeNameSpan').html('Employee name should not be empty..!');
		document.bankform.employeeName.value="";
		document.bankform.employeeName.focus();
		return false;
	}
	else if(document.bankform.employeeName.value.match(/^[\s]+$/))
	{
		$('#employeeNameSpan').html('Employee name must contains alphabets only..!');
		document.bankform.employeeName.value="";
		document.bankform.employeeName.focus();
		return false;
	}
	else if(!document.bankform.employeeName.value.match(/^[a-zA-Z\s]+$/))
	{
		$('#employeeNameSpan').html('Employee name must contains alphabets only..!');
		document.bankform.employeeName.value="";
		document.bankform.employeeName.focus();
		return false;
	}
	
	//validation for employee designation
	if(document.bankform.employeeDesignation.value=="Default")
	{
		$('#employeeDesignationSpan').html('Please, select designation..!');
		document.bankform.employeeDesignation.focus();
		return false;
	}
	
	//validation for employee email id
	if(document.bankform.employeeEmail.value=="")
	{
		$('#employeeEmailSpan').html('Please, enter email id..!');
		document.bankform.employeeEmail.focus();
		return false;
	}
	else if(!document.bankform.employeeEmail.value.match(/^([a-z0-9_\.\-])+\@(([a-z0-9\-])+\.)+([a-z0-9]{2,4})+$/))
	{
		$('#employeeEmailSpan').html('Please, enter valid email id..!');
		document.bankform.employeeEmail.value="";
		document.bankform.employeeEmail.focus();
		return false;
	}
	
	//validation for employee mobile number
	if(document.bankform.employeeMobileno.value=="")
	{
		$('#employeeMobilenoSpan').html('Please, enter mobile number..!');
		document.bankform.employeeMobileno.focus();
		return false;
	}
	else if(!document.bankform.employeeMobileno.value.match(/^[0-9]{10}$/))
	{
		$('#employeeMobilenoSpan').html('mobile number must be 10 digit numbers only with correct format..!');
		document.bankform.employeeMobileno.value="";
		document.bankform.employeeMobileno.focus();
		return false;
	}
	
	 
	$('#bankEmployeeListTable tr').detach();
	 
	 var bankId = $('#bankId').val();
	 var employeeName = $('#employeeName').val();
	 var employeeDesignation = $('#employeeDesignation').val();
	 var employeeEmail = $('#employeeEmail').val();
	 var employeeMobileno = $('#employeeMobileno').val();
	 
	 $.ajax({

		 url : '${pageContext.request.contextPath}/AddBankEmployee',
		type : 'Post',
		data : { bankId : bankId, employeeName : employeeName, employeeDesignation : employeeDesignation, employeeEmail : employeeEmail, employeeMobileno : employeeMobileno},
		dataType : 'json',
		success : function(result)
				  {
					   if (result) 
					   { 
						   $('#bankEmployeeListTable').append('<tr style="background-color: #4682B4;"><th  style="color: white;">Name</th><th style="color: white;">Designation</th><th style="color: white;">Email Id</th><th style="color: white;">Mobile No.</th><th style="color: white;">Action</th>');
							
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].bankEmployeeId;
									$('#bankEmployeeListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].employeeName+'</td><td>'+result[i].employeeDesignation+'</td><td>'+result[i].employeeEmail+'</td><td>'+result[i].employeeMobileno+'</td><td><a onclick="EditEmployee('+id+')" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>|<a onclick="DeleteBankEmployee('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
								else
								{
									var id = result[i].bankEmployeeId;
									$('#bankEmployeeListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].employeeName+'</td><td>'+result[i].employeeDesignation+'</td><td>'+result[i].employeeEmail+'</td><td>'+result[i].employeeMobileno+'</td><td><a onclick="EditEmployee('+id+')" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>|<a onclick="DeleteBankEmployee('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}
				  } 

		});
	 
	 $('#employeeName').val("")
	 $('#employeeEmail').val("");
	 $('#employeeMobileno').val("");
}

function DeleteBankEmployee(employeeId)
{
	var bankId = $('#bankId').val();
    var bankEmployeeId = employeeId;
    
    $('#bankEmployeeListTable tr').detach();
    
    $.ajax({

		 url : '${pageContext.request.contextPath}/DeleteBankEmployee',
		type : 'Post',
		data : { bankId : bankId, bankEmployeeId : bankEmployeeId },
		dataType : 'json',
		success : function(result)
				  {
					   if (result) 
					   { 
						   $('#bankEmployeeListTable').append('<tr style="background-color: #4682B4;"><th  style="color: white;">Name</th><th style="color: white;">Designation</th><th style="color: white;">Email Id</th><th style="color: white;">Mobile No.</th><th style="color: white;">Action</th>');
							
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].bankEmployeeId;
									$('#bankEmployeeListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].employeeName+'</td><td>'+result[i].employeeDesignation+'</td><td>'+result[i].employeeEmail+'</td><td>'+result[i].employeeMobileno+'</td><td><a onclick="DeleteBankEmployee('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
								else
								{
									var id = result[i].bankEmployeeId;
									$('#bankEmployeeListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].employeeName+'</td><td>'+result[i].employeeDesignation+'</td><td>'+result[i].employeeEmail+'</td><td>'+result[i].employeeMobileno+'</td><td><a onclick="DeleteBankEmployee('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}
				  } 

		});
    
}


function getStateList()
{
	 $("#stateId").empty();
	 var countryId = $('#countryId').val();
	 
	$.ajax({

		url : '${pageContext.request.contextPath}/getStateList',
		type : 'Post',
		data : { countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select State-");
							$("#stateId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].stateId).text(result[i].stateName);
							    $("#stateId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get State List


function getCityList()
{
	 $("#cityId").empty();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();
	$.ajax({

		url : '${pageContext.request.contextPath}/getCityList',
		type : 'Post',
		data : { stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select City-");
							$("#cityId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].cityId).text(result[i].cityName);
							    $("#cityId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
}//end of get City List

function getLocationAreaList()
{
	 $("#locationareaId").empty();
	 var cityId = $('#cityId').val();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getLocationAreaList',
		type : 'Post',
		data : { cityId : cityId, stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Location Area-");
							$("#locationareaId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].locationareaId).text(result[i].locationareaName);
							    $("#locationareaId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
}//end of get locationarea List


  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate |Edit Material Purchase </title>
  <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">
 
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
    
 

  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>

<div class="wrapper">

   <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Material Purchase  Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Purchase</a></li>
        <li class="active">Edit Material Purchase </li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="materialform" action="${pageContext.request.contextPath}/EditMaterialPurchaseForm" onSubmit="return validate()" method="post">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
     
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
                <span id="statusSpan" style="color:#FF0000"></span>
              <!-- /.form-group -->
               <div class="box-body">
        	  <div class="row">
        	  
        	    <div class="col-md-2">
                  <label for="materialsPurchasedId">Purchase Id</label>
                   <input type="hidden" class="form-control" id="employeeId" name="employeeId"  value="${materialPurchasedDetails[0].materialsPurchasedId}" readonly>
               
                  <input type="text" class="form-control" id="materialsPurchasedId" name="materialsPurchasedId"  value="${materialPurchasedDetails[0].materialsPurchasedId}" readonly>
                </div>
                
            	<div class="col-md-2">
                  <label for="requisitionId">Requisition Id</label>
                  <input type="text" class="form-control" id="requisitionId" name="requisitionId"  value="${materialPurchasedDetails[0].requisitionId}" readonly>
                </div>
                 <div class="col-md-2">
                  <label for="">Applied Date</label>
                  <input type="text" class="form-control" id="" name=""  value="${materialPurchasedDetails[0].creationDate}" readonly>
                </div>
                 <div class="col-md-2">
                  <label for="materialsPurchasedId">Required Date</label>
                  <input type="text" class="form-control" id="requiredDate" name="requiredDate" readonly>
                </div>
			</div>
			</div>
			  <div class="box-body">
        	  <div class="row">
                 <div class="col-xs-3">
			   		<h4><label>Employee Name : ${materialpurchaserequisition[0].employeeId}</label></h4>
			     </div> 
			     
                 <div class="col-xs-3">
			       <h4><label>Project : ${materialpurchaserequisition[0].projectName} </label></h4> 
			     </div> 
			     
			     <div class="col-xs-3">
			       <h4><label>Project Building Name : ${materialpurchaserequisition[0].buildingName}</label> </h4>
			     </div> 
			
                 <div class="col-xs-3">
			       <h4><label>Wing : ${materialpurchaserequisition[0].wingName}</label> </h4>
                 </div> 
			  
			  </div>
            </div>	
              <div class="box-body">
        	  <div class="row">
        	        
                   <div class="col-md-3">
	                  <label>Supplier Name</label><label class="text-red">* </label>
                      <select class="form-control" id="supplierId" name="supplierId">
				      		<option selected="selected" value="Default">-Select Supplier Name-</option>
                      	<c:forEach var="supplierList" items="${supplierList}">
	                    	<option value="${supplierList.supplierId}">${supplierList.supplierfirmName}</option>
	                  	</c:forEach>
                      </select>
                      <span id="supplierIdSpan" style="color:#FF0000"></span>
                   </div>
        	  
        	     <div class="col-md-3">
	                  <label>Store Name</label><label class="text-red">* </label>
                      <select class="form-control" id="storeId" name="storeId">
				      		<option selected="selected" value="Default">-Select Store Name-</option>
                      	<c:forEach var="storeList" items="${storeList}">
	                    	<option value="${storeList.storeId}">${storeList.storeName}</option>
	                  	</c:forEach>
                      </select>
                      <span id="storeIdSpan" style="color:#FF0000"></span>
                   </div>
                   
        	  </div>
        	  </div>
           <div class="panel box box-danger"></div>		
	
			  <div class="box-body">
             	 <div class="row">
             	 	  <div class="col-md-2">
	                  <label for="itemMainCategoryName">Item Main Category</label><label class="text-red">* </label>
	                  <input type="text" class="form-control" id="itemMainCategoryName" name="itemMainCategoryName" readonly="readonly">
	                  <span id="itemUnitSpan" style="color:#FF0000"></span>
                   </div>
                     <div class="col-md-2">
	                  <label for="itemCategoryName">Item Sub-Category</label><label class="text-red">* </label>
	                  <input type="text" class="form-control" id="itemCategoryName" name="itemCategoryName" readonly="readonly">
	                  <span id="itemUnitSpan" style="color:#FF0000"></span>
                   </div>
             	   <div class="col-md-2">
	                  <input type="hidden" id="itemId" name="itemId" value="" readonly="readonly">
	                  <input type="hidden" id="requisitionHistoryId" name="requisitionHistoryId" value="" readonly="readonly">
	                  
	                  <label for="itemName">Item Name</label><label class="text-red">* </label>
	                  <input type="text" class="form-control" id="itemName" name="itemName"  placeholder="Item Name" readonly="readonly">
	                  <span id="itemNameSpan" style="color:#FF0000"></span>
                   </div>
                   
                   <div class="col-md-1">
	                  <label for="itemUnit">Item Unit</label><label class="text-red">* </label>
	                  <input type="text" class="form-control" id="itemUnit" name="itemUnit"  placeholder="Item Unit" readonly="readonly">
	                  <span id="itemUnitSpan" style="color:#FF0000"></span>
                   </div>
                   
                   <div class="col-md-1">
	                  <label for="itemSize">Item Size</label><label class="text-red">* </label>
	                  <input type="text" class="form-control" id="itemSize" name="itemSize"  placeholder="Item Size" readonly="readonly">
	                  <span id="itemSizeSpan" style="color:#FF0000"></span>
                   </div>
                   
                   <div class="col-md-2">
	                  <label>Brand Name</label><label class="text-red">* </label>
                      <select class="form-control" id="itemBrandName" name="itemBrandName">
				      		<option selected="selected" value="Default">-Select Brand-</option>
                      	<c:forEach var="brandList" items="${brandList}">
	                    	<option value="${brandList.brandName}">${brandList.brandName}</option>
	                  	</c:forEach>
                      </select>
                      <span id="itemBrandNameSpan" style="color:#FF0000"></span>
                   </div>
                  
                   <div class="col-md-2">
	                  <label for="itemQuantity">Item Qty.</label><label class="text-red">* </label>
	                  <input type="text" class="form-control" id="itemQuantity" name="itemQuantity"  placeholder="Item Qty" onchange="AmountCalculation(this.value)">
	                  <span id="itemQuantitySpan" style="color:#FF0000"></span>
                   </div>
                           
             	 </div>
             </div>
             
             <div class="box-body">
             	 <div class="row">
             	  
                   <div class="col-md-2">
	                  <label for="itemPrice">Price</label><label class="text-red">* </label>
	                  <input type="text" class="form-control" id="itemPrice" name="itemPrice"  placeholder="Item Price" onchange="AmountCalculation(this.value)">
	                  <span id="itemPriceSpan" style="color:#FF0000"></span>
                   </div>
             	      <div class="col-md-1">
	                  <label for="totalItemPrice">Total Price</label><label class="text-red">*</label>
	                  <input type="text" class="form-control" id="totalItemPrice" name="totalItemPrice" readonly>
	                  <span id="totalItemPriceSpan" style="color:#FF0000"></span>
                   </div>
				   <div class="col-md-1">
	                  <label for="itemPriceDiscount">Disc(%)</label><label class="text-red">*</label>
	                  <input type="text" class="form-control" id="itemPriceDiscount" name="itemPriceDiscount"  placeholder="Item Price Discount" onchange="AmountCalculation(this.value)">
	                  <span id="itemPriceDiscountSpan" style="color:#FF0000"></span>
                   </div>   
                   
                      <div class="col-md-1">
	                  <label for="itemTotalDiscount">Total Disc</label><label class="text-red">*</label>
	                  <input type="text" class="form-control" id="itemTotalDiscount" name="itemTotalDiscount"  placeholder="Item Price Discount" readonly>
	                  <span id="itemTotalDiscountSpan" style="color:#FF0000"></span>
                   </div> 
                             	  
             	 	  <div class="col-md-2">
	                  <label for="itemAmountwithDiscount">Amt with Dict</label><label class="text-red">*</label>
	                  <input type="text" class="form-control" id="itemAmountwithDiscount" name="itemAmountwithDiscount" readonly>
	                  <span id="itemPriceDiscountSpan" style="color:#FF0000"></span>
                   </div> 
             	   <div class="col-md-1">
	                  <label for="itemGSTPercentage">GST(%)</label><label class="text-red">*</label>
	                  <input type="text" class="form-control" id="itemGSTPercentage" name="itemGSTPercentage"  placeholder="Item GSt Percentage" readonly>
	                  <span id="itemGSTPercentageSpan" style="color:#FF0000"></span>
                   </div>
                   
                   <div class="col-md-1">
	                  <label for="itemGSTPrice">GST Amt</label><label class="text-red">*</label>
	                  <input type="text" class="form-control" id="itemGSTPrice" name="itemGSTPrice"  placeholder="Item GSt Price" readonly>
	                  <span id="itemGSTPriceSpan" style="color:#FF0000"></span>
                   </div>
                   
                   <div class="col-xs-2">
                   	<label for="itemTotalAmount">Total Amount(Rs.)</label><label class="text-red">*</label>
                     <div class="input-group">
        			   <input type="text" class="form-control" id="itemTotalAmount" name="itemTotalAmount"  placeholder="Total Amount" readonly>
	         		   <span class="input-group-btn">
	                   <button type="button" class="btn btn-success" onclick="AddNewItem()"><i class="fa fa-plus"></i></button>
	                   </span>
       		 	    </div>
           			<span id="itemTotalAmountSpan" style="color:#FF0000"></span>
			     </div>

           	 </div>
           </div>
             
             <br/>
             
				<div class="box-body">
             	 <div class="row">
                  
                  <div class="col-xs-4">
                  <div class="table-responsive">
                
	                <table class="table table-bordered" id="ItemListTable">
	                  <thead>
		                  <tr bgcolor=#4682B4>
		                    <th>Sr. No</th>
		                    <th>Item Name</th>
						    <th>Action</th>
		                  </tr>
	                  </thead>
	                   <tbody>
	                   <c:forEach items="${materialpurchaserequisitionhistory}" var="materialpurchaserequisitionhistory" varStatus="loopStatus">
	                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
	                      		<td>${loopStatus.index+1}</td>
		                        <td>${materialpurchaserequisitionhistory.itemName}</td>
		               
		                        <td><button type="button" onclick="AddItemDetails('${materialpurchaserequisitionhistory.requisitionHistoryId}','${materialpurchaserequisitionhistory.itemId}')" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></button></td>
	                       </tr>
						</c:forEach>
	                 
	                 </tbody>
	                </table>
              </div>
            </div>
            
             <div class="col-xs-8">
             <div class="table-responsive">
                
	                <table class="table table-bordered" id="ItempurchesListTable">
	                  <thead>
		                  <tr bgcolor=#4682B4>
		                    <th>Sr. No</th>
		                    <th>Item Name</th>
		                    <th>Item Category</th>
		                    <th>Item Sub category</th>
		                    <th>Unit</th>
		                    <th>Brand Name</th>
		                    <th>Qty</th>
		                    <th>Price</th>
		                    <th>Item Price</th>
		                    <th>Action</th>
		                  </tr>
	                  </thead>
	                   <tbody>
	                   <c:forEach items="${materialspurchasedhistory}" var="materialspurchasedhistory" varStatus="loopStatus">
	                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
	                       <td>${loopStatus.index+1}</td>
		                   <td>${materialspurchasedhistory.itemName}</td>
			               <td>${materialspurchasedhistory.itemMainCategoryName}</td>
			               <td>${materialspurchasedhistory.itemCategoryName}</td>
			               <td>${materialspurchasedhistory.itemUnit}</td>
			               <td>${materialspurchasedhistory.itemBrandName}</td>
			               <td>${materialspurchasedhistory.itemQuantity}</td>
			               <td>${materialspurchasedhistory.itemPrice}</td>
			               <td>${materialspurchasedhistory.totalItemPrice}</td>
			               <td></td>
	                       </tr>
						</c:forEach>
	                 
	                 </tbody>
	                </table>
              </div>
             </div>
             
          </div>
    </div>		  

			  
				<input type="hidden" id="status" name="status" value="Applied"> 
				<input type="hidden" id="creationDate" name="creationDate" value="${materialPurchasedDetails[0].creationDate}">
				<input type="hidden" id="updateDate" name="updateDate" value="">
				<input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>" >
				
              <!-- /.form-group -->
            </div>
            
            <!-- /.col -->
			
          </div>
          
          	<div class="box-body">
             	 <div class="row">
             	 
             	    <div class="col-md-2">
             	    </div>
             	    
             	    <div class="col-md-2">
             	    </div>
             	   
				   <div class="col-md-2">
	                  <label for="totalPrice">Price Total</label><label class="text-red">*</label>
	                  <input type="text" class="form-control" id="totalPrice" name="totalPrice"  readonly="readonly">
	                  <span id="totalPriceSpan" style="color:#FF0000"></span>
                   </div>
                   
                   <div class="col-md-2">
	                  <label for="totalDiscount">Total Disc</label><label class="text-red">*</label>
	                  <input type="text" class="form-control" id="totalDiscount" name="totalDiscount"  readonly="readonly">
	                  <span id="totalDiscountSpan" style="color:#FF0000"></span>
                   </div>
                 
                   <div class="col-md-2">
	                  <label for="totalGstAmount">Total GST Amt(Rs.)</label><label class="text-red">*</label>
	                  <input type="text" class="form-control" id="totalGstAmount" name="totalGstAmount"   readonly="readonly">
	                  <span id="totalGstAmountSpan" style="color:#FF0000"></span>
                   </div>
                   
                   <div class="col-md-2">
	                  <label for="totalAmount">Grand Total</label><label class="text-red">*</label>
	                  <input type="text" class="form-control" id="totalAmount" name="totalAmount"  readonly="readonly">
	                  <span id="totalAmountSpan" style="color:#FF0000"></span>
                   </div>
                 </div>
            </div>
		   	 <br/>
			<div class="box-body">
              <div class="row">

                 <div class="col-xs-4">
                 <div class="col-xs-2">
                	<a href="MaterialPurchaseRequisitionList"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
			     </div>
			     </div>

				 <div class="col-xs-4">
                	<button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
			     </div>

				 <div class="col-xs-2">
			  	    <button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
			     </div> 
			       
			  </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        
         </div>
      <!-- /.box -->
	
			
       </section>
	</form>
    <!-- /.content -->
    
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>

<script>

function clearall()
{
	$('#itemNameSpan').html('');
	$('#itemBrandNameSpan').html('');
	$('#itemQuantitySpan').html('');
	$('#itemPriceSpan').html('');
	$('#supplierIdSpan').html('');
	$('#storeIdSpan').html('');
}

function validate()
{ 
	clearall();
	if(document.materialform.supplierId.value=="Default")
	{
		$('#supplierIdSpan').html('Please, select Supplier name..!');
		document.materialform.supplierId.focus();
		return false;
	}
	if(document.materialform.storeId.value=="Default")
	{
		$('#storeIdSpan').html('Please, select Store name..!');
		document.materialform.storeId.focus();
		return false;
	}
	
}


function AddNewItem()
{
	
	clearall();
	if(document.materialform.itemName.value=="")
	{
		$('#itemNameSpan').html('Please, select item name..!');
		document.materialform.itemName.focus();
		return false;
	}
	
	if(document.materialform.itemQuantity.value=="")
	{
		$('#itemQuantitySpan').html('Please, enter item Quantity..!');
		document.materialform.itemQuantity.focus();
		return false;
	}
	else if(!document.materialform.itemQuantity.value.match(/^[0-9]+$/))
	{
		$('#itemQuantitySpan').html('item quantity must be digit..!');
		document.materialform.itemQuantity.value="";
		document.materialform.itemQuantity.focus();
		return false;
	}
	if(document.materialform.itemBrandName.value=="Default")
	{
		$('#itemBrandNameSpan').html('Please, select brand name..!');
		document.materialform.itemBrandName.focus();
		return false;
	}
	if(document.materialform.itemPrice.value=="")
	{
		$('#itemPriceSpan').html('Please, enter item price..!');
		document.materialform.itemPrice.focus();
		return false;
	}
	else if(!document.materialform.itemPrice.value.match(/^[0-9]+$/))
	{
		$('#itemPriceSpan').html('item price must be digit..!');
		document.materialform.itemPrice.value="";
		document.materialform.itemPrice.focus();
		return false;
	}
	 
	var requisitionHistoryId = $('#requisitionHistoryId').val();
	var materialsPurchasedId = $('#materialsPurchasedId').val();
	var requisitionId = $('#requisitionId').val();
	var itemMainCategoryName = $('#itemMainCategoryName').val();
	var itemCategoryName = $('#itemCategoryName').val();
	var itemId = $('#itemId').val();
	var itemName = $('#itemName').val();
	var itemUnit = $('#itemUnit').val();
	var itemSize = $('#itemSize').val();
	var itemBrandName = $('#itemBrandName').val();
	var itemQuantity = Number($('#itemQuantity').val());
	var itemPrice = Number($('#itemPrice').val());
	
	var totalItemPrice = Number($('#totalItemPrice').val());
	var itemTotalDiscount = Number($('#itemTotalDiscount').val());
	
	var itemPriceDiscount = Number($('#itemPriceDiscount').val());
	var itemAmountwithDiscount = Number($('#itemAmountwithDiscount').val());
	var itemGSTPercentage = Number($('#itemGSTPercentage').val());
	var itemGSTPrice = Number($('#itemGSTPrice').val());
	var itemTotalAmount = Number($('#itemTotalAmount').val());
	var userName =  $('#userName').val();
	var totalPrice=0;
	var totalDiscount=0
	var totalGstAmount=0;
	var totalAmount=0;
	
	$('#ItempurchesListTable tr').detach();
	$.ajax({

		 url : '${pageContext.request.contextPath}/AddItemRequisitionPurches',
		type : 'Post',
		data : { requisitionHistoryId : requisitionHistoryId, materialsPurchasedId : materialsPurchasedId, requisitionId : requisitionId, itemId : itemId, itemName : itemName,itemMainCategoryName: itemMainCategoryName,itemCategoryName : itemCategoryName, itemUnit : itemUnit, itemSize : itemSize, itemBrandName : itemBrandName, itemQuantity : itemQuantity, itemPrice : itemPrice, totalItemPrice : totalItemPrice, itemTotalDiscount : itemTotalDiscount, itemPriceDiscount : itemPriceDiscount, itemAmountwithDiscount : itemAmountwithDiscount, itemGSTPercentage : itemGSTPercentage, itemGSTPrice : itemGSTPrice, itemTotalAmount : itemTotalAmount, userName : userName},
		dataType : 'json',
		success : function(result)
				  {
					if (result) 
						{
							$('#ItempurchesListTable').append('<tr style="background-color: #4682B4;"><th>Sr. No</th> <th>Item Name</th><th>Item Category</th><th>Item Sub category</th><th>Unit</th><th>Brand Name</th><th>Qty</th><th>Price</th><th>Item Price</th><th>Total</th><th>Action</th>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].materialPurchesHistoriId;
									$('#ItempurchesListTable').append('<tr style="background-color: #F0F8FF;"><td>'+(i+1)+'</td><td>'+result[i].itemName+'</td><td>'+result[i].itemMainCategoryName+'</td><td>'+result[i].itemCategoryName+'</td><td>'+result[i].itemUnit+'</td><td>'+result[i].itemBrandName+'</td><td>'+result[i].itemQuantity+'</td><td>'+result[i].itemPrice+'</td><td>'+result[i].totalItemPrice+'</td><td>'+result[i].itemTotalAmount+'</td><td><a onclick="DeletePurchesedItem(\''+id+'\')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
								else
								{
									var id = result[i].materialPurchesHistoriId;
									$('#ItempurchesListTable').append('<tr style="background-color: #F0F8FF;"><td>'+(i+1)+'</td><td>'+result[i].itemName+'</td><td>'+result[i].itemMainCategoryName+'</td><td>'+result[i].itemCategoryName+'</td><td>'+result[i].itemUnit+'</td><td>'+result[i].itemBrandName+'</td><td>'+result[i].itemQuantity+'</td><td>'+result[i].itemPrice+'</td><td>'+result[i].totalItemPrice+'</td><td>'+result[i].itemTotalAmount+'<td><a onclick="DeletePurchesedItem(\''+id+'\')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
								totalPrice =totalPrice+result[i].totalItemPrice;
								totalDiscount=totalDiscount+result[i].itemTotalDiscount;
								totalGstAmount=totalGstAmount+result[i].itemGSTPrice;
								totalAmount=totalAmount+result[i].itemTotalAmount;
								//document.materialform.totalNoItem.value=i+1;
							 }
							document.materialform.totalPrice.value=totalPrice;
							document.materialform.totalDiscount.value=totalDiscount; 
							document.materialform.totalGstAmount.value=totalGstAmount; 
							document.materialform.totalAmount.value=totalAmount; 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	document.materialform.itemMainCategoryName.value="";
	document.materialform.itemCategoryName.value="";
	document.materialform.itemQuantity.value=""; 
	document.materialform.itemName.value="";
	document.materialform.itemUnit.value="";
	document.materialform.itemQuantity.value=""; 
	document.materialform.itemPrice.value="";
	
	document.materialform.totalItemPrice.value="";
	document.materialform.itemTotalDiscount.value=""; 
	document.materialform.itemPriceDiscount.value="";
	document.materialform.itemAmountwithDiscount.value="";
	
	document.materialform.itemAmountwithDiscount.value=""; 
	document.materialform.itemGSTPercentage.value="";
	document.materialform.itemGSTPrice.value="";
	document.materialform.itemTotalAmount.value=""; 
	
	 $("#itemBrandName").empty();
		var option = $('<option/>');
		option.attr('value',"Default").text("-Select Brand-");
		$("#itemBrandName").append(option);
}

function DeletePurchesedItem(id)
{
	var totalPrice=0;
	var totalDiscount=0
	var totalGstAmount=0;
	var totalAmount=0;
var requisitionId = $('#requisitionId').val();
var materialPurchesHistoriId=id;
$('#ItempurchesListTable tr').detach();
$.ajax({

	 url : '${pageContext.request.contextPath}/DeletePurchesedItem',
	type : 'Post',
	data : { materialPurchesHistoriId : materialPurchesHistoriId, requisitionId : requisitionId },
	dataType : 'json',
	success : function(result)
			  {
				   if (result) 
				   {
						$('#ItempurchesListTable').append('<tr style="background-color: #4682B4;"><th>Sr. No</th> <th>Item Name</th><th>Item Category</th><th>Item Sub category</th><th>Unit</th><th>Brand Name</th><th>Qty</th><th>Price</th><th>Item Price</th><th>Total</th><th>Action</th>');
						
						for(var i=0;i<result.length;i++)
						{ 
							if(i%2==0)
							{
								var id = result[i].materialPurchesHistoriId;
								$('#ItempurchesListTable').append('<tr style="background-color: #F0F8FF;"><td>'+(i+1)+'</td><td>'+result[i].itemName+'</td><td>'+result[i].itemMainCategoryName+'</td><td>'+result[i].itemCategoryName+'</td><td>'+result[i].itemUnit+'</td><td>'+result[i].itemBrandName+'</td><td>'+result[i].itemQuantity+'</td><td>'+result[i].itemPrice+'</td><td>'+result[i].totalItemPrice+'</td><td>'+result[i].itemTotalAmount+'</td><td><a onclick="DeletePurchesedItem(\''+id+'\')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
							}
							else
							{
								var id = result[i].materialPurchesHistoriId;
								$('#ItempurchesListTable').append('<tr style="background-color: #F0F8FF;"><td>'+(i+1)+'</td><td>'+result[i].itemName+'</td><td>'+result[i].itemMainCategoryName+'</td><td>'+result[i].itemCategoryName+'</td><td>'+result[i].itemUnit+'</td><td>'+result[i].itemBrandName+'</td><td>'+result[i].itemQuantity+'</td><td>'+result[i].itemPrice+'</td><td>'+result[i].totalItemPrice+'</td><td>'+result[i].itemTotalAmount+'<td><a onclick="DeletePurchesedItem(\''+id+'\')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
							}
							totalPrice =totalPrice+result[i].totalItemPrice;
							totalDiscount=totalDiscount+result[i].itemTotalDiscount;
							totalGstAmount=totalGstAmount+result[i].itemGSTPrice;
							totalAmount=totalAmount+result[i].itemTotalAmount;
							//document.materialform.totalNoItem.value=i+1;
						 }
						document.materialform.totalPrice.value=totalPrice;
						document.materialform.totalDiscount.value=totalDiscount; 
						document.materialform.totalGstAmount.value=totalGstAmount; 
						document.materialform.totalAmount.value=totalAmount; 
					}
					else
					{
						alert("failure111");
					}
			  } 

	});
}


function init()
{
	clearall();
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	/* document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	 */document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
/* 	 if(document.materialform.stateStatus.value == "Fail")
	 {
	  	alert("Sorry, record is present already..!");
	 }
	 else if(document.materialform.stateStatus.value == "Success")
	 {
		 $('#statusSpan').html('Record added successfully..!');
	 } */
	  document.materialform.totalPrice.value=0;
		document.materialform.totalDiscount.value=0; 
		document.materialform.totalGstAmount.value=0; 
		document.materialform.totalAmount.value=0;  
}

function AddItemDetails(requisitionHistoryId, itemId)
{
	  $('#itemMainCategoryName').val('');
	  $('#itemCategoryName').val('');
	  $('#itemName').val('');
	  $('#itemUnit').val('');
	  $('#itemSize').val('');
	  $('#itemQuantity').val('');
	  $('#itemPrice').val('');
	  $('#itemPriceDiscount').val('');
	  $('#itemGSTPercentage').val('');
	  $('#itemId').val(itemId);
	  $('#requisitionHistoryId').val(requisitionHistoryId);
	var totalPrice;
	var discount;
	var gstamount;
	var totalItemAmount;
	var itemAmountwithDiscount;
	$.ajax({
		
		url : '${pageContext.request.contextPath}/getMaterialDetails',
		type : 'Post',
		data : { requisitionHistoryId : requisitionHistoryId, itemId : itemId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							  $('#itemMainCategoryName').val(result.itemMainCategoryName);
							  $('#itemCategoryName').val(result.itemCategoryName);
							  $('#itemName').val(result.itemName);
							  $('#itemUnit').val(result.itemunitName);
							  $('#itemSize').val(result.itemSize);
							  $('#itemQuantity').val(result.itemQuantity);
							  $('#itemPrice').val(result.intemInitialPrice);
							  $('#itemPriceDiscount').val(result.itemInitialDiscount);
							  $('#itemGSTPercentage').val(result.gstPer);
							  totalprice=result.itemQuantity*result.intemInitialPrice;
							  discount=(totalprice*result.itemInitialDiscount)/100;
							  itemAmountwithDiscount=(totalprice-discount);
							  gstamount=(itemAmountwithDiscount*result.gstPer)/100
							  totalItemAmount=(totalprice-discount)+gstamount;
							  
							  $('#itemAmountwithDiscount').val(itemAmountwithDiscount);
							  $('#itemGSTPrice').val(gstamount);
							  $('#itemTotalAmount').val(totalItemAmount);
							  $('#itemTotalDiscount').val(discount);
							  $('#totalItemPrice').val(totalprice);
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
	
	 $("#itemBrandName").empty();
	 
	 $.ajax({

		url : '${pageContext.request.contextPath}/getItemBrandName',
		type : 'Post',
		data : { itemId : itemId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Brand-");
							$("#itemBrandName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].brandName).text(result[i].brandName);
							    $("#itemBrandName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
}

function AddItemDetails1(id)
{
	var res = id.split(",");
	var requisitionHistoryId=res[0];
	var itemId=res[1];
	  $('#itemName').val('');
	  $('#itemUnit').val('');
	  $('#itemSize').val('');
	  $('#itemQuantity').val('');
	  $('#itemPrice').val('');
	  $('#itemPriceDiscount').val('');
	  $('#itemGSTPercentage').val('');
	  $('#itemId').val(itemId);
	  $('#requisitionHistoryId').val(requisitionHistoryId);
	var totalPrice;
	var discount;
	var gstamount;
	var totalItemAmount;
	var itemAmountwithDiscount;
	$.ajax({
		
		url : '${pageContext.request.contextPath}/getMaterialDetails',
		type : 'Post',
		data : { requisitionHistoryId : requisitionHistoryId, itemId : itemId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							  $('#itemName').val(result.itemName);
							  $('#itemUnit').val(result.itemunitName);
							  $('#itemSize').val(result.itemSize);
							  $('#itemQuantity').val(result.itemQuantity);
							  $('#itemPrice').val(result.intemInitialPrice);
							  $('#itemPriceDiscount').val(result.itemInitialDiscount);
							  $('#itemGSTPercentage').val(result.gstPer);
							  totalprice=result.itemQuantity*result.intemInitialPrice;
							  discount=(totalprice*result.itemInitialDiscount)/100;
							  itemAmountwithDiscount=(totalprice-discount);
							  gstamount=(itemAmountwithDiscount*result.gstPer)/100
							  totalItemAmount=(totalprice-discount)+gstamount;
							  
							  $('#itemAmountwithDiscount').val(itemAmountwithDiscount);
							  $('#itemGSTPrice').val(gstamount);
							  $('#itemTotalAmount').val(totalItemAmount);
							  $('#totalItemPrice').val(totalprice);
							  $('#itemTotalDiscount').val(discount);
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
	
	 $("#itemBrandName").empty();
	 $.ajax({

		url : '${pageContext.request.contextPath}/getItemBrandName',
		type : 'Post',
		data : { itemId : itemId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Brand-");
							$("#itemBrandName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].brandName).text(result[i].brandName);
							    $("#itemBrandName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
}

function AmountCalculation()
{
	var totalPrice;
	var discount;
	var gstamount;
	var totalItemAmount;
	var itemAmountwithDiscount;
	var itemTotalDiscount;
	var itemQuantity = Number($('#itemQuantity').val());
	var itemPrice = Number($('#itemPrice').val());
	var itemPriceDiscount = Number($('#itemPriceDiscount').val());
	var itemGSTPercentage = Number($('#itemGSTPercentage').val());
	  totalprice=itemQuantity*itemPrice;
	  discount=((totalprice*itemPriceDiscount)/100).toFixed(0);
	  itemAmountwithDiscount=(totalprice-discount);
	  gstamount=((itemAmountwithDiscount*itemGSTPercentage)/100).toFixed(0);
	
	  totalItemAmount=parseInt(itemAmountwithDiscount)+parseInt(gstamount);
	  $('#itemTotalDiscount').val(discount);
	  $('#itemAmountwithDiscount').val(itemAmountwithDiscount);
	  $('#itemGSTPrice').val(gstamount);
	  $('#itemTotalAmount').val(totalItemAmount);
	  $('#totalItemPrice').val(totalprice);
}

  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
  /* 
 $(function () {
 $('#ItempurchesListTable').DataTable()
 $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
 $(function () {
 $('#ItemListTable').DataTable()
 $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
   */
</script>
</body>
</html>

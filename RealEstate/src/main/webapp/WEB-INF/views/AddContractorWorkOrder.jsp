<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Add Contractor Work Orders</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


  <!-- Google Font -->
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#enquirydbStatusSpan').delay(1000).fadeOut();
      });
    </script>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

  <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Contractor Work Order Details:
        <small>Preview</small>
      </h1>
      
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Contractor</a></li>
        <li class="active">Add Contractor Work Order</li>
      </ol>
    </section>

<form name="contractorworkorderform" action="${pageContext.request.contextPath}/AddContractorWorkOrder" onSubmit="return validate()" method="post">
    <section class="content">

      <div class="box box-default">
        <div class="panel box box-danger"></div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <span id="statusSpan" style="color:#FF0000"></span>
			
			
			<div class="box-body">
              <div class="row">
              
                  <div class="col-xs-2">
                  <label >Work Order Id</label>
                  <input type="text" class="form-control" id="workOrderId" name="workOrderId" value="${workOrderCode}" readonly>
                   <span id="statusSpan" style="color:#FF0000"></span>
                </div>               
            
                  <div class="col-xs-2">
                  <label >Quotation Id</label>
                   <input type="hidden" class="form-control" id="workId" name="workId" value="${contractorquotationDetails[0].workId}" readonly>
                  <input type="text" class="form-control" id="quotationId" name="quotationId" value="${contractorquotationDetails[0].quotationId}" readonly>
                   <span id="statusSpan" style="color:#FF0000"></span>
                </div>               
            
              </div>
            </div>
			
			<div class="box-body">
              <div class="row">
              
                  <div class="col-xs-2">
			      <label>Project  </label> 
			      
				  <input type="text" class="form-control" value="${projectName}" readonly>
				  <input type="hidden" class="form-control" id=projectId name="projectId" value="${contractorworkList[0].projectId}" readonly>
			        <span id="projectIdSpan" style="color:#FF0000"></span>
			     </div> 
			     
			      <div class="col-xs-2">
			      <label>Building </label> 
				   <input type="text" class="form-control" value="${buildingName}" readonly>
				   <input type="hidden" class="form-control" id=buildingId name="buildingId" value="${contractorworkList[0].buildingId}" readonly>
			     </div> 
			
                 <div class="col-xs-1">
			      <label>Wing </label> 
				 	<input type="text" class="form-control" value="${wingName}" readonly>
				 	<input type="hidden" class="form-control" id=wingId name="wingId" value="${contractorworkList[0].wingId}" readonly>
                 </div> 
             
                 <div class="col-xs-2">
			      <label>Work/Contractor Type </label> 
                  <input type="text" class="form-control" value="${contractorType}" readonly>
                  <input type="hidden" class="form-control" id=contractortypeId name="contractortypeId" value="${contractorworkList[0].contractortypeId}" readonly>  
			     </div> 
			     
                  <div class="col-xs-3">
			      <label>Contractor Firm Name  </label> <label class="text-red">* </label>
                  <input type="hidden" class="form-control" id="contractorId" name="contractorId" value="${contractorDetails[0].contractorId}" readonly>
                   <input type="text" class="form-control" id="contractorfirmName" name="contractorfirmName"  value="${contractorDetails[0].contractorfirmName}" readonly>
			     </div> 
			     
			      
                  <div class="col-xs-2">
			      <label>Total Amount </label> <label class="text-red">* </label>
                   <input type="text" class="form-control" id="totalAmount" name="totalAmount"  value="${contractorquotationDetails[0].totalAmount}" readonly>
			     </div> 
			     
			      <input type="hidden" class="form-control" id="tdspercentage" name="tdspercentage" value="${contractorDetails[0].tdspercentage}" readonly>
			      
           </div>
            </div>
			
			<div class="box-body">
              <div class="row">
			      
			     <div class="col-xs-2">
			       <label>Total No.of Male Labour</label> <label class="text-red">* </label>
                   <input type="text" class="form-control" id="noofMaleLabour" name="noofMaleLabour" placeholder="No.of Male Labour">
			       <span id="noofMaleLabourSpan" style="color:#FF0000"></span>
			     </div> 
			
			     <div class="col-xs-2">
			       <label>Total No.of Female Labour </label> <label class="text-red">* </label>
                   <input type="text" class="form-control" id="noofFemaleLabour" name="noofFemaleLabour"  placeholder="No.of Female Labour">
			       <span id="noofFemaleLabourSpan" style="color:#FF0000"></span>
			     </div> 
			    
			     <div class="col-xs-2">
			       <label>No. Of Insured Labour</label> <label class="text-red">* </label>
                   <input type="text" class="form-control" id="noofInsuredLabour" name="noofInsuredLabour" placeholder="No of Insured Labour">
			       <span id="noofInsuredLabourSpan" style="color:#FF0000"></span>
			     </div> 
			  
			     <div class="col-xs-2">
			      <label for="retentionPer">Retention %</label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="retentionPer" name="retentionPer" placeholder="Retention Per" onchange="CalculateRetentionAmount(this.value)">
                   <span id="retentionPerSpan" style="color:#FF0000"></span> 
			     </div>
			         
			     <div class="col-xs-2">
			      <label for="retentionAmount">Retention Amount</label> 
                  <input type="text" class="form-control" id="retentionAmount" name="retentionAmount" placeholder="Retention Amount" readonly>
                   <span id="retentionAmountSpan" style="color:#FF0000"></span> 
			     </div>
			
			
			 </div>
            </div>
			
			<div class="box-body">
              <div class="row">
       
              <div class="col-xs-12">
			    
		           <table class="table table-bordered" id="contractorWorkListTable">
	              <tr bgcolor=#4682B4 style="color: white;">
		              <td>Sub-Contractor Type</td>
		              <td>Work Type</td>
		              <td>Type</td>
		              <td>Unit</td>
		              <td>Rate</td>
		              <td>G.AMT</td>
	               </tr>

               	   <c:forEach items="${contractorworkhistoryList}" var="contractorworkhistoryList" varStatus="loopStatus">
                    <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                        <td>${contractorworkhistoryList.subcontractortypeId}</td>
                        <td>${contractorworkhistoryList.workType}</td>
                        <td>${contractorworkhistoryList.type}</td>
                        <td>${contractorworkhistoryList.unit}</td>
                        <td>${contractorworkhistoryList.workRate}</td>
                        <td>${contractorworkhistoryList.grandTotal}</td>
                      
                     </tr>
                    </c:forEach> 
              </table>
            </div>
			
           </div>
         </div>
	 
         
          <div class="box-body">
            <h4><label>Add Payment Schedule </label></h4>
              <div class="row">
              
			    <div class="col-xs-2">
			      <label for="paymentDecription">Work Payment Details</label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="paymentDecription" name="paymentDecription">
                  <span id="paymentDecriptionSpan" style="color:#FF0000"></span>
			     </div> 

			    <div class="col-xs-2">
			      <label for="paymentAmountper">Amount Per%</label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="paymentAmountper" name="paymentAmountper" onchange="getPaymentTotalAmount(this.value)">
                  <span id="paymentAmountperSpan" style="color:#FF0000"></span>
			     </div> 
			     
			    <div class="col-xs-2">
			      <label for="paymentAmount">Amount</label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="paymentAmount" name="paymentAmount" readonly>
                  <span id="paymentAmountSpan" style="color:#FF0000"></span>
			     </div> 
			     
			    <div class="col-xs-1">
			      <label for="gstAmount">GST Amount</label>
                  <input type="text" class="form-control" id="gstAmount" name="gstAmount" readonly>
                  <span id="gstAmountSpan" style="color:#FF0000"></span>
			     </div> 
			     
			    <div class="col-xs-2">
			      <label for="paymentTotalAmount">Total Amount</label>
                  <input type="text" class="form-control" id="paymentTotalAmount" name="paymentTotalAmount" readonly>
                  <span id="paymentTotalAmountSpan" style="color:#FF0000"></span>
			     </div> 
			     
			    <div class="col-xs-1">
			      <label for="tdsAmount">TDS Amount</label>
                  <input type="text" class="form-control" id="tdsAmount" name="tdsAmount" readonly>
                  <span id="tdsAmountSpan" style="color:#FF0000"></span>
			     </div> 
			        
			    <div class="col-xs-2">
			     <label for="grandAmount">Grand Amount</label> <label class="text-red">* </label>
                  <div class="input-group">
                   <input type="text" class="form-control" id="grandAmount" name="grandAmount" readonly>
			       <span class="input-group-btn">
            	   <button type="button" class="btn btn-success" onclick="return AddContractorPayment()"><i class="fa fa-plus"></i>Add</button>
              	   </span>
                  </div>
                 <span id="grandAmountSpan" style="color:#FF0000"></span>
			   </div>
			     
			  </div>
            </div> 
           
           <div class="box-body">
              <div class="row">
              <br/>
              <div class="col-xs-12">
                <table class="table table-bordered" id="customerPaymentListTable">
	              <tr bgcolor=#4682B4>
		              <th>Work Details</th>
		              <th>Amount per %</th>
		              <th>Amount</th>
		              <th>GST Amount</th>
		              <th>Total Amount</th>
		              <th>TDS Amount</th>
		              <th>Grand Amount</th>
		              <th>Action</th>
	               </tr>

                </table>
              </div>
              
			  </div>
            </div>      
           <input type="hidden" id="gstCost" name="gstCost" >
       
			<input type="hidden" id="creationDate" name="creationDate" >
			<input type="hidden" id="updateDate" name="updateDate" >
			<input type="hidden" id="paymentStatus" name="paymentStatus" value="Unclear">
			
			<input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>">
		</div>	
      </div>
      
		   	 <div class="box-body">
              <div class="row">
              <br/><br/>
              <div class="col-xs-1">
              </div>
                 <div class="col-xs-4">
	            	 <div class="col-xs-2">	
	             		<a href="ContractorWorkListMaster"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
				     </div> 
			     </div>
				  <div class="col-xs-4">
                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
			     </div>
					<div class="col-xs-2">
			        <button type="submit" class="btn btn-info " name="submit">Submit</button>
			     </div> 
			     
              </div>
			  </div>
        </div>
        
      </div>
		
    </section>
	</form>
  </div>
 

  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
   
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>

 <script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
</script>
 
<script>

function CalculateRetentionAmount()
{

	var totalAmount = Number($('#totalAmount').val());
	var retentionPer = Number($('#retentionPer').val());
	var retentionAmount=(totalAmount/100)*retentionPer;
	
	document.contractorworkorderform.retentionAmount.value=retentionAmount.toFixed(0);
}




function getPaymentTotalAmount()
{

var totalAmount  = Number($('#totalAmount').val());

var gstCost = Number($('#gstCost').val());
var tdspercentage = Number($('#tdspercentage').val());

var paymentAmount ;
//= Number($('#paymentAmount').val());

var paymentAmountper = Number($('#paymentAmountper').val());
/* 
var gstAmount = $('#gstAmount').val();
var paymentTotalAmount = $('#paymentTotalAmount').val();
var tdsAmount = $('#tdsAmount').val();
 */
 
var gstAmount ;
var paymentTotalAmount ;
var tdsAmount;
var grandAmount;

paymentAmount=(totalAmount/100)*paymentAmountper;
gstAmount=(paymentAmount/100)*gstCost;
paymentTotalAmount=paymentAmount+gstAmount;

tdsAmount=(totalAmount/100)*tdspercentage;
grandAmount=paymentTotalAmount-tdsAmount;

document.contractorworkorderform.paymentAmount.value=paymentAmount.toFixed(0);
document.contractorworkorderform.gstAmount.value=gstAmount.toFixed(0);
document.contractorworkorderform.paymentTotalAmount.value=paymentTotalAmount.toFixed(0);
document.contractorworkorderform.tdsAmount.value=tdsAmount.toFixed(0);
document.contractorworkorderform.grandAmount.value=grandAmount.toFixed(0);

}


function AddContractorPayment()
{
	 $('#paymentDecriptionSpan').html('');
	 $('#paymentAmountSpan').html('');
	 
	 var workOrderId = $('#workOrderId').val();
	 var contractorId = $('#contractorId').val();
	 
	 var paymentDecription = $('#paymentDecription').val();
	 var paymentAmountper = $('#paymentAmountper').val();
	 var paymentAmount = $('#paymentAmount').val();
	 var gstAmount = $('#gstAmount').val();
	 var paymentTotalAmount = $('#paymentTotalAmount').val();
	 var tdsAmount = $('#tdsAmount').val();
	 var grandAmount = $('#grandAmount').val();
	 
 

		if(document.contractorworkorderform.paymentDecription.value.length==0)
		{
			$('#paymentDecriptionSpan').html('Please, enter work..!');
			//document.contractorworkorderform.employeeBankacno.value="";
			document.contractorworkorderform.paymentDecription.focus();
			return false;
		}
		
		if(document.contractorworkorderform.paymentAmountper.value.length==0)
		{
			$('#paymentAmountperSpan').html('Please, enter Amount per..!');
			//document.contractorworkorderform.employeeBankacno.value="";
			document.contractorworkorderform.paymentAmountper.focus();
			return false;
		}
		else if(!document.contractorworkorderform.paymentAmountper.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			 if(!document.contractorworkorderform.paymentAmountper.value.match(/^[0-9]+$/))
				 {
					//$('#paymentAmountSpan').html('Please, use only digit value for payment Amount');
					document.contractorworkorderform.paymentAmountper.value="";
					document.contractorworkorderform.paymentAmountper.focus();
					return false;
				}
		}	
	 
		
		
	 $("#customerPaymentListTable tr").detach();
	 	
	 	$.ajax({

	 		url : '${pageContext.request.contextPath}/AddContractorPayment',
	 		type : 'Post',
	 		data : { workOrderId : workOrderId, contractorId : contractorId, paymentDecription : paymentDecription, paymentAmountper : paymentAmountper, paymentAmount : paymentAmount, gstAmount : gstAmount, paymentTotalAmount : paymentTotalAmount, tdsAmount : tdsAmount, grandAmount : grandAmount},
	 		dataType : 'json',
	 		success : function(result)
	 				  {
	 						if (result) 
	 						{ 
	 							$('#customerPaymentListTable').append('<tr style="background-color: #4682B4;"> <th>Work Details</th> <th>Amount per%</th> <th>Amount</th><th>GST Amount</th> <th>Total Amount</th> <th>TDS Amount</th><th>Grand Amount</th> <th>Action</th>');
	 						
	 							for(var i=0;i<result.length;i++)
	 							{ 
	 								if(i%2==0)
	 								{
	 									var id = result[i].contractorrschedulerId;
	 									$('#customerPaymentListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].paymentDecription+'</td><td>'+result[i].paymentAmountper+'</td><td>'+result[i].paymentAmount+'</td><td>'+result[i].gstAmount+'</td><td>'+result[i].paymentTotalAmount+'</td><td>'+result[i].tdsAmount+'</td><td>'+result[i].grandAmount+'</td><td><a onclick="DeleteCustomerPayment('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td>');
	 								}
	 								else
	 								{
	 									var id = result[i].contractorrschedulerId;
	 									$('#customerPaymentListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].paymentDecription+'</td><td>'+result[i].paymentAmountper+'</td><td>'+result[i].paymentAmount+'</td><td>'+result[i].gstAmount+'</td><td>'+result[i].paymentTotalAmount+'</td><td>'+result[i].tdsAmount+'</td><td>'+result[i].grandAmount+'</td><td><a onclick="DeleteCustomerPayment('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td>');
	 								}
	 							 } 
	 						} 
	 						else
	 						{
	 							alert("failure111");
	 						}

	 					}
	 		}); 
	 	document.contractorworkorderform.paymentDecription.value="";
	 	document.contractorworkorderform.paymentAmountper.value="";
	 	document.contractorworkorderform.paymentAmount.value="";	
		document.contractorworkorderform.gstAmount.value="";
	 	document.contractorworkorderform.paymentTotalAmount.value="";	
		document.contractorworkorderform.tdsAmount.value="";
	 	document.contractorworkorderform.grandAmount.value="";	
	 	
	 	
}

function DeleteCustomerPayment(contractorrschedulerId)
{
	var workOrderId = $('#workOrderId').val();
    
    $('#customerPaymentListTable tr').detach();
    
    $.ajax({

		 url : '${pageContext.request.contextPath}/DeleteCustomerPayment',
		type : 'Post',
		data : { contractorrschedulerId : contractorrschedulerId, workOrderId : workOrderId },
		dataType : 'json',
		success : function(result)
				  {
				if (result) 
					{ 
						$('#customerPaymentListTable').append('<tr style="background-color: #4682B4;"> <th>Work Details</th> <th>Amount Per %</th><th>Amount</th><th>GST Amount</th> <th>Total Amount</th> <th>TDS Amount</th><th>Grand Amount</th> <th>Action</th>');
					
						for(var i=0;i<result.length;i++)
						{ 
							if(i%2==0)
							{
								var id = result[i].contractorrschedulerId;
								$('#customerPaymentListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].paymentDecription+'</td><td>'+result[i].paymentAmountper+'</td><td>'+result[i].paymentAmount+'</td><td>'+result[i].gstAmount+'</td><td>'+result[i].paymentTotalAmount+'</td><td>'+result[i].tdsAmount+'</td><td>'+result[i].grandAmount+'</td><td><a onclick="DeleteCustomerPayment('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td>');
							}
							else
							{
								var id = result[i].contractorrschedulerId;
								$('#customerPaymentListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].paymentDecription+'</td><td>'+result[i].paymentAmountper+'</td><td>'+result[i].paymentAmount+'</td><td>'+result[i].gstAmount+'</td><td>'+result[i].paymentTotalAmount+'</td><td>'+result[i].tdsAmount+'</td><td>'+result[i].grandAmount+'</td><td><a onclick="DeleteCustomerPayment('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td>');
							}
						 } 
						} 
						else
						{
							alert("failure111");
						}
				  } 

		});	
}

function clearall()
{
	$('#noofMaleLabourSpan').html('');
	$('#noofFemaleLabourSpan').html('');
	$('#noofInsuredLabourSpan').html('');
	$('#retentionPerSpan').html('');
	$('#workStartDateSpan').html('');
	$('#workEndDateSpan').html('');
}

function validate()
{ 
	clearall();
		
	
	if(document.contractorworkorderform.noofMaleLabour.value.length==0)
	{
		$('#noofMaleLabourSpan').html('Please, enter no of male labours..!');
		//document.contractorworkorderform.employeeBankacno.value="";
		document.contractorworkorderform.noofMaleLabour.focus();
		return false;
	}
	else  if(!document.contractorworkorderform.noofMaleLabour.value.match(/^[0-9]+$/))
	{
				$('#noofMaleLabourSpan').html('Please, use only digit value for male labours..! eg: 30');
				document.contractorworkorderform.noofMaleLabour.value="";
				document.contractorworkorderform.noofMaleLabour.focus();
				return false;
	}
	 
	if(document.contractorworkorderform.noofFemaleLabour.value.length==0)
	{
		$('#noofFemaleLabourSpan').html('Please, enter no fo female labours..!');
		//document.contractorworkorderform.employeeBankacno.value="";
		document.contractorworkorderform.noofFemaleLabour.focus();
		return false;
	}
	else if(!document.contractorworkorderform.noofFemaleLabour.value.match(/^[0-9]+$/))
	{
				$('#noofFemaleLabourSpan').html('Please, use only digit value for  Female labours..! eg: 30');
				document.contractorworkorderform.noofFemaleLabour.value="";
				document.contractorworkorderform.noofFemaleLabour.focus();
				return false;
	}
	

	if(document.contractorworkorderform.noofInsuredLabour.value.length==0)
	{
		$('#noofInsuredLabourSpan').html('Please, enter no fo female labours..!');
		//document.contractorworkorderform.employeeBankacno.value="";
		document.contractorworkorderform.noofInsuredLabour.focus();
		return false;
	}
	else if(!document.contractorworkorderform.noofInsuredLabour.value.match(/^[0-9]+$/))
	{
				$('#noofInsuredLabourSpan').html('Please, use only digit value for  Female labours..! eg: 30');
				document.contractorworkorderform.noofInsuredLabour.value="";
				document.contractorworkorderform.noofInsuredLabour.focus();
				return false;
	}
	
	if(document.contractorworkorderform.retentionPer.value.length==0)
	{
		$('#retentionPerSpan').html('Please, enter retention per..!');
		document.contractorworkorderform.retentionPer.focus();
		return false;
	}
	else if(!document.contractorworkorderform.retentionPer.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
	{
		 if(!document.contractorworkorderform.retentionPer.value.match(/^[0-9]+$/))
			 {
				$('#retentionPerSpan').html('Please, use only digit value for retention per..! eg:21.36 OR 30');
				document.contractorworkorderform.retentionPer.value="";
				document.contractorworkorderform.retentionPer.focus();
				return false;
			}
	}
	
}
function init()
{
	clearall();
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getContractorGstCostList',
		type : 'Post',
		data : { },
		dataType : 'json',
		success : function(result)
				  {
			 var gstCost=0;
						if (result) 
						{
							for(var i=0;i<result.length;i++)
							{
								gstCost=result[result.length-1].taxPercentage;
							} 
						} 
						else
						{
							alert("failure111");
						}
						document.contractorworkorderform.gstCost.value=gstCost;
					}
		});
	
	 if(document.contractorworkorderform.status.value=="Fail")
	 {
	  	//alert("Sorry, record is present already..!");
	 }
	 else if(document.contractorworkorderform.status.value=="Success")
	 {
			$('#statusSpan').html('Record added successfully..!');
	 }
}



function getBuldingList()
{
	 $("#buildingName").empty();
	 var projectName = $('#projectName').val();
	 
	 $.ajax({

		url : '${pageContext.request.contextPath}/getBuildingList',
		type : 'Post',
		data : { projectName : projectName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Building Name-");
							$("#buildingName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].buildingName).text(result[i].buildingName);
							    $("#buildingName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
}//end of get Building List


function getWingNameList()
{
	 $("#wingName").empty();
	 var buildingName = $('#buildingName').val();
	 var projectName = $('#projectName').val();
	 $.ajax({

		url : '${pageContext.request.contextPath}/getprojectwingList',
		type : 'Post',
		data : { buildingName : buildingName, projectName : projectName },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Wing Name-");
							$("#wingName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].wingName).text(result[i].wingName);
							    $("#wingName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}


function getFloorNameList()
{
	 $("#floortypeName").empty();
	 var wingName = $('#wingName').val();
	 var buildingName = $('#buildingName').val();
	 var projectName = $('#projectName').val();
	 
	 $.ajax({

		url : '${pageContext.request.contextPath}/getwingfloorNameList',
		type : 'Post',
		data : {wingName : wingName, buildingName : buildingName, projectName : projectName },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"All").text("All");
							$("#floortypeName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].floortypeName).text(result[i].floortypeName);
							    $("#floortypeName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	

}//end of get Floor Type List


  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
     $('#workStartDate').datepicker({
      autoclose: true
    })

    $('#workEndDate').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>RealEstate | Contractor Payment Bill</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">
	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

    <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
    <section class="content-header">
      <h1>
        Contractor Payment Bill Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Project Engg</a></li>
        <li class="active"> Contractor Payment Bill</li>
      </ol>
    </section>
    <!-- Main content -->
	<form name="contractorpaymentbillform" action="${pageContext.request.contextPath}/ContractorPaymentBill" onSubmit="return validate()" method="post">

    <section class="content">
    
    
    <div class="box box-default">
        
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
        
          <div class="box-body">
          <div class="row">
              
                 <div class="col-md-3">
                  <label>Project</label><label class="text-red">* </label>
                   <select class="form-control" name="projectId" id="projectId" onchange="getBuldingList(this.value)">
                  	 <option selected="selected" value="Default">-Select Project-</option>
                   	  <c:forEach var="projectList" items="${projectList}">
                    	<option value="${projectList.projectId}">${projectList.projectName}</option>
				     </c:forEach>
				   </select>
                  </div>
                  
                   <div class="col-md-3">
                     <label>Project Building</label><label class="text-red">* </label>
                     <select class="form-control" name="buildingId" id="buildingId" onchange="getAllWingList(this.value)">
				  		 <option selected="selected" value="Default">-Select Project Building-</option>
                      
                     </select>
                  </div>
                  
                  <div class="col-xs-3">
			     	 <label>Wing </label> <label class="text-red">* </label>
             		 <select class="form-control" name="wingId" id="wingId" onchange="getWingList(this.value)">
				 	 	<option selected="selected" value="Default">-Select Wing Name-</option>
                   	 
                    </select>
                   </div>
                  
                </div>
              </div>
                
		  </div>
            
       </div>
		  	     <div class="box-body">
		  	     </br>
                <div class="row">
                 
				    <div class="col-xs-3">
			  		
			        </div>
			      
					<div class="col-xs-3">
			 			&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
			 			<a href="ContractorPaymentBill">  <button type="button" class="btn btn-default" value="reset" style="width:90px">Reset</button></a>
              		</div> 
              	
			  </div>
			</div>
        </div>
        
      </div>
    
    
      <div class="box box-default">
			<div class="panel box box-danger"></div>
        	 <div class="box-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="contractorPaymentListTable">
                  <thead>
	                  <tr bgcolor=#4682B4>
	                 	<th style="width:100px">Work Order No</th>
	                 	<th>Contractor Firm Name</th>
	                 	<th>Project Name</th>
	                    <th>Building Name</th>
	                    <th>Wing Name</th>
	            		<th>Agg Amount</th>
	            		<th>Work Completed Amount</th>
	            		<th>Paid Amount</th>
	                    <th style="width:100px">Action</th>
	                  </tr>
                  </thead>
                  
                  <tbody>
                   <c:forEach items="${contractorworkorderDetails}" var="contractorworkorderDetails" varStatus="loopStatus">
                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                        <td>${contractorworkorderDetails.workOrderId}</td>
                        <td>${contractorworkorderDetails.contractorfirmName}</td>
                        <td>${contractorworkorderDetails.projectId}</td>
                        <td>${contractorworkorderDetails.buildingId}</td>
                        <td>${contractorworkorderDetails.wingId}</td>
                        <td>${contractorworkorderDetails.totalAmount}</td>
                        <td>${contractorworkorderDetails.workAmount}</td>
                        <td>${contractorworkorderDetails.totalPaidAmount}</td>  
                        <td>
                      <a href="${pageContext.request.contextPath}/AddContractorWorkPayment?workOrderId=${contractorworkorderDetails.workOrderId}" class="btn btn-success btn-sm" data-toggle="tooltip" title="View"><i class="glyphicon glyphicon-eye-open"></i></a>
                         |<a target="_blank" href="${pageContext.request.contextPath}/PrintContractorBillReceipt?workOrderId=${contractorworkorderDetails.workOrderId}" class="btn btn-success btn-sm" data-toggle="tooltip" title="Print Bill"><i class="glyphicon glyphicon-print"></i></a>
                        
                          </td>
                      </tr>
					</c:forEach>
                 </tbody>
                </table>
              </div>
            </div>
	 </div>
     
  </section>
	</form>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <!-- Control Sidebar -->
 <%@ include file="footer.jsp" %>

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- page script -->
<script>

function getBuldingList()
{
	 $("#buildingId").empty();
	 var projectId = $('#projectId').val();

	 $.ajax({

		 url : '${pageContext.request.contextPath}/getBuildingList',
		type : 'Post',
		data : { projectId : projectId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Building Name-");
							$("#buildingId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].buildingId).text(result[i].buildingName);
							    $("#buildingId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
	 
	 
	 $("#contractorPaymentListTable tr").detach();
	 

	$.ajax({

		url : '${pageContext.request.contextPath}/getProjectwiseContractorPaymentList',
		type : 'Post',
		data : { projectId : projectId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
							$('#contractorPaymentListTable').append('<tr style="background-color: #4682B4;"><th style="width:100px">Work Order No</th><th>Contractor Firm Name</th><th>Project Name</th> <th>Building Name</th><th>Wing Name</th><th>Agg Amount</th><th>Work Completed Amount</th><th>Paid Amount</th> <th style="width:100px">Action</th>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].workOrderId;
									$('#contractorPaymentListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].workOrderId+'</td><td>'+result[i].contractorfirmName+'</td><td>'+result[i].projectId+'</td><td>'+result[i].buildingId+'</td><td>'+result[i].wingId+'</td><td>'+result[i].totalAmount+'</td><td>'+result[i].workAmount+'</td><td>'+result[i].totalPaidAmount+'</td><td><a href="${pageContext.request.contextPath}/AddContractorWorkPayment?workOrderId='+id+'" class="btn btn-success btn-sm" data-toggle="tooltip" title="View"><i class="glyphicon glyphicon-eye-open"></i></a>|<a target="_blank" href="${pageContext.request.contextPath}/PrintContractorBillReceipt?workOrderId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-print"></i></a></td>');
									
									
								}
								else
								{
									var id = result[i].workOrderId;
									$('#contractorPaymentListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].workOrderId+'</td><td>'+result[i].contractorfirmName+'</td><td>'+result[i].projectId+'</td><td>'+result[i].buildingId+'</td><td>'+result[i].wingId+'</td><td>'+result[i].totalAmount+'</td><td>'+result[i].workAmount+'</td><td>'+result[i].totalPaidAmount+'</td><td><a href="${pageContext.request.contextPath}/AddContractorWorkPayment?workOrderId='+id+'" class="btn btn-success btn-sm" data-toggle="tooltip" title="View"><i class="glyphicon glyphicon-eye-open"></i></a>|<a target="_blank" href="${pageContext.request.contextPath}/PrintContractorBillReceipt?workOrderId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-print"></i></a></td>');
								}
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	 
}//end of get Building List


function getAllWingList()
{

	  $("#wingId").empty();
	  var buildingId = $('#buildingId').val();
	  var projectId = $('#projectId').val();
	   
	  $.ajax({

		url : '${pageContext.request.contextPath}/getprojectwingList',
		type : 'Post',
		data : { buildingId : buildingId, projectId : projectId },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Wing Name-");
							$("#wingId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].wingId).text(result[i].wingName);
							    $("#wingId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	 
	 
	 $("#contractorPaymentListTable tr").detach();
	 var projectId = $('#projectId').val();
	 var buildingId = $('#buildingId').val();
	 
	 $.ajax({

		url : '${pageContext.request.contextPath}/getBuildingwiseWingContractorPaymentList',
		type : 'Post',
		data : { buildingId : buildingId , projectId : projectId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
							$('#contractorPaymentListTable').append('<tr style="background-color: #4682B4;"><th style="width:100px">Work Order No</th><th>Contractor Firm Name</th><th>Project Name</th> <th>Building Name</th><th>Wing Name</th><th>Agg Amount</th><th>Work Completed Amount</th><th>Paid Amount</th> <th style="width:100px">Action</th>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].workOrderId;
									$('#contractorPaymentListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].workOrderId+'</td><td>'+result[i].contractorfirmName+'</td><td>'+result[i].projectId+'</td><td>'+result[i].buildingId+'</td><td>'+result[i].wingId+'</td><td>'+result[i].totalAmount+'</td><td>'+result[i].workAmount+'</td><td>'+result[i].totalPaidAmount+'</td><td><a href="${pageContext.request.contextPath}/AddContractorWorkPayment?workOrderId='+id+'" class="btn btn-success btn-sm" data-toggle="tooltip" title="View"><i class="glyphicon glyphicon-eye-open"></i></a>|<a target="_blank" href="${pageContext.request.contextPath}/PrintContractorBillReceipt?workOrderId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-print"></i></a></td>');
									
									
								}
								else
								{
									var id = result[i].workOrderId;
									$('#contractorPaymentListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].workOrderId+'</td><td>'+result[i].contractorfirmName+'</td><td>'+result[i].projectId+'</td><td>'+result[i].buildingId+'</td><td>'+result[i].wingId+'</td><td>'+result[i].totalAmount+'</td><td>'+result[i].workAmount+'</td><td>'+result[i].totalPaidAmount+'</td><td><a href="${pageContext.request.contextPath}/AddContractorWorkPayment?workOrderId='+id+'" class="btn btn-success btn-sm" data-toggle="tooltip" title="View"><i class="glyphicon glyphicon-eye-open"></i></a>|<a target="_blank" href="${pageContext.request.contextPath}/PrintContractorBillReceipt?workOrderId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-print"></i></a></td>');
								}
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
}


function getWingList()
{
	 $("#contractorPaymentListTable tr").detach();
	 var projectId = $('#projectId').val();
	 var buildingId = $('#buildingId').val();
	 var wingId = $('#wingId').val();
	 
	 $.ajax({

		url : '${pageContext.request.contextPath}/getWingwiseWingContractorPaymentList',
		type : 'Post',
		data : { wingId : wingId, buildingId : buildingId , projectId : projectId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
							$('#contractorPaymentListTable').append('<tr style="background-color: #4682B4;"><th style="width:100px">Work Order No</th><th>Contractor Firm Name</th><th>Project Name</th> <th>Building Name</th><th>Wing Name</th><th>Agg Amount</th><th>Work Completed Amount</th><th>Paid Amount</th> <th style="width:100px">Action</th>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].workOrderId;
									$('#contractorPaymentListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].workOrderId+'</td><td>'+result[i].contractorfirmName+'</td><td>'+result[i].projectId+'</td><td>'+result[i].buildingId+'</td><td>'+result[i].wingId+'</td><td>'+result[i].totalAmount+'</td><td>'+result[i].workAmount+'</td><td>'+result[i].totalPaidAmount+'</td><td><a href="${pageContext.request.contextPath}/AddContractorWorkPayment?workOrderId='+id+'" class="btn btn-success btn-sm" data-toggle="tooltip" title="View"><i class="glyphicon glyphicon-eye-open"></i></a>|<a target="_blank" href="${pageContext.request.contextPath}/PrintContractorBillReceipt?workOrderId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-print"></i></a></td>');
									
									
								}
								else
								{
									var id = result[i].workOrderId;
									$('#contractorPaymentListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].workOrderId+'</td><td>'+result[i].contractorfirmName+'</td><td>'+result[i].projectId+'</td><td>'+result[i].buildingId+'</td><td>'+result[i].wingId+'</td><td>'+result[i].totalAmount+'</td><td>'+result[i].workAmount+'</td><td>'+result[i].totalPaidAmount+'</td><td><a href="${pageContext.request.contextPath}/AddContractorWorkPayment?workOrderId='+id+'" class="btn btn-success btn-sm" data-toggle="tooltip" title="View"><i class="glyphicon glyphicon-eye-open"></i></a>|<a target="_blank" href="${pageContext.request.contextPath}/PrintContractorBillReceipt?workOrderId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-print"></i></a></td>');
								}
							 }  
						} 
						else
						{
							alert("failure111");
						}

					}
		});
}

  $(function () {
    $('#contractorPaymentListTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>

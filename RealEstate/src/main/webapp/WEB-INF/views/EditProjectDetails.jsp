<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Add Project Building</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
    <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
    </script>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

   <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Project Building Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Project Details</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="projectdetailsform" action="${pageContext.request.contextPath}/EditProjectDetails" method="Post" onSubmit="return validate()">

    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
            
			<div class="box-body">
              <div class="row">
                  <div class="col-xs-2">
			     <label>Project Details Id </label>
			     
                  <input type="text" class="form-control" id="projectDetailsId" name="projectDetailsId" value="${projectDetails[0].projectDetailsId}" readonly>
                
			     </div> 
				</div>
            </div>
			
			  <div class="box-body">
              <div class="row">
                  <div class="col-xs-3">
			     <label>Project Name </label> <label class="text-red">* </label>
			     <select class="form-control" name="projectId" id="projectId">
				  	<option selected="selected" value="${projectDetails[0].projectId}">${projectName}</option>
                   	 <c:forEach var="projectList" items="${projectList}">
                   	  <c:choose>
                   	   <c:when test="${projectDetails[0].projectId ne projectList.projectId}">
                    	<option value="${projectList.projectId}">${projectList.projectName}</option>
                       </c:when>
                      </c:choose>
				     </c:forEach>
				  </select> 
                   <span id="projectIdSpan" style="color:#FF0000"></span>
			     </div> 
          
                  <div class="col-xs-3">
				    <label for="landownerName"> Landowner Name </label> <label class="text-red">* </label>
	                <input type="text" class="form-control" id="landownerName" placeholder="Enter Previous Landowner Name" name="landownerName" value="${projectDetails[0].landownerName}">
			        <span id="landownerNameSpan" style="color:#FF0000"></span>
                  </div>
                  
                  <div class="col-xs-2">
                    <label>Project Type </label> <label class="text-red">* </label>
                    
                  		<select class="form-control" name="projectType" id="projectType">
                    	 <c:choose>
                  		  <c:when test="${projectDetails[0].projectType eq 'Own'}">
                    		<option selected="selected" value="Own">Own</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	     <option value="Own">Own</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 
                    	 <c:choose>
                    	  <c:when test="${projectDetails[0].projectType eq 'Joint Venture'}">
                    		<option selected="selected" value="Joint Venture">Joint Venture</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="Joint Venture">Joint Venture</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 
                    	 <c:choose>
                    	  <c:when test="${projectDetails[0].projectType eq 'Re-Development'}">
                    		<option selected="selected" value="Re-Development">Re-Development</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="Re-Development">Re-Development</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 
                  		</select>
                  
                    <span id="projectTypeSpan" style="color:#FF0000"></span>
			     </div> 
			   
                  <div class="col-xs-2">
				    <label for="landArea">Land Area </label> <label class="text-red">* </label>
	                <input type="text" class="form-control" id="landArea" placeholder="Enter land Area" name="landArea" value="${projectDetails[0].landArea}">
			        <span id="landAreaSpan" style="color:#FF0000"></span>
                  </div>
                    
                  <div class="col-xs-2">
				    <label for="landPrice">Land Price </label> <label class="text-red">* </label>
	                <input type="text" class="form-control" id="landPrice" placeholder="Enter land Price" name="landPrice" value="${projectDetails[0].landPrice}">
			        <span id="landPriceSpan" style="color:#FF0000"></span>
                  </div>
                  
				</div>
            </div>
			   
			   
			   
			  <div class="box-body">
			  <h4>GIS Details </h4>
			 
              <div class="row">
          
                  <div class="col-xs-2">
				    <label for="projectLatitude">Latitude </label> 
	                <input type="text" class="form-control" id="projectLatitude" placeholder="Enter project Latitude" name="projectLatitude" value="${projectDetails[0].projectLatitude}">
			        <span id="projectLatitudeSpan" style="color:#FF0000"></span>
                  </div>
             
                  <div class="col-xs-2">
				    <label for="projectLongitude">Longitude </label> 
	                <input type="text" class="form-control" id="projectLongitude" placeholder="Enter project Longitude" name="projectLongitude" value="${projectDetails[0].projectLongitude}">
			        <span id="projectLongitudeSpan" style="color:#FF0000"></span>
                  </div>
                    
               
				</div>
            </div>
			  
		
		<div class="box-body">
            <div class="row">
            
               <div class="col-md-12">
	              <div class="box-body">
	              
	                <div class="form-group">
	                  <label for="inputEmail3" class="col-sm-2 control-label">Location</label>
	
	                 <div class="col-sm-2">
	                  <label for="inputEmail3" class="col-sm-2 control-label">Location</label>
	
	                 </div>
	                  
	                 <div class="col-sm-2">
	                  <label for="inputEmail3" class="col-sm-2 control-label"> Google</label>
	
	                 </div>
	                  
	                </div>
	                
	                 
	                </div>
	             </div>
	                
	             <div class="col-md-12">
	              <div class="box-body">
	           
	                <div class="form-group">
	                  <label for="inputEmail3" class="col-sm-2 control-label">Boundaries East</label>
	
	                 <div class="col-sm-2">
	                  <input type="text" class="form-control" id="locationInEast" placeholder="location In East" name="locationInEast" value="${projectDetails[0].locationInEast}">
			          <span id="locationInEastSpan" style="color:#FF0000"></span>
	                 </div>
	                  
	                 <div class="col-sm-2">
	                  <input type="text" class="form-control" id="googleLocationInEast" placeholder="Google Location In East" name="googleLocationInEast" value="${projectDetails[0].googleLocationInEast}">
			          <span id="googleLocationInEastSpan" style="color:#FF0000"></span>
	                 </div>
	                  
	                </div>
	                
	                </div>
	             </div>
	                
	             <div class="col-md-12">
	              <div class="box-body">
	           
	                <div class="form-group">
	                  <label for="inputEmail3" class="col-sm-2 control-label">Boundaries West</label>
	
	                 <div class="col-sm-2">
	                <input type="text" class="form-control" id="locationInWest" placeholder="location In West" name="locationInWest" value="${projectDetails[0].locationInWest}">
			        <span id="locationInWestSpan" style="color:#FF0000"></span>
	                 </div>
	                  
	                 <div class="col-sm-2">
	                <input type="text" class="form-control" id="googleLocationInWest" placeholder="Google Location In West" name="googleLocationInWest" value="${projectDetails[0].googleLocationInWest}">
			        <span id="googleLocationInWestSpan" style="color:#FF0000"></span>
	                 </div>
	                  
	                </div>
	              
	                </div>
	             </div>
	                
	             <div class="col-md-12">
	              <div class="box-body">
	           
	                <div class="form-group">
	                  <label for="inputEmail3" class="col-sm-2 control-label">Boundaries North</label>
	
	                 <div class="col-sm-2">
	                <input type="text" class="form-control" id="locationInNorth" placeholder="location In North" name="locationInNorth" value="${projectDetails[0].locationInNorth}">
			        <span id="locationInNorthSpan" style="color:#FF0000"></span>
	                 </div>
	                  
	                 <div class="col-sm-2">
	                <input type="text" class="form-control" id="googleLocationInNorth" placeholder="Google Location In North" name="googleLocationInNorth" value="${projectDetails[0].googleLocationInNorth}">
			        <span id="googleLocationInNorthSpan" style="color:#FF0000"></span>
	                 </div>
	                  
	                </div>
	              
	                </div>
	             </div>
	                
	             <div class="col-md-12">
	              <div class="box-body">
	           
	                <div class="form-group">
	                  <label for="inputEmail3" class="col-sm-2 control-label">Boundaries South</label>
	
	                 <div class="col-sm-2">
	                <input type="text" class="form-control" id="locationInSouth" placeholder="location In South" name="locationInSouth" value="${projectDetails[0].locationInSouth}">
			        <span id="locationInSouthSpan" style="color:#FF0000"></span>
	                 </div>
	                  
	                 <div class="col-sm-2">
	                <input type="text" class="form-control" id="googleLocationInSouth" placeholder="Google Location In South" name="googleLocationInSouth" value="${projectDetails[0].googleLocationInSouth}">
			        <span id="googleLocationInSouthSpan" style="color:#FF0000"></span>
	                 </div>
	                  
	                </div>
	           
	              </div>
	 	
		    </div>
		 </div>
	 </div>
		
		<div class="box-body">
              <div class="row">
              
              	<div class="col-xs-2">
			    <label>Account Holder Name</label> <label class="text-red">* </label>
                <input type="text" class="form-control" id="accountHolderName" placeholder="Account Holder Name " name="accountHolderName">
			     <span id="accountHolderNameSpan" style="color:#FF0000"></span>
			     </div> 
			     
              	<div class="col-xs-2">
			    <label>Bank Name </label> <label class="text-red">* </label>
                <input type="text" class="form-control" id="bankName" placeholder="Bank Name " name="bankName">
			     <span id="bankNameSpan" style="color:#FF0000"></span>
			     </div> 
			     
			     <div class="col-xs-2">
			    <label>Bank Branch Name </label> <label class="text-red">* </label>
			        <input type="text" class="form-control" id="branchName" placeholder="Bank Branch No " name="branchName">
				 <span id="branchNameSpan" style="color:#FF0000"></span>
			     </div> 
			     <div class="col-xs-2">
			    <label>IFSC </label> <label class="text-red">* </label>
                 <input type="text" class="form-control" id="ifscCode" placeholder="Bank IFSC No " name="ifscCode">
			     <span id="ifscCodeSpan" style="color:#FF0000"></span>
			     </div> 
			     
                  <div class="col-xs-2">
			    <label for="accountNumber">Bank A/C No </label> <label class="text-red">* </label>
                <div class="input-group">
         		 <input type="text" class="form-control" id="accountNumber" placeholder="Bank A/C No " name="accountNumber">
                <span id="accountNumberSpan" style="color:#FF0000"></span>
         		 <span class="input-group-btn">
            	  <button type="button" class="btn btn-success" onclick="AddProjectBankDetails()"><i class="fa fa-plus"></i>Add </button>
              	</span>
       			 </div>
                </div> 
			    
			     </div>
			     </div>
			   
			   
			   
			  <div class="box-body">
              <div class="row">
              
               <div class="col-xs-12">
	               
	              <table class="table table-bordered" id="projectBanktable">
	              <thead>
		                  <tr bgcolor=#4682B4 style="color: white;">
		                 <td>Account Holder Name</td>
		                 <td>Bank Name</td>
	             		 <td>Branch Name</td>
	             		 <td>IFSC</td>
	             		 <td>Account number</td>
		                 <td>Action</td> 
		                  </tr>
	               </thead>
	              
	              
                  <tbody >
                   <c:forEach items="${projectBankList}" var="projectBankList" varStatus="loopStatus">
                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                       	<td>${projectBankList.accountHolderName}</td>
	                    <td>${projectBankList.bankName}</td>
	                    <td>${projectBankList.branchName}</td>
	                    <td>${projectBankList.ifscCode}</td>
	                    <td>${projectBankList.accountNumber}</td>
	                    <td><a onclick="DeleteProjectBankDetails('${projectBankList.projectBankDetailsId}')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td>
                    </tr>
				   </c:forEach> 
                 </tbody>
	           
	              <tbody>
	           
                 </tbody>
              
              </table>
              </div>
              
              
              </div>
              </div>
			   
								
			  <input type="hidden" id="creationDate" name="creationDate"  value="${projectDetails[0].creationDate}">
			  <input type="hidden" id="updateDate" name="updateDate" value="">
			  <input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>">
			  		
			
            </div>

			
          </div>
		   <div class="box-body">
              <div class="row">
              </br>
                    <div class="col-xs-5">
	                 <div class="col-xs-2">
	                  <a href="ProjectDetailsMaster"><button type="button" class="btn btn-block btn-primary" style="width:90px">Back</button></a>
				     </div>
			       </div>
				  <div class="col-xs-4">
                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
              
			     </div>
					<div class="col-xs-2">
			  <button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
              
			     </div> 
			     
			   
              </div>
			  </div>
		  
		  
        </div>
        
      </div>
     
    </section>
	</form>
    <!-- /.content -->
  </div>
 

  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script>
function clearall()
{
	$('#projectIdSpan').html('');
	$('#landownerNameSpan').html('');
	$('#projectTypeSpan').html('');	
	$('#landAreaSpan').html('');	
	$('#landPriceSpan').html('');	
	
	$('#locationInEastSpan').html('');
	$('#locationInNorthSpan').html('');	
	$('#locationInSouthSpan').html('');
	$('#locationInWestSpan').html('');	
	
	$('#accountHolderNameSpan').html('');
	$('#bankNameSpan').html('');
	$('#branchNameSpan').html('');	
	$('#ifscCodeSpan').html('');	
	$('#accountNumberSpan').html('');	
	
}


function AddProjectBankDetails()
{
	clearall();
	
	//validation for project name
	if(document.projectdetailsform.projectId.value=="Default")
	{
		 $('#projectIdSpan').html('Please, select project name..!');
		document.projectdetailsform.projectId.focus();
		return false;
	}

	if(document.projectdetailsform.accountHolderName.value=="")
	{
		 $('#accountHolderNameSpan').html('Please, enter Account Holder name..!');
		document.projectdetailsform.accountHolderName.focus();
		return false;
	}

	if(document.projectdetailsform.bankName.value=="")
	{
		 $('#bankNameSpan').html('Please, enter Bank name..!');
		document.projectdetailsform.bankName.focus();
		return false;
	}

	if(document.projectdetailsform.branchName.value=="")
	{
		 $('#branchNameSpan').html('Please, enter branch Name..!');
		document.projectdetailsform.branchName.focus();
		return false;
	}

	if(document.projectdetailsform.ifscCode.value=="")
	{
		 $('#ifscCodeSpan').html('Please, enter IFSC Code..!');
		document.projectdetailsform.ifscCode.focus();
		return false;
	}

	if(document.projectdetailsform.accountNumber.value=="")
	{
		 $('#accountNumberSpan').html('Please, enter Account Holder..!');
		document.projectdetailsform.accountNumber.focus();
		return false;
	}
	
	
	
	$("#projectBanktable td").detach();
	

	var projectDetailsId = $('#projectDetailsId').val();
	var projectId = $('#projectId').val();
	
	var accountHolderName = $('#accountHolderName').val();
	var bankName = $('#bankName').val();
	var branchName = $('#branchName').val();
	var ifscCode = $('#ifscCode').val();
	var accountNumber = $('#accountNumber').val();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/AddProjectBankDetails',
		type : 'Post',
		data : { projectDetailsId : projectDetailsId, projectId : projectId, 
			accountHolderName : accountHolderName, bankName : bankName, branchName : branchName,
			ifscCode : ifscCode, accountNumber : accountNumber},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							 
						$('#projectBanktable').append('<tr style="background-color: #4682B4;"><td style="color: white;">Account Holder Name</td> <td style="color: white;">Bank Name</td> <td style="color: white;">Branch Name</td><td style="color: white;">IFSC</td> <td style="color: white;">Account number  </td><td style="color: white;">Action </td>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].projectBankDetailsId;
									$('#projectBanktable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].accountHolderName+'</td><td>'+result[i].bankName+'</td><td>'+result[i].branchName+'</td><td>'+result[i].ifscCode+'</td><td>'+result[i].accountNumber+'</td><td><a onclick="DeleteProjectBankDetails(\''+id+'\')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
								else
								{
									var id = result[i].projectBankDetailsId;
									$('#projectBanktable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].accountHolderName+'</td><td>'+result[i].bankName+'</td><td>'+result[i].branchName+'</td><td>'+result[i].ifscCode+'</td><td>'+result[i].accountNumber+'</td><td><a onclick="DeleteProjectBankDetails(\''+id+'\')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
							
							 } 
							 
						} 
						else
						{
							alert("failure111");
						}

					}
		});	
	
	document.projectdetailsform.accountHolderName.value="";
	document.projectdetailsform.bankName.value="";
	document.projectdetailsform.branchName.value="";
	document.projectdetailsform.ifscCode.value="";
	document.projectdetailsform.accountNumber.value="";
	
}


function DeleteProjectBankDetails(projectBankDetailsId)
{
	
	$("#projectBanktable td").detach();
	
	var projectDetailsId = $('#projectDetailsId').val();
	var projectId = $('#projectId').val();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/DeleteProjectBankDetails',
		type : 'Post',
		data : { projectBankDetailsId : projectBankDetailsId, projectDetailsId : projectDetailsId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							 
						$('#projectBanktable').append('<tr style="background-color: #4682B4;"><td style="color: white;">Account Holder Name</td> <td style="color: white;">Bank Name</td> <td style="color: white;">Branch Name</td><td style="color: white;">IFSC</td> <td style="color: white;">Account number  </td><td style="color: white;">Action </td>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].projectBankDetailsId;
									$('#projectBanktable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].accountHolderName+'</td><td>'+result[i].bankName+'</td><td>'+result[i].branchName+'</td><td>'+result[i].ifscCode+'</td><td>'+result[i].accountNumber+'</td><td><a onclick="DeleteProjectBankDetails(\''+id+'\')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
								else
								{
									var id = result[i].projectBankDetailsId;
									$('#projectBanktable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].accountHolderName+'</td><td>'+result[i].bankName+'</td><td>'+result[i].branchName+'</td><td>'+result[i].ifscCode+'</td><td>'+result[i].accountNumber+'</td><td><a onclick="DeleteProjectBankDetails(\''+id+'\')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
							
							 } 
							 
						} 
						else
						{
							alert("failure111");
						}

					}
		});	
	
}


function validate()
{ 
	clearall();
		//validation for project name
		if(document.projectdetailsform.projectId.value=="Default")
		{
			 $('#projectIdSpan').html('Please, select project name..!');
			document.projectdetailsform.projectId.focus();
			return false;
		}


		if(document.projectdetailsform.projectType.value=="Default")
		{
			 $('#projectTypeSpan').html('Please, select project type..!');
			document.projectdetailsform.projectType.focus();
			return false;
		}
		if(document.projectdetailsform.landownerName.value=="")
		{
			 $('#landownerNameSpan').html('Please, enter land owner name..!');
			document.projectdetailsform.landownerName.focus();
			return false;
		}
		
		if(document.projectdetailsform.landArea.value=="")
		{
			 $('#landAreaSpan').html('Please, enter previous land area..!');
			document.projectdetailsform.landArea.focus();
			return false;
		}
		else if(document.projectdetailsform.landArea.value.match(/^[\s]+$/))
		{
			$('#landPriceSpan').html('Please, enter land area..!');
			document.projectdetailsform.landArea.value="";
			document.projectdetailsform.landArea.focus();
			return false; 	
		}
		else if(!document.projectdetailsform.landArea.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			 if(!document.projectdetailsform.landArea.value.match(/^[0-9]+$/))
				 {
					$('#landAreaSpan').html('Please, use only digit value for land area..! eg:21.36 OR 30');
					document.projectdetailsform.landArea.value="";
					document.projectdetailsform.landArea.focus();
					return false;
				}
		}
		if(document.projectdetailsform.landPrice.value=="")
		{
			 $('#landPriceSpan').html('Please, enter previous land Price..!');
			document.projectdetailsform.landPrice.focus();
			return false;
		}
		else if(document.projectdetailsform.landPrice.value.match(/^[\s]+$/))
		{
			$('#landPriceSpan').html('Please, enter land price...!');
			document.projectdetailsform.landPrice.value="";
			document.projectdetailsform.landPrice.focus();
			return false; 	
		}
		else if(!document.projectdetailsform.landPrice.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			 if(!document.projectdetailsform.landPrice.value.match(/^[0-9]+$/))
				 {
					$('#landPriceSpan').html('Please, use only digit value for land price..! eg:21.36 OR 30');
					document.projectdetailsform.landPrice.value="";
					document.projectdetailsform.landPrice.focus();
					return false;
				}
		}
		
		/* 
		if(document.projectdetailsform.locationInNorth.value=="")
		{
			 $('#locationInNorthSpan').html('Please, enter location by east..!');
			document.projectdetailsform.locationInNorth.focus();
			return false;
		}
		else if(!document.projectdetailsform.locationInNorth.value.match(/^[0-9]{1,5}$/))
		{
			 $('#locationInNorthSpan').html('Please, enter valid location by North..!');
			document.projectdetailsform.locationInNorth.focus();
			return false;			
		}
		
		
		if(document.projectdetailsform.locationInSouth.value=="")
		{
			 $('#locationInSouthSpan').html('Please, enter location by east..!');
			document.projectdetailsform.locationInSouth.focus();
			return false;
		}
	
		
		if(document.projectdetailsform.locationInEast.value=="")
		{
			 $('#locationInEastSpan').html('Please, enter location by East..!');
			document.projectdetailsform.locationInEast.focus();
			return false;
		}
	

		if(document.projectdetailsform.locationInWest.value=="")
		{
			 $('#locationInWestSpan').html('Please, enter location by West..!');
			document.projectdetailsform.locationInWest.focus();
			return false;
		}
	 */
		
}
function init()
{
	clearall();
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	//document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	
   	 document.projectdetailsform.projectId.focus();
}


  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>

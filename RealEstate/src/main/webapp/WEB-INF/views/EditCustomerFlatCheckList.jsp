<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate |Customer Flat CheckList</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


  <!-- Google Font -->
    <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>


<div class="wrapper">

  <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Customer Flat CheckList Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Edit Customer Flat CheckList</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="customerflatchecklistform" action="${pageContext.request.contextPath}/EditCustomerFlatCheckList" onSubmit="return validate()" method="post">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <span id="statusSpan" style="color:#FF0000"></span>
              <!-- /.form-group -->
          
							
		 <div class="box-body">
              <div class="row">
               <div class="col-md-2">
                  <label>Ref No:</label>
                  <input type="text" class="form-control" id="checkListId" name="checkListId" value="${customerflatchecklistDetails[0].checkListId}" readonly>
                </div>
        	     <div class="col-md-3">
        	     
        	     </div>
			     <div class="col-xs-3">
                   <div class="input-group">
                 	<h3><span class="fa fa-home"></span> 
                    <label>CHECKLIST:  </label> 
                   </h3> 
       		 	  </div>
			     </div> 
			      <div class="col-md-4">
                
                </div>
              </div>
              </div>
        	<input type="hidden" id="bookingId" name="bookingId" value="${bookingDetails[0].bookingId}">
        	<input type="hidden" id="aggreementId" name="aggreementId" value="${aggreementDetails[0].aggreementId}">
    		 <div class="box-body">
              <div class="row">
                   <div class="col-xs-3">
                   	<div class="input-group">
                 	<h4><span class="fa fa-university"></span> 
                   	<label >Project Name : ${projectName}</label> 
                   	</h4> 
       		 		</div>
			      </div>  
			     
			     <div class="col-xs-3">
                   <div class="input-group">
                 	<h4><span class="fa fa-certificate"></span> 
                    <label>Building Name : ${buildingName} </label> 
                   </h4> 
       		 	  </div>
			     </div>  
			     
			     <div class="col-xs-3">
                   <div class="input-group">
                 	<h4><span class="fa fa-cube"></span> 
                    <label>Wing Name : ${wingName} </label> 
                    </h4> 
       		 	   </div>
			     </div>  
			     
			     <div class="col-xs-3">
                   <div class="input-group">
                 	<h4><span class="fa fa-align-center"></span> 
                    <label >Floor Name : ${floortypeName} </label> 
                    </h4> 
       		 	   </div>
			     </div>
			     
			     <div class="col-xs-3">
                   <div class="input-group">
                 	<h4><span class="fa fa-home"></span> 
                    <label >Flat Number : ${flatNumber} </label> 
                    </h4> 
       		 	   </div>
			     </div>  
			     
				
			      
              </div>
            </div>   	
        	
        	<div class="box-body">
        	
              <div class="row">
                <div class="col-xs-3">
                </div>
              <div class="col-xs-5">
              <div class="table-responsive">
              <table id="checklisttable" class="table table-bordered table-striped">

                  <thead>
	                  <tr bgcolor=#4682B4>
	                  	<td style="width:50px"><b>Sr.No</b></td>
	                    <td style="width:200px"><b>ITEM INSPECTED</b></td>
	                    <td style="width:50px"><b>REMARKS</b></td>
	                  </tr>
                  </thead>
                  <tbody>
                      <tr>
	                      <td>1</td>
	                      <td>CEILINGS</td>
	                      <td>
	                    <c:choose>
			     		<c:when test="${customerflatchecklistDetails[0].ceilings eq 'CEILINGS'}">
                  		<input type="checkbox" name="ceilings" id="ceilings" value="CEILINGS" checked="checked">
                		</c:when>
                		<c:otherwise>  
                 		<input type="checkbox" name="ceilings" id="ceilings" value="CEILINGS">
                 		</c:otherwise>
			   		    </c:choose>
			  			 </td>
	               	  </tr>
	               	  <tr>
	                      <td>2</td>
	                      <td>TILES</td>
	                      <td>
	                    <c:choose>
			     		<c:when test="${customerflatchecklistDetails[0].tiles eq 'TILES'}">
                  		<input type="checkbox" name="tiles" id="tiles"  checked="checked" value="TILES">
                		</c:when>
                		<c:otherwise>  
                 		<input type="checkbox" name="tiles" id="tiles" value="TILES">
                 		</c:otherwise>
			   		    </c:choose>
	                   </td>
	               	  </tr>
	               	  <tr>
	                      <td>3</td>
	                      <td>DOOR</td>
	                      <td>
	                    <c:choose>
			     		<c:when test="${customerflatchecklistDetails[0].door eq 'DOOR'}">
                  		<input type="checkbox" name="door" id="door" checked="checked" value="DOOR">
                		</c:when>
                		<c:otherwise>  
                 		<input type="checkbox" name="door" id="door"  value="DOOR">
                 		</c:otherwise>
			   		    </c:choose>
	                    </td>
	               	  </tr>
	               	  <tr>
	                      <td>4</td>
	                      <td>WINDOWS</td>
	                      <td>
	                    <c:choose>
			     		<c:when test="${customerflatchecklistDetails[0].windows eq 'WINDOWS'}">
                  		<input type="checkbox" name="windows" id="windows" checked="checked" value="WINDOWS">
                		</c:when>
                		<c:otherwise>  
                 		<input type="checkbox" name="windows" id="windows"  value="WINDOWS">
                 		</c:otherwise>
			   		    </c:choose>
	                    </td>
	               	  </tr>
	               	  <tr>
	                      <td>5</td>
	                      <td>PLUMBING & SANITORY</td>
	                      <td>
	                    <c:choose>
			     		<c:when test="${customerflatchecklistDetails[0].plumbingAndsanitory eq 'PLUMBING & SANITORY'}">
                  		<input type="checkbox" name="plumbingAndsanitory" id="plumbingAndsanitory" checked="checked" value="PLUMBING & SANITORY">
                		</c:when>
                		<c:otherwise>  
                 		<input type="checkbox" name="plumbingAndsanitory" id="plumbingAndsanitory"  value="PLUMBING & SANITORY">
                 		</c:otherwise>
			   		    </c:choose>
	                    </td>
	               	  </tr>
	               	  <tr>
	                      <td>6</td>
	                      <td>ELECTRIFICATION</td>
	                      <td>
	                    <c:choose>
			     		<c:when test="${customerflatchecklistDetails[0].electrification eq 'ELECTRIFICATION'}">
                  		<input type="checkbox" name="electrification" id="electrification" checked="checked" value="ELECTRIFICATION">
                		</c:when>
                		<c:otherwise>  
                 		<input type="checkbox" name="electrification" id="electrification"  value="ELECTRIFICATION">
                 		</c:otherwise>
			   		    </c:choose>
	                      </td>
	               	  </tr>
	               	  <tr>
	                      <td>7</td>
	                      <td>PAINTING(INTERNAL)</td>
	                      <td>
	                    <c:choose>
			     		<c:when test="${customerflatchecklistDetails[0].painting eq 'PAINTING'}">
                  		<input type="checkbox" name="painting" id="painting" checked="checked" value="PAINTING">
                		</c:when>
                		<c:otherwise>  
                 		<input type="checkbox" name="painting" id="painting"  value="PAINTING">
                 		</c:otherwise>
			   		    </c:choose>
	                    </td>
	               	  </tr>
                 
                 </tbody>
                </table>
                </div>
                </div>
              </div>
            </div>
        	
		 <div class="box-body">
           <div class="row">
            <div class="col-md-3">
               <label>Property Tax No</label> 
               <input type="text" class="form-control" id="propertyTaxNo" name="propertyTaxNo" placeholder="Property TaxNo"  value="${customerflatchecklistDetails[0].propertyTaxNo}">
                  <span id="propertyTaxNoSpan" style="color:#FF0000"></span>
             </div>
              <div class="col-md-3">
               <label>Electric Bill Meter No</label> 
               <input type="text" class="form-control" id="electricBillMeterNo" name="electricBillMeterNo" placeholder="Electric Bill Meter No"  value="${customerflatchecklistDetails[0].electricBillMeterNo}">
                  <span id="electricBillMeterNoSpan" style="color:#FF0000"></span>
             </div>
         	</div>
            </div>
            <div class="panel box box-danger"></div>
            <div class="box-body">
           <div class="row">
           <div class="col-md-3">
           	<h4><span class="fa fa-cube"></span> 
                <label>Add Door Keys </label> 
              </h4> 
             </div>
           </div>
           </div>
           
             <div class="box-body">
           <div class="row">
          
            <div class="col-md-3">
               <label>Door Type</label> 
               <input type="text" class="form-control" id="doorType" name="doorType" placeholder="Door Type">
                  <span id="doorTypeSpan" style="color:#FF0000"></span>
           </div>
       
            <div class="col-md-3">
               <label>No of Key Set</label> 
               <input type="text" class="form-control" id="noOfkeySet" name="noOfkeySet" placeholder="No of Key Set">
                  <span id="noOfkeySetSpan" style="color:#FF0000"></span>
           </div>
       
           <div class="col-md-3">
            <label>Key No</label> 
	  			<div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-key"></i>
                  </div>
                  <input type="text" class="form-control" id="keyNo" name="keyNo" placeholder="Key No">
                <span class="input-group-btn">
            	    <button type="button" class="btn btn-success" onclick="return AddMoreDetails()"><i class="fa fa-plus"></i>Add</button>
              	   </span>
                </div>
           </div>
           </div>
           </div>
           
           <div class="box-body">
           <div class="row">     
           <div class="col-xs-1">
           </div>   
        	<div class="col-xs-8">
              <table class="table table-bordered" id="addMoreDetialsTable">
	              <tr bgcolor=#4682B4>
		              <th style="width:50px">Sr.No</th>
		              <th>Door Type</th>
		              <th>No of Key Set</th>
		              <th>Key No</th>
		              <th>Action</th>
	               </tr>
					<c:forEach items="${morecheckListdetialsList}" var="morecheckListdetialsList" varStatus="loopStatus">
                    <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                    	<td>${loopStatus.index+1}</td>
                    	<td>${morecheckListdetialsList.doorType}</td>
                        <td>${morecheckListdetialsList.noOfkeySet}</td>
                        <td>${morecheckListdetialsList.keyNo}</td>
                        <td>
                          <a onclick="DeleteMoreDetails('${morecheckListdetialsList.typeId}')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a>
                        </td>
                     </tr>
                    </c:forEach> 
              </table>
            </div>
           </div>
          </div>
            <div class="box-body">
            <div class="row">
            
                   
				<input type="hidden" id="creationDate" name="creationDate" value="${customerflatchecklistDetails[0].creationDate}">
				<input type="hidden" id="updateDate" name="updateDate" value="">
				<input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>">
              <!-- /.form-group -->
            </div>
            </div>
             </div>
    
          </div>
		   	  <div class="box-body">
              <div class="row">
                      <div class="col-xs-4">
                      <div class="col-xs-2">
                	<a href="BookingMaster"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
			     </div>
			     </div>
				  <div class="col-xs-3">
                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
              
			     </div>
					<div class="col-xs-2">
			  <button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
              
			     </div> 
              </div>
			  </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        
      </div>
    
      </section>
	</form>
    <!-- /.content -->
    
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
    </script>
<script>
function clearall()
{
	$('#locationareaIdSpan').html('');
	$('#cityIdSpan').html('');
	$('#stateIdSpan').html('');
	$('#countryIdSpan').html('');
	$('#pinCodeSpan').html('');
}

function validate()
{ 
	/* clearall();
		if(document.locationareaform.countryId.value=="Default")
		{
			$('#countryIdSpan').html('Please, select country name..!');
			document.locationareaform.countryId.focus();
			return false;
		}
		
		if(document.locationareaform.stateId.value=="Default")
		{
			$('#stateIdSpan').html('Please, select state name..!');
			document.locationareaform.stateId.focus();
			return false;
		}
		
		if(document.locationareaform.cityId.value=="Default")
		{
			$('#cityIdSpan').html('Please, select city name..!');
			document.locationareaform.cityId.focus();
			return false;
		}
		
		//validation for the location area name
		if(document.locationareaform.locationareaId.value=="")
		{
			//$('#locationareaIdSpan').html('Please, enter location area name..!');
			document.locationareaform.locationareaId.value="";
			document.locationareaform.locationareaId.focus();
			return false;
		}
		else if(document.locationareaform.locationareaId.value.match(/^[\s]+$/))
		{
			$('#locationareaIdSpan').html('Please, enter location area name..!');
			document.locationareaform.locationareaId.value="";
			document.locationareaform.locationareaId.focus();
			return false; 	
		}
	
		  //validation for pincode
		if(document.locationareaform.pinCode.value=="")
		{
			$('#pinCodeSpan').html('Please, enter valid 6 digit pincode');
			document.locationareaform.pinCode.focus();
			return false;
		}
		 */
}

function AddMoreDetails()
{

	if(document.customerflatchecklistform.doorType.value=="")
	{
		$('#doorTypeSpan').html('Door Type name should not be empty..!');
		document.customerflatchecklistform.doorType.value="";
		document.customerflatchecklistform.doorType.focus();
		return false;
	}
	else if(document.customerflatchecklistform.doorType.value.match(/^[\s]+$/))
	{
		$('#doorTypeSpan').html('Door Type name should not be empty..!');
		document.customerflatchecklistform.doorType.value="";
		document.customerflatchecklistform.doorType.focus();
		return false;
	}
	if(document.customerflatchecklistform.noOfkeySet.value=="")
	{
		$('#noOfkeySetSpan').html('No of key Set should not be empty..!');
		document.customerflatchecklistform.noOfkeySet.value="";
		document.customerflatchecklistform.noOfkeySet.focus();
		return false;
	}
	else if(document.customerflatchecklistform.noOfkeySet.value.match(/^[\s]+$/))
	{
		$('#noOfkeySetSpan').html('No of key Set should not be empty..!');
		document.customerflatchecklistform.noOfkeySet.value="";
		document.customerflatchecklistform.noOfkeySet.focus();
		return false;
	}
	if(document.customerflatchecklistform.keyNo.value=="")
	{
		$('#detailsSpan').html('Key No should not be empty..!');
		document.customerflatchecklistform.keyNo.value="";
		document.customerflatchecklistform.keyNo.focus();
		return false;
	}
	else if(document.customerflatchecklistform.keyNo.value.match(/^[\s]+$/))
	{
		$('#detailsSpan').html('Key No should not be empty..!');
		document.customerflatchecklistform.keyNo.value="";
		document.customerflatchecklistform.keyNo.focus();
		return false;
	}
	
	$('#addMoreDetialsTable tr').detach();
	 
	 var checkListId = $('#checkListId').val();
	 var bookingId = $('#bookingId').val();
	 var aggreementId = $('#aggreementId').val();
	 var doorType = $('#doorType').val();
	 var noOfkeySet=$('#noOfkeySet').val();
	 var keyNo = $('#keyNo').val();
	 
	 $.ajax({

		 url : '${pageContext.request.contextPath}/AddMoreCheckListDetails',
		type : 'Post',
		data : { checkListId : checkListId, bookingId : bookingId, aggreementId : aggreementId, doorType : doorType, noOfkeySet : noOfkeySet, keyNo : keyNo},
		dataType : 'json',
		success : function(result)
				  {
					   if (result) 
					   { 
							$('#addMoreDetialsTable').append('<tr style="background-color: #4682B4;"><th style="width:50px">Sr.No</th><th style="width:150px">Door Type</th><th style="width:150px">No of Key Set </th><th style="width:150px">Key No</th><th style="width:100px">Action</th>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].typeId;
									$('#addMoreDetialsTable').append('<tr style="background-color: #F0F8FF;"><td>'+(i+1)+'</td><td>'+result[i].doorType+'</td><td>'+result[i].noOfkeySet+'</td><td>'+result[i].keyNo+'</td><td><a onclick="DeleteMoreDetails(\''+id+'\')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
								else
								{
									var id = result[i].typeId;
									$('#addMoreDetialsTable').append('<tr style="background-color: #CCE5FF;"><td>'+(i+1)+'</td><td>'+result[i].doorType+'</td><td>'+result[i].noOfkeySet+'</td><td>'+result[i].keyNo+'</td><td><a onclick="DeleteMoreDetails(\''+id+'\')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}
				  } 

		});
	 
	 $('#doorType').val("")
	 $('#noOfkeySet').val("");
}

function DeleteMoreDetails(typeId)
{
	var checkListId = $('#checkListId').val();
    
    $('#addMoreDetialsTable tr').detach();
    
    $.ajax({

		 url : '${pageContext.request.contextPath}/DeleteMoreCheckListDetails',
		type : 'Post',
		data : { checkListId : checkListId, typeId : typeId },
		dataType : 'json',
		success : function(result)
				  {
				   if (result) 
				   { 
						$('#addMoreDetialsTable').append('<tr style="background-color: #4682B4;"><th style="width:50px">Sr.No</th><th style="width:150px">Type</th><th style="width:150px">Detials</th><th style="width:100px">Action</th>');
					
						for(var i=0;i<result.length;i++)
						{ 
							if(i%2==0)
							{
								var id = result[i].typeId;
								$('#addMoreDetialsTable').append('<tr style="background-color: #F0F8FF;"><td>'+(i+1)+'</td><td>'+result[i].type+'</td><td>'+result[i].details+'</td><td><a onclick="DeleteMoreDetails(\''+id+'\')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
							}
							else
							{
								var id = result[i].typeId;
								$('#addMoreDetialsTable').append('<tr style="background-color: #CCE5FF;"><td>'+(i+1)+'</td><td>'+result[i].type+'</td><td>'+result[i].details+'</td><td><a onclick="DeleteMoreDetails(\''+id+'\')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
							}
						
						 } 
					} 
					else
					{
						alert("failure111");
					}
		  } 

		});	
}

function init()
{
	clearall();
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	/* document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	 */document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	/*  
	 if(document.locationareaform.locationAreaStatus.value == "Fail")
	 {
	  	//alert("Sorry, record is present already..!");
	 }
	 else if(document.locationareaform.locationAreaStatus.value == "Success")
	 {
		 $('#statusSpan').html('Record added successfully..!');
	 }
	 
  	document.locationareaform.countryId.focus(); */
}



  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>

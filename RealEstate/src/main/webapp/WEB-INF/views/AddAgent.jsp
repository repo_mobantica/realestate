<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Real Estate | Add Challan</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
<!-- daterange picker -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<!-- bootstrap datepicker -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
<!-- Bootstrap Color Picker -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
<!-- Bootstrap time Picker -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
<!-- Select2 -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
<!-- Theme style -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

<!-- Google Font -->
<style type="text/css">
tr.odd {
	background-color: #CCE5FF
}

tr.even {
	background-color: #F0F8FF
}
</style>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
    </script>
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
	<div class="wrapper">

		<%@ include file="headerpage.jsp"%>
		<!-- Left side column. contains the logo and sidebar -->
		<%@ include file="menu.jsp"%>
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Add Challan Details: <small>Preview</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><a href="#">Master</a></li>
					<li class="active">Add Challan</li>
				</ol>
			</section>

			<!-- Main content -->
			<form name="agentform"
				action="${pageContext.request.contextPath}/AddAgent"
				onSubmit="return validate()" method="post">

				<section class="content">

					<!-- SELECT2 EXAMPLE -->
					<div class="box box-default">
						<span id="statusSpan" style="color: #FF0000"></span>
						<!-- /.box-header -->

						<div class="box-body">
							<div class="row">
								<div class="col-md-12">
									<span id="statusSpan" style="color: #FF0000"></span>

									<div class="box-body">
										<div class="row">
											<div class="col-xs-3">
												<label for="agentId">Agent Id </label> <input type="text"
													class="form-control" id="agentId" name="agentId"
													value="${agentCode}" readonly>
											</div>
										</div>
									</div>

									<div class="box-body">
										<div class="row">
											<div class="col-xs-3">
												<label for="agentfirstName">Agent Firm Name</label> <label
													class="text-red">* </label> <input type="text"
													class="form-control" id="agentfirmName"
													placeholder="First Name" name="agentfirmName"
													style="text-transform: capitalize;"
													onchange="getclearstatus()"> <span
													id="agentfirmNameSpan" style="color: #FF0000"></span>
											</div>

											<div class="col-xs-3">
												<label>Select Firm Type</label> <label class="text-red">*
												</label> <select class="form-control" name="agentfirmType"
													id="agentfirmType">
													<option value="Private">Private</option>
													<option value="Public">Public</option>
													<option value="Proprietary">Proprietary</option>
													<option value="Other">Other</option>
												</select> <span id="agentfirmTypeSpan" style="color: #FF0000"></span>
											</div>

											<div class="col-xs-3">
												<label for="agentpanNumber">Firm PAN No</label> <input
													type="text" class="form-control" id="firmpanNumber"
													placeholder="Agent PAN No" name="firmpanNumber"
													style="text-transform: uppercase"
													onchange="getpannounique(this.value)"> <span
													id="firmpanNumberSpan" style="color: #FF0000"></span>
											</div>

											<div class="col-xs-3">
												<label for="agentgstNumber">Firm GST No</label> <input
													type="text" class="form-control" id="firmgstNumber"
													placeholder="Agent GST No" name="firmgstNumber"
													style="text-transform: uppercase"
													onchange="getgstnounique(this.value)"> <span
													id="firmgstNumberSpan" style="color: #FF0000"></span>
											</div>
										</div>
									</div>

									<div class="box-body">
										<div class="row">
											<div class="col-xs-2">
												<label for="brokeragePercentage">Brokerage Per(%)</label> <label
													class="text-red">* </label> <input type="text"
													class="form-control" id="brokeragePercentage"
													placeholder="Brokerage Percentage"
													name="brokeragePercentage"> <span
													id="brokeragePercentageSpan" style="color: #FF0000"></span>
											</div>
											<div class="col-xs-2">
												<label for="commissionBased">Commission Based</label> <label
													class="text-red">* </label> <select class="form-control"
													id="commissionBased" name="commissionBased">
													<option value="onFlatBasicAmount">On Flat Basic Amount</option>
													<option value="onAgreementValue">On Agreement Amount</option>
													<option value="onTotalValue">On Total Value</option>
												</select> <span id="commissionBasedSpan" style="color: #FF0000"></span>
											</div>
											<div class="col-xs-2">
												<label>Agent RERA Number</label> <input type="text"
													class="form-control" id="agentreraNumber"
													placeholder="Rera No:" name="agentreraNumber"
													style="text-transform: uppercase"> <span
													id="reraNumberSpan" style="color: #FF0000"></span>
											</div>
											<div class="col-xs-3">
												<label for="checkPrintingName">Check Printing Name</label> <input
													type="text" class="form-control" id="checkPrintingName"
													placeholder="Check Holder Name" name="checkPrintingName"
													style="text-transform: uppercase"> <span
													id="checkPrintingNameSpan" style="color: #FF0000"></span>
											</div>
											<div class="col-xs-3">
												<label for="agentpaymentTerms">Agent Payment Terms</label> <label
													class="text-red">* </label> <select class="form-control"
													id="agentpaymentTerms" name="agentpaymentTerms">
													<option selected="selected">1</option>
													<option>2</option>
													<option>3</option>
													<option>4</option>
													<option>5</option>
													<option>6</option>
													<option>7</option>
													<option>8</option>
													<option>9</option>
													<option>10</option>
												</select> <span id="agentpaymentTermsSpan" style="color: #FF0000"></span>
											</div>

										</div>
									</div>

									<div class="box-body">
										<div class="row">

											<div class="col-xs-2">
												<label for="agentAddress">Firm Address </label> <label
													class="text-red">* </label>
												<textarea class="form-control" rows="1"
													id="agentfirmAddress" placeholder="Address"
													name="agentfirmAddress"></textarea>
												<span id="agentfirmAddressSpan" style="color: #FF0000"></span>
											</div>

										
											<div class="col-xs-2">
												<label for="agentPincode">Pin Code</label> <label
													class="text-red">* </label> <input type="text"
													class="form-control" id="agentPincode"
													placeholder="Pin Code" name="agentPincode"
													readonly="readonly"> <span id="agentPincodeSpan"
													style="color: #FF0000"></span>
											</div>
										</div>
									</div>



									<div class="box-body">
										<!-- Firm Pan, GST, Bank Name, Branch -->
										<div class="row">

											<div class="col-xs-3">
												<label>Firm Bank Name</label> <label class="text-red">*
												</label> <input type="text" class="form-control" id="bankName"
													placeholder="bank Name" name="bankName"> <span
													id="firmbankNameSpan" style="color: #FF0000"></span>
											</div>

											<div class="col-xs-3">
												<label for="agentbranchName">Firm Bank Branch </label> <label
													class="text-red">* </label> <input type="text"
													class="form-control" id="branchName"
													placeholder="branch Name" name="branchName"> <span
													id="firmbankBranchSpan" style="color: #FF0000"></span>
											</div>

											<div class="col-xs-3">
												<label for="agentbankifscCode">Firm Bank Branch IFSC</label>
												<label class="text-red">* </label> <input type="text"
													class="form-control" id="bankifscCode" placeholder="IFSC "
													name="bankifscCode"> <span id="bankifscCodeSpan"
													style="color: #FF0000"></span>
											</div>

											<div class="col-xs-3">
												<label for="agentbankacNumber">Firm Bank A/C No</label> <label
													class="text-red">* </label> <input type="text"
													class="form-control" id="firmbankacNumber"
													placeholder="Bank A/C No " name="firmbankacNumber">
												<span id="agentbankacNumberSpan" style="color: #FF0000"></span>
											</div>

										</div>
									</div>

									<div class="box-body">
										<!-- Firm -->
										<div class="row"></div>
									</div>

								</div>
							</div>
						</div>
						<!-- Col-12 end -->

						<div class="panel box box-danger"></div>

						<div class="box-body">
							<div class="row">

								<div class="col-md-12">
									<div class="box-body">
										<h4>Agent Employee Details</h4>
										<div class="row">
											<div class="col-xs-3">
												<label for="">Employee Name</label> <input type="text"
													class="form-control" id="employeeName"
													placeholder="Enter Employee Name" name="employeeName">
												<span id="employeeNameSpan" style="color: #FF0000"></span>
											</div>
											<div class="col-xs-3">
												<label for="">Designation</label> <select
													class="form-control" id="employeeDesignation"
													name="employeeDesignation">
													<option selected="selected" value="Default">-Select
														Designation-</option>
													<c:forEach var="designationList" items="${designationList}">
														<option value="${designationList.designationName}">${designationList.designationName}</option>
													</c:forEach>
												</select> <span id="employeeDesignationSpan" style="color: #FF0000"></span>
											</div>


											<div class="col-xs-3">

												<label for="emailid">Email ID </label>
												<div class="input-group">
													<span class="input-group-addon"><i
														class="fa fa-envelope"></i></span> <input type="text"
														class="form-control" placeholder="Email"
														id="employeeEmail" name="employeeEmail">
												</div>
												<span id="employeeEmailSpan" style="color: #FF0000"></span>
											</div>

											<div class="col-xs-3">
												<label>Mobile No </label>
												<div class="input-group">
													<div class="input-group-addon">
														<i class="fa fa-phone"></i>
													</div>
													<input type="text" class="form-control"
														data-inputmask='"mask": "9999999999"' data-mask
														name="employeeMobileno" id="employeeMobileno"> <span
														class="input-group-btn">
														<button type="button" class="btn btn-success"
															onclick="return AddAgentEmployee()">
															<i class="fa fa-plus"></i>Add
														</button>
													</span>
												</div>
												<span id="employeeMobilenoSpan" style="color: #FF0000"></span>
											</div>

										</div>
									</div>

									<div class="box-body">
										<div class="row">
											<div class="col-xs-12">
												<table class="table table-bordered"
													id="agentEmployeeListTable">
													<tr bgcolor=#4682B4>
														<th>Name</th>
														<th>Designation</th>
														<th>E-Mail Id</th>
														<th>Mobile No.</th>
														<th>Action</th>
													</tr>

													<%-- <c:forEach items="${bankEmployeesList}" var="bankEmployeesList" varStatus="loopStatus">
                    <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                        <td>${bankEmployeesList.employeeName}</td>
                        <td>${bankEmployeesList.employeeDesignation}</td>
                        <td>${bankEmployeesList.employeeEmail}</td>
                        <td>${bankEmployeesList.employeeMobileno}</td>
                        <td>
                        	<a href="${pageContext.request.contextPath}/EditBankEmployee?bankId=${bankEmployeesList.bankId}" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>
                           |<a onclick="DeleteBankEmployee()" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a>
                        </td>
                     </tr>
                    </c:forEach> --%>
												</table>
											</div>
										</div>
									</div>

									<input type="hidden" id="bankStatus" name="bankStatus"
										value="${bankStatus}"> <input type="hidden"
										id="creationDate" name="creationDate" value=""> <input
										type="hidden" id="updateDate" name="updateDate" value="">
									<input type="hidden" id="userName" name="userName"
										value="<%=session.getAttribute("user")%>">

								</div>

								<br /> <br /> <br />

								<div class="col-md-12">
									<div class="box-body">
										<div class="row">
											<div class="col-xs-4">
												<div class="col-xs-2">
													<a href="AgentMaster"><button type="button"
															class="btn btn-block btn-primary" value="reset"
															style="width: 90px">Back</button></a>
												</div>
											</div>

											<div class="col-xs-2">
												<button type="reset" class="btn btn-default" value="reset"
													style="width: 90px">Reset</button>
											</div>

											<div class="col-xs-3">
												<button type="submit" class="btn btn-info pull-right"
													name="submit">Submit</button>
											</div>

											<!-- 
			     
			       <div class="col-xs-6">
			         <button type="button" class="btn btn-info pull-right" style="background:#48D1CC"><a href="ImportNewBank"> Import From Excel File</a></button>
              
                  </div>
                  
			        -->
										</div>
									</div>
								</div>

								<!-- /.col -->
							</div>
						</div>
					</div>
				</section>
			</form>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<%@ include file="footer.jsp"%>
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

	<!-- jQuery 3 -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap 3.3.7 -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- Select2 -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
	<!-- InputMask -->
	<script
		src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
	<!-- date-range-picker -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
	<!-- bootstrap datepicker -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
	<!-- bootstrap color picker -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
	<!-- bootstrap time picker -->
	<script
		src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
	<!-- SlimScroll -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<!-- iCheck 1.0.1 -->
	<script
		src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
	<!-- FastClick -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script
		src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script
		src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
	<!-- Page script -->
	<script>

function clearall()
{

	$('#agentfirmNameSpan').html('');
	$('#agentfirmTypeSpan').html('');
	$('#brokeragePercentageSpan').html('');
	$('#firmpanNumberSpan').html('');
	$('#firmgstNumberSpan').html('');
	$('#agentfirmAddressSpan').html('');
	$('#countryIdSpan').html('');
	$('#countryIdSpan').html('');
	$('#stateIdSpan').html('');
	$('#cityIdSpan').html('');
	$('#locationareaIdSpan').html('');
	$('#agentPincodeSpan').html('');
	$('#firmbankNameSpan').html('');
	$('#firmbankBranchSpan').html('');
	$('#bankifscCodeSpan').html('');
	$('#agentbankacNumberSpan').html('');
	$('#agentpaymentTermsSpan').html('');
	
	$('#employeeNameSpan').html('');
	$('#employeeDesignationSpan').html();
	$('#employeeEmailSpan').html('');
	$('#employeeMobilenoSpan').html('');
	
	$('#statusSpan').html('');
}

function validate()
{
	clearall();

	//validation for Agent firm name
	if(document.agentform.agentfirmName.value=="")
	{
		 $('#agentfirmNameSpan').html('Please, enter agent firm name..!');
		document.agentform.agentfirmName.focus();
		return false;
	}
	else if(document.agentform.agentfirmName.value.match(/^[\s]+$/))
	{
		$('#agentfirmNameSpan').html('Please, enter valid agent firm name..!');
		document.agentform.agentfirmName.value="";
		document.agentform.agentfirmName.focus();
		return false; 	
	}
	
	if(document.agentform.brokeragePercentage.value=="")
	{
		$('#brokeragePercentageSpan').html('Please, enter Brokerage %');
		document.agentform.brokeragePercentage.focus();
		return false;
	}
	else if(document.agentform.brokeragePercentage.value.match(/^[\s]+$/))
	{
		$('#brokeragePercentageSpan').html('Please, enter Brokerage %..!');
		document.agentform.brokeragePercentage.value="";
		document.agentform.brokeragePercentage.focus();
		return false; 	
	}
	else if(!document.agentform.brokeragePercentage.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
	{
		 if(!document.agentform.brokeragePercentage.value.match(/^[0-9]+$/))
			 {
				$('#brokeragePercentageSpan').html('Please, use only digit value for Brokerage %..! eg:21.36 OR 30');
				document.agentform.brokeragePercentage.value="";
				document.agentform.brokeragePercentage.focus();
				return false;
			}
	}
	
/*	
	//validation for PAN number
	if(document.agentform.firmpanNumber.value=="")
	{
		$('#firmpanNumberSpan').html('Please, enter Pancard number..!');
		document.agentform.firmpanNumber.focus();
		return false;
	}
	else if(!document.agentform.firmpanNumber.value.match(/^[A-Za-z]{5}[0-9]{4}[A-z]{1}$/))
	{
		$('#firmpanNumberSpan').html('PAN number must start with 5 alphabets follwed by 4 digit number and 1 alphabet..!');
		//document.employeeform.firmpanNumber.value="";
		document.agentform.firmpanNumber.focus();
		return false;
	}
	*/
	//validation for GST number
	/* if(document.agentform.firmgstNumber.value=="")
	{
		$('#firmgstNumberSpan').html('Please, Enter GST number..!');
		document.agentform.firmgstNumber.focus();
		return false;
	} */
	/*
	else if(!document.agentform.firmgstNumber.value.match(/^[0-9]{2}[A-Za-z]{5}[0-9]{4}[A-z]{1}[0-9]{1}[Zz]{1}[0-9]{1}$/))
	{
		$('#firmgstNumberSpan').html('GST number must match format like(22AAAAA0000A1Z5)..!');
		//document.agentform.firmgstNumber.value="";
		document.agentform.firmgstNumber.focus();
		return false;
	}
	*/
	//validation for agent firm address--------------------------------
	if(document.agentform.agentfirmAddress.value=="")
	{
		 $('#agentfirmAddressSpan').html('Please, enter agent firm address..!');
		document.agentform.agentfirmAddress.focus();
		return false;
	}
	else if(document.agentform.agentfirmAddress.value.match(/^[\s]+$/))
	{
		$('#agentfirmAddressSpan').html('Please, enter valid agent firm address..!');
		document.agentform.agentfirmAddress.value="";
		document.agentform.agentfirmAddress.focus();
		return false; 	
	}
	/* else if(!document.agentform.agentfirmAddress.value.match(/^[a-zA-Z\s-,.]+$/))
	{
		$('#agentfirmAddressSpan').html('Please, enter valid agent firm address..!');
 		document.agentform.agentfirmAddress.focus();
		return false;
	} */
	
	//validation for countryId
	if(document.agentform.countryId.value=="Default")
	{
		$('#countryIdSpan').html('Please, select country name..!');
		document.agentform.countryId.focus();
		return false;
	}
	
	//validation for stateId
    if(document.agentform.stateId.value=="Default")
	{
		$('#stateIdSpan').html('Please, select state name..!');
		document.agentform.stateId.focus();
		return false;
	}
	
	//validation for cityId
	if(document.agentform.cityId.value=="Default")
	{
		$('#cityIdSpan').html('Please, select city name..!');
		document.agentform.cityId.focus();
		return false;
	}
	
	//validation for location area name
	if(document.agentform.locationareaId.value=="Default")
	{
		$('#locationareaIdSpan').html('Please, select area name..!');
		document.agentform.locationareaId.focus();
		return false;
	}
	

	
	//validation for bankName
	if(document.agentform.bankName.value=="Default")
	{
		$('#firmbankNameSpan').html('Please, select bank name..!');
		document.agentform.bankName.focus();
		return false;
	}
	
	//validation for branchName
	if(document.agentform.branchName.value=="Default")
	{
		$('#firmbankBranchSpan').html('Please, select bank branch name..!');
		document.agentform.branchName.focus();
		return false;
	}
	
	//validation for bank account number
	if(document.agentform.firmbankacNumber.value.length==0)
	{
		$('#agentbankacNumberSpan').html('Please, enter Bank a/c number..!');
		//document.agentform.employeeBankacno.value="";
		document.agentform.firmbankacNumber.focus();
		return false;
	}
	else if(document.agentform.firmbankacNumber.value.length!=0)
	{
		if(!document.agentform.firmbankacNumber.value.match(/^[0-9]{6,18}$/))
		{
			$('#agentbankacNumberSpan').html('Please, enter valid Bank a/c number..!');
			//document.agentform.employeeBankacno.value="";
			document.agentform.firmbankacNumber.focus();
			return false;
		}
	}
	
}

function init()
{
	clearall();
	
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	
	 if(document.agentform.status.value=="Fail")
	 {
		// $('#statusSpan').html('Sorry, record is present already..!');
	 }
	 else if(document.agentform.status.value=="Success")
	 {
		 $('#statusSpan').html('Record added successfully..!');
	 }
  
	 document.agentform.agentfirmName.focus();
}


function getpinCode()
{

	 $("#agentPincode").empty();
	 var locationareaId = $('#locationareaId').val();
	 var cityId = $('#cityId').val();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();
	 $.ajax({

		url : '${pageContext.request.contextPath}/getallAreaList',
		type : 'Post',
		data : { locationareaId : locationareaId, cityId : cityId, stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							
							for(var i=0;i<result.length;i++)
							{
							$('#agentPincode').val(result[i].pinCode);
								
							} 
						
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}

function getStateList()
{
	 $("#stateId").empty();
	 var countryId = $('#countryId').val();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getStateList',
		type : 'Post',
		data : { countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select State-");
							$("#stateId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].stateId).text(result[i].stateName);
							    $("#stateId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get State List


 function getCityList()
{
	 $("#cityId").empty();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();
	$.ajax({

		url : '${pageContext.request.contextPath}/getCityList',
		type : 'Post',
		data : { stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select City-");
							$("#cityId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].cityId).text(result[i].cityName);
							    $("#cityId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
}//end of get City List



function getLocationAreaList()
{
	 $("#locationareaId").empty();
	 var cityId = $('#cityId').val();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();

	 $.ajax({

		url : '${pageContext.request.contextPath}/getLocationAreaList',
		type : 'Post',
		data : { cityId : cityId, stateId : stateId, countryId:countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Location Area-");
							$("#locationareaId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].locationareaId).text(result[i].locationareaName);
							    $("#locationareaId").append(option);
							} 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
}//end of get Location Area List


function getBranchList()
{
	 $("#branchName").empty();
	 var bankName = $('#bankName').val();
	
	 $.ajax({

		url : '${pageContext.request.contextPath}/getBranchList',
		type : 'Post',
		data : { bankName : bankName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Branch Name-");
							$("#branchName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].branchName).text(result[i].branchName);
							    $("#branchName").append(option);
							} 
						} 
						else
						{
							alert("failure111");
						}

					}
		});	
}

function getBranchIfsc()
{
	 //$("#bankifscCode").val('');
	 var bankName = $('#bankName').val();
     var branchName = $('#branchName').val();
	 
	 $.ajax({

		url : '${pageContext.request.contextPath}/getBranchIfsc',
		type : 'Post',
		data : { bankName : bankName, branchName : branchName },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							$('#bankifscCode').val(result[0].bankifscCode);
						} 
						else
						{
							alert("failure111");
						}

					}
		});
}


function AddAgentEmployee()
{

	clearall();
	
	//validation for employee name
	if(document.agentform.employeeName.value=="")
	{
		$('#employeeNameSpan').html('Employee name should not be empty..!');
		document.agentform.employeeName.value="";
		document.agentform.employeeName.focus();
		return false;
	}
	else if(document.agentform.employeeName.value.match(/^[\s]+$/))
	{
		$('#employeeNameSpan').html('Employee name must contains alphabets only..!');
		document.agentform.employeeName.value="";
		document.agentform.employeeName.focus();
		return false;
	}
	else if(!document.agentform.employeeName.value.match(/^[a-zA-Z\s]+$/))
	{
		$('#employeeNameSpan').html('Employee name must contains alphabets only..!');
		document.agentform.employeeName.value="";
		document.agentform.employeeName.focus();
		return false;
	}
	
	//validation for employee designation
	if(document.agentform.employeeDesignation.value=="Default")
	{
		$('#employeeDesignationSpan').html('Please, select designation..!');
		document.agentform.employeeDesignation.focus();
		return false;
	}
	
	//validation for employee email id
	if(document.agentform.employeeEmail.value=="")
	{
		$('#employeeEmailSpan').html('Please, enter email id..!');
		document.agentform.employeeEmail.focus();
		return false;
	}
	else if(!document.agentform.employeeEmail.value.match(/^([a-z0-9_\.\-])+\@(([a-z0-9\-])+\.)+([a-z0-9]{2,4})+$/))
	{
		$('#employeeEmailSpan').html('Please, enter valid email id..!');
		document.agentform.employeeEmail.value="";
		document.agentform.employeeEmail.focus();
		return false;
	}
	
	//validation for employee mobile number
	if(document.agentform.employeeMobileno.value=="")
	{
		$('#employeeMobilenoSpan').html('Please, enter mobile number..!');
		document.agentform.employeeMobileno.focus();
		return false;
	}
	else if(!document.agentform.employeeMobileno.value.match(/^[0-9]{10}$/))
	{
		$('#employeeMobilenoSpan').html('mobile number must be 10 digit numbers only with correct format..!');
		document.agentform.employeeMobileno.value="";
		document.agentform.employeeMobileno.focus();
		return false;
	}
	
	 
	$('#agentEmployeeListTable tr').detach();
	 
	 var agentId = $('#agentId').val();
	 var employeeName = $('#employeeName').val();
	 var employeeDesignation = $('#employeeDesignation').val();
	 var employeeEmail = $('#employeeEmail').val();
	 var employeeMobileno = $('#employeeMobileno').val();
	 
	 $.ajax({

		 url : '${pageContext.request.contextPath}/AddAgentEmployee',
		type : 'Post',
		data : { agentId : agentId, employeeName : employeeName, employeeDesignation : employeeDesignation, employeeEmail : employeeEmail, employeeMobileno : employeeMobileno},
		dataType : 'json',
		success : function(result)
				  {
					   if (result) 
					   { 
							$('#agentEmployeeListTable').append('<tr style="background-color: #4682B4;"><th style="width:300px">Name</th><th style="width:150px">Designation</th><th style="width:150px">Email Id</th><th style="width:150px">Mobile No.</th><th style="width:100px">Action</th>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].agentEmployeeId;
									$('#agentEmployeeListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].employeeName+'</td><td>'+result[i].employeeDesignation+'</td><td>'+result[i].employeeEmail+'</td><td>'+result[i].employeeMobileno+'</td><td><a onclick="DeleteAgentEmployee('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
								else
								{
									var id = result[i].agentEmployeeId;
									$('#agentEmployeeListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].employeeName+'</td><td>'+result[i].employeeDesignation+'</td><td>'+result[i].employeeEmail+'</td><td>'+result[i].employeeMobileno+'</td><td><a onclick="DeleteAgentEmployee('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}
				  } 

		});
	 
	 $('#employeeName').val("");
	 $('#employeeEmail').val("");
	 $('#employeeMobileno').val("");
}

function DeleteAgentEmployee(employeeId)
{
	var agentId = $('#agentId').val();
    var agentEmployeeId = employeeId;
    
    $('#agentEmployeeListTable tr').detach();
    
    $.ajax({

		 url : '${pageContext.request.contextPath}/DeleteAgentEmployee',
		type : 'Post',
		data : { agentId : agentId, agentEmployeeId : agentEmployeeId },
		dataType : 'json',
		success : function(result)
				  {
					   if (result) 
					   { 
									$('#agentEmployeeListTable').append('<tr style="background-color: #4682B4;"><th style="width:300px">Name</th><th style="width:150px">Designation</th><th style="width:150px">Email Id</th><th style="width:150px">Mobile No.</th><th style="width:100px">Action</th>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].agentEmployeeId;
									$('#agentEmployeeListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].employeeName+'</td><td>'+result[i].employeeDesignation+'</td><td>'+result[i].employeeEmail+'</td><td>'+result[i].employeeMobileno+'</td><td><a onclick="DeleteAgentEmployee('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
								else
								{
									var id = result[i].agentEmployeeId;
									$('#agentEmployeeListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].employeeName+'</td><td>'+result[i].employeeDesignation+'</td><td>'+result[i].employeeEmail+'</td><td>'+result[i].employeeMobileno+'</td><td><a onclick="DeleteAgentEmployee('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}
				  } 

		});

}
	
//to check Unique GST number
/*
function getgstnounique()
{
		 var agentgstNumber = $('#agentgstNumber').val();
		
		$.ajax({

			url : '${pageContext.request.contextPath}/getagentaadharnumberList',
			type : 'Post',
			data : { agentgstNumber : agentgstNumber},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{
								 
								for(var i=0;i<result.length;i++)
								{
										
									if(result[i].agentgstNumber==agentgstNumber)
										{
										  document.agentform.agentgstNumber.value="";
										  document.agentform.agentgstNumber.focus();
										  $('#agentgstNumberSpan').html('This GST number is already exist..!');
										}
									else{
									}
									
								 } 
							} 
							else
							{
								alert("failure111");
								//$("#ajax_div").hide();
							}

						}
			});
		
	}
	
 */
	
$(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>

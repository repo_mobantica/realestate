<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Store Management</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">
  <!-- Font Awesome -->
  <!-- Ionicons -->
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <style type="text/css">
   tr.odd {background-color:#F0F8FF}
tr.even {background-color: #CCE5FF}
    </style>
    
   
    
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
 
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

   <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
    <section class="content-header">
      <h1>
        Store Material Purchase List:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Store</a></li>
        <li class="active">Store Material Purchase List</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="storeManagementform" action="${pageContext.request.contextPath}/StoreManagement" method="post">
   
      <section class="content">
      <div class="box box-default">
      <div class="box-body">
							<div class="row">
								<div class="col-md-12">

									<div class="box-body">
										<div class="row">
											<div class="col-md-3">
												<label>Vender</label> <select class="form-control"
													id="supplierId" name="supplierId"
													onchange="getStoreMaterialList(this.value)">
													<option selected="selected" value="Default">Select
														Supplier-</option>

													<s:forEach var="supplierList" items="${supplierList}">
														<option value="${supplierList.supplierId}">${supplierList.supplierfirmName}</option>
													</s:forEach>

												</select>
											</div>
								
										</div>
									</div>

								</div>

							</div>
					
						</div>
      <div class="box-body">
      <div class="row">
        <div class="col-xs-12">
       
        </br> </br>
        
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#StoreMaterialPurchaseList" data-toggle="tab">Store Material Purchase List</a></li>
              <li><a href="#InCompletedMaterial" data-toggle="tab">InCompleted Material </a></li>
            </ul>
            
            
            
            <div class="tab-content">
              <div class="tab-pane active" id="StoreMaterialPurchaseList">
                <section >
              <h4 class="page-header">Store Material Purchase List</h4>
              <div  class="panel box box-danger">
              </br>
               <div class="table-responsive" >
              
              <table id="purchesedListTable" class="table table-bordered">

                  <thead>
	                  <tr bgcolor=#4682B4>
	                    <td><b>Sr.No</b></td>
	                    <td><b>PO No.</b></td>
	                    <td><b>Vender Name</b></td>
	                    <td><b>Employee Name</b></td>
	                    <td><b>Requisition Id</b></td>
	                    <td><b>Action</b></td>
	                  </tr>
                  </thead>
                  <tbody>
                 <s:forEach items="${purchesedList}" var="purchesedList" varStatus="loopStatus">
                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                      		<td>${loopStatus.index+1}</td>
	                        <td>${purchesedList.materialsPurchasedId}</td>
	                        <td>${purchesedList.supplierfirmName}</td>
	                        <td>${purchesedList.employeeId}</td>
	                        <td>${purchesedList.requisitionId}</td>
	                        <td>
	                           <a href="${pageContext.request.contextPath}/MaterialPurchesedByStore?materialsPurchasedId=${purchesedList.materialsPurchasedId}" class="btn btn-info btn-sm" data-toggle="tooltip" title="Check Material"><i class="glyphicon glyphicon-edit"></i></a>
	                          |<a href="${pageContext.request.contextPath}/PrintMaterialPurchesedByStore?materialsPurchasedId=${purchesedList.materialsPurchasedId}" class="btn btn-success btn-sm" target="_blank" data-toggle="tooltip" title="Print Store Material List"><i class="glyphicon glyphicon-print"></i></a>
	                      
	                        </td>
                     
                      </tr>
					</s:forEach>
                  
                 </tbody>
                </table>
                
              </div> 
            </div> 
           </section>
		</div>
		    
                      
       <div class="tab-pane" id="InCompletedMaterial">
        <section >
          <h4 class="page-header">InCompleted Material </h4>
          <div  class="panel box box-danger">
          </br>
          <div class="table-responsive" >
          
              <table id="InCompletedpurchesedListTable" class="table table-bordered">

                  <thead>
	                  <tr bgcolor=#4682B4>
	                    <td><b>Sr.No</b></td>
	                    <td><b>PO No.</b></td>
	                    <td><b>Vender Name</b></td>
	                    <td><b>Employee Name</b></td>
	                    <td><b>Requisition Id</b></td>
	                    <td><b>Action</b></td>
	                  </tr>
                  </thead>
                  <tbody>
                 <s:forEach items="${IncompletedpurchesedList}" var="IncompletedpurchesedList" varStatus="loopStatus">
                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                      		<td>${loopStatus.index+1}</td>
	                        <td>${IncompletedpurchesedList.materialsPurchasedId}</td>
	                        <td>${IncompletedpurchesedList.supplierfirmName}</td>
	                        <td>${IncompletedpurchesedList.employeeId}</td>
	                        <td>${IncompletedpurchesedList.requisitionId}</td>
	                        <td>
	                           <a href="${pageContext.request.contextPath}/PendingMaterialPurchesedByStore?materialsPurchasedId=${IncompletedpurchesedList.materialsPurchasedId}" class="btn btn-info btn-sm" data-toggle="tooltip" title="Check Material"><i class="glyphicon glyphicon-edit"></i></a>
	                    <%--       |<a href="${pageContext.request.contextPath}/PrintMaterialPurchesedByStore?materialsPurchasedId=${IncompletedpurchesedList.materialsPurchasedId}" class="btn btn-success btn-sm" target="_blank" data-toggle="tooltip" title="Print Store Material List"><i class="glyphicon glyphicon-print"></i></a>
	                     --%>
	                        </td>
                     
                      </tr>
					</s:forEach>
                  
                 </tbody>
                </table>
                
                
                
    			 </div>
                 </div> 
                </section>
			 </div>
 
	</div>
   </div>
 </div>
  </div>
  </div>
  </div>
  </section>
</form>
 </div>
  <%@ include file="footer.jsp" %>
  
<div class="control-sidebar-bg"></div>
</div>
<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>

<script>



function getStoreMaterialList()
{
	
	 //to retrive all banks by Location Area name
	$("#purchesedListTable tr").detach();
	
	 var supplierId = $('#supplierId').val();

	 $.ajax({

		url : '${pageContext.request.contextPath}/getStoreAllMaterialList',
		type : 'Post',
		data : { supplierId : supplierId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
							$('#purchesedListTable').append('<tr style="background-color: #4682B4;"> <td><b>Sr.No</b></td>  <td><b>PO No.</b></td><td><b>Vender Name</b></td><td><b>Employee Name</b></td> <td><b>Requisition Id</b></td> <td><b>Action</b></td>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									 var id = result[i].materialsPurchasedId;
									 $('#purchesedListTable').append('<tr style="background-color: #F0F8FF;"><td>'+(i+1)+'</td><td>'+result[i].materialsPurchasedId+'</td><td>'+result[i].supplierfirmName+'</td><td>'+result[i].employeeId+'</td><td>'+result[i].requisitionId+'</td><td><a href="${pageContext.request.contextPath}/MaterialPurchesedByStore?materialsPurchasedId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Check Material"><i class="glyphicon glyphicon-edit"></i></a><a href="${pageContext.request.contextPath}/PrintMaterialPurchesedByStore?materialsPurchasedId='+id+'" class="btn success btn-sm"  target="_blank" data-toggle="tooltip" title="Print Store Material List"><i class="glyphicon glyphicon-print"></i></a></td>');
								}
								else
								{
									var id = result[i].materialsPurchasedId;
									 $('#purchesedListTable').append('<tr style="background-color: #F0F8FF;"><td>'+(i+1)+'</td><td>'+result[i].materialsPurchasedId+'</td><td>'+result[i].supplierfirmName+'</td><td>'+result[i].employeeId+'</td><td>'+result[i].requisitionId+'</td><td><a href="${pageContext.request.contextPath}/MaterialPurchesedByStore?materialsPurchasedId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Check Material"><i class="glyphicon glyphicon-edit"></i></a><a href="${pageContext.request.contextPath}/PrintMaterialPurchesedByStore?materialsPurchasedId='+id+'" class="btn success btn-sm"  target="_blank" data-toggle="tooltip" title="Print Store Material List"><i class="glyphicon glyphicon-print"></i></a></td>');
								}
							 } 
						} 
						else
						{
							alert("failure111");
						}
					}
		});
	

		$("#InCompletedpurchesedListTable tr").detach();
	 $.ajax({

		url : '${pageContext.request.contextPath}/getStoreInCompletedMaterialList',
		type : 'Post',
		data : { supplierId : supplierId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
							$('#InCompletedpurchesedListTable').append('<tr style="background-color: #4682B4;"> <td><b>Sr.No</b></td>  <td><b>PO No.</b></td><td><b>Vender Name</b></td><td><b>Employee Name</b></td> <td><b>Requisition Id</b></td> <td><b>Action</b></td>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									 var id = result[i].materialsPurchasedId;
									 $('#InCompletedpurchesedListTable').append('<tr style="background-color: #F0F8FF;"><td>'+(i+1)+'</td><td>'+result[i].materialsPurchasedId+'</td><td>'+result[i].supplierfirmName+'</td><td>'+result[i].employeeId+'</td><td>'+result[i].requisitionId+'</td><td><a href="${pageContext.request.contextPath}/PendingMaterialPurchesedByStore?materialsPurchasedId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Check Material"><i class="glyphicon glyphicon-edit"></i></td>');
								}
								else
								{
									var id = result[i].materialsPurchasedId;
									 $('#InCompletedpurchesedListTable').append('<tr style="background-color: #F0F8FF;"><td>'+(i+1)+'</td><td>'+result[i].materialsPurchasedId+'</td><td>'+result[i].supplierfirmName+'</td><td>'+result[i].employeeId+'</td><td>'+result[i].requisitionId+'</td><td><a href="${pageContext.request.contextPath}/PendingMaterialPurchesedByStore?materialsPurchasedId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Check Material"><i class="glyphicon glyphicon-edit"></i></td>');
								}
							 } 
						} 
						else
						{
							alert("failure111");
						}
					}
		});
}

</script>
<script>


$(function () {
    $('#purchesedListTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
  
  
$(function () {
    $('#InCompletedpurchesedListTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>

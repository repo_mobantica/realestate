<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Edit Sub Chart of Accounts</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
    <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>

<div class="wrapper">

  <%@ include file="headerpage.jsp" %>
  
  <%@ include file="menu.jsp" %>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Sub-Chart of Accounts:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Edit Chart of Accounts</li>
      </ol>
    </section>

<form name="subchartofaccountform" action="${pageContext.request.contextPath}/EditSubChartofAccount"  onSubmit="return validate()"  method="post">
    <!-- Main content -->
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              
              <!-- /.form-group -->
              <div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
			      <label> Sub Account Id</label>
                  <input type="text" class="form-control" id="subchartaccountId" name="subchartaccountId" value="${subchartofaccountDetails[0].subchartaccountId}" readonly>
               </div> 
			     
				</div>
            </div>
              
               <div class="box-body">
              <div class="row">
                  <div class="col-xs-12">
			      <label>Chart of Accounts Name</label> <label class="text-red">* </label>
                  <select class="form-control" id="chartaccountId" name="chartaccountId" onchange="getChartAccountNumber(this.value)">
                  
				  		<option value="Default">-Select a Chart of Accounts-</option>
				  		<option selected="selected" value="${subchartofaccountDetails[0].chartaccountId}">${chartaccountName}</option>
                     <c:forEach var="chartofaccountList" items="${chartofaccountList}" >
                      <c:choose>
                       <c:when test="${subchartofaccountDetails[0].chartaccountId ne chartofaccountList.chartaccountId}">
						<option value="${chartofaccountList.chartaccountId}">${chartofaccountList.chartaccountName}</option>
					   </c:when>
					  </c:choose>
					 </c:forEach>
					 
                  </select>
                  </div> 
			     
				</div>
            </div> 
              
             
              
              <div class="box-body">
               <div class="row">
                   
			     <div class="col-xs-6">
			     
			      <label for="chartaccountNumber"> Account Number</label>
                  <input type="text" class="form-control" id="chartaccountNumber" placeholder="Chart Account Number" name="chartaccountNumber" value="${subchartofaccountDetails[0].chartaccountNumber}">
                
                </div> 
               
                 <div class="col-xs-6">
                 
			      <label for="subchartaccountNumber"> Sub-Account Number</label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="subchartaccountNumber" placeholder="Sub Chart Account Number" name="subchartaccountNumber" value="${subchartofaccountDetails[0].subchartaccountNumber}">
                 
                 </div>
                 
			  </div>
            </div> 
              
              
              
              
              
                <div class="box-body">
              <div class="row">
                  <div class="col-xs-12">
			     <label for="subchartaccountName">Sub Chart of Accounts Name</label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="subchartaccountName" placeholder="Sub Chart of Accounts Name" name="subchartaccountName" style="text-transform: capitalize;" value="${subchartofaccountDetails[0].subchartaccountName}">
                </div> 
			     
				</div>
            </div>
              
              	<input type="hidden" id="Status" name="Status" value="${Status}">	
				<input type="hidden" id="creationDate" name="creationDate" value="${subchartofaccountDetails[0].creationDate}" >
				<input type="hidden" id="updateDate" name="updateDate" >
				<input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>">
				 
             		 
              
              
              
              
              
         
              <!-- /.form-group -->
            </div>
            
            <!-- /.col -->
			
          </div>
			<div class="box-body">
              <div class="row">
                 
				 <div class="col-xs-4">
				 <div class="col-xs-2">
                		<a href="SubChartofAccountMaster"><button type="button" class="btn btn-block btn-primary" value="back" style="width:90px">Back</button></a>
              	</div>
			     </div>
				 <div class="col-xs-2">
			  			<button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
			     </div> 
              </div>
			  </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        
      </div>
      <!-- /.box -->

     
    </section>
	</form>
    <!-- /.content -->
  </div>
  
 <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script>

function validate()
{ 
	if(document.subchartofaccountform.chartaccountId.value=="Default")
	{
		alert("Please, select chart of account name..!");
		document.subchartofaccountform.chartaccountId.focus();
		return false;
	}
	if(document.subchartofaccountform.subchartaccountNumber.value=="")
	{
		alert("Please, enter chart of sub account number..!");
		document.subchartofaccountform.subchartaccountNumber.focus();
		return false;
	}
	else if(document.subchartofaccountform.subchartaccountNumber.value.match(/^[\s]+$/))
	{
		alert("Please, enter chart of sub account number..!");
		document.subchartofaccountform.subchartaccountNumber.value="";
		document.subchartofaccountform.subchartaccountNumber.focus();
		return false; 	
	}
	else if(!document.subchartofaccountform.subchartaccountNumber.value.match(/^[0-9]+$/))
	{
		alert("Please, use only numerics for sub chart of account number..!");
		document.subchartofaccountform.subchartaccountNumber.value="";
		document.subchartofaccountform.subchartaccountNumber.focus();
		return false;
	}
	if(document.subchartofaccountform.subchartaccountName.value=="")
	{
		alert("Please, enter chart of sub account name..!");
		document.subchartofaccountform.subchartaccountName.focus();
		return false;
	}
	else if(document.subchartofaccountform.subchartaccountName.value.match(/^[\s]+$/))
	{
		alert("Please, enter chart of sub account name..!");
		document.subchartofaccountform.subchartaccountName.value="";
		document.subchartofaccountform.subchartaccountName.focus();
		return false; 	
	}
	else if(!document.subchartofaccountform.subchartaccountName.value.match(/^[a-zA-Z\s]+$/))
	{
		alert("Please, use only numerics for sub chart of account name..!");
		document.subchartofaccountform.subchartaccountName.value="";
		document.subchartofaccountform.subchartaccountName.focus();
		return false;
	}
		
}

function init()
{
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	/* document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	 */document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	if(document.subchartofaccountform.Status.value=="Fail")
	{
	  	alert("Sorry, record is present already..!");
	}
	else if(document.subchartofaccountform.Status.value=="Success")
	{
	    alert("Record saved successfully..!");
	}
	 
  	document.subchartofaccountform.chartaccountId.focus();
}


function getChartAccountNumber() 
{
	$("#chartaccountNumber").empty();
	 var chartaccountId = $('#chartaccountId').val();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getChartAccountNumber',
		type : 'Post',
		data : { chartaccountId : chartaccountId },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							for(var i=0;i<result.length;i++)
							{
							    $('#chartaccountNumber').val(result[i].chartaccountNumber);
							} 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
}


</script>
</body>
</html>
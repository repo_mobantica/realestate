<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Real Estate | Add Project Market Rate</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
<!-- daterange picker -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<!-- bootstrap datepicker -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
<!-- Bootstrap Color Picker -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
<!-- Bootstrap time Picker -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
<!-- Select2 -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
<!-- Theme style -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

<!-- Google Font -->
<style type="text/css">
tr.odd {
	background-color: #CCE5FF
}

tr.even {
	background-color: #F0F8FF
}
</style>


<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<div class="wrapper">

		<%@ include file="headerpage.jsp"%>
		<!-- Left side column. contains the logo and sidebar -->
		<%@ include file="menu.jsp"%>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Add Project Market Rate: <small>Preview</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><a href="#">Master</a></li>
					<li class="active">Add Project Market Rate</li>
				</ol>
			</section>

			<!-- Main content -->

			<form name="projectmarketRate"
				action="${pageContext.request.contextPath}/AddProjectMarketRate"
				method="Post" onSubmit="return validate()">
				<section class="content">

					<!-- SELECT2 EXAMPLE -->
					<div class="box box-default">
						<span id="statusSpan" style="color: #FF0000"></span>
						<!-- /.box-header -->
						<div class="box-body">
							<div class="row">
								<div class="col-md-12">

									<div class="box-body">
										<div class="row">
											<div class="col-xs-3">
												<label for="projectId">Project Id</label> <input type="text"
													class="form-control" id="projectId" name="projectId"
													value="${projectDetails.projectId}" readonly>

											</div>
											<div class="col-xs-3">
												<label for="projectName">Project Name</label> <input
													type="text" class="form-control" id="projectName"
													name="projectName" value="${projectDetails.projectName}"
													readonly>

											</div>
										</div>
									</div>


									<div class="box-body">
										<div class="row">
											<div class="col-xs-3">
												<label for="year">Year</label> <label class="text-red">*
												</label> <input type="text" class="form-control" id="year"
													placeholder="Enter year" name="year"
													style="text-transform: capitalize;"> <span
													id="yearSpan" style="color: #FF0000"></span>
											</div>
											<div class="col-xs-3">
												<label for="subdivision">Subdivision </label> <input
													type="text" class="form-control" id="subdivision"
													placeholder="subdivision " name="subdivision"
													style="text-transform: uppercase"> <span
													id="subdivisionSpan" style="color: #FF0000"></span>
											</div>

											<div class="col-xs-3">
												<label for="openGround">Open Ground </label> <input
													type="text" class="form-control" id="openGround"
													placeholder="openGround" name="openGround"
													style="text-transform: uppercase"> <span
													id="openGroundSpan" style="color: #FF0000"></span>
											</div>


											<div class="col-xs-3">
												<label for="residentialHouse">Residential House </label> <input
													type="text" class="form-control" id="residentialHouse"
													placeholder="residentialHouse " name="residentialHouse"
													style="text-transform: uppercase"> <span
													id="residentialHouseSpan" style="color: #FF0000"></span>
											</div>

										</div>
									</div>
									<div class="box-body">
										<div class="row">
											<div class="col-xs-3">
												<label for="office">Office</label> <input type="text"
													class="form-control" id="office" placeholder="office "
													name="office" style="text-transform: uppercase"> <span
													id="officeSpan" style="color: #FF0000"></span>
											</div>
											<div class="col-xs-3">
												<label for="shops">Shops </label> <input type="text"
													class="form-control" id="shops" placeholder="shops"
													name="shops" style="text-transform: uppercase"> <span
													id="shopsSpan" style="color: #FF0000"></span>
											</div>


											<div class="col-xs-3">
												<label for="industrial">Industrial </label> <input
													type="text" class="form-control" id="industrial"
													placeholder="industrial" name="industrial"
													style="text-transform: uppercase"> <span
													id="industrialSpan" style="color: #FF0000"></span>
											</div>

											<div class="col-xs-3">
												<label for="unit">Unit (Rs -/)</label> <input type="text"
													class="form-control" id="unit" placeholder="unit"
													name="unit" style="text-transform: uppercase"> <span
													id="unitSpan" style="color: #FF0000"></span>
											</div>


										</div>
									</div>


									<br /> <br />
									<div class="box-body">
										<div class="row">
											<div class="col-xs-12">

												<table class="table table-bordered" id="projectmarketRate">
													<thead>
														<tr bgcolor=#4682B4 style="color: white;">
															<td>Year</td>
															<td>Subdivision</td>
															<td>Open Ground</td>
															<td>Residential House</td>
															<td>Office</td>
															<td>Shops</td>
															<td>Industrial</td>
															<td>Unit</td>
														</tr>
													</thead>

													<tbody>

														<c:forEach items="${projectmarketRateList}" var="projectmarketRateList"
															varStatus="loopStatus">
															<tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">

																<td>${projectmarketRateList.year}</td>
																<td>${projectmarketRateList.subdivision}</td>
																<td>${projectmarketRateList.openGround}</td>
																<td>${projectmarketRateList.residentialHouse}</td>
																<td>${projectmarketRateList.office}</td>
																<td>${projectmarketRateList.shops}</td>
																<td>${projectmarketRateList.industrial}</td>
																<td>${projectmarketRateList.unit}</td>
															</tr>
														</c:forEach>
													</tbody>

												</table>
											</div>
										</div>

										<input type="hidden" id="companyStatus" name="companyStatus"
											value="${companyStatus}"> <input type="hidden"
											id="companyFinancialyear" name="companyFinancialyear"
											value=""> <input type="hidden" id="creationDate"
											name="creationDate" value=""> <input type="hidden"
											id="updateDate" name="updateDate" value=""> <input
											type="hidden" id="userName" name="userName"
											value="<%=session.getAttribute("user")%>">


										<div class="box-body">
											<div class="row">
												</br>
												<div class="col-xs-4">
													<div class="col-xs-2">
														<a href="ProjectMaster"><button type="button"
																class="btn btn-block btn-primary" style="width: 90px">Back</button></a>
													</div>
												</div>

												<div class="col-xs-4">
													<button type="reset" class="btn btn-default" value="reset"
														style="width: 90px">Reset</button>

												</div>
												<div class="col-xs-2">
													<button type="submit" class="btn btn-info pull-right"
														name="submit">Submit</button>

												</div>
											</div>
										</div>
									</div>

								</div>
							</div>
						</div>
				</section>
			</form>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<%@ include file="footer.jsp"%>

		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

	<!-- jQuery 3 -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap 3.3.7 -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- Select2 -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
	<!-- InputMask -->
	<script
		src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
	<!-- date-range-picker -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
	<!-- bootstrap datepicker -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
	<!-- bootstrap color picker -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
	<!-- bootstrap time picker -->
	<script
		src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
	<!-- SlimScroll -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<!-- iCheck 1.0.1 -->
	<script
		src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
	<!-- FastClick -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script
		src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script
		src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
	<!-- Page script -->
	<script>
		function validate() {
			clearall();
			//validation for company name
			if (document.projectmarketRate.year.value == "") {
				$('#yearSpan').html('Please, enter year ..!');
				document.projectmarketRate.year.focus();
				return false;
			} else if (document.projectmarketRate.year.value.match(/^[\s]+$/)) {
				$('#yearSpan').html('Please, enter year ..!');
				document.projectmarketRate.year.value = "";
				document.projectmarketRate.year.focus();
				return false;
			}

		}
		function clearall() {
			$('#yearSpan').html('');

		}

		function init() {

			$('#yearSpan').html('');
			document.projectmarketRate.year.focus();
		}

		$(function() {
			//Initialize Select2 Elements
			$('.select2').select2()

			//Datemask dd/mm/yyyy
			$('#datemask').inputmask('dd/mm/yyyy', {
				'placeholder' : 'dd/mm/yyyy'
			})
			//Datemask2 mm/dd/yyyy
			$('#datemask2').inputmask('mm/dd/yyyy', {
				'placeholder' : 'mm/dd/yyyy'
			})
			//Money Euro
			$('[data-mask]').inputmask()

			//Date range picker
			$('#reservation').daterangepicker()
			//Date range picker with time picker
			$('#reservationtime').daterangepicker({
				timePicker : true,
				timePickerIncrement : 30,
				format : 'MM/DD/YYYY h:mm A'
			})
			//Date range as a button
			$('#daterange-btn').daterangepicker(
					{
						ranges : {
							'Today' : [ moment(), moment() ],
							'Yesterday' : [ moment().subtract(1, 'days'),
									moment().subtract(1, 'days') ],
							'Last 7 Days' : [ moment().subtract(6, 'days'),
									moment() ],
							'Last 30 Days' : [ moment().subtract(29, 'days'),
									moment() ],
							'This Month' : [ moment().startOf('month'),
									moment().endOf('month') ],
							'Last Month' : [
									moment().subtract(1, 'month').startOf(
											'month'),
									moment().subtract(1, 'month')
											.endOf('month') ]
						},
						startDate : moment().subtract(29, 'days'),
						endDate : moment()
					},
					function(start, end) {
						$('#daterange-btn span').html(
								start.format('MMMM D, YYYY') + ' - '
										+ end.format('MMMM D, YYYY'))
					})

			//Date picker
			$('#datepicker').datepicker({
				autoclose : true
			})

			//iCheck for checkbox and radio inputs
			$('input[type="checkbox"].minimal, input[type="radio"].minimal')
					.iCheck({
						checkboxClass : 'icheckbox_minimal-blue',
						radioClass : 'iradio_minimal-blue'
					})
			//Red color scheme for iCheck
			$(
					'input[type="checkbox"].minimal-red, input[type="radio"].minimal-red')
					.iCheck({
						checkboxClass : 'icheckbox_minimal-red',
						radioClass : 'iradio_minimal-red'
					})
			//Flat red color scheme for iCheck
			$('input[type="checkbox"].flat-red, input[type="radio"].flat-red')
					.iCheck({
						checkboxClass : 'icheckbox_flat-green',
						radioClass : 'iradio_flat-green'
					})

			//Colorpicker
			$('.my-colorpicker1').colorpicker()
			//color picker with addon
			$('.my-colorpicker2').colorpicker()

			//Timepicker
			$('.timepicker').timepicker({
				showInputs : false
			})
		})
	</script>
</body>
</html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Edit Consultancy</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


  <style type="text/css">
   tr.odd {background-color: #CCE5FF}
   tr.even {background-color: #F0F8FF}
  </style>

<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
    </script>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
    		
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>

<div class="wrapper">

  <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Consultancy Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Edit Consultancy</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="consultancyform" action="${pageContext.request.contextPath}/EditConsultancy" method="post" onSubmit="return validate()" >
    <section class="content">
  <div class="box box-default">
  
      <div class="box-body">
          <div class="row">
            <div class="col-md-12">
	            <span id="statusSpan" style="color:#008000"></span>
					
				<div class="box-body">
	              <div class="row">
	              
	                  <div class="col-xs-2">
	                  <label>Consultancy Id</label>
	                  <input type="text" class="form-control"  id="consultancyId"name="consultancyId" value="${consultancyDetails[0].consultancyId}" readonly>
	                </div>               
	                
	              </div>
	            </div>
				
	            
			<div class="box-body">
              <div class="row">
              
                <div class="col-xs-3">
				<label for="consultancyfirmName">Firm Name</label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="consultancyfirmName" placeholder="Firm Name" name="consultancyfirmName" style="text-transform: capitalize;" value="${consultancyDetails[0].consultancyfirmName}">
                 <span id="consultancyfirmNameSpan" style="color:#FF0000"></span>
                </div>
                  
                  
                        
	                 <div class="col-xs-2">
                  		<label>Select Firm Type</label> <label class="text-red">* </label>
                  		<select class="form-control" name="firmType" id="firmType">
                  		 <c:choose>
                  		  <c:when test="${consultancyDetails[0].firmType eq 'Private'}">
                    		<option selected="selected" value="Private">Private</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	     <option value="Private">Private</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 
                    	 <c:choose>
                    	  <c:when test="${consultancyDetails[0].firmType eq 'Public'}">
                    		<option selected="selected" value="Public">Public</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="Public">Public</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 
                    	 <c:choose>
                    	  <c:when test="${consultancyDetails[0].firmType eq 'Proprietary'}">
                    		<option selected="selected" value="Proprietary">Proprietary</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="Proprietary">Proprietary</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    	 
                    	 <c:choose>
                    	  <c:when test="${consultancyDetails[0].firmType eq 'Other'}">
                    		<option selected="selected" value="Other">Other</option>
                    	  </c:when>
                    	  <c:otherwise>
                    	    <option value="Other">Other</option>
                    	  </c:otherwise>
                    	 </c:choose>
                    		
                  		</select>
                    	<span id="firmTypeSpan" style="color:#FF0000"></span>
			     	 </div>
			     	   
			     <div class="col-xs-2">
			    	<label for="firmpanNumber">Firm PAN  No</label> 
                  	<input type="text" class="form-control" id="firmpanNumber" placeholder="Firm PAN No" name="firmpanNumber"  style="text-transform:uppercase" value="${consultancyDetails[0].firmpanNumber}">
                	<span id="firmpanNumberSpan" style="color:#FF0000"></span>
			     </div>
			        
			     <div class="col-xs-2">
			       <label for="firmgstNumber">Firm GST No</label>
                   <input type="text" class="form-control" id="firmgstNumber" placeholder="Firm GST No" name="firmgstNumber" style="text-transform:uppercase" value="${consultancyDetails[0].firmgstNumber}">
                   <span id="firmgstNumberSpan" style="color:#FF0000"></span>
			     </div>
			       
			     <div class="col-xs-3">
			     	<label for="checkPrintingName">Check Printing Name</label>
                    <input type="text" class="form-control" id="checkPrintingName" placeholder="Check Holder Name" name="checkPrintingName" style="text-transform:uppercase" value="${consultancyDetails[0].checkPrintingName}">
                    <span id="checkPrintingNameSpan" style="color:#FF0000"></span>
			     </div>
                   
              </div>
            </div>
            
		<!-- 	     
           <div class="box-body">
              <div class="row">
              
			     <div class="col-xs-3">
				     <label>Architectural RERA Number</label> 
	                 <input type="text" class="form-control" id="consultancyreraNumber" placeholder="Rera No:" name="consultancyreraNumber"  style="text-transform:uppercase">
	                 <span id="reraNumberSpan" style="color:#FF0000"></span>
			     </div>  
			       
			    </div>
		      </div>
		    -->   
		    
			<div class="box-body">
              <div class="row">
                  <div class="col-xs-3">
				      <label for="consultancyaddress">Address </label> <label class="text-red">* </label>
	                  <textarea class="form-control" rows="1" id="consultancyAddress" placeholder="Address" name ="consultancyAddress">${consultancyDetails[0].consultancyAddress}</textarea>
				      <span id="consultancyAddressSpan" style="color:#FF0000"></span>
			      </div> 
			
                 <div class="col-xs-2">
                  <label>Country </label> <label class="text-red">* </label>
                  		<select class="form-control" id="countryId" name="countryId"  onchange="getStateList(this.value)">
				  				<option selected="selected" value="${consultancyDetails[0].countryId}">${consultancyDetails[0].countryId}</option>
                      		<c:forEach var="countryList" items="${countryList}">
                       		 <c:choose>
                                <c:when test="${consultancyDetails[0].countryId ne countryList.countryId}">
	                    		    <option value="${countryList.countryId}">${countryList.countryName}</option>
	                            </c:when>
	                         </c:choose>
	                    	</c:forEach>
                  		</select>
                  <span id="countryIdSpan" style="color:#FF0000"></span>
                 </div>             
                  
                <div class="col-xs-2">
				 <label>State </label> <label class="text-red">* </label>
                       <select class="form-control" id="stateId" name="stateId" onchange="getCityList(this.value)">
				    	  <option selected="selected" value="${consultancyDetails[0].stateId}">${consultancyDetails[0].stateId}</option>
                       </select>
                 <span id="stateIdSpan" style="color:#FF0000"></span>
				</div> 
				
                 <div class="col-xs-2">
				  <label>City </label> <label class="text-red">* </label>
                   <select class="form-control" name ="cityId" id="cityId" onchange="getLocationAreaList(this.value)">
				  	  <option selected="selected" value="${consultancyDetails[0].cityId}">${consultancyDetails[0].cityId}</option>
                   </select>
                  <span id="cityIdSpan" style="color:#FF0000"></span>
				  </div> 
				  
				 <div class="col-xs-2">
				    <label>Area </label> <label class="text-red">* </label>
                     <select class="form-control" name="locationareaId" id="locationareaId" onchange="getpinCode(this.value)">
				  	  <option selected="selected" value="${consultancyDetails[0].locationareaId}">${consultancyDetails[0].locationareaId}</option>
                     </select>
                    <span id="locationareaIdSpan" style="color:#FF0000"></span>
				  </div> 
           
                  <div class="col-xs-1">
                   <label for="pincode">Pin Code </label> <label class="text-red">* </label>
                   <input type="text" class="form-control" id="areaPincode" placeholder="Pin Code" name="areaPincode" value="${consultancyDetails[0].areaPincode}">
			       <span id="areaPincodeSpan" style="color:#FF0000"></span>
			     </div> 
        
              </div>
            </div>
            	            
			<div class="box-body">
              <div class="row">
                   
				<div class="col-xs-3">
			    <label>Bank Name </label>
			      <input type="text" class="form-control" id="consultancyBankName" placeholder="Bak Name" name="consultancyBankName" style="text-transform: capitalize;" value="${consultancyDetails[0].consultancyBankName}">
                  <span id="consultancyBankNameSpan" style="color:#FF0000"></span>
			     </div> 
			     
			    <div class="col-xs-3">
			     <label>Bank Branch Name </label>
			     <input type="text" class="form-control" id="branchName" placeholder="Branch Name" name="branchName" style="text-transform: capitalize;" value="${consultancyDetails[0].branchName}"> 
				 <span id="branchNameSpan" style="color:#FF0000"></span>
			     </div> 
			  
                <div class="col-xs-3">
			     <label>IFSC </label>
                 <input type="text" class="form-control" id="bankifscCode" placeholder="Bank IFSC No " name="bankifscCode" style="text-transform:uppercase" value="${consultancyDetails[0].bankifscCode}">
			     <span id="bankifscCodeSpan" style="color:#FF0000"></span>
			     </div> 
			     
                <div class="col-xs-3">
			     <label for="bankacno">Bank A/C No </label> 
                 <input type="text" class="form-control" id="consultancyBankacno" placeholder="Bank A/C No "name="consultancyBankacno" value="${consultancyDetails[0].consultancyBankacno}">
                 <span id="consultancyBankacnoSpan" style="color:#FF0000"></span>
			    </div> 
			  
              </div>
            </div>
			<div class="box-body">
              <div class="row">
               
                  <div class="col-xs-2">
			      <label>Department</label> <label class="text-red">* </label>
                  <select class="form-control" name="departmentId" id="departmentId" onchange="getDesignationList(this.value)">
				  	<option selected="" value="Default">-Select Department-</option>
                    <c:forEach var="departmentList" items="${departmentList}">
                    <option value="${departmentList.departmentId}">${departmentList.departmentName}</option>
				    </c:forEach>
                  </select>
                   <span id="departmentIdSpan" style="color:#FF0000"></span>  
			     </div> 
			      
                 <div class="col-xs-2">
			      <label>Designation  </label> <label class="text-red">* </label>
                  <select class="form-control" name="designationId" id="designationId">
				  	<option selected="" value="Default">-Select Designation-</option>
                  </select>
                   <span id="designationIdSpan" style="color:#FF0000"></span>  
			     </div> 
			      
                 <div class="col-xs-2">
			      <label>Employee Name  </label> <label class="text-red">* </label>
			       <input type="text" class="form-control" id="employeeName" placeholder="Employee Name"name="employeeName">
                   <span id="employeeNameSpan" style="color:#FF0000"></span>  
			     </div> 
			               
			   
                  <div class="col-xs-2">
                    <label for="employeeEmailId">Email ID </label> <label class="text-red">* </label>
				    <div class="input-group">
                	 <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                	 <input type="text" class="form-control" placeholder="Email" id="employeeEmailId" name="employeeEmailId">
                     <span id="employeeEmailIdSpan" style="color:#FF0000"></span>
                    </div>
			     </div> 
                     
                 <div class="col-xs-3">
			     <label>Mobile No </label> <label class="text-red">* </label>
                  <div class="input-group">
                  <div class="input-group-addon">
                  <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask='"mask": "(+99) 9999999999"' data-mask name="employeeMobileNo" id="employeeMobileNo">
                  <span id="employeeMobilenoSpan" style="color:#FF0000"></span>
         		  <span class="input-group-btn">
            	 <button type="button" class="btn btn-success" onclick="AddConsultancyEmployee()"><i class="fa fa-plus"></i>Add </button>
              	</span>
       			 </div>
                </div> 
                  
              </div>
            </div>
			
			<div class="box-body">
           <div class="row">        
        	<div class="col-xs-12">
              <table class="table table-bordered" id="consultancyEmployeeListTable">
	              <tr bgcolor=#4682B4>
		              <th>Designation</th>
		              <th>Name</th>
		              <th>E-Mail Id</th>
		              <th>Mobile No.</th>
		              <th>Action</th>
	               </tr>

               	   <c:forEach items="${consultancyemployeeList}" var="consultancyemployeeList" varStatus="loopStatus">
                    <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                        <td>${consultancyemployeeList.designationId}</td>
                        <td>${consultancyemployeeList.employeeName}</td>
                        <td>${consultancyemployeeList.employeeEmailId}</td>
                        <td>${consultancyemployeeList.employeeMobileNo}</td>
                        <td>
                           <a onclick="DeleteConsultancyEmployee(${consultancyemployeeList.consultancyEmployeeId})" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a>
                        </td>
                     </tr>
                    </c:forEach> 
              </table>
            </div>
           </div>
          </div>
			
	          <div class="box-body">
	             <div class="row">
	              </br>
	             <div class="col-xs-1">
	             </div>
	             
	             <div class="col-xs-4">
	               <div class="col-xs-2">
	              	<a href="ConsultancyMaster"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
				</div>
				 </div>
				
				 <div class="col-xs-4">
	               <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
			     </div>
	
				<div class="col-xs-2">
				  <button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
	    	    </div> 
				 
	            </div>
			 </div>
			 
			 	     
  	          <input type="hidden" id="status" name="status" value="Active">	
			  <input type="hidden" id="consultancyStatus" name="consultancyStatus" value="${consultancyStatus}">	
			  <input type="hidden" id="creationDate" name="creationDate" value="">
			  <input type="hidden" id="updateDate" name="updateDate" value="">
			  <input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>">              
  
				  		
            </div>
          </div>
        </div>
		  
		  
    
		    
     
    </div>
 
</section>
</form>
</div>
  
<%@ include file="footer.jsp" %>
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->

<script>


function getDesignationList()
{

	 $("#designationId").empty();
	 var departmentId = $('#departmentId').val();
	 
	$.ajax({

		url : '${pageContext.request.contextPath}/getDesignationList',
		type : 'Post',
		data : { departmentId : departmentId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Designation-");
							$("#designationId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].designationId).text(result[i].designationName);
							    $("#designationId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
}


  
function AddConsultancyEmployee()
{
	clearall();

	if(document.consultancyform.departmentId.value=="Default")
	{
		$('#departmentIdSpan').html('Please, select Department..!');
		document.consultancyform.departmentId.focus();
		return false;
	}
	
	//validation for employee designation
	if(document.consultancyform.designationId.value=="Default")
	{
		$('#designationIdSpan').html('Please, select designation..!');
		document.consultancyform.designationId.focus();
		return false;
	}
	
	//validation for employee name
	if(document.consultancyform.employeeName.value=="")
	{
		$('#employeeNameSpan').html('Employee name should not be empty..!');
		document.consultancyform.employeeName.value="";
		document.consultancyform.employeeName.focus();
		return false;
	}
	
	else if(document.consultancyform.employeeName.value.match(/^[\s]+$/))
	{
		$('#employeeNameSpan').html('Employee name must contains alphabets only..!');
		document.consultancyform.employeeName.value="";
		document.consultancyform.employeeName.focus();
		return false;
	}
	else if(!document.consultancyform.employeeName.value.match(/^[a-zA-Z\s]+$/))
	{
		$('#employeeNameSpan').html('Employee name must contains alphabets only..!');
		document.consultancyform.employeeName.value="";
		document.consultancyform.employeeName.focus();
		return false;
	}

	//validation for employee email id
	if(document.consultancyform.employeeEmailId.value=="")
	{
		$('#employeeEmailIdSpan').html('Please, enter email id..!');
		document.consultancyform.employeeEmailId.focus();
		return false;
	}
	else if(!document.consultancyform.employeeEmailId.value.match(/^([a-z0-9_\.\-])+\@(([a-z0-9\-])+\.)+([a-z0-9]{2,4})+$/))
	{
		$('#employeeEmailIdSpan').html('Please, enter valid email id..!');
		document.consultancyform.employeeEmailId.value="";
		document.consultancyform.employeeEmailId.focus();
		return false;
	}
	
	//validation for employee mobile number
	if(document.consultancyform.employeeMobileNo.value=="")
	{
		$('#employeeMobileNoSpan').html('Please, enter mobile number..!');
		document.consultancyform.employeeMobileNo.focus();
		return false;
	}
	else if(!document.consultancyform.employeeMobileNo.value.match(/^[(]{1}[+]{1}[0-9]{2}[)]{1}[\s]{1}[0-9]{10}$/))
	{
		$('#employeeMobileNoSpan').html('mobile number must be 10 digit numbers only with correct format..!');
		document.consultancyform.employeeMobileNo.value="";
		document.consultancyform.employeeMobileNo.focus();
		return false;
	}
	
	 
	$('#consultancyEmployeeListTable tr').detach();
	 
	 var consultancyId = $('#consultancyId').val();
	 var designationId = $('#designationId').val();
	 var employeeName = $('#employeeName').val();
	 var employeeEmailId = $('#employeeEmailId').val();
	 var employeeMobileNo = $('#employeeMobileNo').val();
	 
	 $.ajax({

		 url : '${pageContext.request.contextPath}/AddConsultancyEmployee',
		type : 'Post',
		data : { consultancyId : consultancyId, designationId : designationId, employeeName : employeeName, employeeEmailId : employeeEmailId, employeeMobileNo : employeeMobileNo},
		dataType : 'json',
		success : function(result)
				  {
					   if (result) 
					   { 
							$('#consultancyEmployeeListTable').append('<tr style="background-color: #4682B4;"><th>Designation</th><th>Name</th><th>Email Id</th><th>Mobile No.</th><th>Action</th>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].consultancyEmployeeId;
									$('#consultancyEmployeeListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].designationId+'</td><td>'+result[i].employeeName+'</td><td>'+result[i].employeeEmailId+'</td><td>'+result[i].employeeMobileNo+'</td><td><a onclick="DeleteConsultancyEmployee('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
								else
								{
									var id = result[i].consultancyEmployeeId;
									$('#consultancyEmployeeListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].designationId+'</td><td>'+result[i].employeeName+'</td><td>'+result[i].employeeEmailId+'</td><td>'+result[i].employeeMobileNo+'</td><td><a onclick="DeleteConsultancyEmployee('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}
				  } 

		});
	 
	 $('#employeeName').val("");
	 $('#employeeEmailId').val("");
	 $('#employeeMobileNo').val("");
}

function DeleteConsultancyEmployee(consultancyEmployeeId)
{
	var consultancyId = $('#consultancyId').val();
    
    $('#consultancyEmployeeListTable tr').detach();
    
    $.ajax({

		 url : '${pageContext.request.contextPath}/DeleteConsultancyEmployee',
		type : 'Post',
		data : { consultancyId : consultancyId, consultancyEmployeeId : consultancyEmployeeId },
		dataType : 'json',
		success : function(result)
				  {
					if (result) 
					   { 
							$('#consultancyEmployeeListTable').append('<tr style="background-color: #4682B4;"><th>Designation</th><th>Name</th><th>Email Id</th><th>Mobile No.</th><th>Action</th>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].consultancyEmployeeId;
									$('#consultancyEmployeeListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].designationId+'</td><td>'+result[i].employeeName+'</td><td>'+result[i].employeeEmailId+'</td><td>'+result[i].employeeMobileNo+'</td><td><a onclick="DeleteConsultancyEmployee('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
								else
								{
									var id = result[i].consultancyEmployeeId;
									$('#consultancyEmployeeListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].designationId+'</td><td>'+result[i].employeeName+'</td><td>'+result[i].employeeEmailId+'</td><td>'+result[i].employeeMobileNo+'</td><td><a onclick="DeleteConsultancyEmployee('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}
				  } 

		});

	 $('#employeeName').val("");
	 $('#employeeEmailId').val("");
	 $('#employeeMobileNo').val("");
} 
	
function clearall()
{

	$('#consultancyfirmNameSpan').html('');
	$('#firmpanNumberSpan').html('');
	$('#firmgstNumberSpan').html('');
	
	$('#checkPrintingNameSpan').html('');
	
	$('#consultancyAddressSpan').html('');
	$('#countryIdSpan').html('');
	$('#stateIdSpan').html('');
	$('#cityIdSpan').html('');
	$('#locationareaIdSpan').html('');
	$('#areaPincodeSpan').html('');
	$('#consultancyBankNameSpan').html('');
	$('#branchNameSpan').html('');
	$('#bankifscCodeSpan').html('');
	$('#consultancyBankacnoSpan').html('');

	$('#designationIdSpan').html('');
	$('#employeeNameSpan').html('');
	$('#employeeEmailIdSpan').html('');
	$('#employeeMobileNoSpan').html('');
}

    function getpinCode()
    {

    	 $("#areaPincode").empty();
    	 var locationareaId = $('#locationareaId').val();
    	 var cityId = $('#cityId').val();
    	 var stateId = $('#stateId').val();
    	 var countryId = $('#countryId').val();
    	 $.ajax({

    		 url : '${pageContext.request.contextPath}/getallAreaList',
    		type : 'Post',
    		data : { locationareaId : locationareaId, cityId : cityId, stateId : stateId, countryId : countryId},
    		dataType : 'json',
    		success : function(result)
    				  {
    						if (result) 
    						{
    							
    							for(var i=0;i<result.length;i++)
    							{
    							 $('#areaPincode').val(result[i].pinCode);
    								
    							 } 
    						
    						} 
    						else
    						{
    							alert("failure111");
    							//$("#ajax_div").hide();
    						}

    					}
    		});
    	
    }


 function validate()
 {
	    clearall();
	    
	    //validation for consultancy first name
		if(document.consultancyform.consultancyfirmName.value=="")
		{
			$('#consultancyfirmNameSpan').html('Firm name should not be blank..!');
			document.consultancyform.consultancyfirmName.focus();
			return false;
		}
		else if(document.consultancyform.consultancyfirmName.value.match(/^[\s]+$/))
		{
			$('#consultancyfirmNameSpan').html('Firm name should not be blank..!');
			document.consultancyform.consultancyfirmName.value="";
			document.consultancyform.consultancyfirmName.focus();
			return false;
		}
		  
	    //validation for consultancy address----------------------
		if(document.consultancyform.consultancyAddress.value=="")
		{
			$('#consultancyAddressSpan').html('Please, enter consultancy address name..!');
			document.consultancyform.consultancyAddress.focus();
			return false;
		}
		else if(document.consultancyform.consultancyAddress.value.match(/^[\s]+$/))
		{
			$('#consultancyAddressSpan').html('Please, enter consultancy address ...!');
			document.consultancyform.consultancyAddress.focus();
			return false;
		}
	    
	    //validation for consultancy country
		if(document.consultancyform.countryId.value=="Default")
		{
			$('#countryIdSpan').html('Please, select consultancy country name..!');
			document.consultancyform.countryId.focus();
			return false;
		}
	    
	    //validation for consultancy state
		if(document.consultancyform.stateId.value=="Default")
		{
			$('#stateIdSpan').html('Please, select consultancy state name..!');
			document.consultancyform.stateId.focus();
			return false;
		}
	    
	    //validation for consultancy city
		if(document.consultancyform.cityId.value=="Default")
		{
			$('#cityIdSpan').html('Please, select consultancy city name..!');
			document.consultancyform.cityId.focus();
			return false;
		}
	    
	    //validation for consultancy location area
		if(document.consultancyform.locationareaId.value=="Default")
		{
			$('#locationareaIdSpan').html('Please, select consultancy area name..!');
			document.consultancyform.locationareaId.focus();
			return false;
		}

		 if(document.consultancyform.consultancyBankName.value.length!=0)
			{   
		    
			if(document.consultancyform.consultancyBankName.value=="")
			{
				$('#consultancyBankNameSpan').html('Please, enter bank name..!');
				document.consultancyform.consultancyBankName.focus();
				return false;
			}
			if(document.consultancyform.branchName.value=="")
			{
				$('#branchNameSpan').html('Please, enter branch name..!');
				document.consultancyform.branchName.focus();
				return false;
			}
			if(document.consultancyform.bankifscCode.value=="")
			{
				$('#bankifscCodeSpan').html('Please, enter IFSC code..!');
				document.consultancyform.bankifscCode.focus();
				return false;
			}
			if(document.consultancyform.consultancyBankacno.value=="")
			{
				$('#consultancyBankacnoSpan').html('Please, enter Account number..!');
				document.consultancyform.consultancyBankacno.focus();
				return false;
			}
			else if(!document.consultancyform.consultancyBankacno.value.match(/^[0-9]+$/))
				{
					$('#consultancyBankacnoSpan').html('Please, enter valid consultancy bank Ac/No..!');
					document.consultancyform.consultancyBankacno.value="";
					document.consultancyform.consultancyBankacno.focus();
					return false;
				}
		}

		 
}
 
function init()
{
	//clearall();
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	
	 
	 if(document.consultancyform.consultancyStatus.value == "Fail")
	 {
	  //	alert("Sorry, record is present already..!");
	 }
	 else if(document.consultancyform.consultancyStatus.value == "Success")
	 {
		 $('#statusSpan').html('Record added successfully..!');
	 }
	 
     document.consultancyform.consultancyfirmName.focus();
}


function getStateList()
{
	 $("#stateId").empty();
	 var countryId = $('#countryId').val();
	 
	$.ajax({

		url : '${pageContext.request.contextPath}/getStateList',
		type : 'Post',
		data : { countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select State-");
							$("#stateId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].stateId).text(result[i].stateName);
							    $("#stateId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get State List


function getCityList()
{
	 $("#cityId").empty();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();
	$.ajax({

		url : '${pageContext.request.contextPath}/getCityList',
		type : 'Post',
		data : { stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select City-");
							$("#cityId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].cityId).text(result[i].cityName);
							    $("#cityId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get City List

function getLocationAreaList()
{
	 $("#locationareaId").empty();
	 var cityId = $('#cityId').val();
	 var stateId = $('#stateId').val();
	 var countryId = $('#countryId').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getLocationAreaList',
		type : 'Post',
		data : { cityId : cityId, stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Location Area-");
							$("#locationareaId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].locationareaId).text(result[i].locationareaName);
							    $("#locationareaId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
	
}//end of get locationarea List

function getBranchList()
{
	 
	 $("#branchName").empty();
	 var bankName = $('#consultancyBankacname').val();
	
	 $.ajax({

		url : '${pageContext.request.contextPath}/getBranchList',
		type : 'Post',
		data : { bankName : bankName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Branch Name-");
							$("#branchName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].branchName).text(result[i].branchName);
							    $("#branchName").append(option);
							} 
						} 
						else
						{
							alert("failure111");
						}

					}
		});	
}

function getBranchIfsc()
{
	 //$("#bankifscCode").val('');
	 var bankName = $('#consultancyBankacname').val();
     var branchName = $('#branchName').val();
	 $.ajax({

		url : '${pageContext.request.contextPath}/getBranchIfsc',
		type : 'Post',
		data : { bankName : bankName, branchName : branchName },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							$('#bankifscCode').val(result[0].bankifscCode);
							/* for(var i=0;i<result.length;i++)
							{
								 var option = $('<option />');
							    option.attr('value',result[i].branchName).text(result[i].branchName);
							    $("#bankifscCode").append(option); 
							}   */
						} 
						else
						{
							alert("failure111");
						}

					}
		});
}

/* 
function getuniqueaadharnumberName()
{
	
	 var consultancyAadharcardno = $('#consultancyAadharcardno').val();
			$.ajax({

			url : '${pageContext.request.contextPath}/getaadharList',
			type : 'Post',
			data : { consultancyAadharcardno : consultancyAadharcardno},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{
								 
								for(var i=0;i<result.length;i++)
								{
									
									if(result[i].consultancyAadharcardno==consultancyAadharcardno)
										{
										
											 //document.stateform.stateId.value="";
											  document.consultancyform.consultancyAadharcardno.focus();
											  $('#consultancyAadharcardnoSpan').html('This aadhar number is already exist..!');
											 // $('#stateId').val("");
									
										}
									
									
								 } 
							} 
							else
							{
								alert("failure111");
								//$("#ajax_div").hide();
							}

						}
			});	
}
 */

  $(function ()
  {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
     $('#consultancyDob').datepicker({
      autoclose: true
    })
 $('#consultancyJoiningdate').datepicker({
      autoclose: true
    })

$('#consultancyAnniversaryDate').datepicker({
    autoclose: true
  })
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>

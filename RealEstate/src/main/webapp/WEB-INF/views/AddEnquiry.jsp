<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Real Estate | Add Enquiry</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
<!-- daterange picker -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<!-- bootstrap datepicker -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
<!-- Bootstrap Color Picker -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
<!-- Bootstrap time Picker -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
<!-- Select2 -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
<!-- Theme style -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

<!-- Google Font -->
<style type="text/css">
tr.odd {
	background-color: #CCE5FF
}

tr.even {
	background-color: #F0F8FF
}
</style>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
    </script>
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
	<div class="wrapper">

		<%@ include file="headerpage.jsp"%>
		<!-- Left side column. contains the logo and sidebar -->
		<%@ include file="menu.jsp"%>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Add Enquiry Details <small>Preview</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><a href="#">Master</a></li>
					<li class="active">Add Enquiry</li>
				</ol>
			</section>

			<!-- Main content -->

			<form name="enquiryform"
				action="${pageContext.request.contextPath}/AddEnquiry" method="Post"
				onSubmit="return validate()">
				<section class="content">

					<!-- SELECT2 EXAMPLE -->
					<div class="box box-default">
						<div class="panel box box-danger"></div>

						<!-- /.box-header -->
						<div class="box-body">
							<div class="row">
								<div class="col-md-12">

									<!-- /.form-group -->

									<span id="statusSpan" style="color: #008000"></span>
									<div class="box-body">
										<div class="row">
											<div class="col-xs-2">
												<label for="enquairyid">Enquiry Id</label> <input
													type="text" class="form-control" id="enquiryId"
													placeholder="ID" name="enquiryId" value="${enquiryCode}"
													readonly>
											</div>

										</div>
									</div>


									<div class="box-body">
										<div class="row">
											<div class="col-xs-3">
												<label for="enquairyfirstname">Customer Name</label><label
													class="text-red">* </label> <input type="text"
													class="form-control" id="enqfirstName"
													placeholder="First Name" name="enqfirstName"
													onchange="changeStatus()"
													style="text-transform: capitalize;"> <span
													id="enqfirstNameSpan" style="color: #FF0000"></span>
											</div>

											<!-- 
                <div class="col-xs-3">
				<label for="enquairymiddlename">Middle Name </label>
                  <input type="text" class="form-control" id="enqmiddleName" placeholder="Middle Name"  name="enqmiddleName" style="text-transform: capitalize;">
                 <span id="enqmiddleNameSpan" style="color:#FF0000"></span>
                </div>
                <div class="col-xs-3">
				<label for="enquairylastname">Last Name </label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="enqlastName" placeholder="Last Name" name="enqlastName" style="text-transform: capitalize;">
                 <span id="enqlastNameSpan" style="color:#FF0000"></span>
                </div>
                 -->
											<!--    
              </div>
            </div>
				
				
		    <div class="box-body">
              <div class="row">
               -->
											<!--  
                 <div class="col-xs-3">
			       <label for="radiobutton">Gender</label><br/>
                   <input type="radio" class="minimal" name="enqGender" value="Male"> Male
				   &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
  				   <input type="radio" class="minimal" name="enqGender" value="Female"> Female<br/>
  				   <span id="enqGenderSpan" style="color:#FF0000"></span>
                 </div>

                <div class="col-xs-3">
			     <label>Date Of Birth </label>
				 <div class="input-group">
                   <div class="input-group-addon">
                     <i class="fa fa-calendar"></i>
                   </div>
                   <input type="text" class="form-control pull-right" id="datepicker" name="enqDob">
                   <span id="enqDobSpan" style="color:#FF0000"></span>
                  </div>
			    </div>
			     -->
											<div class="col-xs-3">
												<label>Mobile No(Primary)</label><label class="text-red">*
												</label>
												<div class="input-group">
													<div class="input-group-addon">
														<i class="fa fa-phone"></i>
													</div>
													<input type="text" class="form-control"
														data-inputmask='"mask": "9999999999"' data-mask
														name="enqmobileNumber1" data-mask id="enqmobileNumber1"
														name="enqmobileNumber1"
														onchange="getuniquemobilenumber(this.value)"> <span
														id="enqmobileNumber1Span" style="color: #FF0000"></span>
												</div>
											</div>

											<div class="col-xs-3">
												<label>Mobile No(Optional)</label>
												<div class="input-group">
													<div class="input-group-addon">
														<i class="fa fa-phone"></i>
													</div>
													<input type="text" class="form-control"
														data-inputmask='"mask": "9999999999"' data-mask
														name="enqmobileNumber2" data-mask id="enqmobileNumber2">
													<span id="enqmobileNumber2Span" style="color: #FF0000"></span>
												</div>
											</div>
											<div class="col-xs-3">
												<label for="emailid">Email ID </label><label
													class="text-red">* </label>
												<div class="input-group">
													<span class="input-group-addon"><i
														class="fa fa-envelope"></i></span> <input type="text"
														class="form-control" placeholder="Email" name="enqEmail"
														id="enqEmail">
												</div>
												<span id="enqEmailSpan" style="color: #FF0000"></span>
											</div>
										</div>
									</div>
									<div class="box-body">
										<div class="row">

											<div class="col-xs-3">
												<label>Project Name</label><label class="text-red">*
												</label> <select class="form-control" name="projectName"
													id="projectName" >
													<option selected="" value="Default">-Select
														Project-</option>
													<c:forEach var="projectList" items="${projectList}">
														<option value="${projectList.projectName}">${projectList.projectName}</option>
													</c:forEach>
												</select> <span id="projectNameSpan" style="color: #FF0000"></span>
											</div>
											<div class="col-xs-3">
												<label>Flat Type </label><label class="text-red">* </label>
												<select class="form-control" name="flatType" id="flatType">
													<option selected="" value="Default">-Select Flat
														Type-</option>
													<option>RK</option>
													<option>1 BHK</option>
													<option>2 BHK</option>
													<option>3 BHK</option>
													<option>4 BHK</option>
												</select> <span id="flatTypeSpan" style="color: #FF0000"></span>
											</div>
											<div class="col-xs-3">
												<label for="flatRemark">Flat Remark </label>
												<textarea class="form-control" rows="1" id="flatRemark"
													placeholder="flatRemark" name="flatRemark"></textarea>
												<span id="flatRemarkSpan" style="color: #FF0000"></span>
											</div>
											<div class="col-xs-3">
												<label>Budget (Rs.)</label><label class="text-red">*
												</label> <select class="form-control" name="flatBudget"
													id="flatBudget">
													<option selected="" value="Default">-Select
														Budget-</option>
													<c:forEach var="budgetList" items="${budgetList}">
														<option value="${budgetList.budgetCost}">${budgetList.budgetCost}</option>
													</c:forEach>
												</select> <span id="flatBudgetSpan" style="color: #FF0000"></span>
											</div>
										</div>
									</div>

									<div class="box-body">
										<div class="row">



											<div class="col-xs-3">
												<label for="sl">Enquiry Source </label><label
													class="text-red">* </label> <select class="form-control"
													name="enquirysourceId" id="enquirysourceId"
													onchange="getSubEnquiryList(this.value)">
													<option selected="" value="Default">-Select
														Enquiry Source-</option>
													<c:forEach var="enquirySourceList"
														items="${enquirySourceList}">
														<option value="${enquirySourceList.enquirysourceId}">${enquirySourceList.enquirysourceName}</option>
													</c:forEach>
												</select> <span id="enquirysourceIdSpan" style="color: #FF0000"></span>
											</div>

											<div class="col-xs-3">
												<label for="sl">Sub-Enquiry Source </label><label
													class="text-red">* </label> <select class="form-control"
													name="subsourceId" id="subsourceId">
													<option selected="" value="Default">-Select Sub
														Enquiry Source-</option>

												</select> <span id="subsourceIdSpan" style="color: #FF0000"></span>
											</div>
											<div class="col-xs-3">
												<label for="pancardno">Occupation </label> <select
													class="form-control" name="enqOccupation"
													id="enqOccupation">
													<option selected="" value="Default">-Select
														Occupation-</option>
													<c:forEach var="occupationList" items="${occupationList}">
														<option value="${occupationList.occupationName}">${occupationList.occupationName}</option>
													</c:forEach>
												</select> <span id="enqOccupationSpan" style="color: #FF0000"></span>
											</div>
										</div>
									</div>
									<div class="box-body">
										<div class="row">

											<div class="col-xs-3">
												<label for="status">Status </label><label class="text-red">*
												</label> <select class="form-control" name="enqStatus"
													id="enqStatus">
													<option selected="" value="Default">-Status-</option>
													<option>In-Process</option>
													<option>Site Visited</option>
													<option>Booking</option>
													<option>Cancel</option>
												</select> <span id="enqStatusSpan" style="color: #FF0000"></span>
											</div>

											<div class="col-xs-3">
												<label for="enquairyremark">Remark </label><label
													class="text-red">* </label>
												<textarea class="form-control" rows="1" id="enqRemark"
													placeholder="Remark" name="enqRemark"></textarea>
												<span id="enqRemarkSpan" style="color: #FF0000"></span>
											</div>
											<div class="col-xs-3">
												<label>Follow-up Date</label><label class="text-red">*
												</label>
												<div class="input-group date">
													<div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</div>
													<input type="text" class="form-control pull-right"
														id="datepicker1" name="followupDate"> <span
														id="followupDateSpan" style="color: #FF0000"></span>
												</div>
											</div>
										</div>
									</div>

								</div>
							</div>
							</br>
							<div class="box-body">
								<div class="row">
									<div class="col-xs-4">
										<div class="col-xs-2">
											<a href="EnquiryMaster"><button type="button"
													class="btn btn-block btn-primary" value="Back"
													style="width: 90px">Back</button></a>
										</div>
									</div>
									<div class="col-xs-4">
										<button type="reset" class="btn btn-default" value="reset"
											style="width: 90px">Reset</button>
									</div>
									<div class="col-xs-2">
										<button type="submit" class="btn btn-info pull-right"
											name="submit">Submit</button>

									</div>

								</div>
							</div>

							<input type="hidden" id="enquirydbStatus" name="enquirydbStatus"
								value="${enquirydbStatus}"> <input type="hidden"
								id="creationDate" name="creationDate" value=""> <input
								type="hidden" id="updateDate" name="updateDate" value="">
							<input type="hidden" id="userName" name="userName"
								value="<%= session.getAttribute("user") %>">
						</div>

					</div>
				</section>
			</form>
			<!-- /.content -->
		</div>


		<!-- Control Sidebar -->
		<%@ include file="footer.jsp"%>
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

	<!-- jQuery 3 -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap 3.3.7 -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- Select2 -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
	<!-- InputMask -->
	<script
		src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
	<!-- date-range-picker -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
	<!-- bootstrap datepicker -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
	<!-- bootstrap color picker -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
	<!-- bootstrap time picker -->
	<script
		src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
	<!-- SlimScroll -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<!-- iCheck 1.0.1 -->
	<script
		src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
	<!-- FastClick -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script
		src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script
		src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
	<!-- Page script -->

	<script>
function clearall(){
	$('#enqfirstNameSpan').html('');
	/* 
	$('#enqmiddleNameSpan').html('');
	$('#enqlastNameSpan').html('');
	 */
	$('#enqEmailSpan').html('');
	$('#enqmobileNumber1Span').html('');
	$('#enqmobileNumber2Span').html('');
	
	$('#projectNameSpan').html('');
	$('#buildingNameSpan').html('');
	$('#wingNameSpan').html('');
	$('#floortypeNameSpan').html('');
	$('#flatTypeSpan').html('');
	$('#flatNumberSpan').html('');
	$('#flatBudgetSpan').html('');
	$('#enquirysourceIdSpan').html('');
	$('#subsourceIdSpan').html('');
	$('#followupDateSpan').html('');
	$('#enqStatusSpan').html('');
	$('#enquirydbStatusSpan').html('');
	$('#statusSpan').html('');
	
}
function changeStatus()
{
	$('#enquirydbStatusSpan').html('');
}
function validate()
{ 
	clearall();
		if(document.enquiryform.enqfirstName.value=="")
		{
			$('#enqfirstNameSpan').html('First name should not be empty..!');
			document.enquiryform.enqfirstName.focus();
			return false;
		}
	/* 	
		else if(document.enquiryform.enqfirstName.value.match(/^[\s]+$/))
		{
			$('#enqfirstNameSpan').html('First name should not be empty..!');
			document.enquiryform.enqfirstName.value="";
			document.enquiryform.enqfirstName.focus();
			return false;
		}
		else if(!document.enquiryform.enqfirstName.value.match(/^[A-Za-z]+[.]{0,1}$/))
		{
			$('#enqfirstNameSpan').html('First name should contain alphabets only..!');
			//document.enquiryform.enqfirstName.value="";
			document.enquiryform.enqfirstName.focus();
			return false;
		}
		 */
		/* 
		//validation for middle name
		if(document.enquiryform.enqmiddleName.value.length!=0)
		{
			$('#enqfirstNameSpan').html('');
			 if(!document.enquiryform.enqmiddleName.value.match(/^[A-Za-z]+[.]{0,1}$/))
			{
				 $('#enqmiddleNameSpan').html(' Middle name should contain alphabets only..!');
				document.enquiryform.enqmiddleName.value="";
				document.enquiryform.enqmiddleName.focus();
				return false;
			}
		}
		
		//validation for last name
		if(document.enquiryform.enqlastName.value=="")
		{
			 $('#enqlastNameSpan').html('Last name should not be empty..!');
			document.enquiryform.enqlastName.focus();
			return false;
		}
		else if(document.enquiryform.enqlastName.value.match(/^[\s]+$/))
		{
			 $('#enqlastNameSpan').html('Last name should not be empty..!');
			//document.enquiryform.enqlastName.value="";
			document.enquiryform.enqlastName.focus();
			return false;
		}
		else if(!document.enquiryform.enqlastName.value.match(/^[A-Za-z]+[.]{0,1}$/))
		{
			 $('#enqlastNameSpan').html('Last name should contain alphabets only..!');
			document.enquiryform.enqlastName.value="";
			document.enquiryform.enqlastName.focus();
			return false;
		}	 */	
		/*
		//validation for gender
		if(( document.enquiryform.enqGender[0].checked == false ) && ( document.enquiryform.enqGender[1].checked == false ) )
		{
			 $('#enqGenderSpan').html('Please, select Male or Female..!');
			document.enquiryform.enqGender[0].focus();
			return false;
		}
		
		//validation for date of birth
		if(document.enquiryform.enqDob.value!="")
		{
				var today = new Date();
				var dd1 = today.getDate();
				var mm1 = today.getMonth()+1;
				var yy1 = today.getFullYear();
				
		    	var dob= document.enquiryform.enqDob.value;
			   	var dob2=dob.split("/");
			    var yydiff=parseInt(yy1)-parseInt(dob2[2]);
			    if(yydiff<=18)
			    {
			    		 $('#enqDobSpan').html(' date of birth should be 18 or 18+ years..!');
			    		document.enquiryform.enqDob.value="";
			    		document.enquiryform.enqDob.focus();
			    		return false;
			    }
		}
		
		*/
		//validation for mobile number 1
		if(document.enquiryform.enqmobileNumber1.value=="")
		{
			 $('#enqmobileNumber1Span').html('Please, enter primary mobile number..!');
			document.enquiryform.enqmobileNumber1.value="";
			document.enquiryform.enqmobileNumber1.focus();
			return false;
		}
		else if(!document.enquiryform.enqmobileNumber1.value.match(/^[0-9]{10}$/))  
		{
			 $('#enqmobileNumber1Span').html(' enter valid primary mobile number..!');
			document.enquiryform.enqmobileNumber1.value="";
			document.enquiryform.enqmobileNumber1.focus();
			return false;	
		}
		
		//validation for mobile number 2
		if(document.enquiryform.enqmobileNumber2.value.length!=0)
		{
			if(!document.enquiryform.enqmobileNumber2.value.match(/^[0-9]{10}$/))
			{
				 $('#enqmobileNumber2Span').html(' enter valid secondary mobile number..!');
				document.enquiryform.enqmobileNumber2.value="";
				document.enquiryform.enqmobileNumber2.focus();
				return false;	
			}
		}
		
		/*
		//validation for gender
		 if(( document.enquiryform.enqGender[0].checked == false ) && ( document.enquiryform.enqGender[1].checked == false ) )
		{
			 $('#enqGenderSpan').html('Please, choose your Gender: Male or Female..!');
			document.enquiryform.enqGender[0].focus();
			return false;
		}
		*/
		/*
		//validation for address
		if(document.enquiryform.enqAddress.value=="")
		{
			 $('#enqAddressSpan').html('Please, enter address..!');
			document.enquiryform.enqAddress.focus();
			return false;
		}
		else if(document.enquiryform.enqAddress.value.match(/^[\s]+$/))
		{
			 $('#enqAddressSpan').html('Please, enter employee address name..!');
			document.enquiryform.enqAddress.focus();
			return false;
		}
	
		//validation for country name
		if(document.enquiryform.countryName.value=="Default")
		{
			 $('#countryNameSpan').html('Please, select country name..!');
			document.enquiryform.countryName.focus();
			return false;
		}
		
		//validation for state name
		if(document.enquiryform.stateName.value=="Default")
		{
			 $('#stateNameSpan').html('Please, select state name..!');
			document.enquiryform.stateName.focus();
			return false;
		}
		
		//validation for city name
		if(document.enquiryform.cityName.value=="Default")
		{
			 $('#cityNameSpan').html('Please, select city name..!');
			document.enquiryform.cityName.focus();
			return false;
		}
		
		//validation for location area name
		if(document.enquiryform.locationareaName.value=="Default")
		{
			 $('#locationareaNameSpan').html('Please, select location name..!');
			document.enquiryform.locationareaName.focus();
			return false;
		}
		*/
		//validation for email
		if(document.enquiryform.enqEmail.value=="")
		{
			 $('#enqEmailSpan').html('Email Id should not be blank..!');
			document.enquiryform.enqEmail.focus();
			return false;
		}
		else if(!document.enquiryform.enqEmail.value.match(/^(([\-\w]+)\.?)+@(([\-\w]+)\.?)+\.[a-z]{2,4}$/))
		{
			 $('#enqEmailSpan').html(' enter valid email id..!');
			document.enquiryform.enqEmail.value="";
			document.enquiryform.enqEmail.focus();
			return false;
		}
		
/*
		//validation for pan card
		if(document.enquiryform.enqPancardno.value.length!=0)
		{
			if(!document.enquiryform.enqPancardno.value.match(/^[A-Za-z]{5}[0-9]{4}[A-z]{1}$/))
			{
				 $('#enqPancardnoSpan').html(' PAN number must start with 5 alphabets follwed by 4 digit number and 1 alphabet..!');
				document.enquiryform.enqPancardno.value="";
				document.enquiryform.enqPancardno.focus();
				return false;
			}
		}
		
		//validation for aadhar number
		if(document.enquiryform.enqAadharno.value.length!=0)
		{
			 if(!document.enquiryform.enqAadharno.value.match(/^\d{4}\d{4}\d{4}$/))
		    {
				 $('#enqAadharnoSpan').html('Aadhar card number should be 12 digit number only..!');
				document.enquiryform.enqAadharno.value="";
				document.enquiryform.enqAadharno.focus();
				return false;
		    }
		}
		
		//validation for occupation
		if(document.enquiryform.enqOccupation.value=="Default")
		{
			 $('#enqOccupationSpan').html('Please, select proper occupation..!');
			document.enquiryform.enqOccupation.focus();
			return false;
		}
		
		//validation for enquiry person company name
		if(document.enquiryform.enqcompanyName.value=="")
		{
			 $('#enqcompanyNameSpan').html('Please, enter company name..!');
			document.enquiryform.enqcompanyName.focus();
			return false;
		}
		else if(document.enquiryform.enqcompanyName.value.match(/^[\s]+$/))
		{
			 $('#enqcompanyNameSpan').html('Please, enter company name..!');
			document.enquiryform.enqcompanyName.value="";
			document.enquiryform.enqcompanyName.focus();
			return false;
		}
		else if(!document.enquiryform.enqcompanyName.value.match(/^[A-Za-z\s.]+[\s]{0,10}$/))
		{
			 $('#enqcompanyNameSpan').html('Please, enter company name..!');
			document.enquiryform.enqcompanyName.value="";
			document.enquiryform.enqcompanyName.focus();
			return false;
		}
		
		//validation for enquiry person office number
		if(document.enquiryform.enqofficeNumber.value=="")
		{
			 $('#enqofficeNumberSpan').html('Please, enter office phone number..!');
			document.enquiryform.enqofficeNumber.value="";
			document.enquiryform.enqofficeNumber.focus();
			return false;
		}
		else if(!document.enquiryform.enqofficeNumber.value.match(/^\(?([0-9]{3})\)?[\s. ]?([0-9]{3})[-. ]?([0-9]{5})$/))
		{
			 $('#enqofficeNumberSpan').html(' phone number must be 10 digit numbers only..!');
			document.enquiryform.enqofficeNumber.value="";
			document.enquiryform.enqofficeNumber.focus();
			return false;	
		}
	*/	
	/*	
		//validation for project building
		if(document.enquiryform.buildingName.value=="Default")
		{
			 $('#buildingNameSpan').html('Please, select project building name..!');
			document.enquiryform.buildingName.focus();
			return false;
		}
		//validation for project wing
		if(document.enquiryform.wingName.value=="Default")
		{
			 $('#wingNameSpan').html('Please, select project wing name..!');
			document.enquiryform.wingName.focus();
			return false;
		}
		//validation for floor
		if(document.enquiryform.floortypeName.value=="Default")
		{
			 $('#floortypeNameSpan').html('Please, select floor..!');
			document.enquiryform.floortypeName.focus();
			return false;
		}
		*/
		//validation for project name-----------------------------------------------------
		
		if(document.enquiryform.projectName.value=="Default")
		{
			 $('#projectNameSpan').html('Please, select project name..!');
			document.enquiryform.projectName.focus();
			return false;
		}
		
		//validation for flat type
		if(document.enquiryform.flatType.value=="Default")
		{
			 $('#flatTypeSpan').html('Please, select flat type..!');
			document.enquiryform.flatType.focus();
			return false;
		}
		/*
		//validation for flat number
		if(document.enquiryform.flatNumber.value=="Default")
		{
			 $('#flatNumberSpan').html('Please, select flat number..!');
			document.enquiryform.flatNumber.focus();
			return false;
		}
		
		//validation for bank name
		if(document.enquiryform.bankName.value=="Default")
		{
			 $('#bankNameSpan').html('Please, select bank name..!');
			document.enquiryform.bankName.focus();
			return false;
		}
		
	//validation for bank branch name
		if(document.enquiryform.branchName.value=="Default")
		{
			$('#branchNameSpan').html('Please, select branch name..!');
			document.enquiryform.branchName.focus();
			return false;
		}
		*/
		//validation for budget cost
		if(document.enquiryform.flatBudget.value=="Default")
		{
			$('#flatBudgetSpan').html('Please, select budget..!');
			document.enquiryform.flatBudget.focus();
			return false;
		}
		
		//validation for enquiry source
		if(document.enquiryform.enquirysourceId.value=="Default")
		{
			 $('#enquirysourceIdSpan').html('Please, select enquiry source..!');
			document.enquiryform.enquirysourceId.focus();
			return false;
		}
		
		//validation for enquiry source
		if(document.enquiryform.subsourceId.value=="Default")
		{
			 $('#subsourceIdSpan').html('Please, select sub enquiry source..!');
			document.enquiryform.subsourceId.focus();
			return false;
		}
		
		//validation for follow up date
		if(document.enquiryform.followupDate.value=="")
		{
			 $('#followupDateSpan').html('Please, select next follow-up date..!');
			document.enquiryform.followupDate.focus();
			return false;
		}
		else
		{
			/* var todayDate =  new Date();
			var selectedDate = new Date(document.enquiryform.followupDate.value);
			
			if(selectedDate <= todayDate)
			{
				 $('#followupDateSpan').html('Please, select next valid follow-up date..!');
				document.enquiryform.followupDate.value="";
				document.enquiryform.followupDate.focus();
				return false;
			}
			 */
		 }
		
		//validation for enquiry status
		if(document.enquiryform.enqStatus.value=="Default")
		{
			 $('#enqStatusSpan').html('Please, select enquiry person status..!');
			document.enquiryform.enqStatus.focus();
			return false;
		}
		
		//validation for remark
		if(document.enquiryform.enqRemark.value=="")
		{
			 $('#enqRemarkSpan').html('Please, enter enquiry remark..!');
			document.enquiryform.enqRemark.focus();
			return false;
		}
		else if(document.enquiryform.enqRemark.value.match(/^[\s]+$/))
		{
			 $('#enqRemarkSpan').html('Please, enter enquiry remark..!');
			document.enquiryform.enqRemark.value="";
			document.enquiryform.enqRemark.focus();
			return false;
		}
		else if(!document.enquiryform.enqRemark.value.match(/^[A-Za-z0-9\s./]+$/))
		{
			 $('#enqRemarkSpan').html('Please, enter valid enquiry remark..!');
			document.enquiryform.enqRemark.value="";
			document.enquiryform.enqRemark.focus();
			return false;
		}
}

function getBranchList()
{
	 $("#branchName").empty();
	 var bankName = $('#bankName').val();
	
	 $.ajax({

		url : '${pageContext.request.contextPath}/getBranchList',
		type : 'Post',
		data : { bankName : bankName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Branch Name-");
							$("#branchName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].branchName).text(result[i].branchName);
							    $("#branchName").append(option);
							} 
						} 
						else
						{
							alert("failure111");
						}

					}
		});	
}

function getBranchIfsc()
{
	 //$("#bankifscCode").val('');
	 var bankName = $('#bankName').val();
     var branchName = $('#branchName').val();
	 
	 $.ajax({

		url : '${pageContext.request.contextPath}/getBranchIfsc',
		type : 'Post',
		data : { bankName : bankName, branchName : branchName },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							$('#bankifscCode').val(result[0].bankifscCode);
							/* for(var i=0;i<result.length;i++)
							{
								 var option = $('<option />');
							    option.attr('value',result[i].branchName).text(result[i].branchName);
							    $("#bankifscCode").append(option); 
							}   */
						} 
						else
						{
							alert("failure111");
						}

					}
		});
}

function init()
{
	$('#enquirydbStatusSpan').html('');
	clearall();

	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	
	 if(document.enquiryform.enquirydbStatus.value == "Fail")
	 {
		 $('#enquirydbStatusSpan').html(' record is present already..!');
	 }
	 else if(document.enquiryform.enquirydbStatus.value == "Success")
	 {
	 $('#statusSpan').html('Record added successfully..!');
	
	 }
    document.enquiryform.enqfirstName.focus();
	 
}

function getSubEnquiryList()
{
	 $("#subsourceId").empty();
	 var enquirysourceId = $('#enquirysourceId').val();
	 
	$.ajax({

		url : '${pageContext.request.contextPath}/getSubEnquiryList',
		type : 'Post',
		data : { enquirysourceId : enquirysourceId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Sub Enquiry Source-");
							$("#subsourceId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].subsourceId).text(result[i].subenquirysourceName);
							   
							    $("#subsourceId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
}

function getStateList()
{
	 $("#stateName").empty();
	 var countryName = $('#countryName').val();
	 
	$.ajax({

		url : '${pageContext.request.contextPath}/getStateList',
		type : 'Post',
		data : { countryName : countryName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select State-");
							$("#stateName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].stateName).text(result[i].stateName);
							    $("#stateName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get State List


function getCityList()
{
	 $("#cityName").empty();
	 var stateName = $('#stateName').val();
	 var countryName = $('#countryName').val();
	$.ajax({

		url : '${pageContext.request.contextPath}/getCityList',
		type : 'Post',
		data : { stateName : stateName, countryName : countryName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select City-");
							$("#cityName").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].cityName).text(result[i].cityName);
							    $("#cityName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get City List

function getLocationAreaList()
{
	 $("#locationareaName").empty();
	 var cityName = $('#cityName').val();
	 var stateName = $('#stateName').val();
	 var countryName = $('#countryName').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getLocationAreaList',
		type : 'Post',
		data : { cityName : cityName, stateName : stateName, countryName : countryName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Location Area-");
							$("#locationareaName").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].locationareaName).text(result[i].locationareaName);
							    $("#locationareaName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get locationarea List


function getWingNameList()
{
	 $("#wingName").empty();
	 $("#floortypeName").empty();
	 $("#flatNumber").empty();
	 $("#flatType").empty();
	 var buildingName = $('#buildingName').val();
	 var projectName = $('#projectName').val();
	 document.enquiryform.flatFacing.value="";
		document.enquiryform.flatareainSqFt.value="";
		document.enquiryform.flatbasicCost.value="";
	 $.ajax({

		url : '${pageContext.request.contextPath}/getprojectwingList',
		type : 'Post',
		data : { buildingName : buildingName, projectName : projectName },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Floor Name-");
							$("#floortypeName").append(option);
							var option = $('<option/>');

							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Flat Type-");
							$("#flatType").append(option);
							var option = $('<option/>');
							option.attr('value',"RK").text("RK");
							$("#flatType").append(option);
							var option = $('<option/>');
							option.attr('value',"1 BHK").text("1 BHK");
							$("#flatType").append(option);
							var option = $('<option/>');
							option.attr('value',"2 BHK").text("2 BHK");
							$("#flatType").append(option);
							var option = $('<option/>');
							option.attr('value',"3 BHK").text("3 BHK");
							$("#flatType").append(option);
							var option = $('<option/>');
							option.attr('value',"4 BHK").text("4 BHK");
							$("#flatType").append(option);
						
			                var option = $('<option/>');
							option.attr('value',"Default").text("-Select Flat Number-");
							$("#flatNumber").append(option);
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Wing Name-");
							$("#wingName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].wingName).text(result[i].wingName);
							    $("#wingName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}

function getBuildingList()
{
	 $("#buildingName").empty();
	 $("#wingName").empty();
	 $("#floortypeName").empty();
	 $("#flatNumber").empty();
	 $("#flatType").empty();
	 document.enquiryform.flatFacing.value="";
		document.enquiryform.flatareainSqFt.value="";
		document.enquiryform.flatbasicCost.value="";
	 var projectName = $('#projectName').val();
	 
	$.ajax({

		url : '${pageContext.request.contextPath}/getBuildingList1',
		type : 'Post',
		data : { projectName : projectName },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Wing Name-");
							$("#wingName").append(option);
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Floor Name-");
							$("#floortypeName").append(option);
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Flat Number-");
							$("#flatNumber").append(option);

							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Flat Type-");
							$("#flatType").append(option);
							var option = $('<option/>');
							option.attr('value',"RK").text("RK");
							$("#flatType").append(option);
							var option = $('<option/>');
							option.attr('value',"1 BHK").text("1 BHK");
							$("#flatType").append(option);
							var option = $('<option/>');
							option.attr('value',"2 BHK").text("2 BHK");
							$("#flatType").append(option);
							var option = $('<option/>');
							option.attr('value',"3 BHK").text("3 BHK");
							$("#flatType").append(option);
							var option = $('<option/>');
							option.attr('value',"4 BHK").text("4 BHK");
							$("#flatType").append(option);
						
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Building-");
							$("#buildingName").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].buildingName).text(result[i].buildingName);
							    $("#buildingName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get Building List  


function getFloorNameList()
{
	 $("#floortypeName").empty();
	 $("#flatNumber").empty();
	 $("#flatType").empty();
	 var wingName = $('#wingName').val();
	 var buildingName = $('#buildingName').val();
	 var projectName = $('#projectName').val();
	 document.enquiryform.flatFacing.value="";
		document.enquiryform.flatareainSqFt.value="";
		document.enquiryform.flatbasicCost.value="";
	 
	 $.ajax({

		url : '${pageContext.request.contextPath}/getwingfloorNameList',
		type : 'Post',
		data : {wingName : wingName, buildingName : buildingName, projectName : projectName },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Floor Name-");
							$("#floortypeName").append(option);
							
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Flat Number-");
							$("#flatNumber").append(option);
							
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Flat Type-");
							$("#flatType").append(option);
							var option = $('<option/>');
							option.attr('value',"RK").text("RK");
							$("#flatType").append(option);
							var option = $('<option/>');
							option.attr('value',"1 BHK").text("1 BHK");
							$("#flatType").append(option);
							var option = $('<option/>');
							option.attr('value',"2 BHK").text("2 BHK");
							$("#flatType").append(option);
							var option = $('<option/>');
							option.attr('value',"3 BHK").text("3 BHK");
							$("#flatType").append(option);
							var option = $('<option/>');
							option.attr('value',"4 BHK").text("4 BHK");
							$("#flatType").append(option);
						
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].floortypeName).text(result[i].floortypeName);
							    $("#floortypeName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
} 

function getFlatNumberList()
{
	
	document.enquiryform.flatFacing.value="";
	document.enquiryform.flatareainSqFt.value="";
	document.enquiryform.flatbasicCost.value="";
	 $("#flatNumber").empty();
	 var flatType = $('#flatType').val();
	 var floortypeName = $('#floortypeName').val();
		var wingName = $('#wingName').val();
		var buildingName = $('#buildingName').val();
		var projectName = $('#projectName').val();
	$.ajax({

		url : '${pageContext.request.contextPath}/getFlatNumberList',
		type : 'Post',
		data : { flatType : flatType ,floortypeName: floortypeName ,wingName : wingName, buildingName : buildingName, projectName : projectName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Flat Number-");
							$("#flatNumber").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].flatNumber).text(result[i].flatNumber);
							    $("#flatNumber").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get Flat Number List   

function getflattype()
{	
	document.enquiryform.flatFacing.value="";
document.enquiryform.flatareainSqFt.value="";
document.enquiryform.flatbasicCost.value="";
$("#flatNumber").empty();
$("#flatType").empty();

var option = $('<option/>');
option.attr('value',"Default").text("-Select Flat Number-");
$("#flatNumber").append(option);

var option = $('<option/>');
option.attr('value',"Default").text("-Select Flat Type-");
$("#flatType").append(option);
var option = $('<option/>');
option.attr('value',"RK").text("RK");
$("#flatType").append(option);
var option = $('<option/>');
option.attr('value',"1 BHK").text("1 BHK");
$("#flatType").append(option);
var option = $('<option/>');
option.attr('value',"2 BHK").text("2 BHK");
$("#flatType").append(option);
var option = $('<option/>');
option.attr('value',"3 BHK").text("3 BHK");
$("#flatType").append(option);
var option = $('<option/>');
option.attr('value',"4 BHK").text("4 BHK");
$("#flatType").append(option);

}

function getBankCode()
{
	
	 $("#branchName").empty();
	 $("#bankifscCode").empty();
	 var bankName = $('#bankName').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getBankCode',
		type : 'Post',
		data : { bankName : bankName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							
								$('#branchName').val(result[0].branchName);
								$('#bankifscCode').val(result[0].bankifscCode);
							
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
}

function getpinCode()
{

	 $("#enqPincode").empty();
	 var locationareaName = $('#locationareaName').val();
	 var cityName = $('#cityName').val();
	 var stateName = $('#stateName').val();
	 var countryName = $('#countryName').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getallAreaList',
		type : 'Post',
		data : { locationareaName : locationareaName, cityName : cityName, stateName : stateName, countryName : countryName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							
							for(var i=0;i<result.length;i++)
							{
							 $('#enqPincode').val(result[i].pinCode);
								
							 } 
						
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}

function getFlatFacing()
{
	document.enquiryform.flatFacing.value="";
	document.enquiryform.flatareainSqFt.value="";
	document.enquiryform.flatbasicCost.value="";
	
	var flatNumber = $('#flatNumber').val();
	var wingName = $('#wingName').val();
	var buildingName = $('#buildingName').val();
	var projectName = $('#projectName').val();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getFlatDetail',
		type : 'Post',
		data : {flatNumber : flatNumber , wingName : wingName, buildingName : buildingName, projectName : projectName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
						  for(var i=0;i<result.length;i++)
						  {
							  $('#flatFacing').val(result[i].flatfacingName);
							  $('#flatareainSqFt').val(result[i].flatArea);
							  $('#flatbasicCost').val(result[i].flatbasicCost);
						  }
							
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get Flat Number List

// to check Unique mobile number
function getuniquemobilenumber()
{
	$('#enqmobileNumber1Span').html('');
	 var enqmobileNumber1 = $('#enqmobileNumber1').val();
	$.ajax({

		url : '${pageContext.request.contextPath}/getenquirymobilenumberList',
		type : 'Post',
		data : { enqmobileNumber1 : enqmobileNumber1},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
						
							for(var i=0;i<result.length;i++)
							{
								
								if(result[i].enqmobileNumber1==enqmobileNumber1)
									{
									  document.enquiryform.enqmobileNumber1.value="";
									  document.enquiryform.enqmobileNumber1.focus();
									  $('#enqmobileNumber1Span').html('This mobile number is already exist..!');
									}
								
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}

$(function () 
 {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    
    //Date picker
    $('#datepicker1').datepicker({
      autoclose: true
    })
 $('#datepicker').datepicker({
      autoclose: true
    })
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>

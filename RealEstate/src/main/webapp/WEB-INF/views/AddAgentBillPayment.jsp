<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Agent Payment</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


  <!-- Google Font -->
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#enquirydbStatusSpan').delay(1000).fadeOut();
      });
    </script>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

  <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Agent Payment:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Account</a></li>
        <li class="active">Agent Payment </li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="agentbillpaymentform" action="${pageContext.request.contextPath}/AddAgentBillPayment" onSubmit="return validate()" method="post">
    <section class="content">

 <div class="box box-default">
  <div class="box box-danger">   
          
     <input type="hidden" id="agentPaymentId" name="agentPaymentId" value="${agentPaymentId}">
     <input type="hidden" id="bookingId" name="bookingId" value="${bookingDetails[0].bookingId}">
     <input type="hidden" id="agentId" name="agentId" value="${agentDetails[0].agentId}">
     <input type="hidden" id="agentAmount" name="agentAmount" value="${agentAmount}">
     <input type="hidden" id="agentPaidAmount" name="agentPaidAmount" value="${agentPaidAmount}">
           
           
	<div class="box-body">
       <div class="row">
        
           <div class="col-xs-12">
               <table>
                  <tr>
                   <td style="width:10px"></td>
                   <td style="width:130px"> <h4><label>Booking Id : </label></h4></td>
                   <td><h4>${bookingDetails[0].bookingId}</h4></td>
				
					<td style="width:30px"></td>
					<td style="width:150px"> <h4><label>Customer Name : </label></h4></td>
					<td style="width:150px"><h4>${bookingDetails[0].bookingfirstname} ${bookingDetails[0].bookingmiddlename} ${bookingDetails[0].bookinglastname}</h4></td>
					
                   <td style="width:10px"></td>
                   <td style="width:130px"> <h4><label>Basic Amount : </label></h4></td>
                   <td><h4>${bookingDetails[0].flatbasicCost}</h4></td>
                  </tr>
             </table>
             <table>    
                  <tr>
                   <td style="width:10px"></td>
                   <td style="width:150px"> <h4><label>Agent Firm Name : </label></h4></td>
                   <td><h4>${agentDetails[0].agentfirmName}</h4></td>
				
					<td style="width:30px"></td>
					<td style="width:130px"> <h4><label>Agent Per : </label></h4></td>
					<td><h4>${agentDetails[0].brokeragePercentage}</h4></td>
					
					<td style="width:30px"></td>
					<td style="width:180px"> <h4><label>Agent Total Amount : </label></h4></td>
					<td><h4>${agentAmount}</h4></td>
					
					<td style="width:30px"></td>
					<td style="width:150px"> <h4><label>Paid Amount : </label></h4></td>
					<td><h4>${agentPaidAmount}</h4></td>
                  </tr> 
              </table>
                  
          </div> 
                        
		  <div class="col-md-12">
		  
		  <div class="box-body">
		    &nbsp&nbsp&nbsp&nbsp<h4>Company Bank Details In Which Amount Get Credited/Deposited</h4>
            <div class="row">
                                       
             <div class="col-md-2">
                <label>Company Account Number</label> <label class="text-red">* </label>
                <select class="form-control" id="companyBankId" name="companyBankId" onchange="getBankName()">
                  <option value="Default">-Select Bank A/c No.-</option>
                  <c:forEach items="${companyBankDetails}" var="companyBankDetails" varStatus="loopStatus">
                    <option value="${companyBankDetails.companyBankId}">${companyBankDetails.companyBankacno}</option>
                  </c:forEach>
                </select>
                <span id="companyBankIdSpan" style="color:#FF0000"></span>
              </div>
              
                 
              <div class="col-md-2">
                  <label>Bank Name</label> <label class="text-red">* </label>
                    <input  type="text" class="form-control pull-right" id="companyBankname" name="companyBankname" readonly>
                  <span id="companyBanknameSpan" style="color:#FF0000"></span>
              </div>
                      
              <div class="col-md-2">
                  <label>Amount</label> <label class="text-red">* </label>
                  <input  type="text" class="form-control pull-right" id="paymentAmount" name="paymentAmount" placeholder="Enter Amount" onchange="CheckAmount(this.value)"/>
                  <span id="paymentAmountSpan" style="color:#FF0000"></span>
              </div>
              
	            <div class="col-xs-2">
                    <label>Payment Type</label> <label class="text-red">*</label>
                  	<select class="form-control" id="paymentMode" name="paymentMode">
				  	<option selected="selected" value="Default">-Select Type-</option>
				  	<option value="Cash">Cash</option>
				  	<option  value="Cheque">Cheque</option>
				  	<option value="DD">DD</option>
				  	<option value="RTGS">RTGS</option>	
				  	<option value="NEFT">NEFT</option>
				  	<option value="IMPS">IMPS</option>	
                  	</select>
                   <span id="paymentModeSpan" style="color:#FF0000"></span>
               </div>

		       <div class="col-md-2">
	               <label for="refNumber">REF No. </label><label class="text-red">* </label>
	    		   <input type="text" class="form-control" name="refNumber" id="refNumber" placeholder="Enter REF No.">                  
			       <span id="refNumberSpan" style="color:#FF0000"></span>
		       </div>
		       
		      <div class="col-md-2">
	               <label for="narration">Narration</label><label class="text-red">* </label>
	    		   <input type="text" class="form-control" name="narration" id="narration" placeholder="Enter Narration" style="text-transform: capitalize;">                  
			       <span id="narrationSpan" style="color:#FF0000"></span>
		       </div>
		
            </div>
          </div> 
          
             
     <div class="box-body">
       <div class="row">
            
         <div class="col-md-3">
          <label>Chart of Account</label> <label class="text-red">* </label>
             <select class="form-control" id="chartaccountId" name="chartaccountId" onchange="getSunChartOfAccount()">
             <option value="Default">-Select Chart of Account-</option>
             <c:forEach items="${chartofaccountList}" var="chartofaccountList" varStatus="loopStatus">
             <option value="${chartofaccountList.chartaccountId}">${chartofaccountList.chartaccountName}</option>
             </c:forEach>
             </select>
              <span id="chartaccountNameSpan" style="color:#FF0000"></span>
         </div>
             
         <div class="col-md-3"> 
          <label>Sub Chart of Account</label><label class="text-red">* </label>
             <select class="form-control" id="subchartaccountId" name="subchartaccountId">
             <option value="Default">-Sub Chart of Account-</option>
           
             </select>
              <span id="subchartaccountIdSpan" style="color:#FF0000"></span>
         </div>
            					
       </div>
     </div>
	 
          	  <input type="hidden" id="creationDate" name="creationDate" value="">
			  <input type="hidden" id="updateDate" name="updateDate" value="">
			  <input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>">    
			  
		  </div>
	  </div>
	</div>	
      
	<div class="box-body">
      <div class="row">
        <div class="col-xs-1">
        </div>
        <div class="col-xs-4">
            <div class="col-xs-2">	<a href="AddAgentPayment"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
			</div> 
		</div>
			    
	   		    <div class="col-xs-2">
	                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
			    </div>
			    
				<div class="col-xs-3">
		  			<button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
			    </div>
			    	     
       </div>
	 </div>
    </div>    
</div>
</section>
</form>
</div>

   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>

<script>

function getSunChartOfAccount()
{
	var chartaccountId  = $('#chartaccountId').val();
	 $("#subchartaccountId").empty();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getSubChartAccountList',
		type : 'Post',
		data : { chartaccountId : chartaccountId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Sub chart account-");
							$("#subchartaccountId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].subchartaccountId).text(result[i].subchartaccountName);
							    $("#subchartaccountId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
	
}

function CheckAmount()
{

	$('#paymentAmountSpan').html('');
	$('#paymentModeSpan').html('');
	var agentAmount  = Number($('#agentAmount').val());
	var agentPaidAmount  = Number($('#agentPaidAmount').val());
	var paymentAmount  = Number($('#paymentAmount').val());
	
	var checkAmount=agentPaidAmount+paymentAmount;
	if(agentAmount<checkAmount)
		{
		$('#paymentAmountSpan').html('Please, enter less than total amount..!');
		document.agentbillpaymentform.paymentAmount.value="";
		document.agentbillpaymentform.paymentAmount.focus();
		return false;
		} 
	
	
}

function getBankName()
{
	var companyBankId  = $('#companyBankId').val();
	$.ajax({

		url : '${pageContext.request.contextPath}/getCompanyAccountBankName',
		type : 'Post',
		data : { companyBankId : companyBankId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							for(var i=0;i<result.length;i++)
							{
								 $('#companyBankname').val(result[i].companyBankname);
							}
						} 
						else
						{
						  alert("failure111");
						}
					}
		});
	
}

function clearall()
{
	$('#companyBankIdSpan').html('');
	$('#paymentAmountSpan').html('');
	$('#paymentModeSpan').html('');
	$('#refNumberSpan').html('');
	$('#narrationSpan').html('');
	$('#chartaccountIdSpan').html('');
	$('#subchartaccountIdSpan').html('');
}

function validate()
{
	clearall();

	if(document.agentbillpaymentform.companyBankId.value=="Default")
	{
		$('#companyBankIdSpan').html('Please, select company A/c No..!');
		document.agentbillpaymentform.companyBankId.focus();
		return false;
	}

	if(document.agentbillpaymentform.paymentAmount.value=="")
	{
		 $('#paymentAmountSpan').html('Please, enter payment amount..!');
		document.agentbillpaymentform.paymentAmount.focus();
		return false;
	}
	else if(document.agentbillpaymentform.paymentAmount.value.match(/^[\s]+$/))
	{
		$('#paymentAmountSpan').html('Please, enter valid payment amount..!');
		document.agentbillpaymentform.paymentAmount.value="";
		document.agentbillpaymentform.paymentAmount.focus();
		return false; 	
	}
	else if(!document.agentbillpaymentform.paymentAmount.value.match(/^[0-9]+$/))
	{
		$('#paymentAmountSpan').html('Please, enter valid payment amount..!');
		//document.agentbillpaymentform.employeeBankacno.value="";
		document.agentbillpaymentform.paymentAmount.focus();
		return false;
	}
	

	if(document.agentbillpaymentform.paymentMode.value=="Default")
	{
		$('#paymentModeSpan').html('Please, select payment mode..!');
		document.agentbillpaymentform.paymentMode.focus();
		return false;
	}
	
		
	if(document.agentbillpaymentform.refNumber.value=="")
	{
		 $('#refNumberSpan').html('Please, enter ref. no..!');
		document.agentbillpaymentform.refNumber.focus();
		return false;
	}
	else if(document.agentbillpaymentform.refNumber.value.match(/^[\s]+$/))
	{
		$('#refNumberSpan').html('Please, enter valid ref. no..!');
		document.agentbillpaymentform.refNumber.value="";
		document.agentbillpaymentform.refNumber.focus();
		return false; 	
	}
	
	if(document.agentbillpaymentform.narration.value=="")
	{
		 $('#narrationSpan').html('Please, enter narration..!');
		document.agentbillpaymentform.narration.focus();
		return false;
	}
	else if(document.agentbillpaymentform.narration.value.match(/^[\s]+$/))
	{
		$('#narrationSpan').html('Please, enter valid narration..!');
		document.agentbillpaymentform.narration.value="";
		document.agentbillpaymentform.narration.focus();
		return false; 	
	}
	if(document.agentbillpaymentform.chartaccountId.value=="Default")
	{
		$('#chartaccountNameSpan').html('Please, select chart account Name..!');
		document.agentbillpaymentform.chartaccountId.focus();
		return false;
	}
	
	if(document.agentbillpaymentform.subchartaccountId.value=="Default")
	{
		$('#subchartaccountIdSpan').html('Please, select sub chart account Name..!');
		document.agentbillpaymentform.subchartaccountId.focus();
		return false;
	}
	
}

function init()
{
	clearall();
	
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
}



</script>
</body>
</html>

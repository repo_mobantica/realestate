<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate |Payment Scheduler Master</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">
<!-- Google Font -->
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>

<div class="wrapper">

    <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
  <%@ include file="menu.jsp" %>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Payment Scheduler Master :
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Project Engineering</a></li>
        <li class="active"> Payment Scheduler</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="paymentschedulerform" action="${pageContext.request.contextPath}/PaymentSchedulerForProjectEnggMaster" onSubmit="return validate()" method="post">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <div class="box-body">
			<div class="box-body">
			
			  <div class="row">
                
                <div class="col-md-3">
	                  <label>Project</label><label class="text-red">* </label>
	                  <select class="form-control" name="projectName" id="projectName" onchange="getBuldingList(this.value)">
	                  	 <option selected="" value="Default">-Select Project-</option>
	                  	   <c:forEach var="projectList" items="${projectList}">
	                  	<option value="${projectList.projectName}">${projectList.projectName}</option>
	                     </c:forEach>
					  </select>
                  </div>
                  
                  <div class="col-xs-3">
				      <label>Project Building Name  </label> <label class="text-red">* </label>
	                  <select class="form-control" name="buildingName" id="buildingName" onchange="getWingNameList(this.value)">
					  		<option selected="" value="Default">-Select Project Building-</option>
	                     <c:forEach var="projectbuildingList" items="${projectbuildingList}">
	                    	<option value="${projectbuildingList.buildingName}">${projectbuildingList.buildingName}</option>
					     </c:forEach>
	                  </select>
			     </div> 
			     
                  <div class="col-xs-3">
			     	 <label>Wing </label> <label class="text-red">* </label>
              		 <select class="form-control" name="wingName" id="wingName" onchange="getPaymentScheduleList(this.value)">
				 	 	<option selected="" value="Default">-Select Wing Name-</option>
                       <c:forEach var="projectwingList" items="${projectwingList}">
                    	<option value="${projectwingList.wingName}">${projectwingList.wingName}</option>
				       </c:forEach>
                  	 </select>
                 </div>
               </div>
			
			  </div>
			  
			  <div class="box-body">
	              <div class="row">
					<div class="col-xs-3">
				 		&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
				 		<a href="PaymentSchedulerForProjectEnggMaster">  <button type="button" class="btn btn-default" value="reset" style="width:90px"> Reset</button></a>
	                </div> 
	              
				  </div>
			  </div>
        </div>
      </div>
      
      
    <div class="box box-default">
		<div  class="panel box box-danger"></div>
        	 <div class="box-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="paymentschedulerTable">
                  <thead>
                  <tr bgcolor="#4682B4">
                     <th style="width:150px">Project Name </th>
                       <th style="width:150px">Building Name </th>
                         <th style="width:150px">Wing Name </th>
	                    <th style="width:150px">Installment Number</th>
	                    <th style="width:300px">Payment Description</th>
	                    <th style="width:50px">Percentage(%)</th>
	                    <th style="width:150px">Slab Status</th>
	                    <th style="width:50px">Action</th>
                  </tr>
                  </thead>
                  <tbody >
                   <c:forEach items="${paymentschedulerList}" var="paymentschedulerList" varStatus="loopStatus">
                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                        <td>${paymentschedulerList.projectId}</td>
                        <td>${paymentschedulerList.buildingId}</td>
                        <td>${paymentschedulerList.wingId}</td>
                        <td>${paymentschedulerList.installmentNumber}</td>
                        <td>${paymentschedulerList.paymentDecription}</td>
                        <td>${paymentschedulerList.percentage}</td>
                        <td>${paymentschedulerList.slabStatus}</td>
                        <td><a href="${pageContext.request.contextPath}/ViewPaymentScheduler?paymentschedulerId=${paymentschedulerList.paymentschedulerId}" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>
                        </tr>
				   </c:forEach>
                 </tbody>
                </table>
              </div>
            </div>
	 </div>
     
       </section>
	</form>
    <!-- /.content -->
    
  </div>
       <%@ include file="footer.jsp" %>
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script>

function getBuldingList()
{
	 $("#buildingName").empty();
	 var projectName = $('#projectName').val();

	 $.ajax({

		 url : '${pageContext.request.contextPath}/getBuildingList',
		type : 'Post',
		data : { projectName : projectName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Building Name-");
							$("#buildingName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].buildingName).text(result[i].buildingName);
							    $("#buildingName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	 

	 
		 $("#paymentschedulerTable tr").detach();
   	    var projectName = $('#projectName').val();

		 $.ajax({

			url : '${pageContext.request.contextPath}/getProjectWisePaymentSchedule',
			type : 'Post',
			data : { projectName : projectName},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{ 
										$('#paymentschedulerTable').append('<tr style="background-color: #4682B4;"><th>Project Name</th><th>Building Name</th> <th>Wing Name</th><th>Installment Number</th> <th>Payment Description</th> <th>Percentage</th> <th>Slab Status</th> <th>Action</th>');
							
								for(var i=0;i<result.length;i++)
								{ 
									if(i%2==0)
									{
										var id = result[i].paymentschedulerId;
										$('#paymentschedulerTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].projectName+'</td><td>'+result[i].buildingName+'</td><td>'+result[i].wingName+'</td><td>'+result[i].installmentNumber+'</td><td>'+result[i].paymentDecription+'</td><td>'+result[i].percentage+'</td><td>'+result[i].slabStatus+'</td><td><a href="${pageContext.request.contextPath}/ViewPaymentScheduler?paymentschedulerId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
									}
									else
									{
										var id = result[i].paymentschedulerId;
										$('#paymentschedulerTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].projectName+'</td><td>'+result[i].buildingName+'</td><td>'+result[i].wingName+'</td><td>'+result[i].installmentNumber+'</td><td>'+result[i].paymentDecription+'</td><td>'+result[i].percentage+'</td><td>'+result[i].slabStatus+'</td><td><a href="${pageContext.request.contextPath}/ViewPaymentScheduler?paymentschedulerId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
									}
								
								 } 
							} 
							else
							{
								alert("failure111");
							}

						}
			});
		
	
}//end of get Building List


function getWingNameList()
{
	 $("#wingName").empty();
	 var buildingName = $('#buildingName').val();
	 var projectName = $('#projectName').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getprojectwingList',
		type : 'Post',
		data : { buildingName : buildingName, projectName : projectName },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Wing Name-");
							$("#wingName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].wingName).text(result[i].wingName);
							    $("#wingName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	


		 $("#paymentschedulerTable tr").detach();
		 var buildingName = $('#buildingName').val();
		 var projectName = $('#projectName').val();

		 $.ajax({

			url : '${pageContext.request.contextPath}/getBuildingWisePaymentSchedule',
			type : 'Post',
			data : { buildingName : buildingName, projectName : projectName},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{ 
										$('#paymentschedulerTable').append('<tr style="background-color: #4682B4;"><th>Project Name</th><th>Building Name</th> <th>Wing Name</th><th>Installment Number</th> <th>Payment Description</th> <th>Percentage</th> <th>Slab Status</th> <th>Action</th>');
							
								for(var i=0;i<result.length;i++)
								{ 
									if(i%2==0)
									{
										var id = result[i].paymentschedulerId;
										$('#paymentschedulerTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].projectName+'</td><td>'+result[i].buildingName+'</td><td>'+result[i].wingName+'</td><td>'+result[i].installmentNumber+'</td><td>'+result[i].paymentDecription+'</td><td>'+result[i].percentage+'</td><td>'+result[i].slabStatus+'</td><td><a href="${pageContext.request.contextPath}/ViewPaymentScheduler?paymentschedulerId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
									}
									else
									{
										var id = result[i].paymentschedulerId;
										$('#paymentschedulerTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].projectName+'</td><td>'+result[i].buildingName+'</td><td>'+result[i].wingName+'</td><td>'+result[i].installmentNumber+'</td><td>'+result[i].paymentDecription+'</td><td>'+result[i].percentage+'</td><td>'+result[i].slabStatus+'</td><td><a href="${pageContext.request.contextPath}/ViewPaymentScheduler?paymentschedulerId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
									}
								
								 } 
							} 
							else
							{
								alert("failure111");
							}

						}
			}); 
		
}//end of get Wing List


function getPaymentScheduleList()
{
		 $("#paymentschedulerTable tr").detach();
		 
		 var projectName = $('#projectName').val();
		 var buildingName = $('#buildingName').val();
		 var wingName = $('#wingName').val();
		 
		 $.ajax({

			url : '${pageContext.request.contextPath}/getWingWisePaymentSchedule',
			type : 'Post',
			data : { wingName : wingName, buildingName : buildingName, projectName : projectName},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{ 
								$('#paymentschedulerTable').append('<tr style="background-color: #4682B4;"><th>Project Name</th><th>Building Name</th> <th>Wing Name</th><th>Installment Number</th> <th>Payment Description</th> <th>Percentage</th> <th>Slab Status</th> <th>Action</th>');
							
								for(var i=0;i<result.length;i++)
								{ 
									if(i%2==0)
									{
										var id = result[i].paymentschedulerId;
										$('#paymentschedulerTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].projectName+'</td><td>'+result[i].buildingName+'</td><td>'+result[i].wingName+'</td><td>'+result[i].installmentNumber+'</td><td>'+result[i].paymentDecription+'</td><td>'+result[i].percentage+'</td><td>'+result[i].slabStatus+'</td><td><a href="${pageContext.request.contextPath}/ViewPaymentScheduler?paymentschedulerId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
									}
									else
									{
										var id = result[i].paymentschedulerId;
										$('#paymentschedulerTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].projectName+'</td><td>'+result[i].buildingName+'</td><td>'+result[i].wingName+'</td><td>'+result[i].installmentNumber+'</td><td>'+result[i].paymentDecription+'</td><td>'+result[i].percentage+'</td><td>'+result[i].slabStatus+'</td><td><a href="${pageContext.request.contextPath}/ViewPaymentScheduler?paymentschedulerId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
									}
								
								 } 
							} 
							else
							{
								alert("failure111");
							}

						}
			}); 
		
}

$(function () {
    $('#paymentschedulerTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>

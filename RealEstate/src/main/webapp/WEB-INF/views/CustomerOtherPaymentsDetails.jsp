<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Add Extra Payment</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
   <style type="text/css">
   	tr.odd {background-color: #CCE5FF}
	tr.even {background-color: #F0F8FF}
   </style>
    
 	<script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
    </script>

  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
       
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>

<div class="wrapper">

   <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Extra Payment :
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Sale</a></li>
        <li class="active">Add Extra Payment</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="customerreceiptform" action="${pageContext.request.contextPath}/CustomerOtherPaymentsDetails" onSubmit="return validate()" method="post">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
      <div class="box box-danger"></div>
        
         <div class="box-body" id="paymentStatus" hidden="hidden">
           <div class="callout callout-success">
                <h4><i class="icon fa fa-check"></i>Amount payed by  ${bookingDetails.bookingfirstname} ${bookingDetails.bookingmiddlename} ${bookingDetails.bookinglastname}</h4>
           </div>
	     </div>
  
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
                  <span id="statusSpan" style="color:#FF0000"></span>
                   
                  <div class="box-body">
                     <div class="row">
                     
                     <div class="col-xs-4">
                    	<div class="input-group">
                 		<h4><span class="fa fa-star"></span> 
                   		 <label>Booking Id : ${bookingId}</label>
                   		 <input type="hidden" id="bookingId" name="bookingId" value="${bookingId}">
                   		</h4> 
       		 		  </div>
			        </div>   
			           
                     <div class="col-xs-5">
                    	<div class="input-group">
                 		<h4><span class="fa fa-user"></span> 
                   		 <label>Customer Name : ${customerName}</label>
                     	</h4> 
       		 		  </div>
			        </div>   
			            
                 
			                  
              		 </div>
            	  </div>
            	 
            	  
        <div class="box-body">
          <div class="row">
               
            <div class="col-xs-9">
.
                  <div class="col-xs-12">
               
                 <div class="table-responsive">
                 <table class="table table-bordered ">
				  <thead>
                   <tr bgcolor=#AAB7B8>
                     <th style="width:150px; text-align: center;">Type</th>
   					 <th style="width:100px; text-align: center;">Maint Amt</th>
				     <th style="width:100px; text-align: center;">Handling Charge</th>
   					 <th style="width:100px; text-align: center;">Extra Charges</th>
                   </tr>
                 </thead>
                 <tbody>
                 
				  <tr>
				    <td>Total Amt</td>
				    <td style="text-align: right;"> <label id="totalMaintCost">${totalMaintCost}</label> </td>
				    <td style="text-align: right;"> <label id="totalHandlingCharge">${totalHandlingCharge}</label> </td>
				    <td style="text-align: right;"> <label id="totalExtraCharges">${totalExtraCharges}</label> </td>
				  </tr>
				 
				  <tr>
				    <td>Paid Amt</td>
				    <td style="text-align: right;"> <label id="totalPaidMaintainanceAmt">${customerPayment[0].totalPaidMaintainanceAmt}</label> </td>
				    <td style="text-align: right;"> <label id="totalPaidHandlingCharges">${customerPayment[0].totalPaidHandlingCharges}</label> </td>
				    <td style="text-align: right;"> <label id="totalPaidExtraCharges">${customerPayment[0].totalPaidExtraCharges}</label> </td>
				  </tr>
				 
				  <tr>
				    <td>Remaining Amt</td>
				    <td style="text-align: right;"> <label id="reamaingMaintainance">${reamaingMaintainance}</label> </td>
				    <td style="text-align: right;"> <label id="reamaingHandling">${reamaingHandling}</label> </td>
				    <td style="text-align: right;"> <label id="reamaingExtraCharges">${reamaingExtraCharges}</label> </td>
				  </tr>
				 
			
				  <tr>
				    <td>Add More Payment</td>
				    <td> <input type="text" class="form-control" style="text-align: right;" id="maintainanceAmt" placeholder="Maintainance Amount " name="maintainanceAmt" onchange="return checkAmount()"></td>
				    <td> <input type="text" class="form-control" style="text-align: right;" id="handlingChargesAmt" placeholder="Handling Charge Amount " name="handlingChargesAmt" onchange="return checkAmount()"> </td>
				    <td> <input type="text" class="form-control" style="text-align: right;" id="extraChargesAmt" placeholder="Extra Charge Amount " name="extraChargesAmt" onchange="return checkAmount()"> </td>
				  </tr>
			
				</tbody>
			 </table>
				 
			</div>
			
			
			</div>
          </div> 
          <div class="col-xs-4">
          </div>
          <div class="col-xs-4">
            <span id="buttonSpan" style="color:#FF0000"></span>
          </div>
       </div>
       </div>
       
       <div class="box-body">
         <div class="row">
         	<div class="col-md-1">
         	</div>
            
		  	   <div class="col-md-2">
	               <label for="bankName">Bank Name </label><label class="text-red">* </label>
	    		   <input type="text" class="form-control" name="bankName" id="bankName" placeholder="Enter bank Name">                  
			       <span id="bankNameSpan" style="color:#FF0000"></span>
		       </div>
		       
		  	   <div class="col-md-2">
	               <label for="branchName">Branch Name </label><label class="text-red">* </label>
	    		   <input type="text" class="form-control" name="branchName" id="branchName" placeholder="Enter branch Name">                  
			       <span id="branchNameSpan" style="color:#FF0000"></span>
		       </div>
		       
		       <div class="col-xs-2">
                    <label>Payment Type</label> <label class="text-red">*</label>
                  	<select class="form-control" id="paymentMode" name="paymentMode">
				  	<option selected="selected" value="Default">-Select Type-</option>
				  	<option value="Cash">Cash</option>
				  	<option  value="Cheque">Cheque</option>
				  	<option value="DD">DD</option>
				  	<option value="RTGS">RTGS</option>	
				  	<option value="NEFT">NEFT</option>
				  	<option value="IMPS">IMPS</option>	
                  	</select>
                   <span id="paymentModeSpan" style="color:#FF0000"></span>
               </div>
        
		       <div class="col-md-2">
	               <label for="chequeNumber">REF No. </label><label class="text-red">* </label>
	    		   <input type="text" class="form-control" name="chequeNumber" id="chequeNumber" placeholder="Enter REF No.">                  
			       <span id="chequeNumberSpan" style="color:#FF0000"></span>
		       </div>
		       
		       <div class="col-md-2">
	               <label for="narration">Narration</label><label class="text-red">* </label>
	    		   <input type="text" class="form-control" name="narration" id="narration" placeholder="Enter Narration">                  
			       <span id="narrationSpan" style="color:#FF0000"></span>
		       </div>
		
            </div>
        </div>
     
     
    </div>
  </div>
</div>
         
         
<div class="box-body">
  <div class="row">
    <div class="col-md-12">
      <div class="box-body">
       <div class="row">
      
       </div>
      </div>
          
          <div class="box-body">
           <div class="row">
         
                 </br>
                 
                 <div class="col-xs-5">
                 <div class="col-xs-4">
                   <a href="AllCustomerExtraChargesList"><button type="button" class="btn btn-primary" value="Back" style="width:90px">Back</button></a>
                 </div>
                 </div>
				 
				 <div class="col-xs-3">
                   <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
			     </div>
				 
				 <div class="col-xs-3" id="submitButton">
				   <button type="submit" class="btn btn-info pull-right" name="submit" id="submit">Submit</button>
			     </div> 
			     
				<!--  <div class="col-xs-3" hidden="hidden">
			  		<button type="submit" class="btn btn-info pull-right" name="submit" id="submit">Submit</button>
				 </div>  -->
				 
            </div>
        </div>  	 
    </div>
            
            
            
          </div>
        </div>
              <input type="hidden" id="creationDate" name="creationDate" value="">
			  <input type="hidden" id="updateDate" name="updateDate" value="">
			  <input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>">                             
	  </div>	
      </section>
	</form>
 </div>
    <%@ include file="footer.jsp" %>
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>

<script>


function validate()
{
	$('#buttonSpan').html('');
	$('#paymentModeSpan').html('');
	$('#bankNameSpan').html('');
	$('#branchNameSpan').html('');
	$('#chequeNumberSpan').html('');
	$('#narrationSpan').html('');
	
	checkAmount();
	
	if(document.customerreceiptform.maintainanceAmt.value.length==0)
	{
		if(document.customerreceiptform.handlingChargesAmt.value.length==0)
		{
	  	   if(document.customerreceiptform.extraChargesAmt.value.length==0)
		   {
				$('#buttonSpan').html('Please, add any one entry any amount..!');
				return false;
		   }
		}
	 }
	
	if(document.customerreceiptform.maintainanceAmt.value.length!=0)
    {
		if(!document.customerreceiptform.maintainanceAmt.value.match(/^[0-9]+$/))
		{
			$('#buttonSpan').html('Please, enter correct maintainance amount..!');
			document.customerreceiptform.maintainanceAmt.focus();
			document.customerreceiptform.maintainanceAmt.value="";
			return false;
		} 		
    }
	
	if(document.customerreceiptform.handlingChargesAmt.value.length!=0)
    {
		if(!document.customerreceiptform.handlingChargesAmt.value.match(/^[0-9]+$/))
		{
			$('#buttonSpan').html('Please, enter correct handling charges amount..!');
			document.customerreceiptform.handlingChargesAmt.focus();
			document.customerreceiptform.handlingChargesAmt.value="";
			return false;
		} 		
    }
	
	if(document.customerreceiptform.extraChargesAmt.value.length!=0)
    {
		if(!document.customerreceiptform.extraChargesAmt.value.match(/^[0-9]+$/))
		{
			$('#buttonSpan').html('Please, enter correct extra charges amount..!');
			document.customerreceiptform.extraChargesAmt.focus();
			document.customerreceiptform.extraChargesAmt.value="";
			return false;
		} 		
    }
	
	if(document.customerreceiptform.maintainanceAmt.value.length==0 || document.customerreceiptform.handlingChargesAmt.value.length==0 || document.customerreceiptform.extraChargesAmt.value.length==0)
	{
		if(document.customerreceiptform.maintainanceAmt.value.length==0)
		{
		  $('#maintainanceAmt').val("0");
		}
		
		if(document.customerreceiptform.handlingChargesAmt.value.length==0)
		{
		  $('#handlingChargesAmt').val("0");
		}
		
		if(document.customerreceiptform.extraChargesAmt.value.length==0)
		{
		  $('#extraChargesAmt').val("0");
		}
	}
	
	
	if(document.customerreceiptform.paymentMode.value == "Default")
	{
		$('#paymentModeSpan').html('Please, select payment mode..!');
		return false;
	}
	
	/* if(document.customerreceiptform.paymentMode.value == "" && document.customerreceiptform.paymentMode[1].checked == false && document.customerreceiptform.paymentMode[2].checked == false && document.customerreceiptform.paymentMode[3].checked == false)
	{
		$('#paymentModeSpan').html('Please, select payment mode..!');
		return false;
	} */
	
	if(document.customerreceiptform.paymentMode.value == "Cash")
	{
		document.customerreceiptform.bankName.value="N.A.";
		document.customerreceiptform.branchName.value="N.A.";
		document.customerreceiptform.chequeNumber.value="N.A.";
	}
	
	if(document.customerreceiptform.paymentMode.value == "Cheque" || document.customerreceiptform.paymentMode.value == "DD" || document.customerreceiptform.paymentMode.value == "RTGS" || document.customerreceiptform.paymentMode.value == "NEFT" || document.customerreceiptform.paymentMode.value == "IMPS") 
	{
		if(document.customerreceiptform.bankName.value == "")
		{
			$('#bankNameSpan').html('Please, enter bank name..!');
			document.customerreceiptform.bankName.value = "";
			document.customerreceiptform.bankName.focus();
			return false;
		}
		
		//validation for branch name
		if(document.customerreceiptform.branchName.value == "")
		{
			$('#branchNameSpan').html('Please, enter branch name..!');
			document.customerreceiptform.branchName.value = "";
			document.customerreceiptform.branchName.focus();
			return false;
		}
		
		if(document.customerreceiptform.chequeNumber.value == "")
		{
			$('#chequeNumberSpan').html('Please, enter Cheque/DD/RTGS/NEFT/IMPS Number..!');
			return false;
		}
	 }
	 
	 if(document.customerreceiptform.narration.value == "")
	 {
		$('#narrationSpan').html('Please, enter narration..!');
		return false;
	 }
}


function init()
{
	
	 var n = new Date();
	 var y = n.getFullYear();
	 var m = n.getMonth()+1;
	 var d = n.getDate();
	 
	 document.getElementById("creationDate").value = d + "/"+ m + "/" + y;
	 
	 document.getElementById("updateDate").value = d + "/"+ m + "/" + y;
	
	 document.customerreceiptform.maintainanceAmt.focus();
	 
	 //total payble amount till date
	 var totalMaintCost 	 = Number($("#totalMaintCost").text()) ;
	 var totalHandlingCharge = Number($("#totalHandlingCharge").text()) ;
	 var totalExtraCharges 	 = Number($("#totalExtraCharges").text()) ;
	 
	 //total paid till date
	 var totalPaidMaintainanceAmt = Number($("#totalPaidMaintainanceAmt").text()) ;
	 var totalPaidHandlingCharges = Number($("#totalPaidHandlingCharges").text()) ;
	 var totalPaidExtraCharges    = Number($("#totalPaidExtraCharges").text()) ;
	
	 //remaining total till date
	 var reamaingMaintainance = Number($("#reamaingMaintainance").text()) ;
	 var reamaingHandling     = Number($("#reamaingHandling").text()) ;
	 var reamaingExtraCharges = Number($("#reamaingExtraCharges").text()) ;
	
	if(reamaingMaintainance<=0 && reamaingHandling<=0 && reamaingExtraCharges<=0 && totalMaintCost==totalPaidMaintainanceAmt && totalHandlingCharge==totalPaidHandlingCharges && totalExtraCharges==totalPaidExtraCharges)
	{
		document.getElementById('submitButton').style.display ='none';
		document.getElementById('paymentStatus').style.display ='block';
	}
    
}


function checkAmount()
{
  $('#buttonSpan').html('');
  
  var reamaingMaintainance = Number($("#reamaingMaintainance").text()) ;
  var reamaingHandling     = Number($("#reamaingHandling").text()) ;
  var reamaingExtraCharges = Number($("#reamaingExtraCharges").text()) ;
  
  var maintainanceAmt      =  Number($("#maintainanceAmt").val()) ;
  var handlingChargesAmt   =  Number($("#handlingChargesAmt").val()) ;
  var extraChargesAmt      =  Number($("#extraChargesAmt").val()) ;
	  
  if(maintainanceAmt!=0)
  {
	 if(maintainanceAmt>reamaingMaintainance)
	 {
		 $('#buttonSpan').html('Please, enter maintainance amount less than or eqal to remaining amount..!');
		 document.customerreceiptform.maintainanceAmt.focus();
		 return false;
	 }
  }
  
  if(handlingChargesAmt!=0)
  {
	 if(handlingChargesAmt>reamaingHandling)
	 {
		 $('#buttonSpan').html('Please, enter handling charges less than or eqal to remaining amount..!');
		 document.customerreceiptform.handlingChargesAmt.focus();
		 return false;
	 }
  }
  
  if(extraChargesAmt!=0)
  {
	 if(extraChargesAmt>reamaingExtraCharges)
	 {
		 $('#buttonSpan').html('Please, enter extra charges less than or eqal to remaining amount..!');
		 document.customerreceiptform.extraChargesAmt.focus();
		 return false;
	 }
  }
  
}

</script>
</body>
</html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | User Master</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
 
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

   <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Master Master Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">User Master</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="usermasterform" action="${pageContext.request.contextPath}/UserMaster" method="post">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <%-- <div class="row">
            <div class="col-md-12">
     
	          <div class="box-body">
	            <div class="row">
	            
	               <div class="col-md-3">
	                  <label>Role</label> 
	                  <select class="form-control" id="countryName" name="countryName" onchange="getUserListBasedOnRole()">
					        <option selected="selected" value="Default">-Select Role-</option>
					    <c:forEach var="departmentList" items="${departmentList}">
	                        <option value="${departmentList.departmentName}">${departmentList.departmentName}</option>
					    </c:forEach>
	                  </select>
	               </div>
	                    
	             </div>
	           </div>
                
            </div>
            
          </div> --%>
		  	 
		  	 <div class="box-body">
                <div class="row">
                 <br/>
                 
				  <div class="col-xs-3">
			  	
			      </div>
			     
				  <div class="col-xs-3">
			 			&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
			  			<a href="UserMaster">  <button type="button" class="btn btn-default" value="reset" style="width:90px"> Reset</button></a>
              	  </div>
              	   
	              <div class="col-xs-2">
				      <a href="AddUserModel"> <button type="button" class="btn btn-success"><i class="fa fa-plus"></i>Add User</button></a>
	              </div>
			       
			  </div>
			</div>
        </div>
        
      </div>
				
	<div class="box box-default">
        
			 <div class="box-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="employeeListTable">
                  <thead>
	                  <tr bgcolor=#4682B4>
	                    <th>Employee Id</th>
	                    <th>Employee Name</th>
	                    <th>Role</th>
	                    <th>Username</th>
	                    <th>Password</th>
	                    <th>Action</th>
	                  </tr>
                  </thead>
                  
                  <tbody>
                  
	                   <s:forEach items="${usersList}" var="usersList" varStatus="loopStatus">
	                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
	                            <td>${usersList.employeeId}</td>
		                        <td>${usersList.employeeName}</td>
		                        <td>${usersList.role}</td>
		                        <td>${usersList.userId}</td>
		                        <td>${usersList.passWord}</td>
		                        <td><a href="${pageContext.request.contextPath}/EditUserModel?userModelId=${usersList.userModelId}" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit User"><i class="glyphicon glyphicon-edit"></i></a></td>
	                        </tr>
						</s:forEach>
                
                 </tbody>
                </table>
              </div>
            </div>
				
              <!-- /.form-group -->
          
        <!-- /.box-body -->
          
      </div>
     
    </section>
	</form>
    <!-- /.content -->
    
  </div>
 
  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
       
  <script src="https://code.jquery.com/jquery-3.0.0.js"></script>
  <script src="https://code.jquery.com/jquery-migrate-3.0.1.js"></script>
  
<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script>



//function to get country wise state list------------------------------------
function getStateList()
{
	 $("#stateName").empty();
	 var countryName = $('#countryName').val();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getStateList',
		type : 'Post',
		data : { countryName : countryName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select State-");
							$("#stateName").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].stateName).text(result[i].stateName);
							    $("#stateName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
	// to retrive all value in table..........
	// for clear table
	$("#employeeListTable tr").detach();
	
	
	 var countryName = $('#countryName').val();
	 
	 $("#stateName").empty(); // for clear the State
	 $("#cityName").empty(); // for clear city
		var option = $('<option/>');// to add select option in city
		option.attr('value',"Default").text("-Select City-");
		
		$("#cityName").append(option);
		 $("#locationareaName").empty();
		 var option = $('<option/>');
		 option.attr('value',"Default").text("-Select Location Area-");
		 $("#locationareaName").append(option);
		
	  $.ajax({

		url : '${pageContext.request.contextPath}/getCountryEmployeeList',
		type : 'Post',
		data : { countryName : countryName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
									$('#employeeListTable').append('<tr style="background-color: #4682B4;"><th style="width:150px">Employee Id</th> <th style="width:300px">Name</th>  <th style="width:140px">Designation</th>  <th style="width:140px">Email Id</th>  <th style="width:200px">Mobile No.</th>  <th style="width:150px">Aaadhar card No</th><th style="width:130px">Joining Date</th> <th style="width:130px">Basic Pay</th><th>Action</th>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].employeeId;
									$('#employeeListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].employeeId+'</td><td>'+result[i].employeefirstName+' '+result[i].employeemiddleName+' '+result[i].employeelastName+'</td><td>'+result[i].employeeDesignation+'</td><td>'+result[i].employeeEmailId+'</td><td>'+result[i].employeeMobileno+'</td><td>'+result[i].employeeAadharcardno+'</td><td>'+result[i].employeeJoiningdate+'</td><td>'+result[i].employeeBasicpay+'</td><td><a href="${pageContext.request.contextPath}/EditEmployee?employeeId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
								else
								{
									var id = result[i].employeeId;
									$('#employeeListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].employeeId+'</td><td>'+result[i].employeefirstName+' '+result[i].employeemiddleName+' '+result[i].employeelastName+'</td><td>'+result[i].employeeDesignation+'</td><td>'+result[i].employeeEmailId+'</td><td>'+result[i].employeeMobileno+'</td><td>'+result[i].employeeAadharcardno+'</td><td>'+result[i].employeeJoiningdate+'</td><td>'+result[i].employeeBasicpay+'</td><td><a href="${pageContext.request.contextPath}/EditEmployee?employeeId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
										
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}
					}
		});
	
}//end of get State List



function getCityList()
{
	 $("#cityName").empty();
	 
	 var countryName = $('#countryName').val();
	 var stateName = $('#stateName').val();
	 
	 $.ajax({

		url : '${pageContext.request.contextPath}/getCityList',
		type : 'Post',
		data : { countryName : countryName, stateName : stateName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select City-");
							$("#cityName").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].cityName).text(result[i].cityName);
							    $("#cityName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
						}
					}
		});
	
	
	
	// to retrive all value in table..........
	
	 $("#employeeListTable tr").detach();
	 var stateName = $('#stateName').val();
	 var countryName = $('#countryName').val();
	 
	 $("#cityName").empty();
	 $("#locationareaName").empty();
	 var option = $('<option/>');
	 option.attr('value',"Default").text("-Select Location Area-");
	 $("#locationareaName").append(option);
	
	 $.ajax({
		//alert("hjhjh");

		url : '${pageContext.request.contextPath}/getCityWiseEmployeeList',
		type : 'Post',
		data : { stateName : stateName, countryName : countryName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
									$('#employeeListTable').append('<tr style="background-color: #4682B4;"><th style="width:150px">Employee Id</th> <th style="width:300px">Name</th>  <th style="width:140px">Designation</th>  <th style="width:140px">Email Id</th>  <th style="width:200px">Mobile No.</th>  <th style="width:150px">Aaadhar card No</th><th style="width:130px">Joining Date</th> <th style="width:130px">Basic Pay</th><th>Action</th>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].employeeId;
									$('#employeeListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].employeeId+'</td><td>'+result[i].employeefirstName+' '+result[i].employeemiddleName+' '+result[i].employeelastName+'</td><td>'+result[i].employeeDesignation+'</td><td>'+result[i].employeeEmailId+'</td><td>'+result[i].employeeMobileno+'</td><td>'+result[i].employeeAadharcardno+'</td><td>'+result[i].employeeJoiningdate+'</td><td>'+result[i].employeeBasicpay+'</td><td><a href="${pageContext.request.contextPath}/EditEmployee?employeeId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
								else
								{
									var id = result[i].employeeId;
									$('#employeeListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].employeeId+'</td><td>'+result[i].employeefirstName+' '+result[i].employeemiddleName+' '+result[i].employeelastName+'</td><td>'+result[i].employeeDesignation+'</td><td>'+result[i].employeeEmailId+'</td><td>'+result[i].employeeMobileno+'</td><td>'+result[i].employeeAadharcardno+'</td><td>'+result[i].employeeJoiningdate+'</td><td>'+result[i].employeeBasicpay+'</td><td><a href="${pageContext.request.contextPath}/EditEmployee?employeeId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
										
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
		
}//end of get City List


function getLocationAreaList()
{
	$("#locationareaName").empty();
	
	 //to retrive all banks by city name
	$("#employeeListTable tr").detach();
	
	 
	 var countryName = $('#countryName').val();
	 var stateName = $('#stateName').val();
	 var cityName = $('#cityName').val();
	 
	 $.ajax({

		url : '${pageContext.request.contextPath}/getLocationAreaList',
		type : 'Post',
		data : { countryName : countryName, stateName : stateName, cityName : cityName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Location Area-");
							$("#locationareaName").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].locationareaName).text(result[i].locationareaName);
							    $("#locationareaName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
						}
					}
		});
	 
	 
	 var cityName = $('#cityName').val();
	 var stateName = $('#stateName').val();
	 var countryName = $('#countryName').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getLocationAreaWiseEmployeeList',
		type : 'Post',
		data : { cityName : cityName, stateName : stateName, countryName : countryName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
									$('#employeeListTable').append('<tr style="background-color: #4682B4;"><th style="width:150px">Employee Id</th> <th style="width:300px">Name</th>  <th style="width:140px">Designation</th>  <th style="width:140px">Email Id</th>  <th style="width:200px">Mobile No.</th>  <th style="width:150px">Aaadhar card No</th><th style="width:130px">Joining Date</th> <th style="width:130px">Basic Pay</th><th>Action</th>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].employeeId;
									$('#employeeListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].employeeId+'</td><td>'+result[i].employeefirstName+' '+result[i].employeemiddleName+' '+result[i].employeelastName+'</td><td>'+result[i].employeeDesignation+'</td><td>'+result[i].employeeEmailId+'</td><td>'+result[i].employeeMobileno+'</td><td>'+result[i].employeeAadharcardno+'</td><td>'+result[i].employeeJoiningdate+'</td><td>'+result[i].employeeBasicpay+'</td><td><a href="${pageContext.request.contextPath}/EditEmployee?employeeId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
								else
								{
									var id = result[i].employeeId;
									$('#employeeListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].employeeId+'</td><td>'+result[i].employeefirstName+' '+result[i].employeemiddleName+' '+result[i].employeelastName+'</td><td>'+result[i].employeeDesignation+'</td><td>'+result[i].employeeEmailId+'</td><td>'+result[i].employeeMobileno+'</td><td>'+result[i].employeeAadharcardno+'</td><td>'+result[i].employeeJoiningdate+'</td><td>'+result[i].employeeBasicpay+'</td><td><a href="${pageContext.request.contextPath}/EditEmployee?employeeId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
										
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
}//end of get locationarea List



function getBankList()
{
	
	 //to retrive all banks by Location Area name
	$("#employeeListTable tr").detach();
	
	 var locationareaName = $('#locationareaName').val();
	 var cityName = $('#cityName').val();
	 var stateName = $('#stateName').val();
	 var countryName = $('#countryName').val();
	 
	 
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getEmployeeList',
		type : 'Post',
		data : { locationareaName : locationareaName, cityName : cityName, stateName : stateName, countryName : countryName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
									$('#employeeListTable').append('<tr style="background-color: #4682B4;"><th style="width:150px">Employee Id</th> <th style="width:300px">Name</th>  <th style="width:140px">Designation</th>  <th style="width:140px">Email Id</th>  <th style="width:200px">Mobile No.</th>  <th style="width:150px">Aaadhar card No</th><th style="width:130px">Joining Date</th> <th style="width:130px">Basic Pay</th><th>Action</th>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].employeeId;
									$('#employeeListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].employeeId+'</td><td>'+result[i].employeefirstName+' '+result[i].employeemiddleName+' '+result[i].employeelastName+'</td><td>'+result[i].employeeDesignation+'</td><td>'+result[i].employeeEmailId+'</td><td>'+result[i].employeeMobileno+'</td><td>'+result[i].employeeAadharcardno+'</td><td>'+result[i].employeeJoiningdate+'</td><td>'+result[i].employeeBasicpay+'</td><td><a href="${pageContext.request.contextPath}/EditEmployee?employeeId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
								else
								{
									var id = result[i].employeeId;
									$('#employeeListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].employeeId+'</td><td>'+result[i].employeefirstName+' '+result[i].employeemiddleName+' '+result[i].employeelastName+'</td><td>'+result[i].employeeDesignation+'</td><td>'+result[i].employeeEmailId+'</td><td>'+result[i].employeeMobileno+'</td><td>'+result[i].employeeAadharcardno+'</td><td>'+result[i].employeeJoiningdate+'</td><td>'+result[i].employeeBasicpay+'</td><td><a href="${pageContext.request.contextPath}/EditEmployee?employeeId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
										
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}
		           }
		
			});
	
}//end of get locationarea List



$(function () {
    $('#employeeListTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
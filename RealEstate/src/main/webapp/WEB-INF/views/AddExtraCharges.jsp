<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Add Extra Charges</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
    </script>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        
  <script src="https://code.jquery.com/jquery-3.0.0.js"></script>
  <script src="https://code.jquery.com/jquery-migrate-3.0.1.js"></script>
          
</head>
<body class="hold-transition skin-blue sidebar-mini"  onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

  <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
  <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Add Extra Charges:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Add Extra Charges</li>
      </ol>
    </section>

<form name="extrachargeform" action="${pageContext.request.contextPath}/AddExtraCharges" onSubmit="return validate()" method="POST">
    <!-- Main content -->
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              
              <!-- /.form-group -->
			  
			   <span id="statusSpan" style="color:#FF0000"></span>
			     <div class="box-body">
              <div class="row">
               <div class="col-xs-4">
			     <label for="chargesId">Id</label>
                  <input type="text" class="form-control" id="chargesId" name="chargesId" value="${extraCode}" readonly>
                 </div> 
                  <div class="col-xs-4">
			     <label for="bookingId">Booking Id</label>
                  <input type="text" class="form-control" id="bookingId" name="bookingId" value="${bookingDetails[0].bookingId}" readonly>
                 </div> 
                    <div class="col-xs-4">
			     <label for="bookingDate">Booking Date</label>
                  <input type="text" class="form-control" id="bookingDate" name="bookingDate" value="${bookingDetails[0].creationDate}" readonly>
                 </div>
				</div>
             </div>
		   </div>
			  
		   <div class="col-md-12">	  
			 
			 <div class="box-body">
               <div class="row">
                  
                 <div class="col-xs-3">
                    <label for="customerName">Customer Name  </label> 
                    <input type="text" class="form-control" id="customerName" name="customerName" value="${bookingDetails[0].bookingfirstname}  ${bookingDetails[0].bookingmiddlename}  ${bookingDetails[0].bookinglastname}" readonly>
                    <span id="customerNameSpan" style="color:#FF0000"></span>
			     </div>
			     
			     <div class="col-xs-3">
			   		<label for="aggreementValue1">Agreement Value</label> 
 					<input type="text" class="form-control" id="aggreementValue1" name="aggreementValue1" value="${bookingDetails[0].aggreementValue1}" readonly>
			     </div> 
			     
				 <div class="col-xs-3">
                  	<label>Maintenance Cost</label> 
              		<input type="text" class="form-control" id="maintenanceCost" name="maintenanceCost" value="${wingDetails.maintenanceCharges}" readonly="readonly">
                    <span id="maintenanceCostSpan" style="color:#FF0000"></span>
                 </div>
			     
			   </div>
            </div>
			  
			 <br/><br/>
			 <div class="box-body">
			    <h4><b>Other Extra Charges Payment Details:</b></h4>
              <div class="row">
                  
                  <div class="col-xs-3">
				    <label for="">Charges Description</label> 
				    <input type="hidden" id="otherChargesId" name="otherChargesId" value="">
	                <input type="text" class="form-control" id="chargesDescription" name="chargesDescription" placeholder="Other charge discription" style="text-transform: capitalize;">
	                <span id="chargesDescriptionSpan" style="color:#FF0000"></span>
                 </div> 
                 
                 <div class="col-xs-3">
				     <label for="Amount">Amount</label> 
					 <div class="input-group">
	                   <div class="input-group-addon">
	                     <i class="fa fa fa-inr"></i>
	                   </div>
	                     <input type="text" class="form-control" id="amount" name="amount" placeholder="Amount for this charge">
	                     <span id="AddCharge" class="input-group-btn">
	            	     <button type="button" class="btn btn-success" onclick="return AddExtraChagresPayment()"><i class="fa fa-plus"></i>Add</button>
	              	     </span>
	              	     <span id="UpdateCharge" class="input-group-btn">
	            	     <button type="button" class="btn btn-success" onclick="return UpdateExtraChagresPayment()"><i class="fa fa-plus"></i>Update</button>
	              	     </span>
	              	   
	                 </div>
	                  <span id="amountSpan" style="color:#FF0000"></span>
			     </div>
                 
		  </div>
        </div>		

        <div class="col-md-6">
          <div class="box-body">
           <div class="row">        
              <table class="table table-bordered" id="otherChargesListTable">
	              <tr bgcolor=#4682B4>
		              <th >Other Charges Description</th>
		              <th style="width: 120px;">Amount(Rs.)</th>
		              <th style="width: 120px;">Action</th>
	               </tr>
				
				   <c:forEach items="${multipleOtherCharges}" var="multipleOtherCharges" varStatus="loopStatus">
                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
		                   <td>${multipleOtherCharges.chargesDescription}</td>
		                   <td>${multipleOtherCharges.amount}</td>
			               <td><a onclick="EditOtherCharges('${multipleOtherCharges.otherChargesId}')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>  
			               |<a onclick="DeleteOtherCharge('${multipleOtherCharges.otherChargesId}')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td>
	             	   </tr>
				   </c:forEach>
				   
              </table>
            </div>
          </div>
        </div>
        	
        		 <input type="hidden" id="chargesStatus" name="chargesStatus" value="${chargesStatus}">	
				 <input type="hidden" id="creationDate" name="creationDate" value="">
			     <input type="hidden" id="updateDate" name="updateDate" value="">
			     <input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>"> 
				 
             
              <!-- /.form-group -->
            </div>
           
		   
          <!-- /.row -->
		  
		  
        </div>
        <div class="box-body">
              <div class="row">
                  </br>
                    
                 <div class="col-xs-4">
	            	 <div class="col-xs-2">	
	             		<a href="BookingMaster"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
				     </div> 
			     </div>
				  <div class="col-xs-4">
                	<button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
			     </div>
			     
				 <div class="col-xs-2">
			  		<button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
			     </div> 
			     
              </div>
		</div>
        <!-- /.box-body -->
        
      </div>
      <!-- /.box -->

     
    </section>
	</form>
    <!-- /.content -->
  </div>
 

  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->

<script>

function clearall()
{
	$('#maintenanceCostSpan').html('');
	$('#parkingChargesSpan').html('');
	$('#otherChargesSpan').html('');
	$('#chargesDescriptionSpan').html('');
	$('#amountSpan').html('');
}

function validate()
{
	    clearall();
	
	    if(document.extrachargeform.maintenanceCost.value=="")
		{
			$('#maintenanceCostSpan').html('Maintenance cost should not be empty..!');
			document.extrachargeform.maintenanceCost.value="";
			document.extrachargeform.maintenanceCost.focus();
			return false;
		}
	    else if(document.extrachargeform.maintenanceCost.value.match(/^[\s]+$/))
		{
			$('#maintenanceCostSpan').html('Maintenance cost should not be empty..!');
			document.extrachargeform.maintenanceCost.value="";
			document.extrachargeform.maintenanceCost.focus();
			return false;
		}
	    else if(!document.extrachargeform.maintenanceCost.value.match(/^[0-9]+[.]{0,1}[0-9]+$/))
		{
			$('#maintenanceCostSpan').html(' maintenance Cost should be digits only..!');
			document.extrachargeform.maintenanceCost.value="";
			document.extrachargeform.maintenanceCost.focus();
			return false;
		}
		
		if(document.extrachargeform.parkingCharges.value.length!=0)
		{
			if(!document.extrachargeform.parkingCharges.value.match(/^[0-9]+$/))
			{
				$('#parkingChargesSpan').html(' parking Cost must be digits only..!');
				document.extrachargeform.parkingCharges.value="";
				document.extrachargeform.parkingCharges.focus();
				return false;
			}
		}
		
		/* if(document.extrachargeform.otherCharges.value.length!=0)
		{
			if(!document.extrachargeform.otherCharges.value.match(/^[0-9]+$/))
		{
			$('#otherChargesSpan').html(' other Charges Cost must be digit numbers only..!');
			document.extrachargeform.otherCharges.value="";
			document.extrachargeform.otherCharges.focus();
			return false;
		}
		} */
		
		
}

function init()
{
	clearall();
	
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	
	$("#UpdateCharge").removeAttr("style").hide();
	
	//document.getElementById("UpdateCharge").style('display','none');
	
    document.extrachargeform.maintenanceCost.focus();
}


function AddExtraChagresPayment()
{

	clearall();
	
	//validation for employee name
	if(document.extrachargeform.chargesDescription.value=="")
	{
		$('#chargesDescriptionSpan').html('Description should not be empty..!');
		document.extrachargeform.chargesDescription.value="";
		document.extrachargeform.chargesDescription.focus();
		
		return false;
	}
	else if(document.extrachargeform.chargesDescription.value.match(/^[\s]+$/))
	{
		$('#chargesDescriptionSpan').html('Description should not be empty..!');
		document.extrachargeform.chargesDescription.value="";
		document.extrachargeform.chargesDescription.focus();
		
		return false;
	}
	
	//validation for employee email id
	if(document.extrachargeform.amount.value=="")
	{
		$('#amountSpan').html('Please, enter amount..!');
		document.extrachargeform.amount.focus();
		return false;
	}
	else if(document.extrachargeform.amount.value.match(/^[\s]+$/))
	{
		$('#amountSpan').html('Please, enter amount..!');
		document.extrachargeform.amount.value="";
		document.extrachargeform.amount.focus();
		return false;
	}
	else if(!document.extrachargeform.amount.value.match(/^[0-9]+[.]{0,1}[0-9]{0,2}$/))
	{
		$('#amountSpan').html('Please, enter valid amount..!');
		document.extrachargeform.amount.value="";
		document.extrachargeform.amount.focus();
		return false;
	}
	
	 
	$('#otherChargesListTable tr').detach();
	 
	 var chargesId = $('#chargesId').val();
	 var bookingId = $('#bookingId').val();
	 
	 var chargesDescription = $('#chargesDescription').val();
	 var amount = $('#amount').val();
	 
	 $.ajax({

		 url : '${pageContext.request.contextPath}/AddExtraOtherChagresPayment',
		type : 'Post',
		data : { chargesId : chargesId, bookingId : bookingId, chargesDescription : chargesDescription, amount : amount},
		dataType : 'json',
		success : function(result)
				  {
					   if (result) 
					   { 
									$('#otherChargesListTable').append('<tr style="background-color: #4682B4;"><th >Charges Description</th><th style="width:120px">Amount(Rs.)</th><th style="width:120px">Action</th>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].otherChargesId;
									$('#otherChargesListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].chargesDescription+'</td><td>'+result[i].amount+'</td><td><a onclick="EditOtherCharges('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>  <a onclick="DeleteOtherCharge('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td>');
								}
								else
								{
									var id = result[i].otherChargesId;
									$('#otherChargesListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].chargesDescription+'</td><td>'+result[i].amount+'</td><td><a onclick="EditOtherCharges('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>  <a onclick="DeleteOtherCharge('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}
				  } 

		});
	 
	 $('#chargesDescription').val("");
	 $('#amount').val("");
}

function EditOtherCharges(otherChargesId)
{
	var chargesId = $('#chargesId').val();
	var bookingId = $('#bookingId').val();

	$.ajax({

		 url : '${pageContext.request.contextPath}/EditExtraOtherChagresPayment',
		type : 'Post',
		data : { otherChargesId : otherChargesId, chargesId : chargesId, bookingId : bookingId },
		dataType : 'json',
		success : function(result)
				  {
					   if (result) 
					   { 
							 for(var i=0; i < result.length; i++)
							 {
							   $('#otherChargesId').val(result[i].otherChargesId);
							   $('#chargesDescription').val(result[i].chargesDescription);
							   $('#amount').val(result[i].amount);
							 }
						   
						   $("#AddCharge").removeAttr("style").hide();
						   $("#UpdateCharge").show();
						} 
						else
						{
							alert("failure111");
						}
				  } 

		}); 
	
}

function UpdateExtraChagresPayment()
{
	
	$('#otherChargesListTable tr').detach();
	
	var otherChargesId		= $('#otherChargesId').val();
	var chargesId 			= $('#chargesId').val();
	var bookingId 			= $('#bookingId').val();
	var chargesDescription  = $('#chargesDescription').val();
	var amount 				= $('#amount').val();
	
	$.ajax({

		 url : '${pageContext.request.contextPath}/UpdateExtraOtherChagresPayment',
		type : 'Post',
		data : { otherChargesId : otherChargesId, chargesId : chargesId, bookingId : bookingId, chargesDescription : chargesDescription, amount : amount},
		dataType : 'json',
		success : function(result)
				  {
					   if (result) 
					   { 
									$('#otherChargesListTable').append('<tr style="background-color: #4682B4;"><th >Charges Description</th><th style="width:120px">Amount(Rs.)</th><th style="width:120px">Action</th>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].otherChargesId;
									$('#otherChargesListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].chargesDescription+'</td><td>'+result[i].amount+'</td><td><a onclick="EditOtherCharges('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>  <a onclick="DeleteOtherCharge('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td>');
								}
								else
								{
									var id = result[i].otherChargesId;
									$('#otherChargesListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].chargesDescription+'</td><td>'+result[i].amount+'</td><td><a onclick="EditOtherCharges('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>  <a onclick="DeleteOtherCharge('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}
				  } 

		});
	
}


function DeleteOtherCharge(otherChargesId)
{
	$('#otherChargesListTable tr').detach();
	
	var chargesId = $('#chargesId').val();
	var bookingId = $('#bookingId').val();
	
	$.ajax({

		 url : '${pageContext.request.contextPath}/DeleteExtraOtherChagresPayment',
		type : 'Post',
		data : { otherChargesId : otherChargesId, chargesId : chargesId, bookingId : bookingId },
		dataType : 'json',
		success : function(result)
				  {
					   if (result) 
					   { 
									$('#otherChargesListTable').append('<tr style="background-color: #4682B4;"><th >Charges Description</th><th style="width:120px">Amount(Rs.)</th><th style="width:120px">Action</th>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].otherChargesId;
									$('#otherChargesListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].chargesDescription+'</td><td>'+result[i].amount+'</td><td><a onclick="EditOtherCharges('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>  <a onclick="DeleteOtherCharge('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td>');
								}
								else
								{
									var id = result[i].otherChargesId;
									$('#otherChargesListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].chargesDescription+'</td><td>'+result[i].amount+'</td><td><a onclick="EditOtherCharges('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>  <a onclick="DeleteOtherCharge('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}
				  } 

		});
	
}


</script>
</body>
</html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Real Estate | Edit Project</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
<!-- daterange picker -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<!-- bootstrap datepicker -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
<!-- Bootstrap Color Picker -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
<!-- Bootstrap time Picker -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
<!-- Select2 -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
<!-- Theme style -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

<!-- Google Font -->
<style type="text/css">
tr.odd {
	background-color: #CCE5FF
}

tr.even {
	background-color: #F0F8FF
}
</style>

<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#statusSpan').delay(1000).fadeOut();
	});
</script>
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control", "no-cache,no-store,must-revalidate");//HTTP 1.1
		response.setHeader("Pragma", "no-cache"); //HTTP 1.0
		response.setDateHeader("Expires", 0);

		if (session != null) {
			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null
					|| session.getAttribute("profile_img") == null) {
				response.sendRedirect("login");
			}
		}
	%>

	<div class="wrapper">

		<%@ include file="headerpage.jsp"%>

		<%@ include file="menu.jsp"%>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Edit Project Details: <small>Preview</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><a href="#">Master</a></li>
					<li class="active">Edit Project</li>
				</ol>
			</section>

			<!-- Main content -->

			<form name="projectform"
				action="${pageContext.request.contextPath}/EditProject"
				method="Post" onSubmit="return validate()">
				<section class="content">

					<!-- SELECT2 EXAMPLE -->
					<div class="box box-default">

						<!-- /.box-header -->
						<div class="box-body">
							<div class="row">
								<div class="col-md-12">

									<!-- /.form-group -->

									<span id="statusSpan" style="color: #FF0000"></span>
									<div class="box-body">
										<div class="row">
											<div class="col-xs-3">
												<label>Project Id</label> <input type="text"
													class="form-control" id="projectId" placeholder="ID"
													name="projectId" value="${projectDetails[0].projectId}"
													readonly>

											</div>
										</div>
									</div>


									<div class="box-body">
										<div class="row">
											<div class="col-xs-3">
												<label for="projectName">Project Name </label> <label
													class="text-red">* </label> <input type="text"
													class="form-control" id="projectName"
													placeholder="Enter project Name" name="projectName"
													style="text-transform: capitalize;"
													value="${projectDetails[0].projectName}"
													onchange="getprojectList(this.value)"> <span
													id="projectNameSpan" style="color: #FF0000"></span>
											</div>
											<div class="col-xs-3">
												<label>Project Type </label> <label class="text-red">*
												</label> <select class="form-control" name="projectType"
													id="projectType">
													<option value="Default">-select Project Type-</option>
													<option selected="selected"
														value="${projectDetails[0].projectType}">${projectDetails[0].projectType}</option>
													<c:choose>
														<c:when
															test="${projectDetails[0].projectType ne 'Residential'}">
															<option>Residential</option>
														</c:when>
													</c:choose>
													<c:choose>
														<c:when
															test="${projectDetails[0].projectType ne 'Commercial'}">
															<option>Commercial</option>
														</c:when>
													</c:choose>
													<c:choose>
														<c:when
															test="${projectDetails[0].projectType ne 'Commercial & Residential'}">
															<option>Commercial & Residential</option>
														</c:when>
													</c:choose>

												</select> <span id="projectTypeSpan" style="color: #FF0000"></span>
											</div>

											<div class="col-xs-3">
												<label>Company Name</label> <label class="text-red">*
												</label> <select class="form-control" name="companyId"
													id="companyId">
													<option value="Default">-Select Company Name-</option>
													<option selected="selected"
														value="${projectDetails[0].companyId}">${companyName}</option>

													<c:forEach var="companyList" items="${companyList}">
														<c:choose>
															<c:when
																test="${projectDetails[0].companyId ne companyList.companyId}">
																<option value="${companyList.companyId}">${companyList.companyName}</option>
															</c:when>
														</c:choose>
													</c:forEach>
												</select> <span id="companyIdSpan" style="color: #FF0000"></span>
											</div>

											<div class="col-xs-3">
												<label>Gat No./Survey No.</label><label class="text-red">*</label>
												<input type="text" class="form-control"
													id="gatORserveyNumber" placeholder="Gat/Servey No."
													name="gatORserveyNumber"
													value="${projectDetails[0].gatORserveyNumber}"> <span
													id="gatORserveyNumberSpan" style="color: #FF0000"></span>
											</div>
										</div>
									</div>

									<div class="box-body">
										<div class="row">
											<div class="col-xs-3">
												<label for="projectlandarea">Project Land
													Area(SQ.Ft.)</label> <label class="text-red">* </label> <input
													type="text" class="form-control" id="projectlandArea"
													placeholder="Project Land Area :" name="projectlandArea"
													value="${projectDetails[0].projectlandArea}"> <span
													id="projectlandAreaSpan" style="color: #FF0000"></span>
											</div>
											<div class="col-xs-3">
												<label for="projectcost">Project Cost </label> <label
													class="text-red">* </label> <input type="text"
													class="form-control" id="projectCost"
													placeholder="Project Cost :" name="projectCost"
													value="${projectCost}"> <span id="projectCostSpan"
													style="color: #FF0000"></span>
											</div>
											<div class="col-xs-3">
												<label for="noofunits">No. of Buildings </label> <label
													class="text-red">* </label> <input type="text"
													class="form-control" id="numberofBuildings"
													placeholder="No. of Buildings" name="numberofBuildings"
													value="${projectDetails[0].numberofBuildings}"> <span
													id="numberofBuildingsSpan" style="color: #FF0000"></span>
											</div>
											<div class="col-xs-3">
												<label for="noofunits">Loading % </label> <label
													class="text-red">* </label> <input type="text"
													class="form-control" id="loading"
													placeholder="Enter the loading" name="loading"
													value="${projectDetails[0].loading}"> <span
													id="loadingSpan" style="color: #FF0000"></span>
											</div>

										</div>
									</div>

									<div class="box-body">
										<div class="row">
											<div class="col-xs-3">
												<label>RERA / Non-RERA </label> <label class="text-red">*
												</label> <select class="form-control" name="reraStatus"
													id="reraStatus">
													<option value="${projectDetails[0].reraStatus}">${projectDetails[0].reraStatus}</option>
													<c:choose>
														<c:when test="${projectDetails[0].reraStatus ne 'RERA'}">
															<option>RERA</option>
														</c:when>
													</c:choose>
													<c:choose>
														<c:when
															test="${projectDetails[0].reraStatus ne 'Non-RERA'}">
															<option>Non-RERA</option>
														</c:when>
													</c:choose>
												</select> <span id="reraStatusSpan" style="color: #FF0000"></span>
											</div>
											<div class="col-xs-3">
												<label>RERA Number</label> <input type="text"
													class="form-control" id="reraNumber" placeholder="Rera No:"
													name="reraNumber" value="${projectDetails[0].reraNumber}">
												<span id="reraNumberSpan" style="color: #FF0000"></span>
											</div>

											<div class="col-xs-3">
												<label>Project Start Date </label> <label class="text-red">*
												</label>
												<div class="input-group date">
													<div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</div>
													<input type="text" class="form-control pull-right"
														id="projectstartDate" name="projectstartDate"
														value="${projectDetails[0].projectstartDate}">
												</div>
												<span id="projectstartDateSpan" style="color: #FF0000"></span>
											</div>
											<div class="col-xs-3">
												<label>Project End Date </label>
												<div class="input-group date">
													<div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</div>
													<input type="text" class="form-control pull-right"
														id="projectendDate" name="projectendDate"
														value="${projectDetails[0].projectendDate}">
												</div>
												<span id="projectendDateSpan" style="color: #FF0000"></span>
											</div>
										</div>
									</div>

									<div class="box-body">
										<div class="row">
											<div class="col-xs-3">
												<label for="projectAddress">Address </label> <label
													class="text-red">* </label>
												<textarea class="form-control" rows="1" id="projectAddress"
													placeholder="Enter Address" name="projectAddress">${projectDetails[0].projectAddress}</textarea>
												<span id="projectAddressSpan" style="color: #FF0000"></span>
											</div>


											<div class="col-xs-3">
												<label>Country</label> <label class="text-red">* </label> <select
													class="form-control" name="countryId" id="countryId"
													onchange="getStateList(this.value)">
													<option value="Default">-Select Country-</option>
													<option selected="selected"
														value="${projectDetails[0].countryId}">${countryName}</option>

													<c:forEach var="countryList" items="${countryList}">
														<c:choose>
															<c:when
																test="${projectDetails[0].countryId ne countryList.countryId}">
																<option value="${countryList.countryId}">${countryList.countryName}</option>
															</c:when>
														</c:choose>
													</c:forEach>

												</select> <span id="countryIdSpan" style="color: #FF0000"></span>
											</div>
											<div class="col-xs-3">
												<label>State</label> <label class="text-red">* </label> <select
													class="form-control" name="stateId" id="stateId"
													onchange="getCityList(this.value)">
													<option value="Default">Select State-</option>
													<option selected="selected"
														value="${projectDetails[0].stateId}">${stateName}</option>
													<c:forEach var="stateList" items="${stateList}">
														<c:choose>
															<c:when
																test="${projectDetails[0].stateId ne stateList.stateId}">
																<option value="${stateList.stateId}">${stateList.stateId}</option>
															</c:when>
														</c:choose>
													</c:forEach>
												</select> <span id="stateIdSpan" style="color: #FF0000"></span>
											</div>
											<div class="col-xs-3">
												<label>City</label> <label class="text-red">* </label> <select
													class="form-control" name="cityId" id="cityId"
													onchange="getLocationAreaList(this.value)">
													<option value="Default">-Select City-</option>
													<option selected="selected"
														value="${projectDetails[0].cityId}">${cityName}</option>

													<c:forEach var="cityList" items="${cityList}">
														<c:choose>
															<c:when
																test="${projectDetails[0].cityId ne cityList.cityId}">
																<option value="${cityList.cityId}">${cityList.cityId}</option>
															</c:when>
														</c:choose>
													</c:forEach>

												</select> <span id="cityIdSpan" style="color: #FF0000"></span>
											</div>
										</div>
									</div>



									<div class="box-body">
										<div class="row">
											<div class="col-xs-3">
												<label>Area</label> <label class="text-red">* </label> <select
													class="form-control" name="locationareaId"
													id="locationareaId" onchange="getpinCode(this.value)">
													<option value="Default">-Select Area-</option>
													<option selected="selected"
														value="${projectDetails[0].locationareaId}">${locationareaName}</option>
													<c:forEach var="locationareaList"
														items="${locationareaList}">
														<c:choose>
															<c:when
																test="${projectDetails[0].locationareaId ne locationareaList.locationareaId}">
																<option value="${locationareaList.locationareaId}">${locationareaList.locationareaId}</option>
															</c:when>
														</c:choose>
													</c:forEach>
												</select> <span id="locationareaIdSpan" style="color: #FF0000"></span>
											</div>
											<div class="col-xs-3">
												<label for="areaPincode">Pin Code</label> <label
													class="text-red">* </label> <input type="text"
													class="form-control" id="areaPincode"
													placeholder="Enter Pin Code" name="areaPincode"
													value="${projectDetails[0].areaPincode}" readonly>
												<span id="areaPincode" style="color: #FF0000"></span>
											</div>
											<div class="col-xs-3">
												<label>Office Phone No</label> <label class="text-red">*
												</label>
												<div class="input-group">
													<div class="input-group-addon">
														<i class="fa fa-phone"></i>
													</div>
													<input type="text" class="form-control"
														data-inputmask='"mask": "(999) 999-99999"' data-mask
														name="projectofficePhoneno" id="projectofficePhoneno"
														value="${projectDetails[0].projectofficePhoneno}">
													<span id="projectofficePhonenoSpan" style="color: #FF0000"></span>
												</div>
											</div>
											<div class="col-xs-3">
												<label>Project Status </label> <select class="form-control"
													name="projectStatus" id="projectStatus">
													<option value="Default">-Select Project Status-</option>
													<option selected="selected"
														value="${projectDetails[0].projectStatus}">${projectDetails[0].projectStatus}</option>
													<c:choose>
														<c:when
															test="${projectDetails[0].projectStatus ne 'Not-Started'}">
															<option>Not-Started</option>
														</c:when>
													</c:choose>
													<c:choose>
														<c:when
															test="${projectDetails[0].projectStatus ne 'In-Process'}">
															<option>In-Process</option>
														</c:when>
													</c:choose>
													<c:choose>
														<c:when
															test="${projectDetails[0].projectStatus ne 'Completed'}">
															<option>Completed</option>
														</c:when>
													</c:choose>

												</select> <span id="projectStatusSpan" style="color: #FF0000"></span>
											</div>
										</div>
									</div>


									<div class="box-body">
										<div class="row">
											<div class="col-xs-3">
												<label for="projectlandarea">GST Per</label> <label
													class="text-red">* </label> <input type="text"
													class="form-control" id="gstPer"
													value="${projectDetails[0].gstPer}"
													placeholder="Project gst Per:" name="gstPer"> <span
													id="gstPerSpan" style="color: #FF0000"></span>
											</div>
											<div class="col-xs-3">
												<label for="projectcost">Stamp Duty Per </label> <label
													class="text-red">* </label> <input type="text"
													class="form-control" id="stampdutyPer"
													value="${projectDetails[0].stampdutyPer}"
													placeholder="Stamp Duty Per :" name="stampdutyPer">
												<span id="stampdutyPerSpan" style="color: #FF0000"></span>
											</div>
											<div class="col-xs-3">
												<label for="noofunits">Registration Per </label> <label
													class="text-red">* </label> <input type="text"
													class="form-control" id="registrationPer"
													value="${projectDetails[0].registrationPer}"
													placeholder="Registration Per" name="registrationPer">
												<span id="registrationPerSpan" style="color: #FF0000"></span>
											</div>

										</div>
									</div>



									<input type="hidden" id="priviousLoading"
										name="priviousLoading" value="${projectDetails[0].loading}">
									<input type="hidden" id="projectdbStatus"
										name="projectdbStatus" value="${projectdbStatus}"> <input
										type="hidden" id="creationDate" name="creationDate"
										value="${projectDetails[0].creationDate}"> <input
										type="hidden" id="updateDate" name="updateDate" value="">
									<input type="hidden" id="userName" name="userName"
										value="<%=session.getAttribute("user")%>">
									<div class="box-body">
										<div class="row">
											</br>

											<div class="col-xs-5">
												<div class="col-xs-2">
													<a href="ProjectMaster"><button type="button"
															class="btn btn-block btn-primary" style="width: 90px">Back</button></a>
												</div>
											</div>
											<div class="col-xs-4">
												<button type="reset" class="btn btn-default" value="reset"
													style="width: 90px">Reset</button>
											</div>
											<div class="col-xs-2">
												<button type="submit" class="btn btn-info pull-right"
													name="submit">Submit</button>

											</div>

										</div>
									</div>
								</div>
							</div>
						</div>
				</section>
			</form>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->


		<!-- Control Sidebar -->

		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->

	<!-- jQuery 3 -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap 3.3.7 -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- Select2 -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
	<!-- InputMask -->
	<script
		src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
	<!-- date-range-picker -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
	<!-- bootstrap datepicker -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
	<!-- bootstrap color picker -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
	<!-- bootstrap time picker -->
	<script
		src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
	<!-- SlimScroll -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<!-- iCheck 1.0.1 -->
	<script
		src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
	<!-- FastClick -->
	<script
		src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script
		src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script
		src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
	<!-- Page script -->

	<script>
		function clearall() {
			$('#projectNameSpan').html('');
			$('#projectTypeSpan').html('');
			$('#projectAddressSpan').html('');
			$('#countryIdSpan').html('');
			$('#stateIdSpan').html('');
			$('#cityIdSpan').html('');
			$('#locationareaIdSpan').html('');
			$('#areaPincodeSpan').html('');
			$('#projectofficePhonenoSpan').html('');
			$('#companyIdSpan').html('');
			$('#projectlandAreaSpan').html('');
			$('#projectCostSpan').html('');
			$('#numberofBuildingsSpan').html('');
			$('#loadingSpan').html('');
			$('#reraStatusSpan').html('');
			$('#reraNumberSpan').html('');
			$('#projectstartDateSpan').html('');
			$('#projectendDateSpan').html('');
			$('#projectStatusSpan').html('');
			$('#gstPerSpan').html('');
			$('#stampdutyPerSpan').html('');
			$('#registrationPerSpan').html('');
		}
		function validate() {
			clearall();

			//validation for project name
			if (document.projectform.projectName.value == "") {
				$('#projectNameSpan').html(
						'Project name should not be empty..!');
				document.projectform.projectName.focus();
				return false;
			} else if (document.projectform.projectName.value.match(/^[\s]+$/)) {
				$('#projectNameSpan').html(
						'Project name should not be empty..!');
				document.projectform.projectName.focus();
				return false;
			}

			//validation for project type
			if (document.projectform.projectType.value == "Default") {
				$('#projectTypeSpan').html('Please, select project type..!');
				document.projectform.projectType.focus();
				return false;
			}

			//validation for project address
			if (document.projectform.projectAddress.value == "") {
				$('#projectAddressSpan').html(
						' project address should not be blank..!');
				document.projectform.projectAddress.focus();
				return false;
			} else if (document.projectform.projectAddress.value
					.match(/^[\s]+$/)) {
				$('#projectAddressSpan').html(
						' project address should not be blank..!');
				document.projectform.projectAddress.value = "";
				document.projectform.projectAddress.focus();
				return false;
			}

			//validation for countryId
			if (document.projectform.countryId.value == "Default") {
				$('#countryIdSpan').html('Please, Select Country Name..!');
				document.projectform.countryId.focus();
				return false;
			}

			//validation for stateId
			if (document.projectform.stateId.value == "Default") {
				$('#stateIdSpan').html('Please, Select State Name..!');
				document.projectform.stateId.focus();
				return false;
			}

			//validation for city name
			if (document.projectform.cityId.value == "Default") {
				$('#cityIdSpan').html('Please, Select City Name..!');
				document.projectform.cityId.focus();
				return false;
			}

			//validation for location area name
			if (document.projectform.locationareaId.value == "Default") {
				$('#locationareaIdSpan')
						.html('Please, Select Location Name..!');
				document.projectform.locationareaId.focus();
				return false;
			}

			//validation for project office phone number
			if (document.projectform.projectofficePhoneno.value == "") {
				$('#projectofficePhonenoSpan').html(
						'Please, enter office phone number..!');
				document.projectform.projectofficePhoneno.focus();
				return false;
			} else if (!document.projectform.projectofficePhoneno.value
					.match(/^\(?([0-9]{3})\)?[\s. ]?([0-9]{3})[-. ]?([0-9]{5})$/)) {
				$('#projectofficePhonenoSpan').html(
						'Please, enter office phone number..!');
				document.projectform.projectofficePhoneno.value = "";
				document.projectform.projectofficePhoneno.focus();
				return false;
			}

			//validation for company name
			if (document.projectform.companyId.value == "Default") {
				$('#companyIdSpan').html('Please, select company name..!');
				document.projectform.companyId.focus();
				return false;
			}

			//validation for project land area
			if (document.projectform.projectlandArea.value == "") {
				$('#projectlandAreaSpan').html(
						'Please, enter project land area..!');
				document.projectform.projectlandArea.focus();
				return false;
			} else if (!document.projectform.projectlandArea.value
					.match(/^[0-9]+$/)) {
				$('#projectlandAreaSpan').html(
						'Please, Enter valid project land area.. (e.g. 620)');
				document.projectform.projectlandArea.value = "";
				document.projectform.projectlandArea.focus();
				return false;
			}

			//validation for project cost
			if (document.projectform.projectCost.value == "") {
				$('#projectCostSpan').html(
						'Please, Enter total project cost..!');
				document.projectform.projectCost.focus();
				return false;
			} else if (!document.projectform.projectCost.value
					.match(/^[0-9]+[.]{1}[0-9]{1,2}$/)) {
				if (!document.projectform.projectCost.value.match(/^[0-9]+$/)) {
					$('#projectCostSpan')
							.html(
									'Please, Enter valid project cost.. (e.g.3000.65 OR 3000)');
					document.projectform.projectCost.value = "";
					document.projectform.projectCost.focus();
					return false;
				}
			}

			//validation for project building number
			if (document.projectform.numberofBuildings.value == "") {
				$('#numberofBuildingsSpan').html(
						'Please, Enter project building number..!');
				document.projectform.numberofBuildings.focus();
				return false;
			} else if (!document.projectform.numberofBuildings.value
					.match(/^[0-9]{1,5}$/)) {
				$('#numberofBuildingsSpan')
						.html(
								'Please, Enter valid project building number.. (e.g. 3)');
				document.projectform.numberofBuildings.value = "";
				document.projectform.numberofBuildings.focus();
				return false;
			}
			//validation for project Loading %
			if (document.projectform.loading.value == "") {
				$('#loadingSpan').html('Please, Enter loading percentage..!');
				document.projectform.loading.focus();
				return false;
			} else if (!document.projectform.loading.value
					.match(/^[0-9]{1,5}$/)) {
				$('#loadingSpan').html(
						'Please, Enter valid Loading Percentage.. (eg. 30)');
				document.projectform.loading.value = "";
				document.projectform.loading.focus();
				return false;
			}
			//validation for rera status	
			if (document.projectform.reraStatus.value == "Default") {
				$('#reraStatusSpan').html('Please, Select Rera Or Non-RERA..!');
				document.projectform.reraStatus.focus();
				return false;
			}

			//validatiopn for rera number
			if (document.projectform.reraStatus.value == "RERA") {
				if (document.projectform.reraNumber.value == "") {
					$('#reraNumberSpan').html('Please, Enter RERA number..!');
					document.projectform.reraNumber.focus();
					return false;
				} else if (!document.projectform.reraNumber.value
						.match(/^[A-Za-z]{1}[0-9]{11}$/)) {
					$('#reraNumberSpan')
							.html(
									'Please, Enter valid RERA number.. (e.g.P52100002218 )');
					document.projectform.reraNumber.value = "";
					document.projectform.reraNumber.focus();
					return false;
				}
			}

			//validation for project start date
			if (document.projectform.projectstartDate.value == "") {
				$('#projectstartDateSpan').html(
						'Please, Select project start date..!');
				document.projectform.projectstartDate.focus();
				return false;
			} else if (document.projectform.projectstartDate.value != "") {
				if (document.projectform.projectendDate.value == "") {
					$('#projectendDateSpan').html(
							'Please, Select project end date..!');
					document.projectform.projectendDate.focus();
					return false;
				} else if (document.projectform.projectendDate.value != "") {
					var startDate = document.projectform.projectstartDate.value;
					var endDate = document.projectform.projectendDate.value;
					var startDate2 = startDate.split("/");
					var endDate2 = endDate.split("/");
					if (startDate2[2] > endDate2[2]) {
						$('#projectendDateSpan').html(
								'Please, Select correct end date..!');
						document.projectform.projectendDate.value = "";
						document.projectform.projectendDate.focus();
						return false;
					} else if (parseInt(startDate2[2]) == parseInt(endDate2[2])) {
						if (parseInt(startDate2[1]) > parseInt(endDate2[1])) {
							$('#projectendDateSpan').html(
									'Please, Select correct end date..!');
							document.projectform.projectendDate.value = "";
							document.projectform.projectendDate.focus();
							return false;
						} else if (parseInt(startDate2[1]) == parseInt(endDate2[1])) {
							if (parseInt(startDate2[0]) > parseInt(endDate2[0])) {
								$('#projectendDateSpan').html(
										'Please, Select correct end date..!');
								document.projectform.projectendDate.value = "";
								document.projectform.projectendDate.focus();
								return false;
							}
						}
					}

				}
			}

			//validation for project Status
			if (document.projectform.projectStatus.value == "Default") {
				$('#projectStatusSpan')
						.html('Please, Select project status..!');
				document.projectform.projectStatus.focus();
				return false;
			}

			if (document.projectform.gstPer.value == "") {
				$('#gstPerSpan').html('Please, Enter GST percentage..!');
				document.projectform.gstPer.focus();
				return false;
			}

			if (document.projectform.stampdutyPer.value == "") {
				$('#stampdutyPerSpan').html(
						'Please, Enter Stamp Duty percentage..!');
				document.projectform.stampdutyPer.focus();
				return false;
			}

			if (document.projectform.registrationPer.value == "") {
				$('#registrationPerSpan').html(
						'Please, Enter Registration percentage..!');
				document.projectform.registrationPer.focus();
				return false;
			}
		}
		function init() {
			clearall();
			var date = new Date();
			var year = date.getFullYear();
			var month = date.getMonth() + 1;
			var day = date.getDate();

			/* document.getElementById("creationDate").value = day + "/" + month + "/" + year;
			 */document.getElementById("updateDate").value = day + "/" + month
					+ "/" + year;

			if (document.projectform.projectdbStatus.value == "Fail") {
				alert("Sorry, record is present already..!");
			} else if (document.projectform.projectdbStatus.value == "Success") {
				$('#statusSpan').html('Record saved successfully..!');
			}
			document.projectform.projectName.focus();
		}

		function getpinCode() {

			$("#areaPincode").empty();
			var locationareaId = $('#locationareaId').val();
			var cityId = $('#cityId').val();
			var stateId = $('#stateId').val();
			var countryId = $('#countryId').val();
			$.ajax({

				url : '${pageContext.request.contextPath}/getallAreaList',
				type : 'Post',
				data : {
					locationareaId : locationareaId,
					cityId : cityId,
					stateId : stateId,
					countryId : countryId
				},
				dataType : 'json',
				success : function(result) {
					if (result) {

						for (var i = 0; i < result.length; i++) {
							$('#areaPincode').val(result[i].pinCode);

						}

					} else {
						alert("failure111");
						//$("#ajax_div").hide();
					}

				}
			});

		}

		function getStateList() {
			$("#stateId").empty();
			$("#cityId").empty();
			$("#locationareaId").empty();

			var countryId = $('#countryId').val();

			$.ajax({

				url : '${pageContext.request.contextPath}/getStateList',
				type : 'Post',
				data : {
					countryId : countryId
				},
				dataType : 'json',
				success : function(result) {
					if (result) {

						var option = $('<option/>');
						option.attr('value', "Default").text(
								"-Select Location Area-");
						$("#locationareaId").append(option);

						var option = $('<option/>');
						option.attr('value', "Default").text("-Select City-");
						$("#cityId").append(option);

						var option = $('<option/>');
						option.attr('value', "Default").text("-Select State-");
						$("#stateId").append(option);

						for (var i = 0; i < result.length; i++) {
							var option = $('<option />');
							option.attr('value', result[i].stateId).text(
									result[i].stateName);
							$("#stateId").append(option);
						}
					} else {
						alert("failure111");
						//$("#ajax_div").hide();
					}

				}
			});

		}//end of get State List

		function getCityList() {

			$("#cityId").empty();
			$("#locationareaId").empty();
			var stateId = $('#stateId').val();
			var countryId = $('#countryId').val();
			$.ajax({

				url : '${pageContext.request.contextPath}/getCityList',
				type : 'Post',
				data : {
					stateId : stateId,
					countryId : countryId
				},
				dataType : 'json',
				success : function(result) {
					if (result) {
						var option = $('<option/>');
						option.attr('value', "Default").text(
								"-Select Location Area-");
						$("#locationareaId").append(option);

						var option = $('<option/>');
						option.attr('value', "Default").text("-Select City-");
						$("#cityId").append(option);

						for (var i = 0; i < result.length; i++) {
							var option = $('<option />');
							option.attr('value', result[i].cityId).text(
									result[i].cityName);
							$("#cityId").append(option);
						}
					} else {
						alert("failure111");
						//$("#ajax_div").hide();
					}

				}
			});

		}//end of get City List

		function getLocationAreaList() {
			$("#locationareaId").empty();
			var cityId = $('#cityId').val();
			var stateId = $('#stateId').val();
			var countryId = $('#countryId').val();
			$.ajax({

				url : '${pageContext.request.contextPath}/getLocationAreaList',
				type : 'Post',
				data : {
					cityId : cityId,
					stateId : stateId,
					countryId : countryId
				},
				dataType : 'json',
				success : function(result) {
					if (result) {
						var option = $('<option/>');
						option.attr('value', "Default").text(
								"-Select Location Area-");
						$("#locationareaId").append(option);
						for (var i = 0; i < result.length; i++) {
							var option = $('<option />');
							option.attr('value', result[i].locationareaId)
									.text(result[i].locationareaName);
							$("#locationareaId").append(option);
						}
					} else {
						alert("failure111");
						//$("#ajax_div").hide();
					}

				}
			});
		}//end of get locationarea List

		function getprojectList() {

			var projectName = $('#projectName').val();
			$
					.ajax({

						url : '${pageContext.request.contextPath}/getallprojectList',
						type : 'Post',
						data : {
							projectName : projectName
						},
						dataType : 'json',
						success : function(result) {
							if (result) {

								for (var i = 0; i < result.length; i++) {

									if (result[i].projectName == projectName) {

										//document.stateform.stateId.value="";
										document.projectform.projectName
												.focus();
										$('#projectNameSpan')
												.html(
														'This project name is already exist..!');
										// $('#stateId').val("");

									}

								}
							} else {
								alert("failure111");
								//$("#ajax_div").hide();
							}

						}
					});
		}

		$(function() {
			//Initialize Select2 Elements
			$('.select2').select2()

			//Datemask dd/mm/yyyy
			$('#datemask').inputmask('dd/mm/yyyy', {
				'placeholder' : 'dd/mm/yyyy'
			})
			//Datemask2 mm/dd/yyyy
			$('#datemask2').inputmask('mm/dd/yyyy', {
				'placeholder' : 'mm/dd/yyyy'
			})
			//Money Euro
			$('[data-mask]').inputmask()

			//Date range picker
			$('#reservation').daterangepicker()
			//Date range picker with time picker
			$('#reservationtime').daterangepicker({
				timePicker : true,
				timePickerIncrement : 30,
				format : 'MM/DD/YYYY h:mm A'
			})
			//Date range as a button
			$('#daterange-btn').daterangepicker(
					{
						ranges : {
							'Today' : [ moment(), moment() ],
							'Yesterday' : [ moment().subtract(1, 'days'),
									moment().subtract(1, 'days') ],
							'Last 7 Days' : [ moment().subtract(6, 'days'),
									moment() ],
							'Last 30 Days' : [ moment().subtract(29, 'days'),
									moment() ],
							'This Month' : [ moment().startOf('month'),
									moment().endOf('month') ],
							'Last Month' : [
									moment().subtract(1, 'month').startOf(
											'month'),
									moment().subtract(1, 'month')
											.endOf('month') ]
						},
						startDate : moment().subtract(29, 'days'),
						endDate : moment()
					},
					function(start, end) {
						$('#daterange-btn span').html(
								start.format('MMMM D, YYYY') + ' - '
										+ end.format('MMMM D, YYYY'))
					})

			//Date picker
			$('#projectstartDate').datepicker({
				autoclose : true
			})
			$('#projectendDate').datepicker({
				autoclose : true
			})

			//iCheck for checkbox and radio inputs
			$('input[type="checkbox"].minimal, input[type="radio"].minimal')
					.iCheck({
						checkboxClass : 'icheckbox_minimal-blue',
						radioClass : 'iradio_minimal-blue'
					})
			//Red color scheme for iCheck
			$(
					'input[type="checkbox"].minimal-red, input[type="radio"].minimal-red')
					.iCheck({
						checkboxClass : 'icheckbox_minimal-red',
						radioClass : 'iradio_minimal-red'
					})
			//Flat red color scheme for iCheck
			$('input[type="checkbox"].flat-red, input[type="radio"].flat-red')
					.iCheck({
						checkboxClass : 'icheckbox_flat-green',
						radioClass : 'iradio_flat-green'
					})

			//Colorpicker
			$('.my-colorpicker1').colorpicker()
			//color picker with addon
			$('.my-colorpicker2').colorpicker()

			//Timepicker
			$('.timepicker').timepicker({
				showInputs : false
			})
		})
	</script>
</body>
</html>
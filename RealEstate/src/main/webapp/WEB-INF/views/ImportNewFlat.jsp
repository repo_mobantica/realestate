<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate |Import New Flat</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">
<%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
  <%@ include file="menu.jsp" %>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Import Flat Details From Excel File:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Import Flats From Excel(.xlsx) File </li>
      </ol>
    </section>

<form name="importnewflatform" action="${pageContext.request.contextPath}/ImportNewFlat" onSubmit="return validate()" method="post" enctype="multipart/form-data">
    <!-- Main content -->
    <section class="content">


      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        	<div class="box-body">
				<div class="form-horizontal">
											<div class="row">
					<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<label for="profile_img1" class="col-sm-4 control-label">Excel File</label>
								<div class="col-sm-5">
									<input type="file" accept=".xls,.xlsx,.csv" class="form-control" id="flat_excel" name="flat_excel" required="required"/>
									<label id="profile_img1-error"></label>
								</div>
								
								<div id="divchanged1">
                	 </div>
							</div>
					</div>
				
						</div>
				</div>
					<div class="box-footer" style="text-align: center;">
			
			
				<div class="box-body">
              <div class="row">
              </br>
                
				  <div class="col-xs-4">
				<a href="FlatMaster"><button type="button" class="btn btn-default" style="width:90px">Back</button></a>
				
				</div>
				  <div class="col-xs-4">
				<input type="submit" class="btn btn-success" value="Import"/>&nbsp
	
				</div>
				</div>
				</div>
			</div>
			  <div class="box-header with-border">
                  <i class="fa fa-text-width"></i>
                  <h3 class="box-title">Import Instructions</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <dl class="dl-horizontal">
                  
                    <dt>Template</dt>
                    <dd>Use Standard Template only for Import.
                     You can download it from <a href="DownloadFlatTemplate">here</a>. </dd>
                  
                  </dl>
                 
                </div>
			</div>
			
	<div class="box box-default" id="flatListStatus" hidden="hidden">
        <div class="box-body">
        <h4>Rejected List</h4>
              <div class="table-responsive">
                <table class="table table-bordered" id="flatListTable" >
                  <thead>
                  <tr bgcolor=#4682B4>
	                    <th>Excel Row No.</th>
	                    <th>Project Name</th>
	                    <th>Building Name</th>
	                    <th>Wing Name</th>
	                    <th>Flat Number</th>
	                    <th>Flat Facing</th>
                    	<th>Flat Type</th>
                    	<th>Msg</th>
                  </tr>
                  </thead>
                  <tbody>
                   <c:forEach items="${flatRejectedList}" var="flatRejectedList" varStatus="loopStatus">
                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
	                        <td>${flatRejectedList.flatId}</td>
	                        <td>${flatRejectedList.projectId}</td>
	                        <td>${flatRejectedList.buildingId}</td>
	                        <td>${flatRejectedList.wingId}</td>
	                        <td>${flatRejectedList.flatNumber}</td>
	                        <td>${flatRejectedList.flatfacingName}</td>
	                        <td>${flatRejectedList.flatType}</td>
	                        <td>${flatRejectedList.flatstatus}</td>
                      </tr>
					</c:forEach>
                 
                 </tbody>
                </table>
              </div>
            </div>
				
              <!-- /.form-group -->
          		<input type="hidden" id="flatListSize" name="flatListSize" value="${flatListSize}">
          		
        <!-- /.box-body -->
          
      </div>
     
   </section>
	</form>
    <!-- /.content -->
  </div>
  
     <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script>

function init()
{
	 var flatListSize =Number($('#flatListSize').val());
	 if(flatListSize!=0)
		 {
		 document.getElementById('flatListStatus').style.display = 'block';
		 }
 }
 


$(function () {
    $('#flatListTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>

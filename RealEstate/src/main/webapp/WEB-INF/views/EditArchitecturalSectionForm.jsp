<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Architectural</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


 <style type="text/css">
    tr.odd {background-color: #CCE5FF}
    tr.even {background-color: #F0F8FF}
 </style>

<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
 
 <script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
 </script>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
    		
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>

<div class="wrapper">

  <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
         Architectural Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Architectural</a></li>
        <li class="active">Architectural Section</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="architecturalSectionForm" action="${pageContext.request.contextPath}/EditArchitecturalSectionForm" method="post" onSubmit="return validate()">
    <section class="content">

    <div class="box box-default">
      <div class="box-body">
          <div class="row">
            <div class="col-md-12">
             <span id="statusSpan" style="color:#008000"></span>
				
			<div class="box-body">
              <div class="row">
                  <div class="col-xs-2">
                  <label>Id</label>
                  <input type="text" class="form-control"  id="projectArchitecturalId"name="projectArchitecturalId"  value="${projectArchitecturalCode}" readonly>
                </div>               
                
                 <div class="col-xs-2">
                  <label>Project Name</label>
                  <input type="hidden" class="form-control"  id="projectId"name="projectId"  value="${projectarchitecturalsectionFormDetails[0].projectId}" readonly>
                  <input type="text" class="form-control" value="${projectName}" readonly>
                </div>               
                
                
                <div class="col-xs-3">
                  <label>Architectural Name </label> <label class="text-red">* </label>
                     <input type="hidden" class="form-control"  id="consultancyId"name="consultancyId" value="${projectarchitecturalsectionFormDetails[0].consultancyId}" readonly>
                     
                    <input type="text" class="form-control"   value="${consultancyfirmName}" readonly>
                    
                 <%--  
                  <select class="form-control" name="architecturalId" id="architecturalId" >
                  
				  	   <option selected="" value="${projectarchitecturalsectionFormDetails[0].architecturalId}">${architecturalFirmName}</option>
                        <c:forEach var="architecturalList" items="${architecturalList}">
	                    <c:choose>
	                     <c:when test="${projectarchitecturalsectionFormDetails[0].architecturalId ne architecturalList.architecturalId}">
	                 	  	<option value="${architecturalList.architecturalId}">${architecturalList.architecturalfirmName}</option>
	                     </c:when>
	                    </c:choose>
				     </c:forEach>
                  
                   </select>
                    --%>
                  <span id="architecturalIdSpan" style="color:#FF0000"></span>
                </div>  
              </div>
            </div>
      
        <div class="panel box box-danger"></div>
              
		<div class="box-body">
			<div class="row">
			<div id="dvTable">
			
			</div>
			
              <div class="col-xs-12">
              <div class="table-responsive">
                <table class="table table-bordered" id="architecturalListTable">
                
                  <thead>
	                  <tr bgcolor=#4682B4>
	                    <th>A</th>
	                    <th>AREA STATEMENT</th>
	                    <th>Per</th>
	                    <th>Add Area in SQ.M.</th>
	                  </tr>
                  </thead>
                 
	                  <tr>
	                   <td>1a</td>
	                   <td>AREA OF PLOT AS PER 7/12 EXTRACT</td>
	                   <td> <label id="aeraOfPlotAsPer7_12ExtractPer">${projectarchitecturalsectionFormDetails[0].aeraOfPlotAsPer7_12ExtractPer}</label> </td>
	                   <td> <input type="text" class="form-control"  id="aeraOfPlotAsPer7_12ExtractArea"name="aeraOfPlotAsPer7_12ExtractArea"  onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                  
	                  <tr>
	                   <td>1b</td>
	                   <td>AREA OF PLOT AS PER PROPERTY CARD</td>
	                       <td> <label id="areaOfPlotAsPerPropetyPer">${projectarchitecturalsectionFormDetails[0].areaOfPlotAsPerPropetyPer}</label> </td>
	                   <td> <input type="text" class="form-control"  id="areaOfPlotAsPerPropetyArea"name="areaOfPlotAsPerPropetyArea"></td>
	                  </tr>
	                  
	                  <tr>
	                   <td>1c</td>
	                   <td>AREA OF PLOT AS PER DEMARCATION</td>
	                      <td> <label id="areaOfPlotAsPerDemarcationPer">${projectarchitecturalsectionFormDetails[0].areaOfPlotAsPerDemarcationPer}</label> </td>
	                   <td> <input type="text" class="form-control"  id="areaOfPlotAsPerDemarcationArea"name="areaOfPlotAsPerDemarcationArea"></td>
	                  </tr>
	                  
	                  <tr>
	                   <td>1d</td>
	                   <td>AREA AS PER SANCTIONED LAYOUT</td>
	                     <td> <label id="areaAsPerSanctionedLayoutPer">${projectarchitecturalsectionFormDetails[0].areaAsPerSanctionedLayoutPer}</label> </td>
	                   <td> <input type="text" class="form-control"  id="areaAsPerSanctionedLayoutArea"name="areaAsPerSanctionedLayoutArea"></td>
	                  </tr>
	                    
	                  <tr>
	                   <td>1e</td>
	                   <td>AREA AS PER U.L.C. ORDER</td>
	                     <td> <label id="areaAsPerULCOrderPer">${projectarchitecturalsectionFormDetails[0].areaAsPerULCOrderPer}</label> </td>
	                   <td> <input type="text" class="form-control"  id="areaAsPerULCOrderArea"name="areaAsPerULCOrderArea"></td>
	                  </tr>
	                     
	                  <tr>
	                   <td>1f</td>
	                   <td>AREA AS PER DEVELOPMENT AGREEMENT /P.O.A.H.</td>
	                     <td> <label id="areaAsPerDevelopmentAgreementPer">${projectarchitecturalsectionFormDetails[0].areaAsPerDevelopmentAgreementPer}</label> </td>  
	                   <td> <input type="text" class="form-control"  id="areaAsPerDevelopmentAgreementArea" name="areaAsPerDevelopmentAgreementArea"></td>
	                  </tr>
	                     
	                  <tr>
	                   <td>1g</td>
	                   <td>AREA OF PLOT MINIMUM CONSIDERATION</td>
	                     <td> <label id="areaOfPlotMinimumConsiderationPer">${projectarchitecturalsectionFormDetails[0].areaOfPlotMinimumConsiderationPer}</label> </td> 
	                   <td> <input type="text" class="form-control"  id="areaOfPlotMinimumConsiderationArea"name="areaOfPlotMinimumConsiderationArea"></td>
	                  </tr>
	                  
	               <thead>
	                  <tr>
	                    <th>2</th>
	                    <th>DEDUCTIONS FOR</th>
	                    
	                  </tr>
                  </thead> 
                     
	                  <tr>
	                   <td>2a</td>
	                   <td>AREA UNDER B.R.T. CORRIDOR</td>
	                      <td> <label id="areaUnderBRTCorridorPer">${projectarchitecturalsectionFormDetails[0].areaUnderBRTCorridorPer}</label> </td> 
	                   <td> <input type="text" class="form-control"  id="areaUnderBRTCorridorArea"name="areaUnderBRTCorridorArea" onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                     
	                  <tr>
	                   <td>2b</td>
	                   <td>AREA UNDER 1.50 M. ROAD WIDENING</td>
	                     <td> <label id="areaUnder1_50MRoadWideningPer">${projectarchitecturalsectionFormDetails[0].areaUnder1_50MRoadWideningPer}</label> </td>
	                   <td> <input type="text" class="form-control"  id="areaUnder1_50MRoadWideningArea"name="areaUnder1_50MRoadWideningArea" onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                  
	                      
	                  <tr>
	                   <td>2c</td>
	                   <td>AREA UNDER 3.00 M. ROAD WIDENING</td>
	                     <td> <label id="areaUnder3_00MRoadWideningPer">${projectarchitecturalsectionFormDetails[0].areaUnder3_00MRoadWideningPer}</label> </td>  
	                   <td> <input type="text" class="form-control"  id="areaUnder3_00MRoadWideningArea"name="areaUnder3_00MRoadWideningArea" onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                      
	                  <tr>
	                   <td>2d</td>
	                   <td>AREA UNDER 4.50 M. ROAD WIDENING</td>
	                       <td> <label id="areaUnder4_50MRoadWideningPer">${projectarchitecturalsectionFormDetails[0].areaUnder4_50MRoadWideningPer}</label> </td>
	                   <td> <input type="text" class="form-control"  id="areaUnder4_50MRoadWideningArea"name="areaUnder4_50MRoadWideningArea" onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                      
	                        
	                      
	                  <tr>
	                   <td>2e</td>
	                   <td>AREA UNDER 6.00 M. ROAD WIDENING</td>
	                       <td> <label id="areaUnder6_00MRoadWideningPer">${projectarchitecturalsectionFormDetails[0].areaUnder6_00MRoadWideningPer}</label> </td>
	                   <td> <input type="text" class="form-control"  id="areaUnder6_00MRoadWideningArea"name="areaUnder6_00MRoadWideningArea" onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                      
	                  <tr>
	                   <td>2f</td>
	                   <td>AREA UNDER 7.50 M. ROAD WIDENING</td>
	                    <td> <label id="areaUnder7_50MRoadWideningPer">${projectarchitecturalsectionFormDetails[0].areaUnder7_50MRoadWideningPer}</label> </td>   
	                   <td> <input type="text" class="form-control"  id="areaUnder7_50MRoadWideningArea"name="areaUnder7_50MRoadWideningArea" onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                      
	                              
	                      
	                  <tr>
	                   <td>2g</td>
	                   <td>AREA UNDER 9.00 M. ROAD WIDENING</td>
	                    <td> <label id="areaUnder9_00MRoadWideningPer">${projectarchitecturalsectionFormDetails[0].areaUnder9_00MRoadWideningPer}</label> </td>
	                   <td> <input type="text" class="form-control"  id="areaUnder9_00MRoadWideningArea"name="areaUnder9_00MRoadWideningArea" onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                      
	                  <tr>
	                   <td>2h</td>
	                   <td>AREA UNDER 12.00 M. ROAD WIDENING</td>
	                  <td> <label id="areaUnder12_00MRoadWideningPer">${projectarchitecturalsectionFormDetails[0].areaUnder12_00MRoadWideningPer}</label> </td>     
	                   <td> <input type="text" class="form-control"  id="areaUnder12_00MRoadWideningArea"name="areaUnder12_00MRoadWideningArea" onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                             
	                      
	                  <tr>
	                   <td>2i</td>
	                   <td>AREA UNDER 15.00 M. ROAD WIDENING</td>
	                  <td> <label id="areaUnder15_00MRoadWideningPer">${projectarchitecturalsectionFormDetails[0].areaUnder15_00MRoadWideningPer}</label> </td>     
	                   <td> <input type="text" class="form-control"  id="areaUnder15_00MRoadWideningArea"name="areaUnder15_00MRoadWideningArea" onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                      
	                  <tr>
	                   <td>2j</td>
	                   <td>AREA UNDER 18.00 M. ROAD WIDENING</td>
	                    <td> <label id="areaUnder18_00MRoadWideningPer">${projectarchitecturalsectionFormDetails[0].areaUnder18_00MRoadWideningPer}</label> </td>   
	                   <td> <input type="text" class="form-control"  id="areaUnder18_00MRoadWideningArea"name="areaUnder18_00MRoadWideningArea" onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                             
	                      
	                  <tr>
	                   <td>2k</td>
	                   <td>AREA UNDER 24.00 M. ROAD WIDENING</td>
	                    <td> <label id="areaUnder24_00MRoadWideningPer">${projectarchitecturalsectionFormDetails[0].areaUnder24_00MRoadWideningPer}</label> </td>   
	                   <td> <input type="text" class="form-control"  id="areaUnder24_00MRoadWideningArea"name="areaUnder24_00MRoadWideningArea" onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                      
	                  <tr>
	                   <td>2l</td>
	                   <td>AREA UNDER 30.00 M. ROAD WIDENING</td>
	                    <td> <label id="areaUnder30_00MRoadWideningPer">${projectarchitecturalsectionFormDetails[0].areaUnder30_00MRoadWideningPer}</label> </td>   
	                   <td> <input type="text" class="form-control"  id="areaUnder30_00MRoadWideningArea"name="areaUnder30_00MRoadWideningArea" onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                      
	                      
	                  <tr>
	                   <td>2m</td>
	                   <td>AREA UNDER SERVICE ROAD</td>
	                    <td> <label id="areaUnderServiceRoadPer">${projectarchitecturalsectionFormDetails[0].areaUnderServiceRoadPer}</label> </td>   
	                   <td> <input type="text" class="form-control"  id="areaUnderServiceRoadArea"name="areaUnderServiceRoadArea" onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                      
	                  <tr>
	                   <td>2n</td>
	                   <td>AREA UNDER RESERVATION</td>
	                    <td> <label id="areaUnderReservationPer">${projectarchitecturalsectionFormDetails[0].areaUnderReservationPer}</label> </td>   
	                   <td> <input type="text" class="form-control"  id="areaUnderReservationArea"name="areaUnderReservationArea" onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                      
	                  <tr>
	                   <td>2o</td>
	                   <td>AREA UNDER GREEN BELT</td>
	                    <td> <label id="areaUnderGreenBeltPer">${projectarchitecturalsectionFormDetails[0].areaUnderGreenBeltPer}</label> </td>   
	                   <td> <input type="text" class="form-control"  id="areaUnderGreenBeltArea"name="areaUnderGreenBeltArea" onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                          
	                  <tr>
	                   <td>2p</td>
	                   <td>AREA UNDER NALA</td>
	                   <td> <label id="areaUnderNalaPer">${projectarchitecturalsectionFormDetails[0].areaUnderNalaPer}</label> </td>    
	                   <td> <input type="text" class="form-control"  id="areaUnderNalaArea"name="areaUnderNalaArea" onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                
	                  <tr>
	                    <th></th>
	                    <th>TOTAL (a+b+c+d+e+f+g+h+i+j+k+l+m+n+o+p)</th>
	                    <th></th>
	                    <th> <input type="text" class="form-control"  id="total2Area"name="total2Area"></th>
	                  </tr>
	    
	               <thead>
	                  <tr>
	                   <th>3</th>
	                   <th>NET GROSS AREA OF PLOT (1-2)</th>
	                   <th></th>
	                   <th> <input type="text" class="form-control"  id="netGrossAreaOfPlot1_2Area"name="netGrossAreaOfPlot1_2Area"></th>
	                  </tr>
                  </thead>  
	                      
	               <thead>
	                  <tr>
	                    <th>4</th>
	                    <th>DEDUCTIONS FOR</th>
	                  </tr>
                  </thead>  
	                             
	                  <tr>
	                   <td>4a</td>
	                   <td>RECREATION GROUND 10%</td>
	                   <td> <label id="recreationGround10Per">${projectarchitecturalsectionFormDetails[0].recreationGround10Per}</label> </td> 
	                   <td> <input type="text" class="form-control"  id="recreationGround10Area"name="recreationGround10Area"></td>
	                  </tr>
	                             
	                  <tr>
	                   <td>4b</td>
	                   <td>AMENITY SPACE (5%)</td>
	                      <td> <label id="amenitySpace5Per">${projectarchitecturalsectionFormDetails[0].amenitySpace5Per}</label> </td> 
	                   <td> <input type="text" class="form-control"  id="amenitySpace5Area"name="amenitySpace5Area"></td>
	                  </tr>
	                             
	                  <tr>
	                   <td>4c</td>
	                   <td>INTERNAL ROADS</td>
	                      <td> <label id="internalRoadsPer">${projectarchitecturalsectionFormDetails[0].internalRoadsPer}</label> </td> 
	                   <td> <input type="text" class="form-control"  id="internalRoadsArea"name="internalRoadsArea"  onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                             
	                  <tr>
	                   <td>4b</td>
	                   <td>TRANSFORMER</td>
	                       <td> <label id="transformerPer">${projectarchitecturalsectionFormDetails[0].transformerPer}</label> </td>
	                   <td> <input type="text" class="form-control"  id="transformerArea"name="transformerArea" onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                                  
	                  <tr>
	                   <td></td>
	                   <td>TOTAL (a+b+c+d)</td>
	                   <td></td>
	                   <td> <input type="text" class="form-control"  id="total4Area"name="total4Area"></td>
	                  </tr>
	                      
	               <thead>
	                  <tr>
	                    <th>5</th>
	                    <th>NET AREA OF PLOT (3-4)</th>
	                    <th></th>
	                    <th> <input type="text" class="form-control"  id="netAreaOfPlot3_4Area"name="netAreaOfPlot3_4Area"></th>
	                  </tr>
                  </thead> 
                                 
	               <thead>
	                  <tr>
	                    <th>6</th>
	                    <th>ADDITION FOR</th>
	                    <th></th>
	                  </tr>
                  </thead> 
                                 
	                  <tr>
	                   <td>6a</td>
	                   <td>T.D.R.( X 40%)</td>
	                      <td> <label id="tdr40Per">${projectarchitecturalsectionFormDetails[0].tdr40Per}</label> </td> 
	                   <td> <input type="text" class="form-control"  id="tdr40Area"name="tdr40Area"  onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                             
	                  <tr>
	                   <td>6b</td>
	                   <td>ADDITIONS FOR F.A.R. 4a  (RECREATION GROUND)</td>
	                     <td> <label id="additionsForFAR4a_RecreationGroundPer">${projectarchitecturalsectionFormDetails[0].additionsForFAR4a_RecreationGroundPer}</label> </td>  
	                   <td> <input type="text" class="form-control"  id="additionsForFAR4a_RecreationGroundArea"name="additionsForFAR4a_RecreationGroundArea"  onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                             
	                  <tr>
	                   <td>6c</td>
	                   <td>ADDITIONS FOR F.A.R. 4b  (AMENITY SPACE)</td>
	                     <td> <label id="additionForFAR4b_AmenitySpacePer">${projectarchitecturalsectionFormDetails[0].additionForFAR4b_AmenitySpacePer}</label> </td>  
	                   <td> <input type="text" class="form-control"  id="additionForFAR4b_AmenitySpaceArea"name="additionForFAR4b_AmenitySpaceArea"  onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                             
	                  <tr>
	                   <td>6d</td>
	                   <td>ADDITIONS FOR F.A.R. 4c  (INTERNALS ROAD)</td>
	                       <td> <label id="additionForFAR4c_InternalsRoadPer">${projectarchitecturalsectionFormDetails[0].additionForFAR4c_InternalsRoadPer}</label> </td>
	                   <td> <input type="text" class="form-control"  id="additionForFAR4c_InternalsRoadArea"name="additionForFAR4c_InternalsRoadArea"  onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                              
	                  <tr>
	                   <td>6e</td>
	                   <td>ADDITIONS FOR F.A.R. 4d  (TRANSFORMER)</td>
	                      <td> <label id="additionForFAR4d_TransformerPer">${projectarchitecturalsectionFormDetails[0].additionForFAR4d_TransformerPer}</label> </td> 
	                   <td> <input type="text" class="form-control"  id="additionForFAR4d_TransformerArea"name="additionForFAR4d_TransformerArea"  onchange="Calculatetotal2Cal(this.value)"></td>
	                  </tr>
	                                  
	                  <tr>
	                   <td></td>
	                   <td>TOTAL (a+b+c+d+e)</td>
	                   <td></td>
	                   <td> <input type="text" class="form-control"  id="total6Area"name="total6Area"></td>
	                  </tr>
	                     
	               <thead>
	                  <tr>
	                    <th>7</th>
	                    <th>TOTAL AREA (5+6)</th>
	                    <th></th>
	                    <th> <input type="text" class="form-control"  id="totalArea5_6Area"name="totalArea5_6Area"></th>
	                  </tr>
                  </thead> 
                     
	               <thead>
	                  <tr>
	                    <th>8</th>
	                    <th>F.A.R. PERMISSIBLE</th>
	                      <td> <label id="farPermissiblePer">${projectarchitecturalsectionFormDetails[0].farPermissiblePer}</label> </td>  
	                    <th> <input type="text" class="form-control"  id="farPermissibleArea"name="farPermissibleArea"  onchange="Calculatetotal2Cal(this.value)"></th>
	                  </tr>
                  </thead> 
                                            
	               <thead>
	                  <tr>
	                    <th>9</th>
	                    <th>TOTAL PERMISSIBLE FLOOR AREA (7X8)</th>
	                    <th></th>
	                    <th> <input type="text" class="form-control"  id="totalPermissibleFloorArea7_8Area"name="totalPermissibleFloorArea7_8Area" onchange="Calculatetotal2Cal(this.value)"></th>
	                  </tr>
                  </thead> 
                                          
	               <thead>
	                  <tr>
	                    <th>10</th>
	                    <th>PERMISSIBLE RESIDENTIAL AREA</th>
	                      <th> <label id="permissibleResidentialPer">${projectarchitecturalsectionFormDetails[0].permissibleResidentialPer}</label> </th>  
	                    <th> <input type="text" class="form-control"  id="permissibleResidentialArea"name="permissibleResidentialArea"></th>
	                  </tr>
                  </thead> 
                                            
	               <thead>
	                  <tr>
	                    <th>11</th>
	                    <th>EXISTING RESIDENTIAL AREA</th>
	                      <th> <label id="existingResidentialPer">${projectarchitecturalsectionFormDetails[0].existingResidentialPer}</label> </th>  
	                    <th> <input type="text" class="form-control"  id="existingResidentialArea"name="existingResidentialArea" onchange="CalculatetotalResidentialArea11_12Area(this.value)"></th>
	                  </tr>
                  </thead> 
                                            
	               <thead>
	                  <tr>
	                    <th>12</th>
	                    <th>PROPOSED RESIDENTIAL AREA</th>
	                     <th> <label id="proposedResidentialPer">${projectarchitecturalsectionFormDetails[0].proposedResidentialPer}</label> </th>    
	                    <th> <input type="text" class="form-control"  id="proposedResidentialArea"name="proposedResidentialArea" onchange="CalculatetotalResidentialArea11_12Area(this.value)"></th>
	                  </tr>
                  </thead> 
                                            
	               <thead>
	                  <tr>
	                    <th>13</th>
	                    <th>TOTAL RESIDENTIAL AREA (11+12)</th>
	                   <th></th> 
	                    <th> <input type="text" class="form-control"  id="totalResidentialArea11_12Area"name="totalResidentialArea11_12Area" onchange="CalculatetotalResidentialArea11_12Area(this.value)"></th>
	                  </tr>
                  </thead> 
                                            
	               <thead>
	                  <tr>
	                    <th>14</th>
	                    <th>PERMISSIBLE COMMERCIAL/INDUSTRIAL AREA</th>
	                       <th> <label id="permissibleCommercial_IndustrialPer">${projectarchitecturalsectionFormDetails[0].permissibleCommercial_IndustrialPer}</label> </th>  
	                    <th> <input type="text" class="form-control"  id="permissibleCommercial_IndustrialArea"name="permissibleCommercial_IndustrialArea" onchange="CalculatetotalResidentialArea11_12Area(this.value)"></th>
	                  </tr>
                  </thead> 
                                             
	               <thead>
	                  <tr>
	                    <th>15</th>
	                    <th>EXISTING COMMERCIAL/INDUSTRIAL AREA</th>
	                    <th> <label id="existingCommercial_IndustrialPer">${projectarchitecturalsectionFormDetails[0].existingCommercial_IndustrialPer}</label> </th>     
	                    <th> <input type="text" class="form-control"  id="existingCommercial_IndustrialArea"name="existingCommercial_IndustrialArea" onchange="CalculatetotalResidentialArea11_12Area(this.value)"></th>
	                  </tr>
                  </thead> 
                                             
	               <thead>
	                  <tr>
	                    <th>16</th>
	                    <th>PROPOSED COMMERCIAL/INDUSTRIAL AREA</th>
	                     <th> <label id="proposedCommercial_IndustrialPer">${projectarchitecturalsectionFormDetails[0].proposedCommercial_IndustrialPer}</label> </th>    
	                    <th> <input type="text" class="form-control"  id="proposedCommercial_IndustrialArea"name="proposedCommercial_IndustrialArea" onchange="CalculatetotalResidentialArea11_12Area(this.value)"></th>
	                  </tr>
                  </thead> 
                           
	               <thead>
	                  <tr>
	                    <th>17</th>
	                    <th>TOTAL COMMERCIAL/INDUSTRIAL AREA  (15+16)</th>
	                      <th></th>   
	                    <th> <input type="text" class="form-control"  id="totalCommercial_IndustrialArea15_16Area"name="totalCommercial_IndustrialArea15_16Area" onchange="CalculatetotalResidentialArea11_12Area(this.value)"></th>
	                  </tr>
                  </thead> 
                                             
	               <thead>
	                  <tr>
	                    <th>18</th>
	                    <th>TOTAL EXISTING AREA (11+15)</th>
	                    <th></th>
	                    <th> <input type="text" class="form-control"  id="totalExistingArea11_15"name="totalExistingArea11_15" onchange="CalculatetotalResidentialArea11_12Area(this.value)"></th>
	                  </tr>
                  </thead> 
                                             
	               <thead>
	                  <tr>
	                    <th>19</th>
	                    <th>TOTAL PROPOSED AREA (12+16)</th>
	                    <th></th>
	                    <th> <input type="text" class="form-control"  id="totalProposedArea12_16Area"name="totalProposedArea12_16Area" onchange="CalculatetotalResidentialArea11_12Area(this.value)"></th>
	                  </tr>
                  </thead> 
                                             
	               <thead>
	                  <tr>
	                    <th>20</th>
	                    <th>TOTAL EXISTING AREA + PROPOSED (18+19)</th>
	                    <th></th>
	                    <th> <input type="text" class="form-control"  id="totalExistingArea_Proposed18_19Area"name="totalExistingArea_Proposed18_19Area" onchange="CalculatetotalResidentialArea11_12Area(this.value)"></th>
	                  </tr>
                  </thead> 
                                             
	               <thead>
	                  <tr>
	                    <th>21</th>
	                    <th>EXCESS BALCONY AREA TAKEN IN F.S.I. </th>
	                     <th> <label id="excessBalconyAreaTakenInFSIPer">${projectarchitecturalsectionFormDetails[0].excessBalconyAreaTakenInFSIPer}</label> </th>
	                    <th> <input type="text" class="form-control"  id="excessBalconyAreaTakenInFSIArea"name="excessBalconyAreaTakenInFSIArea" onchange="CalculatetotalResidentialArea11_12Area(this.value)"></th>
	                  </tr>
                  </thead> 
                                             
	               <thead>
	                  <tr>
	                    <th>22</th>
	                    <th>TOTAL B'UP AREA (20+21)</th>
	                       <th>  </th>  
	                    <th> <input type="text" class="form-control"  id="totalBupArea20_21Area"name="totalBupArea20_21Area" onchange="CalculatetotalResidentialArea11_12Area(this.value)"></th>
	                  </tr>
                  </thead> 
                                             
	               <thead>
	                  <tr>
	                    <th>23</th>
	                    <th>TOTAL F.S.I. CONSUMED</th>
	                       <th> <label id="totalFSIConSumedPer">${projectarchitecturalsectionFormDetails[0].totalFSIConSumedPer}</label> </th>  
	                    <th> <input type="text" class="form-control"  id="totalFSIConSumedArea"name="totalFSIConSumedArea"></th>
	                  </tr>
                  </thead> 
                    
	               <thead>
	                  <tr>
	                    <th>B</th>
	                    <th>BALCONY AREA STATEMENT</th>
	                    <th></th>
	                    <th></th>
	                  </tr>
                  </thead> 
                    
	                  <tr>
	                   <td>a</td>
	                   <td>PERMISSIBLE BALCONY AREA</td>
	                      <th> <label id="permissibleBalconyPer">${projectarchitecturalsectionFormDetails[0].permissibleBalconyPer}</label> </th>  
	                    <td> <input type="text" class="form-control"  id="permissibleBalconyArea"name="permissibleBalconyArea"  onchange="CalculateexcessBalconyArea(this.value)"></td>
	                 
	                  <tr>
	                   <td>b</td>
	                   <td>PROPOSED BALCONY AREA</td>
	                      <td> <label id="proposedBalconyPer">${projectarchitecturalsectionFormDetails[0].proposedBalconyPer}</label> </td>  
	                    <td> <input type="text" class="form-control"  id="proposedBalconyArea"name="proposedBalconyArea" onchange="CalculateexcessBalconyArea(this.value)"></td>
	                 
	                  </tr>
	                   
	                  <tr>
	                   <td>c</td>
	                   <td>EXCESS BALCONY AREA (TOTAL)</td>
	                    <td></td>  
	                    <td> <input type="text" class="form-control"  id="excessBalconyArea"name="excessBalconyArea"></td>
	                 
	                  </tr>
	                   	      
	               <thead>
	                  <tr>
	                    <th>C</th>
	                    <th>TENEMENT STATEMENT</th>
	                   <th></th>
	                  </tr>
                  </thead> 
                          
	                  <tr>
	                   <td>a</td>
	                   <td>NET GROSS AREA OF PLOT (A7)</td>
	                    <td> <label id="netGrossAreaOfPlot_A7_Per">${projectarchitecturalsectionFormDetails[0].netGrossAreaOfPlot_A7_Per}</label> </td>  
	                    <td> <input type="text" class="form-control"  id="netGrossAreaOfPlot_A7_Area"name="netGrossAreaOfPlot_A7_Area" onchange="CalculatetenenmentsPermissibleArea(this.value)"></td>
	                 
	                  </tr>
	                      
	                  <tr>
	                   <td>b</td>
	                   <td>LESS DEDUCTION FOR NON-RESIDENTIAL AREA</td>
	                    <td> <label id="lessDeductionForNon_ResidentialPer">${projectarchitecturalsectionFormDetails[0].lessDeductionForNon_ResidentialPer}</label> </td> 
	                    <td> <input type="text" class="form-control"  id="lessDeductionForNon_ResidentialArea"name="lessDeductionForNon_ResidentialArea" onchange="CalculatetenenmentsPermissibleArea(this.value)"></td>
	                  </tr>
	                      
	                  <tr>
	                   <td>c</td>
	                   <td>AREA OF TENEMENTS (a-b)</td>
	                    <td></td> 
	                    <td> <input type="text" class="form-control"  id="areaOfTenementsa_bArea"name="areaOfTenementsa_bArea" onchange="CalculatetenenmentsPermissibleArea(this.value)"></td>
	                  </tr>
	                          
	                  <tr>
	                   <td>d</td>
	                   <td>TENEMENTS PERMISSIBLE</td>
	                    <td> <label id="tenenmentsPermissiblePer">${projectarchitecturalsectionFormDetails[0].tenenmentsPermissiblePer}</label> </td> 
	                    <td> <input type="text" class="form-control"  id="tenenmentsPermissibleArea"name="tenenmentsPermissibleArea" onchange="CalculatetenenmentsPermissibleArea(this.value)"></td>
	                 
	                  </tr>
	                         
	                  <tr>
	                   <td>e</td>
	                   <td>TOTAL TENEMENTS PROPOSED</td>
	                    <td> <label id="totalTenementsProposedPer">${projectarchitecturalsectionFormDetails[0].totalTenementsProposedPer}</label> </td> 
	                    <td> <input type="text" class="form-control"  id="totalTenementsProposedArea"name="totalTenementsProposedArea"></td>
	                  </tr>
	                                        
	               <thead>
	                  <tr>
	                    <th>D</th>
	                    <th>GROUND COVERAGE  STATEMENT</th>
	                  </tr>
                  </thead> 
                                             
	                  <tr>
	                   <td>a</td>
	                   <td>NET  PLOT AREA</td>
	                   <td> <label id="netPlotAreaPer">${projectarchitecturalsectionFormDetails[0].netPlotAreaPer}</label> </td> 
	                    <td> <input type="text" class="form-control"  id="netPlotAreaArea"name="netPlotAreaArea" onchange="CalculateexcessGroundConveraged_bArea(this.value)"></td>
	                  </tr>
	                            
	                  <tr>
	                   <td>b</td>
	                   <td>PERMISSIBLE GROUND COVERAGE </td>
	                    <td> <label id="permissibleGroundCoveragePer">${projectarchitecturalsectionFormDetails[0].permissibleGroundCoveragePer}</label> </td> 
	                    <td> <input type="text" class="form-control"  id="permissibleGroundCoverageArea"name="permissibleGroundCoverageArea" onchange="CalculateexcessGroundConveraged_bArea(this.value)"></td>
	                  </tr>
	                            
	                  <tr>
	                   <td>c</td>
	                   <td>EXISTING GROUND COVERAGE</td>
	                    <td> <label id="existingGroundCoveragePer">${projectarchitecturalsectionFormDetails[0].existingGroundCoveragePer}</label> </td> 
	                    <td> <input type="text" class="form-control"  id="existingGroundCoverageArea"name="existingGroundCoverageArea" onchange="CalculateexcessGroundConveraged_bArea(this.value)"></td>
	                  </tr>
	                            
	                  <tr>
	                   <td>d</td>
	                   <td>PROPOSED GROUND COVERAGE</td>
	                    <td> <label id="proposedGroundCoveragePer">${projectarchitecturalsectionFormDetails[0].proposedGroundCoveragePer}</label> </td> 
	                    <td> <input type="text" class="form-control"  id="proposedGroundCoverageArea"name="proposedGroundCoverageArea" onchange="CalculateexcessGroundConveraged_bArea(this.value)"></td>
	                  </tr>
	                            
	                  <tr>
	                   <td>e</td>
	                   <td>PERMISSIBLE GROUND COVERAGE IN PREMIUM  15% OF</td>
	                    <td> <label id="permissibleGroundCoverageInPremium15Per">${projectarchitecturalsectionFormDetails[0].permissibleGroundCoverageInPremium15Per}</label> </td> 
	                    <td> <input type="text" class="form-control"  id="permissibleGroundCoverageInPremium15Area"name="permissibleGroundCoverageInPremium15Area" onchange="CalculateexcessGroundConveraged_bArea(this.value)"></td>
	                  </tr>
	                            
	                  <tr>
	                   <td>f</td>
	                   <td>EXCESS GROUND COVERAGE (d-b)</td>
	                   <td></td>  
	                    <td> <input type="text" class="form-control"  id="excessGroundConveraged_bArea"name="excessGroundConveraged_bArea"></td>
	                  </tr>
	                          
                </table>
              </div>
              </div>
              
              </div>
            </div> 
             
           <div class="box-body">
            <div class="row">
              </br>
                <div class="col-xs-1">
                </div>
                   <div class="col-xs-4">
                   <div class="col-xs-2">
                	<a href="ArchitecturalSectionMaster"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
			     </div>
			     </div>
				  <div class="col-xs-4">
                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
              
			     </div>
				 <div class="col-xs-2">
			      <button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
			     </div> 
			
              </div>
		  </div>   
           
  	          <input type="hidden" id="status" name="status" value="Active">	
			  <input type="hidden" id="architecturalStatus" name="architecturalStatus" value="${architecturalStatus}">	
			  <input type="hidden" id="creationDate" name="creationDate" value="${projectarchitecturalsectionFormDetails[0].creationDate}">
			  <input type="hidden" id="updateDate" name="updateDate" value="">
			  <input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>">              
 	</div>
 </div>	   
  </div>
 </div>
</section>
</form>
</div>            
              
 <%@ include file="footer.jsp" %>
<div class="control-sidebar-bg"></div>
</div> 
<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script>
function Calculatetotal2Cal()
{
	//1st
	var aeraOfPlotAsPer7_12ExtractArea = Number($('#aeraOfPlotAsPer7_12ExtractArea').val());
	
	//2nd
	 var areaUnderBRTCorridorArea =Number($('#areaUnderBRTCorridorArea').val());
	 var areaUnder1_50MRoadWideningArea = Number($('#areaUnder1_50MRoadWideningArea').val());
	 var areaUnder3_00MRoadWideningArea = Number($('#areaUnder3_00MRoadWideningArea').val());
	 var areaUnder4_50MRoadWideningArea = Number($('#areaUnder4_50MRoadWideningArea').val());
	 var areaUnder6_00MRoadWideningArea = Number($('#areaUnder6_00MRoadWideningArea').val());
	 var areaUnder7_50MRoadWideningArea =Number($('#areaUnder7_50MRoadWideningArea').val());
	 var areaUnder9_00MRoadWideningArea = Number($('#areaUnder9_00MRoadWideningArea').val());
	 var areaUnder12_00MRoadWideningArea = Number($('#areaUnder12_00MRoadWideningArea').val());
	 var areaUnder15_00MRoadWideningArea = Number($('#areaUnder15_00MRoadWideningArea').val());
	 var areaUnder18_00MRoadWideningArea = Number($('#areaUnder18_00MRoadWideningArea').val());
	 var areaUnder24_00MRoadWideningArea =Number($('#areaUnder24_00MRoadWideningArea').val());
	 var areaUnder30_00MRoadWideningArea = Number($('#areaUnder30_00MRoadWideningArea').val());
	 var areaUnderServiceRoadArea = Number($('#areaUnderServiceRoadArea').val());
	 var areaUnderReservationArea = Number($('#areaUnderReservationArea').val());
	 var areaUnderGreenBeltArea = Number($('#areaUnderGreenBeltArea').val());
	 var areaUnderNalaArea = Number($('#areaUnderNalaArea').val());
	 
	 //3th
	 var internalRoadsArea = Number($('#internalRoadsArea').val());
	 var transformerArea = Number($('#transformerArea').val());
	 
	 var total2Area=areaUnderBRTCorridorArea+areaUnder1_50MRoadWideningArea+areaUnder3_00MRoadWideningArea+areaUnder4_50MRoadWideningArea+areaUnder6_00MRoadWideningArea + areaUnder7_50MRoadWideningArea + areaUnder9_00MRoadWideningArea + areaUnder12_00MRoadWideningArea + areaUnder15_00MRoadWideningArea + areaUnder18_00MRoadWideningArea + areaUnder24_00MRoadWideningArea + areaUnder30_00MRoadWideningArea + areaUnderServiceRoadArea + areaUnderReservationArea + areaUnderGreenBeltArea + areaUnderNalaArea 
	 
	 
	 var netGrossAreaOfPlot1_2Area=aeraOfPlotAsPer7_12ExtractArea-total2Area;
	 var recreationGround10Area=(netGrossAreaOfPlot1_2Area/100)*10;
	 var amenitySpace5Area=(netGrossAreaOfPlot1_2Area/100)*5;
	 
	 var total4Area=recreationGround10Area+amenitySpace5Area+internalRoadsArea+transformerArea;
	
	 //5th
	 var netAreaOfPlot3_4Area= netGrossAreaOfPlot1_2Area-total4Area;
		 
	 document.architecturalSectionForm.total2Area.value=total2Area.toFixed(2);;
	 document.architecturalSectionForm.netGrossAreaOfPlot1_2Area.value=netGrossAreaOfPlot1_2Area.toFixed(2);;
	 document.architecturalSectionForm.recreationGround10Area.value=recreationGround10Area.toFixed(2);;
	 document.architecturalSectionForm.amenitySpace5Area.value=amenitySpace5Area.toFixed(2);;
	 document.architecturalSectionForm.total4Area.value=total4Area.toFixed(2);;
	 document.architecturalSectionForm.netAreaOfPlot3_4Area.value=netAreaOfPlot3_4Area.toFixed(2);;
	 
	 //6th
	 var tdr40Area = Number($('#tdr40Area').val());
	 var additionsForFAR4a_RecreationGroundArea = Number($('#additionsForFAR4a_RecreationGroundArea').val());
	 var additionForFAR4b_AmenitySpaceArea = Number($('#additionForFAR4b_AmenitySpaceArea').val());
	 var additionForFAR4c_InternalsRoadArea = Number($('#additionForFAR4c_InternalsRoadArea').val());
	 var additionForFAR4d_TransformerArea = Number($('#additionForFAR4d_TransformerArea').val());
	 
	 var total6Area=tdr40Area+additionsForFAR4a_RecreationGroundArea+additionForFAR4b_AmenitySpaceArea+additionForFAR4c_InternalsRoadArea+additionForFAR4d_TransformerArea;
	 document.architecturalSectionForm.total6Area.value=total6Area.toFixed(2);;
	 
	 //7th
	 var totalArea5_6Area=netAreaOfPlot3_4Area+total6Area;
	 document.architecturalSectionForm.totalArea5_6Area.value=totalArea5_6Area.toFixed(2);;
	 
	 //8th
	 var farPermissibleArea = Number($('#farPermissibleArea').val());
	 
	 //9th
	 var totalPermissibleFloorArea7_8Area=farPermissibleArea*totalArea5_6Area;
	 document.architecturalSectionForm.totalPermissibleFloorArea7_8Area.value=totalPermissibleFloorArea7_8Area.toFixed(2);;
		
	 
	 
}

function CalculatetotalResidentialArea11_12Area()
{
	//11th 
	 var existingResidentialArea = Number($('#existingResidentialArea').val());
	 //12th
	 var proposedResidentialArea = Number($('#proposedResidentialArea').val());
	 
	 //13th
	 var totalResidentialArea11_12Area=existingResidentialArea+proposedResidentialArea;
	 document.architecturalSectionForm.totalResidentialArea11_12Area.value=totalResidentialArea11_12Area.toFixed(2);;
	
	 //15th
	 var existingCommercial_IndustrialArea = Number($('#existingCommercial_IndustrialArea').val());
	 //16th
	 var proposedCommercial_IndustrialArea = Number($('#proposedCommercial_IndustrialArea').val());
	
	//17th
	var totalCommercial_IndustrialArea15_16Area=existingCommercial_IndustrialArea+proposedCommercial_IndustrialArea;
	document.architecturalSectionForm.totalCommercial_IndustrialArea15_16Area.value=totalCommercial_IndustrialArea15_16Area.toFixed(2);;
		
	//18th 
	var totalExistingArea11_15= existingResidentialArea+existingCommercial_IndustrialArea;
	document.architecturalSectionForm.totalExistingArea11_15.value=totalExistingArea11_15.toFixed(2);;
	
   //19th
	var totalProposedArea12_16Area=proposedResidentialArea+proposedCommercial_IndustrialArea;
	document.architecturalSectionForm.totalProposedArea12_16Area.value=totalProposedArea12_16Area.toFixed(2);;
	
  //20th	
	var totalExistingArea_Proposed18_19Area=totalExistingArea11_15+totalProposedArea12_16Area;
	document.architecturalSectionForm.totalExistingArea_Proposed18_19Area.value=totalExistingArea_Proposed18_19Area.toFixed(2);;
	
  //21
  var excessBalconyAreaTakenInFSIArea=Number($('#excessBalconyAreaTakenInFSIArea').val());
  
  //22th
  var totalBupArea20_21Area=totalExistingArea_Proposed18_19Area+excessBalconyAreaTakenInFSIArea;
  document.architecturalSectionForm.totalBupArea20_21Area.value=totalBupArea20_21Area.toFixed(2);;
  
  
	
}

function CalculateexcessBalconyArea()
{
	 //For BALCONY AREA STATEMENT
	 //a
	 var permissibleBalconyArea = Number($('#permissibleBalconyArea').val());
	 
	 //b
	 var proposedBalconyArea = Number($('#proposedBalconyArea').val());
	
	 var excessBalconyArea=permissibleBalconyArea+proposedBalconyArea;
	 document.architecturalSectionForm.excessBalconyArea.value=excessBalconyArea.toFixed(2);;
	  
}

function CalculatetenenmentsPermissibleArea()
{
	//TENEMENT STATEMENT
	//a
	 var netGrossAreaOfPlot_A7_Area = Number($('#netGrossAreaOfPlot_A7_Area').val());
	 //b
	 var lessDeductionForNon_ResidentialArea = Number($('#lessDeductionForNon_ResidentialArea').val());
	 //c 
	 var areaOfTenementsa_bArea = netGrossAreaOfPlot_A7_Area-lessDeductionForNon_ResidentialArea;
	 document.architecturalSectionForm.areaOfTenementsa_bArea.value=areaOfTenementsa_bArea.toFixed(2);;
	 
	 //d
	 var tenenmentsPermissibleArea = Number($('#tenenmentsPermissibleArea').val());
	 
	 var totalTenementsProposedArea=areaOfTenementsa_bArea+tenenmentsPermissibleArea;
	 document.architecturalSectionForm.totalTenementsProposedArea.value=totalTenementsProposedArea.toFixed(2);;
		
}


function CalculateexcessGroundConveraged_bArea()
{
	//GROUND COVERAGE STATEMENT
	//a
	 var netPlotAreaArea = Number($('#netPlotAreaArea').val());
	 //b
	 var permissibleGroundCoverageArea = Number($('#permissibleGroundCoverageArea').val());
	 //c 
    var existingGroundCoverageArea = Number($('#existingGroundCoverageArea').val());
	 
	 //d
	 var proposedGroundCoverageArea = Number($('#proposedGroundCoverageArea').val());
	 //e
	 var permissibleGroundCoverageInPremium15Area=(permissibleGroundCoverageArea/100)*15;
	 document.architecturalSectionForm.permissibleGroundCoverageInPremium15Area.value=permissibleGroundCoverageInPremium15Area.toFixed(2);;
	//f
	var excessGroundConveraged_bArea=proposedGroundCoverageArea - permissibleGroundCoverageArea;
	document.architecturalSectionForm.excessGroundConveraged_bArea.value=excessGroundConveraged_bArea.toFixed(2);;
		
	 
}

function validate()
{
	
}
function SetAllZero()
{
	document.getElementById("aeraOfPlotAsPer7_12ExtractArea").value =0;
	document.getElementById("areaOfPlotAsPerPropetyArea").value =0;
	document.getElementById("areaOfPlotAsPerDemarcationArea").value =0;
	document.getElementById("areaAsPerSanctionedLayoutArea").value =0;
	document.getElementById("areaAsPerULCOrderArea").value =0;
	document.getElementById("areaAsPerDevelopmentAgreementArea").value =0;
	document.getElementById("areaOfPlotMinimumConsiderationArea").value =0;
	document.getElementById("areaUnderBRTCorridorArea").value =0;
	document.getElementById("areaUnder1_50MRoadWideningArea").value =0;
	document.getElementById("areaUnder3_00MRoadWideningArea").value =0;
	document.getElementById("areaUnder4_50MRoadWideningArea").value =0;
	document.getElementById("areaUnder6_00MRoadWideningArea").value =0;
	document.getElementById("areaUnder7_50MRoadWideningArea").value =0;
	document.getElementById("areaUnder9_00MRoadWideningArea").value =0;
	document.getElementById("areaUnder12_00MRoadWideningArea").value =0;
	document.getElementById("areaUnder15_00MRoadWideningArea").value =0;
	document.getElementById("areaUnder18_00MRoadWideningArea").value =0;
	document.getElementById("areaUnder24_00MRoadWideningArea").value =0;
	document.getElementById("areaUnder30_00MRoadWideningArea").value =0;
	document.getElementById("areaUnderServiceRoadArea").value =0;
	document.getElementById("areaUnderReservationArea").value =0;
	document.getElementById("areaUnderGreenBeltArea").value =0;
	document.getElementById("areaUnderNalaArea").value =0;
	document.getElementById("total2Area").value =0;
	document.getElementById("netGrossAreaOfPlot1_2Area").value =0;
	document.getElementById("recreationGround10Area").value =0;
	document.getElementById("amenitySpace5Area").value =0;
	document.getElementById("internalRoadsArea").value =0;
	document.getElementById("transformerArea").value =0;
	document.getElementById("total4Area").value =0;
	document.getElementById("netAreaOfPlot3_4Area").value =0;
	document.getElementById("tdr40Area").value =0;
	document.getElementById("additionsForFAR4a_RecreationGroundArea").value =0;
	document.getElementById("additionForFAR4b_AmenitySpaceArea").value =0;
	document.getElementById("additionForFAR4c_InternalsRoadArea").value =0;
	document.getElementById("additionForFAR4d_TransformerArea").value =0;
	document.getElementById("total6Area").value =0;
	document.getElementById("totalArea5_6Area").value =0;
	document.getElementById("farPermissibleArea").value =0;
	document.getElementById("totalPermissibleFloorArea7_8Area").value =0;
	document.getElementById("permissibleResidentialArea").value =0;
	document.getElementById("existingResidentialArea").value =0;
	document.getElementById("proposedResidentialArea").value =0;
	document.getElementById("totalResidentialArea11_12Area").value =0;
	document.getElementById("permissibleCommercial_IndustrialArea").value =0;
	document.getElementById("existingCommercial_IndustrialArea").value =0;
	document.getElementById("proposedCommercial_IndustrialArea").value =0;
	document.getElementById("totalCommercial_IndustrialArea15_16Area").value =0;
	document.getElementById("totalExistingArea11_15").value =0;
	document.getElementById("totalProposedArea12_16Area").value =0;
	document.getElementById("totalExistingArea_Proposed18_19Area").value =0;
	document.getElementById("excessBalconyAreaTakenInFSIArea").value =0;
	document.getElementById("totalBupArea20_21Area").value =0;
	document.getElementById("totalFSIConSumedArea").value =0;
	document.getElementById("permissibleBalconyArea").value =0;
	document.getElementById("proposedBalconyArea").value =0;
	document.getElementById("excessBalconyArea").value =0;
	document.getElementById("netGrossAreaOfPlot_A7_Area").value =0;
	document.getElementById("lessDeductionForNon_ResidentialArea").value =0;
	document.getElementById("areaOfTenementsa_bArea").value =0;
	document.getElementById("tenenmentsPermissibleArea").value =0;
	document.getElementById("totalTenementsProposedArea").value =0;
	document.getElementById("netPlotAreaArea").value =0;
	document.getElementById("permissibleGroundCoverageArea").value =0;
	document.getElementById("existingGroundCoverageArea").value =0;
	document.getElementById("proposedGroundCoverageArea").value =0;
	document.getElementById("permissibleGroundCoverageInPremium15Area").value =0;
	document.getElementById("excessGroundConveraged_bArea").value =0;


}
function init()
{
	
	    <s:forEach items="${projectarchitecturalsectionFormDetails}" var="projectarchitecturalsectionFormDetails" varStatus="loopStatus">
			
			tbl = document.getElementsByTagName("body")[0];
		    tr = tbl.getElementsByTagName("tr");
		 
			//header
		  	var th = document.createElement('th');
			var header=${loopStatus.index+1}+" Area in SQ.M.";
		    th.append(header);
		    tr[0].appendChild(th);
		   
		  //A1
		  	var td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.aeraOfPlotAsPer7_12ExtractArea});
		    tr[1].appendChild(td);
		    
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.areaOfPlotAsPerPropetyArea});
		    tr[2].appendChild(td);
	                 
		    
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.areaOfPlotAsPerDemarcationArea});
		    tr[3].appendChild(td);
	                 
		    
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.areaAsPerSanctionedLayoutArea});
		    tr[4].appendChild(td);
	                 
		    
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.areaAsPerULCOrderArea});
		    tr[5].appendChild(td);
	                 
		    
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.areaAsPerDevelopmentAgreementArea});
		    tr[6].appendChild(td);
	                 
		    
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.areaOfPlotMinimumConsiderationArea});
		    tr[7].appendChild(td);
	                 
		  //2
		           
		    
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.areaUnderBRTCorridorArea});
		    tr[9].appendChild(td);
         
		    
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.areaUnder1_50MRoadWideningArea});
		    tr[10].appendChild(td);         
		    
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.areaUnder3_00MRoadWideningArea});
		    tr[11].appendChild(td);         
		    
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.areaUnder4_50MRoadWideningArea});
		    tr[12].appendChild(td);         
		    
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.areaUnder6_00MRoadWideningArea});
		    tr[13].appendChild(td);         
		    
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.areaUnder7_50MRoadWideningArea});
		    tr[14].appendChild(td);         
		    
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.areaUnder9_00MRoadWideningArea});
		    tr[15].appendChild(td);         
		    
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.areaUnder12_00MRoadWideningArea});
		    tr[16].appendChild(td);         
		    
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.areaUnder15_00MRoadWideningArea});
		    tr[17].appendChild(td);         
		    
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.areaUnder18_00MRoadWideningArea});
		    tr[18].appendChild(td);         
		    
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.areaUnder24_00MRoadWideningArea});
		    tr[19].appendChild(td);         
		    
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.areaUnder30_00MRoadWideningArea});
		    tr[20].appendChild(td);         
		    
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.areaUnderServiceRoadArea});
		    tr[21].appendChild(td);         
		    
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.areaUnderReservationArea});
		    tr[22].appendChild(td);         
		    
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.areaUnderGreenBeltArea});
		    tr[23].appendChild(td);         
		    
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.areaUnderNalaArea});
		    tr[24].appendChild(td);         
		    
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.total2Area});
		    tr[25].appendChild(td);
		    
		   //3
		       
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.netGrossAreaOfPlot1_2Area});
		    tr[26].appendChild(td);
		    
		    //4
		    
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.recreationGround10Area});
		    tr[28].appendChild(td);

		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.amenitySpace5Area});
		    tr[29].appendChild(td);
		    
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.internalRoadsArea});
		    tr[30].appendChild(td);
		    
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.transformerArea});
		    tr[31].appendChild(td);

		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.total4Area});
		    tr[32].appendChild(td);
		    
		    //5
		    
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.netAreaOfPlot3_4Area});
		    tr[33].appendChild(td);
		    
		   //6
		      
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.tdr40Area});
		    tr[35].appendChild(td);
		    
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.additionsForFAR4a_RecreationGroundArea});
		    tr[36].appendChild(td);
		    
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.additionForFAR4b_AmenitySpaceArea});
		    tr[37].appendChild(td);
		    
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.additionForFAR4c_InternalsRoadArea});
		    tr[38].appendChild(td);
		    
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.additionForFAR4d_TransformerArea});
		    tr[39].appendChild(td);
		    
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.total6Area});
		    tr[40].appendChild(td);
		    
		    //7
		        
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.totalArea5_6Area});
		    tr[41].appendChild(td);
		    
		    //8
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.farPermissibleArea});
		    tr[42].appendChild(td);
		    
		    //9
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.totalPermissibleFloorArea7_8Area});
		    tr[43].appendChild(td);
		    
		    //10
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.permissibleResidentialArea});
		    tr[44].appendChild(td);
		    
		    //11
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.existingResidentialArea});
		    tr[45].appendChild(td);
		    
		    //12
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.proposedResidentialArea});
		    tr[46].appendChild(td);
		    
		    //13
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.totalResidentialArea11_12Area});
		    tr[47].appendChild(td);
		    
		    //14
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.permissibleCommercial_IndustrialArea});
		    tr[48].appendChild(td);
		    
		    //15
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.existingCommercial_IndustrialArea});
		    tr[49].appendChild(td);
		    
		    //16
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.proposedCommercial_IndustrialArea});
		    tr[50].appendChild(td);
		    
		    //17
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.totalCommercial_IndustrialArea15_16Area});
		    tr[51].appendChild(td);
		    
		    //18
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.totalExistingArea11_15});
		    tr[52].appendChild(td);
		    
		    //19
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.totalProposedArea12_16Area});
		    tr[53].appendChild(td);
		    
		    //20
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.totalExistingArea_Proposed18_19Area});
		    tr[54].appendChild(td);
		    
		    //21
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.excessBalconyAreaTakenInFSIArea});
		    tr[55].appendChild(td);
		    
		    //22
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.totalBupArea20_21Area});
		    tr[56].appendChild(td);
		    
		    //23
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.totalFSIConSumedArea});
		    tr[57].appendChild(td);
		    
		    //B
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.permissibleBalconyArea});
		    tr[59].appendChild(td);
		    
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.proposedBalconyArea});
		    tr[60].appendChild(td);
		    
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.excessBalconyArea});
		    tr[61].appendChild(td);

		    //c
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.netGrossAreaOfPlot_A7_Area});
		    tr[63].appendChild(td);

		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.lessDeductionForNon_ResidentialArea});
		    tr[64].appendChild(td);
		    
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.areaOfTenementsa_bArea});
		    tr[65].appendChild(td);

		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.tenenmentsPermissibleArea});
		    tr[66].appendChild(td);

		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.totalTenementsProposedArea});
		    tr[67].appendChild(td);
		    
		    //D
		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.netPlotAreaArea});
		    tr[69].appendChild(td);

		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.permissibleGroundCoverageArea});
		    tr[70].appendChild(td);

		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.existingGroundCoverageArea});
		    tr[71].appendChild(td);

		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.proposedGroundCoverageArea});
		    tr[72].appendChild(td);

		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.permissibleGroundCoverageInPremium15Area});
		    tr[73].appendChild(td);

		    td = document.createElement('td');
		    td.append(${projectarchitecturalsectionFormDetails.excessGroundConveraged_bArea});
		    tr[74].appendChild(td);
		    
		    
		</s:forEach>
        
   /*      
		
		tbl = document.getElementsByTagName("body")[0];
	    tr = tbl.getElementsByTagName("tr");
	 
		//header
	  	var th = document.createElement('th');
		var header=" Area in SQ.M.";
	    th.append(header);
	    tr[0].appendChild(th);
	   
	    
	     */
	    
	    
	    
	    /* 
	    $('#architecturalListTable tr').append($("<td>"));
        $('#architecturalListTable thead tr>td:last').html($('#col').val());
        $('#architecturalListTable tbody tr').each(function(){$(this).children('td:last').append($('<input type="checkbox">'))});
        
	 */
	    
	
/* 	

	tbl = document.getElementsByTagName("body")[0];
  tr = tbl.getElementsByTagName("tr");
 
  var td = document.createElement('td');
  var input = document.createElement('INPUT');
  input.type = 'text';
  var a="fjkh";
  td.append(a);
  tr[1].appendChild(td); 
	 */

 
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	//document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	SetAllZero();
}


</script>
</body>
</html>


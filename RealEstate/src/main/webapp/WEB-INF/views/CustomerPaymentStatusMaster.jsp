<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Customer Payment Status Master</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">
  <!-- Font Awesome -->
  <!-- Ionicons -->
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <style type="text/css">
   tr.odd {background-color:#F0F8FF}
tr.even {background-color: #CCE5FF}
    </style>
    
   
    
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
 
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

   <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
     Customer Payment Status Master Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Sale</a></li>
        <li class="active">Booking Master</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="customerpaymentstatusmaster" action="${pageContext.request.contextPath}/CustomerPaymentStatusMaster" method="post">
   
      <section class="content">
      <div class="box box-default">
      
      <div class="box-body">
      <div class="row">
  
        <div class="col-xs-12">
       
        
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#AllPaymentList" data-toggle="tab">Booking Remaining List</a></li>
              <li><a href="#ClearPaymentList" data-toggle="tab">Clear Payment List</a></li>
              <li><a href="#CancelPaymentList" data-toggle="tab">Cancel Payment List</a></li>
            </ul>
            
            
            <div class="tab-content">
              <div class="tab-pane active" id="AllPaymentList">
                <section >
              <h4 class="page-header">Payment List</h4>
              <div  class="panel box box-danger">
              </br>
              <div class="table-responsive">
                <table class="table table-bordered" id="PaymentListTable">
                  <thead>
	                  <tr bgcolor=#4682B4>
	                    <th>Receipt Id</th>
	                    <th>Booking Id</th>
	                    <th>Name</th>
	                    <th>Payment Against</th>
	                    <th>Payment Amount</th>
	                    <th>Payment Mode</th>
	                    <th>Bank Name</th>
	                    <th>Status</th>
	                    <th>Action</th>
	                  </tr>
                  </thead>
                  
                  <tbody>
                  
	                   <s:forEach items="${customerReceiptList}" var="customerReceiptList" varStatus="loopStatus">
	                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
	                            <td>${customerReceiptList.receiptId}</td>
	                            <td>${customerReceiptList.bookingId}</td>
		                        <td>${customerReceiptList.customerName}</td>
		                        <td>${customerReceiptList.paymentType}</td>
		                        <td>${customerReceiptList.paymentAmount}</td>
		                        <td>${customerReceiptList.paymentMode}</td>
		                        <td>${customerReceiptList.bankName}</td>
		                        <td>${customerReceiptList.status}</td>
		                        <td><a href="${pageContext.request.contextPath}/UpdatePaymentStatus?receiptId=${customerReceiptList.receiptId}" class="btn btn-info btn-sm" data-toggle="tooltip" title="Update Payment Status"><i class="glyphicon glyphicon-edit"></i></a></td>
	                        </tr>
						</s:forEach>
                
                 </tbody>
                </table>
              </div>
            </div> 
           </section>
		</div>
		    
                      
       <div class="tab-pane" id="ClearPaymentList">
        <section >
          <h4 class="page-header">Clear Payment List</h4>
          <div  class="panel box box-danger">
          </br>
          
              <div class="table-responsive">
                <table class="table table-bordered" id="ClearPaymentListTable">
                  <thead>
	                  <tr bgcolor=#4682B4>
	                    <th>Receipt Id</th>
	                    <th>Booking Id</th>
	                    <th>Name</th>
	                    <th>Payment Against</th>
	                    <th>Payment Amount</th>
	                    <th>Payment Mode</th>
	                    <th>Bank Name</th>
	                    <th>Status</th>
	                    <th>Action</th> 
	                  </tr>
                  </thead>
                  
                  <tbody>
                  
	                   <s:forEach items="${customerclearpaymentList}" var="customerclearpaymentList" varStatus="loopStatus">
	                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
	                            <td>${customerclearpaymentList.receiptId}</td>
	                            <td>${customerclearpaymentList.bookingId}</td>
		                        <td>${customerclearpaymentList.customerName}</td>
		                        <td>${customerclearpaymentList.paymentType}</td>
		                        <td>${customerclearpaymentList.paymentAmount}</td>
		                        <td>${customerclearpaymentList.paymentMode}</td>
		                        <td>${customerclearpaymentList.bankName}</td>
		                        <td>${customerclearpaymentList.status}</td>
		                        <td><a href="${pageContext.request.contextPath}/OtherPaymentReceipt?receiptId=${customerclearpaymentList.receiptId}" target="_blank" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Print"><span class="glyphicon glyphicon-print"></span></a></td>	                        
                  
	                        </tr>
						</s:forEach>
                
                 </tbody>
                </table>
              </div>
          
                 </div> 
                </section>
			 </div>
 
 
 
 <div class="tab-pane" id="CancelPaymentList">
       <section>
              <h4 class="page-header">All Cancel List</h4>
              <div  class="panel box box-danger">
              </br>
              
              <div class="table-responsive">
                <table class="table table-bordered" id="CancelPaymentListTable">
                  <thead>
	                  <tr bgcolor=#4682B4>
	                    <th>Receipt Id</th>
	                    <th>Booking Id</th>
	                    <th>Name</th>
	                    <th>Payment Against</th>
	                    <th>Payment Amount</th>
	                    <th>Payment Mode</th>
	                    <th>Bank Name</th>
	                    <th>Status</th>
	                    <th>Action</th>
	                  </tr>
                  </thead>
                  
                  <tbody>
                  
	                   <s:forEach items="${customercancelpaymentList}" var="customercancelpaymentList" varStatus="loopStatus">
	                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
	                            <td>${customercancelpaymentList.receiptId}</td>
	                            <td>${customercancelpaymentList.bookingId}</td>
		                        <td>${customercancelpaymentList.customerName}</td>
		                        <td>${customercancelpaymentList.paymentType}</td>
		                        <td>${customercancelpaymentList.paymentAmount}</td>
		                        <td>${customercancelpaymentList.paymentMode}</td>
		                        <td>${customercancelpaymentList.bankName}</td>
		                        <td>${customercancelpaymentList.status}</td>
		                        <td><a href="${pageContext.request.contextPath}/OtherCancelledPaymentReceipt?receiptId=${customercancelpaymentList.receiptId}" target="_blank" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Print"><span class="glyphicon glyphicon-print"></span></a></td>
	                      </tr>
					   </s:forEach>
                
                 </tbody>
                </table>
              </div>
              </div>
            </section>
		</div>
 
	</div>
   </div>
 </div>
  </div>
  </div>
  </div>
  </section>
</form>
 </div>
  <%@ include file="footer.jsp" %>
 <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
       
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Bootstrap 3.3.7 -->
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script>


$(function () {
    $('#PaymentListTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
  

$(function () {
    $('#ClearPaymentListTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
  
  
$(function () {
    $('#CancelPaymentListTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
  
</script>
</body>
</html>

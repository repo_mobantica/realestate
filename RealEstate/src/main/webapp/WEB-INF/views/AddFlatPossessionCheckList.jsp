<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Flat Possession Check List</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


  <!-- Google Font -->
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#enquirydbStatusSpan').delay(1000).fadeOut();
      });
    </script>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

  <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Flat Possession Check List:
        <small>Preview</small>
      </h1>
      
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Sale</a></li>
        <li class="active">Add Flat Possession Check List</li>
      </ol>
    </section>

<form name="contractorworkorderform" action="${pageContext.request.contextPath}/AddFlatPossessionCheckList" onSubmit="return validate()" method="post">
    <section class="content">

      <div class="box box-default">
        <div class="panel box box-danger">
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <span id="statusSpan" style="color:#FF0000"></span>
			
			
			<div class="box-body">
              <div class="row">
              
                  <div class="col-xs-2">
                  <label >Possession Id</label>
                  <input type="text" class="form-control" id="possessionId" name="possessionId" value="${possessionId}" readonly>
                  </div>               
            
              </div>
            </div>
			<input type="hidden" class="form-control" id="bookingId" name="bookingId" value="${bookingdetails[0].bookingId}" readonly>
			<div class="box-body">
              <div class="row">
                <div class="col-xs-12">
                <table>
	                <tr>
	                	<td style="width:10px"></td>
	                     <td> <h4><label>Project  </label></h4> </td>
	                     <td style="width:10px">:</td>
	                     <td> <h4> <label>${projectName}</label> </h4></td>
	                     
	                     <td style="width:30px"></td>
	                     <td> <h4><label>Building </label></h4> </td>
	                     <td style="width:10px">:</td>
	                     <td> <h4><label>${buildingNAme}  </label></h4> </td>
	                     
	                     <td style="width:30px"></td>
	                     <td> <h4> <label>Wing </label> </h4></td>
	                     <td style="width:10px">:</td>
	                     <td> <h4><label>${wingName}  </label></h4> </td>
	                     
	                     <td style="width:30px"></td>
	                     <td> <h4> <label>Flat No </label></h4> </td>
	                     <td style="width:10px">:</td>
	                     <td> <h4> <label>${flatNumber}  </label></h4> </td>
	                    
	                     <td style="width:30px"></td>
	                     <td> <h4><label>Customer Name </label></h4></td>
	                     <td style="width:10px">:</td>
	                     <td> <h4><label>${bookingdetails[0].bookingfirstname} ${bookingdetails[0].bookingmiddlename} ${bookingdetails[0].bookinglastname} </label> </h4></td>
	                      
	                </tr>
                </table>
                </div>
              
              </div>
            </div>
			
			<div class="box-body">
              <div class="row">
               
         		 <div class="col-xs-12">
         		   <div class="table-responsive">
         		 	<table class="table table-bordered">
         		 	
         		 		<tr bgcolor=#4682B4>
         		 		     <th>No</th>
         		 		     <th>DESCRIPTION OF WORKS</th>
         		 		     <th>Work Status</th>
         		 		     
         		 		</tr>
         		 		
         		 		<tr>
         		 		     <th>A</th>
         		 		     <th>GYPSUM WORK(Wall & Ceiling)</th>
         		 		     <th></th>
         		 		     
         		 		</tr>
         		 		
         		 		<tr>
         		 		     <td>1</td>
         		 		     <td>All wall cracks filled up neatly</td>
         		 		     <td>
	         		 		    <select class="form-control" name="allWallCracksFilledUpNeatly" id="allWallCracksFilledUpNeatly">
				         		 		     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].allWallCracksFilledUpNeatly eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].allWallCracksFilledUpNeatly eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].allWallCracksFilledUpNeatly eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 			
         		 		<tr>
         		 		     <td>2</td>
         		 		     <td>All water patches removed neatly</td>
         		 		     <td>
	         		 		    <select class="form-control" name="allWaterPatchesremovedNeatly" id="allWaterPatchesremovedNeatly">
	                    			     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].allWaterPatchesremovedNeatly eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].allWaterPatchesremovedNeatly eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].allWaterPatchesremovedNeatly eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 			
         		 		<tr>
         		 		     <td>3</td>
         		 		     <td>All fan hooks in position</td>
         		 		     <td>
	         		 		    <select class="form-control" name="allFanHooksInPosition" id="allFanHooksInPosition">
	                    			     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].allFanHooksInPosition eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].allFanHooksInPosition eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].allFanHooksInPosition eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 			
         		 		<tr>
         		 		     <td>4</td>
         		 		     <td>Good overall finishing of walls</td>
         		 		     <td>
	         		 		    <select class="form-control" name="goodOverallFinishingOfWall" id="goodOverallFinishingOfWall">
	                    			     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].goodOverallFinishingOfWall eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].goodOverallFinishingOfWall eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].goodOverallFinishingOfWall eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 			
         		 		<tr>
         		 		     <th>B</th>
         		 		     <th>TILING WORK(Flooring/Dado Tiles)</th>
         		 		     <td> </td>
         		 		     
         		 		</tr>
         		 		
         		 		<tr>
         		 		     <td>1</td>
         		 		     <td>Flooring in uniform level</td>
         		 		     <td>
	         		 		    <select class="form-control" name="flooringInUniformLevel" id="flooringInUniformLevel">
	                    			     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].flooringInUniformLevel eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].flooringInUniformLevel eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].flooringInUniformLevel eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 		
         		 		<tr>
         		 		     <td>2</td>
         		 		     <td>Dado tiles are properly finished and joint filled</td>
         		 		     <td>
	         		 		    <select class="form-control" name="dadoTilesAreaProperlyFinishedandJointFilled" id="dadoTilesAreaProperlyFinishedandJointFilled">
	                    			     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].dadoTilesAreaProperlyFinishedandJointFilled eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].dadoTilesAreaProperlyFinishedandJointFilled eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].dadoTilesAreaProperlyFinishedandJointFilled eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 		
         		 		<tr>
         		 		     <td>3</td>
         		 		     <td>Dado tiles are proper line and in right angle</td>
         		 		     <td>
	         		 		    <select class="form-control" name="dadoTilesAreProperLineAndInRightAngle" id="dadoTilesAreProperLineAndInRightAngle">
	                    		     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].dadoTilesAreProperLineAndInRightAngle eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].dadoTilesAreProperLineAndInRightAngle eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].dadoTilesAreProperLineAndInRightAngle eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 		
         		 		<tr>
         		 		     <td>4</td>
         		 		     <td>No cracked tiles on the wall</td>
         		 		     <td>
	         		 		    <select class="form-control" name="noCrackedTilesOnTheWall" id="noCrackedTilesOnTheWall">
	                    			     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].noCrackedTilesOnTheWall eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].noCrackedTilesOnTheWall eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].noCrackedTilesOnTheWall eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 		
         		 		<tr>
         		 		     <td>5</td>
         		 		     <td>Proper slope has been given to bath flooring</td>
         		 		     <td>
	         		 		    <select class="form-control" name="properSlopehasbeenToBathFlooring" id="properSlopehasbeenToBathFlooring">
	                    		     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].properSlopehasbeenToBathFlooring eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].properSlopehasbeenToBathFlooring eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].properSlopehasbeenToBathFlooring eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 		
         		 		<tr>
         		 		     <td>6</td>
         		 		     <td>Proper slope given to terrace outlet/ spout</td>
         		 		     <td>
	         		 		    <select class="form-control" name="properSlopeGivenToTerraceOutlet" id="properSlopeGivenToTerraceOutlet">
	                    			     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].properSlopeGivenToTerraceOutlet eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].properSlopeGivenToTerraceOutlet eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].properSlopeGivenToTerraceOutlet eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 		<tr>
         		 		     <td>7</td>
         		 		     <td>Kitchen Platform properly fitted all joints filled</td>
         		 		     <td>
	         		 		    <select class="form-control" name="kitchenPlatformProperlyFittedallJointsFilled" id="kitchenPlatformProperlyFittedallJointsFilled">
	                    		     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].kitchenPlatformProperlyFittedallJointsFilled eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].kitchenPlatformProperlyFittedallJointsFilled eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].kitchenPlatformProperlyFittedallJointsFilled eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 		
         		 		<tr>
         		 		     <td>8</td>
         		 		     <td>No leakage in the sink of the kitchen</td>
         		 		     <td>
	         		 		    <select class="form-control" name="noLeakagesIntheSinkOftheKitchen" id="noLeakagesIntheSinkOftheKitchen">
	                    			     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].noLeakagesIntheSinkOftheKitchen eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].noLeakagesIntheSinkOftheKitchen eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].noLeakagesIntheSinkOftheKitchen eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 		
         		 		<tr>
         		 		     <td>9</td>
         		 		     <td>Wall below Kitchen otta properly finished</td>
         		 		     <td>
	         		 		    <select class="form-control" name="wallBelowKitchenOttaProperlyFinished" id="wallBelowKitchenOttaProperlyFinished">
	                    	     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].wallBelowKitchenOttaProperlyFinished eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].wallBelowKitchenOttaProperlyFinished eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].wallBelowKitchenOttaProperlyFinished eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 		
         		 		<tr>
         		 		     <th>C</th>
         		 		     <th>CARPETARY WORK(Door & Windows)</th>
         		 		     <th></th>
         		 		</tr>
         		 		
         		 		<tr>
         		 		     <td>1</td>
         		 		     <td>All doors and window open and shut properly</td>
         		 		     <td>
	         		 		    <select class="form-control" name="allDoorsAndWindowOpenandShutProperly" id="allDoorsAndWindowOpenandShutProperly">
	                    			     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].allDoorsAndWindowOpenandShutProperly eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].allDoorsAndWindowOpenandShutProperly eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].allDoorsAndWindowOpenandShutProperly eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 		
         		 		<tr>
         		 		     <td>2</td>
         		 		     <td>All locks, tower bolts, stoppers, hinges operate well</td>
         		 		     <td>
	         		 		    <select class="form-control" name="allLocksTowerBoltsStoppersHingesOperateWell" id="allLocksTowerBoltsStoppersHingesOperateWell">
	                    		     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].allLocksTowerBoltsStoppersHingesOperateWell eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].allLocksTowerBoltsStoppersHingesOperateWell eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].allLocksTowerBoltsStoppersHingesOperateWell eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 		
         		 		<tr>
         		 		     <td>3</td>
         		 		     <td>All glass to window properly fixed & operating well and cleaned </td>
         		 		     <td>
	         		 		    <select class="form-control" name="allGlassToWindowProperlyFixedAndOperationWellAndCleaned" id="allGlassToWindowProperlyFixedAndOperationWellAndCleaned">
	                    			     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].allGlassToWindowProperlyFixedAndOperationWellAndCleaned eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].allGlassToWindowProperlyFixedAndOperationWellAndCleaned eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].allGlassToWindowProperlyFixedAndOperationWellAndCleaned eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 		
         		 		<tr>
         		 		     <td>4</td>
         		 		     <td>Name plate fixed on Main door</td>
         		 		     <td>
	         		 		    <select class="form-control" name="namePlateFixedOnMainDoor" id="namePlateFixedOnMainDoor">
	                    	     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].namePlateFixedOnMainDoor eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].namePlateFixedOnMainDoor eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].namePlateFixedOnMainDoor eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 		
         		 		<tr>
         		 		     <td>5</td>
         		 		     <td>M.S.Folding door in terrace is operating properly</td>
         		 		     <td>
	         		 		    <select class="form-control" name="msFoldingDoorInTerraceIsOperatingProperly" id="msFoldingDoorInTerraceIsOperatingProperly">
	                    		     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].msFoldingDoorInTerraceIsOperatingProperly eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].msFoldingDoorInTerraceIsOperatingProperly eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].msFoldingDoorInTerraceIsOperatingProperly eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 		
         		 		<tr>
         		 		     <td>6</td>
         		 		     <td>Eye piece fitting to the main door</td>
         		 		     <td>
	         		 		    <select class="form-control" name="eyePieceFittingTotheMainDoor" id="eyePieceFittingTotheMainDoor">
	                    	     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].eyePieceFittingTotheMainDoor eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].eyePieceFittingTotheMainDoor eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].eyePieceFittingTotheMainDoor eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 		
         		 		<tr>
         		 		     <td>7</td>
         		 		     <td>All door fitting and mortise locks are properly fixed</td>
         		 		     <td>
	         		 		    <select class="form-control" name="allDoorFittingAndMortiseLocksareProperlyFixed" id="allDoorFittingAndMortiseLocksareProperlyFixed">
	                    	     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].allDoorFittingAndMortiseLocksareProperlyFixed eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].allDoorFittingAndMortiseLocksareProperlyFixed eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].allDoorFittingAndMortiseLocksareProperlyFixed eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 		
         		 		<tr>
         		 		     <td>8</td>
         		 		     <td>All door magnetic catcher are properly fixed </td>
         		 		     <td>
	         		 		    <select class="form-control" name="allDoorMagneticCatcherareProperlyFixed" id="allDoorMagneticCatcherareProperlyFixed">
	                    		     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].allDoorMagneticCatcherareProperlyFixed eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].allDoorMagneticCatcherareProperlyFixed eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].allDoorMagneticCatcherareProperlyFixed eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 		
         		 		<tr>
         		 		     <td>9</td>
         		 		     <td>All fitting are free paint spots</td>
         		 		     <td>
	         		 		    <select class="form-control" name="allFittingareFreePaintSpots" id="allFittingareFreePaintSpots">
	                    		     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].allFittingareFreePaintSpots eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].allFittingareFreePaintSpots eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].allFittingareFreePaintSpots eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 			
         		 		<tr>
         		 		     <th>D</th>
         		 		     <th>PLUMBING WORK(INTERANAL CP & SANTARY WORK)</th>
         		 		     <td></td>
         		 		</tr>
         		 			
         		 		<tr>
         		 		     <td>1</td>
         		 		     <td>CP Fittings fixed properly</td>
         		 		     <td>
	         		 		    <select class="form-control" name="cpFittingFixedProperly" id="cpFittingFixedProperly">
	                    		     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].cpFittingFixedProperly eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].cpFittingFixedProperly eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].cpFittingFixedProperly eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 				
         		 		<tr>
         		 		     <td>2</td>
         		 		     <td>No leakages in CP fittings</td>
         		 		     <td>
	         		 		    <select class="form-control" name="noLeakagesInCPFittings" id="noLeakagesInCPFittings">
	                    		     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].noLeakagesInCPFittings eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].noLeakagesInCPFittings eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].noLeakagesInCPFittings eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 				
         		 		<tr>
         		 		     <td>3</td>
         		 		     <td>Hot and cold mixer operates properly</td>
         		 		     <td>
	         		 		    <select class="form-control" name="hotAndColdMixerOparatesProperly" id="hotAndColdMixerOparatesProperly">
	                    		     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].hotAndColdMixerOparatesProperly eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].hotAndColdMixerOparatesProperly eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].hotAndColdMixerOparatesProperly eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 				
         		 		<tr>
         		 		     <td>4</td>
         		 		     <td>Flush tank can easily operate</td>
         		 		     <td>
	         		 		    <select class="form-control" name="flushTankcanEasilyOprate" id="flushTankcanEasilyOprate">
	                    		     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].flushTankcanEasilyOprate eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].flushTankcanEasilyOprate eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].flushTankcanEasilyOprate eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 				
         		 		<tr>
         		 		     <td>5</td>
         		 		     <td>EWC cover properly fitted</td>
         		 		     <td>
	         		 		    <select class="form-control" name="ewcCoverProperlyFitted" id="ewcCoverProperlyFitted">
	                    		     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].ewcCoverProperlyFitted eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].ewcCoverProperlyFitted eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].ewcCoverProperlyFitted eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 				
         		 		<tr>
         		 		     <td>6</td>
         		 		     <td>Gap betwwen wall tiles and wash basin filled</td>
         		 		     <td>
	         		 		    <select class="form-control" name="gapBetwwenWallTilesandWashBasinFilled" id="gapBetwwenWallTilesandWashBasinFilled">
	                    	     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].gapBetwwenWallTilesandWashBasinFilled eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].gapBetwwenWallTilesandWashBasinFilled eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].gapBetwwenWallTilesandWashBasinFilled eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 				
         		 		<tr>
         		 		     <td>7</td>
         		 		     <td>All sanitary fitting are neatly cleaned</td>
         		 		     <td>
	         		 		    <select class="form-control" name="allSanitaryFittingareNeatlyCleaned" id="allSanitaryFittingareNeatlyCleaned">
	                    			     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].allSanitaryFittingareNeatlyCleaned eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].allSanitaryFittingareNeatlyCleaned eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].allSanitaryFittingareNeatlyCleaned eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 				
         		 		<tr>
         		 		     <td>8</td>
         		 		     <td>Wash basin is properly fitted and does not move</td>
         		 		     <td>
	         		 		    <select class="form-control" name="washbasinIsProperlyFittedAndDoesNotMove" id="washbasinIsProperlyFittedAndDoesNotMove">
	                    		     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].washbasinIsProperlyFittedAndDoesNotMove eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].washbasinIsProperlyFittedAndDoesNotMove eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].washbasinIsProperlyFittedAndDoesNotMove eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 				
         		 		<tr>
         		 		     <td>9</td>
         		 		     <td>No sanitary fittings are cracked / broken</td>
         		 		     <td>
	         		 		    <select class="form-control" name="noSanitaryFittingsareCracked" id="noSanitaryFittingsareCracked">
	                    			     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].noSanitaryFittingsareCracked eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].noSanitaryFittingsareCracked eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].noSanitaryFittingsareCracked eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 				
         		 		<tr>
         		 		     <td>10</td>
         		 		     <td>Both taps in kitchen operative</td>
         		 		     <td>
	         		 		    <select class="form-control" name="bothTapsInKitchenOperative" id="bothTapsInKitchenOperative">
	                    		     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].bothTapsInKitchenOperative eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].bothTapsInKitchenOperative eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].bothTapsInKitchenOperative eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 			
         		 		<tr>
         		 		     <th>E</th>
         		 		     <th>ELECTIFICATION</th>
         		 		     <th></th>
         		 		</tr>
         		 						
         		 		<tr>
         		 		     <td>1</td>
         		 		     <td>Electric meter connected to the flat</td>
         		 		     <td>
	         		 		    <select class="form-control" name="electricMeterConnectedTotheFlat" id="electricMeterConnectedTotheFlat">
	                    			     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].electricMeterConnectedTotheFlat eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].electricMeterConnectedTotheFlat eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].electricMeterConnectedTotheFlat eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 					
         		 		<tr>
         		 		     <td>2</td>
         		 		     <td>All switches operated properly</td>
         		 		     <td>
	         		 		    <select class="form-control" name="allSwitchesOperatedProperly" id="allSwitchesOperatedProperly">
	                    			     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].allSwitchesOperatedProperly eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].allSwitchesOperatedProperly eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].allSwitchesOperatedProperly eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 					
         		 		<tr>
         		 		     <td>3</td>
         		 		     <td>MCB & DCB properly fixed</td>
         		 		     <td>
	         		 		    <select class="form-control" name="mcbAnddcbProperlyFixed" id="mcbAnddcbProperlyFixed">
	                    		     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].mcbAnddcbProperlyFixed eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].mcbAnddcbProperlyFixed eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].mcbAnddcbProperlyFixed eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 					
         		 		<tr>
         		 		     <td>4</td>
         		 		     <td>Color of all switches is uniform</td>
         		 		     <td>
	         		 		    <select class="form-control" name="colorOfAllSwitchesIsUniform" id="colorOfAllSwitchesIsUniform">
	                    			     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].colorOfAllSwitchesIsUniform eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].colorOfAllSwitchesIsUniform eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].colorOfAllSwitchesIsUniform eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 					
         		 		<tr>
         		 		     <td>5</td>
         		 		     <td>All switchboards are properly screwed</td>
         		 		     <td>
	         		 		    <select class="form-control" name="allswitchboardsareProperlyScrewed" id="allswitchboardsareProperlyScrewed">
	                    			     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].allswitchboardsareProperlyScrewed eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].allswitchboardsareProperlyScrewed eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].allswitchboardsareProperlyScrewed eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 			
         		 		<tr>
         		 		     <th>F</th>
         		 		     <th>PAINTING </th>
         		 		     <th></th>
         		 		</tr>
         		 					
         		 		<tr>
         		 		     <td>1</td>
         		 		     <td>Painting of all walls and ceilings is properly done</td>
         		 		     <td>
	         		 		    <select class="form-control" name="paintingOfallWallsAndCeilingsIsProperlyDone" id="paintingOfallWallsAndCeilingsIsProperlyDone">
	                    		     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].paintingOfallWallsAndCeilingsIsProperlyDone eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].paintingOfallWallsAndCeilingsIsProperlyDone eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].paintingOfallWallsAndCeilingsIsProperlyDone eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 						
         		 		<tr>
         		 		     <td>2</td>
         		 		     <td>Oil painting of window grills and terrace railing done properly</td>
         		 		     <td>
	         		 		    <select class="form-control" name="oilPaintingOfWindowGrillsAndTerraceRailingDoneProperly" id="oilPaintingOfWindowGrillsAndTerraceRailingDoneProperly">
	                    		     
								 <c:choose>
							  		<c:when test="${flatpossessionchecklistdetails[0].oilPaintingOfWindowGrillsAndTerraceRailingDoneProperly eq 'Pending'}">
			                    		<option selected="selected">Pending</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option selected="selected">Pending</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].oilPaintingOfWindowGrillsAndTerraceRailingDoneProperly eq 'Progress'}">
			                    		<option selected="selected">Progress</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Progress</option>
			                    	</c:otherwise>
			                     </c:choose>
			                        
			                     <c:choose>	
			                    	<c:when test="${flatpossessionchecklistdetails[0].oilPaintingOfWindowGrillsAndTerraceRailingDoneProperly eq 'Done'}">
			                    		<option selected="selected">Done</option>
			                    	</c:when>
			                    	<c:otherwise>
			                    	    <option>Done</option>
			                    	</c:otherwise>
			                     </c:choose>
			                     
	                  			</select>
         		 		     </td>
         		 		</tr>
         		 			
         		 	</table>
         		 	</div>
         		 </div>
               
              </div>
            </div>
			
       
			<input type="hidden" id="creationDate" name="creationDate" >
			<input type="hidden" id="updateDate" name="updateDate" >
			<input type="hidden" id="paymentStatus" name="paymentStatus" value="Unclear">
			
			<input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>">
		</div>	
      </div>
      
		   	 <div class="box-body">
              <div class="row">
              <br/><br/>
              <div class="col-xs-1">
              </div>
                 <div class="col-xs-4">
	            	 <div class="col-xs-2">	
	             		<a href="FlatPossessionCheckList"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
				     </div> 
			     </div>
				  <div class="col-xs-4">
                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
			     </div>
					<div class="col-xs-2">
			        <button type="submit" class="btn btn-info " name="submit">Submit</button>
			     </div> 
			     
              </div>
			  </div>
        </div>
        
      </div>
	</div>	
   </section>
</form>
</div>
 

  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
   
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>

 <script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
</script>
 
<script>

function CalculateRetentionAmount()
{

	var totalAmount = Number($('#totalAmount').val());
	var retentionPer = Number($('#retentionPer').val());
	var retentionAmount=(totalAmount/100)*retentionPer;
	
	document.contractorworkorderform.retentionAmount.value=retentionAmount.toFixed(0);
}




function getPaymentTotalAmount()
{

var totalAmount  = Number($('#totalAmount').val());

var gstCost = Number($('#gstCost').val());
var tdspercentage = Number($('#tdspercentage').val());

var paymentAmount ;
//= Number($('#paymentAmount').val());

var paymentAmountper = Number($('#paymentAmountper').val());
/* 
var gstAmount = $('#gstAmount').val();
var paymentTotalAmount = $('#paymentTotalAmount').val();
var tdsAmount = $('#tdsAmount').val();
 */
 
var gstAmount ;
var paymentTotalAmount ;
var tdsAmount;
var grandAmount;

paymentAmount=(totalAmount/100)*paymentAmountper;
gstAmount=(paymentAmount/100)*gstCost;
paymentTotalAmount=paymentAmount+gstAmount;

tdsAmount=(paymentTotalAmount/100)*tdspercentage;
grandAmount=paymentTotalAmount-tdsAmount;

document.contractorworkorderform.paymentAmount.value=paymentAmount.toFixed(0);
document.contractorworkorderform.gstAmount.value=gstAmount.toFixed(0);
document.contractorworkorderform.paymentTotalAmount.value=paymentTotalAmount.toFixed(0);
document.contractorworkorderform.tdsAmount.value=tdsAmount.toFixed(0);
document.contractorworkorderform.grandAmount.value=grandAmount.toFixed(0);

}


function AddContractorPayment()
{
	 $('#paymentDecriptionSpan').html('');
	 $('#paymentAmountSpan').html('');
	 
	 var workOrderId = $('#workOrderId').val();
	 var contractorId = $('#contractorId').val();
	 
	 var paymentDecription = $('#paymentDecription').val();
	 var paymentAmountper = $('#paymentAmountper').val();
	 var paymentAmount = $('#paymentAmount').val();
	 var gstAmount = $('#gstAmount').val();
	 var paymentTotalAmount = $('#paymentTotalAmount').val();
	 var tdsAmount = $('#tdsAmount').val();
	 var grandAmount = $('#grandAmount').val();
	 
 

		if(document.contractorworkorderform.paymentDecription.value.length==0)
		{
			$('#paymentDecriptionSpan').html('Please, enter work..!');
			//document.contractorworkorderform.employeeBankacno.value="";
			document.contractorworkorderform.paymentDecription.focus();
			return false;
		}
		
		if(document.contractorworkorderform.paymentAmountper.value.length==0)
		{
			$('#paymentAmountperSpan').html('Please, enter Amount per..!');
			//document.contractorworkorderform.employeeBankacno.value="";
			document.contractorworkorderform.paymentAmountper.focus();
			return false;
		}
		else if(!document.contractorworkorderform.paymentAmountper.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
		{
			 if(!document.contractorworkorderform.paymentAmountper.value.match(/^[0-9]+$/))
				 {
					//$('#paymentAmountSpan').html('Please, use only digit value for payment Amount');
					document.contractorworkorderform.paymentAmountper.value="";
					document.contractorworkorderform.paymentAmountper.focus();
					return false;
				}
		}	
	 
		
		
	 $("#customerPaymentListTable tr").detach();
	 	
	 	$.ajax({

	 		url : '${pageContext.request.contextPath}/AddContractorPayment',
	 		type : 'Post',
	 		data : { workOrderId : workOrderId, contractorId : contractorId, paymentDecription : paymentDecription, paymentAmountper : paymentAmountper, paymentAmount : paymentAmount, gstAmount : gstAmount, paymentTotalAmount : paymentTotalAmount, tdsAmount : tdsAmount, grandAmount : grandAmount},
	 		dataType : 'json',
	 		success : function(result)
	 				  {
	 						if (result) 
	 						{ 
	 							$('#customerPaymentListTable').append('<tr style="background-color: #4682B4;"> <th>Work Details</th> <th>Amount per%</th> <th>Amount</th><th>GST Amount</th> <th>Total Amount</th> <th>TDS Amount</th><th>Grand Amount</th> <th>Action</th>');
	 						
	 							for(var i=0;i<result.length;i++)
	 							{ 
	 								if(i%2==0)
	 								{
	 									var id = result[i].contractorrschedulerId;
	 									$('#customerPaymentListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].paymentDecription+'</td><td>'+result[i].paymentAmountper+'</td><td>'+result[i].paymentAmount+'</td><td>'+result[i].gstAmount+'</td><td>'+result[i].paymentTotalAmount+'</td><td>'+result[i].tdsAmount+'</td><td>'+result[i].grandAmount+'</td><td><a onclick="DeleteCustomerPayment('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td>');
	 								}
	 								else
	 								{
	 									var id = result[i].contractorrschedulerId;
	 									$('#customerPaymentListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].paymentDecription+'</td><td>'+result[i].paymentAmountper+'</td><td>'+result[i].paymentAmount+'</td><td>'+result[i].gstAmount+'</td><td>'+result[i].paymentTotalAmount+'</td><td>'+result[i].tdsAmount+'</td><td>'+result[i].grandAmount+'</td><td><a onclick="DeleteCustomerPayment('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td>');
	 								}
	 							 } 
	 						} 
	 						else
	 						{
	 							alert("failure111");
	 						}

	 					}
	 		}); 
	 	document.contractorworkorderform.paymentDecription.value="";
	 	document.contractorworkorderform.paymentAmountper.value="";
	 	document.contractorworkorderform.paymentAmount.value="";	
		document.contractorworkorderform.gstAmount.value="";
	 	document.contractorworkorderform.paymentTotalAmount.value="";	
		document.contractorworkorderform.tdsAmount.value="";
	 	document.contractorworkorderform.grandAmount.value="";	
	 	
	 	
}
function DeleteCustomerPayment(contractorrschedulerId)
{
	var workOrderId = $('#workOrderId').val();
    
    $('#customerPaymentListTable tr').detach();
    
    $.ajax({

		 url : '${pageContext.request.contextPath}/DeleteCustomerPayment',
		type : 'Post',
		data : { contractorrschedulerId : contractorrschedulerId, workOrderId : workOrderId },
		dataType : 'json',
		success : function(result)
				  {
				if (result) 
					{ 
						$('#customerPaymentListTable').append('<tr style="background-color: #4682B4;"> <th>Work Details</th> <th>Amount Per %</th><th>Amount</th><th>GST Amount</th> <th>Total Amount</th> <th>TDS Amount</th><th>Grand Amount</th> <th>Action</th>');
					
						for(var i=0;i<result.length;i++)
						{ 
							if(i%2==0)
							{
								var id = result[i].contractorrschedulerId;
								$('#customerPaymentListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].paymentDecription+'</td><td>'+result[i].paymentAmountper+'</td><td>'+result[i].paymentAmount+'</td><td>'+result[i].gstAmount+'</td><td>'+result[i].paymentTotalAmount+'</td><td>'+result[i].tdsAmount+'</td><td>'+result[i].grandAmount+'</td><td><a onclick="DeleteCustomerPayment('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td>');
							}
							else
							{
								var id = result[i].contractorrschedulerId;
								$('#customerPaymentListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].paymentDecription+'</td><td>'+result[i].paymentAmountper+'</td><td>'+result[i].paymentAmount+'</td><td>'+result[i].gstAmount+'</td><td>'+result[i].paymentTotalAmount+'</td><td>'+result[i].tdsAmount+'</td><td>'+result[i].grandAmount+'</td><td><a onclick="DeleteCustomerPayment('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td>');
							}
						 } 
						} 
						else
						{
							alert("failure111");
						}
				  } 

		});	
}

function clearall()
{
	$('#noofMaleLabourSpan').html('');
	$('#noofFemaleLabourSpan').html('');
	$('#noofInsuredLabourSpan').html('');
	$('#retentionAmountSpan').html('');
	$('#workStartDateSpan').html('');
	$('#workEndDateSpan').html('');
}
function validate()
{ 
	clearall();
		
	
	if(document.contractorworkorderform.noofMaleLabour.value.length==0)
	{
		$('#noofMaleLabourSpan').html('Please, enter no of male labours..!');
		//document.contractorworkorderform.employeeBankacno.value="";
		document.contractorworkorderform.noofMaleLabour.focus();
		return false;
	}
	else if(!document.contractorworkorderform.noofMaleLabour.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
	{
		 if(!document.contractorworkorderform.noofMaleLabour.value.match(/^[0-9]+$/))
			 {
				$('#noofMaleLabourSpan').html('Please, use only digit value for male labours..! eg: 30');
				document.contractorworkorderform.noofMaleLabour.value="";
				document.contractorworkorderform.noofMaleLabour.focus();
				return false;
			}
	}
	 
	if(document.contractorworkorderform.noofFemaleLabour.value.length==0)
	{
		$('#noofFemaleLabourSpan').html('Please, enter no fo female labours..!');
		//document.contractorworkorderform.employeeBankacno.value="";
		document.contractorworkorderform.noofFemaleLabour.focus();
		return false;
	}
	else if(!document.contractorworkorderform.noofFemaleLabour.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
	{
		 if(!document.contractorworkorderform.noofFemaleLabour.value.match(/^[0-9]+$/))
			 {
				$('#noofFemaleLabourSpan').html('Please, use only digit value for  Female labours..! eg: 30');
				document.contractorworkorderform.noofFemaleLabour.value="";
				document.contractorworkorderform.noofFemaleLabour.focus();
				return false;
			}
	}
	

	if(document.contractorworkorderform.noofInsuredLabour.value.length==0)
	{
		$('#noofInsuredLabourSpan').html('Please, enter no fo female labours..!');
		//document.contractorworkorderform.employeeBankacno.value="";
		document.contractorworkorderform.noofInsuredLabour.focus();
		return false;
	}
	else if(!document.contractorworkorderform.noofInsuredLabour.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
	{
		 if(!document.contractorworkorderform.noofInsuredLabour.value.match(/^[0-9]+$/))
			 {
				$('#noofInsuredLabourSpan').html('Please, use only digit value for  Female labours..! eg: 30');
				document.contractorworkorderform.noofInsuredLabour.value="";
				document.contractorworkorderform.noofInsuredLabour.focus();
				return false;
			}
	}
	
	if(document.contractorworkorderform.retentionPer.value.length==0)
	{
		$('#retentionAmountSpan').html('Please, enter retention per..!');
		document.contractorworkorderform.retentionPer.focus();
		return false;
	}
	else if(!document.contractorworkorderform.retentionPer.value.match(/^[0-9]+(\.[0-9]{1,2})+$/))
	{
		 if(!document.contractorworkorderform.retentionPer.value.match(/^[0-9]+$/))
			 {
				$('#retentionPerSpan').html('Please, use only digit value for retention per..! eg:21.36 OR 30');
				document.contractorworkorderform.retentionPer.value="";
				document.contractorworkorderform.retentionPer.focus();
				return false;
			}
	}
	
	if(document.contractorworkorderform.workStartDate.value=="")
	{
	    $('#workStartDateSpan').html('Please select start date..!');
		document.contractorworkorderform.workStartDate.focus();
		return false;
	}

	if(document.contractorworkorderform.workEndDate.value=="")
	{
	    $('#workEndDateSpan').html('Please select end date..!');
		document.contractorworkorderform.workEndDate.focus();
		return false;
	}
	
}
function init()
{
	clearall();
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getContractorGstCostList',
		type : 'Post',
		data : { },
		dataType : 'json',
		success : function(result)
				  {
			 var gstCost=0;
						if (result) 
						{
							for(var i=0;i<result.length;i++)
							{
								gstCost=result[result.length-1].taxPercentage;
							} 
						} 
						else
						{
							alert("failure111");
						}
						document.contractorworkorderform.gstCost.value=gstCost;
					}
		});
	
	 if(document.contractorworkorderform.status.value=="Fail")
	 {
	  	//alert("Sorry, record is present already..!");
	 }
	 else if(document.contractorworkorderform.status.value=="Success")
	 {
			$('#statusSpan').html('Record added successfully..!');
	 }
}



function getBuldingList()
{
	 $("#buildingName").empty();
	 var projectName = $('#projectName').val();
	 
	 $.ajax({

		url : '${pageContext.request.contextPath}/getBuildingList',
		type : 'Post',
		data : { projectName : projectName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Building Name-");
							$("#buildingName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].buildingName).text(result[i].buildingName);
							    $("#buildingName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
}//end of get Building List


function getWingNameList()
{
	 $("#wingName").empty();
	 var buildingName = $('#buildingName').val();
	 var projectName = $('#projectName').val();
	 $.ajax({

		url : '${pageContext.request.contextPath}/getprojectwingList',
		type : 'Post',
		data : { buildingName : buildingName, projectName : projectName },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Wing Name-");
							$("#wingName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].wingName).text(result[i].wingName);
							    $("#wingName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}


function getFloorNameList()
{
	 $("#floortypeName").empty();
	 var wingName = $('#wingName').val();
	 var buildingName = $('#buildingName').val();
	 var projectName = $('#projectName').val();
	 
	 $.ajax({

		url : '${pageContext.request.contextPath}/getwingfloorNameList',
		type : 'Post',
		data : {wingName : wingName, buildingName : buildingName, projectName : projectName },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"All").text("All");
							$("#floortypeName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].floortypeName).text(result[i].floortypeName);
							    $("#floortypeName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	

}//end of get Floor Type List


  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
     $('#workStartDate').datepicker({
      autoclose: true
    })

    $('#workEndDate').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Customer Receipt Form</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
    
 	<script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
    </script>

  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>

<div class="wrapper">

   <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Payment Scheduler Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Sale</a></li>
        <li class="active">Customer Receipt Form</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="customerreceiptform" action="${pageContext.request.contextPath}/CustomerReceiptForm" onSubmit="return validate()" method="post">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
      <div class="box box-danger"></div>
        
         <div class="box-body" id="paymentStatus" hidden="hidden">
           <div class="callout callout-success">
                <h4><i class="icon fa fa-check"></i>Amount payed by  ${bookingDetails[0].bookingfirstname} ${bookingDetails[0].bookingmiddlename} ${bookingDetails[0].bookinglastname}</h4>

         </div>
	  </div>
	 
	 
  
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
                  <span id="statusSpan" style="color:#FF0000"></span>
                   
                  <div class="box-body">
                     <div class="row">
                     
                     <div class="col-xs-4">
                    	<div class="input-group">
                 		<h4><span class="fa fa-star"></span> 
                   		 <label >Booking Id : ${bookingDetails[0].bookingId}</label>
                   		 </h4> 
                   		   <input type="hidden" class="form-control" id="bookingId" name="bookingId" value="${bookingDetails[0].bookingId}">
       		 		  </div>
			        </div>   
			           
                     <div class="col-xs-6">
                    	<div class="input-group">
                 		<h4><span class="fa fa-user"></span> 
                   		 <label>Customer Name : ${bookingDetails[0].bookingfirstname} ${bookingDetails[0].bookingmiddlename} ${bookingDetails[0].bookinglastname}</label>
                     	</h4> 
       		 		  </div>
			        </div>   
			            
                 
			                  
              		 </div>
            	  </div>
            	 
            	  
             <div class="box-body">
                <div class="row">
                  <div class="col-xs-3">
                   <div class="table-responsive">
                   <table id="bankListTable" class="table table-bordered ">
                   <thead>
	                  <tr bgcolor=#AAB7B8>
	                    <th>Particulars</th>
	                    <th>Amount(Rs.)</th>
	                    
	                    <!-- <th>Work Stage</th>
	                    <th>Total Due as per work stage</th>
	                    <th>Less Total Receipt Till Date</th>
	                    <th>Total due to us as on date(Excluding GST)</th>
	                    <th>GST due on above amount</th>
	                    <th>Total Due including GST</th> -->
	                    
	                  </tr>
                   </thead>
                   <tbody>
                      <tr>
             		    <td>Agreement Value</td>
             		    <td>${demandDetails.agreementValue}</td>
             		  </tr>
             		  <tr>
             		    <td>Work stage</td>
             		    <td>${demandDetails.completedWorkSteg}</td>
             		  </tr>
             		  <tr>
             		    <td>Total Due as per work stage</td>
             		    <td>${demandDetails.dueAsPerWork}</td>
             		  </tr>
             		  <tr>
             		    <td>Less Total Receipt Till Date</td>
             		    <td>${demandDetails.totalTillDate}</td>
             		  </tr>
             		  <tr>
             		    <td>Total due to us as on date(Excluding GST)</td>
             		    <td>${demandDetails.totalExcludingGST}</td>
             		  </tr>
             		  <tr>
             		    <td>GST due on above amount</td>
             		    <td>${demandDetails.GSTdue}</td>
             		  </tr>
             		  <tr>
             		    <td>Total Due including GST</td>
             		    <td>${demandDetails.totalIncludingGST}</td>
             		  </tr>
             		  
                  </tbody>
                </table>
              </div>
           </div>  
                      
           
               
            <div class="col-xs-9">
                   <div class="col-xs-4">
                    	<div class="input-group">
                 		<h4><span class="fa fa-money"></span> 
                   		  <label>Loan Amount : ${loanAmount} </label>  
                     	</h4> 
       		 		  </div>
			        </div>   
                <div class="col-xs-8">
                    	<div class="input-group">
                 		<h4><span class="fa fa-star"></span> 
                   		  <label>Loan Amount Against By: ${loanAmountAgainst} </label>  
                     	</h4> 
       		 		  </div>
                </div>
                
                  <div class="col-xs-12">
               
                 <div class="table-responsive">
                 <table class="table table-bordered ">
				  <thead>
                   <tr bgcolor=#AAB7B8>
                     <th style="width:150px">Type</th>
   					 <th style="width:150px">Agg Amt</th>
				     <th style="width:150px">Stamp Duty(${bookingDetails[0].stampDutyPer}%)</th>
   					 <th style="width:150px">Reg. Amt(${bookingDetails[0].registrationPer}%)</th>
				     <th style="width:150px">GST Amt(${bookingDetails[0].gstCost}%)</th>
				     <th style="width:150px">Total</th>
                   </tr>
                 </thead>
                 <tbody>
                 
				  <tr>
				    <td>Total Amt</td>
				    <td> <label id="aggreementValue1">${bookingDetails[0].aggreementValue1}</label> </td>
				    <td> <label id="stampDuty1">${bookingDetails[0].stampDuty1}</label> </td>
				    <td> <label id="registrationCost1">${bookingDetails[0].registrationCost1}</label> </td>
				    <td> <label id="gstAmount1">${bookingDetails[0].gstAmount1}</label> </td>
				    <td> <label id="grandTotal1"> ${bookingDetails[0].grandTotal1}</label> </td>
				  </tr>
				 
				  <tr>
				    <td>Paid Amt</td>
				    <td> <label id="totalpayAggreement">${customerPayment[0].totalpayAggreement}</label> </td>
				    <td> <label id="totalpayStampDuty">${customerPayment[0].totalpayStampDuty}</label> </td>
				    <td> <label id="totalpayRegistration">${customerPayment[0].totalpayRegistration}</label> </td>
				    <td> <label id="totalpaygstAmount">${customerPayment[0].totalpaygstAmount}</label> </td>
				    <td> <label id="totalPayedAmount1"></label> </td>
				  </tr>
				 
				  <tr>
				    <td>Remaining Amt</td>
				    <td> <label id="reamaingAggreement"> </label> </td>
				    <td> <label id="reamaingstampDuty"> </label> </td>
				    <td> <label id="reamaingRegistration"> </label> </td>
				    <td> <label id="reamaingGstAmount"> </label> </td>
				    <td> <label id="totalRemainingAmount">  </label> </td>
				  </tr>
				 
			
				  <tr>
				    <td>Add More Payment</td>
				    <td> <input type="text" class="form-control" id="aggreementAmount" placeholder="Aggreement Amount " name="aggreementAmount" onchange="getTotal(this.value)"> </td>
				    <td> <input type="text" class="form-control" id="stampDutyAmount" placeholder="Stamp Duty Amount " name="stampDutyAmount" onchange="getTotal(this.value)"> </td>
				    <td> <input type="text" class="form-control" id="registrationAmount" placeholder="Registration Amount " name="registrationAmount" onchange="getTotal(this.value)"> </td>
				    <td> <input type="text" class="form-control" id="gstAmount" placeholder="GST Amount " name="gstAmount" onchange="getTotal(this.value)"> </td>
				    <td> <label id="total">  </label> </td>
				  </tr>
				 
			
				</tbody>
			 </table>
				 
			</div>
			
			
			</div>
          </div> 
          <div class="col-xs-2">
          </div>
          <div class="col-xs-4">
          <span id="buttonSpan" style="color:#FF0000"></span>
          </div>
       </div>
       </div>
       
      <div class="box-body">
         <div class="row">
         	
              <div class="col-md-2">
                <label> Date</label> <label class="text-red">*</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input  type="text" class="form-control pull-right" id="date" name="date">
                </div>
                <span id="dateSpan" style="color:#FF0000"></span>
              </div>
              
		  	<div class="col-md-2">
               <label for="bankName">Bank Name </label><label class="text-red">* </label>
    		   <input type="text" class="form-control" name="bankName" id="bankName" placeholder="Enter bank Name">                  
		       <span id="bankNameSpan" style="color:#FF0000"></span>
		       </div>
		       
		  	<div class="col-md-2">
               <label for="branchName">Branch Name </label><label class="text-red">* </label>
    		   <input type="text" class="form-control" name="branchName" id="branchName" placeholder="Enter branch Name">                  
		       <span id="branchNameSpan" style="color:#FF0000"></span>
		       </div>
        
	            <div class="col-xs-2">
                    <label>Payment Type</label> <label class="text-red">*</label>
                  	<select class="form-control" id="paymentMode" name="paymentMode">
				  	<option selected="selected" value="Default">-Select Type-</option>
				  	<option value="Cash">Cash</option>
				  	<option  value="Cheque">Cheque</option>
				  	<option value="DD">DD</option>
				  	<option value="RTGS">RTGS</option>	
				  	<option value="NEFT">NEFT</option>
				  	<option value="IMPS">IMPS</option>	
                  	</select>
                   <span id="paymentModeSpan" style="color:#FF0000"></span>
               </div>

		       <div class="col-md-2">
               <label for="chequeNumber">REF No. </label><label class="text-red">* </label>
    		   <input type="text" class="form-control" name="chequeNumber" id="chequeNumber" placeholder="Enter REF No.">                  
		       <span id="chequeNumberSpan" style="color:#FF0000"></span>
		       </div>
		       
		      <div class="col-md-2">
               <label for="narration">Narration</label><label class="text-red">* </label>
    		   <input type="text" class="form-control" name="narration" id="narration" placeholder="Enter Narration">                  
		       <span id="narrationSpan" style="color:#FF0000"></span>
		       </div>
		
         </div>
     </div>
    </div>
  </div>
</div>
         
         
<div class="box-body">
  <div class="row">
    <div class="col-md-12">

          
          <div class="box-body">
           <div class="row">
         
              </br>
                 <div class="col-xs-5">
                 <div class="col-xs-4">
                   <a href="AddCustomerPayment"><button type="button" class="btn btn-primary" value="Back" style="width:90px">Back</button></a>
                 </div>
                 </div>
				 <div class="col-xs-3">
                   <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
			     </div>
				 
				 <div class="col-xs-3" id="submitButton">
				<!--  <button type="button" class="btn btn-info pull-right" onclick="return AddAllData()">Submit</button>
			     </div> 
			 <div class="col-xs-3" hidden="hidden"> -->
		  			<button type="submit" class="btn btn-info pull-right" name="submit" id="submit">Submit</button>
			    </div> 
            </div>
        </div>  	 
    </div>
            
</div>
 </div>
            	 	 

              <input type="hidden" id="creationDate" name="creationDate" value="">
			  <input type="hidden" id="updateDate" name="updateDate" value="">
			  <input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>">                             
	  </div>	
      </section>
	</form>
 </div>
    <%@ include file="footer.jsp" %>
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>

<script>

function checkAmount()
{
  $('#buttonSpan').html('');
  
  var reamaingAggreement = Number($("#reamaingAggreement").text()) ;
  var reamaingstampDuty     = Number($("#reamaingstampDuty").text()) ;
  var reamaingRegistration = Number($("#reamaingRegistration").text()) ;
  var reamaingGstAmount = Number($("#reamaingGstAmount").text()) ;
  
  var aggreementAmount  =  Number($("#aggreementAmount").val()) ;
  var stampDutyAmount   =  Number($("#stampDutyAmount").val()) ;
  var registrationAmount=  Number($("#registrationAmount").val()) ;
  var gstAmount         =  Number($("#gstAmount").val()) ;
		  
  if(aggreementAmount!=0)
  {
	 if(aggreementAmount>reamaingAggreement)
	 {
		 $('#buttonSpan').html('Please, enter agreement amount less than or equal to remaining amount..!');
		 document.customerreceiptform.aggreementAmount.focus();
		 return false;
	 }
  }
  
  if(reamaingstampDuty!=0)
  {
	 if(reamaingstampDuty>reamaingstampDuty)
	 {
		 $('#buttonSpan').html('Please, enter stamp duty less than or equal to remaining amount..!');
		 document.customerreceiptform.reamaingstampDuty.focus();
		 return false;
	 }
  }
  
  if(registrationAmount!=0)
  {
	 if(registrationAmount>reamaingRegistration)
	 {
		 $('#buttonSpan').html('Please, enter registration less than or equal to remaining amount..!');
		 document.customerreceiptform.registrationAmount.focus();
		 return false;
	 }
  }
  
  if(gstAmount!=0)
  {
	 if(gstAmount>reamaingGstAmount)
	 {
		 $('#buttonSpan').html('Please, enter GST less than or equal to remaining amount..!');
		 document.customerreceiptform.gstAmount.focus();
		 return false;
	 }
  }
}

function validate()
{
	$('#buttonSpan').html('');
	$('#paymentModeSpan').html('');
	$('#bankNameSpan').html('');
	$('#chequeNumberSpan').html('');
	
	$('#branchNameSpan').html('');
	$('#narration').html('');
	$('#dateSpan').html('');
	//checkAmount();
	
	
	$('#buttonSpan').html('');
  
  var reamaingAggreement = Number($("#reamaingAggreement").text()) ;
  var reamaingstampDuty     = Number($("#reamaingstampDuty").text()) ;
  var reamaingRegistration = Number($("#reamaingRegistration").text()) ;
  var reamaingGstAmount = Number($("#reamaingGstAmount").text()) ;
  
  var aggreementAmount  =  Number($("#aggreementAmount").val()) ;
  var stampDutyAmount   =  Number($("#stampDutyAmount").val()) ;
  var registrationAmount=  Number($("#registrationAmount").val()) ;
  var gstAmount         =  Number($("#gstAmount").val()) ;
		  
  if(aggreementAmount!=0)
  {
	 if(aggreementAmount>reamaingAggreement)
	 {
		 $('#buttonSpan').html('Please, enter agreement amount less than or equal to remaining amount..!');
		 document.customerreceiptform.aggreementAmount.focus();
		 return false;
	 }
  }
  
  if(reamaingstampDuty!=0)
  {
	 if(reamaingstampDuty>reamaingstampDuty)
	 {
		 $('#buttonSpan').html('Please, enter stamp duty less than or equal to remaining amount..!');
		 document.customerreceiptform.reamaingstampDuty.focus();
		 return false;
	 }
  }
  
  if(registrationAmount!=0)
  {
	 if(registrationAmount>reamaingRegistration)
	 {
		 $('#buttonSpan').html('Please, enter registration less than or equal to remaining amount..!');
		 document.customerreceiptform.registrationAmount.focus();
		 return false;
	 }
  }
  
  if(gstAmount!=0)
  {
	 if(gstAmount>reamaingGstAmount)
	 {
		 $('#buttonSpan').html('Please, enter GST less than or equal to remaining amount..!');
		 document.customerreceiptform.gstAmount.focus();
		 return false;
	 }
  }
	
	if(document.customerreceiptform.aggreementAmount.value.length==0 )
	{
	if(document.customerreceiptform.stampDutyAmount.value.length==0)
		{
		if(document.customerreceiptform.registrationAmount.value.length==0 )
			{
				if(document.customerreceiptform.gstAmount.value.length==0 )
				{
					$('#buttonSpan').html('Please, add any one entry any amount..!');
					return false;
				}
			}
		}
	}
	

	if(document.customerreceiptform.aggreementAmount.value!=0)
    {
		if(!document.customerreceiptform.aggreementAmount.value.match(/^[0-9]+$/))
		{
			$('#buttonSpan').html('Please, enter correct aggreement amount..!');
			document.customerreceiptform.aggreementAmount.focus();
			document.customerreceiptform.aggreementAmount.value="";
			return false;
		} 		
    }
	
	if(document.customerreceiptform.stampDutyAmount.value!=0)
    {
		if(!document.customerreceiptform.stampDutyAmount.value.match(/^[0-9]+$/))
		{
			$('#buttonSpan').html('Please, enter correct stampDuty Amount..!');
			document.customerreceiptform.stampDutyAmount.focus ();
			document.customerreceiptform.stampDutyAmount.value="";
			return false;
		} 		
    }
	
	if(document.customerreceiptform.registrationAmount.value!=0)
    {
		if(!document.customerreceiptform.registrationAmount.value.match(/^[0-9]+$/))
		{
			$('#buttonSpan').html('Please, enter correct registration Amount..!');
			document.customerreceiptform.registrationAmount.focus();
			document.customerreceiptform.registrationAmount.value="";
			return false;
		} 		
    }
	if(document.customerreceiptform.gstAmount.value!=0)
    {
		if(!document.customerreceiptform.gstAmount.value.match(/^[0-9]+$/))
		{
			$('#buttonSpan').html('Please, enter correct gst Amount..!');
			document.customerreceiptform.gstAmount.focus();
			document.customerreceiptform.gstAmount.value="";
			return false;
		} 		
    }
	
	if(document.customerreceiptform.aggreementAmount.value.length==0 || document.customerreceiptform.stampDutyAmount.value.length==0 || document.customerreceiptform.registrationAmount.value.length==0 || document.customerreceiptform.gstAmount.value.length==0)
	{
		if(document.customerreceiptform.aggreementAmount.value.length==0)
		{
		  $('#aggreementAmount').val("0.0");
		}
		
		if(document.customerreceiptform.stampDutyAmount.value.length==0)
		{
		  $('#stampDutyAmount').val("0.0");
		}
		
		if(document.customerreceiptform.registrationAmount.value.length==0)
		{
		  $('#registrationAmount').val("0.0");
		}
		
		if(document.customerreceiptform.gstAmount.value.length==0)
		{
		  $('#gstAmount').val("0.0");
		} 
	}
	

	if(document.customerreceiptform.date.value=="")
	{
		$('#dateSpan').html('Please, select  date..!');
		document.customerreceiptform.date.focus();
		return false;
	}
	
	//validation for employee designation
	if(document.customerreceiptform.paymentMode.value=="Default")
	{
		$('#paymentModeSpan').html('Please, select payment type..!');
		document.customerreceiptform.paymentMode.focus();
		return false;
	}
	
  
	if(document.customerreceiptform.paymentMode.value == "Cheque" || document.customerreceiptform.paymentMode.value == "DD" || document.customerreceiptform.paymentMode.value == "RTGS"  || document.customerreceiptform.paymentMode.value == "NEFT" || document.customerreceiptform.paymentMode.value == "IMPS" )
	{
		if(document.customerreceiptform.bankName.value == "")
		{
			$('#bankNameSpan').html('Please, enter bank name..!');
			document.customerreceiptform.bankName.focus();
			return false;
		}
		else if(document.customerreceiptform.bankName.value.match(/^[\s]+$/))
		{
			$('#bankNameSpan').html('Bank name should not be empty..!');
			document.customerreceiptform.bankName.focus();
			document.customerreceiptform.bankName.value="";
			return false;
		}
		
		if(document.customerreceiptform.branchName.value == "")
		{
			$('#branchNameSpan').html('Please, enter branch name..!');
			document.customerreceiptform.branchName.focus();
			return false;
		}
		else if(document.customerreceiptform.branchName.value.match(/^[\s]+$/))
		{
			$('#branchNameSpan').html('Branch name should not be empty..!');
			document.customerreceiptform.branchName.focus();
			document.customerreceiptform.branchName.value="";
			return false;
		}
	
		if(document.customerreceiptform.chequeNumber.value == "")
		{
			$('#chequeNumberSpan').html('Please, enter Ref Number..!');
			document.customerreceiptform.chequeNumber.focus();
			return false;
		}

		if(document.customerreceiptform.narration.value == "")
		{
			$('#narrationSpan').html('Please, enter narration..!');
			document.customerreceiptform.narration.focus();
			return false;
		}
	 }
	
}



function getTotal()
{
	var aggreementAmount=Number($('#aggreementAmount').val());
	var stampDutyAmount = Number($('#stampDutyAmount').val());
	var registrationAmount=Number($('#registrationAmount').val());
	var gstAmount = Number($('#gstAmount').val());
	
	var total=aggreementAmount+stampDutyAmount+registrationAmount+gstAmount;
	document.getElementById('total').innerHTML = total;
	
}

/* 
function submitFunction()
{
submit.click();
} */

function init()
{
	
		var date =  new Date();
		var year = date.getFullYear();
		var month = date.getMonth() + 1;
		var day = date.getDate();
		
		document.getElementById("creationDate").value = day + "/" + month + "/" + year;
		document.getElementById("updateDate").value = day + "/" + month + "/" + year;

	var totalpayAggreement= Number($('#totalpayAggreement').text());
	var totalpayStampDuty= Number($('#totalpayStampDuty').text());
	var totalpayRegistration= Number($('#totalpayRegistration').text());
	var totalpaygstAmount= Number($('#totalpaygstAmount').text());
	
	var totalPayedAmount1=totalpayAggreement+totalpayStampDuty+totalpayRegistration+totalpaygstAmount;
	
	document.getElementById('totalPayedAmount1').innerHTML = totalPayedAmount1;
	
	
	  
var aggreementValue1=Number($('#aggreementValue1').text());
var stampDuty1=Number($('#stampDuty1').text());

var registrationCost1=Number($('#registrationCost1').text());
var gstAmount1=Number($('#gstAmount1').text()); 

var reamaingAggreement=aggreementValue1-totalpayAggreement;
var reamaingstampDuty=stampDuty1-totalpayStampDuty;
var reamaingRegistration= registrationCost1-totalpayRegistration;
var reamaingGstAmount=gstAmount1-totalpaygstAmount;
 
 
document.getElementById('reamaingAggreement').innerHTML = reamaingAggreement;
document.getElementById('reamaingstampDuty').innerHTML =reamaingstampDuty ;
document.getElementById('reamaingRegistration').innerHTML =reamaingRegistration;
document.getElementById('reamaingGstAmount').innerHTML = reamaingGstAmount;

document.getElementById('totalRemainingAmount').innerHTML = reamaingAggreement+reamaingstampDuty+reamaingRegistration+reamaingGstAmount;

 
if(reamaingAggreement<=0 && reamaingstampDuty<=0 && reamaingRegistration<=0 && reamaingGstAmount<=0)
{
document.getElementById('submitButton').style.display ='none';
document.getElementById('paymentStatus').style.display ='block';
}
 
  
}


/* 
  $(document).ready(function() {
	  $(window).keydown(function(event){
	    if(event.keyCode == 13) {
	      event.preventDefault();
	      return false;
	    }
	  });
	});
   */

   $(function () {
       //Initialize Select2 Elements
       $('.select2').select2()

       //Datemask dd/mm/yyyy
       $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
       //Datemask2 mm/dd/yyyy
       $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
       //Money Euro
       $('[data-mask]').inputmask()

       //Date range picker
       $('#reservation').daterangepicker()
       //Date range picker with time picker
       $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
       //Date range as a button
       $('#daterange-btn').daterangepicker(
         {
           ranges   : {
             'Today'       : [moment(), moment()],
             'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
             'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
             'Last 30 Days': [moment().subtract(29, 'days'), moment()],
             'This Month'  : [moment().startOf('month'), moment().endOf('month')],
             'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
           },
           startDate: moment().subtract(29, 'days'),
           endDate  : moment()
         },
         function (start, end) {
           $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
         }
       )

       //Date picker
       $('#date').datepicker({
         autoclose: true
       })

       //iCheck for checkbox and radio inputs
       $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
         checkboxClass: 'icheckbox_minimal-blue',
         radioClass   : 'iradio_minimal-blue'
       })
       //Red color scheme for iCheck
       $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
         checkboxClass: 'icheckbox_minimal-red',
         radioClass   : 'iradio_minimal-red'
       })
       //Flat red color scheme for iCheck
       $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
         checkboxClass: 'icheckbox_flat-green',
         radioClass   : 'iradio_flat-green'
       })

       //Colorpicker
       $('.my-colorpicker1').colorpicker()
       //color picker with addon
       $('.my-colorpicker2').colorpicker()

       //Timepicker
       $('.timepicker').timepicker({
         showInputs: false
       })
     })
</script>
</body>
</html>
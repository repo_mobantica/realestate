<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Add Agreement</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


  <!-- Google Font -->
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#enquirydbStatusSpan').delay(1000).fadeOut();
      });
    </script>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
    		
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">
<%@ include file="headerpage.jsp" %>
 
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Agreement Details
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Add Agreement</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="aggreementform" action="${pageContext.request.contextPath}/AddAggreementForm" onSubmit="return validate()" method="Post" >
    <section class="content">
    
      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
       <div class="panel box box-danger"> </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              
           
			<span id="statusSpan" style="color:#FF0000"></span>
				<div class="box-body">
              <div class="row">
                  <div class="col-xs-2">
                  <label for="aggreementId">Agreement Id</label>
                  <input type="text" class="form-control" id="aggreementId" placeholder="ID" name="aggreementId" value="${aggreementCode}"  readonly>
                </div>               
                    <div class="col-xs-2">
                  <label for="bookingId">Booking Id</label>
                  <input type="text" class="form-control" id="bookingId" placeholder="ID" name="bookingId" value="${bookingDetails[0].bookingId}" readonly>
                </div>
                
                  <div class="col-xs-3">
                  <label for="aggreementNumber">Agreement Number</label>
                  <input type="text" class="form-control" id="aggreementNumber" placeholder="Aggreement Number" name="aggreementNumber">
                </div>
                
                  <div class="col-xs-3">
                  <label for="aggreementDate">Agreement Date</label>
                  
				  <div class="input-group">
                  <div class="input-group-addon">
                   <i class="fa fa-calendar"></i>
                  </div>
                   <input type="text" class="form-control pull-right" id="aggreementDate" name="aggreementDate" >
                  </div>
                </div>
                
              </div>
            </div>
				
				<div class="box-body">
              <div class="row">
              <div class="col-xs-5">
              </div>
        	     
			     <div class="col-xs-3">
                   <div class="input-group">
                 	<h3><span class="fa fa-home"></span> 
                    <label>Unit Details :  </label> 
                   </h3> 
       		 	  </div>
			     </div> 
              </div>
              </div>
			<div class="box-body">
              <div class="row">
                   <div class="col-xs-3">
                   	<div class="input-group">
                 	<h4><span class="fa fa-university"></span> 
                   	Project Name : ${projectName}
                   	</h4> 
       		 		</div>
			      </div>  
			     
			     <div class="col-xs-3">
                   <div class="input-group">
                 	<h4><span class="fa fa-certificate"></span> 
                    Building Name : ${buildingName} 
                   </h4> 
       		 	  </div>
			     </div>  
			     
			     <div class="col-xs-3">
                   <div class="input-group">
                 	<h4><span class="fa fa-cube"></span> 
                    Wing Name :  ${wingName} 
                    </h4> 
       		 	   </div>
			     </div>  
			     
			     <div class="col-xs-3">
                   <div class="input-group">
                 	<h4><span class="fa fa-align-center"></span> 
                   Floor Name : ${floortypeName}
                    </h4> 
       		 	   </div>
			     </div>
			     
			     <div class="col-xs-3">
                   <div class="input-group">
                 	<h4><span class="fa fa-home"></span> 
                    Flat Number : ${flatNumber} 
                    </h4> 
       		 	   </div>
			     </div>  
			     
			     <div class="col-xs-3">
                   <div class="input-group">
                 	<h4><span class="fa fa-trophy"></span> 
                    Flat Type : ${bookingDetails[0].flatType} 
                    </h4> 
       		 	   </div>
			     </div>
			     
			      
			     <div class="col-xs-3">
                   <div class="input-group">
                 	<h4><span class="fa fa-trophy"></span> 
                   Flat Facing Type : ${bookingDetails[0].flatFacing} 
                    </h4> 
       		 	   </div>
			     </div>  
			     
			     <div class="col-xs-3">
                   <div class="input-group">
                 	<h4><span class="fa fa-area-chart"></span> 
                    Flat Total Area : ${bookingDetails[0].flatareainSqFt} 
                    </h4> 
       		 	   </div>
			     </div>
			     
			     <div class="col-xs-3">
                   <div class="input-group">
                 	<h4><span class="fa fa-money"></span> 
                    Agreement Amount: ${bookingDetails[0].aggreementValue1}  
                    </h4> 
       		 	   </div>
			     </div>  
			     
			     <div class="col-xs-3">
                   <div class="input-group">
                 	<h4><span class="fa fa-money"></span> 
                   Total Amount : ${bookingDetails[0].grandTotal1}  
                    </h4> 
       		 	   </div>
			     </div>
			      
			     <div class="col-xs-3">
                   <div class="input-group">
                 	<h4><span class="fa fa-money"></span> 
                    TDS Amount : ${bookingDetails[0].tds}  
                    </h4> 
       		 	   </div>
			     </div>
			      
              </div>
            </div>
				
				
				
			<div class="box-body">
              <div class="row">
                <div  class="panel box box-success"></div>    
			     <div class="col-xs-6">
                   <div class="input-group">
                 	<h3>
                   Sole/First Applicant Details : 
                   </h3> 
       		 	  </div>
			     </div> 
              </div>
              </div>	
			 
			 
          <div class="box-body">
              <div class="row">
                <div class="col-xs-2">
				<label for="firstApplicantfirstname">First Name</label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="firstApplicantfirstname" placeholder="First Name" name="firstApplicantfirstname" style="text-transform: capitalize;">
                  <span id="firstApplicantfirstnameSpan" style="color:#FF0000"></span>
                </div>
                <div class="col-xs-2">
				<label for="firstApplicantmiddlename">Middle Name </label>
                  <input type="text" class="form-control" id="firstApplicantmiddlename" placeholder="Middle Name"  name="firstApplicantmiddlename" style="text-transform: capitalize;">
                 <span id="firstApplicantmiddlenameSpan" style="color:#FF0000"></span>
                </div>
                <div class="col-xs-2">
				<label for="firstApplicantlastname">Last Name </label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="firstApplicantlastname" placeholder="Last Name" name="firstApplicantlastname" style="text-transform: capitalize;">
                 <span id="firstApplicantlastnameSpan" style="color:#FF0000"></span>
                </div>
            
            
			<div class="box-body" id="maidenName1" hidden="hidden">
                <div class="col-xs-2">
				<label for="firstApplicantmaidenfirstname">Maiden First Name</label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="firstApplicantmaidenfirstname" placeholder="First Name" name="firstApplicantmaidenfirstname"  style="text-transform: capitalize;">
                  <span id="firstApplicantmaidenfirstnameSpan" style="color:#FF0000"></span>
                </div>
                <div class="col-xs-2">
				<label for="firstApplicantmaidenmiddlename">Maiden Middle Name </label>
                  <input type="text" class="form-control" id="firstApplicantmaidenmiddlename" placeholder="Middle Name"  name="firstApplicantmaidenmiddlename"  style="text-transform: capitalize;">
                 <span id="firstApplicantmaidenmiddlenameSpan" style="color:#FF0000"></span>
                </div>
                <div class="col-xs-2">
				<label for="firstApplicantmaidenlastname">Maiden Last Name </label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="firstApplicantmaidenlastname" placeholder="Last Name" name="firstApplicantmaidenlastname"  style="text-transform: capitalize;">
                 <span id="firstApplicantmaidenlastnameSpan" style="color:#FF0000"></span>
                </div>
            </div>
			</div>
		</div>		
			
			<div class="box-body">
              <div class="row">
              
                 <div class="col-xs-3">
			   <label for="radiobutton">Gender</label><label class="text-red">* </label>   <span id="firstApplicantGenderSpan" style="color:#FF0000"></span></br>
   				<input type="radio" name="firstApplicantGender" id="firstApplicantGender" value="Male" onclick = "firstApplicantMaidenMale()"> Male
				   &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
  					  <input type="radio" name="firstApplicantGender" id="firstApplicantGender" value="Female" onclick = "firstApplicantMaidenFemale()"> Female
                 </div>
                 
                  <div class="col-xs-3">
			   <label for="firstApplicantMarried">Marital Status</label><label class="text-red">* </label>   <span id="firstApplicantMarriedSpan" style="color:#FF0000"></span></br>
   				<input type="radio" name="firstApplicantMarried" id="firstApplicantMarried" value="Married" onclick = "firstApplicantMaidenFemale()"> Married
				   &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
  					  <input type="radio" name="firstApplicantMarried" id="firstApplicantMarried" value="Unmarried" onclick = "firstApplicantMaidenMale()"> Unmarried
                 </div>
                 
                <div class="col-xs-2">
			      <label>Date Of Birth </label><label class="text-red">* </label> 
				  <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                <input type="text" class="form-control pull-right" id="firstApplicantDob" name="firstApplicantDob" >
               </div>
                <span id="firstApplicantDobSpan" style="color:#FF0000"></span>
				   </div>
				   
				<div class="col-xs-2">
			    <label>Mobile No(Primary)</label><label class="text-red">* </label>
				  <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask='"mask": "9999999999"' data-mask name="firstApplicantmobileNumber1"  id="firstApplicantmobileNumber1">
                </div>
                   <span id="firstApplicantmobileNumber1Span" style="color:#FF0000"></span>
				 </div>
				  <div class="col-xs-2">
			    <label>Mobile No 2</label>
				  <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask='"mask": "9999999999"' data-mask name="firstApplicantmobileNumber2" data-mask id="firstApplicantmobileNumber2" >
                </div>
                 <span id="firstApplicantmobileNumber2Span" style="color:#FF0000"></span>
				 </div> 
				 </div>
            </div>
			
			<div class="box-body">
              <div class="row">
              
                 <div class="col-xs-3">
				 <label for="firstApplicantSpuseName">Spouse Name </label>
                  <input type="text" class="form-control" id="firstApplicantSpuseName" placeholder="Spouse Name" name="firstApplicantSpuseName" style="text-transform: capitalize;" >
                 <span id="firstApplicantSpuseNameSpan" style="color:#FF0000"></span>
                </div>
                 
                  <div class="col-xs-3">
			      <label>Spouse's Date Of Birth </label>
				  <div class="input-group">
                  <div class="input-group-addon">
                   <i class="fa fa-calendar"></i>
                  </div>
                   <input type="text" class="form-control pull-right" id="firstApplicantSpouseDob" name="firstApplicantSpouseDob" >
                  </div>
                  <span id="firstApplicantSpouseDobSpan" style="color:#FF0000"></span>
				</div>
				   
				  <div class="col-xs-3">
			      <label>Anniversary Date </label>
				  <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
	                <input type="text" class="form-control pull-right" id="firstApplicantAnniversaryDate" name="firstApplicantAnniversaryDate" >
	               </div>
	                <span id="firstApplicantAnniversaryDateSpan" style="color:#FF0000"></span>
				   </div>
				   
				   <div class="col-xs-3">
				  <label for="firstApplicantFatherName">Father's Name </label>
                  <input type="text" class="form-control" id="firstApplicantFatherName" placeholder="Last Name" name="firstApplicantFatherName" style="text-transform: capitalize;" >
                 <span id="firstApplicantFatherNameSpan" style="color:#FF0000"></span>
                </div>
              </div>
            </div>
			
			<div class="box-body">
              <div class="row">
              <div class="col-xs-3">
			     <label for="firstApplicantEmailId">Email ID </label><label class="text-red">* </label>
				   <div class="input-group">
	                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
	                <input type="text" class="form-control" placeholder="Email" name ="firstApplicantEmailId" id="firstApplicantEmailId" >
	              </div>
                   <span id="firstApplicantEmailIdSpan" style="color:#FF0000"></span>
			     </div> 
                   <div class="col-xs-3">
			    <label for="firstApplicantPanCardNo">Pan Card No </label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="firstApplicantPanCardNo" placeholder="Pan Card No " name="firstApplicantPanCardNo"  style="text-transform:uppercase">
                  <span id="bookingPancardnoSpan" style="color:#FF0000"></span>
			     </div> 
				  <div class="col-xs-3">
			      <label for="firstApplicantAadharno">Aadhar Card No</label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="firstApplicantAadharno" placeholder="Adhar Card No " name="firstApplicantAadharno"  style="text-transform:uppercase">
			       <span id="firstApplicantAadharnoSpan" style="color:#FF0000"></span>
			     </div> 
			    <div class="col-xs-3">
				  <label for="firstApplicantMotherTonque">Mother Tongue</label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="firstApplicantMotherTonque" placeholder="Mother Tonque" name="firstApplicantMotherTonque"  style="text-transform: capitalize;">
                 <span id="firstApplicantMotherTonqueSpan" style="color:#FF0000"></span>
                </div>
                      
			   </div>
			  </div>
			
			 <div class="box-body">
              <div class="row">
              <div class="col-xs-3">
              <h4> <label for="">Present Address </label></h4>
              </div>
                  
              </div>
             </div>
                  	
			<div class="box-body">
              <div class="row">
                <div class="col-xs-3">
			   <label for="firstApplicantPresentAddress">Address </label><label class="text-red">* </label>
                 <textarea class="form-control" rows="1" id="firstApplicantPresentAddress" placeholder="Address" name="firstApplicantPresentAddress"> </textarea>
                  <span id="firstApplicantPresentAddressSpan" style="color:#FF0000"></span>
			     </div> 
			   <%--   
				   <div class="col-xs-2">
                  <label>Country </label><label class="text-red">* </label>
    			  <select class="form-control" name="firstApplicantPresentcountryId" id="firstApplicantPresentcountryId" onchange="getfirstApplicantPresentStateList(this.value)">
				<option selected="" value="Default">-Select Country-</option>
                   <c:forEach var="countryList" items="${countryList}">
	                    <option value="${countryList.countryId}">${countryList.countryName}</option>
	                </c:forEach>
                  </select>
                   <span id="firstApplicantPresentcountryIdSpan" style="color:#FF0000"></span>
                </div>               
                <div class="col-xs-2">
				 <label>State </label><label class="text-red">* </label>
				  <select class="form-control" name="firstApplicantPresentstateId" id="firstApplicantPresentstateId" onchange="getfirstApplicantPresentCityList(this.Value)">
			  		  <option selected="" value="Default">-Select State-</option>
                     <c:forEach var="stateList" items="${stateList}">
                       <option value="${stateList.stateId}">${stateList.stateId}</option>
				    </c:forEach>
                  </select>
                   <span id="firstApplicantPresentstateIdSpan" style="color:#FF0000"></span>
               </div> 
               <div class="col-xs-2">
				  <label>City </label><label class="text-red">* </label>
                      <select class="form-control" name ="firstApplicantPresentcityId" id="firstApplicantPresentcityId" onchange="getfirstApplicantPresentLocationAreaList(this.value)">
			  		<option selected="" value="Default">-Select City-</option>
                    <c:forEach var="cityList" items="${cityList}">
                    	<option value="${cityList.cityId}">${cityList.cityId}</option>
				    </c:forEach>
                  </select>
                  <span id="firstApplicantPresentcityIdSpan" style="color:#FF0000"></span>
				  </div> 
                 
				    <div class="col-xs-2">
				    <label>Area </label><label class="text-red">* </label>
				        <select class="form-control" name="firstApplicantPresentlocationareaId" id="firstApplicantPresentlocationareaId" onchange="getfirstApplicantPresentpinCode(this.value)">
			    		 <option selected="" value="Default">-Select Area-</option>
                     <c:forEach var="locationareaList" items="${locationareaList}">
                    	<option value="${locationareaList.locationareaId}">${locationareaList.locationareaId}</option>
				     </c:forEach>
				   </select>
                   <span id="firstApplicantPresentlocationareaIdSpan" style="color:#FF0000"></span>
				  </div> 
				   --%>
				  
				   <div class="col-xs-2">
                  <label for="firstApplicantPresentPincode">Pin Code </label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="firstApplicantPresentPincode" placeholder="Pin Code" name="firstApplicantPresentPincode">
                    <span id="firstApplicantPresentPincodeSpan" style="color:#FF0000"></span>
			     </div> 
              </div>
            </div>
				
		
			 <div class="box-body">
              <div class="row">
              <div class="col-xs-2">
              <h4> <label for="bookingaddress">Permanent Address </label></h4>
              </div>
               <div class="col-xs-3">
               
			  <label for="">Permanent Address Same as Present Address</label> 
   				</div>
   				 <div class="col-xs-4">
   				<input type="radio" name="firstApplicantPresentAndPermanentAddress" id="firstApplicantPresentAndPermanentAddress" value="Yes" onclick = "firstApplicantPresentAndPermanentAddressYes()"> Yes
				   &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
  					  <input type="radio" name="firstApplicantPresentAndPermanentAddress" id="firstApplicantPresentAndPermanentAddress" value="No" onclick = "firstApplicantPresentAndPermanentAddressNo()"> No
                 </div> 
                  
              </div>
             </div>
                  	
			<div class="box-body">
              <div class="row">
                <div class="col-xs-3">
			   <label for="firstApplicantPermanentaddress">Address </label><label class="text-red">* </label>
                 <textarea class="form-control" rows="1" id="firstApplicantPermanentaddress" placeholder="Address" name="firstApplicantPermanentaddress"> </textarea>
                  <span id="firstApplicantPermanentaddressSpan" style="color:#FF0000"></span>
			     </div> 
			     <%-- 
				   <div class="col-xs-2">
                  <label>Country </label><label class="text-red">* </label>
    			  <select class="form-control" name="firstApplicantPermanentcountryId" id="firstApplicantPermanentcountryId" onchange="getfirstApplicantPermanentStateList(this.value)">
				    <option selected="" value="Default">-Select Country-</option>
                    <c:forEach var="countryList" items="${countryList}">
	                <option value="${countryList.countryId}">${countryList.countryName}</option>
	                </c:forEach>
                  </select>
                  <span id="firstApplicantPermanentcountryIdSpan" style="color:#FF0000"></span>
                </div>               
                <div class="col-xs-2">
				 <label>State </label><label class="text-red">* </label>
				  <select class="form-control" name="firstApplicantPermanentstateId" id="firstApplicantPermanentstateId" onchange="getfirstApplicantPermanentCityList(this.Value)">
			  		 <option selected="" value="Default">-Select State-</option>
                     <c:forEach var="stateList" items="${stateList}">
                     <option value="${stateList.stateId}">${stateList.stateId}</option>
				     </c:forEach>
                  </select>
                  <span id="firstApplicantPermanentstateIdSpan" style="color:#FF0000"></span>
               </div> 
               <div class="col-xs-2">
				  <label>City </label><label class="text-red">* </label>
                      <select class="form-control" name ="firstApplicantPermanentcityId" id="firstApplicantPermanentcityId" onchange="getfirstApplicantPermanentLocationAreaList(this.value)">
			  		<option selected="" value="Default">-Select City-</option>
                    <c:forEach var="cityList" items="${cityList}">
                    	<option value="${cityList.cityId}">${cityList.cityId}</option>
				    </c:forEach>
                  </select>
                  <span id="firstApplicantPermanentcityIdSpan" style="color:#FF0000"></span>
				  </div> 
                 
				    <div class="col-xs-2">
				    <label>Area </label><label class="text-red">* </label>
				        <select class="form-control" name="firstApplicantPermanentlocationareaId" id="firstApplicantPermanentlocationareaId" onchange="getfirstApplicantPermanentpinCode(this.value)">
			    		 <option selected="" value="Default">-Select Area-</option>
                     <c:forEach var="locationareaList" items="${locationareaList}">
                    	<option value="${locationareaList.locationareaId}">${locationareaList.locationareaId}</option>
				     </c:forEach>
				   </select>
                   <span id="firstApplicantPermanentlocationareaIdSpan" style="color:#FF0000"></span>
				  </div> 
				   --%>
				  
				   <div class="col-xs-2">
                  <label for="firstApplicantPermanentPincode">Pin Code </label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="firstApplicantPermanentPincode" placeholder="Pin Code" name="firstApplicantPermanentPincode" >
                    <span id="firstApplicantPermanentPincodeSpan" style="color:#FF0000"></span>
			     </div> 
				  
			
              </div>
            </div>
					
			<div class="box-body">
              <div class="row">
              <div class="col-xs-3">
              <h4> <label for="bookingaddress">Professional Details </label></h4>
              </div>
                  
              </div>
             </div>
                  	
			<div class="box-body">
              <div class="row">
              
				   <div class="col-xs-3">
				    <label>Education Qualification </label><label class="text-red">* </label>
				     <input type="text" class="form-control" id="firstApplicantEducation" placeholder="Education Qualifaction" name="firstApplicantEducation"  style="text-transform: capitalize;">
                    <span id="firstApplicantEducationSpan" style="color:#FF0000"></span> 
                 </div>  
                 
               <div class="col-xs-3">
			    <label for="firstApplicantOccupation">Occupation </label><label class="text-red">* </label>
			     <select class="form-control" name="firstApplicantOccupation" id="firstApplicantOccupation" >
				  <option selected="" value="Default">-Select Occupation-</option>
					<c:forEach var="occupationList" items="${occupationList}">
                    <option value="${occupationList.occupationName}">${occupationList.occupationName}</option>
				    </c:forEach>				
				 </select>
                 <span id="firstApplicantOccupationSpan" style="color:#FF0000"></span>
                </div> 
                
				   <div class="col-xs-3">
				    <label>Name of Organization/ Business </label>
				     <input type="text" class="form-control" id="firstApplicantOrganizationName" placeholder="Name of Organization/ Business" name="firstApplicantOrganizationName" style="text-transform: capitalize;">
                    <span id="firstApplicantOrganizationNameSpan" style="color:#FF0000"></span> 
                 </div> 
                  
			  <div class="col-xs-3">
                  <label>Organizational Type</label> 
                  	<select class="form-control" name="firstApplicantOrganizationType" id="firstApplicantOrganizationType">
                  	<option selected="" value="Default">-Select Organizational Type-</option>
                    <option value="Govt. Services">Govt. Services</option>
                    <option value="Private Ltd.">Private Ltd.</option>
                    <option value="Public Ltd">Public Ltd</option>
                    <option value="Proprietary">Proprietary</option>
                    <option value="Other">Other</option>
                  	</select>
                    <span id="firstApplicantOrganizationTypeSpan" style="color:#FF0000"></span>
			   </div>
			 
               </div>
            </div>
            		
			<div class="box-body">
              <div class="row">
                              
                <div class="col-xs-3">
                 <label>Address of Organization/ Business </label>
  					 <textarea class="form-control" rows="1" id="firstApplicantOrganizationaddress" placeholder="Address" name="firstApplicantOrganizationaddress"> </textarea>
                    <span id="firstApplicantOrganizationaddressSpan" style="color:#FF0000"></span>
               </div> 
               
               <div class="col-xs-3">
			      <label for="firstApplicantofficeNumber">Office Phone Number</label>
				  <div class="input-group">
                  	<div class="input-group-addon">
                   <i class="fa fa-phone"></i>
                 	</div>
                    <input type="text" class="form-control" data-inputmask = '"mask": "(999) 999-99999"' data-mask name="firstApplicantofficeNumber" id="firstApplicantofficeNumber" >
                    <span id="firstApplicantofficeNumberSpan" style="color:#FF0000"></span>
                  </div>
			     </div> 
			      
              <div class="col-xs-3">
			     <label for="firstApplicantofficeEmail">Official Email ID </label>
				   <div class="input-group">
	                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
	                <input type="text" class="form-control" placeholder="Email" name ="firstApplicantofficeEmail" id="firstApplicantofficeEmail" >
	              </div>
                   <span id="firstApplicantofficeEmailSpan" style="color:#FF0000"></span>
			     </div>
			 
			   
			  <div class="col-xs-3">
                  <label>Industry Sector of work/ Business</label> 
                  	<select class="form-control" name="firstApplicantIndustrySector" id="firstApplicantIndustrySector">
                    <option selected="" value="Default">-Select Industry Sector of work/ Business-</option>
                    <option value="IT">IT</option>
                    <option value="ITES/BPO/KPO">ITES/BPO/KPO</option>
                    <option value="Manufacturing">Manufacturing</option>
                    <option value="Financial">Financial</option>
                    <option value="Hospitality Services">Hospitality Services</option>
                    <option value="Medical/ Pharmaceutical">Medical/ Pharmaceutical</option>
                    <option value="Media/Entertainment">Media/Entertainment</option>
                    <option value="Travel/ transport">Travel/ transport</option>
                    <option value="Retails Services">Retails Services</option>
                    <option value="Telecom">Telecom</option>
                    <option value="Other"> Other</option>
                  	</select>
                    <span id="firstApplicantIndustrySectorSpan" style="color:#FF0000"></span>
			   </div>
			    </div>
			 </div>    	 
			  		
			<div class="box-body">
              <div class="row">   
			  <div class="col-xs-3">
                  <label>Work Function/ Role</label> 
                  	<select class="form-control" name="firstApplicantWorkFunction" id="firstApplicantWorkFunction">
                  	<option selected="" value="Default">-Select Work Function/ Role-</option>
                    <option value="Software">Software</option>
                    <option value="Sales and Marketing">Sales and Marketing</option>
                    <option value="HR/ Administration">HR/ Administration</option>
                    <option value="Finance">Finance</option>
                    <option value="Production">Production</option>
                    <option value="Legal">Legal</option>
                    <option value="Operations">Operations</option>
                    <option value="Other">Other</option>
                  	</select>
                    <span id="firstApplicantWorkFunctionSpan" style="color:#FF0000"></span>
			   </div>
			
			  <div class="col-xs-3">
                  <label>Number of Years of work Experience</label> 
                  	<select class="form-control" name="firstApplicantExperience" id="firstApplicantExperience">
                  	<option selected="" value="Default">-Select Number of Years of work Experience-</option>
                    <option value="0-5 years">0-5 years</option>
                    <option value="6-10 years">6-10 years</option>
                    <option value="11-15 years">11-15 years</option>
                    <option value="16-20 years">16-20 years</option>
                    <option value="21-25 years">21-25 years</option>
                    <option value="26-30 years">26-30 years</option>
                    <option value=">30 years"> >30 years</option>
                  	</select>
                    <span id="firstApplicantExperienceSpan" style="color:#FF0000"></span>
			   </div>
			      	 
			        
			  <div class="col-xs-3">
                  <label>Annual Household Income(Rupees)</label> <label class="text-red">* </label>
                  	<select class="form-control" name="firstApplicantIncome" id="firstApplicantIncome">
                  	<option selected="" value="Default">-Select Annual Household Income-</option>
                    <option value="0-5 lakhs">0-5 lakhs</option>
                    <option value="6-10 lakhs">6-10 lakhs</option>
                    <option value="11-15 lakhs">11-15 lakhs</option>
                    <option value="16-20 lakhs">16-20 lakhs</option>
                    <option value="21-25 lakhs">21-25 lakhs </option>
                    <option value="26-30 lakhs">26-30 lakhs</option>
                    <option value="30-35 lakhs">30-35 lakhs</option>
                    <option value="36-40 lakhs">36-40 lakhs</option>
                    <option value="41-45 lakhs">41-45 lakhs </option>
                    <option value="56-50 lakhs">56-50 lakhs</option>
                    <option value=">50 lakhs">>50 lakhs</option>
                  	</select>
                    <span id="firstApplicantIncomeSpan" style="color:#FF0000"></span>
			   </div>
			      	 
			     	    
              </div>
            </div>
			
	
				
			<div class="box-body">
              <div class="row">
             <div  class="panel box box-success"></div>    
			     <div class="col-xs-6">
                   <div class="input-group">
                 	<h3>
                 	<!-- <span class="fa fa-user"></span>  -->
                    Second Applicant Details :  
                   </h3> 
       		 	  </div>
			     </div> 
              </div>
              </div>	
			 
			 
          <div class="box-body">
              <div class="row">
                <div class="col-xs-2">
				<label for="secondApplicantfirstname">First Name</label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="secondApplicantfirstname" placeholder="First Name" name="secondApplicantfirstname"  style="text-transform: capitalize;">
                  <span id="secondApplicantfirstnameSpan" style="color:#FF0000"></span>
                </div>
                <div class="col-xs-2">
				<label for="secondApplicantmiddlename">Middle Name </label>
                  <input type="text" class="form-control" id="secondApplicantmiddlename" placeholder="Middle Name"  name="secondApplicantmiddlename" style="text-transform: capitalize;">
                 <span id="secondApplicantmiddlenameSpan" style="color:#FF0000"></span>
                </div>
                <div class="col-xs-2">
				<label for="secondApplicantlastname">Last Name </label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="secondApplicantlastname" placeholder="Last Name" name="secondApplicantlastname" style="text-transform: capitalize;">
                 <span id="secondApplicantlastnameSpan" style="color:#FF0000"></span>
                </div>
            
            
			<div class="box-body" id="maidenName2" hidden="hidden">
                <div class="col-xs-2">
				<label for="secondApplicantmaidenfirstname">Maiden First Name</label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="secondApplicantmaidenfirstname" placeholder="First Name" name="secondApplicantmaidenfirstname"  style="text-transform: capitalize;">
                  <span id="secondApplicantmaidenfirstnameSpan" style="color:#FF0000"></span>
                </div>
                <div class="col-xs-2">
				<label for="secondApplicantmaidenmiddlename">Maiden Middle Name </label>
                  <input type="text" class="form-control" id="secondApplicantmaidenmiddlename" placeholder="Middle Name"  name="secondApplicantmaidenmiddlename"  style="text-transform: capitalize;">
                 <span id="secondApplicantmaidenmiddlenameSpan" style="color:#FF0000"></span>
                </div>
                <div class="col-xs-2">
				<label for="secondApplicantmaidenlastname">Maiden Last Name </label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="secondApplicantmaidenlastname" placeholder="Last Name" name="secondApplicantmaidenlastname" style="text-transform: capitalize;" >
                 <span id="secondApplicantmaidenlastnameSpan" style="color:#FF0000"></span>
                </div>
            </div>
			</div>
		</div>		
			
			<div class="box-body">
              <div class="row">
                 <div class="col-xs-3">
			   <label for="radiobutton">Gender</label><label class="text-red">* </label>   <span id="secondApplicantGenderSpan" style="color:#FF0000"></span></br>
   				<input type="radio" name="secondApplicantGender" id="secondApplicantGender" value="Male" onclick = "secondApplicantMaidenMale()"> Male
				   &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
  					  <input type="radio" name="secondApplicantGender" id="secondApplicantGender" value="Female" onclick = "secondApplicantMaidenFemale()"> Female
                 </div>
                  <div class="col-xs-3">
			   <label for="secondApplicantMarried">Marital Status</label><label class="text-red">* </label>   <span id="secondApplicantMarriedSpan" style="color:#FF0000"></span></br>
   				<input type="radio" name="secondApplicantMarried" id="secondApplicantMarried" value="Married" onclick = "secondApplicantMaidenFemale()"> Married
				   &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
  					  <input type="radio" name="secondApplicantMarried" id="secondApplicantMarried" value="Unmarried" onclick = "secondApplicantMaidenMale()"> Unmarried
                 </div>
                <div class="col-xs-2">
			      <label>Date Of Birth </label><label class="text-red">* </label> 
				  <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                <input type="text" class="form-control pull-right" id="secondApplicantDob" name="secondApplicantDob" >
               </div>
                <span id="secondApplicantDobSpan" style="color:#FF0000"></span>
				   </div>
				   
				<div class="col-xs-2">
			    <label>Mobile No(Primary)</label><label class="text-red">* </label>
				  <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask='"mask": "9999999999"' data-mask name="secondApplicantmobileNumber1" data-mask id="secondApplicantmobileNumber1"  >
                </div>
                   <span id="secondApplicantmobileNumber1Span" style="color:#FF0000"></span>
				 </div>
				  <div class="col-xs-2">
			    <label>Mobile No 2</label>
				  <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask='"mask": "9999999999"' data-mask name="secondApplicantmobileNumber2" data-mask id="secondApplicantmobileNumber2" >
                </div>
                 <span id="secondApplicantmobileNumber2Span" style="color:#FF0000"></span>
				 </div> 
				 </div>
            </div>
			
			<div class="box-body">
              <div class="row">
                 <div class="col-xs-3">
				 <label for="secondApplicantSpouseName">Spouse Name </label>
                  <input type="text" class="form-control" id="secondApplicantSpouseName" placeholder="Spouse Name" name="secondApplicantSpouseName" style="text-transform: capitalize;" >
                 <span id="secondApplicantSpouseNameSpan" style="color:#FF0000"></span>
                </div>
                  <div class="col-xs-3">
			      <label>Spouse's Date Of Birth </label>
				  <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                <input type="text" class="form-control pull-right" id="secondApplicantSpouseDob" name="secondApplicantSpouseDob"  style="text-transform: capitalize;">
               </div>
                <span id="secondApplicantSpouseDobSpan" style="color:#FF0000"></span>
				   </div>
				   <div class="col-xs-3">
			      <label>Anniversary Date </label>
				  <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
	                <input type="text" class="form-control pull-right" id="secondApplicantAnnivaversaryDate" name="secondApplicantAnnivaversaryDate" >
	               </div>
	                <span id="secondApplicantAnnivaversaryDateSpan" style="color:#FF0000"></span>
				   </div>
				   <div class="col-xs-3">
				  <label for="secondApplicantFatherName">Father's Name </label>
                  <input type="text" class="form-control" id="secondApplicantFatherName" placeholder="Father Name" name="secondApplicantFatherName" >
                 <span id="secondApplicantFatherNameSpan" style="color:#FF0000"></span>
                </div>
              </div>
            </div>
			
			<div class="box-body">
              <div class="row">
              <div class="col-xs-3">
			     <label for="secondApplicantEmail">Email ID </label><label class="text-red">* </label>
				   <div class="input-group">
	                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
	                <input type="text" class="form-control" placeholder="Email" name ="secondApplicantEmail" id="secondApplicantEmail" >
	              </div>
                   <span id="secondApplicantEmailSpan" style="color:#FF0000"></span>
			     </div> 
                   <div class="col-xs-3">
			    <label for="secondApplicantPancardno">Pan Card No </label><label class="text-red">* </label> 
                  <input type="text" class="form-control" id="secondApplicantPancardno" placeholder="Pan Card No " name="secondApplicantPancardno" style="text-transform:uppercase" >
                  <span id="bookingPancardnoSpan" style="color:#FF0000"></span>
			     </div> 
				  <div class="col-xs-2">
			      <label for="secondApplicantAadharno">Aadhar Card No</label><label class="text-red">* </label> 
                  <input type="text" class="form-control" id="secondApplicantAadharno" placeholder="Adhar Card No " name="secondApplicantAadharno" style="text-transform:uppercase" >
			       <span id="secondApplicantAadharnoSpan" style="color:#FF0000"></span>
			     </div> 
			    <div class="col-xs-2">
				  <label for="secondApplicantMotherTongue">Mother Tongue</label><label class="text-red">* </label> 
                  <input type="text" class="form-control" id="secondApplicantMotherTongue" placeholder="Mother Tongue" name="secondApplicantMotherTongue" style="text-transform: capitalize;" >
                 <span id="secondApplicantMotherTongueSpan" style="color:#FF0000"></span>
                </div>
                <div class="col-xs-2">
				  <label for="secondApplicantRelation">Relation With First Applicant</label><label class="text-red">* </label> 
                  <input type="text" class="form-control" id="secondApplicantRelation" placeholder="Relation With First Applicant" name="secondApplicantRelation"  style="text-transform: capitalize;">
                 <span id="secondApplicantRelationSpan" style="color:#FF0000"></span>
                </div>
                         
			   </div>
			  </div>
			
			 <div class="box-body">
              <div class="row">
              <div class="col-xs-3">
              <h4> <label for="">Present Address </label><label class="text-red">* </label></h4>
              </div>
                  
              </div>
             </div>
                  	
			<div class="box-body">
              <div class="row">
                <div class="col-xs-3">
			   <label for="secondApplicantPresentAddress">Address </label><label class="text-red">* </label>
                 <textarea class="form-control" rows="1" id="secondApplicantPresentAddress" placeholder="Address" name="secondApplicantPresentAddress"> </textarea>
                  <span id="secondApplicantPresentAddressSpan" style="color:#FF0000"></span>
			     </div> 
			     
			     <%-- 
				   <div class="col-xs-2">
                  <label>Country </label><label class="text-red">* </label>
    			  <select class="form-control" name="secondApplicantPresentcountryId" id="secondApplicantPresentcountryId" onchange="getsecondApplicantPresentStateList(this.value)">
				<option selected="" value="Default">-Select Country-</option>
                   <c:forEach var="countryList" items="${countryList}">
	                    <option value="${countryList.countryId}">${countryList.countryName}</option>
	                </c:forEach>
                  </select>
                   <span id="secondApplicantPresentcountryIdSpan" style="color:#FF0000"></span>
                </div>               
                <div class="col-xs-2">
				 <label>State </label><label class="text-red">* </label>
				  <select class="form-control" name="secondApplicantPresentstateId" id="secondApplicantPresentstateId" onchange="getsecondApplicantPresentCityList(this.Value)">
			  		  <option selected="" value="Default">-Select State-</option>
                     <c:forEach var="stateList" items="${stateList}">
                       <option value="${stateList.stateId}">${stateList.stateId}</option>
				    </c:forEach>
                  </select>
                   <span id="secondApplicantPresentstateIdSpan" style="color:#FF0000"></span>
               </div> 
               <div class="col-xs-2">
				  <label>City </label><label class="text-red">* </label>
                      <select class="form-control" name ="secondApplicantPresentcityId" id="secondApplicantPresentcityId" onchange="getsecondApplicantPresentLocationAreaList(this.value)">
			  		<option selected="" value="Default">-Select City-</option>
                    <c:forEach var="cityList" items="${cityList}">
                    	<option value="${cityList.cityId}">${cityList.cityId}</option>
				    </c:forEach>
                  </select>
                  <span id="secondApplicantPresentcityIdSpan" style="color:#FF0000"></span>
				  </div> 
                 
				    <div class="col-xs-2">
				    <label>Area </label><label class="text-red">* </label>
				        <select class="form-control" name="secondApplicantPresentlocationareaId" id="secondApplicantPresentlocationareaId" onchange="getsecondApplicantPresentpinCode(this.value)">
			    		 <option selected="" value="Default">-Select Area-</option>
                     <c:forEach var="locationareaList" items="${locationareaList}">
                    	<option value="${locationareaList.locationareaId}">${locationareaList.locationareaId}</option>
				     </c:forEach>
				   </select>
                   <span id="secondApplicantPresentlocationareaIdSpan" style="color:#FF0000"></span>
				  </div> 
				   --%>
				   <div class="col-xs-2">
                  <label for="secondApplicantPresentPincode">Pin Code </label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="secondApplicantPresentPincode" placeholder="Pin Code" name="secondApplicantPresentPincode" >
                    <span id="secondApplicantPresentPincodeSpan" style="color:#FF0000"></span>
			     </div> 
              </div>
            </div>
				
		
			 <div class="box-body">
              <div class="row">
              <div class="col-xs-2">
              <h4> <label for="bookingaddress">Permanent Address </label></h4>
              </div>
              <div class="col-xs-3">
			  <label for="">Permanent Address Same as Present Address</label> 
   				</div>
   				 <div class="col-xs-4">
   				<input type="radio" name="secondApplicantPresentAndPermanentAddress" id="secondApplicantPresentAndPermanentAddress" value="Yes" onclick = "secondApplicantPresentAndPermanentAddressYes()"> Yes
				   &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
  					  <input type="radio" name="secondApplicantPresentAndPermanentAddress" id="secondApplicantPresentAndPermanentAddress" value="No" onclick = "secondApplicantPresentAndPermanentAddressNo()"> No
                 </div> 
                      
              </div>
             </div>
                  	
			<div class="box-body">
              <div class="row">
                <div class="col-xs-3">
			   <label for="secondApplicantPermanentaddress">Address </label><label class="text-red">* </label>
                 <textarea class="form-control" rows="1" id="secondApplicantPermanentaddress" placeholder="Address" name="secondApplicantPermanentaddress"> </textarea>
                  <span id="secondApplicantPermanentaddressSpan" style="color:#FF0000"></span>
			     </div> 
			     
			     <%-- 
				   <div class="col-xs-2">
                  <label>Country </label><label class="text-red">* </label>
    			  <select class="form-control" name="secondApplicantPermanentcountryId" id="secondApplicantPermanentcountryId" onchange="getsecondApplicantPermanentStateList(this.value)">
				<option selected="" value="Default">-Select Country-</option>
                   <c:forEach var="countryList" items="${countryList}">
	                    <option value="${countryList.countryId}">${countryList.countryName}</option>
	                </c:forEach>
                  </select>
                   <span id="secondApplicantPermanentcountryIdSpan" style="color:#FF0000"></span>
                </div>               
                <div class="col-xs-2">
				 <label>State </label><label class="text-red">* </label>
				  <select class="form-control" name="secondApplicantPermanentstateId" id="secondApplicantPermanentstateId" onchange="getsecondApplicantPermanentCityList(this.Value)">
			  		  <option selected="" value="Default">-Select State-</option>
                     <c:forEach var="stateList" items="${stateList}">
                       <option value="${stateList.stateId}">${stateList.stateId}</option>
				    </c:forEach>
                  </select>
                   <span id="secondApplicantPermanentstateIdSpan" style="color:#FF0000"></span>
               </div> 
               <div class="col-xs-2">
				  <label>City </label><label class="text-red">* </label>
                      <select class="form-control" name ="secondApplicantPermanentcityId" id="secondApplicantPermanentcityId" onchange="getsecondApplicantPermanentLocationAreaList(this.value)">
			  		<option selected="" value="Default">-Select City-</option>
                    <c:forEach var="cityList" items="${cityList}">
                    	<option value="${cityList.cityId}">${cityList.cityId}</option>
				    </c:forEach>
                  </select>
                  <span id="secondApplicantPermanentcityIdSpan" style="color:#FF0000"></span>
				  </div> 
                 
				    <div class="col-xs-2">
				    <label>Area </label><label class="text-red">* </label>
				        <select class="form-control" name="secondApplicantPermanentlocationareaId" id="secondApplicantPermanentlocationareaId" onchange="getsecondApplicantPermanentpinCode(this.value)">
			    		 <option selected="" value="Default">-Select Area-</option>
                     <c:forEach var="locationareaList" items="${locationareaList}">
                    	<option value="${locationareaList.locationareaId}">${locationareaList.locationareaId}</option>
				     </c:forEach>
				   </select>
                   <span id="secondApplicantPermanentlocationareaIdSpan" style="color:#FF0000"></span>
				  </div> 
				   --%>
				   <div class="col-xs-2">
                  <label for="secondApplicantPermanentPincode">Pin Code </label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="secondApplicantPermanentPincode" placeholder="Pin Code" name="secondApplicantPermanentPincode" >
                    <span id="secondApplicantPermanentPincodeSpan" style="color:#FF0000"></span>
			     </div> 
				  
			
              </div>
            </div>
					
			<div class="box-body">
              <div class="row">
              <div class="col-xs-3">
              <h4> <label for="">Professional Details </label></h4>
              </div>
                  
              </div>
             </div>
                  	
			<div class="box-body">
              <div class="row">
              
				   <div class="col-xs-3">
				    <label>Education Qualification </label><label class="text-red">* </label>
				     <input type="text" class="form-control" id="secondApplicantEducation" placeholder="Education Qualifaction" name="secondApplicantEducation"  style="text-transform: capitalize;">
                    <span id="secondApplicantEducationSpan" style="color:#FF0000"></span> 
                 </div>  
                 
               <div class="col-xs-3">
			    <label for="secondApplicantOccupation">Occupation </label><label class="text-red">* </label>
			     <select class="form-control" name="secondApplicantOccupation" id="secondApplicantOccupation" >
				  <option selected="" value="Default">-Select Occupation-</option>
					<c:forEach var="occupationList" items="${occupationList}">
                    <option value="${occupationList.occupationName}">${occupationList.occupationName}</option>
				    </c:forEach>				
				 </select>
                 <span id="secondApplicantOccupationOccupationSpan" style="color:#FF0000"></span>
                </div> 
                
				   <div class="col-xs-3">
				    <label>Name of Organization/ Business </label>
				     <input type="text" class="form-control" id="secondApplicantOrganizationName" placeholder="Name of Organization/ Business" name="secondApplicantOrganizationName" style="text-transform: capitalize;" >
                    <span id="secondApplicantOccupationSpan" style="color:#FF0000"></span> 
                 </div> 
                  
			  <div class="col-xs-3">
                  <label>Organizational Type</label> 
                  	<select class="form-control" name="secondApplicantOrganizationType" id="secondApplicantOrganizationType" style="text-transform: capitalize;">
                  	<option selected="" value="Default">-Select Organizational Type-</option>
                    <option value="Govt. Services">Govt. Services</option>
                    <option value="Private Ltd.">Private Ltd.</option>
                    <option value="Public Ltd">Public Ltd</option>
                    <option value="Proprietary">Proprietary</option>
                    <option value="Other">Other</option>
                  	</select>
                    <span id="secondApplicantOrganizationTypeSpan" style="color:#FF0000"></span>
			   </div>
			 
               </div>
            </div>
            		
			<div class="box-body">
              <div class="row">
                              
                <div class="col-xs-3">
                 <label>Address of Organization/ Business </label>
  					 <textarea class="form-control" rows="1" id="secondApplicantOrganizationaddress" placeholder="Address" name="secondApplicantOrganizationaddress"> </textarea>
                    <span id="secondApplicantOrganizationaddressSpan" style="color:#FF0000"></span>
               </div> 
               
               <div class="col-xs-3">
			      <label for="secondApplicantofficeNumber">Office Phone Number</label>
				  <div class="input-group">
                  	<div class="input-group-addon">
                   <i class="fa fa-phone"></i>
                 	</div>
                    <input type="text" class="form-control" data-inputmask = '"mask": "(999) 999-99999"' data-mask name="secondApplicantofficeNumber" id="secondApplicantofficeNumber" >
                    <span id="secondApplicantofficeNumberSpan" style="color:#FF0000"></span>
                  </div>
			     </div> 
			      
              <div class="col-xs-3">
			     <label for="secondApplicantofficeEmail">Official Email ID </label>
				   <div class="input-group">
	                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
	                <input type="text" class="form-control" placeholder="Email" name ="secondApplicantofficeEmail" id="secondApplicantofficeEmail" >
	              </div>
                   <span id="secondApplicantofficeEmailSpan" style="color:#FF0000"></span>
			     </div>
			 
			   
			  <div class="col-xs-3">
                  <label>Industry Sector of work/ Business</label> 
                  	<select class="form-control" name="secondApplicantIndustrySector" id="secondApplicantIndustrySector">
                    <option selected="" value="Default">-Select Industry Sector of work/ Business-</option>
                    <option value="IT">IT</option>
                    <option value="ITES/BPO/KPO">ITES/BPO/KPO</option>
                    <option value="Manufacturing">Manufacturing</option>
                    <option value="Financial">Financial</option>
                    <option value="Hospitality Services">Hospitality Services</option>
                    <option value="Medical/ Pharmaceutical">Medical/ Pharmaceutical</option>
                    <option value="Media/Entertainment">Media/Entertainment</option>
                    <option value="Travel/ transport">Travel/ transport</option>
                    <option value="Retails Services">Retails Services</option>
                    <option value="Telecom">Telecom</option>
                    <option value="Other"> Other</option>
                  	</select>
                    <span id="secondApplicantIndustrySectorSpan" style="color:#FF0000"></span>
			   </div>
			    </div>
			 </div>    	 
			  		
			<div class="box-body">
              <div class="row">   
			  <div class="col-xs-3">
                  <label>Work Function/ Role</label> 
                  	<select class="form-control" name="secondApplicantWorkFunction" id="secondApplicantWorkFunction">
                  	<option selected="" value="Default">-Select Work Function/ Role-</option>
                    <option value="Software">Software</option>
                    <option value="Sales and Marketing">Sales and Marketing</option>
                    <option value="HR/ Administration">HR/ Administration</option>
                    <option value="Finance">Finance</option>
                    <option value="Production">Production </option>
                    <option value="Legal">Legal</option>
                    <option value="Operations">Operations</option>
                    <option value="Other">Other</option>
                  	</select>
                    <span id="secondApplicantWorkFunctionSpan" style="color:#FF0000"></span>
			   </div>
			
			  <div class="col-xs-3">
                  <label>Number of Years of work Experience</label> 
                  	<select class="form-control" name="secondApplicantExperience" id="secondApplicantExperience">
                  	<option selected="" value="Default">-Select Number of Years of work Experience-</option>
                    <option value="0-5 years">0-5 years</option>
                    <option value="6-10 years">6-10 years</option>
                    <option value="11-15 years">11-15 years</option>
                    <option value="16-20 years">16-20 years</option>
                    <option value="21-25 years">21-25 years </option>
                    <option value="26-30 years">26-30 years</option>
                    <option value=">30 years"> >30 years</option>
                  	</select>
                    <span id="secondApplicantExperienceSpan" style="color:#FF0000"></span>
			   </div>
			      	 
			        
			  <div class="col-xs-3">
                  <label>Annual Household Income(Rupees)</label> <label class="text-red">* </label>
                  	<select class="form-control" name="secondApplicantIncome" id="secondApplicantIncome">
                  	<option selected="" value="Default">-Select Annual Household Income-</option>
                    <option value="0-5 lakhs">0-5 lakhs</option>
                    <option value="6-10 lakhs">6-10 lakhs</option>
                    <option value="11-15 lakhs">11-15 lakhs</option>
                    <option value="16-20 lakhs">16-20 lakhs</option>
                    <option value="21-25 lakhs">21-25 lakhs </option>
                    <option value="26-30 lakhs">26-30 lakhs</option>
                    <option value="30-35 lakhs">30-35 lakhs</option>
                    <option value="36-40 lakhs">36-40 lakhs</option>
                    <option value="41-45 lakhs">41-45 lakhs </option>
                    <option value="56-50 lakhs">56-50 lakhs</option>
                    <option value=">50 lakhs"> >50 lakhs</option>
                  	</select>
                    <span id="secondApplicantIncomeSpan" style="color:#FF0000"></span>
			   </div>
			      	 
			     	    
              </div>
            </div>
			
									
					    
          </div>
     </div>
     </div>
 
         	  <input type="hidden" id="aggreementstatus" name="aggreementstatus" value="Aggreement">
			  <input type="hidden" id="creationDate" name="creationDate" value="">
			  <input type="hidden" id="updateDate" name="updateDate" value="">
			  <input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>"> 
			  
            <div class="box-body">
              <div class="row">
              </br>
               <div class="col-xs-1">
               </div>
                 <div class="col-xs-4">
                 <div class="col-xs-2">
                	<a href="BookingMaster"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
			     </div>
			     </div>
				 <div class="col-xs-4">
                  <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
			     </div>
				<div class="col-xs-2">
			    <button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
			    </div> 
			     
              </div>
			</div>
     </div>
         
    </section>
	</form>
    <!-- /.content -->
  </div>
 

  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->

<script>
function firstApplicantMaidenFemale()
{
	if((document.aggreementform.firstApplicantMarried[0].checked == true ) && (document.aggreementform.firstApplicantGender[1].checked == true ))
	{
 		document.getElementById('maidenName1').style.display ='block';
	}
}

function firstApplicantMaidenMale()
{
	 document.getElementById('maidenName1').style.display ='none';
}


function secondApplicantMaidenFemale()
{
	if((document.aggreementform.secondApplicantMarried[0].checked == true ) && (document.aggreementform.secondApplicantGender[1].checked == true ))
	{
 		document.getElementById('maidenName2').style.display ='block';
	}
}

function secondApplicantMaidenMale()
{
	 document.getElementById('maidenName2').style.display ='none';
}

function firstApplicantPresentAndPermanentAddressYes()
{
	var firstApplicantPresentPincode = $('#firstApplicantPresentPincode').val();
	var firstApplicantPresentAddress = $('#firstApplicantPresentAddress').val();

	 $("#firstApplicantPermanentaddress").empty();
	 $("#firstApplicantPermanentPincode").empty();
	 
	 document.aggreementform.firstApplicantPermanentaddress.value=firstApplicantPresentAddress;
	 document.aggreementform.firstApplicantPermanentPincode.value=firstApplicantPresentPincode;
	 
}
function firstApplicantPresentAndPermanentAddressNo()
{
	 $("#firstApplicantPermanentaddress").empty();
	 $("#firstApplicantPermanentPincode").empty();
	 
	 /* 
	 $.ajax({

			url : '${pageContext.request.contextPath}/getAllCountryList',
			type : 'Post',
			data : { },
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{
								
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select Location Area-");
								$("#firstApplicantPermanentlocationareaId").append(option);
								
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select City-");
								$("#firstApplicantPermanentcityId").append(option);
								
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select State-");
								$("#firstApplicantPermanentstateId").append(option);
								
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select Country-");
								$("#firstApplicantPermanentcountryId").append(option);
								
								for(var i=0;i<result.length;i++)
								{
									var option = $('<option />');
								    option.attr('value',result[i].countryId).text(result[i].countryId);
								    $("#firstApplicantPermanentcountryId").append(option);
								 } 
							} 
							else
							{
								alert("failure111");
								//$("#ajax_div").hide();
							}

						}
			});
	 
	 	 */
	 
	 	
}

function secondApplicantPresentAndPermanentAddressYes()
{
	var secondApplicantPresentPincode = $('#secondApplicantPresentPincode').val();
	var secondApplicantPresentAddress = $('#secondApplicantPresentAddress').val();

	 $("#secondApplicantPermanentaddress").empty();
	 $("#secondApplicantPermanentPincode").empty();
	 
	 document.aggreementform.secondApplicantPermanentaddress.value=secondApplicantPresentAddress;
	 document.aggreementform.secondApplicantPermanentPincode.value=secondApplicantPresentPincode;
	 
}
function secondApplicantPresentAndPermanentAddressNo()
{
	 $("#secondApplicantPermanentAddress").empty();
	 $("#secondApplicantPermanentPincode").empty();
	 
	 	/* 
	 $.ajax({

			url : '${pageContext.request.contextPath}/getAllCountryList',
			type : 'Post',
			data : { },
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{

								var option = $('<option/>');
								option.attr('value',"Default").text("-Select Location Area-");
								$("#secondApplicantPermanentlocationareaId").append(option);
								
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select City-");
								$("#secondApplicantPermanentcityId").append(option);
								
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select State-");
								$("#secondApplicantPermanentstateId").append(option);
								
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select Country-");
								$("#secondApplicantPermanentcountryId").append(option);
								
								for(var i=0;i<result.length;i++)
								{
									var option = $('<option />');
								    option.attr('value',result[i].countryId).text(result[i].countryId);
								    $("#secondApplicantPermanentcountryId").append(option);
								 } 
							} 
							else
							{
								alert("failure111");
								//$("#ajax_div").hide();
							}

						}
			});
	 
	 	
	  */
	 	
}

function clearall(){
	$('#firstApplicantfirstnameSpan').html('');
	$('#firstApplicantmiddlenameSpan').html('');
	$('#firstApplicantlastnameSpan').html('');
	$('#firstApplicantmaidenfirstnameSpan').html('');
	$('#firstApplicantmaidenmiddlenameSpan').html('');
	$('#firstApplicantmaidenlastnameSpan').html('');
	$('#firstApplicantGenderSpan').html('');
	$('#firstApplicantMarriedSpan').html('');
	$('#firstApplicantDobSpan').html('');
	$('#firstApplicantmobileNumber1Span').html('');
	$('#firstApplicantmobileNumber2Span').html('');
	$('#firstApplicantSpuseNameSpan').html('');
	$('#firstApplicantSpouseDobSpan').html('');
	$('#firstApplicantAnniversaryDateSpan').html('');
	$('#firstApplicantFatherNameSpan').html('');
	$('#firstApplicantEmailIdSpan').html('');
	$('#firstApplicantPanCardNoSpan').html('');
	$('#firstApplicantAadharnoSpan').html('');
	$('#firstApplicantMotherTonqueSpan').html('');
	$('#firstApplicantPresentAddressSpan').html('');
	$('#firstApplicantPresentlocationareaIdSpan').html('');
	$('#firstApplicantPresentPincodeSpan').html('');
	$('#firstApplicantPermanentaddressSpan').html('');
	$('#firstApplicantPermanentlocationareaIdSpan').html('');
	$('#firstApplicantPermanentPincodeSpan').html('');
	$('#firstApplicantEducationSpan').html('');
	$('#firstApplicantOccupationSpan').html('');
	$('#firstApplicantOrganizationNameSpan').html('');
	$('#firstApplicantOrganizationTypeSpan').html('');
	$('#firstApplicantOrganizationaddressSpan').html('');
	$('#firstApplicantofficeNumberSpan').html('');
	$('#firstApplicantofficeEmailSpan').html('');
	$('#firstApplicantIndustrySectorSpan').html('');
	$('#firstApplicantWorkFunctionSpan').html('');
	$('#firstApplicantExperienceSpan').html('');
	$('#firstApplicantIncomeSpan').html('');

	$('#secondApplicantfirstnameSpan').html('');
	$('#secondApplicantmiddlenameSpan').html('');
	$('#secondApplicantlastnameSpan').html('');
	$('#secondApplicantmaidenfirstnameSpan').html('');
	$('#secondApplicantmaidenmiddlenameSpan').html('');
	$('#secondApplicantmaidenlastnameSpan').html('');
	$('#secondApplicantGenderSpan').html('');
	$('#secondApplicantMarriedSpan').html('');
	$('#secondApplicantDobSpan').html('');
	$('#secondApplicantmobileNumber1Span').html('');
	$('#secondApplicantmobileNumber2Span').html('');
	$('#secondApplicantSpouseNameSpan').html('');
	$('#secondApplicantSpouseDobSpan').html('');
	$('#secondApplicantAnnivaversaryDateSpan').html('');
	$('#secondApplicantFatherNameSpan').html('');
	$('#secondApplicantEmailSpan').html('');
	$('#secondApplicantPancardnoSpan').html('');
	$('#secondApplicantAadharnoSpan').html('');
	$('#secondApplicantMotherTongueSpan').html('');
	$('#secondApplicantRelationSpan').html('');
	$('#secondApplicantPresentAddressSpan').html('');
	$('#secondApplicantPresentPincodeSpan').html('');
	$('#secondApplicantPermanentaddressSpan').html('');
	$('#secondApplicantPermanentPincodeSpan').html('');
	$('#secondApplicantEducationSpan').html('');
	$('#secondApplicantOccupationSpan').html('');
	$('#secondApplicantOrganizationNameSpan').html('');
	$('#secondApplicantOrganizationTypeSpan').html('');
	$('#secondApplicantOrganizationaddressSpan').html('');
	$('#secondApplicantofficeNumberSpan').html('');
	$('#secondApplicantofficeEmailSpan').html('');
	$('#secondApplicantIndustrySectorSpan').html('');
	$('#secondApplicantWorkFunctionSpan').html('');
	$('#secondApplicantExperienceSpan').html('');
	$('#secondApplicantIncomeSpan').html('');
	$('#aggreementstatusSpan').html('');

	
}

function init()
{
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	
	 document.aggreementform.firstApplicantfirstname.focus();
}

function validate()
{ 
	
clearall();
	
	//validation for first name
	if(document.aggreementform.firstApplicantfirstname.value=="")
	{
		
		$('#firstApplicantfirstnameSpan').html('First name should not be empty..!');
		document.aggreementform.firstApplicantfirstname.focus();
		return false;
	}
	else if(document.aggreementform.firstApplicantfirstname.value.match(/^[\s]+$/))
	{
		$('#firstApplicantfirstnameSpan').html('First name should not be empty..!');
		document.aggreementform.firstApplicantfirstname.value="";
		document.aggreementform.firstApplicantfirstname.focus();
		return false;
	}
	else if(!document.aggreementform.firstApplicantfirstname.value.match(/^[A-Za-z]+[.]{0,1}$/))
	{
		$('#firstApplicantfirstnameSpan').html('First name should contain alphabets only..!');
		//document.aggreementform.enqfirstName.value="";
		document.aggreementform.firstApplicantfirstname.focus();
		return false;
	}
	
	   //validation for middle name
    if(document.aggreementform.firstApplicantmiddlename.value.length!=0)
    {
    	 if(document.aggreementform.firstApplicantmiddlename.value.match(/^[\s]+$/))
    		{
    			$('#firstApplicantmiddlenameSpan').html('Middel name should not be empty..!');
    			document.aggreementform.firstApplicantmiddlename.value="";
    			document.aggreementform.firstApplicantmiddlename.focus();
    			return false;
    		}
    		else if(!document.aggreementform.firstApplicantmiddlename.value.match(/^[A-Za-z]+[.]{0,1}$/))
    		{
    			$('#firstApplicantmiddlenameSpan').html('Middel name should contain alphabets only..!');
    			//document.aggreementform.enqfirstName.value="";
    			document.aggreementform.firstApplicantmiddlename.focus();
    			return false;
    		}
    }
	
	
	//validation for last name
	if(document.aggreementform.firstApplicantlastname.value=="")
	{
		 $('#firstApplicantlastnameSpan').html('Last name should not be empty..!');
		document.aggreementform.firstApplicantlastname.focus();
		return false;
	}
	else if(document.aggreementform.firstApplicantlastname.value.match(/^[\s]+$/))
	{
		 $('#firstApplicantlastnameSpan').html('Last name should not be empty..!');
		document.aggreementform.firstApplicantlastname.focus();
		return false;
	}
	else if(!document.aggreementform.firstApplicantlastname.value.match(/^[A-Za-z]+[.]{0,1}$/))
	{
		 $('#firstApplicantlastnameSpan').html('Last name should contain alphabets only..!');
		document.aggreementform.firstApplicantlastname.value="";
		document.aggreementform.firstApplicantlastname.focus();
		return false;
	}		

	
	//validation for gender selection
    if(( document.aggreementform.firstApplicantGender[0].checked == false ) && ( document.aggreementform.firstApplicantGender[1].checked == false ) )
	{
    	$('#firstApplicantGenderSpan').html('Please, choose your Gender..!');
		document.aggreementform.firstApplicantGender[0].focus();
		return false;
	}
    
    if(( document.aggreementform.firstApplicantMarried[0].checked == false ) && ( document.aggreementform.firstApplicantMarried[1].checked == false ) )
	{
    	$('#firstApplicantMarriedSpan').html('Please, choose your married status..!');
		document.aggreementform.firstApplicantMarried[0].focus();
		return false;
	}
	  
     
    if(( document.aggreementform.firstApplicantGender[1].checked == true ) && (document.aggreementform.firstApplicantMarried[0].checked == true ))
	  {
		if(document.aggreementform.firstApplicantmaidenfirstname.value=="")
		{
			$('#firstApplicantmaidenfirstnameSpan').html('First name should not be empty..!');
			document.aggreementform.firstApplicantmaidenfirstname.focus();
			return false;
		}
		else if(document.aggreementform.firstApplicantmaidenfirstname.value.match(/^[\s]+$/))
		{
			$('#firstApplicantmaidenfirstnameSpan').html('First name should not be empty..!');
			document.aggreementform.firstApplicantmaidenfirstname.value="";
			document.aggreementform.firstApplicantmaidenfirstname.focus();
			return false;
		}
		else if(!document.aggreementform.firstApplicantmaidenfirstname.value.match(/^[A-Za-z]+[.]{0,1}$/))
		{
			$('#firstApplicantmaidenfirstnameSpan').html('First name should contain alphabets only..!');
			//document.aggreementform.enqfirstName.value="";
			document.aggreementform.firstApplicantmaidenfirstname.focus();
			return false;
		}
		
		   //validation for middle name
	    if(document.aggreementform.firstApplicantmaidenmiddlename.value.length!=0)
	    {
	    	 if(document.aggreementform.firstApplicantmaidenmiddlename.value.match(/^[\s]+$/))
	    		{
	    			$('#firstApplicantmaidenmiddlenameSpan').html('Middel name should not be empty..!');
	    			document.aggreementform.firstApplicantmaidenmiddlename.value="";
	    			document.aggreementform.firstApplicantmaidenmiddlename.focus();
	    			return false;
	    		}
	    		else if(!document.aggreementform.firstApplicantmaidenmiddlename.value.match(/^[A-Za-z]+[.]{0,1}$/))
	    		{
	    			$('#firstApplicantmaidenmiddlenameSpan').html('Middel name should contain alphabets only..!');
	    			//document.aggreementform.enqfirstName.value="";
	    			document.aggreementform.firstApplicantmaidenmiddlename.focus();
	    			return false;
	    		}
	    }
    	//validation for last name
		if(document.aggreementform.firstApplicantmaidenlastname.value=="")
		{
			 $('#firstApplicantmaidenlastnameSpan').html('Last name should not be empty..!');
			document.aggreementform.firstApplicantmaidenlastname.focus();
			return false;
		}
		else if(document.aggreementform.firstApplicantmaidenlastname.value.match(/^[\s]+$/))
		{
			 $('#firstApplicantmaidenlastnameSpan').html('Last name should not be empty..!');
			document.aggreementform.firstApplicantmaidenlastname.focus();
			return false;
		}
		else if(!document.aggreementform.firstApplicantmaidenlastname.value.match(/^[A-Za-z]+[.]{0,1}$/))
		{
			 $('#firstApplicantmaidenlastnameSpan').html('Last name should contain alphabets only..!');
			document.aggreementform.firstApplicantmaidenlastname.value="";
			document.aggreementform.firstApplicantmaidenlastname.focus();
			return false;
		}		
		  
		  
	  }
	 
	  //validation for date of birth
	  if(document.aggreementform.firstApplicantDob.value=="")
		{
			$('#firstApplicantDobSpan').html('Please select date of birth..!');
			document.aggreementform.firstApplicantDob.focus();
			return false;
		}
	  else if(document.aggreementform.firstApplicantDob.value!="")
		{
				var today = new Date();
				var dd1 = today.getDate();
				var mm1 = today.getMonth()+1;
				var yy1 = today.getFullYear();
				
		    	var dob= document.aggreementform.firstApplicantDob.value;
		   	var dob2=dob.split("/");
		   
		    var yydiff=parseInt(yy1)-parseInt(dob2[2]);
		    if(yydiff<=18)
		    {
		    		 $('#firstApplicantDobSpan').html(' date of birth should be 18 or 18+ years..!');
		    		document.aggreementform.firstApplicantDob.value="";
		    		document.aggreementform.firstApplicantDob.focus();
		    		return false;
		    }
		}
	
	  
	  

		//validation for mobile number 1
		if(document.aggreementform.firstApplicantmobileNumber1.value=="")
		{
			 $('#firstApplicantmobileNumber1Span').html('Please, enter primary mobile number..!');
			document.aggreementform.firstApplicantmobileNumber1.value="";
			document.aggreementform.firstApplicantmobileNumber1.focus();
			return false;
		}
		else if(!document.aggreementform.firstApplicantmobileNumber1.value.match(/^[0-9]{10}$/))
		{
			 $('#firstApplicantmobileNumber1Span').html(' enter valid primary mobile number..!');
			document.aggreementform.firstApplicantmobileNumber1.value="";
			document.aggreementform.firstApplicantmobileNumber1.focus();
			return false;	
		}
		
		//validation for mobile number 2
		if(document.aggreementform.firstApplicantmobileNumber2.value.length!=0)
		{
			if(!document.aggreementform.firstApplicantmobileNumber2.value.match(/^[0-9]{10}$/))
			{
				 $('#firstApplicantmobileNumber2Span').html(' enter valid secondary mobile number..!');
				document.aggreementform.firstApplicantmobileNumber2.value="";
				document.aggreementform.firstApplicantmobileNumber2.focus();
				return false;	
			}
		}
	  
	  

		//validation for email
		if(document.aggreementform.firstApplicantEmailId.value=="")
		{
			 $('#firstApplicantEmailIdSpan').html('Email Id should not be blank..!');
			document.aggreementform.firstApplicantEmailId.focus();
			return false;
		}
		else if(!document.aggreementform.firstApplicantEmailId.value.match(/^(([\-\w]+)\.?)+@(([\-\w]+)\.?)+\.[a-z]{2,4}$/))
		{
			 $('#firstApplicantEmailIdSpan').html(' enter valid email id..!');
			document.aggreementform.firstApplicantEmailId.value="";
			document.aggreementform.firstApplicantEmailId.focus();
			return false;
		}
		
		
		
		//validation for pan card
		
		if(document.aggreementform.firstApplicantPanCardNo.value=="")
		{
			 $('#firstApplicantPanCardNoSpan').html('Please, enter PAN number..!');
			document.aggreementform.firstApplicantPanCardNo.value="";
			document.aggreementform.firstApplicantPanCardNo.focus();
			return false;
		}
		else if(!document.aggreementform.firstApplicantPanCardNo.value.match(/^[A-Za-z]{5}[0-9]{4}[A-z]{1}$/))
		{
			 $('#firstApplicantPanCardNoSpan').html(' PAN number must start with 5 alphabets follwed by 4 digit number and 1 alphabet..!');
			document.aggreementform.firstApplicantPanCardNo.value="";
			document.aggreementform.firstApplicantPanCardNo.focus();
			return false;	
		}
		
		//validation for aadhar number
			
		if(document.aggreementform.firstApplicantAadharno.value=="")
		{
			 $('#firstApplicantAadharnoSpan').html('Please, enter Aadhar number..!');
			document.aggreementform.firstApplicantAadharno.value="";
			document.aggreementform.firstApplicantAadharno.focus();
			return false;
		}
		else if(!document.aggreementform.firstApplicantAadharno.value.match(/^\d{4}\d{4}\d{4}$/))
		{
			 $('#firstApplicantAadharnoSpan').html('Aadhar card number should be 12 digit number only..!');
			document.aggreementform.firstApplicantAadharno.value="";
			document.aggreementform.firstApplicantAadharno.focus();
			return false;	
		}
		
	
		//validation for last name
		if(document.aggreementform.firstApplicantMotherTonque.value=="")
		{
			 $('#firstApplicantMotherTonqueSpan').html('Mother Tonque should not be empty..!');
			document.aggreementform.firstApplicantMotherTonque.focus();
			return false;
		}
		else if(document.aggreementform.firstApplicantMotherTonque.value.match(/^[\s]+$/))
		{
			 $('#firstApplicantMotherTonqueSpan').html('Mother Tonque should not be empty..!');
			document.aggreementform.firstApplicantMotherTonque.focus();
			return false;
		}
		else if(!document.aggreementform.firstApplicantMotherTonque.value.match(/^[A-Za-z]+[.]{0,1}$/))
		{
			 $('#firstApplicantMotherTonqueSpan').html('Mother Tonque should contain alphabets only..!');
			document.aggreementform.firstApplicantMotherTonque.value="";
			document.aggreementform.firstApplicantMotherTonque.focus();
			return false;
		}		
		
			
	//validation for address
	if(document.aggreementform.firstApplicantPresentAddress.value=="")
	{
		 $('#firstApplicantPresentAddressSpan').html('Please, enter address..!');
		document.aggreementform.firstApplicantPresentAddress.focus();
		return false;
	}
	else if(document.aggreementform.firstApplicantPresentAddress.value.match(/^[\s]+$/))
	{
		 $('#firstApplicantPresentAddressSpan').html('Please, enter address name..!');
		document.aggreementform.firstApplicantPresentAddress.focus();
		return false;
	}
/* 	else if(!document.aggreementform.firstApplicantPresentAddress.value.match(/^[a-zA-Z0-9()-,.\s]+$/))
	{
		 $('#firstApplicantPresentAddressSpan').html('Please, use only alphabets and some special characters like (),.-  for person adrress..!');
		document.aggreementform.firstApplicantPresentAddress.value="";
		document.aggreementform.firstApplicantPresentAddress.focus();
		return false;
	}
	 */
	 /* 
	//validation for country name
	if(document.aggreementform.firstApplicantPresentcountryId.value=="Default")
	{
		 $('#firstApplicantPresentcountryIdSpan').html('Please, select country name..!');
		document.aggreementform.firstApplicantPresentcountryId.focus();
		return false;
	}
	
	//validation for state name
	if(document.aggreementform.firstApplicantPresentstateId.value=="Default")
	{
		 $('#firstApplicantPresentstateIdSpan').html('Please, select state name..!');
		document.aggreementform.firstApplicantPresentstateId.focus();
		return false;
	}
	
	//validation for city name
	if(document.aggreementform.firstApplicantPresentcityId.value=="Default")
	{
		 $('#firstApplicantPresentcityIdSpan').html('Please, select city name..!');
		document.aggreementform.firstApplicantPresentcityId.focus();
		return false;
	}
	
	//validation for location area name
	if(document.aggreementform.firstApplicantPresentlocationareaId.value=="Default")
	{
		 $('#firstApplicantPresentlocationareaIdSpan').html('Please, select location name..!');
		document.aggreementform.firstApplicantPresentlocationareaId.focus();
		return false;
	}
	
		 */
	//validation for address
	if(document.aggreementform.firstApplicantPermanentaddress.value=="")
	{
		 $('#firstApplicantPermanentaddressSpan').html('Please, enter address..!');
		document.aggreementform.firstApplicantPermanentaddress.focus();
		return false;
	}
	else if(document.aggreementform.firstApplicantPermanentaddress.value.match(/^[\s]+$/))
	{
		 $('#firstApplicantPermanentaddressSpan').html('Please, enter address name..!');
		document.aggreementform.firstApplicantPermanentaddress.focus();
		return false;
	}
/* 	else if(!document.aggreementform.firstApplicantPermanentaddress.value.match(/^[a-zA-Z0-9()-,.\s]+$/))
	{
		 $('#firstApplicantPermanentaddressSpan').html('Please, use only alphabets and some special characters like (),.-  for person adrress..!');
		document.aggreementform.firstApplicantPermanentaddress.value="";
		document.aggreementform.firstApplicantPermanentaddress.focus();
		return false;
	} */
	/* 
	//validation for country name
	if(document.aggreementform.firstApplicantPermanentcountryId.value=="Default")
	{
		 $('#firstApplicantPermanentcountryIdSpan').html('Please, select country name..!');
		document.aggreementform.firstApplicantPermanentcountryId.focus();
		return false;
	}
	
	//validation for state name
	if(document.aggreementform.firstApplicantPermanentstateId.value=="Default")
	{
		 $('#firstApplicantPermanentstateIdSpan').html('Please, select state name..!');
		document.aggreementform.firstApplicantPermanentstateId.focus();
		return false;
	}
	
	//validation for city name
	if(document.aggreementform.firstApplicantPermanentcityId.value=="Default")
	{
		 $('#firstApplicantPermanentcityIdSpan').html('Please, select city name..!');
		document.aggreementform.firstApplicantPermanentcityId.focus();
		return false;
	}
	
	//validation for location area name
	if(document.aggreementform.firstApplicantPermanentlocationareaId.value=="Default")
	{
		 $('#firstApplicantPermanentlocationareaIdSpan').html('Please, select location name..!');
		document.aggreementform.firstApplicantPermanentlocationareaId.focus();
		return false;
	}
	 */
		
	
	if(document.aggreementform.firstApplicantEducation.value=="")
	{
		 $('#firstApplicantEducationSpan').html('Please, enter education..!');
		document.aggreementform.firstApplicantEducation.focus();
		return false;
	}
	else if(document.aggreementform.firstApplicantEducation.value.match(/^[\s]+$/))
	{
		 $('#firstApplicantEducationSpan').html('Please, enter education..!');
		document.aggreementform.firstApplicantEducation.focus();
		return false;
	}

	
	//validation for occupation
	if(document.aggreementform.firstApplicantOccupation.value=="Default")
	{
		 $('#firstApplicantOccupationSpan').html('Please, select proper occupation..!');
		document.aggreementform.firstApplicantOccupation.focus();
		return false;
	}
	
	if(document.aggreementform.firstApplicantIncome.value=="Default")
	{
		 $('#firstApplicantIncomeSpan').html('Please, select Income..!');
		document.aggreementform.firstApplicantIncome.focus();
		return false;
	}
	
	
	if(document.aggreementform.secondApplicantfirstname.value.length!=0)
	{	
		
	if(document.aggreementform.secondApplicantfirstname.value=="")
	{
		
		$('#secondApplicantfirstnameSpan').html('First name should not be empty..!');
		document.aggreementform.secondApplicantfirstname.focus();
		return false;
	}
	else if(document.aggreementform.secondApplicantfirstname.value.match(/^[\s]+$/))
	{
		$('#secondApplicantfirstnameSpan').html('First name should not be empty..!');
		document.aggreementform.secondApplicantfirstname.value="";
		document.aggreementform.secondApplicantfirstname.focus();
		return false;
	}
	else if(!document.aggreementform.secondApplicantfirstname.value.match(/^[A-Za-z]+[.]{0,1}$/))
	{
		$('#secondApplicantfirstnameSpan').html('First name should contain alphabets only..!');
		//document.aggreementform.enqfirstName.value="";
		document.aggreementform.secondApplicantfirstname.focus();
		return false;
	}
	
	
	//validation for last name
	if(document.aggreementform.secondApplicantlastname.value=="")
	{
		 $('#secondApplicantlastnameSpan').html('Last name should not be empty..!');
		document.aggreementform.secondApplicantlastname.focus();
		return false;
	}
	else if(document.aggreementform.secondApplicantlastname.value.match(/^[\s]+$/))
	{
		 $('#secondApplicantlastnameSpan').html('Last name should not be empty..!');
		document.aggreementform.secondApplicantlastname.focus();
		return false;
	}
	else if(!document.aggreementform.secondApplicantlastname.value.match(/^[A-Za-z]+[.]{0,1}$/))
	{
		 $('#secondApplicantlastnameSpan').html('Last name should contain alphabets only..!');
		document.aggreementform.secondApplicantlastname.value="";
		document.aggreementform.secondApplicantlastname.focus();
		return false;
	}		

	
	//validation for gender selection
    if(( document.aggreementform.secondApplicantGender[0].checked == false ) && ( document.aggreementform.secondApplicantGender[1].checked == false ) )
	{
    	$('#secondApplicantGenderSpan').html('Please, choose your Gender..!');
		document.aggreementform.secondApplicantGender[0].focus();
		return false;
	}
    
    if(( document.aggreementform.secondApplicantMarried[0].checked == false ) && ( document.aggreementform.secondApplicantMarried[1].checked == false ) )
	{
    	$('#secondApplicantMarriedSpan').html('Please, choose your married status..!');
		document.aggreementform.secondApplicantMarried[0].focus();
		return false;
	}
	  
     
    if(( document.aggreementform.secondApplicantGender[1].checked == true ) && (document.aggreementform.secondApplicantMarried[0].checked == true ))
	  {
		if(document.aggreementform.secondApplicantmaidenfirstname.value=="")
		{
			$('#secondApplicantmaidenfirstnameSpan').html('First name should not be empty..!');
			document.aggreementform.secondApplicantmaidenfirstname.focus();
			return false;
		}
		else if(document.aggreementform.secondApplicantmaidenfirstname.value.match(/^[\s]+$/))
		{
			$('#secondApplicantmaidenfirstnameSpan').html('First name should not be empty..!');
			document.aggreementform.secondApplicantmaidenfirstname.value="";
			document.aggreementform.secondApplicantmaidenfirstname.focus();
			return false;
		}
		/* else if(!document.aggreementform.secondApplicantmaidenfirstname.value.match(/^[A-Za-z]+[.]{0,1}$/))
		{
			$('#secondApplicantmaidenfirstnameSpan').html('First name should contain alphabets only..!');
			//document.aggreementform.enqfirstName.value="";
			document.aggreementform.secondApplicantmaidenfirstname.focus();
			return false;
		}
		 */
    	//validation for last name
		if(document.aggreementform.secondApplicantmaidenlastname.value=="")
		{
			 $('#secondApplicantmaidenlastnameSpan').html('Last name should not be empty..!');
			document.aggreementform.secondApplicantmaidenlastname.focus();
			return false;
		}
		else if(document.aggreementform.secondApplicantmaidenlastname.value.match(/^[\s]+$/))
		{
			 $('#secondApplicantmaidenlastnameSpan').html('Last name should not be empty..!');
			document.aggreementform.secondApplicantmaidenlastname.focus();
			return false;
		}
		/* else if(!document.aggreementform.secondApplicantmaidenlastname.value.match(/^[A-Za-z]+[.]{0,1}$/))
		{
			 $('#secondApplicantmaidenlastnameSpan').html('Last name should contain alphabets only..!');
			document.aggreementform.secondApplicantmaidenlastname.value="";
			document.aggreementform.secondApplicantmaidenlastname.focus();
			return false;
		}	 */	
		  
		  
	  }
	 
	  //validation for date of birth
	  if(document.aggreementform.secondApplicantDob.value=="")
		{
			 $('#secondApplicantDobSpan').html('Please select date od birth..!');
			document.aggreementform.secondApplicantDob.focus();
			return false;
		}
	  else if(document.aggreementform.secondApplicantDob.value!="")
		{
				var today = new Date();
				var dd1 = today.getDate();
				var mm1 = today.getMonth()+1;
				var yy1 = today.getFullYear();
				
		    	var dob= document.aggreementform.secondApplicantDob.value;
		   	var dob2=dob.split("/");
		   
		    var yydiff=parseInt(yy1)-parseInt(dob2[2]);
		    if(yydiff<=18)
		    {
		    		 $('#secondApplicantDobSpan').html(' date of birth should be 18 or 18+ years..!');
		    		document.aggreementform.secondApplicantDob.value="";
		    		document.aggreementform.secondApplicantDob.focus();
		    		return false;
		    }
		}
	
	  
	  

		//validation for mobile number 1
		if(document.aggreementform.secondApplicantmobileNumber1.value=="")
		{
			 $('#secondApplicantmobileNumber1Span').html('Please, enter primary mobile number..!');
			document.aggreementform.secondApplicantmobileNumber1.value="";
			document.aggreementform.secondApplicantmobileNumber1.focus();
			return false;
		}
		else if(!document.aggreementform.secondApplicantmobileNumber1.value.match(/^[0-9]{10}$/))
		{
			 $('#secondApplicantmobileNumber1Span').html(' enter valid primary mobile number..!');
			document.aggreementform.secondApplicantmobileNumber1.value="";
			document.aggreementform.secondApplicantmobileNumber1.focus();
			return false;	
		}
		
		//validation for mobile number 2
		if(document.aggreementform.secondApplicantmobileNumber2.value.length!=0)
		{
			if(!document.aggreementform.secondApplicantmobileNumber2.value.match(/^[0-9]{10}$/))
			{
				 $('#secondApplicantmobileNumber2Span').html(' enter valid secondary mobile number..!');
				document.aggreementform.secondApplicantmobileNumber2.value="";
				document.aggreementform.secondApplicantmobileNumber2.focus();
				return false;	
			}
		}
	  
	  

		//validation for email
		if(document.aggreementform.secondApplicantEmail.value=="")
		{
			 $('#secondApplicantEmailSpan').html('Email Id should not be blank..!');
			document.aggreementform.secondApplicantEmail.value="";
			document.aggreementform.secondApplicantEmail.focus();
			return false;
		}
		else if(!document.aggreementform.secondApplicantEmail.value.match(/^(([\-\w]+)\.?)+@(([\-\w]+)\.?)+\.[a-z]{2,4}$/))
		{
			 $('#secondApplicantEmailSpan').html(' enter valid email id..!');
			document.aggreementform.secondApplicantEmail.value="";
			document.aggreementform.secondApplicantEmail.focus();
			return false;
		}
		
		
		
		//validation for pan card
		
		if(document.aggreementform.secondApplicantPancardno.value=="")
		{
			 $('#secondApplicantPanCardNoSpan').html('Please, enter PAN number..!');
			document.aggreementform.secondApplicantPancardno.value="";
			document.aggreementform.secondApplicantPancardno.focus();
			return false;
		}
		else if(!document.aggreementform.secondApplicantPancardno.value.match(/^[A-Za-z]{5}[0-9]{4}[A-z]{1}$/))
		{
			 $('#secondApplicantPanCardNoSpan').html(' PAN number must start with 5 alphabets follwed by 4 digit number and 1 alphabet..!');
			document.aggreementform.secondApplicantPancardno.value="";
			document.aggreementform.secondApplicantPancardno.focus();
			return false;	
		}
		
		//validation for aadhar number
			
		if(document.aggreementform.secondApplicantAadharno.value=="")
		{
			 $('#secondApplicantAadharnoSpan').html('Please, enter Aadhar number..!');
			document.aggreementform.secondApplicantAadharno.value="";
			document.aggreementform.secondApplicantAadharno.focus();
			return false;
		}
		else if(!document.aggreementform.secondApplicantAadharno.value.match(/^\d{4}\d{4}\d{4}$/))
		{
			 $('#secondApplicantAadharnoSpan').html('Aadhar card number should be 12 digit number only..!');
			document.aggreementform.secondApplicantAadharno.value="";
			document.aggreementform.secondApplicantAadharno.focus();
			return false;	
		}
		
	
		//validation for last name
		if(document.aggreementform.secondApplicantMotherTongue.value=="")
		{
			 $('#secondApplicantMotherTongueSpan').html('Mother Tonque should not be empty..!');
			document.aggreementform.secondApplicantMotherTongue.focus();
			return false;
		}
		else if(document.aggreementform.secondApplicantMotherTongue.value.match(/^[\s]+$/))
		{
			 $('#secondApplicantMotherTongueSpan').html('Mother Tonque should not be empty..!');
			document.aggreementform.secondApplicantMotherTongue.focus();
			return false;
		}
		else if(!document.aggreementform.secondApplicantMotherTongue.value.match(/^[A-Za-z]+[.]{0,1}$/))
		{
			 $('#secondApplicantMotherTongueSpan').html('Mother Tonque should contain alphabets only..!');
			document.aggreementform.secondApplicantMotherTongue.value="";
			document.aggreementform.secondApplicantMotherTongue.focus();
			return false;
		}		
		
			
	//validation for address
	if(document.aggreementform.secondApplicantPresentAddress.value=="")
	{
		 $('#secondApplicantPresentAddressSpan').html('Please, enter address..!');
		document.aggreementform.secondApplicantPresentAddress.focus();
		return false;
	}
	else if(document.aggreementform.secondApplicantPresentAddress.value.match(/^[\s]+$/))
	{
		 $('#secondApplicantPresentAddressSpan').html('Please, enter address name..!');
		document.aggreementform.secondApplicantPresentAddress.focus();
		return false;
	}
/* 	else if(!document.aggreementform.secondApplicantPresentAddress.value.match(/^[a-zA-Z0-9()-,.\s]+$/))
	{
		 $('#secondApplicantPresentAddressSpan').html('Please, use only alphabets and some special characters like (),.-  for person adrress..!');
		document.aggreementform.secondApplicantPresentAddress.value="";
		document.aggreementform.secondApplicantPresentAddress.focus();
		return false;
	} */
	/* 
	//validation for country name
	if(document.aggreementform.secondApplicantPresentcountryId.value=="Default")
	{
		 $('#secondApplicantPresentcountryIdSpan').html('Please, select country name..!');
		document.aggreementform.secondApplicantPresentcountryId.focus();
		return false;
	}
	
	//validation for state name
	if(document.aggreementform.secondApplicantPresentstateId.value=="Default")
	{
		 $('#secondApplicantPresentstateIdSpan').html('Please, select state name..!');
		document.aggreementform.secondApplicantPresentstateId.focus();
		return false;
	}
	
	//validation for city name
	if(document.aggreementform.secondApplicantPresentcityId.value=="Default")
	{
		 $('#secondApplicantPresentcityIdSpan').html('Please, select city name..!');
		document.aggreementform.secondApplicantPresentcityId.focus();
		return false;
	}
	
	//validation for location area name
	if(document.aggreementform.secondApplicantPresentlocationareaId.value=="Default")
	{
		 $('#secondApplicantPresentlocationareaIdSpan').html('Please, select location name..!');
		document.aggreementform.secondApplicantPresentlocationareaId.focus();
		return false;
	}
	 */
		
	//validation for address
	if(document.aggreementform.secondApplicantPermanentaddress.value=="")
	{
		 $('#secondApplicantPermanentaddressSpan').html('Please, enter address..!');
		document.aggreementform.secondApplicantPermanentaddress.focus();
		return false;
	}
	else if(document.aggreementform.secondApplicantPermanentaddress.value.match(/^[\s]+$/))
	{
		 $('#secondApplicantPermanentaddressSpan').html('Please, enter address name..!');
		document.aggreementform.secondApplicantPermanentaddress.focus();
		return false;
	}
/* 	else if(!document.aggreementform.secondApplicantPermanentaddress.value.match(/^[a-zA-Z0-9()-,.\s]+$/))
	{
		 $('#secondApplicantPermanentaddressSpan').html('Please, use only alphabets and some special characters like (),.-  for person adrress..!');
		document.aggreementform.secondApplicantPermanentaddress.value="";
		document.aggreementform.secondApplicantPermanentaddress.focus();
		return false;
	} */
	/* 
	//validation for country name
	if(document.aggreementform.secondApplicantPermanentcountryId.value=="Default")
	{
		 $('#secondApplicantPermanentcountryIdSpan').html('Please, select country name..!');
		document.aggreementform.secondApplicantPermanentcountryId.focus();
		return false;
	}
	
	//validation for state name
	if(document.aggreementform.secondApplicantPermanentstateId.value=="Default")
	{
		 $('#secondApplicantPermanentstateIdSpan').html('Please, select state name..!');
		document.aggreementform.secondApplicantPermanentstateId.focus();
		return false;
	}
	
	//validation for city name
	if(document.aggreementform.secondApplicantPermanentcityId.value=="Default")
	{
		 $('#secondApplicantPermanentcityIdSpan').html('Please, select city name..!');
		document.aggreementform.secondApplicantPermanentcityId.focus();
		return false;
	}
	
	//validation for location area name
	if(document.aggreementform.secondApplicantPermanentlocationareaId.value=="Default")
	{
		 $('#secondApplicantPermanentlocationareaIdSpan').html('Please, select location name..!');
		document.aggreementform.secondApplicantPermanentlocationareaId.focus();
		return false;
	}
	
		 */
	
	if(document.aggreementform.secondApplicantEducation.value=="")
	{
		 $('#secondApplicantEducationSpan').html('Please, enter education..!');
		document.aggreementform.secondApplicantEducation.focus();
		return false;
	}
	else if(document.aggreementform.secondApplicantEducation.value.match(/^[\s]+$/))
	{
		 $('#secondApplicantEducationSpan').html('Please, enter education..!');
		document.aggreementform.secondApplicantEducation.focus();
		return false;
	}

	
	//validation for occupation
	if(document.aggreementform.secondApplicantOccupation.value=="Default")
	{
		 $('#secondApplicantOccupationSpan').html('Please, select proper occupation..!');
		document.aggreementform.secondApplicantOccupation.focus();
		return false;
	}
	
	if(document.aggreementform.secondApplicantIncome.value=="Default")
	{
		 $('#secondApplicantIncomeSpan').html('Please, select Income..!');
		document.aggreementform.secondApplicantIncome.focus();
		return false;
	}
	
	}
	
		
}

function getfirstApplicantPresentpinCode()
{

	 $("#firstApplicantPresentPincode").empty();
	 var locationareaId = $('#firstApplicantPresentlocationareaId').val();
	 var cityId = $('#firstApplicantPresentcityId').val();
	 var stateId = $('#firstApplicantPresentstateId').val();
	 var countryId = $('#firstApplicantPresentcountryId').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getallAreaList',
		type : 'Post',
		data : { locationareaId : locationareaId, cityId : cityId, stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							
							for(var i=0;i<result.length;i++)
							{
								$('#firstApplicantPresentPincode').val(result[i].pinCode);
								
							 } 
						
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}

function getfirstApplicantPresentStateList()
{

	 $("#firstApplicantPresentstateId").empty();
	 $("#firstApplicantPresentcityId").empty();
	 $("#firstApplicantPresentlocationareaId").empty();
	 
	 var countryId = $('#firstApplicantPresentcountryId').val();

		$.ajax({

			url : '${pageContext.request.contextPath}/getStateList',
			type : 'Post',
			data : { countryId : countryId},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{

								var option = $('<option/>');
								option.attr('value',"Default").text("-Select Location Area-");
								$("#firstApplicantPresentlocationareaId").append(option);
								
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select City-");
								$("#firstApplicantPresentcityId").append(option);
								
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select State-");
								$("#firstApplicantPresentstateId").append(option);
								
								for(var i=0;i<result.length;i++)
								{
									var option = $('<option />');
								    option.attr('value',result[i].stateId).text(result[i].stateName);
								    $("#firstApplicantPresentstateId").append(option);
								 } 
							} 
							else
							{
								alert("failure111");
								//$("#ajax_div").hide();
							}

						}
			});
		
}//end of get State List


function getfirstApplicantPresentCityList()
{

	$("#firstApplicantPresentcityId").empty();
	$("#firstApplicantPresentlocationareaId").empty();
	var stateId = $('#firstApplicantPresentstateId').val();
	var countryId = $('#firstApplicantPresentcountryId').val();
	$.ajax({

		url : '${pageContext.request.contextPath}/getCityList',
		type : 'Post',
		data : { stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Location Area-");
							$("#firstApplicantPresentlocationareaId").append(option);
							
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select City-");
							$("#firstApplicantPresentcityId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].cityId).text(result[i].cityName);
							    $("#firstApplicantPresentcityId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
}//end of get City List

function getfirstApplicantPresentLocationAreaList()
{
	$("#firstApplicantPresentlocationareaId").empty();
	 var cityId = $('#firstApplicantPresentcityId').val();
	 var stateId = $('#firstApplicantPresentstateId').val();
	 var countryId = $('#firstApplicantPresentcountryId').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getLocationAreaList',
		type : 'Post',
		data : { cityId : cityId, stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Location Area-");
							$("#firstApplicantPresentlocationareaId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].locationareaId).text(result[i].locationareaName);
							    $("#firstApplicantPresentlocationareaId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get locationarea List

// First Applicant Permanent Address


function getfirstApplicantPermanentpinCode()
{

	 $("#firstApplicantPermanentPincode").empty();
	 var locationareaId = $('#firstApplicantPermanentlocationareaId').val();
	 var cityId = $('#firstApplicantPermanentcityId').val();
	 var stateId = $('#firstApplicantPermanentstateId').val();
	 var countryId = $('#firstApplicantPermanentcountryId').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getallAreaList',
		type : 'Post',
		data : { locationareaId : locationareaId, cityId : cityId, stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							
							for(var i=0;i<result.length;i++)
							{
								 $('#firstApplicantPermanentPincode').val(result[i].pinCode);
								
							 } 
						
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}

function getfirstApplicantPermanentStateList()
{

	 $("#firstApplicantPermanentstateId").empty();
	 $("#firstApplicantPermanentcityId").empty();
	 $("#firstApplicantPermanentlocationareaId").empty();
	 
	 var countryId = $('#firstApplicantPermanentcountryId').val();

		$.ajax({

			url : '${pageContext.request.contextPath}/getStateList',
			type : 'Post',
			data : { countryId : countryId},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{

								var option = $('<option/>');
								option.attr('value',"Default").text("-Select Location Area-");
								$("#firstApplicantPermanentlocationareaId").append(option);
								
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select City-");
								$("#firstApplicantPermanentcityId").append(option);
								
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select State-");
								$("#firstApplicantPermanentstateId").append(option);
								
								for(var i=0;i<result.length;i++)
								{
									var option = $('<option />');
								    option.attr('value',result[i].stateId).text(result[i].stateName);
								    $("#firstApplicantPermanentstateId").append(option);
								 } 
							} 
							else
							{
								alert("failure111");
								//$("#ajax_div").hide();
							}

						}
			});
		
}//end of get State List


function getfirstApplicantPermanentCityList()
{

	$("#firstApplicantPermanentcityId").empty();
	$("#firstApplicantPermanentlocationareaId").empty();
	var stateId = $('#firstApplicantPermanentstateId').val();
	var countryId = $('#firstApplicantPermanentcountryId').val();
	$.ajax({

		url : '${pageContext.request.contextPath}/getCityList',
		type : 'Post',
		data : { stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Location Area-");
							$("#firstApplicantPermanentlocationareaId").append(option);
							
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select City-");
							$("#firstApplicantPermanentcityId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].cityId).text(result[i].cityName);
							    $("#firstApplicantPermanentcityId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
}//end of get City List

function getfirstApplicantPermanentLocationAreaList()
{
	$("#firstApplicantPermanentlocationareaId").empty();
	 var cityId = $('#firstApplicantPermanentcityId').val();
	 var stateId = $('#firstApplicantPermanentstateId').val();
	 var countryId = $('#firstApplicantPermanentcountryId').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getLocationAreaList',
		type : 'Post',
		data : { cityId : cityId, stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Location Area-");
							$("#firstApplicantPermanentlocationareaId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].locationareaId).text(result[i].locationareaName);
							    $("#firstApplicantPermanentlocationareaId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get locationarea List


// second Aplicant

function getsecondApplicantPresentpinCode()
{

	 $("#secondApplicantPresentPincode").empty();
	 var locationareaId = $('#secondApplicantPresentlocationareaId').val();
	 var cityId = $('#secondApplicantPresentcityId').val();
	 var stateId = $('#secondApplicantPresentstateId').val();
	 var countryId = $('#secondApplicantPresentcountryId').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getallAreaList',
		type : 'Post',
		data : { locationareaId : locationareaId, cityId : cityId, stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							
							for(var i=0;i<result.length;i++)
							{
								$('#secondApplicantPresentPincode').val(result[i].pinCode);
								
							} 
						
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}

function getsecondApplicantPresentStateList()
{

	 $("#secondApplicantPresentstateId").empty();
	 $("#secondApplicantPresentcityId").empty();
	 $("#secondApplicantPresentlocationareaId").empty();
	 
	 var countryId = $('#secondApplicantPresentcountryId').val();

		$.ajax({

			url : '${pageContext.request.contextPath}/getStateList',
			type : 'Post',
			data : { countryId : countryId},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{

								var option = $('<option/>');
								option.attr('value',"Default").text("-Select Location Area-");
								$("#secondApplicantPresentlocationareaId").append(option);
								
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select City-");
								$("#secondApplicantPresentcityId").append(option);
								
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select State-");
								$("#secondApplicantPresentstateId").append(option);
								
								for(var i=0;i<result.length;i++)
								{
									var option = $('<option />');
								    option.attr('value',result[i].stateId).text(result[i].stateName);
								    $("#secondApplicantPresentstateId").append(option);
								 } 
							} 
							else
							{
								alert("failure111");
								//$("#ajax_div").hide();
							}

						}
			});
		
}//end of get State List


function getsecondApplicantPresentCityList()
{

	$("#secondApplicantPresentcityId").empty();
	$("#secondApplicantPresentlocationareaId").empty();
	var stateId = $('#secondApplicantPresentstateId').val();
	var countryId = $('#secondApplicantPresentcountryId').val();
	$.ajax({

		url : '${pageContext.request.contextPath}/getCityList',
		type : 'Post',
		data : { stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Location Area-");
							$("#secondApplicantPresentlocationareaId").append(option);
							
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select City-");
							$("#secondApplicantPresentcityId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].cityId).text(result[i].cityName);
							    $("#secondApplicantPresentcityId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
}//end of get City List

function getsecondApplicantPresentLocationAreaList()
{
	$("#secondApplicantPresentlocationareaId").empty();
	 var cityId = $('#secondApplicantPresentcityId').val();
	 var stateId = $('#secondApplicantPresentstateId').val();
	 var countryId = $('#secondApplicantPresentcountryId').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getLocationAreaList',
		type : 'Post',
		data : { cityId : cityId, stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Location Area-");
							$("#secondApplicantPresentlocationareaId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].locationareaId).text(result[i].locationareaName);
							    $("#secondApplicantPresentlocationareaId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get locationarea List

// second Applicant Permanent Address


function getsecondApplicantPermanentpinCode()
{

	 $("#secondApplicantPermanentPincode").empty();
	 var locationareaId = $('#secondApplicantPermanentlocationareaId').val();
	 var cityId = $('#secondApplicantPermanentcityId').val();
	 var stateId = $('#secondApplicantPermanentstateId').val();
	 var countryId = $('#secondApplicantPermanentcountryId').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getallAreaList',
		type : 'Post',
		//data : { locationareaId : locationareaId, cityId : cityId, stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							
							for(var i=0;i<result.length;i++)
							{
								if(result[i].countryId==countryId)
									{
									if(result[i].stateId==stateId)
										{
											if(result[i].cityId==cityId)
											{
												if(result[i].locationareaId==locationareaId)
												{
													 $('#secondApplicantPermanentPincode').val(result[i].pinCode);
												}
											}
										}
									}
								
							 } 
						
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}

function getsecondApplicantPermanentStateList()
{

	 $("#secondApplicantPermanentstateId").empty();
	 $("#secondApplicantPermanentcityId").empty();
	 $("#secondApplicantPermanentlocationareaId").empty();
	 
	 var countryId = $('#secondApplicantPermanentcountryId').val();

		$.ajax({

			url : '${pageContext.request.contextPath}/getStateList',
			type : 'Post',
			data : { countryId : countryId},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{

								var option = $('<option/>');
								option.attr('value',"Default").text("-Select Location Area-");
								$("#secondApplicantPermanentlocationareaId").append(option);
								
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select City-");
								$("#secondApplicantPermanentcityId").append(option);
								
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select State-");
								$("#secondApplicantPermanentstateId").append(option);
								
								for(var i=0;i<result.length;i++)
								{
									var option = $('<option />');
								    option.attr('value',result[i].stateId).text(result[i].stateName);
								    $("#secondApplicantPermanentstateId").append(option);
								 } 
							} 
							else
							{
								alert("failure111");
								//$("#ajax_div").hide();
							}

						}
			});
		
}//end of get State List


function getsecondApplicantPermanentCityList()
{

	$("#secondApplicantPermanentcityId").empty();
	$("#secondApplicantPermanentlocationareaId").empty();
	var stateId = $('#secondApplicantPermanentstateId').val();
	var countryId = $('#secondApplicantPermanentcountryId').val();
	$.ajax({

		url : '${pageContext.request.contextPath}/getCityList',
		type : 'Post',
		data : { stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Location Area-");
							$("#secondApplicantPermanentlocationareaId").append(option);
							
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select City-");
							$("#secondApplicantPermanentcityId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].cityId).text(result[i].cityName);
							    $("#secondApplicantPermanentcityId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
}//end of get City List

function getsecondApplicantPermanentLocationAreaList()
{
	$("#secondApplicantPermanentlocationareaId").empty();
	 var cityId = $('#secondApplicantPermanentcityId').val();
	 var stateId = $('#secondApplicantPermanentstateId').val();
	 var countryId = $('#secondApplicantPermanentcountryId').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getLocationAreaList',
		type : 'Post',
		data : { cityId : cityId, stateId : stateId, countryId : countryId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Location Area-");
							$("#secondApplicantPermanentlocationareaId").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].locationareaId).text(result[i].locationareaName);
							    $("#secondApplicantPermanentlocationareaId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get locationarea List



$(function () 
 {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    
    //Date picker
    $('#firstApplicantDob').datepicker({
      autoclose: true
    })
    
   $('#firstApplicantSpouseDob').datepicker({
      autoclose: true
    })
    
    $('#firstApplicantAnniversaryDate').datepicker({
      autoclose: true
    })
    
     $('#secondApplicantDob').datepicker({
      autoclose: true
    })
     $('#secondApplicantSpouseDob').datepicker({
      autoclose: true
    })
    
     $('#secondApplicantAnnivaversaryDate').datepicker({
      autoclose: true
    })
    
    
    $('#aggreementDate').datepicker({
        autoclose: true
      })
      
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Flat Market Price</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
 
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

  
  <!-- Left side column. contains the logo and sidebar -->
    <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Flat Market Price Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Report</a></li>
        <li class="active">Flat Market Price</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="flatmarketpriceform" action="${pageContext.request.contextPath}/PrintFlatMarketPrice" target="_blank" onSubmit="return validate()" method="post">
  <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              
       		
	<div class="box-body">
          <div class="row">
              
                 <div class="col-md-3">
                  <label>Project</label><label class="text-red">* </label>
                   <select class="form-control" name="projectId" id="projectId" onchange="getBuldingList(this.value)">
                  	 <option selected="selected" value="Default">-Select Project-</option>
                   	  <c:forEach var="projectList" items="${projectList}">
                    	<option value="${projectList.projectId}">${projectList.projectName}</option>
				     </c:forEach>
				   </select>
				   <span id="projectIdSpan" style="color:#FF0000"></span>
                  </div>
                  
                   <div class="col-md-3">
                     <label>Project Building</label><label class="text-red">* </label>
                     <select class="form-control" name="buildingId" id="buildingId" onchange="getAllWingList(this.value)">
				  		 <option selected="selected" value="Default">-Select Project Building-</option>
                      
                     </select>
                     <span id="buildingIdSpan" style="color:#FF0000"></span>
                  </div>
                  
                  <div class="col-xs-3">
			     	 <label>Wing </label> <label class="text-red">* </label>
             		 <select class="form-control" name="wingId" id="wingId" onchange="getWingList(this.value)">
				 	 	<option selected="selected" value="Default">-Select Wing Name-</option>
                   	 
                    </select>
                    <span id="wingIdSpan" style="color:#FF0000"></span>
                   </div>
                 <div class="col-xs-3">
			      		<label>.</label><br/>
			      		<button type="button" class="btn btn-success" onclick="return SearchMarkertPrice()">Search</button>&nbsp &nbsp &nbsp
			      	   <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>&nbsp &nbsp &nbsp
			      	    <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-print"></span>Print</button>
                 </div>  
           </div>
	</div>
		
<div  class="panel box box-danger"></div>
				
	<div class="box-body">
      <div class="row">
       <div class="col-md-1">
       </div>
      <div class="col-md-3">
        <label for="projectId1" id="projectId1">Project Name: ${projectId1} </label>
      </div>
       <div class="col-md-3">
        <label for="buildingId1" id="buildingId1">Building Name: ${buildingId1} </label>
      </div>
       <div class="col-md-3">
        <label for="wingId1" id="wingId1">Wing Name: ${wingId1} </label>
      </div>
     </div>
     </div> 
      <div class="box-body">
      <div class="row">
       <div class="col-md-12">
        
        	 <div class="box-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="FlatMarketPriceTable">
                  <thead>
                  <tr bgcolor="#4682B4">
                    <th>Type</th>
                    <th>C.Area</th>
                    <th>S.Area</th>
                    <th>B.Rate</th>
                    <th>F.Rise</th>
                    <th>Infra</th>
                    <th>Agg Amt</th>
                    <th>Stp Duty(6%)</th>
                    <th>Reg Charg (1%)</th>
                    <th>Lgl.Chrg</th>
                    <th>CGST </th>
                    <th>SGST </th>
                    <th>Total </th>
                    <th>Maint.</th>
                    <th>Net Amt</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody >
                 <c:forEach items="${flat}" var="flat" varStatus="loopStatus">
                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                        <td>${flat.flatType}</td>
                        <td>${flat.flatArea}</td>
                        <td>${flat.flatAreawithLoadingInFt}</td>
                        <td>${flat.flatCostwithotfloorise}</td>
                        <td>${flat.floorRise}</td>
                        <td>${flat.infrastructureCharges}</td>
                        <td>${flat.aggreementAmount}</td>
                        <td>${flat.stampDuty}</td>
                        <td>${flat.registeration}</td>
                        <td>${flat.handlingCharge}</td>
                        <td>${flat.cgst}</td>
                        <td>${flat.sgst}</td>
                        <td>${flat.totalCost}</td>
                        <td>${flat.maintenanceCharges}</td>
                        <td>${flat.netAmount}</td>
                        <td><button type="button" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit" onclick="return EditFlatMarketPrice(${loopStatus.index})"><i class="glyphicon glyphicon-edit"></i></button></td>
                      </tr>
				   </c:forEach> 
                 </tbody>
                </table>
              </div>
            </div>
	 </div>
	 
	 <%-- <div class="col-md-6">
        	 <div class="box-body">
        	  <label><font color="red">Payment Schedule </font></label>
              <div class="table-responsive">
                <table class="table table-bordered" id="paymentschedulerTable">
                  <thead>
                  <tr bgcolor="#4682B4">
	                    <th style="width:300px">Payment Description</th>
	                    <th style="width:50px">Percentage(100%)</th>
	                    <th style="width:150px">Slab Status</th>
                  </tr>
                  </thead>
                  <tbody >
                   <c:forEach items="${paymentschedulerList}" var="paymentschedulerList" varStatus="loopStatus">
                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                        <td>${paymentschedulerList.paymentDecription}</td>
                        <td>${paymentschedulerList.percentage}</td>
                        <td>${paymentschedulerList.slabStatus}</td>
                        </tr>
				   </c:forEach>
                 </tbody>
                </table>
              </div>
            </div>
	 </div> --%>
	</div>
	</div>
	
	<div class="box-body">
      <div class="row">

		    
		    
          <div class="col-xs-1">
		    <label for="flatArea">C.Area </label>
		    <input type="text" class="form-control" id="flatArea" name="flatArea" readonly>
          </div> 
       
          <div class="col-xs-1">
		    <label for="flatAreawithLoadingInFt">S.Area </label>
		    <input type="text" class="form-control" id="flatAreawithLoadingInFt" name="flatAreawithLoadingInFt" readonly>
          </div> 
          
          <div class="col-xs-1">
		    <label for="flatCostwithotfloorise">B Rate</label>
		    <input type="text" class="form-control" id="flatCostwithotfloorise" name="flatCostwithotfloorise"  onchange="CalculateTotalFlatCost(this.value)">
          </div> 
          
          <div class="col-xs-1">
		    <label for="floorRise">F.Rise</label>
		    <input type="text" class="form-control" id="floorRise" name="floorRise" onchange="CalculateTotalFlatCost(this.value)">
          </div> 
          
          <div class="col-xs-1">
		    <label for="infrastructureCharges">Infra </label>
		    <input type="text" class="form-control" id="infrastructureCharges" name="infrastructureCharges" readonly>
          </div> 
          
          <div class="col-xs-2">
		    <label for="aggreementAmount">Agg Amt </label>
		    <input type="text" class="form-control" id="aggreementAmount" name="aggreementAmount" readonly>
          </div> 
             
          <div class="col-xs-2">
		    <label for="stampDuty">Stp Duty(6%)</label>
		    <input type="text" class="form-control" id="stampDuty" name="stampDuty" readonly>
          </div> 
             
          <div class="col-xs-2">
		    <label for="registeration">Reg Charg(1%)</label>
		    <input type="text" class="form-control" id="registeration" name="registeration" readonly>
          </div> 
             
          <div class="col-xs-1">
		    <label for="handlingCharge">Lgl.Chrg </label>
		    <input type="text" class="form-control" id="handlingCharge" name="handlingCharge" readonly>
          </div> 
        
        </div>
	</div>
	
	<div class="box-body">
      <div class="row">      
        <input type="hidden" class="form-control" id="cgstPer" name="cgstPer"  value="${taxList[0].cgstPercentage}" onchange="CalculateTotalFlatCost(this.value)">
          <input type="hidden" class="form-control" id="sgstPer" name="sgstPer" value="${taxList[0].sgstPercentage}" onchange="CalculateTotalFlatCost(this.value)">
          
          <div class="col-xs-1">
		    <label for="cgst">CGST %</label>
		   <input type="text" class="form-control" id="cgstPercentage" name="cgstPercentage"  onchange="CalculateTotalFlatCost(this.value)">
          </div> 
                 
          <div class="col-xs-1">
		    <label for="cgst">CGST</label>
		    <input type="text" class="form-control" id="cgst" name="cgst" readonly>
          </div> 
               
          <div class="col-xs-1">
		    <label for="sgst">SCGT %</label>
		        <input type="text" class="form-control" id="sgstPercentage" name="sgstPercentage" onchange="CalculateTotalFlatCost(this.value)">
          </div> 
               
          <div class="col-xs-1">
		    <label for="sgst">SCGT</label>
		    <input type="text" class="form-control" id="sgst" name="sgst" readonly>
          </div> 
            
          <div class="col-xs-2">
		    <label for="totalCost">Total</label>
		    <input type="text" class="form-control" id="totalCost" name="totalCost" readonly>
          </div> 
               
          <div class="col-xs-2">
		    <label for="maintenanceCharges">Maint</label>
		    <input type="text" class="form-control" id="maintenanceCharges" name="maintenanceCharges" readonly>
          </div> 
             
          <div class="col-xs-2">
		    <label for="netAmount">Net Amt</label>
		    <input type="text" class="form-control" id="netAmount" name="netAmount" readonly>
          </div> 
                        
     </div>
    </div>
	
	
	</div> 
   </div>  
   </div>
   </div>
</section> 
</form>
    
  </div>
 
 <%@ include file="footer.jsp" %>
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
     
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script>
function CalculateTotalFlatCost()
{

	var cgstPercentage = Number($('#cgstPercentage').val());
	var sgstPercentage = Number($('#sgstPercentage').val());
	 var flatAreawithLoadingInFt = Number($('#flatAreawithLoadingInFt').val());
	 var flatCostwithotfloorise = Number($('#flatCostwithotfloorise').val());
	 var floorRise=Number($('#floorRise').val());
	 var infrastructureCharges = Number($('#infrastructureCharges').val());
	 
	 var handlingCharge=Number($('#handlingCharge').val());
	 var maintenanceCharges = Number($('#maintenanceCharges').val());
	 
	 
	 var aggreementAmount=(flatAreawithLoadingInFt*(flatCostwithotfloorise+floorRise))+infrastructureCharges;
	 var stampDuty=(aggreementAmount/100)*6;
	 var registeration;
	 if(aggreementAmount>3000000)
		 {
		 registeration=30000;
		 }
	 else
		 {
		 registeration=(aggreementAmount/100);
		 }
	 
	 
	 var cgst=(aggreementAmount/100)*cgstPercentage;
	 var sgst=(aggreementAmount/100)*sgstPercentage; 
	 
	 var totalCost=aggreementAmount+stampDuty+registeration+handlingCharge+cgst+sgst;
	 var netAmount=totalCost+maintenanceCharges;
	 
	document.flatmarketpriceform.aggreementAmount.value=aggreementAmount.toFixed(0);
	document.flatmarketpriceform.stampDuty.value=stampDuty.toFixed(0);
	document.flatmarketpriceform.registeration.value=registeration.toFixed(0);
	document.flatmarketpriceform.cgst.value=cgst.toFixed(0);
	document.flatmarketpriceform.sgst.value=sgst.toFixed(0);
	document.flatmarketpriceform.totalCost.value=totalCost.toFixed(0);
	document.flatmarketpriceform.netAmount.value=netAmount.toFixed(0);
}


function EditFlatMarketPrice(i)
{
	var cgstPer = Number($('#cgstPer').val());
	var sgstPer = Number($('#sgstPer').val());

	i=i+1;
    
	var oTable = document.getElementById('FlatMarketPriceTable');

	var oCells = oTable.rows.item(i).cells;
	
	var flatArea=Number(oCells.item(1).innerHTML);
	var flatAreawithLoadingInFt=Number(oCells.item(2).innerHTML);
	var flatCostwithotfloorise=Number(oCells.item(3).innerHTML);
	var floorRise=Number(oCells.item(4).innerHTML);
	var infrastructureCharges=Number(oCells.item(5).innerHTML);
	var aggreementAmount=Number(oCells.item(6).innerHTML);
	var stampDuty=Number(oCells.item(7).innerHTML);
	var registeration=Number(oCells.item(8).innerHTML);
	var handlingCharge=Number(oCells.item(9).innerHTML);
	var cgst=Number(oCells.item(10).innerHTML);
	var sgst=Number(oCells.item(11).innerHTML);
	var totalCost=Number(oCells.item(12).innerHTML);
	var maintenanceCharges=Number(oCells.item(13).innerHTML);
	var netAmount=Number(oCells.item(14).innerHTML);
	
	document.flatmarketpriceform.cgstPercentage.value=cgstPer;
	document.flatmarketpriceform.sgstPercentage.value=sgstPer;
	
	
	document.flatmarketpriceform.flatArea.value=flatArea;
	document.flatmarketpriceform.flatAreawithLoadingInFt.value=flatAreawithLoadingInFt;
	document.flatmarketpriceform.flatCostwithotfloorise.value=flatCostwithotfloorise;
	document.flatmarketpriceform.floorRise.value=floorRise;
	document.flatmarketpriceform.infrastructureCharges.value=infrastructureCharges;
	document.flatmarketpriceform.aggreementAmount.value=aggreementAmount;
	document.flatmarketpriceform.stampDuty.value=stampDuty;
	document.flatmarketpriceform.registeration.value=registeration;
	document.flatmarketpriceform.handlingCharge.value=handlingCharge;
	document.flatmarketpriceform.cgst.value=cgst;
	document.flatmarketpriceform.sgst.value=sgst;
	document.flatmarketpriceform.totalCost.value=totalCost;
	document.flatmarketpriceform.maintenanceCharges.value=maintenanceCharges;
	document.flatmarketpriceform.netAmount.value=netAmount;
	
}

function getBuldingList()
{
	 $("#buildingId").empty();
	 var projectId = $('#projectId').val();

	 $.ajax({

		url : '${pageContext.request.contextPath}/getBuildingList',
		type : 'Post',
		data : { projectId : projectId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Building Name-");
							$("#buildingId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].buildingId).text(result[i].buildingName);
							    $("#buildingId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
	
	 
}//end of get Building List


function getAllWingList()
{

	  $("#wingId").empty();
	  var buildingId = $('#buildingId').val();
	  var projectId = $('#projectId').val();
	   
	  $.ajax({

		 url : '${pageContext.request.contextPath}/getprojectwingList',
		type : 'Post',
		data : { buildingId : buildingId, projectId : projectId },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Wing Name-");
							$("#wingId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].wingId).text(result[i].wingName);
							    $("#wingId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	 
}

function validate()
{
	/*if(document.flatmarketpriceform.projectId.value=="Default")
	{
		$('#projectIdSpan').html('Please Select Project');
		document.flatmarketpriceform.projectId.focus();
		return false;
	}
	//for Building name
	
	if(document.flatmarketpriceform.buildingId.value=="Default")
	{
		$('#buildingIdSpan').html('Please Select Project Building');
		document.flatmarketpriceform.buildingId.focus();
		return false;
	}
	if(document.flatmarketpriceform.wingId.value=="Default")
	{
		$('#wingIdSpan').html('Please Select Project wing');
		document.flatmarketpriceform.buildingId.focus();
		return false;
	}*/

}
function SearchMarkertPrice()
{
	//for project name
	if(document.flatmarketpriceform.projectId.value=="Default")
	{
		$('#projectIdSpan').html('Please Select Project');
		document.flatmarketpriceform.projectId.focus();
		return false;
	}
	//for Building name
	
	if(document.flatmarketpriceform.buildingId.value=="Default")
	{
		$('#buildingIdSpan').html('Please Select Project Building');
		document.flatmarketpriceform.buildingId.focus();
		return false;
	}
	if(document.flatmarketpriceform.wingId.value=="Default")
	{
		$('#wingIdSpan').html('Please Select Project wing');
		document.flatmarketpriceform.buildingId.focus();
		return false;
	}
	 $("#FlatMarketPriceTable tr").detach();
	 var projectId = $('#projectId').val();
	 var buildingId = $('#buildingId').val();
	 var wingId = $('#wingId').val();
	
	 var projectSelect = document.getElementById("projectId");
	 var projectName = projectSelect.options[projectSelect.selectedIndex].text;
	 
	 var projectBuildingSelect = document.getElementById("buildingId");
	 var buildingName = projectBuildingSelect.options[projectBuildingSelect.selectedIndex].text;
	 
	 var projectWingSelect = document.getElementById("wingId");
	 var wingName = projectWingSelect.options[projectWingSelect.selectedIndex].text;
	 
	 document.getElementById('projectId1').innerHTML = "Project Name: "+projectName;
	 document.getElementById('buildingId1').innerHTML = "Building Name: "+buildingName;
	 document.getElementById('wingId1').innerHTML = "Wing Name: "+wingName;
	 $.ajax({

		url : '${pageContext.request.contextPath}/SearchMarkertPrice',
		type : 'Post',
		data : { wingId : wingId, buildingId : buildingId , projectId : projectId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
							$('#FlatMarketPriceTable').append('<tr style="background-color: #4682B4;"><th>Type</th><th>C.Area</th><th>S.Area</th><th>B.Rate</th><th>F.Rise</th><th>Infra</th> <th>Agg Amt</th><th>Stp Duty(6%)</th><th>Reg Charg (1%)</th><th>Lgl.Chrg</th> <th>CGST </th><th>SGST </th> <th>Total </th> <th>Maint.</th><th>Net Amt</th><th>Action</th>');
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									$('#FlatMarketPriceTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].flatType+'</td><td>'+result[i].flatArea+'</td><td>'+result[i].flatAreawithLoadingInFt+'</td><td>'+result[i].flatCostwithotfloorise+'</td><td>'+result[i].floorRise+'</td><td>'+result[i].infrastructureCharges+'</td><td>'+result[i].aggreementAmount+'</td><td>'+result[i].stampDuty+'</td><td>'+result[i].registeration+'</td><td>'+result[i].handlingCharge+'</td><td>'+result[i].cgst+'</td><td>'+result[i].sgst+'</td><td>'+result[i].totalCost+'</td><td>'+result[i].maintenanceCharges+'</td><td>'+result[i].netAmount+'</td><td><button type="button" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit" onclick="return EditFlatMarketPrice('+i+')"><i class="glyphicon glyphicon-edit"></i></button></td>');
								}
								else
								{
									$('#FlatMarketPriceTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].flatType+'</td><td>'+result[i].flatArea+'</td><td>'+result[i].flatAreawithLoadingInFt+'</td><td>'+result[i].flatCostwithotfloorise+'</td><td>'+result[i].floorRise+'</td><td>'+result[i].infrastructureCharges+'</td><td>'+result[i].aggreementAmount+'</td><td>'+result[i].stampDuty+'</td><td>'+result[i].registeration+'</td><td>'+result[i].handlingCharge+'</td><td>'+result[i].cgst+'</td><td>'+result[i].sgst+'</td><td>'+result[i].totalCost+'</td><td>'+result[i].maintenanceCharges+'</td><td>'+result[i].netAmount+'</td><td><button type="button" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit" onclick="return EditFlatMarketPrice('+i+')"><i class="glyphicon glyphicon-edit"></i></button></td>');
								}
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
		
		document.flatmarketpriceform.cgstPercentage.value="";
		document.flatmarketpriceform.sgstPercentage.value="";
		
		
		document.flatmarketpriceform.flatArea.value="";
		document.flatmarketpriceform.flatAreawithLoadingInFt.value="";
		document.flatmarketpriceform.flatCostwithotfloorise.value="";
		document.flatmarketpriceform.floorRise.value="";
		document.flatmarketpriceform.infrastructureCharges.value="";
		document.flatmarketpriceform.aggreementAmount.value="";
		document.flatmarketpriceform.stampDuty.value="";
		document.flatmarketpriceform.registeration.value="";
		document.flatmarketpriceform.handlingCharge.value="";
		document.flatmarketpriceform.cgst.value="";
		document.flatmarketpriceform.sgst.value="";
		document.flatmarketpriceform.totalCost.value="";
		document.flatmarketpriceform.maintenanceCharges.value="";
		document.flatmarketpriceform.netAmount.value="";
}
/* 
$(function () {
    $('#FlatMarketPriceTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
  
  $(function () {
	    $('#paymentschedulerTable').DataTable()
	    $('#example2').DataTable({
	      'paging'      : true,
	      'lengthChange': false,
	      'searching'   : false,
	      'ordering'    : true,
	      'info'        : true,
	      'autoWidth'   : false
	    })
	  }) 
   */
</script>
</body>
</html>
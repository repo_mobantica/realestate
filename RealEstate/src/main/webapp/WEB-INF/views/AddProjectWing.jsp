<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Add Project Wing</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
    <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("user") == null || session.getAttribute("userMenuAccessList") == null || session.getAttribute("profile_img") == null) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
<div class="wrapper">

   <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
  <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Wing Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Add Wings</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="projectwingform" action="${pageContext.request.contextPath}/AddProjectWing" method="Post" onSubmit="return validate()">

    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
                <span id="statusSpan" style="color:	#008000"></span>
			  
			 <div class="box-body">
              <div class="row">
                  <div class="col-xs-2">
			      <label >Building Id </label>
                  <input type="text" class="form-control" id="wingId" name="wingId" Value="${wingCode}" readonly>
                
			     </div> 
				</div>
            </div>
			
		<div class="box-body">
          <div class="row">
            <div class="col-md-3">
                  <label>Project</label><label class="text-red">* </label>
                  
                  <select class="form-control" name="projectId" id="projectId" onchange="getBuldingList(this.value)">
                  	 <option selected="" value="Default">-Select Project-</option>
                   	  <c:forEach var="projectList" items="${projectList}">
                    	<option value="${projectList.projectId}">${projectList.projectName}</option>
				     </c:forEach>
				  </select>
				  <span id="projectIdSpan" style="color:#FF0000"></span>
                  </div>
                  
               <div class="col-md-3">
                  <label>Project Building</label><label class="text-red">* </label>
                   <select class="form-control" name="buildingId" id="buildingId" >
				   <option selected="" value="Default">-Select Project Building-</option>
                  </select>
                  <span id="buildingIdSpan" style="color:#FF0000"></span>
                  </div>
			
                 <div class="col-xs-3">
				    <label for="wingName">Wing Name </label> <label class="text-red">* </label>
	                <input type="text" class="form-control" id="wingName" placeholder="Wing Name " name="wingName"  style="text-transform: capitalize;" onchange="getuniquewingName(this.value)">
			        <span id="wingNameSpan" style="color:#FF0000"></span>
			    </div> 
			  
			  </div>
             </div>
        
			<div class="box-body">
              <div class="row">
			     
                <div class="col-xs-3">
				  <label for="infrastructureCharges">Infrastructure Charges</label> <label class="text-red">* </label>
	              <input type="text" class="form-control" id="infrastructureCharges" placeholder="Infrastructure Charges" name="infrastructureCharges" >      
				  <span id="infrastructureChargesSpan" style="color:#FF0000"></span>     
                </div>
			     
                <div class="col-xs-3">
				  <label for="maintenanceCharges">Maintenance Charges</label> <label class="text-red">* </label>
	              <input type="text" class="form-control" id="maintenanceCharges" placeholder="Maintenance Charges" name="maintenanceCharges" >      
				  <span id="maintenanceChargesSpan" style="color:#FF0000"></span>     
                </div>
			     
                <div class="col-xs-3">
				  <label for="handlingCharges">Handling Charges</label> <label class="text-red">* </label>
	              <input type="text" class="form-control" id="handlingCharges" placeholder="Handling Charges" name="handlingCharges" >      
				  <span id="handlingChargesSpan" style="color:#FF0000"></span>     
                </div>
			     
			  </div>
            </div>
        
			<div class="box-body">
              <div class="row">
                <div class="col-xs-2">
				    <label for="noofBasement">Total Number of Basement </label> <label class="text-red">* </label>
	                <input type="text" class="form-control" id="noofBasement" placeholder="No. Of. Basement " onchange="getTotal(this.value)"  name="noofBasement" >
			        <span id="noofBasementSpan" style="color:#FF0000"></span>
			     </div>
			     
			      <div class="col-xs-2">
				    <label for="noofStilts">Total Number of Stilts </label> <label class="text-red">* </label>
	                <input type="text" class="form-control" id="noofStilts" placeholder="No. Of. Stilts " onchange="getTotal(this.value)"  name="noofStilts" >
			        <span id="noofStiltsSpan" style="color:#FF0000"></span>
			     </div>  
			      <div class="col-xs-2">
				    <label for="noofPodiums">Total Number of Podium </label> <label class="text-red">* </label>
	                <input type="text" class="form-control" id="noofPodiums" placeholder="No. Of. Podium " onchange="getTotal(this.value)"  name="noofPodiums" >
			        <span id="noofPodiumsSpan" style="color:#FF0000"></span>
			     </div>  
            
                   <div class="col-xs-2">
				    <label for="noofFloors">Total Number of Floors</label> <label class="text-red">* </label>
	                <input type="text" class="form-control" id="noofFloors" placeholder=" " name="noofFloors"onchange="getTotal(this.value)">
			        <span id="noofFloorsSpan" style="color:#FF0000"></span>
			     </div>
           
			     
                <div class="col-xs-2">
				    <label for="total">Total</label> 
	                <input type="text" class="form-control" id="total" placeholder=" " name="total" readonly>
			        <span id="totalSpan" style="color:#FF0000"></span>
			     </div>
			     <div class="col-xs-2">
			     <label> Status </label> <label class="text-red">* </label>
                  <select class="form-control" name="wingStatus" id="wingStatus">
				  <option value="Default">-Select Wing Status-</option>
					   <option>Not-Started</option>
					   <option>In-Process </option>
	                   <option>Completed </option>
                  </select>
                  <span id="wingStatusSpan" style="color:#FF0000"></span>
			     </div>  
              </div>
            </div>
            <div class="box-body">
              <div class="row">
             
              </div>
            </div>
			</br>
			 
              <!-- /.form-group -->
              <input type="hidden" id="Status" name="Status" value="${wingStatus}">	
			  <input type="hidden" id="creationDate" name="creationDate" value="">
			  <input type="hidden" id="updateDate" name="updateDate" value="">
			  <input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>">
			  
            </div>
		
          </div>
		   <div class="box-body">
              <div class="row">
              </br>
                <div class="col-xs-5">
                <div class="col-xs-2">
                <a href="ProjectWingMaster"><button type="button" class="btn btn-block btn-primary" style="width:90px">Back</button></a>
			     </div>
			     </div>
				<div class="col-xs-4">
                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
     		    </div>
				<div class="col-xs-2">
			   <button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
			     </div> 
			     
			   
              </div>
			  </div>
         </div>
        <!-- /.box-body -->
        
      </div>
      <!-- /.box -->

     
    </section>
	</form>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 

  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script>
function clearAll()
{
	$('#projectIdSpan').html('');
	$('#buildingIdSpan').html('');
	$('#wingNameSpan').html('');
	$('#infrastructureChargesSpan').html('');
	$('#maintenanceChargesSpan').html('');
	$('#maintenanceChargesSpan').html('');
	$('#noofBasementSpan').html('');
	$('#noofStiltsSpan').html('');
	$('#noofPodiumsSpan').html('');
	$('#noofFloorsSpan').html('');
	$('#wingStatusSpan').html('');
}
function getTotal()
{
	 var noofBasement = parseInt($('#noofBasement').val());
	 var noofStilts = parseInt($('#noofStilts').val());
	 var noofPodiums = parseInt($('#noofPodiums').val());
	 var noofFloors = parseInt($('#noofFloors').val());
	 var total=noofPodiums + noofFloors + noofStilts + noofBasement;
	
	 document.projectwingform.total.value=total;
}
function validate()
{ 
	clearAll();
		//validation for project name
		if(document.projectwingform.projectId.value=="Default")
		{
			 $('#projectIdSpan').html('Please, select project name..!');
			document.projectwingform.projectId.focus();
			return false;
		}
		//validation for project name
		if(document.projectwingform.buildingId.value=="Default")
		{
			 $('#buildingIdSpan').html('Please, select project building name..!');
			document.projectwingform.buildingId.focus();
			return false;
		}
		//validation for project wing name
		if(document.projectwingform.wingName.value=="")
		{
			 $('#wingNameSpan').html('Project wing name should not be empty..!');
			document.projectwingform.wingName.focus();
			return false;
		}
		
		
		// for Infrastructure Charges 
		if(document.projectwingform.infrastructureCharges.value=="")
		{
			 $('#infrastructureChargesSpan').html('Please, enter infrastructure Charges..!');
			document.projectwingform.infrastructureCharges.focus();
			return false;
		}
		else if(!document.projectwingform.infrastructureCharges.value.match(/^[0-9]+$/))
		{
			 $('#infrastructureChargesSpan').html('Please, enter valid infrastructure Charges..!');
			document.projectwingform.infrastructureCharges.focus();
			return false;			
		}
		
		
		// for Maintenance Charges 
		if(document.projectwingform.maintenanceCharges.value=="")
		{
			 $('#maintenanceChargesSpan').html('Please, enter maintenance Charges..!');
			document.projectwingform.maintenanceCharges.focus();
			return false;
		}
		else if(!document.projectwingform.maintenanceCharges.value.match(/^[0-9]+$/))
		{
			 $('#maintenanceChargesSpan').html('Please, enter valid maintenance Charges..!');
			document.projectwingform.maintenanceCharges.focus();
			return false;			
		}
		
		
		// for Handling  Charges 
		if(document.projectwingform.handlingCharges.value=="")
		{
			 $('#handlingChargesSpan').html('Please, enter handling Charges..!');
			document.projectwingform.handlingCharges.focus();
			return false;
		}
		else if(!document.projectwingform.handlingCharges.value.match(/^[0-9]+$/))
		{
			 $('#handlingChargesSpan').html('Please, enter valid handling Charges..!');
			document.projectwingform.handlingCharges.focus();
			return false;			
		}
		
	
		// for basement 
		if(document.projectwingform.noofBasement.value=="")
		{
			 $('#noofBasementSpan').html('Please, enter number of Basement..!');
			document.projectwingform.noofBasement.focus();
			return false;
		}
		else if(!document.projectwingform.noofBasement.value.match(/^[0-9]{1,5}$/))
		{
			 $('#noofBasementSpan').html('Please, enter valid number of basement..!');
			document.projectwingform.noofBasement.focus();
			return false;			
		}
		
		//for stilts
		if(document.projectwingform.noofStilts.value=="")
		{
			 $('#noofStiltsSpan').html('Please, enter number of Stilts..!');
			document.projectwingform.noofStilts.focus();
			return false;
		}
		else if(!document.projectwingform.noofStilts.value.match(/^[0-9]{1,5}$/))
		{
			 $('#noofStiltsSpan').html('Please, enter valid number of stilts..!');
			document.projectwingform.noofStilts.focus();
			return false;			
		}
		// for podium
		if(document.projectwingform.noofPodiums.value=="")
		{
			 $('#noofPodiumsSpan').html('Please, enter number of podium..!');
			document.projectwingform.noofPodiums.focus();
			return false;
		}
		else if(!document.projectwingform.noofPodiums.value.match(/^[0-9]{1,5}$/))
		{
			 $('#noofPodiumsSpan').html('Please, enter valid number of podium..!');
			document.projectwingform.noofPodiums.focus();
			return false;			
		}
		
		
		//validation for number of floors
		if(document.projectwingform.noofFloors.value=="")
		{
			 $('#noofFloorsSpan').html('Please, enter number of floors..!');
			document.projectwingform.noofFloors.focus();
			return false;
		}
		else if(!document.projectwingform.noofFloors.value.match(/^[0-9]{1,5}$/))
		{
			 $('#noofFloorsSpan').html('Please, enter valid number of floors..!');
			document.projectwingform.noofFloors.focus();
			return false;			
		}
		
		//validation for build status
		if(document.projectwingform.wingStatus.value=="Default")
		{
			 $('#wingStatusSpan').html('Please, select wing status..!');
			document.projectwingform.wingStatus.focus();
			return false;
		}
		
}
function init()
{
	var date =  new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	document.getElementById("creationDate").value = day + "/" + month + "/" + year;
	document.getElementById("updateDate").value = day + "/" + month + "/" + year;
	
	 
	 if(document.projectwingform.wingStatus.value == "Fail")
	 {
	  	//alert("Sorry, record is present already..!");
	 }
	 else if(document.projectwingform.Status.value == "Success")
	 {
		 $('#statusSpan').html('Record added successfully..!');
	 }
	 
   	 document.projectwingform.projectId.focus();
}


function getBuldingList()
{
	 $("#buildingId").empty();
	 var projectId = $('#projectId').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getBuildingList',
		type : 'Post',
		data : { projectId : projectId},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Building Name-");
							$("#buildingId").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].buildingId).text(result[i].buildingName);
							    $("#buildingId").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get Building List

function getuniquewingName()
{
	 var wingName = $('#wingName').val();
	 var buildingId = $('#buildingId').val();
	 var projectId = $('#projectId').val();
			$.ajax({

				url : '${pageContext.request.contextPath}/getuniqueWingList',
			type : 'Post',
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{
								 
								for(var i=0;i<result.length;i++)
								{
									
									if(result[i].projectId==projectId)
										{
										if(result[i].buildingId==buildingId)
										{
											if(result[i].wingName==wingName)
											{
												 //document.stateform.stateName.value="";
												  document.projectwingform.wingName.focus();
												  $('#wingNameSpan').html('This wing Name is already exist..!');
												  $('#wingName').val("");
											}
										}
										}
									
									
								 } 
							} 
							else
							{
								alert("failure111");
								//$("#ajax_div").hide();
							}

						}
			});	
}
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>

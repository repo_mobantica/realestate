package com.realestate.response;

public class HomeGraphDetails {
	
	private String projectName;
	private int bookingSaleInSunday;
	private int bookingSaleInMonday;
	private int bookingSaleInTuesday;
	private int bookingSaleInWednesday;
	private int bookingSaleInThursday;
	private int bookingSaleInFriday;
	private int bookingSaleInSaturday;
    
	
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public int getBookingSaleInSunday() {
		return bookingSaleInSunday;
	}
	public void setBookingSaleInSunday(int bookingSaleInSunday) {
		this.bookingSaleInSunday = bookingSaleInSunday;
	}
	public int getBookingSaleInMonday() {
		return bookingSaleInMonday;
	}
	public void setBookingSaleInMonday(int bookingSaleInMonday) {
		this.bookingSaleInMonday = bookingSaleInMonday;
	}
	public int getBookingSaleInTuesday() {
		return bookingSaleInTuesday;
	}
	public void setBookingSaleInTuesday(int bookingSaleInTuesday) {
		this.bookingSaleInTuesday = bookingSaleInTuesday;
	}
	public int getBookingSaleInWednesday() {
		return bookingSaleInWednesday;
	}
	public void setBookingSaleInWednesday(int bookingSaleInWednesday) {
		this.bookingSaleInWednesday = bookingSaleInWednesday;
	}
	public int getBookingSaleInThursday() {
		return bookingSaleInThursday;
	}
	public void setBookingSaleInThursday(int bookingSaleInThursday) {
		this.bookingSaleInThursday = bookingSaleInThursday;
	}
	public int getBookingSaleInFriday() {
		return bookingSaleInFriday;
	}
	public void setBookingSaleInFriday(int bookingSaleInFriday) {
		this.bookingSaleInFriday = bookingSaleInFriday;
	}
	public int getBookingSaleInSaturday() {
		return bookingSaleInSaturday;
	}
	public void setBookingSaleInSaturday(int bookingSaleInSaturday) {
		this.bookingSaleInSaturday = bookingSaleInSaturday;
	}
	 
}

package com.realestate.response;

public class ProjectGraghDetails {

	private int totalFlat;
	private int totalFlatBooking;
	private int totalFlatAgreement;
	private int totalFlatRemaining;
	
	
	public int getTotalFlat() {
		return totalFlat;
	}
	public void setTotalFlat(int totalFlat) {
		this.totalFlat = totalFlat;
	}
	public int getTotalFlatBooking() {
		return totalFlatBooking;
	}
	public void setTotalFlatBooking(int totalFlatBooking) {
		this.totalFlatBooking = totalFlatBooking;
	}
	public int getTotalFlatAgreement() {
		return totalFlatAgreement;
	}
	public void setTotalFlatAgreement(int totalFlatAgreement) {
		this.totalFlatAgreement = totalFlatAgreement;
	}
	public int getTotalFlatRemaining() {
		return totalFlatRemaining;
	}
	public void setTotalFlatRemaining(int totalFlatRemaining) {
		this.totalFlatRemaining = totalFlatRemaining;
	}
	
	
}

package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.Booking;
import com.realestate.bean.CustomerReceiptForm;
import com.realestate.bean.Enquiry;

public class DailyReportResponse {


	private String report;
	private String startDate;
	private String endDate;
	private double totalPayment;
	
	List<Booking> bookingList=new ArrayList<Booking>(); 
	List<Enquiry> enquiryList=new ArrayList<Enquiry>();
	List<CustomerReceiptForm> paymentList=new ArrayList<CustomerReceiptForm>();
	
	public List<Booking> getBookingList() {
		return bookingList;
	}
	public void setBookingList(List<Booking> bookingList) {
		this.bookingList = bookingList;
	}
	public List<Enquiry> getEnquiryList() {
		return enquiryList;
	}
	public void setEnquiryList(List<Enquiry> enquiryList) {
		this.enquiryList = enquiryList;
	}
	public String getReport() {
		return report;
	}
	public void setReport(String report) {
		this.report = report;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public List<CustomerReceiptForm> getPaymentList() {
		return paymentList;
	}
	public void setPaymentList(List<CustomerReceiptForm> paymentList) {
		this.paymentList = paymentList;
	}
	public double getTotalPayment() {
		return totalPayment;
	}
	public void setTotalPayment(double totalPayment) {
		this.totalPayment = totalPayment;
	} 
	
}

package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.CustomerReceiptForm;

public interface CustomerReceiptFormRepository  extends MongoRepository<CustomerReceiptForm, String>{

}

package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.Stock;

public interface StockRepository extends MongoRepository<Stock, String>{

	
}

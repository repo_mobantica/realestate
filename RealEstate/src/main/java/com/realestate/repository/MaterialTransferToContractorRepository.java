package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.MaterialTransferToContractor;

public interface MaterialTransferToContractorRepository extends MongoRepository<MaterialTransferToContractor, String>{

}

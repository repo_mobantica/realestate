package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.realestate.bean.UserModel;

public interface UserModelRepository extends MongoRepository<UserModel, String> 
{
  
}

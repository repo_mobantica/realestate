package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.CustomerPaymentDetails;

public interface CustomerPaymentDetailsRepository  extends MongoRepository<CustomerPaymentDetails, String>{

}

package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.MaterialPurchaseRequisition;

public interface MaterialPurchaseRequisitionRepository extends MongoRepository<MaterialPurchaseRequisition, String>{

}

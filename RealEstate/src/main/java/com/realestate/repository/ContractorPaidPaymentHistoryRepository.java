package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.ContractorPaidPaymentHistory;

public interface ContractorPaidPaymentHistoryRepository  extends MongoRepository<ContractorPaidPaymentHistory, String>{

}

package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.ContractorEmployees;

public interface ContractorEmployeesRepository extends MongoRepository<ContractorEmployees, String>
{

}

package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.ProjectDetails;

public interface ProjectDetailsRepository  extends MongoRepository<ProjectDetails, String>{

}

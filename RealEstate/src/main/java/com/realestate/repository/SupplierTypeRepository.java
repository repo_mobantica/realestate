package com.realestate.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import com.realestate.bean.SupplierType;

public interface SupplierTypeRepository extends MongoRepository<SupplierType, String>{
	 public List<SupplierType> findAll(Sort sort);
}

package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.realestate.bean.ContractorWorkOrder;

public interface ContractorWorkOrderRepository extends MongoRepository<ContractorWorkOrder, String>{

}

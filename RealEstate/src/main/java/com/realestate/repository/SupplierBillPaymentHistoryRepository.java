package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.SupplierBillPaymentHistory;

public interface SupplierBillPaymentHistoryRepository extends MongoRepository<SupplierBillPaymentHistory, String>{

}

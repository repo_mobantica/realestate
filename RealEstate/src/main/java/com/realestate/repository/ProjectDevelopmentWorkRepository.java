package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.ProjectDevelopmentWork;

public interface ProjectDevelopmentWorkRepository extends MongoRepository<ProjectDevelopmentWork, String>{

}

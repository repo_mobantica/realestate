package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.realestate.bean.Tax;

public interface TaxRepository extends MongoRepository<Tax, String>{

}

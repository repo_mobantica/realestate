package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.FinancialYear;

public interface FinancialYearRepository  extends MongoRepository<FinancialYear, String>{

}

package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.Architectural;

public interface ArchitecturalRepository extends MongoRepository<Architectural, String> {

}

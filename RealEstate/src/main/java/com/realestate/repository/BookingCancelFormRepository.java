package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.BookingCancelForm;

public interface BookingCancelFormRepository extends MongoRepository<BookingCancelForm, String>{

}

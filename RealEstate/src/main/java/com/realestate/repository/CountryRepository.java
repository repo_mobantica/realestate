package com.realestate.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import com.realestate.bean.Country;

public interface CountryRepository extends MongoRepository<Country, String>
{
   public List<Country> findAll(Sort sort);
   
}
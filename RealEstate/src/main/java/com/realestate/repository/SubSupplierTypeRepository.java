package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.SubSupplierType;

public interface SubSupplierTypeRepository extends MongoRepository<SubSupplierType, String>{

}

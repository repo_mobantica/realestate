package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.realestate.bean.Login;

public interface LoginRepository extends MongoRepository<Login, String>
{
		
}

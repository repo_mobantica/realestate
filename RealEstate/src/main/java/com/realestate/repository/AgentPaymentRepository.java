package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.AgentPayment;

public interface AgentPaymentRepository extends MongoRepository<AgentPayment, String>{

}

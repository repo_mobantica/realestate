package com.realestate.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.GeneratePaymentScheduler;

public interface GeneratePaymentSchedulerRepository extends MongoRepository<GeneratePaymentScheduler, String>{

	 public List<GeneratePaymentScheduler> findAll(Sort sort);
}

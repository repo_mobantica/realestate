package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.BankName;

public interface BankNameRepository extends MongoRepository<BankName, String>{

}

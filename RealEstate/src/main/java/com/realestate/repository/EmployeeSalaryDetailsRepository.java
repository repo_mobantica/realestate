package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.EmployeeSalaryDetails;

public interface EmployeeSalaryDetailsRepository extends MongoRepository<EmployeeSalaryDetails, String> {

}

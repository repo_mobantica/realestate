package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.CompanyBanks;

public interface CompanyBanksRepository extends MongoRepository<CompanyBanks, String>{

}

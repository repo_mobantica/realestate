package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.SubEnquirySource;

public interface SubEnquirySourceRepository extends MongoRepository<SubEnquirySource, String>
{

}

package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.realestate.bean.Project;

public interface ProjectRepository extends MongoRepository<Project, String>
{

}

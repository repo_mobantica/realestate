package com.realestate.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.Budget;
public interface BudgetRepository extends MongoRepository<Budget, String>{
	   public List<Budget> findAll(Sort sort);
}

package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.realestate.bean.ProjectBuilding;

public interface ProjectBuildingRepository extends MongoRepository<ProjectBuilding, String>
{

}

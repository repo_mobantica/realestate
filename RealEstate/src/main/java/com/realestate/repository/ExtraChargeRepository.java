package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.ExtraCharges;

public interface ExtraChargeRepository  extends MongoRepository<ExtraCharges, String>{

}

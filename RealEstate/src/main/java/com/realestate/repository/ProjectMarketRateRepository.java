package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.ProjectMarketRate;

public interface ProjectMarketRateRepository extends MongoRepository<ProjectMarketRate, String>{

}

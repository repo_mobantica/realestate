package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.realestate.bean.ParkingZone;

public interface ParkingZoneRepository extends MongoRepository<ParkingZone, String> 
{
	
}

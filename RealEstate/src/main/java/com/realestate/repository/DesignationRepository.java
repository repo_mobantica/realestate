package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.realestate.bean.Designation;

public interface DesignationRepository extends MongoRepository<Designation, String>
{

}

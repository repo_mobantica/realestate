package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.realestate.bean.Contractor;
public interface ContractorRepository extends MongoRepository<Contractor, String> {

}

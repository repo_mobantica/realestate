package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.LabourAttendance;

public interface LabourAttendanceRepository extends MongoRepository<LabourAttendance, String>{

}

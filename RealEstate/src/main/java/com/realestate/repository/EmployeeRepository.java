package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.realestate.bean.Employee;

public interface EmployeeRepository extends MongoRepository<Employee, String>
{
	
}

package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.UserProjectList;

public interface UserProjectListRepository extends MongoRepository<UserProjectList, String> {

}

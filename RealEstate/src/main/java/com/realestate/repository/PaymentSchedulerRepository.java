package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.realestate.bean.PaymentScheduler;

public interface PaymentSchedulerRepository extends MongoRepository<PaymentScheduler , String> 
{
	
}

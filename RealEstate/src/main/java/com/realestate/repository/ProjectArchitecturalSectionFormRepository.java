package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.ProjectArchitecturalSectionForm;

public interface ProjectArchitecturalSectionFormRepository extends MongoRepository<ProjectArchitecturalSectionForm, String> {

}

package com.realestate.repository;

import java.util.List;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import com.realestate.bean.State;

public interface StateRepository extends MongoRepository<State, String>
{
	
	public List<State> findAll(Sort sort); 
	
}

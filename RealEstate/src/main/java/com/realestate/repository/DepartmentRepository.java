package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.realestate.bean.Department;

public interface DepartmentRepository extends MongoRepository<Department, String>
{
	
}

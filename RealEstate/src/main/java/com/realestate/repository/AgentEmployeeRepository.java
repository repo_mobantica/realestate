package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.AgentEmployees;

public interface AgentEmployeeRepository extends MongoRepository<AgentEmployees, String>{

}

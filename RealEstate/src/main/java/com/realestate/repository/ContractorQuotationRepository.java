package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.ContractorQuotation;

public interface ContractorQuotationRepository  extends MongoRepository<ContractorQuotation, String>{

}

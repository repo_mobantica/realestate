package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.UserAssignedProject;

public interface UserAssignedProjectRepository extends MongoRepository<UserAssignedProject, String> 
{

}

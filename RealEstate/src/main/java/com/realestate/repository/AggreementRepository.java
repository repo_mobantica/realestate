package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.realestate.bean.Aggreement;

public interface AggreementRepository extends MongoRepository<Aggreement, String>{

}

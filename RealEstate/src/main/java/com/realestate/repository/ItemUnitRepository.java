package com.realestate.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import com.realestate.bean.ItemUnit;

public interface ItemUnitRepository extends MongoRepository<ItemUnit, String>{
	public List<ItemUnit> findAll(Sort sort);
}

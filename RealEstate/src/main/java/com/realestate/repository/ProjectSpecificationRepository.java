package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.realestate.bean.ProjectSpecification;

public interface ProjectSpecificationRepository extends MongoRepository<ProjectSpecification, String> 
{

}

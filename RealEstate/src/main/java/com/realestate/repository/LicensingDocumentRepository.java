package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.LicensingDocument;

public interface LicensingDocumentRepository extends MongoRepository<LicensingDocument, String>{

}

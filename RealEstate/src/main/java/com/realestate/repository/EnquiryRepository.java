package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.realestate.bean.Enquiry;

public interface EnquiryRepository extends MongoRepository<Enquiry, String>
{
  
}

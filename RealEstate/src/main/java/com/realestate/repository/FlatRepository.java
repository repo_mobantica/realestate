package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.Flat;
public interface FlatRepository extends MongoRepository<Flat, String>{

}

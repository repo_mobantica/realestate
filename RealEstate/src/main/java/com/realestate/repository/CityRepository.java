package com.realestate.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import com.realestate.bean.City;

public interface CityRepository extends MongoRepository<City, String>
{
	  public List<City> findAll(Sort sort);
}

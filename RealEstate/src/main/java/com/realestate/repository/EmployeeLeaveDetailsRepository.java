package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.EmployeeLeaveDetails;

public interface EmployeeLeaveDetailsRepository extends MongoRepository<EmployeeLeaveDetails, String> {

}

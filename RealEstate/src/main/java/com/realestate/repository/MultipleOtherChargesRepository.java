package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.MultipleOtherCharges;

public interface MultipleOtherChargesRepository extends MongoRepository<MultipleOtherCharges, String> 
{

}

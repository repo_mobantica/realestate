package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.MaterialPurchaseRequisitionHistory;

public interface MaterialPurchaseRequisitionHistoryRepository extends MongoRepository<MaterialPurchaseRequisitionHistory, String>{

}

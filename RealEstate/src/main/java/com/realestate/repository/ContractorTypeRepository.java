package com.realestate.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.ContractorType;

public interface ContractorTypeRepository extends MongoRepository<ContractorType, String>{

	public List<ContractorType> findAll(Sort sort);
}

package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.BankEmployees;

public interface BankEmployeesRepository extends MongoRepository<BankEmployees, String>
{

}

package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.ArchitecturalEmployees;

public interface ArchitecturalEmployeesRepository extends MongoRepository<ArchitecturalEmployees, String> {

}

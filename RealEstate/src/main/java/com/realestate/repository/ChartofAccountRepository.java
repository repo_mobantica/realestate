package com.realestate.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import com.realestate.bean.ChartofAccount;

public interface ChartofAccountRepository extends MongoRepository<ChartofAccount, String> {

	 public List<ChartofAccount> findAll(Sort sort); 
}

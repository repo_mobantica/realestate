package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.Labour;

public interface LabourRepository extends MongoRepository<Labour, String>{

}

package com.realestate.repository;
import org.springframework.data.mongodb.repository.MongoRepository;
import com.realestate.bean.Brand;

public interface BrandRepository extends MongoRepository<Brand, String>{

}

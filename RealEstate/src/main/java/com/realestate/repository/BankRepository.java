package com.realestate.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import com.realestate.bean.Bank;

public interface BankRepository extends MongoRepository<Bank, String>
{
	   public List<Bank> findAll(Sort sort);
}

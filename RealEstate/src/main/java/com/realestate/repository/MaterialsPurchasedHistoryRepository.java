package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.MaterialsPurchasedHistory;

public interface MaterialsPurchasedHistoryRepository extends MongoRepository<MaterialsPurchasedHistory, String>
{

}

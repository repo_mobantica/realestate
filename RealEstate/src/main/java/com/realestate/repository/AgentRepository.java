package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.realestate.bean.Agent;

public interface AgentRepository extends MongoRepository<Agent, String>{

}

package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.FlatPossessionCheckList;

public interface FlatPossessionCheckListRepository extends MongoRepository<FlatPossessionCheckList, String>{

}

package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.SupplierBillPayment;

public interface SupplierBillPaymentRepository  extends MongoRepository<SupplierBillPayment, String>{

}

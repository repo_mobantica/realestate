package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.Supplier;

public interface SupplierRepository extends MongoRepository<Supplier, String>{

}

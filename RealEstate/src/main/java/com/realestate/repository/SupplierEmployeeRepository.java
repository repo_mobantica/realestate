package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.realestate.bean.SupplierEmployees;

public interface SupplierEmployeeRepository extends MongoRepository<SupplierEmployees, String>
{

}

package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.realestate.bean.ItemSubCategory;

public interface ItemSubCategoryRepository extends MongoRepository<ItemSubCategory, String>
{
	
}

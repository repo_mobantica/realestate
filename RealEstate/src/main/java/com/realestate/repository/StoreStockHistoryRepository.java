package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.StoreStockHistory;
public interface StoreStockHistoryRepository   extends MongoRepository<StoreStockHistory, String>{

}

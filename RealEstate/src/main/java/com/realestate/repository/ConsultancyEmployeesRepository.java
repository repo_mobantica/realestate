package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.ConsultancyEmployees;

public interface ConsultancyEmployeesRepository extends MongoRepository<ConsultancyEmployees, String> {

}

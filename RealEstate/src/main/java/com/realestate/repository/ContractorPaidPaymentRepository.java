package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.ContractorPaidPayment;

public interface ContractorPaidPaymentRepository extends MongoRepository<ContractorPaidPayment, String> {

}

package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.CustomerPaymentReminderLetterHistory;

public interface CustomerPaymentReminderLetterHistoryRepository extends MongoRepository<CustomerPaymentReminderLetterHistory, String>{

}

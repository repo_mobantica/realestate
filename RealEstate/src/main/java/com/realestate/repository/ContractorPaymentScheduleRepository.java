package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.realestate.bean.ContractorPaymentSchedule;

public interface ContractorPaymentScheduleRepository extends MongoRepository<ContractorPaymentSchedule, String>{

}

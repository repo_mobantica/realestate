package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.SupplierQuotation;

public interface SupplierQuotationRepository  extends MongoRepository<SupplierQuotation, String>{

}

package com.realestate.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.SupplierQuotationHistory;

public interface SupplierQuotationHistoryRepository  extends MongoRepository<SupplierQuotationHistory, String>{
/*	public List<SupplierQuotationHistory> findAll(Sort sort);
	public Long countByquotationHistoriId(long quotationHistoriId);*/
}

package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.realestate.bean.Floor;

public interface FloorRepository extends MongoRepository<Floor, String>
{
	
}

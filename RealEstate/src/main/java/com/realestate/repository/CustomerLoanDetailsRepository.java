package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.CustomerLoanDetails;

public interface CustomerLoanDetailsRepository extends MongoRepository<CustomerLoanDetails, String>{

}

package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.CustomerOtherPaymentDetails;

public interface CustomerOtherPaymentDetailsRepository extends MongoRepository<CustomerOtherPaymentDetails, String> 
{

}

package com.realestate.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import com.realestate.bean.Currency;

public interface CurrencyRepository extends MongoRepository<Currency, String>{
	   public List<Currency> findAll(Sort sort); 
}

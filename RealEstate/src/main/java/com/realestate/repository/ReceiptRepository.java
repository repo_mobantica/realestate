package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.Receipt;

public interface ReceiptRepository extends MongoRepository<Receipt, String> 
{

}

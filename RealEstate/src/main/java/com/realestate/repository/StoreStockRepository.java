package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.StoreStock;

public interface StoreStockRepository extends MongoRepository<StoreStock, String>{

}

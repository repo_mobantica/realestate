package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.CompanyDocuments;

public interface CompanyDocumentsRepository extends MongoRepository<CompanyDocuments, String> 
{

}

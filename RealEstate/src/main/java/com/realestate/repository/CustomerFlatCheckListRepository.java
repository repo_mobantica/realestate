package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.CustomerFlatCheckList;

public interface CustomerFlatCheckListRepository extends MongoRepository<CustomerFlatCheckList, String>{

}

package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.realestate.bean.LocationArea;


public interface LocationAreaRepository extends MongoRepository<LocationArea, String>
{
	
}

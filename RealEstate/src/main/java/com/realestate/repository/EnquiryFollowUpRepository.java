package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.realestate.bean.EnquiryFollowUp;

public interface EnquiryFollowUpRepository extends MongoRepository<EnquiryFollowUp, String>
{
	
}

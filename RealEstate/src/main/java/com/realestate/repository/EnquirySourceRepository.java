package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.realestate.bean.EnquirySource;
public interface EnquirySourceRepository extends MongoRepository<EnquirySource, String> {

}

package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.ReraDocuments;

public interface ReraDocumentRepository extends MongoRepository<ReraDocuments, String>
{
  
}

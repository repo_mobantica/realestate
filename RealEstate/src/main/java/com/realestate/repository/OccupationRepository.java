package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.realestate.bean.Occupation;
public interface OccupationRepository extends MongoRepository<Occupation, String>{

}

package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.SubContractorType;

public interface SubContractorTypeRepository extends MongoRepository<SubContractorType, String>{

}

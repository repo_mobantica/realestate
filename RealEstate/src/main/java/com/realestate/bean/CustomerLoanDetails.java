package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "customerloandetails")
public class CustomerLoanDetails
{
	@Id
	private String customerloanId;
	private String bookingId;
	private String customerName;
	private Double totalAmount;	
	private Double loanAmount;
	private Double remainingAmount;
	private String bankName;
	private String fileNumber;
	private String bankPersonName;
	private String bankPersonMobNo;
	private String bankPersonEmailId;
	private String againstAggreement;
	private String againstStampDuty;
	private String againstRegistration;
	private String againstGST;
	
	public CustomerLoanDetails()
	{
		
	}

	public CustomerLoanDetails(String customerloanId, String bookingId, String customerName, Double totalAmount,
			Double loanAmount, Double remainingAmount, String bankName, String fileNumber, String bankPersonName,
			String bankPersonMobNo, String bankPersonEmailId, String againstAggreement, String againstStampDuty,
			String againstRegistration, String againstGST) {
		super();
		this.customerloanId = customerloanId;
		this.bookingId = bookingId;
		this.customerName = customerName;
		this.totalAmount = totalAmount;
		this.loanAmount = loanAmount;
		this.remainingAmount = remainingAmount;
		this.bankName = bankName;
		this.fileNumber = fileNumber;
		this.bankPersonName = bankPersonName;
		this.bankPersonMobNo = bankPersonMobNo;
		this.bankPersonEmailId = bankPersonEmailId;
		this.againstAggreement = againstAggreement;
		this.againstStampDuty = againstStampDuty;
		this.againstRegistration = againstRegistration;
		this.againstGST = againstGST;
	}

	public String getCustomerloanId() {
		return customerloanId;
	}

	public void setCustomerloanId(String customerloanId) {
		this.customerloanId = customerloanId;
	}

	public String getBookingId() {
		return bookingId;
	}

	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Double getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(Double loanAmount) {
		this.loanAmount = loanAmount;
	}

	public Double getRemainingAmount() {
		return remainingAmount;
	}

	public void setRemainingAmount(Double remainingAmount) {
		this.remainingAmount = remainingAmount;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getFileNumber() {
		return fileNumber;
	}

	public void setFileNumber(String fileNumber) {
		this.fileNumber = fileNumber;
	}

	public String getBankPersonName() {
		return bankPersonName;
	}

	public void setBankPersonName(String bankPersonName) {
		this.bankPersonName = bankPersonName;
	}

	public String getBankPersonMobNo() {
		return bankPersonMobNo;
	}

	public void setBankPersonMobNo(String bankPersonMobNo) {
		this.bankPersonMobNo = bankPersonMobNo;
	}

	public String getBankPersonEmailId() {
		return bankPersonEmailId;
	}

	public void setBankPersonEmailId(String bankPersonEmailId) {
		this.bankPersonEmailId = bankPersonEmailId;
	}

	public String getAgainstAggreement() {
		return againstAggreement;
	}

	public void setAgainstAggreement(String againstAggreement) {
		this.againstAggreement = againstAggreement;
	}

	public String getAgainstStampDuty() {
		return againstStampDuty;
	}

	public void setAgainstStampDuty(String againstStampDuty) {
		this.againstStampDuty = againstStampDuty;
	}

	public String getAgainstRegistration() {
		return againstRegistration;
	}

	public void setAgainstRegistration(String againstRegistration) {
		this.againstRegistration = againstRegistration;
	}

	public String getAgainstGST() {
		return againstGST;
	}

	public void setAgainstGST(String againstGST) {
		this.againstGST = againstGST;
	}

}

package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "materialpurchaserequisitions")
public class MaterialPurchaseRequisition {

	@Id
	private String requisitionId;
	private String employeeId;
	private String projectId;
	private String buildingId;
	private String wingId;
	private String requiredDate;
	private int totalNoItem;
	private double totalQuantity;
	private String status;
	
	private String creationDate;
	private String updateDate;
	private String userName;
	
	@Transient
	 private String employeeName;
	   
	public MaterialPurchaseRequisition() {}

	public MaterialPurchaseRequisition(String requisitionId, String employeeId, String projectId, String buildingId,
			String wingId, String requiredDate, int totalNoItem, double totalQuantity, String status,
			String creationDate, String updateDate, String userName) {
		super();
		this.requisitionId = requisitionId;
		this.employeeId = employeeId;
		this.projectId = projectId;
		this.buildingId = buildingId;
		this.wingId = wingId;
		this.requiredDate = requiredDate;
		this.totalNoItem = totalNoItem;
		this.totalQuantity = totalQuantity;
		this.status = status;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getRequisitionId() {
		return requisitionId;
	}

	public void setRequisitionId(String requisitionId) {
		this.requisitionId = requisitionId;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	public String getWingId() {
		return wingId;
	}

	public void setWingId(String wingId) {
		this.wingId = wingId;
	}

	public String getRequiredDate() {
		return requiredDate;
	}

	public void setRequiredDate(String requiredDate) {
		this.requiredDate = requiredDate;
	}

	public int getTotalNoItem() {
		return totalNoItem;
	}

	public void setTotalNoItem(int totalNoItem) {
		this.totalNoItem = totalNoItem;
	}

	public double getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(double totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	
}

package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "contractorquotations")
public class ContractorQuotation {


	@Id
    private String quotationId;
	private String workId;;
    private String contractorId;;
    private long totalAmount;
    
    private String status;
    
	private String creationDate;
	private String updateDate;
	private String userName;
	
    public ContractorQuotation()
    {}

	public ContractorQuotation(String quotationId, String workId, String contractorId, long totalAmount,
			String status, String creationDate, String updateDate, String userName) {
		super();
		this.quotationId = quotationId;
		this.workId = workId;
		this.contractorId = contractorId;
		this.totalAmount = totalAmount;
		this.status = status;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getQuotationId() {
		return quotationId;
	}

	public void setQuotationId(String quotationId) {
		this.quotationId = quotationId;
	}

	public String getWorkId() {
		return workId;
	}

	public void setWorkId(String workId) {
		this.workId = workId;
	}

	public String getContractorId() {
		return contractorId;
	}

	public void setContractorId(String contractorId) {
		this.contractorId = contractorId;
	}

	public long getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(long totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}

package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "supplierquotationhistories")
public class SupplierQuotationHistory {

	@Id
    private long quotationHistoriId;
	private String quotationId;
	private String itemId;
	private String itemSize;
	private String itemUnit;
    private String itemBrandName;
    private Double quantity;
    private Double rate;
    private Double total;
    private Double discountPer;
    private Double discountAmount;
    private Double gstPer;
    private Double gstAmount;
    private Double grandTotal;
    
    
	@Transient
	private String itemName;
	@Transient
	private String suppliertypeId;
	@Transient
    private String subsuppliertypeId;
	@Transient
    private String subsupplierType;
	
    public SupplierQuotationHistory()
    {    }

	public SupplierQuotationHistory(long quotationHistoriId, String quotationId, String itemId, String itemSize,
			String itemUnit, String itemBrandName, Double quantity, Double rate, Double total, Double discountPer,
			Double discountAmount, Double gstPer, Double gstAmount, Double grandTotal) {
		super();
		this.quotationHistoriId = quotationHistoriId;
		this.quotationId = quotationId;
		this.itemId = itemId;
		this.itemSize = itemSize;
		this.itemUnit = itemUnit;
		this.itemBrandName = itemBrandName;
		this.quantity = quantity;
		this.rate = rate;
		this.total = total;
		this.discountPer = discountPer;
		this.discountAmount = discountAmount;
		this.gstPer = gstPer;
		this.gstAmount = gstAmount;
		this.grandTotal = grandTotal;
	}

	public long getQuotationHistoriId() {
		return quotationHistoriId;
	}

	public void setQuotationHistoriId(long quotationHistoriId) {
		this.quotationHistoriId = quotationHistoriId;
	}

	public String getQuotationId() {
		return quotationId;
	}

	public void setQuotationId(String quotationId) {
		this.quotationId = quotationId;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getItemSize() {
		return itemSize;
	}

	public void setItemSize(String itemSize) {
		this.itemSize = itemSize;
	}

	public String getItemUnit() {
		return itemUnit;
	}

	public void setItemUnit(String itemUnit) {
		this.itemUnit = itemUnit;
	}

	public String getItemBrandName() {
		return itemBrandName;
	}

	public void setItemBrandName(String itemBrandName) {
		this.itemBrandName = itemBrandName;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Double getDiscountPer() {
		return discountPer;
	}

	public void setDiscountPer(Double discountPer) {
		this.discountPer = discountPer;
	}

	public Double getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}

	public Double getGstPer() {
		return gstPer;
	}

	public void setGstPer(Double gstPer) {
		this.gstPer = gstPer;
	}

	public Double getGstAmount() {
		return gstAmount;
	}

	public void setGstAmount(Double gstAmount) {
		this.gstAmount = gstAmount;
	}

	public Double getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(Double grandTotal) {
		this.grandTotal = grandTotal;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getSuppliertypeId() {
		return suppliertypeId;
	}

	public void setSuppliertypeId(String suppliertypeId) {
		this.suppliertypeId = suppliertypeId;
	}

	public String getSubsuppliertypeId() {
		return subsuppliertypeId;
	}

	public void setSubsuppliertypeId(String subsuppliertypeId) {
		this.subsuppliertypeId = subsuppliertypeId;
	}

	public String getSubsupplierType() {
		return subsupplierType;
	}

	public void setSubsupplierType(String subsupplierType) {
		this.subsupplierType = subsupplierType;
	}

}

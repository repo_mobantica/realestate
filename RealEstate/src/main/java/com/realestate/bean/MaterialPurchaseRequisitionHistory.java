package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "materialpurchaserequisitionhistories")
public class MaterialPurchaseRequisitionHistory {
	@Id
	private String requisitionHistoryId;
	private String requisitionId;
	private String itemId;
	private String itemunitName;
	private String itemSize;
	private double itemQuantity;
	
	@Transient
	private String suppliertypeId;
	@Transient
    private String subsuppliertypeId;
	@Transient
	private double itemInitialDiscount;
	@Transient
	private double gstPer;
	@Transient
	private double itemInitialPrice;
	@Transient
	private String itemName;
	
	public MaterialPurchaseRequisitionHistory() {}

	public MaterialPurchaseRequisitionHistory(String requisitionHistoryId, String requisitionId, String itemId,
			String itemunitName, String itemSize, double itemQuantity) {
		super();
		this.requisitionHistoryId = requisitionHistoryId;
		this.requisitionId = requisitionId;
		this.itemId = itemId;
		this.itemunitName = itemunitName;
		this.itemSize = itemSize;
		this.itemQuantity = itemQuantity;
	}

	public String getRequisitionHistoryId() {
		return requisitionHistoryId;
	}

	public void setRequisitionHistoryId(String requisitionHistoryId) {
		this.requisitionHistoryId = requisitionHistoryId;
	}

	public String getRequisitionId() {
		return requisitionId;
	}

	public void setRequisitionId(String requisitionId) {
		this.requisitionId = requisitionId;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getItemunitName() {
		return itemunitName;
	}

	public void setItemunitName(String itemunitName) {
		this.itemunitName = itemunitName;
	}

	public String getItemSize() {
		return itemSize;
	}

	public void setItemSize(String itemSize) {
		this.itemSize = itemSize;
	}

	public double getItemQuantity() {
		return itemQuantity;
	}

	public void setItemQuantity(double itemQuantity) {
		this.itemQuantity = itemQuantity;
	}

	public String getSuppliertypeId() {
		return suppliertypeId;
	}

	public void setSuppliertypeId(String suppliertypeId) {
		this.suppliertypeId = suppliertypeId;
	}

	public String getSubsuppliertypeId() {
		return subsuppliertypeId;
	}

	public void setSubsuppliertypeId(String subsuppliertypeId) {
		this.subsuppliertypeId = subsuppliertypeId;
	}

	public double getItemInitialDiscount() {
		return itemInitialDiscount;
	}

	public void setItemInitialDiscount(double itemInitialDiscount) {
		this.itemInitialDiscount = itemInitialDiscount;
	}

	public double getGstPer() {
		return gstPer;
	}

	public void setGstPer(double gstPer) {
		this.gstPer = gstPer;
	}

	public double getItemInitialPrice() {
		return itemInitialPrice;
	}

	public void setItemInitialPrice(double itemInitialPrice) {
		this.itemInitialPrice = itemInitialPrice;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

}

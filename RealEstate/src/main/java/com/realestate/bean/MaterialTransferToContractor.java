package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "materialtransfertocontractor")
public class MaterialTransferToContractor {

	@Id
    private String materialTransferId;
	private String storeId;
	private String contractorId;
    private long contractorEmployeeId;
    private String projectId;
    private String buildingId;
    private String wingId;

	private String creationDate;
	private String updateDate;
	private String userName;
	
	@Transient
	private String contractorfirmName;
	@Transient
	private String employeeName;
	@Transient
	private String projectName;
	@Transient
	private String buildingName;
	@Transient
	private String wingName;
	
	public MaterialTransferToContractor() {}

	public MaterialTransferToContractor(String materialTransferId, String storeId, String contractorId,
			long contractorEmployeeId, String projectId, String buildingId, String wingId, String creationDate,
			String updateDate, String userName) {
		super();
		this.materialTransferId = materialTransferId;
		this.storeId = storeId;
		this.contractorId = contractorId;
		this.contractorEmployeeId = contractorEmployeeId;
		this.projectId = projectId;
		this.buildingId = buildingId;
		this.wingId = wingId;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getMaterialTransferId() {
		return materialTransferId;
	}

	public void setMaterialTransferId(String materialTransferId) {
		this.materialTransferId = materialTransferId;
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public String getContractorId() {
		return contractorId;
	}

	public void setContractorId(String contractorId) {
		this.contractorId = contractorId;
	}

	public long getContractorEmployeeId() {
		return contractorEmployeeId;
	}

	public void setContractorEmployeeId(long contractorEmployeeId) {
		this.contractorEmployeeId = contractorEmployeeId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	public String getWingId() {
		return wingId;
	}

	public void setWingId(String wingId) {
		this.wingId = wingId;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getContractorfirmName() {
		return contractorfirmName;
	}

	public void setContractorfirmName(String contractorfirmName) {
		this.contractorfirmName = contractorfirmName;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getBuildingName() {
		return buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	public String getWingName() {
		return wingName;
	}

	public void setWingName(String wingName) {
		this.wingName = wingName;
	}
	  
}

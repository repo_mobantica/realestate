package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "agents")
public class Agent {

	@Id
    private String agentId;
	private String agentfirmName;
	private String agentfirmType;
    private String firmpanNumber;
    private String firmgstNumber;
    private Double brokeragePercentage;
    private String agentreraNumber;
    private String checkPrintingName;
    private String agentpaymentTerms;
    
	private String agentfirmAddress;

    private String agentPincode;
    private String bankName;
    private String branchName;
    private String bankifscCode;
    private String firmbankacNumber;

	private String commissionBased;
	
	private String creationDate;
	private String updateDate;
	private String userName;
	
	
	public Agent() {}


	public Agent(String agentId, String agentfirmName, String agentfirmType, String firmpanNumber, String firmgstNumber,
			Double brokeragePercentage, String agentreraNumber, String checkPrintingName, String agentpaymentTerms,
			String agentfirmAddress, String agentPincode, String bankName, String branchName, String bankifscCode,
			String firmbankacNumber, String commissionBased, String creationDate, String updateDate, String userName) {
		super();
		this.agentId = agentId;
		this.agentfirmName = agentfirmName;
		this.agentfirmType = agentfirmType;
		this.firmpanNumber = firmpanNumber;
		this.firmgstNumber = firmgstNumber;
		this.brokeragePercentage = brokeragePercentage;
		this.agentreraNumber = agentreraNumber;
		this.checkPrintingName = checkPrintingName;
		this.agentpaymentTerms = agentpaymentTerms;
		this.agentfirmAddress = agentfirmAddress;
		this.agentPincode = agentPincode;
		this.bankName = bankName;
		this.branchName = branchName;
		this.bankifscCode = bankifscCode;
		this.firmbankacNumber = firmbankacNumber;
		this.commissionBased = commissionBased;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}


	public String getAgentId() {
		return agentId;
	}


	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}


	public String getAgentfirmName() {
		return agentfirmName;
	}


	public void setAgentfirmName(String agentfirmName) {
		this.agentfirmName = agentfirmName;
	}


	public String getAgentfirmType() {
		return agentfirmType;
	}


	public void setAgentfirmType(String agentfirmType) {
		this.agentfirmType = agentfirmType;
	}


	public String getFirmpanNumber() {
		return firmpanNumber;
	}


	public void setFirmpanNumber(String firmpanNumber) {
		this.firmpanNumber = firmpanNumber;
	}


	public String getFirmgstNumber() {
		return firmgstNumber;
	}


	public void setFirmgstNumber(String firmgstNumber) {
		this.firmgstNumber = firmgstNumber;
	}


	public Double getBrokeragePercentage() {
		return brokeragePercentage;
	}


	public void setBrokeragePercentage(Double brokeragePercentage) {
		this.brokeragePercentage = brokeragePercentage;
	}


	public String getAgentreraNumber() {
		return agentreraNumber;
	}


	public void setAgentreraNumber(String agentreraNumber) {
		this.agentreraNumber = agentreraNumber;
	}


	public String getCheckPrintingName() {
		return checkPrintingName;
	}


	public void setCheckPrintingName(String checkPrintingName) {
		this.checkPrintingName = checkPrintingName;
	}


	public String getAgentpaymentTerms() {
		return agentpaymentTerms;
	}


	public void setAgentpaymentTerms(String agentpaymentTerms) {
		this.agentpaymentTerms = agentpaymentTerms;
	}


	public String getAgentfirmAddress() {
		return agentfirmAddress;
	}


	public void setAgentfirmAddress(String agentfirmAddress) {
		this.agentfirmAddress = agentfirmAddress;
	}


	public String getAgentPincode() {
		return agentPincode;
	}


	public void setAgentPincode(String agentPincode) {
		this.agentPincode = agentPincode;
	}


	public String getBankName() {
		return bankName;
	}


	public void setBankName(String bankName) {
		this.bankName = bankName;
	}


	public String getBranchName() {
		return branchName;
	}


	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}


	public String getBankifscCode() {
		return bankifscCode;
	}


	public void setBankifscCode(String bankifscCode) {
		this.bankifscCode = bankifscCode;
	}


	public String getFirmbankacNumber() {
		return firmbankacNumber;
	}


	public void setFirmbankacNumber(String firmbankacNumber) {
		this.firmbankacNumber = firmbankacNumber;
	}


	public String getCommissionBased() {
		return commissionBased;
	}


	public void setCommissionBased(String commissionBased) {
		this.commissionBased = commissionBased;
	}


	public String getCreationDate() {
		return creationDate;
	}


	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}


	public String getUpdateDate() {
		return updateDate;
	}


	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}

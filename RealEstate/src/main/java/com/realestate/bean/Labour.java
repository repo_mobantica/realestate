package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "labours")
public class Labour {

	@Id
	private String labourId;
	private String labourfirstName;
	private String labourmiddleName;
	private String labourlastName;
	private String labourGender;
	private String labourAge;
	private String projectId;
	
	private String labourAddress;
	private String countryId;
	private String stateId;
	private String cityId;
	private String locationareaId;
	private String areaPincode;
	
	private String labourMobileno;
	private String labourAadharcardno;
	private String labourJoiningdate;
	private Double labourBasicpay;
	
	private String status;
	
	private String creationDate;
	private String updateDate;
	private String userName;
	
	public Labour() {}

	public Labour(String labourId, String labourfirstName, String labourmiddleName, String labourlastName,
			String labourGender, String labourAge, String projectId, String labourAddress, String countryId,
			String stateId, String cityId, String locationareaId, String areaPincode, String labourMobileno,
			String labourAadharcardno, String labourJoiningdate, Double labourBasicpay, String status,
			String creationDate, String updateDate, String userName) {
		super();
		this.labourId = labourId;
		this.labourfirstName = labourfirstName;
		this.labourmiddleName = labourmiddleName;
		this.labourlastName = labourlastName;
		this.labourGender = labourGender;
		this.labourAge = labourAge;
		this.projectId = projectId;
		this.labourAddress = labourAddress;
		this.countryId = countryId;
		this.stateId = stateId;
		this.cityId = cityId;
		this.locationareaId = locationareaId;
		this.areaPincode = areaPincode;
		this.labourMobileno = labourMobileno;
		this.labourAadharcardno = labourAadharcardno;
		this.labourJoiningdate = labourJoiningdate;
		this.labourBasicpay = labourBasicpay;
		this.status = status;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getLabourId() {
		return labourId;
	}

	public void setLabourId(String labourId) {
		this.labourId = labourId;
	}

	public String getLabourfirstName() {
		return labourfirstName;
	}

	public void setLabourfirstName(String labourfirstName) {
		this.labourfirstName = labourfirstName;
	}

	public String getLabourmiddleName() {
		return labourmiddleName;
	}

	public void setLabourmiddleName(String labourmiddleName) {
		this.labourmiddleName = labourmiddleName;
	}

	public String getLabourlastName() {
		return labourlastName;
	}

	public void setLabourlastName(String labourlastName) {
		this.labourlastName = labourlastName;
	}

	public String getLabourGender() {
		return labourGender;
	}

	public void setLabourGender(String labourGender) {
		this.labourGender = labourGender;
	}

	public String getLabourAge() {
		return labourAge;
	}

	public void setLabourAge(String labourAge) {
		this.labourAge = labourAge;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getLabourAddress() {
		return labourAddress;
	}

	public void setLabourAddress(String labourAddress) {
		this.labourAddress = labourAddress;
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getStateId() {
		return stateId;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getLocationareaId() {
		return locationareaId;
	}

	public void setLocationareaId(String locationareaId) {
		this.locationareaId = locationareaId;
	}

	public String getAreaPincode() {
		return areaPincode;
	}

	public void setAreaPincode(String areaPincode) {
		this.areaPincode = areaPincode;
	}

	public String getLabourMobileno() {
		return labourMobileno;
	}

	public void setLabourMobileno(String labourMobileno) {
		this.labourMobileno = labourMobileno;
	}

	public String getLabourAadharcardno() {
		return labourAadharcardno;
	}

	public void setLabourAadharcardno(String labourAadharcardno) {
		this.labourAadharcardno = labourAadharcardno;
	}

	public String getLabourJoiningdate() {
		return labourJoiningdate;
	}

	public void setLabourJoiningdate(String labourJoiningdate) {
		this.labourJoiningdate = labourJoiningdate;
	}

	public Double getLabourBasicpay() {
		return labourBasicpay;
	}

	public void setLabourBasicpay(Double labourBasicpay) {
		this.labourBasicpay = labourBasicpay;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	
}

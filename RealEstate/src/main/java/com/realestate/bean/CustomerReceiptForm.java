package com.realestate.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
@Document(collection = "customerreceiptform")
public class CustomerReceiptForm {

	@Id
	private String receiptId;
	private String bookingId;
	private Double paymentAmount;
	private String paymentType;
	private String paymentMode;
	private String bankName;
	private String branchName;
	private String chequeNumber;
	private String narration;
	private String date;
	
	private Date creationDate;
	private Date updateDate;
	private String userName;
	
	private String status;
	
	@Transient
	private String customerName;

	@Transient
	private String flatNumber;
	
	public CustomerReceiptForm()
	{	}

	public CustomerReceiptForm(String receiptId, String bookingId, Double paymentAmount, String paymentType,
			String paymentMode, String bankName, String branchName, String chequeNumber, String narration, String date,
			Date creationDate, Date updateDate, String userName, String status, String customerName) {
		super();
		this.receiptId = receiptId;
		this.bookingId = bookingId;
		this.paymentAmount = paymentAmount;
		this.paymentType = paymentType;
		this.paymentMode = paymentMode;
		this.bankName = bankName;
		this.branchName = branchName;
		this.chequeNumber = chequeNumber;
		this.narration = narration;
		this.date = date;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
		this.status = status;
		this.customerName = customerName;
	}

	public String getReceiptId() {
		return receiptId;
	}

	public void setReceiptId(String receiptId) {
		this.receiptId = receiptId;
	}

	public String getBookingId() {
		return bookingId;
	}

	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}

	public Double getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(Double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	public String getNarration() {
		return narration;
	}

	public void setNarration(String narration) {
		this.narration = narration;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getFlatNumber() {
		return flatNumber;
	}

	public void setFlatNumber(String flatNumber) {
		this.flatNumber = flatNumber;
	}
	
}

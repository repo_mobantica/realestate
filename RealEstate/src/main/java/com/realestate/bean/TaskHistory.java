package com.realestate.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "taskhistories")
public class TaskHistory 
{
   @Id
   private long taskHistoryId;
   private String taskId;
   private String taskDescription;
   private String otherTaskStatus;
   private Date   startDate;
   private Date   expectedEndDate;
   private Date   actualEndDate;
   
   @Transient
   private String startDate1;
   @Transient
   private String expectedEndDate1;
   @Transient
   private String actualEndDate1;
   
   
	public TaskHistory() 
	{
		
	}

	public TaskHistory(long taskHistoryId, String taskId, String taskDescription, String otherTaskStatus, Date startDate, Date expectedEndDate, Date actualEndDate)
	{
		super();
		this.taskHistoryId = taskHistoryId;
		this.taskId = taskId;
		this.taskDescription = taskDescription;
		this.otherTaskStatus = otherTaskStatus;
		this.startDate = startDate;
		this.expectedEndDate = expectedEndDate;
		this.actualEndDate = actualEndDate;
	}

	public long getTaskHistoryId() {
		return taskHistoryId;
	}

	public void setTaskHistoryId(long taskHistoryId) {
		this.taskHistoryId = taskHistoryId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getTaskDescription() {
		return taskDescription;
	}

	public void setTaskDescription(String taskDescription) {
		this.taskDescription = taskDescription;
	}

	public String getOtherTaskStatus() {
		return otherTaskStatus;
	}

	public void setOtherTaskStatus(String otherTaskStatus) {
		this.otherTaskStatus = otherTaskStatus;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getExpectedEndDate() {
		return expectedEndDate;
	}

	public void setExpectedEndDate(Date expectedEndDate) {
		this.expectedEndDate = expectedEndDate;
	}

	public Date getActualEndDate() {
		return actualEndDate;
	}

	public void setActualEndDate(Date actualEndDate) {
		this.actualEndDate = actualEndDate;
	}

	public String getStartDate1() {
		return startDate1;
	}

	public void setStartDate1(String startDate1) {
		this.startDate1 = startDate1;
	}

	public String getExpectedEndDate1() {
		return expectedEndDate1;
	}

	public void setExpectedEndDate1(String expectedEndDate1) {
		this.expectedEndDate1 = expectedEndDate1;
	}

	public String getActualEndDate1() {
		return actualEndDate1;
	}

	public void setActualEndDate1(String actualEndDate1) {
		this.actualEndDate1 = actualEndDate1;
	}
	
}

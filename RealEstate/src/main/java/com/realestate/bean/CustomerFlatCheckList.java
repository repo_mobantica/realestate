package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "customerflatchecklists")
public class CustomerFlatCheckList {

	@Id
    private String checkListId;
	private String bookingId;
	private String aggreementId;
	
    private String ceilings;
    private String tiles;
    private String door;
    private String windows;
    private String plumbingAndsanitory;
    private String electrification;
    private String painting;
    private String propertyTaxNo;
    private String electricBillMeterNo;
    
    private String creationDate;
	private String updateDate;
	private String userName;
	
	public CustomerFlatCheckList() {}

	public CustomerFlatCheckList(String checkListId, String bookingId, String aggreementId, String ceilings,
			String tiles, String door, String windows, String plumbingAndsanitory, String electrification,
			String painting, String propertyTaxNo, String electricBillMeterNo, String creationDate, String updateDate,
			String userName) {
		super();
		this.checkListId = checkListId;
		this.bookingId = bookingId;
		this.aggreementId = aggreementId;
		this.ceilings = ceilings;
		this.tiles = tiles;
		this.door = door;
		this.windows = windows;
		this.plumbingAndsanitory = plumbingAndsanitory;
		this.electrification = electrification;
		this.painting = painting;
		this.propertyTaxNo = propertyTaxNo;
		this.electricBillMeterNo = electricBillMeterNo;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getCheckListId() {
		return checkListId;
	}

	public void setCheckListId(String checkListId) {
		this.checkListId = checkListId;
	}

	public String getBookingId() {
		return bookingId;
	}

	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}

	public String getAggreementId() {
		return aggreementId;
	}

	public void setAggreementId(String aggreementId) {
		this.aggreementId = aggreementId;
	}

	public String getCeilings() {
		return ceilings;
	}

	public void setCeilings(String ceilings) {
		this.ceilings = ceilings;
	}

	public String getTiles() {
		return tiles;
	}

	public void setTiles(String tiles) {
		this.tiles = tiles;
	}

	public String getDoor() {
		return door;
	}

	public void setDoor(String door) {
		this.door = door;
	}

	public String getWindows() {
		return windows;
	}

	public void setWindows(String windows) {
		this.windows = windows;
	}

	public String getPlumbingAndsanitory() {
		return plumbingAndsanitory;
	}

	public void setPlumbingAndsanitory(String plumbingAndsanitory) {
		this.plumbingAndsanitory = plumbingAndsanitory;
	}

	public String getElectrification() {
		return electrification;
	}

	public void setElectrification(String electrification) {
		this.electrification = electrification;
	}

	public String getPainting() {
		return painting;
	}

	public void setPainting(String painting) {
		this.painting = painting;
	}

	public String getPropertyTaxNo() {
		return propertyTaxNo;
	}

	public void setPropertyTaxNo(String propertyTaxNo) {
		this.propertyTaxNo = propertyTaxNo;
	}

	public String getElectricBillMeterNo() {
		return electricBillMeterNo;
	}

	public void setElectricBillMeterNo(String electricBillMeterNo) {
		this.electricBillMeterNo = electricBillMeterNo;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	
}

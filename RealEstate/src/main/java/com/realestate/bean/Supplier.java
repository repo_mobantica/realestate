package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "suppliers")
public class Supplier 
{
	@Id
    private String supplierId;
	private String supplierfirmName;
	private String supplierfirmType;
	private String supplierfirmAddress;
    private String supplierPincode;
    private String firmpanNumber;
    private String firmgstNumber;
    private String bankName;
    private String branchName;
    private String bankifscCode;
    private String firmbankacNumber;
    private String suppliertypeId;
    private String subsuppliertypeId;
    private String supplierpaymentTerms;
    private String checkPrintingName;
    private String supplierOfficeNo;
    
    private String creationDate;
    private String updateDate;
    private String userName;
    
    public Supplier()
    {
    	
    }

	public Supplier(String supplierId, String supplierfirmName, String supplierfirmType, String supplierfirmAddress,
			String supplierPincode, String firmpanNumber, String firmgstNumber, String bankName, String branchName,
			String bankifscCode, String firmbankacNumber, String suppliertypeId, String subsuppliertypeId,
			String supplierpaymentTerms, String checkPrintingName, String supplierOfficeNo, String creationDate,
			String updateDate, String userName) {
		super();
		this.supplierId = supplierId;
		this.supplierfirmName = supplierfirmName;
		this.supplierfirmType = supplierfirmType;
		this.supplierfirmAddress = supplierfirmAddress;
		this.supplierPincode = supplierPincode;
		this.firmpanNumber = firmpanNumber;
		this.firmgstNumber = firmgstNumber;
		this.bankName = bankName;
		this.branchName = branchName;
		this.bankifscCode = bankifscCode;
		this.firmbankacNumber = firmbankacNumber;
		this.suppliertypeId = suppliertypeId;
		this.subsuppliertypeId = subsuppliertypeId;
		this.supplierpaymentTerms = supplierpaymentTerms;
		this.checkPrintingName = checkPrintingName;
		this.supplierOfficeNo = supplierOfficeNo;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	public String getSupplierfirmName() {
		return supplierfirmName;
	}

	public void setSupplierfirmName(String supplierfirmName) {
		this.supplierfirmName = supplierfirmName;
	}

	public String getSupplierfirmType() {
		return supplierfirmType;
	}

	public void setSupplierfirmType(String supplierfirmType) {
		this.supplierfirmType = supplierfirmType;
	}

	public String getSupplierfirmAddress() {
		return supplierfirmAddress;
	}

	public void setSupplierfirmAddress(String supplierfirmAddress) {
		this.supplierfirmAddress = supplierfirmAddress;
	}

	public String getSupplierPincode() {
		return supplierPincode;
	}

	public void setSupplierPincode(String supplierPincode) {
		this.supplierPincode = supplierPincode;
	}

	public String getFirmpanNumber() {
		return firmpanNumber;
	}

	public void setFirmpanNumber(String firmpanNumber) {
		this.firmpanNumber = firmpanNumber;
	}

	public String getFirmgstNumber() {
		return firmgstNumber;
	}

	public void setFirmgstNumber(String firmgstNumber) {
		this.firmgstNumber = firmgstNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getBankifscCode() {
		return bankifscCode;
	}

	public void setBankifscCode(String bankifscCode) {
		this.bankifscCode = bankifscCode;
	}

	public String getFirmbankacNumber() {
		return firmbankacNumber;
	}

	public void setFirmbankacNumber(String firmbankacNumber) {
		this.firmbankacNumber = firmbankacNumber;
	}

	public String getSuppliertypeId() {
		return suppliertypeId;
	}

	public void setSuppliertypeId(String suppliertypeId) {
		this.suppliertypeId = suppliertypeId;
	}

	public String getSubsuppliertypeId() {
		return subsuppliertypeId;
	}

	public void setSubsuppliertypeId(String subsuppliertypeId) {
		this.subsuppliertypeId = subsuppliertypeId;
	}

	public String getSupplierpaymentTerms() {
		return supplierpaymentTerms;
	}

	public void setSupplierpaymentTerms(String supplierpaymentTerms) {
		this.supplierpaymentTerms = supplierpaymentTerms;
	}

	public String getCheckPrintingName() {
		return checkPrintingName;
	}

	public void setCheckPrintingName(String checkPrintingName) {
		this.checkPrintingName = checkPrintingName;
	}

	public String getSupplierOfficeNo() {
		return supplierOfficeNo;
	}

	public void setSupplierOfficeNo(String supplierOfficeNo) {
		this.supplierOfficeNo = supplierOfficeNo;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}



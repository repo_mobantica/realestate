package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "subsuppliertypes")
public class SubSupplierType {

	@Id
    private String subsuppliertypeId;
	 private String subsupplierType;
    private String suppliertypeId;
    private String creationDate;
    private String updateDate;
    private String userName;
    
    public SubSupplierType() {}

	public SubSupplierType(String subsuppliertypeId, String subsupplierType, String suppliertypeId, String creationDate,
			String updateDate, String userName) {
		super();
		this.subsuppliertypeId = subsuppliertypeId;
		this.subsupplierType = subsupplierType;
		this.suppliertypeId = suppliertypeId;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getSubsuppliertypeId() {
		return subsuppliertypeId;
	}

	public void setSubsuppliertypeId(String subsuppliertypeId) {
		this.subsuppliertypeId = subsuppliertypeId;
	}

	public String getSubsupplierType() {
		return subsupplierType;
	}

	public void setSubsupplierType(String subsupplierType) {
		this.subsupplierType = subsupplierType;
	}
	
	public String getSuppliertypeId() {
		return suppliertypeId;
	}

	public void setSuppliertypeId(String suppliertypeId) {
		this.suppliertypeId = suppliertypeId;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
    
}

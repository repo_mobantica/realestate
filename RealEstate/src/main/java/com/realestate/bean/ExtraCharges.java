package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "extracharges")
public class ExtraCharges 
{
	@Id
	private String chargesId;
	private String bookingId;
	private String bookingDate;
	private String customerName;
	private Double aggreementValue1;
	private Double maintenanceCost;
	private Double otherCharges;
	
	private String creationDate;
	private String updateDate;
	private String userName;
	
	public ExtraCharges()
	{
		
	}
	
	public ExtraCharges(String chargesId, String bookingId, String bookingDate, String customerName,
			Double aggreementValue1, Double maintenanceCost, Double otherCharges, String creationDate,
			String updateDate, String userName)
	{
		super();
		this.chargesId = chargesId;
		this.bookingId = bookingId;
		this.bookingDate = bookingDate;
		this.customerName = customerName;
		this.aggreementValue1 = aggreementValue1;
		this.maintenanceCost = maintenanceCost;
		this.otherCharges = otherCharges;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}



	public String getChargesId() {
		return chargesId;
	}

	public void setChargesId(String chargesId) {
		this.chargesId = chargesId;
	}

	public String getBookingId() {
		return bookingId;
	}

	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}

	public String getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Double getAggreementValue1() {
		return aggreementValue1;
	}

	public void setAggreementValue1(Double aggreementValue1) {
		this.aggreementValue1 = aggreementValue1;
	}

	public Double getMaintenanceCost() {
		return maintenanceCost;
	}

	public void setMaintenanceCost(Double maintenanceCost) {
		this.maintenanceCost = maintenanceCost;
	}

	public Double getOtherCharges() {
		return otherCharges;
	}

	public void setOtherCharges(Double otherCharges) {
		this.otherCharges = otherCharges;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}

package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "stores")
@CompoundIndexes({@CompoundIndex(name="storeIndex", unique= true, def="{'storeName':1}")})
public class Store {

	 	@Id
	    private String storeId;
	    private String storeName;
	    private String creationDate;
	    private String updateDate;
	    private String userName;
	    
	    public Store() 
	    {
	    	
	    }

		public Store(String storeId, String storeName, String creationDate, String updateDate, String userName) {
			super();
			this.storeId = storeId;
			this.storeName = storeName;
			this.creationDate = creationDate;
			this.updateDate = updateDate;
			this.userName = userName;
		}

		public String getStoreId() {
			return storeId;
		}

		public void setStoreId(String storeId) {
			this.storeId = storeId;
		}

		public String getStoreName() {
			return storeName;
		}

		public void setStoreName(String storeName) {
			this.storeName = storeName;
		}

		public String getCreationDate() {
			return creationDate;
		}

		public void setCreationDate(String creationDate) {
			this.creationDate = creationDate;
		}

		public String getUpdateDate() {
			return updateDate;
		}

		public void setUpdateDate(String updateDate) {
			this.updateDate = updateDate;
		}

		public String getUserName() {
			return userName;
		}

		public void setUserName(String userName) {
			this.userName = userName;
		} 
	    
	    
}

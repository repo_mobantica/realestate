package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "supplierbillpaymenthistories")
public class SupplierBillPaymentHistory {

	@Id
    private String supplierBillPaymentHistoriId;
	private String materialsPurchasedId;
	private String supplierId;
	private String storeId;
	
	private String companyBankId;
	private Double amount;
	private String paymentMode;
	private String refNumber;
	private String narration;
	
	private String chartaccountId;
	private String subchartaccountId;
	
    private String creationDate;
    private String updateDate;
    private String userName;
    
    public SupplierBillPaymentHistory() {}

	public SupplierBillPaymentHistory(String supplierBillPaymentHistoriId, String materialsPurchasedId,
			String supplierId, String storeId, String companyBankId, Double amount, String paymentMode,
			String refNumber, String narration, String chartaccountId, String subchartaccountId,
			String creationDate, String updateDate, String userName) {
		super();
		this.supplierBillPaymentHistoriId = supplierBillPaymentHistoriId;
		this.materialsPurchasedId = materialsPurchasedId;
		this.supplierId = supplierId;
		this.storeId = storeId;
		this.companyBankId = companyBankId;
		this.amount = amount;
		this.paymentMode = paymentMode;
		this.refNumber = refNumber;
		this.narration = narration;
		this.chartaccountId = chartaccountId;
		this.subchartaccountId = subchartaccountId;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getSupplierBillPaymentHistoriId() {
		return supplierBillPaymentHistoriId;
	}

	public void setSupplierBillPaymentHistoriId(String supplierBillPaymentHistoriId) {
		this.supplierBillPaymentHistoriId = supplierBillPaymentHistoriId;
	}

	public String getMaterialsPurchasedId() {
		return materialsPurchasedId;
	}

	public void setMaterialsPurchasedId(String materialsPurchasedId) {
		this.materialsPurchasedId = materialsPurchasedId;
	}

	public String getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public String getCompanyBankId() {
		return companyBankId;
	}

	public void setCompanyBankId(String companyBankId) {
		this.companyBankId = companyBankId;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getRefNumber() {
		return refNumber;
	}

	public void setRefNumber(String refNumber) {
		this.refNumber = refNumber;
	}

	public String getNarration() {
		return narration;
	}

	public void setNarration(String narration) {
		this.narration = narration;
	}

	public String getChartaccountId() {
		return chartaccountId;
	}

	public void setChartaccountId(String chartaccountId) {
		this.chartaccountId = chartaccountId;
	}

	public String getSubchartaccountId() {
		return subchartaccountId;
	}

	public void setSubchartaccountId(String subchartaccountId) {
		this.subchartaccountId = subchartaccountId;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

    
}

package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "suppliertypes")
@CompoundIndexes({@CompoundIndex(name="suppliertypeIndex", unique= true, def="{'supplierType':1}")})

public class SupplierType {

	@Id
    private String suppliertypeId;
    private String supplierType;
    private String creationDate;
    private String updateDate;
    private String userName;
    
    public SupplierType() {}

	public SupplierType(String suppliertypeId, String supplierType, String creationDate, String updateDate,
			String userName) {
		super();
		this.suppliertypeId = suppliertypeId;
		this.supplierType = supplierType;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getSuppliertypeId() {
		return suppliertypeId;
	}

	public void setSuppliertypeId(String suppliertypeId) {
		this.suppliertypeId = suppliertypeId;
	}

	public String getSupplierType() {
		return supplierType;
	}

	public void setSupplierType(String supplierType) {
		this.supplierType = supplierType;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
    
}

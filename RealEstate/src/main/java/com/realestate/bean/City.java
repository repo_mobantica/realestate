package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "cities")
//@CompoundIndexes({@CompoundIndex(name="cityIndex", unique= true, def="{'cityName':1}")})
public class City
{
	@Id
	private String cityId;
	private String cityName;
	private String countryId;
	private String stateId;
	private String creationDate;
	private String updateDate;
	private String userName;
	
	public City()
	{
		
	}

	public City(String cityId, String cityName, String countryId, String stateId, String creationDate,
			String updateDate, String userName) {
		super();
		this.cityId = cityId;
		this.cityName = cityName;
		this.countryId = countryId;
		this.stateId = stateId;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getStateId() {
		return stateId;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	
}

package com.realestate.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "customerpaymentdetails")
public class CustomerPaymentDetails {

	@Id
	private String id;
	private String bookingId;
	private Double totalpayAggreement;
	private Double totalpayStampDuty;
	private Double totalpayRegistration;
	private Double totalpaygstAmount;
	private String amountStatus;

	private Date creationDate;
	private Date updateDate;
	private String userName;
	
	public CustomerPaymentDetails() {}

	public CustomerPaymentDetails(String id, String bookingId, Double totalpayAggreement, Double totalpayStampDuty,
			Double totalpayRegistration, Double totalpaygstAmount, String amountStatus, Date creationDate,
			Date updateDate, String userName) {
		super();
		this.id = id;
		this.bookingId = bookingId;
		this.totalpayAggreement = totalpayAggreement;
		this.totalpayStampDuty = totalpayStampDuty;
		this.totalpayRegistration = totalpayRegistration;
		this.totalpaygstAmount = totalpaygstAmount;
		this.amountStatus = amountStatus;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBookingId() {
		return bookingId;
	}

	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}

	public Double getTotalpayAggreement() {
		return totalpayAggreement;
	}

	public void setTotalpayAggreement(Double totalpayAggreement) {
		this.totalpayAggreement = totalpayAggreement;
	}

	public Double getTotalpayStampDuty() {
		return totalpayStampDuty;
	}

	public void setTotalpayStampDuty(Double totalpayStampDuty) {
		this.totalpayStampDuty = totalpayStampDuty;
	}

	public Double getTotalpayRegistration() {
		return totalpayRegistration;
	}

	public void setTotalpayRegistration(Double totalpayRegistration) {
		this.totalpayRegistration = totalpayRegistration;
	}

	public Double getTotalpaygstAmount() {
		return totalpaygstAmount;
	}

	public void setTotalpaygstAmount(Double totalpaygstAmount) {
		this.totalpaygstAmount = totalpaygstAmount;
	}

	public String getAmountStatus() {
		return amountStatus;
	}

	public void setAmountStatus(String amountStatus) {
		this.amountStatus = amountStatus;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}


	
}

package com.realestate.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "aggreements")
public class Aggreement {

	@Id
	private String aggreementId;
	private String bookingId;
	private String aggreementNumber;
	private String aggreementDate;
	private String firstApplicantfirstname;
	private String firstApplicantmiddlename;
	private String firstApplicantlastname;
	private String firstApplicantmaidenfirstname;
	private String firstApplicantmaidenmiddlename;
	private String firstApplicantmaidenlastname;
	private String firstApplicantGender;
	private String firstApplicantMarried;
	private String firstApplicantDob;
	private String firstApplicantmobileNumber1;
	private String firstApplicantmobileNumber2;
	private String firstApplicantSpuseName;
	private String firstApplicantSpouseDob;
	private String firstApplicantAnniversaryDate;
	private String firstApplicantFatherName;
	private String firstApplicantEmailId;
	private String firstApplicantPanCardNo;
	private String firstApplicantAadharno;
	private String firstApplicantMotherTonque;
	private String firstApplicantPresentAddress;
	private String firstApplicantPresentPincode;
	private String firstApplicantPermanentaddress;
	private String firstApplicantPermanentPincode;
	private String firstApplicantEducation;
	private String firstApplicantOccupation;
	private String firstApplicantOrganizationName;
	private String firstApplicantOrganizationType;
	private String firstApplicantOrganizationaddress;
	private String firstApplicantofficeNumber;
	private String firstApplicantofficeEmail;
	private String firstApplicantIndustrySector;
	private String firstApplicantWorkFunction;
	private String firstApplicantExperience;
	private String firstApplicantIncome;
	
	private String secondApplicantfirstname;
	private String secondApplicantmiddlename;
	private String secondApplicantlastname;
	private String secondApplicantmaidenfirstname;
	private String secondApplicantmaidenmiddlename;
	private String secondApplicantmaidenlastname;
	private String secondApplicantGender;
	private String secondApplicantMarried;
	private String secondApplicantDob;
	private String secondApplicantmobileNumber1;
	private String secondApplicantmobileNumber2;
	private String secondApplicantSpouseName;
	private String secondApplicantSpouseDob;
	private String secondApplicantAnnivaversaryDate;
	private String secondApplicantFatherName;
	private String secondApplicantEmail;
	private String secondApplicantPancardno;
	private String secondApplicantAadharno;
	private String secondApplicantMotherTongue;
	private String secondApplicantRelation;
	private String secondApplicantPresentAddress;
	private String secondApplicantPresentPincode;
	private String secondApplicantPermanentaddress;
	private String secondApplicantPermanentPincode;
	private String secondApplicantEducation;
	private String secondApplicantOccupation;
	private String secondApplicantOrganizationName;
	private String secondApplicantOrganizationType;
	private String secondApplicantOrganizationaddress;
	private String secondApplicantofficeNumber;
	private String secondApplicantofficeEmail;
	private String secondApplicantIndustrySector;
	private String secondApplicantWorkFunction;
	private String secondApplicantExperience;
	private String secondApplicantIncome;
	private String aggreementstatus;
	
	private Date creationDate;
	private Date updateDate;
	private String userName;
	
	public Aggreement() {}

	public Aggreement(String aggreementId, String bookingId, String aggreementNumber, String aggreementDate,
			String firstApplicantfirstname, String firstApplicantmiddlename, String firstApplicantlastname,
			String firstApplicantmaidenfirstname, String firstApplicantmaidenmiddlename,
			String firstApplicantmaidenlastname, String firstApplicantGender, String firstApplicantMarried,
			String firstApplicantDob, String firstApplicantmobileNumber1, String firstApplicantmobileNumber2,
			String firstApplicantSpuseName, String firstApplicantSpouseDob, String firstApplicantAnniversaryDate,
			String firstApplicantFatherName, String firstApplicantEmailId, String firstApplicantPanCardNo,
			String firstApplicantAadharno, String firstApplicantMotherTonque, String firstApplicantPresentAddress,
			String firstApplicantPresentPincode, String firstApplicantPermanentaddress,
			String firstApplicantPermanentPincode, String firstApplicantEducation, String firstApplicantOccupation,
			String firstApplicantOrganizationName, String firstApplicantOrganizationType,
			String firstApplicantOrganizationaddress, String firstApplicantofficeNumber,
			String firstApplicantofficeEmail, String firstApplicantIndustrySector, String firstApplicantWorkFunction,
			String firstApplicantExperience, String firstApplicantIncome, String secondApplicantfirstname,
			String secondApplicantmiddlename, String secondApplicantlastname, String secondApplicantmaidenfirstname,
			String secondApplicantmaidenmiddlename, String secondApplicantmaidenlastname, String secondApplicantGender,
			String secondApplicantMarried, String secondApplicantDob, String secondApplicantmobileNumber1,
			String secondApplicantmobileNumber2, String secondApplicantSpouseName, String secondApplicantSpouseDob,
			String secondApplicantAnnivaversaryDate, String secondApplicantFatherName, String secondApplicantEmail,
			String secondApplicantPancardno, String secondApplicantAadharno, String secondApplicantMotherTongue,
			String secondApplicantRelation, String secondApplicantPresentAddress, String secondApplicantPresentPincode,
			String secondApplicantPermanentaddress, String secondApplicantPermanentPincode,
			String secondApplicantEducation, String secondApplicantOccupation, String secondApplicantOrganizationName,
			String secondApplicantOrganizationType, String secondApplicantOrganizationaddress,
			String secondApplicantofficeNumber, String secondApplicantofficeEmail, String secondApplicantIndustrySector,
			String secondApplicantWorkFunction, String secondApplicantExperience, String secondApplicantIncome,
			String aggreementstatus, Date creationDate, Date updateDate, String userName) {
		super();
		this.aggreementId = aggreementId;
		this.bookingId = bookingId;
		this.aggreementNumber = aggreementNumber;
		this.aggreementDate = aggreementDate;
		this.firstApplicantfirstname = firstApplicantfirstname;
		this.firstApplicantmiddlename = firstApplicantmiddlename;
		this.firstApplicantlastname = firstApplicantlastname;
		this.firstApplicantmaidenfirstname = firstApplicantmaidenfirstname;
		this.firstApplicantmaidenmiddlename = firstApplicantmaidenmiddlename;
		this.firstApplicantmaidenlastname = firstApplicantmaidenlastname;
		this.firstApplicantGender = firstApplicantGender;
		this.firstApplicantMarried = firstApplicantMarried;
		this.firstApplicantDob = firstApplicantDob;
		this.firstApplicantmobileNumber1 = firstApplicantmobileNumber1;
		this.firstApplicantmobileNumber2 = firstApplicantmobileNumber2;
		this.firstApplicantSpuseName = firstApplicantSpuseName;
		this.firstApplicantSpouseDob = firstApplicantSpouseDob;
		this.firstApplicantAnniversaryDate = firstApplicantAnniversaryDate;
		this.firstApplicantFatherName = firstApplicantFatherName;
		this.firstApplicantEmailId = firstApplicantEmailId;
		this.firstApplicantPanCardNo = firstApplicantPanCardNo;
		this.firstApplicantAadharno = firstApplicantAadharno;
		this.firstApplicantMotherTonque = firstApplicantMotherTonque;
		this.firstApplicantPresentAddress = firstApplicantPresentAddress;
		this.firstApplicantPresentPincode = firstApplicantPresentPincode;
		this.firstApplicantPermanentaddress = firstApplicantPermanentaddress;
		this.firstApplicantPermanentPincode = firstApplicantPermanentPincode;
		this.firstApplicantEducation = firstApplicantEducation;
		this.firstApplicantOccupation = firstApplicantOccupation;
		this.firstApplicantOrganizationName = firstApplicantOrganizationName;
		this.firstApplicantOrganizationType = firstApplicantOrganizationType;
		this.firstApplicantOrganizationaddress = firstApplicantOrganizationaddress;
		this.firstApplicantofficeNumber = firstApplicantofficeNumber;
		this.firstApplicantofficeEmail = firstApplicantofficeEmail;
		this.firstApplicantIndustrySector = firstApplicantIndustrySector;
		this.firstApplicantWorkFunction = firstApplicantWorkFunction;
		this.firstApplicantExperience = firstApplicantExperience;
		this.firstApplicantIncome = firstApplicantIncome;
		this.secondApplicantfirstname = secondApplicantfirstname;
		this.secondApplicantmiddlename = secondApplicantmiddlename;
		this.secondApplicantlastname = secondApplicantlastname;
		this.secondApplicantmaidenfirstname = secondApplicantmaidenfirstname;
		this.secondApplicantmaidenmiddlename = secondApplicantmaidenmiddlename;
		this.secondApplicantmaidenlastname = secondApplicantmaidenlastname;
		this.secondApplicantGender = secondApplicantGender;
		this.secondApplicantMarried = secondApplicantMarried;
		this.secondApplicantDob = secondApplicantDob;
		this.secondApplicantmobileNumber1 = secondApplicantmobileNumber1;
		this.secondApplicantmobileNumber2 = secondApplicantmobileNumber2;
		this.secondApplicantSpouseName = secondApplicantSpouseName;
		this.secondApplicantSpouseDob = secondApplicantSpouseDob;
		this.secondApplicantAnnivaversaryDate = secondApplicantAnnivaversaryDate;
		this.secondApplicantFatherName = secondApplicantFatherName;
		this.secondApplicantEmail = secondApplicantEmail;
		this.secondApplicantPancardno = secondApplicantPancardno;
		this.secondApplicantAadharno = secondApplicantAadharno;
		this.secondApplicantMotherTongue = secondApplicantMotherTongue;
		this.secondApplicantRelation = secondApplicantRelation;
		this.secondApplicantPresentAddress = secondApplicantPresentAddress;
		this.secondApplicantPresentPincode = secondApplicantPresentPincode;
		this.secondApplicantPermanentaddress = secondApplicantPermanentaddress;
		this.secondApplicantPermanentPincode = secondApplicantPermanentPincode;
		this.secondApplicantEducation = secondApplicantEducation;
		this.secondApplicantOccupation = secondApplicantOccupation;
		this.secondApplicantOrganizationName = secondApplicantOrganizationName;
		this.secondApplicantOrganizationType = secondApplicantOrganizationType;
		this.secondApplicantOrganizationaddress = secondApplicantOrganizationaddress;
		this.secondApplicantofficeNumber = secondApplicantofficeNumber;
		this.secondApplicantofficeEmail = secondApplicantofficeEmail;
		this.secondApplicantIndustrySector = secondApplicantIndustrySector;
		this.secondApplicantWorkFunction = secondApplicantWorkFunction;
		this.secondApplicantExperience = secondApplicantExperience;
		this.secondApplicantIncome = secondApplicantIncome;
		this.aggreementstatus = aggreementstatus;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getAggreementId() {
		return aggreementId;
	}

	public void setAggreementId(String aggreementId) {
		this.aggreementId = aggreementId;
	}

	public String getBookingId() {
		return bookingId;
	}

	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}

	public String getAggreementNumber() {
		return aggreementNumber;
	}

	public void setAggreementNumber(String aggreementNumber) {
		this.aggreementNumber = aggreementNumber;
	}

	public String getAggreementDate() {
		return aggreementDate;
	}

	public void setAggreementDate(String aggreementDate) {
		this.aggreementDate = aggreementDate;
	}

	public String getFirstApplicantfirstname() {
		return firstApplicantfirstname;
	}

	public void setFirstApplicantfirstname(String firstApplicantfirstname) {
		this.firstApplicantfirstname = firstApplicantfirstname;
	}

	public String getFirstApplicantmiddlename() {
		return firstApplicantmiddlename;
	}

	public void setFirstApplicantmiddlename(String firstApplicantmiddlename) {
		this.firstApplicantmiddlename = firstApplicantmiddlename;
	}

	public String getFirstApplicantlastname() {
		return firstApplicantlastname;
	}

	public void setFirstApplicantlastname(String firstApplicantlastname) {
		this.firstApplicantlastname = firstApplicantlastname;
	}

	public String getFirstApplicantmaidenfirstname() {
		return firstApplicantmaidenfirstname;
	}

	public void setFirstApplicantmaidenfirstname(String firstApplicantmaidenfirstname) {
		this.firstApplicantmaidenfirstname = firstApplicantmaidenfirstname;
	}

	public String getFirstApplicantmaidenmiddlename() {
		return firstApplicantmaidenmiddlename;
	}

	public void setFirstApplicantmaidenmiddlename(String firstApplicantmaidenmiddlename) {
		this.firstApplicantmaidenmiddlename = firstApplicantmaidenmiddlename;
	}

	public String getFirstApplicantmaidenlastname() {
		return firstApplicantmaidenlastname;
	}

	public void setFirstApplicantmaidenlastname(String firstApplicantmaidenlastname) {
		this.firstApplicantmaidenlastname = firstApplicantmaidenlastname;
	}

	public String getFirstApplicantGender() {
		return firstApplicantGender;
	}

	public void setFirstApplicantGender(String firstApplicantGender) {
		this.firstApplicantGender = firstApplicantGender;
	}

	public String getFirstApplicantMarried() {
		return firstApplicantMarried;
	}

	public void setFirstApplicantMarried(String firstApplicantMarried) {
		this.firstApplicantMarried = firstApplicantMarried;
	}

	public String getFirstApplicantDob() {
		return firstApplicantDob;
	}

	public void setFirstApplicantDob(String firstApplicantDob) {
		this.firstApplicantDob = firstApplicantDob;
	}

	public String getFirstApplicantmobileNumber1() {
		return firstApplicantmobileNumber1;
	}

	public void setFirstApplicantmobileNumber1(String firstApplicantmobileNumber1) {
		this.firstApplicantmobileNumber1 = firstApplicantmobileNumber1;
	}

	public String getFirstApplicantmobileNumber2() {
		return firstApplicantmobileNumber2;
	}

	public void setFirstApplicantmobileNumber2(String firstApplicantmobileNumber2) {
		this.firstApplicantmobileNumber2 = firstApplicantmobileNumber2;
	}

	public String getFirstApplicantSpuseName() {
		return firstApplicantSpuseName;
	}

	public void setFirstApplicantSpuseName(String firstApplicantSpuseName) {
		this.firstApplicantSpuseName = firstApplicantSpuseName;
	}

	public String getFirstApplicantSpouseDob() {
		return firstApplicantSpouseDob;
	}

	public void setFirstApplicantSpouseDob(String firstApplicantSpouseDob) {
		this.firstApplicantSpouseDob = firstApplicantSpouseDob;
	}

	public String getFirstApplicantAnniversaryDate() {
		return firstApplicantAnniversaryDate;
	}

	public void setFirstApplicantAnniversaryDate(String firstApplicantAnniversaryDate) {
		this.firstApplicantAnniversaryDate = firstApplicantAnniversaryDate;
	}

	public String getFirstApplicantFatherName() {
		return firstApplicantFatherName;
	}

	public void setFirstApplicantFatherName(String firstApplicantFatherName) {
		this.firstApplicantFatherName = firstApplicantFatherName;
	}

	public String getFirstApplicantEmailId() {
		return firstApplicantEmailId;
	}

	public void setFirstApplicantEmailId(String firstApplicantEmailId) {
		this.firstApplicantEmailId = firstApplicantEmailId;
	}

	public String getFirstApplicantPanCardNo() {
		return firstApplicantPanCardNo;
	}

	public void setFirstApplicantPanCardNo(String firstApplicantPanCardNo) {
		this.firstApplicantPanCardNo = firstApplicantPanCardNo;
	}

	public String getFirstApplicantAadharno() {
		return firstApplicantAadharno;
	}

	public void setFirstApplicantAadharno(String firstApplicantAadharno) {
		this.firstApplicantAadharno = firstApplicantAadharno;
	}

	public String getFirstApplicantMotherTonque() {
		return firstApplicantMotherTonque;
	}

	public void setFirstApplicantMotherTonque(String firstApplicantMotherTonque) {
		this.firstApplicantMotherTonque = firstApplicantMotherTonque;
	}

	public String getFirstApplicantPresentAddress() {
		return firstApplicantPresentAddress;
	}

	public void setFirstApplicantPresentAddress(String firstApplicantPresentAddress) {
		this.firstApplicantPresentAddress = firstApplicantPresentAddress;
	}

	public String getFirstApplicantPresentPincode() {
		return firstApplicantPresentPincode;
	}

	public void setFirstApplicantPresentPincode(String firstApplicantPresentPincode) {
		this.firstApplicantPresentPincode = firstApplicantPresentPincode;
	}

	public String getFirstApplicantPermanentaddress() {
		return firstApplicantPermanentaddress;
	}

	public void setFirstApplicantPermanentaddress(String firstApplicantPermanentaddress) {
		this.firstApplicantPermanentaddress = firstApplicantPermanentaddress;
	}

	public String getFirstApplicantPermanentPincode() {
		return firstApplicantPermanentPincode;
	}

	public void setFirstApplicantPermanentPincode(String firstApplicantPermanentPincode) {
		this.firstApplicantPermanentPincode = firstApplicantPermanentPincode;
	}

	public String getFirstApplicantEducation() {
		return firstApplicantEducation;
	}

	public void setFirstApplicantEducation(String firstApplicantEducation) {
		this.firstApplicantEducation = firstApplicantEducation;
	}

	public String getFirstApplicantOccupation() {
		return firstApplicantOccupation;
	}

	public void setFirstApplicantOccupation(String firstApplicantOccupation) {
		this.firstApplicantOccupation = firstApplicantOccupation;
	}

	public String getFirstApplicantOrganizationName() {
		return firstApplicantOrganizationName;
	}

	public void setFirstApplicantOrganizationName(String firstApplicantOrganizationName) {
		this.firstApplicantOrganizationName = firstApplicantOrganizationName;
	}

	public String getFirstApplicantOrganizationType() {
		return firstApplicantOrganizationType;
	}

	public void setFirstApplicantOrganizationType(String firstApplicantOrganizationType) {
		this.firstApplicantOrganizationType = firstApplicantOrganizationType;
	}

	public String getFirstApplicantOrganizationaddress() {
		return firstApplicantOrganizationaddress;
	}

	public void setFirstApplicantOrganizationaddress(String firstApplicantOrganizationaddress) {
		this.firstApplicantOrganizationaddress = firstApplicantOrganizationaddress;
	}

	public String getFirstApplicantofficeNumber() {
		return firstApplicantofficeNumber;
	}

	public void setFirstApplicantofficeNumber(String firstApplicantofficeNumber) {
		this.firstApplicantofficeNumber = firstApplicantofficeNumber;
	}

	public String getFirstApplicantofficeEmail() {
		return firstApplicantofficeEmail;
	}

	public void setFirstApplicantofficeEmail(String firstApplicantofficeEmail) {
		this.firstApplicantofficeEmail = firstApplicantofficeEmail;
	}

	public String getFirstApplicantIndustrySector() {
		return firstApplicantIndustrySector;
	}

	public void setFirstApplicantIndustrySector(String firstApplicantIndustrySector) {
		this.firstApplicantIndustrySector = firstApplicantIndustrySector;
	}

	public String getFirstApplicantWorkFunction() {
		return firstApplicantWorkFunction;
	}

	public void setFirstApplicantWorkFunction(String firstApplicantWorkFunction) {
		this.firstApplicantWorkFunction = firstApplicantWorkFunction;
	}

	public String getFirstApplicantExperience() {
		return firstApplicantExperience;
	}

	public void setFirstApplicantExperience(String firstApplicantExperience) {
		this.firstApplicantExperience = firstApplicantExperience;
	}

	public String getFirstApplicantIncome() {
		return firstApplicantIncome;
	}

	public void setFirstApplicantIncome(String firstApplicantIncome) {
		this.firstApplicantIncome = firstApplicantIncome;
	}

	public String getSecondApplicantfirstname() {
		return secondApplicantfirstname;
	}

	public void setSecondApplicantfirstname(String secondApplicantfirstname) {
		this.secondApplicantfirstname = secondApplicantfirstname;
	}

	public String getSecondApplicantmiddlename() {
		return secondApplicantmiddlename;
	}

	public void setSecondApplicantmiddlename(String secondApplicantmiddlename) {
		this.secondApplicantmiddlename = secondApplicantmiddlename;
	}

	public String getSecondApplicantlastname() {
		return secondApplicantlastname;
	}

	public void setSecondApplicantlastname(String secondApplicantlastname) {
		this.secondApplicantlastname = secondApplicantlastname;
	}

	public String getSecondApplicantmaidenfirstname() {
		return secondApplicantmaidenfirstname;
	}

	public void setSecondApplicantmaidenfirstname(String secondApplicantmaidenfirstname) {
		this.secondApplicantmaidenfirstname = secondApplicantmaidenfirstname;
	}

	public String getSecondApplicantmaidenmiddlename() {
		return secondApplicantmaidenmiddlename;
	}

	public void setSecondApplicantmaidenmiddlename(String secondApplicantmaidenmiddlename) {
		this.secondApplicantmaidenmiddlename = secondApplicantmaidenmiddlename;
	}

	public String getSecondApplicantmaidenlastname() {
		return secondApplicantmaidenlastname;
	}

	public void setSecondApplicantmaidenlastname(String secondApplicantmaidenlastname) {
		this.secondApplicantmaidenlastname = secondApplicantmaidenlastname;
	}

	public String getSecondApplicantGender() {
		return secondApplicantGender;
	}

	public void setSecondApplicantGender(String secondApplicantGender) {
		this.secondApplicantGender = secondApplicantGender;
	}

	public String getSecondApplicantMarried() {
		return secondApplicantMarried;
	}

	public void setSecondApplicantMarried(String secondApplicantMarried) {
		this.secondApplicantMarried = secondApplicantMarried;
	}

	public String getSecondApplicantDob() {
		return secondApplicantDob;
	}

	public void setSecondApplicantDob(String secondApplicantDob) {
		this.secondApplicantDob = secondApplicantDob;
	}

	public String getSecondApplicantmobileNumber1() {
		return secondApplicantmobileNumber1;
	}

	public void setSecondApplicantmobileNumber1(String secondApplicantmobileNumber1) {
		this.secondApplicantmobileNumber1 = secondApplicantmobileNumber1;
	}

	public String getSecondApplicantmobileNumber2() {
		return secondApplicantmobileNumber2;
	}

	public void setSecondApplicantmobileNumber2(String secondApplicantmobileNumber2) {
		this.secondApplicantmobileNumber2 = secondApplicantmobileNumber2;
	}

	public String getSecondApplicantSpouseName() {
		return secondApplicantSpouseName;
	}

	public void setSecondApplicantSpouseName(String secondApplicantSpouseName) {
		this.secondApplicantSpouseName = secondApplicantSpouseName;
	}

	public String getSecondApplicantSpouseDob() {
		return secondApplicantSpouseDob;
	}

	public void setSecondApplicantSpouseDob(String secondApplicantSpouseDob) {
		this.secondApplicantSpouseDob = secondApplicantSpouseDob;
	}

	public String getSecondApplicantAnnivaversaryDate() {
		return secondApplicantAnnivaversaryDate;
	}

	public void setSecondApplicantAnnivaversaryDate(String secondApplicantAnnivaversaryDate) {
		this.secondApplicantAnnivaversaryDate = secondApplicantAnnivaversaryDate;
	}

	public String getSecondApplicantFatherName() {
		return secondApplicantFatherName;
	}

	public void setSecondApplicantFatherName(String secondApplicantFatherName) {
		this.secondApplicantFatherName = secondApplicantFatherName;
	}

	public String getSecondApplicantEmail() {
		return secondApplicantEmail;
	}

	public void setSecondApplicantEmail(String secondApplicantEmail) {
		this.secondApplicantEmail = secondApplicantEmail;
	}

	public String getSecondApplicantPancardno() {
		return secondApplicantPancardno;
	}

	public void setSecondApplicantPancardno(String secondApplicantPancardno) {
		this.secondApplicantPancardno = secondApplicantPancardno;
	}

	public String getSecondApplicantAadharno() {
		return secondApplicantAadharno;
	}

	public void setSecondApplicantAadharno(String secondApplicantAadharno) {
		this.secondApplicantAadharno = secondApplicantAadharno;
	}

	public String getSecondApplicantMotherTongue() {
		return secondApplicantMotherTongue;
	}

	public void setSecondApplicantMotherTongue(String secondApplicantMotherTongue) {
		this.secondApplicantMotherTongue = secondApplicantMotherTongue;
	}

	public String getSecondApplicantRelation() {
		return secondApplicantRelation;
	}

	public void setSecondApplicantRelation(String secondApplicantRelation) {
		this.secondApplicantRelation = secondApplicantRelation;
	}

	public String getSecondApplicantPresentAddress() {
		return secondApplicantPresentAddress;
	}

	public void setSecondApplicantPresentAddress(String secondApplicantPresentAddress) {
		this.secondApplicantPresentAddress = secondApplicantPresentAddress;
	}

	public String getSecondApplicantPresentPincode() {
		return secondApplicantPresentPincode;
	}

	public void setSecondApplicantPresentPincode(String secondApplicantPresentPincode) {
		this.secondApplicantPresentPincode = secondApplicantPresentPincode;
	}

	public String getSecondApplicantPermanentaddress() {
		return secondApplicantPermanentaddress;
	}

	public void setSecondApplicantPermanentaddress(String secondApplicantPermanentaddress) {
		this.secondApplicantPermanentaddress = secondApplicantPermanentaddress;
	}

	public String getSecondApplicantPermanentPincode() {
		return secondApplicantPermanentPincode;
	}

	public void setSecondApplicantPermanentPincode(String secondApplicantPermanentPincode) {
		this.secondApplicantPermanentPincode = secondApplicantPermanentPincode;
	}

	public String getSecondApplicantEducation() {
		return secondApplicantEducation;
	}

	public void setSecondApplicantEducation(String secondApplicantEducation) {
		this.secondApplicantEducation = secondApplicantEducation;
	}

	public String getSecondApplicantOccupation() {
		return secondApplicantOccupation;
	}

	public void setSecondApplicantOccupation(String secondApplicantOccupation) {
		this.secondApplicantOccupation = secondApplicantOccupation;
	}

	public String getSecondApplicantOrganizationName() {
		return secondApplicantOrganizationName;
	}

	public void setSecondApplicantOrganizationName(String secondApplicantOrganizationName) {
		this.secondApplicantOrganizationName = secondApplicantOrganizationName;
	}

	public String getSecondApplicantOrganizationType() {
		return secondApplicantOrganizationType;
	}

	public void setSecondApplicantOrganizationType(String secondApplicantOrganizationType) {
		this.secondApplicantOrganizationType = secondApplicantOrganizationType;
	}

	public String getSecondApplicantOrganizationaddress() {
		return secondApplicantOrganizationaddress;
	}

	public void setSecondApplicantOrganizationaddress(String secondApplicantOrganizationaddress) {
		this.secondApplicantOrganizationaddress = secondApplicantOrganizationaddress;
	}

	public String getSecondApplicantofficeNumber() {
		return secondApplicantofficeNumber;
	}

	public void setSecondApplicantofficeNumber(String secondApplicantofficeNumber) {
		this.secondApplicantofficeNumber = secondApplicantofficeNumber;
	}

	public String getSecondApplicantofficeEmail() {
		return secondApplicantofficeEmail;
	}

	public void setSecondApplicantofficeEmail(String secondApplicantofficeEmail) {
		this.secondApplicantofficeEmail = secondApplicantofficeEmail;
	}

	public String getSecondApplicantIndustrySector() {
		return secondApplicantIndustrySector;
	}

	public void setSecondApplicantIndustrySector(String secondApplicantIndustrySector) {
		this.secondApplicantIndustrySector = secondApplicantIndustrySector;
	}

	public String getSecondApplicantWorkFunction() {
		return secondApplicantWorkFunction;
	}

	public void setSecondApplicantWorkFunction(String secondApplicantWorkFunction) {
		this.secondApplicantWorkFunction = secondApplicantWorkFunction;
	}

	public String getSecondApplicantExperience() {
		return secondApplicantExperience;
	}

	public void setSecondApplicantExperience(String secondApplicantExperience) {
		this.secondApplicantExperience = secondApplicantExperience;
	}

	public String getSecondApplicantIncome() {
		return secondApplicantIncome;
	}

	public void setSecondApplicantIncome(String secondApplicantIncome) {
		this.secondApplicantIncome = secondApplicantIncome;
	}

	public String getAggreementstatus() {
		return aggreementstatus;
	}

	public void setAggreementstatus(String aggreementstatus) {
		this.aggreementstatus = aggreementstatus;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}

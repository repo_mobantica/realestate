package com.realestate.bean;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.ui.ModelMap;

import com.realestate.repository.ProjectRepository;

public class UserLoginProjectLists {

	@Autowired
    ProjectRepository projectRepository;
	@Autowired
	MongoTemplate mongoTemplate;
	public List<Project>UserLoginProjectList(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		

    	Query query = new Query();
    	
		HttpSession session = req.getSession(true);
		
		List<Project> userprojectList=(List<Project>) session.getAttribute("projectList");
		
		Project  project=new Project();
		
		List<Project> projectList=new ArrayList<Project>();
		List<Project> projectList1;
		String projectId;
		for(int i=0;i<userprojectList.size();i++)
		{
			projectId=userprojectList.get(i).getProjectId();
			 project=new Project();
				try
				{
					//  projectList1= new ArrayList<Project>();
				        
				     query = new Query();
				     projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)), Project.class);
					//List<Project> projectList1 = projectRepository.findAll(); 

					System.out.println("projectList1 = "+projectList1.size());
					 if(projectList1.size()!=0)
					 {
					 project.setProjectId(projectList1.get(0).getProjectId());
					/*
					 project.setProjectName(projectList1.get(0).getProjectName());
					 project.setProjectType(projectList1.get(0).getProjectType());
					 project.setProjectAddress(projectList1.get(0).getProjectAddress());
					 project.setCountryName(projectList1.get(0).getCountryName());
					 project.setStateName(projectList1.get(0).getStateName());
					 project.setCityName(projectList1.get(0).getCityName());
					 project.setLocationareaName(projectList1.get(0).getLocationareaName());
					 project.setAreaPincode(projectList1.get(0).getAreaPincode());
					 project.setProjectofficePhoneno(projectList1.get(0).getProjectofficePhoneno());
					 project.setProjectcompanyName(projectList1.get(0).getProjectcompanyName());
					 project.setProjectlandArea(projectList1.get(0).getProjectlandArea());
					 project.setProjectCost(projectList1.get(0).getProjectCost());
					 project.setNumberofBuildings(projectList1.get(0).getNumberofBuildings());
					 project.setLoading(projectList1.get(0).getLoading());
					 project.setReraStatus(projectList1.get(0).getReraStatus());
					 project.setReraNumber(projectList1.get(0).getReraNumber());
					 project.setProjectstartDate(projectList1.get(0).getProjectstartDate());
					 project.setProjectendDate(projectList1.get(0).getProjectendDate());
					 project.setProjectStatus(projectList1.get(0).getProjectStatus());
					 project.setGatORserveyNumber(projectList1.get(0).getGatORserveyNumber());
					 project.setCreationDate(projectList1.get(0).getCreationDate());
					 project.setUpdateDate(projectList1.get(0).getUpdateDate());
					 project.setUserName(projectList1.get(0).getUserName());
					 */
					 projectList.add(project);
					 }
				}
				catch (Exception e) {
					System.out.println("errr = "+e);
				}
		}
		System.out.println(projectList.size());
		
		return 	projectList;
		 
	}
	
}

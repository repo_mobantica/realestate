package com.realestate.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "employeesalarydetails")
public class EmployeeSalaryDetails {

	@Id
    private String salaryDetailsId;
	private int month;
	private int year;
	private String employeeId;
	
	private int daysPresent;
	private int paidLeave;
	
	private Double leaveDeduction;
	private Double conveyance;
	private Double providentFund;
	private Double esiAmount;
	private Double loanAmount;
	private Double professionalTax;
	private Double tds;
	
	private Double totalDeduction;
	private Double netSalary;
	private Double totalAddition;
	
	private Date creationDate;
	private Date updateDate;
	private String userName;	
	
	public EmployeeSalaryDetails() {}

	public EmployeeSalaryDetails(String salaryDetailsId, int month, int year, String employeeId, int daysPresent,
			int paidLeave, Double leaveDeduction, Double conveyance, Double providentFund, Double esiAmount, Double loanAmount,
			Double professionalTax, Double tds, Double totalDeduction, Double netSalary, Double totalAddition, Date creationDate,
			Date updateDate, String userName) {
		super();
		this.salaryDetailsId = salaryDetailsId;
		this.month = month;
		this.year = year;
		this.employeeId = employeeId;
		this.daysPresent = daysPresent;
		this.paidLeave = paidLeave;
		this.leaveDeduction = leaveDeduction;
		this.conveyance = conveyance;
		this.providentFund = providentFund;
		this.esiAmount = esiAmount;
		this.loanAmount = loanAmount;
		this.professionalTax = professionalTax;
		this.tds = tds;
		this.totalDeduction = totalDeduction;
		this.netSalary = netSalary;
		this.totalAddition = totalAddition;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getSalaryDetailsId() {
		return salaryDetailsId;
	}

	public void setSalaryDetailsId(String salaryDetailsId) {
		this.salaryDetailsId = salaryDetailsId;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public int getDaysPresent() {
		return daysPresent;
	}

	public void setDaysPresent(int daysPresent) {
		this.daysPresent = daysPresent;
	}

	public int getPaidLeave() {
		return paidLeave;
	}

	public void setPaidLeave(int paidLeave) {
		this.paidLeave = paidLeave;
	}

	public Double getLeaveDeduction() {
		return leaveDeduction;
	}

	public void setLeaveDeduction(Double leaveDeduction) {
		this.leaveDeduction = leaveDeduction;
	}

	public Double getConveyance() {
		return conveyance;
	}

	public void setConveyance(Double conveyance) {
		this.conveyance = conveyance;
	}

	public Double getProvidentFund() {
		return providentFund;
	}

	public void setProvidentFund(Double providentFund) {
		this.providentFund = providentFund;
	}

	public Double getEsiAmount() {
		return esiAmount;
	}

	public void setEsiAmount(Double esiAmount) {
		this.esiAmount = esiAmount;
	}

	public Double getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(Double loanAmount) {
		this.loanAmount = loanAmount;
	}

	public Double getProfessionalTax() {
		return professionalTax;
	}

	public void setProfessionalTax(Double professionalTax) {
		this.professionalTax = professionalTax;
	}

	public Double getTds() {
		return tds;
	}

	public void setTds(Double tds) {
		this.tds = tds;
	}

	public Double getTotalDeduction() {
		return totalDeduction;
	}

	public void setTotalDeduction(Double totalDeduction) {
		this.totalDeduction = totalDeduction;
	}

	public Double getNetSalary() {
		return netSalary;
	}

	public void setNetSalary(Double netSalary) {
		this.netSalary = netSalary;
	}

	public Double getTotalAddition() {
		return totalAddition;
	}

	public void setTotalAddition(Double totalAddition) {
		this.totalAddition = totalAddition;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}

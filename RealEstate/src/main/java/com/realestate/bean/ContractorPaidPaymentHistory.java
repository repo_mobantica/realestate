package com.realestate.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "contractorpaidpaymenthistories")
public class ContractorPaidPaymentHistory {

	@Id
    private String contractorPaidPaymentHistoriId;
	private String contractorId;
	private String workOrderId;
	private String billNumber;
	private String billDate;
	private long paymentAmount;
	private String paymentDecription;
	private String narration;
	
	private Date creationDate;
	
	private String paymentStatus;
	
	
	public ContractorPaidPaymentHistory() {}


	public ContractorPaidPaymentHistory(String contractorPaidPaymentHistoriId, String contractorId, String workOrderId, String billNumber, String billDate,
			long paymentAmount, String paymentDecription, String narration, Date creationDate, String paymentStatus) {
		super();
		this.contractorPaidPaymentHistoriId = contractorPaidPaymentHistoriId;
		this.contractorId = contractorId;
		this.workOrderId = workOrderId;
		this.billNumber = billNumber;
		this.billDate = billDate;
		this.paymentAmount = paymentAmount;
		this.paymentDecription = paymentDecription;
		this.narration = narration;
		this.creationDate = creationDate;
		this.paymentStatus = paymentStatus;
	}


	public String getContractorPaidPaymentHistoriId() {
		return contractorPaidPaymentHistoriId;
	}


	public void setContractorPaidPaymentHistoriId(String contractorPaidPaymentHistoriId) {
		this.contractorPaidPaymentHistoriId = contractorPaidPaymentHistoriId;
	}


	public String getContractorId() {
		return contractorId;
	}


	public void setContractorId(String contractorId) {
		this.contractorId = contractorId;
	}


	public String getWorkOrderId() {
		return workOrderId;
	}


	public void setWorkOrderId(String workOrderId) {
		this.workOrderId = workOrderId;
	}

	public String getBillNumber() {
		return billNumber;
	}


	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}


	public String getBillDate() {
		return billDate;
	}


	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}


	public long getPaymentAmount() {
		return paymentAmount;
	}


	public void setPaymentAmount(long paymentAmount) {
		this.paymentAmount = paymentAmount;
	}


	public String getPaymentDecription() {
		return paymentDecription;
	}


	public void setPaymentDecription(String paymentDecription) {
		this.paymentDecription = paymentDecription;
	}


	public String getNarration() {
		return narration;
	}


	public void setNarration(String narration) {
		this.narration = narration;
	}


	public Date getCreationDate() {
		return creationDate;
	}


	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}


	public String getPaymentStatus() {
		return paymentStatus;
	}


	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	
}

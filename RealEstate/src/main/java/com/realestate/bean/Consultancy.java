package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "consultancies")
public class Consultancy {
	@Id
	private String consultancyId;
	private String consultancyfirmName;
	private String firmType;
	private String firmpanNumber;
	private String firmgstNumber;
	private String checkPrintingName;
	
	private String consultancyAddress;
	private String countryId;
	private String stateId;
	private String cityId;
	private String locationareaId;
	private String areaPincode;
	
	private String departmentId;
	private String designationId;
	private String employeeId;
	
	private String consultancyBankName;
	private String branchName;
	private String bankifscCode;
	private String consultancyBankacno;
	private String status;
	
	private String creationDate;
	private String updateDate;
	private String userName;
	
	public Consultancy()
	{}

	public Consultancy(String consultancyId, String consultancyfirmName, String firmType, String firmpanNumber,
			String firmgstNumber, String checkPrintingName, String consultancyAddress, String countryId, String stateId,
			String cityId, String locationareaId, String areaPincode, String departmentId, String designationId,
			String employeeId, String consultancyBankName, String branchName, String bankifscCode,
			String consultancyBankacno, String status, String creationDate, String updateDate, String userName) {
		super();
		this.consultancyId = consultancyId;
		this.consultancyfirmName = consultancyfirmName;
		this.firmType = firmType;
		this.firmpanNumber = firmpanNumber;
		this.firmgstNumber = firmgstNumber;
		this.checkPrintingName = checkPrintingName;
		this.consultancyAddress = consultancyAddress;
		this.countryId = countryId;
		this.stateId = stateId;
		this.cityId = cityId;
		this.locationareaId = locationareaId;
		this.areaPincode = areaPincode;
		this.departmentId = departmentId;
		this.designationId = designationId;
		this.employeeId = employeeId;
		this.consultancyBankName = consultancyBankName;
		this.branchName = branchName;
		this.bankifscCode = bankifscCode;
		this.consultancyBankacno = consultancyBankacno;
		this.status = status;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getConsultancyId() {
		return consultancyId;
	}

	public void setConsultancyId(String consultancyId) {
		this.consultancyId = consultancyId;
	}

	public String getConsultancyfirmName() {
		return consultancyfirmName;
	}

	public void setConsultancyfirmName(String consultancyfirmName) {
		this.consultancyfirmName = consultancyfirmName;
	}

	public String getFirmType() {
		return firmType;
	}

	public void setFirmType(String firmType) {
		this.firmType = firmType;
	}

	public String getFirmpanNumber() {
		return firmpanNumber;
	}

	public void setFirmpanNumber(String firmpanNumber) {
		this.firmpanNumber = firmpanNumber;
	}

	public String getFirmgstNumber() {
		return firmgstNumber;
	}

	public void setFirmgstNumber(String firmgstNumber) {
		this.firmgstNumber = firmgstNumber;
	}

	public String getCheckPrintingName() {
		return checkPrintingName;
	}

	public void setCheckPrintingName(String checkPrintingName) {
		this.checkPrintingName = checkPrintingName;
	}

	public String getConsultancyAddress() {
		return consultancyAddress;
	}

	public void setConsultancyAddress(String consultancyAddress) {
		this.consultancyAddress = consultancyAddress;
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getStateId() {
		return stateId;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getLocationareaId() {
		return locationareaId;
	}

	public void setLocationareaId(String locationareaId) {
		this.locationareaId = locationareaId;
	}

	public String getAreaPincode() {
		return areaPincode;
	}

	public void setAreaPincode(String areaPincode) {
		this.areaPincode = areaPincode;
	}

	public String getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}

	public String getDesignationId() {
		return designationId;
	}

	public void setDesignationId(String designationId) {
		this.designationId = designationId;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getConsultancyBankName() {
		return consultancyBankName;
	}

	public void setConsultancyBankName(String consultancyBankName) {
		this.consultancyBankName = consultancyBankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getBankifscCode() {
		return bankifscCode;
	}

	public void setBankifscCode(String bankifscCode) {
		this.bankifscCode = bankifscCode;
	}

	public String getConsultancyBankacno() {
		return consultancyBankacno;
	}

	public void setConsultancyBankacno(String consultancyBankacno) {
		this.consultancyBankacno = consultancyBankacno;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}


}

package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "contractorpaidpayments")
public class ContractorPaidPayment {

	@Id
    private String contractorPaidPaymentId;
	private String contractorId;
	private String workOrderId;
	private long paidAmount;

	public ContractorPaidPayment() {}

	public ContractorPaidPayment(String contractorPaidPaymentId, String contractorId, String workOrderId,
			long paidAmount) {
		super();
		this.contractorPaidPaymentId = contractorPaidPaymentId;
		this.contractorId = contractorId;
		this.workOrderId = workOrderId;
		this.paidAmount = paidAmount;
	}

	public String getContractorPaidPaymentId() {
		return contractorPaidPaymentId;
	}

	public void setContractorPaidPaymentId(String contractorPaidPaymentId) {
		this.contractorPaidPaymentId = contractorPaidPaymentId;
	}

	public String getContractorId() {
		return contractorId;
	}

	public void setContractorId(String contractorId) {
		this.contractorId = contractorId;
	}

	public String getWorkOrderId() {
		return workOrderId;
	}

	public void setWorkOrderId(String workOrderId) {
		this.workOrderId = workOrderId;
	}

	public long getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(long paidAmount) {
		this.paidAmount = paidAmount;
	}
	
	
}

package com.realestate.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "customerpaymentreminderletterhistories")
public class CustomerPaymentReminderLetterHistory {

	@Id
	private String reminderHistoryId;
	private String bookingId;
	private String paymentschedulerId;
	private int reminderCount;
	private String status;
	
	private Date creationDate;
	private Date updateDate;
	private String userName;
	
	public CustomerPaymentReminderLetterHistory() {}

	public CustomerPaymentReminderLetterHistory(String reminderHistoryId, String bookingId, String paymentschedulerId,
			int reminderCount, String status, Date creationDate, Date updateDate, String userName) {
		super();
		this.reminderHistoryId = reminderHistoryId;
		this.bookingId = bookingId;
		this.paymentschedulerId = paymentschedulerId;
		this.reminderCount = reminderCount;
		this.status = status;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getReminderHistoryId() {
		return reminderHistoryId;
	}

	public void setReminderHistoryId(String reminderHistoryId) {
		this.reminderHistoryId = reminderHistoryId;
	}

	public String getBookingId() {
		return bookingId;
	}

	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}

	public String getPaymentschedulerId() {
		return paymentschedulerId;
	}

	public void setPaymentschedulerId(String paymentschedulerId) {
		this.paymentschedulerId = paymentschedulerId;
	}

	public int getReminderCount() {
		return reminderCount;
	}

	public void setReminderCount(int reminderCount) {
		this.reminderCount = reminderCount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}

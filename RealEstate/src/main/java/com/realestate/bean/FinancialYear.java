package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "financialyears")
//@CompoundIndexes({@CompoundIndex(name="financialyearIndex", unique= true, def="{'minimumBudget':1}")})
public class FinancialYear {

	@Id
    private String financialyearId;
    private String financialstartDate;
    private String financialendDate;
    private String creationDate;
    private String updateDate;
    private String userName;
    
    public FinancialYear() {}
	public FinancialYear(String financialyearId, String financialstartDate, String financialendDate,
			String creationDate, String updateDate, String userName) {
		super();
		this.financialyearId = financialyearId;
		this.financialstartDate = financialstartDate;
		this.financialendDate = financialendDate;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}
	public String getFinancialyearId() {
		return financialyearId;
	}
	public void setFinancialyearId(String financialyearId) {
		this.financialyearId = financialyearId;
	}
	public String getFinancialstartDate() {
		return financialstartDate;
	}
	public void setFinancialstartDate(String financialstartDate) {
		this.financialstartDate = financialstartDate;
	}
	public String getFinancialendDate() {
		return financialendDate;
	}
	public void setFinancialendDate(String financialendDate) {
		this.financialendDate = financialendDate;
	}
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
    
}

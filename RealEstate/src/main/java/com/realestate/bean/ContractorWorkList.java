package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "contractorworklists")
public class ContractorWorkList {


	@Id
    private String workId;
	private String projectId;
	private String buildingId;
    private String wingId;
    private String contractortypeId;
     
    private String status;
    
	private String creationDate;
	private String updateDate;
	private String userName;
	
    public ContractorWorkList()
    {}

	public ContractorWorkList(String workId, String projectId, String buildingId, String wingId,
			String contractortypeId, String status, String creationDate, String updateDate, String userName) {
		super();
		this.workId = workId;
		this.projectId = projectId;
		this.buildingId = buildingId;
		this.wingId = wingId;
		this.contractortypeId = contractortypeId;
		this.status = status;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getWorkId() {
		return workId;
	}

	public void setWorkId(String workId) {
		this.workId = workId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	public String getWingId() {
		return wingId;
	}

	public void setWingId(String wingId) {
		this.wingId = wingId;
	}

	public String getContractortypeId() {
		return contractortypeId;
	}

	public void setContractortypeId(String contractortypeId) {
		this.contractortypeId = contractortypeId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}

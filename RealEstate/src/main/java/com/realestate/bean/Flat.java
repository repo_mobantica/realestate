package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "flats")
//@CompoundIndexes({@CompoundIndex(name="flatIndex", unique= true, def="{'flatNumber':1}")})
public class Flat {


	@Id
    private String flatId;
    private String flatNumber;
    private String projectId;
    private String buildingId;
    private String wingId;
    private String floorId;
    private String flatfacingName;
    private String flatType;
    private Double carpetArea;
    private Double terraceArea;
    private Double balconyArea;
    private Double dryterraceArea;
    private Double flatArea; 
    
    private Double flatCostwithotfloorise;   
    private Double floorRise; 
    private Double flatCost;   
    private Double flatminimumCost;   
    private Double flatbasicCost; 
    private Double loading;
    private Double loadingpercentage;
    private Double flatAreawithLoadingInM;
    private Double flatAreawithLoadingInFt;
    
    private String flatstatus;
    private String creationDate;
    private String updateDate;
    private String userName;
    
    @Transient
    private String bookingName;
    
   /* @Transient
    private Double gst;
    @Transient
    private Double totalCost;
    @Transient
    private Double registeration;
    @Transient
    private Double stampDuty;
    @Transient
    private Double handlingCharge;*/
    
    public Flat()
    {
    	
    }

	public Flat(String flatId, String flatNumber, String projectId, String buildingId, String wingId,
			String floorId, String flatfacingName, String flatType, Double carpetArea, Double terraceArea,
			Double balconyArea, Double dryterraceArea, Double flatArea, Double flatCostwithotfloorise, Double floorRise,
			Double flatCost, Double flatminimumCost, Double flatbasicCost, Double loading, Double loadingpercentage,
			Double flatAreawithLoadingInM, Double flatAreawithLoadingInFt, String flatstatus, String creationDate,
			String updateDate, String userName) {
		super();
		this.flatId = flatId;
		this.flatNumber = flatNumber;
		this.projectId = projectId;
		this.buildingId = buildingId;
		this.wingId = wingId;
		this.floorId = floorId;
		this.flatfacingName = flatfacingName;
		this.flatType = flatType;
		this.carpetArea = carpetArea;
		this.terraceArea = terraceArea;
		this.balconyArea = balconyArea;
		this.dryterraceArea = dryterraceArea;
		this.flatArea = flatArea;
		this.flatCostwithotfloorise = flatCostwithotfloorise;
		this.floorRise = floorRise;
		this.flatCost = flatCost;
		this.flatminimumCost = flatminimumCost;
		this.flatbasicCost = flatbasicCost;
		this.loading = loading;
		this.loadingpercentage = loadingpercentage;
		this.flatAreawithLoadingInM = flatAreawithLoadingInM;
		this.flatAreawithLoadingInFt = flatAreawithLoadingInFt;
		this.flatstatus = flatstatus;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getFlatId() {
		return flatId;
	}

	public void setFlatId(String flatId) {
		this.flatId = flatId;
	}

	public String getFlatNumber() {
		return flatNumber;
	}

	public void setFlatNumber(String flatNumber) {
		this.flatNumber = flatNumber;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	public String getWingId() {
		return wingId;
	}

	public void setWingId(String wingId) {
		this.wingId = wingId;
	}

	public String getFloorId() {
		return floorId;
	}

	public void setFloorId(String floorId) {
		this.floorId = floorId;
	}

	public String getFlatfacingName() {
		return flatfacingName;
	}

	public void setFlatfacingName(String flatfacingName) {
		this.flatfacingName = flatfacingName;
	}

	public String getFlatType() {
		return flatType;
	}

	public void setFlatType(String flatType) {
		this.flatType = flatType;
	}

	public Double getCarpetArea() {
		return carpetArea;
	}

	public void setCarpetArea(Double carpetArea) {
		this.carpetArea = carpetArea;
	}

	public Double getTerraceArea() {
		return terraceArea;
	}

	public void setTerraceArea(Double terraceArea) {
		this.terraceArea = terraceArea;
	}

	public Double getBalconyArea() {
		return balconyArea;
	}

	public void setBalconyArea(Double balconyArea) {
		this.balconyArea = balconyArea;
	}

	public Double getDryterraceArea() {
		return dryterraceArea;
	}

	public void setDryterraceArea(Double dryterraceArea) {
		this.dryterraceArea = dryterraceArea;
	}

	public Double getFlatArea() {
		return flatArea;
	}

	public void setFlatArea(Double flatArea) {
		this.flatArea = flatArea;
	}

	public Double getFlatCostwithotfloorise() {
		return flatCostwithotfloorise;
	}

	public void setFlatCostwithotfloorise(Double flatCostwithotfloorise) {
		this.flatCostwithotfloorise = flatCostwithotfloorise;
	}

	public Double getFloorRise() {
		return floorRise;
	}

	public void setFloorRise(Double floorRise) {
		this.floorRise = floorRise;
	}

	public Double getFlatCost() {
		return flatCost;
	}

	public void setFlatCost(Double flatCost) {
		this.flatCost = flatCost;
	}

	public Double getFlatminimumCost() {
		return flatminimumCost;
	}

	public void setFlatminimumCost(Double flatminimumCost) {
		this.flatminimumCost = flatminimumCost;
	}

	public Double getFlatbasicCost() {
		return flatbasicCost;
	}

	public void setFlatbasicCost(Double flatbasicCost) {
		this.flatbasicCost = flatbasicCost;
	}

	public Double getLoading() {
		return loading;
	}

	public void setLoading(Double loading) {
		this.loading = loading;
	}

	public Double getLoadingpercentage() {
		return loadingpercentage;
	}

	public void setLoadingpercentage(Double loadingpercentage) {
		this.loadingpercentage = loadingpercentage;
	}

	public Double getFlatAreawithLoadingInM() {
		return flatAreawithLoadingInM;
	}

	public void setFlatAreawithLoadingInM(Double flatAreawithLoadingInM) {
		this.flatAreawithLoadingInM = flatAreawithLoadingInM;
	}

	public Double getFlatAreawithLoadingInFt() {
		return flatAreawithLoadingInFt;
	}

	public void setFlatAreawithLoadingInFt(Double flatAreawithLoadingInFt) {
		this.flatAreawithLoadingInFt = flatAreawithLoadingInFt;
	}

	public String getFlatstatus() {
		return flatstatus;
	}

	public void setFlatstatus(String flatstatus) {
		this.flatstatus = flatstatus;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getBookingName() {
		return bookingName;
	}

	public void setBookingName(String bookingName) {
		this.bookingName = bookingName;
	}

}

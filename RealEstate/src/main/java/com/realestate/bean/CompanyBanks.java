package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "companibanks")
public class CompanyBanks {

	@Id
	private String companyBankId;
	private String companyId;
	private String companyBankname;
	private String branchName;
	private String bankifscCode;
	private String companyBankacno;
	private String status;
	public CompanyBanks() {}
	public CompanyBanks(String companyBankId, String companyId, String companyBankname, String branchName,
			String bankifscCode, String companyBankacno, String status) {
		super();
		this.companyBankId = companyBankId;
		this.companyId = companyId;
		this.companyBankname = companyBankname;
		this.branchName = branchName;
		this.bankifscCode = bankifscCode;
		this.companyBankacno = companyBankacno;
		this.status = status;
	}
	public String getCompanyBankId() {
		return companyBankId;
	}
	public void setCompanyBankId(String companyBankId) {
		this.companyBankId = companyBankId;
	}
	public String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	public String getCompanyBankname() {
		return companyBankname;
	}
	public void setCompanyBankname(String companyBankname) {
		this.companyBankname = companyBankname;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getBankifscCode() {
		return bankifscCode;
	}
	public void setBankifscCode(String bankifscCode) {
		this.bankifscCode = bankifscCode;
	}
	public String getCompanyBankacno() {
		return companyBankacno;
	}
	public void setCompanyBankacno(String companyBankacno) {
		this.companyBankacno = companyBankacno;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	
}

package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "supplierbillpayments")
public class SupplierBillPayment {

	@Id
    private String supplierBillPaymentId;
	private String materialsPurchasedId;
	private String supplierId;
	private String storeId;
	
	private Double totalAmount;
	
	public SupplierBillPayment() {}

	public SupplierBillPayment(String supplierBillPaymentId, String materialsPurchasedId, String supplierId,
			String storeId, Double totalAmount) {
		super();
		this.supplierBillPaymentId = supplierBillPaymentId;
		this.materialsPurchasedId = materialsPurchasedId;
		this.supplierId = supplierId;
		this.storeId = storeId;
		this.totalAmount = totalAmount;
	}

	public String getSupplierBillPaymentId() {
		return supplierBillPaymentId;
	}

	public void setSupplierBillPaymentId(String supplierBillPaymentId) {
		this.supplierBillPaymentId = supplierBillPaymentId;
	}

	public String getMaterialsPurchasedId() {
		return materialsPurchasedId;
	}

	public void setMaterialsPurchasedId(String materialsPurchasedId) {
		this.materialsPurchasedId = materialsPurchasedId;
	}

	public String getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}
	
	
}

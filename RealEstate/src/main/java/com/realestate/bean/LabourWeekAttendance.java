package com.realestate.bean;


public class LabourWeekAttendance {

	private String attendanceId;
	private String labourId;
	private String labourName;
	
	private Double sunday;
	private Double monday;
	private Double tuesday;
	private Double wednesday;
	private Double thursday;
	private Double friday;
	private Double saturday;
	private Double totalThisWeek;
	
	public String getAttendanceId() {
		return attendanceId;
	}
	public void setAttendanceId(String attendanceId) {
		this.attendanceId = attendanceId;
	}
	public String getLabourId() {
		return labourId;
	}
	public void setLabourId(String labourId) {
		this.labourId = labourId;
	}
	public String getLabourName() {
		return labourName;
	}
	public void setLabourName(String labourName) {
		this.labourName = labourName;
	}
	public Double getSunday() {
		return sunday;
	}
	public void setSunday(Double sunday) {
		this.sunday = sunday;
	}
	public Double getMonday() {
		return monday;
	}
	public void setMonday(Double monday) {
		this.monday = monday;
	}
	public Double getTuesday() {
		return tuesday;
	}
	public void setTuesday(Double tuesday) {
		this.tuesday = tuesday;
	}
	public Double getWednesday() {
		return wednesday;
	}
	public void setWednesday(Double wednesday) {
		this.wednesday = wednesday;
	}
	public Double getThursday() {
		return thursday;
	}
	public void setThursday(Double thursday) {
		this.thursday = thursday;
	}
	public Double getFriday() {
		return friday;
	}
	public void setFriday(Double friday) {
		this.friday = friday;
	}
	public Double getSaturday() {
		return saturday;
	}
	public void setSaturday(Double saturday) {
		this.saturday = saturday;
	}
	public Double getTotalThisWeek() {
		return totalThisWeek;
	}
	public void setTotalThisWeek(Double totalThisWeek) {
		this.totalThisWeek = totalThisWeek;
	}
	
	
}

package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "contractorworklisthistories")
public class ContractorWorkListHistory {

	@Id
    private long workHistoryId;
	private String workId;
	private String contractortypeId;
	private String subcontractortypeId;
	private String workType;
	private String type;
	
	private Double unit;
	private Double workRate;
	private Double grandTotal;
	
	public ContractorWorkListHistory() {}

	public ContractorWorkListHistory(long workHistoryId, String workId, String contractortypeId,
			String subcontractortypeId, String workType, String type, Double unit, Double workRate, Double grandTotal) {
		super();
		this.workHistoryId = workHistoryId;
		this.workId = workId;
		this.contractortypeId = contractortypeId;
		this.subcontractortypeId = subcontractortypeId;
		this.workType = workType;
		this.type = type;
		this.unit = unit;
		this.workRate = workRate;
		this.grandTotal = grandTotal;
	}

	public long getWorkHistoryId() {
		return workHistoryId;
	}

	public void setWorkHistoryId(long workHistoryId) {
		this.workHistoryId = workHistoryId;
	}

	public String getWorkId() {
		return workId;
	}

	public void setWorkId(String workId) {
		this.workId = workId;
	}

	public String getContractortypeId() {
		return contractortypeId;
	}

	public void setContractortypeId(String contractortypeId) {
		this.contractortypeId = contractortypeId;
	}

	public String getSubcontractortypeId() {
		return subcontractortypeId;
	}

	public void setSubcontractortypeId(String subcontractortypeId) {
		this.subcontractortypeId = subcontractortypeId;
	}

	public String getWorkType() {
		return workType;
	}

	public void setWorkType(String workType) {
		this.workType = workType;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Double getUnit() {
		return unit;
	}

	public void setUnit(Double unit) {
		this.unit = unit;
	}

	public Double getWorkRate() {
		return workRate;
	}

	public void setWorkRate(Double workRate) {
		this.workRate = workRate;
	}

	public Double getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(Double grandTotal) {
		this.grandTotal = grandTotal;
	}

}

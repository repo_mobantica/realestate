package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "states")
//@CompoundIndexes({@CompoundIndex(name="stateIndex", unique= true, def="{'stateName':1}")})
public class State 
{
	@Id
	private String stateId;
	private String stateName;
	private String countryId;
    private String creationDate;
    private String updateDate;
    private String userName;
    
    public State() 
    {
    	
    }

	public State(String stateId, String stateName, String countryId, String creationDate, String updateDate,
			String userName) {
		super();
		this.stateId = stateId;
		this.stateName = stateName;
		this.countryId = countryId;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getStateId() {
		return stateId;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}

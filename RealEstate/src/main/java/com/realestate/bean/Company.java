package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "companies")
@CompoundIndexes({@CompoundIndex(name="companynameIndex", unique= true, def="{'companyName':1}")})
public class Company 
{
	@Id
	private String companyId;
	private String companyName;
	private String companyType;
	private String companyAddress;
	private String countryId;
	private String stateId;
	private String cityId;
	private String locationareaId;
	private String companyPincode;
	private String companyPhoneno;
	private String companyPanno;
	private String companyRegistrationno;
	private String companyTanno;
	private String companyPfno;
	private String companyGstno;
	
	private String companyFinancialyear;
	private String creationDate;
	private String updateDate;
	private String userName;
	
	public Company()
	{
		
	}

	public Company(String companyId, String companyName, String companyType, String companyAddress, String countryId,
			String stateId, String cityId, String locationareaId, String companyPincode, String companyPhoneno,
			String companyPanno, String companyRegistrationno, String companyTanno, String companyPfno,
			String companyGstno, String companyFinancialyear, String creationDate, String updateDate, String userName) {
		super();
		this.companyId = companyId;
		this.companyName = companyName;
		this.companyType = companyType;
		this.companyAddress = companyAddress;
		this.countryId = countryId;
		this.stateId = stateId;
		this.cityId = cityId;
		this.locationareaId = locationareaId;
		this.companyPincode = companyPincode;
		this.companyPhoneno = companyPhoneno;
		this.companyPanno = companyPanno;
		this.companyRegistrationno = companyRegistrationno;
		this.companyTanno = companyTanno;
		this.companyPfno = companyPfno;
		this.companyGstno = companyGstno;
		this.companyFinancialyear = companyFinancialyear;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyType() {
		return companyType;
	}

	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}

	public String getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getStateId() {
		return stateId;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getLocationareaId() {
		return locationareaId;
	}

	public void setLocationareaId(String locationareaId) {
		this.locationareaId = locationareaId;
	}

	public String getCompanyPincode() {
		return companyPincode;
	}

	public void setCompanyPincode(String companyPincode) {
		this.companyPincode = companyPincode;
	}

	public String getCompanyPhoneno() {
		return companyPhoneno;
	}

	public void setCompanyPhoneno(String companyPhoneno) {
		this.companyPhoneno = companyPhoneno;
	}

	public String getCompanyPanno() {
		return companyPanno;
	}

	public void setCompanyPanno(String companyPanno) {
		this.companyPanno = companyPanno;
	}

	public String getCompanyRegistrationno() {
		return companyRegistrationno;
	}

	public void setCompanyRegistrationno(String companyRegistrationno) {
		this.companyRegistrationno = companyRegistrationno;
	}

	public String getCompanyTanno() {
		return companyTanno;
	}

	public void setCompanyTanno(String companyTanno) {
		this.companyTanno = companyTanno;
	}

	public String getCompanyPfno() {
		return companyPfno;
	}

	public void setCompanyPfno(String companyPfno) {
		this.companyPfno = companyPfno;
	}

	public String getCompanyGstno() {
		return companyGstno;
	}

	public void setCompanyGstno(String companyGstno) {
		this.companyGstno = companyGstno;
	}

	public String getCompanyFinancialyear() {
		return companyFinancialyear;
	}

	public void setCompanyFinancialyear(String companyFinancialyear) {
		this.companyFinancialyear = companyFinancialyear;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	
}

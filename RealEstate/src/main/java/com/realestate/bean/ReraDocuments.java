package com.realestate.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "reradocuments")
public class ReraDocuments
{
   @Id
   private String documentId;
   private String projectId;
   private String documentName;
   private String extension;
   private Date   creationDate;
   private Date   updateDate;
	
   public ReraDocuments() 
   {

   }

   public ReraDocuments(String documentId, String projectId, String documentName, String extension, Date creationDate, Date updateDate) 
   {
		super();
		this.documentId = documentId;
		this.projectId = projectId;
		this.documentName = documentName;
		this.extension = extension;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
	}

	public String getDocumentId() {
		return documentId;
	}
	
	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}
	
	public String getProjectId() {
		return projectId;
	}
	
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	
	public String getDocumentName() {
		return documentName;
	}
	
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
	
	public String getExtension() {
		return extension;
	}
	
	public void setExtension(String extension) {
		this.extension = extension;
	}
	
	public Date getCreationDate() {
		return creationDate;
	}
	
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	public Date getUpdateDate() {
		return updateDate;
	}
	
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
}

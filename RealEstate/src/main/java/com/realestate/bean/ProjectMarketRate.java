package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="projectmarketrate")
public class ProjectMarketRate {

	@Id
	private String projectmarketrateId;
	private String projectId;
	private String year;
	private String subdivision;
	private String openGround;
	private String residentialHouse;
	private String office;

	private String shops;
	private String industrial;
	private String unit;
	private String userName;
	
	public ProjectMarketRate() {}

	public ProjectMarketRate(String projectmarketrateId, String projectId, String year, String subdivision,
			String openGround, String residentialHouse, String office, String shops, String industrial, String unit,
			String userName) {
		super();
		this.projectmarketrateId = projectmarketrateId;
		this.projectId = projectId;
		this.year = year;
		this.subdivision = subdivision;
		this.openGround = openGround;
		this.residentialHouse = residentialHouse;
		this.office = office;
		this.shops = shops;
		this.industrial = industrial;
		this.unit = unit;
		this.userName = userName;
	}

	public String getProjectmarketrateId() {
		return projectmarketrateId;
	}

	public void setProjectmarketrateId(String projectmarketrateId) {
		this.projectmarketrateId = projectmarketrateId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getSubdivision() {
		return subdivision;
	}

	public void setSubdivision(String subdivision) {
		this.subdivision = subdivision;
	}

	public String getOpenGround() {
		return openGround;
	}

	public void setOpenGround(String openGround) {
		this.openGround = openGround;
	}

	public String getResidentialHouse() {
		return residentialHouse;
	}

	public void setResidentialHouse(String residentialHouse) {
		this.residentialHouse = residentialHouse;
	}

	public String getOffice() {
		return office;
	}

	public void setOffice(String office) {
		this.office = office;
	}

	public String getShops() {
		return shops;
	}

	public void setShops(String shops) {
		this.shops = shops;
	}

	public String getIndustrial() {
		return industrial;
	}

	public void setIndustrial(String industrial) {
		this.industrial = industrial;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}

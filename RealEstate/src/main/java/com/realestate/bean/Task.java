package com.realestate.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.annotation.Transient;

@Document(collection = "tasks")
public class Task
{
	@Id
    private String taskId;
    private String projectId;
    private String buildingId;
    private String wingId;
    private String floorId;
    private String flatId;
    private String taskName;
    private String taskassignFrom;
    private String taskassignTo;
    private Date   taskassingDate;
    private Date   taskendDate;
    private String taskStatus;
    private String creationDate;
    private String updateDate;
    private String userName;
    
    @Transient
    private String  assignFrom;
    
    @Transient
    private String assignTo;

    @Transient
    private String taskassingDate1;
    
    @Transient
    private String taskendDate1;
    
    @Transient
    private String projectName;
    
    @Transient
    private String buildingName;
    
    @Transient
    private String  wingName;
    
    @Transient
    private String floorName;
    
    @Transient
    private String flatNumber;
    
    public Task() 
    {
    	
    }

	public Task(String taskId, String projectId, String buildingId, String wingId, String floorId, String flatId,
			String taskName, String taskassignFrom, String taskassignTo, Date taskassingDate, Date taskendDate,
			String taskStatus, String creationDate, String updateDate, String userName)
	{
		super();
		this.taskId = taskId;
		this.projectId = projectId;
		this.buildingId = buildingId;
		this.wingId = wingId;
		this.floorId = floorId;
		this.flatId = flatId;
		this.taskName = taskName;
		this.taskassignFrom = taskassignFrom;
		this.taskassignTo = taskassignTo;
		this.taskassingDate = taskassingDate;
		this.taskendDate = taskendDate;
		this.taskStatus = taskStatus;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}



	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	public String getWingId() {
		return wingId;
	}

	public void setWingId(String wingId) {
		this.wingId = wingId;
	}

	public String getFloorId() {
		return floorId;
	}

	public void setFloorId(String floorId) {
		this.floorId = floorId;
	}

	public String getFlatId() {
		return flatId;
	}

	public void setFlatId(String flatId) {
		this.flatId = flatId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getTaskassignFrom() {
		return taskassignFrom;
	}

	public void setTaskassignFrom(String taskassignFrom) {
		this.taskassignFrom = taskassignFrom;
	}

	public String getTaskassignTo() {
		return taskassignTo;
	}

	public void setTaskassignTo(String taskassignTo) {
		this.taskassignTo = taskassignTo;
	}

	public Date getTaskassingDate() {
		return taskassingDate;
	}

	public void setTaskassingDate(Date taskassingDate) {
		this.taskassingDate = taskassingDate;
	}

	public Date getTaskendDate() {
		return taskendDate;
	}

	public void setTaskendDate(Date taskendDate) {
		this.taskendDate = taskendDate;
	}

	public String getTaskStatus() {
		return taskStatus;
	}

	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getAssignFrom() {
		return assignFrom;
	}

	public void setAssignFrom(String assignFrom) {
		this.assignFrom = assignFrom;
	}

	public String getAssignTo() {
		return assignTo;
	}

	public void setAssignTo(String assignTo) {
		this.assignTo = assignTo;
	}

	public String getTaskassingDate1() {
		return taskassingDate1;
	}

	public void setTaskassingDate1(String taskassingDate1) {
		this.taskassingDate1 = taskassingDate1;
	}

	public String getTaskendDate1() {
		return taskendDate1;
	}

	public void setTaskendDate1(String taskendDate1) {
		this.taskendDate1 = taskendDate1;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getBuildingName() {
		return buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	public String getWingName() {
		return wingName;
	}

	public void setWingName(String wingName) {
		this.wingName = wingName;
	}

	public String getFloorName() {
		return floorName;
	}

	public void setFloorName(String floorName) {
		this.floorName = floorName;
	}

	public String getFlatNumber() {
		return flatNumber;
	}

	public void setFlatNumber(String flatNumber) {
		this.flatNumber = flatNumber;
	}

}

package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "storestockhistories")
public class StoreStockHistory {

	@Id
    private String storeStockHistoriId;
	private String materialsPurchasedId;
	private String storeId;
	private String itemId;
	private String itemUnit;
	private String itemSize;
	private String itemBrandName;
	private double itemQuantity;
	private String remark;
	private long noOfPieces;
	private String creationDate;
	
	public StoreStockHistory() {}

	public StoreStockHistory(String storeStockHistoriId, String materialsPurchasedId, String storeId, String itemId,
			String itemUnit, String itemSize, String itemBrandName, double itemQuantity, String remark, long noOfPieces,
			String creationDate) {
		super();
		this.storeStockHistoriId = storeStockHistoriId;
		this.materialsPurchasedId = materialsPurchasedId;
		this.storeId = storeId;
		this.itemId = itemId;
		this.itemUnit = itemUnit;
		this.itemSize = itemSize;
		this.itemBrandName = itemBrandName;
		this.itemQuantity = itemQuantity;
		this.remark = remark;
		this.noOfPieces = noOfPieces;
		this.creationDate = creationDate;
	}

	public String getStoreStockHistoriId() {
		return storeStockHistoriId;
	}

	public void setStoreStockHistoriId(String storeStockHistoriId) {
		this.storeStockHistoriId = storeStockHistoriId;
	}

	public String getMaterialsPurchasedId() {
		return materialsPurchasedId;
	}

	public void setMaterialsPurchasedId(String materialsPurchasedId) {
		this.materialsPurchasedId = materialsPurchasedId;
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getItemUnit() {
		return itemUnit;
	}

	public void setItemUnit(String itemUnit) {
		this.itemUnit = itemUnit;
	}

	public String getItemSize() {
		return itemSize;
	}

	public void setItemSize(String itemSize) {
		this.itemSize = itemSize;
	}

	public String getItemBrandName() {
		return itemBrandName;
	}

	public void setItemBrandName(String itemBrandName) {
		this.itemBrandName = itemBrandName;
	}

	public double getItemQuantity() {
		return itemQuantity;
	}

	public void setItemQuantity(double itemQuantity) {
		this.itemQuantity = itemQuantity;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public long getNoOfPieces() {
		return noOfPieces;
	}

	public void setNoOfPieces(long noOfPieces) {
		this.noOfPieces = noOfPieces;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}


}

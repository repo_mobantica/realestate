package com.realestate.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.annotation.Transient;
@Document(collection = "bookings")
//@CompoundIndexes({@CompoundIndex(name="bookingIndex", unique= true, def="{'bookingId':1}")})
public class Booking {

	@Id
	private String bookingId;
	private String enquiryId;
	private String bookingfirstname;
	private String bookingmiddlename;
	private String bookinglastname;
	/*private String bookingmaidenfirstname;
	private String bookingmaidenmiddlename;
	private String bookingmaidenlastname;*/
	private String bookingaddress;
	/*
	private String countryId;
	private String stateId;
	private String cityId;
	private String locationareaId;
	*/
	
	
	private String bookingPincode;
	private String bookingEmail;
	private String bookingmobileNumber1;
	private String bookingmobileNumber2;
	private String bookingOccupation;
	private String purposeOfFlat;
		
	
	private String projectId;
	private String buildingId;
	private String wingId;
	private String floorId;
	private String flatType;
	private String flatId;
	private String flatFacing;
	private Double flatareainSqFt;

	private Double flatCostwithotfloorise;
	private Double floorRise;
	
	private Double flatCost;
	private Double flatbasicCost;
	
	private String parkingFloorId;
	private String parkingZoneId;
	private String agentId;
	
	private Double infrastructureCharge;
	private Double aggreementValue1;
	private Double handlingCharges;
	private Double stampDutyPer;
	private Double registrationPer;
	private Double stampDuty1;
	private Double registrationCost1;
	private Double gstCost;
	private Double gstAmount1;
	private Double grandTotal1;
	private Double tds;
	private String bookingstatus;
	
	private Date creationDate;
	private Date updateDate;
	private String userName;
	
	
	@Transient
	private Double agentPer;
	@Transient
	private Double agentAmount;
	@Transient
	private Double agentPaidPayment;
	@Transient
	private String agentPaymentStatus;
	@Transient
	private String projectName;

	@Transient
	private String flatNumber;
	@Transient
	private String area;

	@Transient
	private String stringDate;
	public Booking()
	{
	}

	public Booking(String bookingId, String enquiryId, String bookingfirstname, String bookingmiddlename,
			String bookinglastname, String bookingaddress, String bookingPincode, String bookingEmail, String bookingmobileNumber1,
			String bookingmobileNumber2, String bookingOccupation, String purposeOfFlat,String projectId, String buildingId,
			String wingId, String floorId, String flatType, String flatId, String flatFacing,
			Double flatareainSqFt, Double flatCostwithotfloorise, Double floorRise, Double flatCost,
			Double flatbasicCost, String parkingFloorId, String parkingZoneId, String agentId,
			Double infrastructureCharge, Double aggreementValue1, Double handlingCharges, Double stampDutyPer,
			Double registrationPer, Double stampDuty1, Double registrationCost1, Double gstCost, Double gstAmount1,
			Double grandTotal1, Double tds, String bookingstatus, Date creationDate, Date updateDate,
			String userName) {
		super();
		this.bookingId = bookingId;
		this.enquiryId = enquiryId;
		this.bookingfirstname = bookingfirstname;
		this.bookingmiddlename = bookingmiddlename;
		this.bookinglastname = bookinglastname;
		this.bookingaddress = bookingaddress;
		this.bookingPincode = bookingPincode;
		this.bookingEmail = bookingEmail;
		this.bookingmobileNumber1 = bookingmobileNumber1;
		this.bookingmobileNumber2 = bookingmobileNumber2;
		this.bookingOccupation = bookingOccupation;
		this.purposeOfFlat=purposeOfFlat;
		this.projectId = projectId;
		this.buildingId = buildingId;
		this.wingId = wingId;
		this.floorId = floorId;
		this.flatType = flatType;
		this.flatId = flatId;
		this.flatFacing = flatFacing;
		this.flatareainSqFt = flatareainSqFt;
		this.flatCostwithotfloorise = flatCostwithotfloorise;
		this.floorRise = floorRise;
		this.flatCost = flatCost;
		this.flatbasicCost = flatbasicCost;
		this.parkingFloorId = parkingFloorId;
		this.parkingZoneId = parkingZoneId;
		this.agentId = agentId;
		this.infrastructureCharge = infrastructureCharge;
		this.aggreementValue1 = aggreementValue1;
		this.handlingCharges = handlingCharges;
		this.stampDutyPer = stampDutyPer;
		this.registrationPer = registrationPer;
		this.stampDuty1 = stampDuty1;
		this.registrationCost1 = registrationCost1;
		this.gstCost = gstCost;
		this.gstAmount1 = gstAmount1;
		this.grandTotal1 = grandTotal1;
		this.tds = tds;
		this.bookingstatus = bookingstatus;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getBookingId() {
		return bookingId;
	}

	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}

	public String getEnquiryId() {
		return enquiryId;
	}

	public void setEnquiryId(String enquiryId) {
		this.enquiryId = enquiryId;
	}

	public String getBookingfirstname() {
		return bookingfirstname;
	}

	public void setBookingfirstname(String bookingfirstname) {
		this.bookingfirstname = bookingfirstname;
	}

	public String getBookingmiddlename() {
		return bookingmiddlename;
	}

	public void setBookingmiddlename(String bookingmiddlename) {
		this.bookingmiddlename = bookingmiddlename;
	}

	public String getBookinglastname() {
		return bookinglastname;
	}

	public void setBookinglastname(String bookinglastname) {
		this.bookinglastname = bookinglastname;
	}

	public String getBookingaddress() {
		return bookingaddress;
	}

	public void setBookingaddress(String bookingaddress) {
		this.bookingaddress = bookingaddress;
	}


	public String getBookingPincode() {
		return bookingPincode;
	}

	public void setBookingPincode(String bookingPincode) {
		this.bookingPincode = bookingPincode;
	}

	public String getBookingEmail() {
		return bookingEmail;
	}

	public void setBookingEmail(String bookingEmail) {
		this.bookingEmail = bookingEmail;
	}

	public String getBookingmobileNumber1() {
		return bookingmobileNumber1;
	}

	public void setBookingmobileNumber1(String bookingmobileNumber1) {
		this.bookingmobileNumber1 = bookingmobileNumber1;
	}

	public String getBookingmobileNumber2() {
		return bookingmobileNumber2;
	}

	public void setBookingmobileNumber2(String bookingmobileNumber2) {
		this.bookingmobileNumber2 = bookingmobileNumber2;
	}

	public String getBookingOccupation() {
		return bookingOccupation;
	}

	public void setBookingOccupation(String bookingOccupation) {
		this.bookingOccupation = bookingOccupation;
	}

	public String getPurposeOfFlat() {
		return purposeOfFlat;
	}

	public void setPurposeOfFlat(String purposeOfFlat) {
		this.purposeOfFlat = purposeOfFlat;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	public String getWingId() {
		return wingId;
	}

	public void setWingId(String wingId) {
		this.wingId = wingId;
	}

	public String getFloorId() {
		return floorId;
	}

	public void setFloorId(String floorId) {
		this.floorId = floorId;
	}

	public String getFlatType() {
		return flatType;
	}

	public void setFlatType(String flatType) {
		this.flatType = flatType;
	}

	public String getFlatId() {
		return flatId;
	}

	public void setFlatId(String flatId) {
		this.flatId = flatId;
	}

	public String getFlatFacing() {
		return flatFacing;
	}

	public void setFlatFacing(String flatFacing) {
		this.flatFacing = flatFacing;
	}

	public Double getFlatareainSqFt() {
		return flatareainSqFt;
	}

	public void setFlatareainSqFt(Double flatareainSqFt) {
		this.flatareainSqFt = flatareainSqFt;
	}

	public Double getFlatCostwithotfloorise() {
		return flatCostwithotfloorise;
	}

	public void setFlatCostwithotfloorise(Double flatCostwithotfloorise) {
		this.flatCostwithotfloorise = flatCostwithotfloorise;
	}

	public Double getFloorRise() {
		return floorRise;
	}

	public void setFloorRise(Double floorRise) {
		this.floorRise = floorRise;
	}

	public Double getFlatCost() {
		return flatCost;
	}

	public void setFlatCost(Double flatCost) {
		this.flatCost = flatCost;
	}

	public Double getFlatbasicCost() {
		return flatbasicCost;
	}

	public void setFlatbasicCost(Double flatbasicCost) {
		this.flatbasicCost = flatbasicCost;
	}

	public String getParkingFloorId() {
		return parkingFloorId;
	}

	public void setParkingFloorId(String parkingFloorId) {
		this.parkingFloorId = parkingFloorId;
	}

	public String getParkingZoneId() {
		return parkingZoneId;
	}

	public void setParkingZoneId(String parkingZoneId) {
		this.parkingZoneId = parkingZoneId;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public Double getInfrastructureCharge() {
		return infrastructureCharge;
	}

	public void setInfrastructureCharge(Double infrastructureCharge) {
		this.infrastructureCharge = infrastructureCharge;
	}

	public Double getAggreementValue1() {
		return aggreementValue1;
	}

	public void setAggreementValue1(Double aggreementValue1) {
		this.aggreementValue1 = aggreementValue1;
	}

	public Double getHandlingCharges() {
		return handlingCharges;
	}

	public void setHandlingCharges(Double handlingCharges) {
		this.handlingCharges = handlingCharges;
	}

	public Double getStampDutyPer() {
		return stampDutyPer;
	}

	public void setStampDutyPer(Double stampDutyPer) {
		this.stampDutyPer = stampDutyPer;
	}

	public Double getRegistrationPer() {
		return registrationPer;
	}

	public void setRegistrationPer(Double registrationPer) {
		this.registrationPer = registrationPer;
	}

	public Double getStampDuty1() {
		return stampDuty1;
	}

	public void setStampDuty1(Double stampDuty1) {
		this.stampDuty1 = stampDuty1;
	}

	public Double getRegistrationCost1() {
		return registrationCost1;
	}

	public void setRegistrationCost1(Double registrationCost1) {
		this.registrationCost1 = registrationCost1;
	}

	public Double getGstCost() {
		return gstCost;
	}

	public void setGstCost(Double gstCost) {
		this.gstCost = gstCost;
	}

	public Double getGstAmount1() {
		return gstAmount1;
	}

	public void setGstAmount1(Double gstAmount1) {
		this.gstAmount1 = gstAmount1;
	}

	public Double getGrandTotal1() {
		return grandTotal1;
	}

	public void setGrandTotal1(Double grandTotal1) {
		this.grandTotal1 = grandTotal1;
	}

	public Double getTds() {
		return tds;
	}

	public void setTds(Double tds) {
		this.tds = tds;
	}

	public String getBookingstatus() {
		return bookingstatus;
	}

	public void setBookingstatus(String bookingstatus) {
		this.bookingstatus = bookingstatus;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Double getAgentPer() {
		return agentPer;
	}

	public void setAgentPer(Double agentPer) {
		this.agentPer = agentPer;
	}

	public Double getAgentAmount() {
		return agentAmount;
	}

	public void setAgentAmount(Double agentAmount) {
		this.agentAmount = agentAmount;
	}

	public Double getAgentPaidPayment() {
		return agentPaidPayment;
	}

	public void setAgentPaidPayment(Double agentPaidPayment) {
		this.agentPaidPayment = agentPaidPayment;
	}

	public String getAgentPaymentStatus() {
		return agentPaymentStatus;
	}

	public void setAgentPaymentStatus(String agentPaymentStatus) {
		this.agentPaymentStatus = agentPaymentStatus;
	}

	public String getFlatNumber() {
		return flatNumber;
	}

	public void setFlatNumber(String flatNumber) {
		this.flatNumber = flatNumber;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getStringDate() {
		return stringDate;
	}

	public void setStringDate(String stringDate) {
		this.stringDate = stringDate;
	}

}

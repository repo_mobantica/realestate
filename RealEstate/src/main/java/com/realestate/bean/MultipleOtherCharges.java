package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="multipleothercharges")
public class MultipleOtherCharges 
{
	@Id
	private int otherChargesId;
	private String chargesId;
	private String bookingId;
	private String chargesDescription;
	private double amount;
	
	public MultipleOtherCharges() 
	{

	}

	public MultipleOtherCharges(int otherChargesId, String chargesId, String bookingId, String chargesDescription, double amount) 
	{
		super();
		this.otherChargesId = otherChargesId;
		this.chargesId = chargesId;
		this.bookingId = bookingId;
		this.chargesDescription = chargesDescription;
		this.amount = amount;
	}

	public int getOtherChargesId() {
		return otherChargesId;
	}

	public void setOtherChargesId(int otherChargesId) {
		this.otherChargesId = otherChargesId;
	}

	public String getChargesId() {
		return chargesId;
	}

	public void setChargesId(String chargesId) {
		this.chargesId = chargesId;
	}

	public String getBookingId() {
		return bookingId;
	}

	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}

	public String getChargesDescription() {
		return chargesDescription;
	}

	public void setChargesDescription(String chargesDescription) {
		this.chargesDescription = chargesDescription;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

}

package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="stocks")
public class Stock {

 	@Id
 	private String stockId;
    private String itemId;
    private String storeId;
    
    private double minimumStock;
    private double currentStock;
    private long noOfPieces;
    
    //private String brandName;
    private String itemStatus;
    private String creationDate;
    private String updateDate;
    private String userName;
    
    @Transient
    private String itemName;
    @Transient
    private String suppliertypeId;
    @Transient
    private String subsuppliertypeId;
    @Transient
    private Double itemQuantity;
    
    
    public Stock() {}

	public Stock(String stockId, String itemId, String storeId, double minimumStock, double currentStock,
			long noOfPieces, String itemStatus, String creationDate, String updateDate, String userName) {
		super();
		this.stockId = stockId;
		this.itemId = itemId;
		this.storeId = storeId;
		this.minimumStock = minimumStock;
		this.currentStock = currentStock;
		this.noOfPieces = noOfPieces;
		this.itemStatus = itemStatus;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getStockId() {
		return stockId;
	}

	public void setStockId(String stockId) {
		this.stockId = stockId;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public double getMinimumStock() {
		return minimumStock;
	}

	public void setMinimumStock(double minimumStock) {
		this.minimumStock = minimumStock;
	}

	public double getCurrentStock() {
		return currentStock;
	}

	public void setCurrentStock(double currentStock) {
		this.currentStock = currentStock;
	}

	public long getNoOfPieces() {
		return noOfPieces;
	}

	public void setNoOfPieces(long noOfPieces) {
		this.noOfPieces = noOfPieces;
	}

	public String getItemStatus() {
		return itemStatus;
	}

	public void setItemStatus(String itemStatus) {
		this.itemStatus = itemStatus;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getSuppliertypeId() {
		return suppliertypeId;
	}

	public void setSuppliertypeId(String suppliertypeId) {
		this.suppliertypeId = suppliertypeId;
	}

	public String getSubsuppliertypeId() {
		return subsuppliertypeId;
	}

	public void setSubsuppliertypeId(String subsuppliertypeId) {
		this.subsuppliertypeId = subsuppliertypeId;
	}

	public Double getItemQuantity() {
		return itemQuantity;
	}

	public void setItemQuantity(Double itemQuantity) {
		this.itemQuantity = itemQuantity;
	}

	
}

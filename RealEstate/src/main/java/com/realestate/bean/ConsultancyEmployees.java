package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "architecturalemployees")
public class ConsultancyEmployees {

	@Id
	private long consultancyEmployeeId;
	private String consultancyId;
	private String designationId;
	private String employeeName;
	private String employeeEmailId;
	private String employeeMobileNo;
	
	public ConsultancyEmployees() {}

	public ConsultancyEmployees(long consultancyEmployeeId, String consultancyId, String designationId,
			String employeeName, String employeeEmailId, String employeeMobileNo) {
		super();
		this.consultancyEmployeeId = consultancyEmployeeId;
		this.consultancyId = consultancyId;
		this.designationId = designationId;
		this.employeeName = employeeName;
		this.employeeEmailId = employeeEmailId;
		this.employeeMobileNo = employeeMobileNo;
	}

	public long getConsultancyEmployeeId() {
		return consultancyEmployeeId;
	}

	public void setConsultancyEmployeeId(long consultancyEmployeeId) {
		this.consultancyEmployeeId = consultancyEmployeeId;
	}

	public String getConsultancyId() {
		return consultancyId;
	}

	public void setConsultancyId(String consultancyId) {
		this.consultancyId = consultancyId;
	}

	public String getDesignationId() {
		return designationId;
	}

	public void setDesignationId(String designationId) {
		this.designationId = designationId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getEmployeeEmailId() {
		return employeeEmailId;
	}

	public void setEmployeeEmailId(String employeeEmailId) {
		this.employeeEmailId = employeeEmailId;
	}

	public String getEmployeeMobileNo() {
		return employeeMobileNo;
	}

	public void setEmployeeMobileNo(String employeeMobileNo) {
		this.employeeMobileNo = employeeMobileNo;
	}

}

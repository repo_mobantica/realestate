package com.realestate.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="customerotherpaymentdetails")
public class CustomerOtherPaymentDetails
{
  @Id
  private String paymentId;
  private String bookingId;
  private long totalPaidMaintainanceAmt;
  private long totalPaidHandlingCharges;
  private long totalPaidExtraCharges;
  private String amountStatus;
  
  private Date creationDate;
  private Date updateDate;
  
  public CustomerOtherPaymentDetails() 
  {

  }

  public CustomerOtherPaymentDetails(String paymentId, String bookingId, long totalPaidMaintainanceAmt, long totalPaidHandlingCharges, long totalPaidExtraCharges, String amountStatus, Date creationDate, Date updateDate) 
  {
	super();
	this.paymentId = paymentId;
	this.bookingId = bookingId;
	this.totalPaidMaintainanceAmt = totalPaidMaintainanceAmt;
	this.totalPaidHandlingCharges = totalPaidHandlingCharges;
	this.totalPaidExtraCharges = totalPaidExtraCharges;
	this.amountStatus = amountStatus;
	this.creationDate = creationDate;
	this.updateDate = updateDate;
  }

	public String getPaymentId() {
		return paymentId;
	}
	
	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}
	
	public String getBookingId() {
		return bookingId;
	}
	
	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}
	
	public long getTotalPaidMaintainanceAmt() {
		return totalPaidMaintainanceAmt;
	}
	
	public void setTotalPaidMaintainanceAmt(long totalPaidMaintainanceAmt) {
		this.totalPaidMaintainanceAmt = totalPaidMaintainanceAmt;
	}
	
	public long getTotalPaidHandlingCharges() {
		return totalPaidHandlingCharges;
	}
	
	public void setTotalPaidHandlingCharges(long totalPaidHandlingCharges) {
		this.totalPaidHandlingCharges = totalPaidHandlingCharges;
	}
	
	public long getTotalPaidExtraCharges() {
		return totalPaidExtraCharges;
	}
	
	public void setTotalPaidExtraCharges(long totalPaidExtraCharges) {
		this.totalPaidExtraCharges = totalPaidExtraCharges;
	}
	
	public String getAmountStatus() {
		return amountStatus;
	}
	
	public void setAmountStatus(String amountStatus) {
		this.amountStatus = amountStatus;
	}
	
	public Date getCreationDate() {
		return creationDate;
	}
	
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	public Date getUpdateDate() {
		return updateDate;
	}
	
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
}

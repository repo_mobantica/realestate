package com.realestate.bean;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="enquiryfollowups")

public class EnquiryFollowUp 
{
	private String enquiryId;
	private String followupDate;
	private String enqRemark;
	private String enqStatus;
	private String followupTakenBy;
	
	private Date creationDate;
	private String userName;
	
	
	public EnquiryFollowUp()
	{
		
	}


	public EnquiryFollowUp(String enquiryId, String followupDate, String enqRemark, String enqStatus, String followupTakenBy, Date creationDate, String userName) 
	{
		super();
		this.enquiryId = enquiryId;
		this.followupDate = followupDate;
		this.enqRemark = enqRemark;
		this.enqStatus = enqStatus;
		this.followupTakenBy = followupTakenBy;
		this.creationDate = creationDate;
		this.userName = userName;
	}

	
	public String getEnquiryId() 
	{
		return enquiryId;
	}


	public void setEnquiryId(String enquiryId)
	{
		this.enquiryId = enquiryId;
	}


	public String getFollowupDate() 
	{
		return followupDate;
	}


	public void setFollowupDate(String followupDate) 
	{
		this.followupDate = followupDate;
	}


	public String getEnqRemark() 
	{
		return enqRemark;
	}


	public void setEnqRemark(String enqRemark) 
	{
		this.enqRemark = enqRemark;
	}


	public String getEnqStatus()
	{
		return enqStatus;
	}


	public void setEnqStatus(String enqStatus)
	{
		this.enqStatus = enqStatus;
	}


	public String getFollowupTakenBy()
	{
		return followupTakenBy;
	}


	public void setFollowupTakenBy(String followupTakenBy) 
	{
		this.followupTakenBy = followupTakenBy;
	}


	public Date getCreationDate() 
	{
		return creationDate;
	}


	public void setCreationDate(Date creationDate) 
	{
		this.creationDate = creationDate;
	}


	public String getUserName() 
	{
		return userName;
	}


	public void setUserName(String userName)
	{
		this.userName = userName;
	}
	
}

package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "flatpossessionchecklist")
public class FlatPossessionCheckList {

	@Id
	private String possessionId;
	private String bookingId;
	
	private String allWallCracksFilledUpNeatly;
	private String allWaterPatchesremovedNeatly;
	private String allFanHooksInPosition;
	private String goodOverallFinishingOfWall;
	private String flooringInUniformLevel;
	private String dadoTilesAreaProperlyFinishedandJointFilled;
	private String dadoTilesAreProperLineAndInRightAngle;
	private String noCrackedTilesOnTheWall;
	private String properSlopehasbeenToBathFlooring;
	private String properSlopeGivenToTerraceOutlet;
	private String kitchenPlatformProperlyFittedallJointsFilled;
	private String noLeakagesIntheSinkOftheKitchen;
	private String wallBelowKitchenOttaProperlyFinished;
	private String allDoorsAndWindowOpenandShutProperly;
	
	private String allLocksTowerBoltsStoppersHingesOperateWell;
	private String allGlassToWindowProperlyFixedAndOperationWellAndCleaned;
	private String namePlateFixedOnMainDoor;
	private String msFoldingDoorInTerraceIsOperatingProperly;
	private String eyePieceFittingTotheMainDoor;
	private String allDoorFittingAndMortiseLocksareProperlyFixed;
	private String allDoorMagneticCatcherareProperlyFixed;
	private String allFittingareFreePaintSpots;
	private String cpFittingFixedProperly;
	private String noLeakagesInCPFittings;
	private String hotAndColdMixerOparatesProperly;
	private String flushTankcanEasilyOprate;
	private String ewcCoverProperlyFitted;
	private String gapBetwwenWallTilesandWashBasinFilled;
	private String allSanitaryFittingareNeatlyCleaned;
	private String washbasinIsProperlyFittedAndDoesNotMove;
	
	private String noSanitaryFittingsareCracked;
	private String bothTapsInKitchenOperative;
	private String electricMeterConnectedTotheFlat;
	private String allSwitchesOperatedProperly;
	private String mcbAnddcbProperlyFixed;
	private String colorOfAllSwitchesIsUniform;
	private String allswitchboardsareProperlyScrewed;
	private String paintingOfallWallsAndCeilingsIsProperlyDone;
	private String oilPaintingOfWindowGrillsAndTerraceRailingDoneProperly;
	
	private String creationDate;
	private String updateDate;
	private String userName;
	
	public FlatPossessionCheckList()
	{}

	public FlatPossessionCheckList(String possessionId, String bookingId, String allWallCracksFilledUpNeatly,
			String allWaterPatchesremovedNeatly, String allFanHooksInPosition, String goodOverallFinishingOfWall,
			String flooringInUniformLevel, String dadoTilesAreaProperlyFinishedandJointFilled,
			String dadoTilesAreProperLineAndInRightAngle, String noCrackedTilesOnTheWall,
			String properSlopehasbeenToBathFlooring, String properSlopeGivenToTerraceOutlet,
			String kitchenPlatformProperlyFittedallJointsFilled, String noLeakagesIntheSinkOftheKitchen,
			String wallBelowKitchenOttaProperlyFinished, String allDoorsAndWindowOpenandShutProperly,
			String allLocksTowerBoltsStoppersHingesOperateWell,
			String allGlassToWindowProperlyFixedAndOperationWellAndCleaned, String namePlateFixedOnMainDoor,
			String msFoldingDoorInTerraceIsOperatingProperly, String eyePieceFittingTotheMainDoor,
			String allDoorFittingAndMortiseLocksareProperlyFixed, String allDoorMagneticCatcherareProperlyFixed,
			String allFittingareFreePaintSpots, String cpFittingFixedProperly, String noLeakagesInCPFittings,
			String hotAndColdMixerOparatesProperly, String flushTankcanEasilyOprate, String ewcCoverProperlyFitted,
			String gapBetwwenWallTilesandWashBasinFilled, String allSanitaryFittingareNeatlyCleaned,
			String washbasinIsProperlyFittedAndDoesNotMove, String noSanitaryFittingsareCracked,
			String bothTapsInKitchenOperative, String electricMeterConnectedTotheFlat,
			String allSwitchesOperatedProperly, String mcbAnddcbProperlyFixed, String colorOfAllSwitchesIsUniform,
			String allswitchboardsareProperlyScrewed, String paintingOfallWallsAndCeilingsIsProperlyDone,
			String oilPaintingOfWindowGrillsAndTerraceRailingDoneProperly, String creationDate, String updateDate,
			String userName) {
		super();
		this.possessionId = possessionId;
		this.bookingId = bookingId;
		this.allWallCracksFilledUpNeatly = allWallCracksFilledUpNeatly;
		this.allWaterPatchesremovedNeatly = allWaterPatchesremovedNeatly;
		this.allFanHooksInPosition = allFanHooksInPosition;
		this.goodOverallFinishingOfWall = goodOverallFinishingOfWall;
		this.flooringInUniformLevel = flooringInUniformLevel;
		this.dadoTilesAreaProperlyFinishedandJointFilled = dadoTilesAreaProperlyFinishedandJointFilled;
		this.dadoTilesAreProperLineAndInRightAngle = dadoTilesAreProperLineAndInRightAngle;
		this.noCrackedTilesOnTheWall = noCrackedTilesOnTheWall;
		this.properSlopehasbeenToBathFlooring = properSlopehasbeenToBathFlooring;
		this.properSlopeGivenToTerraceOutlet = properSlopeGivenToTerraceOutlet;
		this.kitchenPlatformProperlyFittedallJointsFilled = kitchenPlatformProperlyFittedallJointsFilled;
		this.noLeakagesIntheSinkOftheKitchen = noLeakagesIntheSinkOftheKitchen;
		this.wallBelowKitchenOttaProperlyFinished = wallBelowKitchenOttaProperlyFinished;
		this.allDoorsAndWindowOpenandShutProperly = allDoorsAndWindowOpenandShutProperly;
		this.allLocksTowerBoltsStoppersHingesOperateWell = allLocksTowerBoltsStoppersHingesOperateWell;
		this.allGlassToWindowProperlyFixedAndOperationWellAndCleaned = allGlassToWindowProperlyFixedAndOperationWellAndCleaned;
		this.namePlateFixedOnMainDoor = namePlateFixedOnMainDoor;
		this.msFoldingDoorInTerraceIsOperatingProperly = msFoldingDoorInTerraceIsOperatingProperly;
		this.eyePieceFittingTotheMainDoor = eyePieceFittingTotheMainDoor;
		this.allDoorFittingAndMortiseLocksareProperlyFixed = allDoorFittingAndMortiseLocksareProperlyFixed;
		this.allDoorMagneticCatcherareProperlyFixed = allDoorMagneticCatcherareProperlyFixed;
		this.allFittingareFreePaintSpots = allFittingareFreePaintSpots;
		this.cpFittingFixedProperly = cpFittingFixedProperly;
		this.noLeakagesInCPFittings = noLeakagesInCPFittings;
		this.hotAndColdMixerOparatesProperly = hotAndColdMixerOparatesProperly;
		this.flushTankcanEasilyOprate = flushTankcanEasilyOprate;
		this.ewcCoverProperlyFitted = ewcCoverProperlyFitted;
		this.gapBetwwenWallTilesandWashBasinFilled = gapBetwwenWallTilesandWashBasinFilled;
		this.allSanitaryFittingareNeatlyCleaned = allSanitaryFittingareNeatlyCleaned;
		this.washbasinIsProperlyFittedAndDoesNotMove = washbasinIsProperlyFittedAndDoesNotMove;
		this.noSanitaryFittingsareCracked = noSanitaryFittingsareCracked;
		this.bothTapsInKitchenOperative = bothTapsInKitchenOperative;
		this.electricMeterConnectedTotheFlat = electricMeterConnectedTotheFlat;
		this.allSwitchesOperatedProperly = allSwitchesOperatedProperly;
		this.mcbAnddcbProperlyFixed = mcbAnddcbProperlyFixed;
		this.colorOfAllSwitchesIsUniform = colorOfAllSwitchesIsUniform;
		this.allswitchboardsareProperlyScrewed = allswitchboardsareProperlyScrewed;
		this.paintingOfallWallsAndCeilingsIsProperlyDone = paintingOfallWallsAndCeilingsIsProperlyDone;
		this.oilPaintingOfWindowGrillsAndTerraceRailingDoneProperly = oilPaintingOfWindowGrillsAndTerraceRailingDoneProperly;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getPossessionId() {
		return possessionId;
	}

	public void setPossessionId(String possessionId) {
		this.possessionId = possessionId;
	}

	public String getBookingId() {
		return bookingId;
	}

	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}

	public String getAllWallCracksFilledUpNeatly() {
		return allWallCracksFilledUpNeatly;
	}

	public void setAllWallCracksFilledUpNeatly(String allWallCracksFilledUpNeatly) {
		this.allWallCracksFilledUpNeatly = allWallCracksFilledUpNeatly;
	}

	public String getAllWaterPatchesremovedNeatly() {
		return allWaterPatchesremovedNeatly;
	}

	public void setAllWaterPatchesremovedNeatly(String allWaterPatchesremovedNeatly) {
		this.allWaterPatchesremovedNeatly = allWaterPatchesremovedNeatly;
	}

	public String getAllFanHooksInPosition() {
		return allFanHooksInPosition;
	}

	public void setAllFanHooksInPosition(String allFanHooksInPosition) {
		this.allFanHooksInPosition = allFanHooksInPosition;
	}

	public String getGoodOverallFinishingOfWall() {
		return goodOverallFinishingOfWall;
	}

	public void setGoodOverallFinishingOfWall(String goodOverallFinishingOfWall) {
		this.goodOverallFinishingOfWall = goodOverallFinishingOfWall;
	}

	public String getFlooringInUniformLevel() {
		return flooringInUniformLevel;
	}

	public void setFlooringInUniformLevel(String flooringInUniformLevel) {
		this.flooringInUniformLevel = flooringInUniformLevel;
	}

	public String getDadoTilesAreaProperlyFinishedandJointFilled() {
		return dadoTilesAreaProperlyFinishedandJointFilled;
	}

	public void setDadoTilesAreaProperlyFinishedandJointFilled(String dadoTilesAreaProperlyFinishedandJointFilled) {
		this.dadoTilesAreaProperlyFinishedandJointFilled = dadoTilesAreaProperlyFinishedandJointFilled;
	}

	public String getDadoTilesAreProperLineAndInRightAngle() {
		return dadoTilesAreProperLineAndInRightAngle;
	}

	public void setDadoTilesAreProperLineAndInRightAngle(String dadoTilesAreProperLineAndInRightAngle) {
		this.dadoTilesAreProperLineAndInRightAngle = dadoTilesAreProperLineAndInRightAngle;
	}

	public String getNoCrackedTilesOnTheWall() {
		return noCrackedTilesOnTheWall;
	}

	public void setNoCrackedTilesOnTheWall(String noCrackedTilesOnTheWall) {
		this.noCrackedTilesOnTheWall = noCrackedTilesOnTheWall;
	}

	public String getProperSlopehasbeenToBathFlooring() {
		return properSlopehasbeenToBathFlooring;
	}

	public void setProperSlopehasbeenToBathFlooring(String properSlopehasbeenToBathFlooring) {
		this.properSlopehasbeenToBathFlooring = properSlopehasbeenToBathFlooring;
	}

	public String getProperSlopeGivenToTerraceOutlet() {
		return properSlopeGivenToTerraceOutlet;
	}

	public void setProperSlopeGivenToTerraceOutlet(String properSlopeGivenToTerraceOutlet) {
		this.properSlopeGivenToTerraceOutlet = properSlopeGivenToTerraceOutlet;
	}

	public String getKitchenPlatformProperlyFittedallJointsFilled() {
		return kitchenPlatformProperlyFittedallJointsFilled;
	}

	public void setKitchenPlatformProperlyFittedallJointsFilled(String kitchenPlatformProperlyFittedallJointsFilled) {
		this.kitchenPlatformProperlyFittedallJointsFilled = kitchenPlatformProperlyFittedallJointsFilled;
	}

	public String getNoLeakagesIntheSinkOftheKitchen() {
		return noLeakagesIntheSinkOftheKitchen;
	}

	public void setNoLeakagesIntheSinkOftheKitchen(String noLeakagesIntheSinkOftheKitchen) {
		this.noLeakagesIntheSinkOftheKitchen = noLeakagesIntheSinkOftheKitchen;
	}

	public String getWallBelowKitchenOttaProperlyFinished() {
		return wallBelowKitchenOttaProperlyFinished;
	}

	public void setWallBelowKitchenOttaProperlyFinished(String wallBelowKitchenOttaProperlyFinished) {
		this.wallBelowKitchenOttaProperlyFinished = wallBelowKitchenOttaProperlyFinished;
	}

	public String getAllDoorsAndWindowOpenandShutProperly() {
		return allDoorsAndWindowOpenandShutProperly;
	}

	public void setAllDoorsAndWindowOpenandShutProperly(String allDoorsAndWindowOpenandShutProperly) {
		this.allDoorsAndWindowOpenandShutProperly = allDoorsAndWindowOpenandShutProperly;
	}

	public String getAllLocksTowerBoltsStoppersHingesOperateWell() {
		return allLocksTowerBoltsStoppersHingesOperateWell;
	}

	public void setAllLocksTowerBoltsStoppersHingesOperateWell(String allLocksTowerBoltsStoppersHingesOperateWell) {
		this.allLocksTowerBoltsStoppersHingesOperateWell = allLocksTowerBoltsStoppersHingesOperateWell;
	}

	public String getAllGlassToWindowProperlyFixedAndOperationWellAndCleaned() {
		return allGlassToWindowProperlyFixedAndOperationWellAndCleaned;
	}

	public void setAllGlassToWindowProperlyFixedAndOperationWellAndCleaned(
			String allGlassToWindowProperlyFixedAndOperationWellAndCleaned) {
		this.allGlassToWindowProperlyFixedAndOperationWellAndCleaned = allGlassToWindowProperlyFixedAndOperationWellAndCleaned;
	}

	public String getNamePlateFixedOnMainDoor() {
		return namePlateFixedOnMainDoor;
	}

	public void setNamePlateFixedOnMainDoor(String namePlateFixedOnMainDoor) {
		this.namePlateFixedOnMainDoor = namePlateFixedOnMainDoor;
	}

	public String getMsFoldingDoorInTerraceIsOperatingProperly() {
		return msFoldingDoorInTerraceIsOperatingProperly;
	}

	public void setMsFoldingDoorInTerraceIsOperatingProperly(String msFoldingDoorInTerraceIsOperatingProperly) {
		this.msFoldingDoorInTerraceIsOperatingProperly = msFoldingDoorInTerraceIsOperatingProperly;
	}

	public String getEyePieceFittingTotheMainDoor() {
		return eyePieceFittingTotheMainDoor;
	}

	public void setEyePieceFittingTotheMainDoor(String eyePieceFittingTotheMainDoor) {
		this.eyePieceFittingTotheMainDoor = eyePieceFittingTotheMainDoor;
	}

	public String getAllDoorFittingAndMortiseLocksareProperlyFixed() {
		return allDoorFittingAndMortiseLocksareProperlyFixed;
	}

	public void setAllDoorFittingAndMortiseLocksareProperlyFixed(String allDoorFittingAndMortiseLocksareProperlyFixed) {
		this.allDoorFittingAndMortiseLocksareProperlyFixed = allDoorFittingAndMortiseLocksareProperlyFixed;
	}

	public String getAllDoorMagneticCatcherareProperlyFixed() {
		return allDoorMagneticCatcherareProperlyFixed;
	}

	public void setAllDoorMagneticCatcherareProperlyFixed(String allDoorMagneticCatcherareProperlyFixed) {
		this.allDoorMagneticCatcherareProperlyFixed = allDoorMagneticCatcherareProperlyFixed;
	}

	public String getAllFittingareFreePaintSpots() {
		return allFittingareFreePaintSpots;
	}

	public void setAllFittingareFreePaintSpots(String allFittingareFreePaintSpots) {
		this.allFittingareFreePaintSpots = allFittingareFreePaintSpots;
	}

	public String getCpFittingFixedProperly() {
		return cpFittingFixedProperly;
	}

	public void setCpFittingFixedProperly(String cpFittingFixedProperly) {
		this.cpFittingFixedProperly = cpFittingFixedProperly;
	}

	public String getNoLeakagesInCPFittings() {
		return noLeakagesInCPFittings;
	}

	public void setNoLeakagesInCPFittings(String noLeakagesInCPFittings) {
		this.noLeakagesInCPFittings = noLeakagesInCPFittings;
	}

	public String getHotAndColdMixerOparatesProperly() {
		return hotAndColdMixerOparatesProperly;
	}

	public void setHotAndColdMixerOparatesProperly(String hotAndColdMixerOparatesProperly) {
		this.hotAndColdMixerOparatesProperly = hotAndColdMixerOparatesProperly;
	}

	public String getFlushTankcanEasilyOprate() {
		return flushTankcanEasilyOprate;
	}

	public void setFlushTankcanEasilyOprate(String flushTankcanEasilyOprate) {
		this.flushTankcanEasilyOprate = flushTankcanEasilyOprate;
	}

	public String getEwcCoverProperlyFitted() {
		return ewcCoverProperlyFitted;
	}

	public void setEwcCoverProperlyFitted(String ewcCoverProperlyFitted) {
		this.ewcCoverProperlyFitted = ewcCoverProperlyFitted;
	}

	public String getGapBetwwenWallTilesandWashBasinFilled() {
		return gapBetwwenWallTilesandWashBasinFilled;
	}

	public void setGapBetwwenWallTilesandWashBasinFilled(String gapBetwwenWallTilesandWashBasinFilled) {
		this.gapBetwwenWallTilesandWashBasinFilled = gapBetwwenWallTilesandWashBasinFilled;
	}

	public String getAllSanitaryFittingareNeatlyCleaned() {
		return allSanitaryFittingareNeatlyCleaned;
	}

	public void setAllSanitaryFittingareNeatlyCleaned(String allSanitaryFittingareNeatlyCleaned) {
		this.allSanitaryFittingareNeatlyCleaned = allSanitaryFittingareNeatlyCleaned;
	}

	public String getWashbasinIsProperlyFittedAndDoesNotMove() {
		return washbasinIsProperlyFittedAndDoesNotMove;
	}

	public void setWashbasinIsProperlyFittedAndDoesNotMove(String washbasinIsProperlyFittedAndDoesNotMove) {
		this.washbasinIsProperlyFittedAndDoesNotMove = washbasinIsProperlyFittedAndDoesNotMove;
	}

	public String getNoSanitaryFittingsareCracked() {
		return noSanitaryFittingsareCracked;
	}

	public void setNoSanitaryFittingsareCracked(String noSanitaryFittingsareCracked) {
		this.noSanitaryFittingsareCracked = noSanitaryFittingsareCracked;
	}

	public String getBothTapsInKitchenOperative() {
		return bothTapsInKitchenOperative;
	}

	public void setBothTapsInKitchenOperative(String bothTapsInKitchenOperative) {
		this.bothTapsInKitchenOperative = bothTapsInKitchenOperative;
	}

	public String getElectricMeterConnectedTotheFlat() {
		return electricMeterConnectedTotheFlat;
	}

	public void setElectricMeterConnectedTotheFlat(String electricMeterConnectedTotheFlat) {
		this.electricMeterConnectedTotheFlat = electricMeterConnectedTotheFlat;
	}

	public String getAllSwitchesOperatedProperly() {
		return allSwitchesOperatedProperly;
	}

	public void setAllSwitchesOperatedProperly(String allSwitchesOperatedProperly) {
		this.allSwitchesOperatedProperly = allSwitchesOperatedProperly;
	}

	public String getMcbAnddcbProperlyFixed() {
		return mcbAnddcbProperlyFixed;
	}

	public void setMcbAnddcbProperlyFixed(String mcbAnddcbProperlyFixed) {
		this.mcbAnddcbProperlyFixed = mcbAnddcbProperlyFixed;
	}

	public String getColorOfAllSwitchesIsUniform() {
		return colorOfAllSwitchesIsUniform;
	}

	public void setColorOfAllSwitchesIsUniform(String colorOfAllSwitchesIsUniform) {
		this.colorOfAllSwitchesIsUniform = colorOfAllSwitchesIsUniform;
	}

	public String getAllswitchboardsareProperlyScrewed() {
		return allswitchboardsareProperlyScrewed;
	}

	public void setAllswitchboardsareProperlyScrewed(String allswitchboardsareProperlyScrewed) {
		this.allswitchboardsareProperlyScrewed = allswitchboardsareProperlyScrewed;
	}

	public String getPaintingOfallWallsAndCeilingsIsProperlyDone() {
		return paintingOfallWallsAndCeilingsIsProperlyDone;
	}

	public void setPaintingOfallWallsAndCeilingsIsProperlyDone(String paintingOfallWallsAndCeilingsIsProperlyDone) {
		this.paintingOfallWallsAndCeilingsIsProperlyDone = paintingOfallWallsAndCeilingsIsProperlyDone;
	}

	public String getOilPaintingOfWindowGrillsAndTerraceRailingDoneProperly() {
		return oilPaintingOfWindowGrillsAndTerraceRailingDoneProperly;
	}

	public void setOilPaintingOfWindowGrillsAndTerraceRailingDoneProperly(
			String oilPaintingOfWindowGrillsAndTerraceRailingDoneProperly) {
		this.oilPaintingOfWindowGrillsAndTerraceRailingDoneProperly = oilPaintingOfWindowGrillsAndTerraceRailingDoneProperly;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	
}

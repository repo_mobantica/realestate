package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "projectbankdetails")
public class ProjectBankDetails {

	@Id
    private String projectBankDetailsId;
    private String projectDetailsId;
	private String projectId;
	
	private String accountHolderName;
	private String bankName;
	private String branchName;
	private String ifscCode;
	private String accountNumber;
	
	private String status;
	
	public ProjectBankDetails() {}

	public ProjectBankDetails(String projectBankDetailsId, String projectDetailsId, String projectId,
			String accountHolderName, String bankName, String branchName, String ifscCode, String accountNumber,
			String status) {
		super();
		this.projectBankDetailsId = projectBankDetailsId;
		this.projectDetailsId = projectDetailsId;
		this.projectId = projectId;
		this.accountHolderName = accountHolderName;
		this.bankName = bankName;
		this.branchName = branchName;
		this.ifscCode = ifscCode;
		this.accountNumber = accountNumber;
		this.status = status;
	}

	public String getProjectBankDetailsId() {
		return projectBankDetailsId;
	}

	public void setProjectBankDetailsId(String projectBankDetailsId) {
		this.projectBankDetailsId = projectBankDetailsId;
	}

	public String getProjectDetailsId() {
		return projectDetailsId;
	}

	public void setProjectDetailsId(String projectDetailsId) {
		this.projectDetailsId = projectDetailsId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getAccountHolderName() {
		return accountHolderName;
	}

	public void setAccountHolderName(String accountHolderName) {
		this.accountHolderName = accountHolderName;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}

package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "projects")
@CompoundIndexes({@CompoundIndex(name="projectIndex", unique= true, def="{'projectName':1}")})
public class Project 
{
	@Id
	private String projectId;
	private String projectName;
	private String projectType;
	private String projectAddress;
	private String countryId;
	private String stateId;
	private String cityId;
	private String locationareaId;
	private String areaPincode;
	private String projectofficePhoneno;
	private String companyId;
	private String projectlandArea;
	private double projectCost;
	private int	   numberofBuildings;
	private int	   loading;
	private String reraStatus;
	private String reraNumber;
	private String projectstartDate;
	private String projectendDate;
	private String projectStatus;
	private String gatORserveyNumber;

	private double gstPer;
	private double stampdutyPer;
	private double registrationPer;
	
	private String creationDate;
	private String updateDate;
	private String userName;
	
	@Transient
	private int priviousLoading;
	
	public Project()
	{
	}

	public Project(String projectId, String projectName, String projectType, String projectAddress, String countryId,
			String stateId, String cityId, String locationareaId, String areaPincode, String projectofficePhoneno,
			String companyId, String projectlandArea, double projectCost, int numberofBuildings, int loading,
			String reraStatus, String reraNumber, String projectstartDate, String projectendDate, String projectStatus,
			String gatORserveyNumber, double gstPer, double stampdutyPer, double registrationPer, String creationDate, String updateDate, String userName) {
		super();
		this.projectId = projectId;
		this.projectName = projectName;
		this.projectType = projectType;
		this.projectAddress = projectAddress;
		this.countryId = countryId;
		this.stateId = stateId;
		this.cityId = cityId;
		this.locationareaId = locationareaId;
		this.areaPincode = areaPincode;
		this.projectofficePhoneno = projectofficePhoneno;
		this.companyId = companyId;
		this.projectlandArea = projectlandArea;
		this.projectCost = projectCost;
		this.numberofBuildings = numberofBuildings;
		this.loading = loading;
		this.reraStatus = reraStatus;
		this.reraNumber = reraNumber;
		this.projectstartDate = projectstartDate;
		this.projectendDate = projectendDate;
		this.projectStatus = projectStatus;
		this.gatORserveyNumber = gatORserveyNumber;
		this.gstPer = gstPer;
		this.stampdutyPer = stampdutyPer;
		this.registrationPer = registrationPer;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectType() {
		return projectType;
	}

	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

	public String getProjectAddress() {
		return projectAddress;
	}

	public void setProjectAddress(String projectAddress) {
		this.projectAddress = projectAddress;
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getStateId() {
		return stateId;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getLocationareaId() {
		return locationareaId;
	}

	public void setLocationareaId(String locationareaId) {
		this.locationareaId = locationareaId;
	}

	public String getAreaPincode() {
		return areaPincode;
	}

	public void setAreaPincode(String areaPincode) {
		this.areaPincode = areaPincode;
	}

	public String getProjectofficePhoneno() {
		return projectofficePhoneno;
	}

	public void setProjectofficePhoneno(String projectofficePhoneno) {
		this.projectofficePhoneno = projectofficePhoneno;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getProjectlandArea() {
		return projectlandArea;
	}

	public void setProjectlandArea(String projectlandArea) {
		this.projectlandArea = projectlandArea;
	}

	public double getProjectCost() {
		return projectCost;
	}

	public void setProjectCost(double projectCost) {
		this.projectCost = projectCost;
	}

	public int getNumberofBuildings() {
		return numberofBuildings;
	}

	public void setNumberofBuildings(int numberofBuildings) {
		this.numberofBuildings = numberofBuildings;
	}

	public int getLoading() {
		return loading;
	}

	public void setLoading(int loading) {
		this.loading = loading;
	}

	public String getReraStatus() {
		return reraStatus;
	}

	public void setReraStatus(String reraStatus) {
		this.reraStatus = reraStatus;
	}

	public String getReraNumber() {
		return reraNumber;
	}

	public void setReraNumber(String reraNumber) {
		this.reraNumber = reraNumber;
	}

	public String getProjectstartDate() {
		return projectstartDate;
	}

	public void setProjectstartDate(String projectstartDate) {
		this.projectstartDate = projectstartDate;
	}

	public String getProjectendDate() {
		return projectendDate;
	}

	public void setProjectendDate(String projectendDate) {
		this.projectendDate = projectendDate;
	}

	public String getProjectStatus() {
		return projectStatus;
	}

	public void setProjectStatus(String projectStatus) {
		this.projectStatus = projectStatus;
	}

	public String getGatORserveyNumber() {
		return gatORserveyNumber;
	}

	public void setGatORserveyNumber(String gatORserveyNumber) {
		this.gatORserveyNumber = gatORserveyNumber;
	}

	public double getGstPer() {
		return gstPer;
	}

	public void setGstPer(double gstPer) {
		this.gstPer = gstPer;
	}

	public double getStampdutyPer() {
		return stampdutyPer;
	}

	public void setStampdutyPer(double stampdutyPer) {
		this.stampdutyPer = stampdutyPer;
	}

	public double getRegistrationPer() {
		return registrationPer;
	}

	public void setRegistrationPer(double registrationPer) {
		this.registrationPer = registrationPer;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getPriviousLoading() {
		return priviousLoading;
	}

	public void setPriviousLoading(int priviousLoading) {
		this.priviousLoading = priviousLoading;
	}
	
}

package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="materialspurchased")
public class MaterialsPurchased {

	@Id
	private String materialsPurchasedId;
	private String requisitionId;
	private String supplierId;
	private String storeId;
	private String employeeId;
	
	private Double totalPrice;
	private Double totalDiscount;
	private Double totalGstAmount;
	private Double totalAmount;
	private Double transportCharges;
	private Double totalchargeWithtransport;

	private String delivery;
	private String paymentterms;
	private String afterdelivery;
	
	private String invoiceNo;
	
	private String status;
	private String paymentStatus;
	private String creationDate;
	private String updateDate;
	private String userName;
	
	
	@Transient
	private String supplierfirmName;
	@Transient
	private String storeName;
	@Transient
	private int number;
	
	public MaterialsPurchased() {}

	public MaterialsPurchased(String materialsPurchasedId, String requisitionId, String supplierId, String storeId,
			String employeeId, Double totalPrice, Double totalDiscount, Double totalGstAmount, Double totalAmount, Double transportCharges, Double totalchargeWithtransport,
			String delivery, String paymentterms, String afterdelivery,
			String status, String paymentStatus, String creationDate, String updateDate, String userName) {
		super();
		this.materialsPurchasedId = materialsPurchasedId;
		this.requisitionId = requisitionId;
		this.supplierId = supplierId;
		this.storeId = storeId;
		this.employeeId = employeeId;
		this.totalPrice = totalPrice;
		this.totalDiscount = totalDiscount;
		this.totalGstAmount = totalGstAmount;
		this.totalAmount = totalAmount;
		this.transportCharges = transportCharges;
		this.totalchargeWithtransport = totalchargeWithtransport;
		this.delivery = delivery;
		this.paymentterms = paymentterms;
		this.afterdelivery = afterdelivery;
		this.status = status;
		this.paymentStatus = paymentStatus;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getMaterialsPurchasedId() {
		return materialsPurchasedId;
	}

	public void setMaterialsPurchasedId(String materialsPurchasedId) {
		this.materialsPurchasedId = materialsPurchasedId;
	}

	public String getRequisitionId() {
		return requisitionId;
	}

	public void setRequisitionId(String requisitionId) {
		this.requisitionId = requisitionId;
	}

	public String getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Double getTotalDiscount() {
		return totalDiscount;
	}

	public void setTotalDiscount(Double totalDiscount) {
		this.totalDiscount = totalDiscount;
	}

	public Double getTotalGstAmount() {
		return totalGstAmount;
	}

	public void setTotalGstAmount(Double totalGstAmount) {
		this.totalGstAmount = totalGstAmount;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getSupplierfirmName() {
		return supplierfirmName;
	}

	public void setSupplierfirmName(String supplierfirmName) {
		this.supplierfirmName = supplierfirmName;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getDelivery() {
		return delivery;
	}

	public void setDelivery(String delivery) {
		this.delivery = delivery;
	}

	public String getPaymentterms() {
		return paymentterms;
	}

	public void setPaymentterms(String paymentterms) {
		this.paymentterms = paymentterms;
	}

	public String getAfterdelivery() {
		return afterdelivery;
	}

	public void setAfterdelivery(String afterdelivery) {
		this.afterdelivery = afterdelivery;
	}

	public Double getTransportCharges() {
		return transportCharges;
	}

	public void setTransportCharges(Double transportCharges) {
		this.transportCharges = transportCharges;
	}

	public Double getTotalchargeWithtransport() {
		return totalchargeWithtransport;
	}

	public void setTotalchargeWithtransport(Double totalchargeWithtransport) {
		this.totalchargeWithtransport = totalchargeWithtransport;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

}

package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "supplieremployees")
public class SupplierEmployees 
{
	@Id
	private int supplierEmployeeId;
	private String supplierId;
	private String employeeName;
	private String employeeDesignation;
	private String employeeEmail;
	private String employeeMobileno;
	
	
	public SupplierEmployees()
	{

	}


	public SupplierEmployees(int supplierEmployeeId, String supplierId, String employeeName, String employeeDesignation, String employeeEmail, String employeeMobileno) 
	{
		super();
		this.supplierEmployeeId = supplierEmployeeId;
		this.supplierId = supplierId;
		this.employeeName = employeeName;
		this.employeeDesignation = employeeDesignation;
		this.employeeEmail = employeeEmail;
		this.employeeMobileno = employeeMobileno;
	}


	public int getSupplierEmployeeId() {
		return supplierEmployeeId;
	}


	public void setSupplierEmployeeId(int supplierEmployeeId) {
		this.supplierEmployeeId = supplierEmployeeId;
	}


	public String getSupplierId() {
		return supplierId;
	}


	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}


	public String getEmployeeName() {
		return employeeName;
	}


	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}


	public String getEmployeeDesignation() {
		return employeeDesignation;
	}


	public void setEmployeeDesignation(String employeeDesignation) {
		this.employeeDesignation = employeeDesignation;
	}


	public String getEmployeeEmail() {
		return employeeEmail;
	}


	public void setEmployeeEmail(String employeeEmail) {
		this.employeeEmail = employeeEmail;
	}


	public String getEmployeeMobileno() {
		return employeeMobileno;
	}


	public void setEmployeeMobileno(String employeeMobileno) {
		this.employeeMobileno = employeeMobileno;
	}
	
}

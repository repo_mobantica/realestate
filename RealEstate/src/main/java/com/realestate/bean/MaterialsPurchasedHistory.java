package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="materialspurchasedhistories")
public class MaterialsPurchasedHistory 
{
	@Id
	private String materialPurchesHistoriId;
	private String materialsPurchasedId;
	private String requisitionId;
	private String itemId;
	private String itemName;
	private String itenSize;
	private String itemUnit;
	private String itemBrandName;
	private double itemQuantity;
	private double rate;
	private double total;
	private double discountPer;
	private double discountAmount;
	private double gstPer;
	private double gstAmount;
	private double grandTotal;
	private String status;
	
	@Transient
	private double previousQuantity;
	@Transient
	private double remainingQuantity;
	@Transient
	private String storeStockHistoriId;
	@Transient
	private long previousNoOfPieces;
	@Transient
	private String subsuppliertypeName;
	
	public MaterialsPurchasedHistory() {}

	public MaterialsPurchasedHistory(String materialPurchesHistoriId, String materialsPurchasedId, String requisitionId,
			String itemId, String itemName, String itenSize, String itemUnit,
			String itemBrandName, double itemQuantity, double rate, double total, double discountPer,
			double discountAmount, double gstPer, double gstAmount, double grandTotal, String status) {
		super();
		this.materialPurchesHistoriId = materialPurchesHistoriId;
		this.materialsPurchasedId = materialsPurchasedId;
		this.requisitionId = requisitionId;
		this.itemId = itemId;
		this.itemName = itemName;
		this.itenSize = itenSize;
		this.itemUnit = itemUnit;
		this.itemBrandName = itemBrandName;
		this.itemQuantity = itemQuantity;
		this.rate = rate;
		this.total = total;
		this.discountPer = discountPer;
		this.discountAmount = discountAmount;
		this.gstPer = gstPer;
		this.gstAmount = gstAmount;
		this.grandTotal = grandTotal;
		this.status = status;
	}

	public String getMaterialPurchesHistoriId() {
		return materialPurchesHistoriId;
	}

	public void setMaterialPurchesHistoriId(String materialPurchesHistoriId) {
		this.materialPurchesHistoriId = materialPurchesHistoriId;
	}

	public String getMaterialsPurchasedId() {
		return materialsPurchasedId;
	}

	public void setMaterialsPurchasedId(String materialsPurchasedId) {
		this.materialsPurchasedId = materialsPurchasedId;
	}

	public String getRequisitionId() {
		return requisitionId;
	}

	public void setRequisitionId(String requisitionId) {
		this.requisitionId = requisitionId;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItenSize() {
		return itenSize;
	}

	public void setItenSize(String itenSize) {
		this.itenSize = itenSize;
	}

	public String getItemUnit() {
		return itemUnit;
	}

	public void setItemUnit(String itemUnit) {
		this.itemUnit = itemUnit;
	}

	public String getItemBrandName() {
		return itemBrandName;
	}

	public void setItemBrandName(String itemBrandName) {
		this.itemBrandName = itemBrandName;
	}

	public double getItemQuantity() {
		return itemQuantity;
	}

	public void setItemQuantity(double itemQuantity) {
		this.itemQuantity = itemQuantity;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public double getDiscountPer() {
		return discountPer;
	}

	public void setDiscountPer(double discountPer) {
		this.discountPer = discountPer;
	}

	public double getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(double discountAmount) {
		this.discountAmount = discountAmount;
	}

	public double getGstPer() {
		return gstPer;
	}

	public void setGstPer(double gstPer) {
		this.gstPer = gstPer;
	}

	public double getGstAmount() {
		return gstAmount;
	}

	public void setGstAmount(double gstAmount) {
		this.gstAmount = gstAmount;
	}

	public double getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(double grandTotal) {
		this.grandTotal = grandTotal;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public double getPreviousQuantity() {
		return previousQuantity;
	}

	public void setPreviousQuantity(double previousQuantity) {
		this.previousQuantity = previousQuantity;
	}

	public String getStoreStockHistoriId() {
		return storeStockHistoriId;
	}

	public void setStoreStockHistoriId(String storeStockHistoriId) {
		this.storeStockHistoriId = storeStockHistoriId;
	}

	public long getPreviousNoOfPieces() {
		return previousNoOfPieces;
	}

	public void setPreviousNoOfPieces(long previousNoOfPieces) {
		this.previousNoOfPieces = previousNoOfPieces;
	}

	public String getSubsuppliertypeName() {
		return subsuppliertypeName;
	}

	public void setSubsuppliertypeName(String subsuppliertypeName) {
		this.subsuppliertypeName = subsuppliertypeName;
	}

	public double getRemainingQuantity() {
		return remainingQuantity;
	}

	public void setRemainingQuantity(double remainingQuantity) {
		this.remainingQuantity = remainingQuantity;
	}

}

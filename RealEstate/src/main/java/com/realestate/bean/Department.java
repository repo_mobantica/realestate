package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "departments")
//@CompoundIndexes({@CompoundIndex(name="departmentnameIndex", unique= true, def="{'departmentName':1}")})
public class Department 
{
	@Id
	private String departmentId;
	private String departmentName;
	private String creationDate;
	private String updateDate;
	private String userName;
	
	public Department() 
	{
		
	}

	

	public Department(String departmentId, String departmentName, String creationDate, String updateDate,
			String userName) {
		super();
		this.departmentId = departmentId;
		this.departmentName = departmentName;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}



	public String getDepartmentId()
	{
		return departmentId;
	}

	public void setDepartmentId(String departmentId)
	{
		this.departmentId = departmentId;
	}

	public String getDepartmentName() 
	{
		return departmentName;
	}

	public void setDepartmentName(String departmentName) 
	{
		this.departmentName = departmentName;
	}

	public String getCreationDate() 
	{
		return creationDate;
	}

	public void setCreationDate(String creationDate) 
	{
		this.creationDate = creationDate;
	}

	public String getUpdateDate()
	{
		return updateDate;
	}

	public void setUpdateDate(String updateDate)
	{
		this.updateDate = updateDate;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}
	
}

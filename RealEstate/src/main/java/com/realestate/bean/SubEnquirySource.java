package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "subenquirysources")
public class SubEnquirySource {

	@Id
	private String subsourceId;
	private String enquirysourceId;
	private String subenquirysourceName;
	
	private String creationDate;
	private String updateDate;
	private String userName;
	
	public SubEnquirySource()
	{}
	public SubEnquirySource(String subsourceId, String enquirysourceId, String subenquirysourceName,
			String creationDate, String updateDate, String userName) {
		super();
		this.subsourceId = subsourceId;
		this.enquirysourceId = enquirysourceId;
		this.subenquirysourceName = subenquirysourceName;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}
	public String getSubsourceId() {
		return subsourceId;
	}
	public void setSubsourceId(String subsourceId) {
		this.subsourceId = subsourceId;
	}
	
	public String getEnquirysourceId() {
		return enquirysourceId;
	}
	public void setEnquirysourceId(String enquirysourceId) {
		this.enquirysourceId = enquirysourceId;
	}
	public String getSubenquirysourceName() {
		return subenquirysourceName;
	}
	public void setSubenquirysourceName(String subenquirysourceName) {
		this.subenquirysourceName = subenquirysourceName;
	}
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	
	
}

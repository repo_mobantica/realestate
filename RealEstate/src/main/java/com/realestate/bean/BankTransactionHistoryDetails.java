package com.realestate.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "banktransactionhistorydetails")
public class BankTransactionHistoryDetails {
	
	@Id
    private String bankTransactionId;
	private String companyBankId;
    private Double amount;
    //paymentType -for booking customer/supplier/contractor/agent ect
    // paymentId- bookingId/contractorId/supplierId/agentId
    private String paymentType;
    private String paymentId;
    
    private String creditOrDebiteType;
    private Date creationDate;
    private String userName;
    
    public BankTransactionHistoryDetails() {}

	public BankTransactionHistoryDetails(String bankTransactionId, String companyBankId, Double amount,
			String paymentType, String paymentId, String creditOrDebiteType, Date creationDate, String userName) {
		super();
		this.bankTransactionId = bankTransactionId;
		this.companyBankId = companyBankId;
		this.amount = amount;
		this.paymentType = paymentType;
		this.paymentId = paymentId;
		this.creditOrDebiteType = creditOrDebiteType;
		this.creationDate = creationDate;
		this.userName = userName;
	}

	public String getBankTransactionId() {
		return bankTransactionId;
	}

	public void setBankTransactionId(String bankTransactionId) {
		this.bankTransactionId = bankTransactionId;
	}

	public String getCompanyBankId() {
		return companyBankId;
	}

	public void setCompanyBankId(String companyBankId) {
		this.companyBankId = companyBankId;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public String getCreditOrDebiteType() {
		return creditOrDebiteType;
	}

	public void setCreditOrDebiteType(String creditOrDebiteType) {
		this.creditOrDebiteType = creditOrDebiteType;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}

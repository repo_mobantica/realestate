package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "subchartofaccounts")
@CompoundIndexes({@CompoundIndex(name="subchartofaccountIndex", unique= true, def="{'subchartaccountNumber':1}")})

public class SubChartofAccount {

	@Id
    private String subchartaccountId;
    private String chartaccountId;
    private Integer chartaccountNumber;
    private Integer subchartaccountNumber;
    private String subchartaccountName;
    private String creationDate;
    private String updateDate;
    private String userName;
    
    public SubChartofAccount(){}

	public SubChartofAccount(String subchartaccountId, String chartaccountId, Integer chartaccountNumber,
			Integer subchartaccountNumber, String subchartaccountName, String creationDate, String updateDate,
			String userName) 
	{
		super();
		this.subchartaccountId = subchartaccountId;
		this.chartaccountId = chartaccountId;
		this.chartaccountNumber = chartaccountNumber;
		this.subchartaccountNumber = subchartaccountNumber;
		this.subchartaccountName = subchartaccountName;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getSubchartaccountId() {
		return subchartaccountId;
	}

	public void setSubchartaccountId(String subchartaccountId) {
		this.subchartaccountId = subchartaccountId;
	}

	public String getChartaccountId() {
		return chartaccountId;
	}

	public void setChartaccountId(String chartaccountId) {
		this.chartaccountId = chartaccountId;
	}

	public Integer getChartaccountNumber() {
		return chartaccountNumber;
	}

	public void setChartaccountNumber(Integer chartaccountNumber) {
		this.chartaccountNumber = chartaccountNumber;
	}

	public Integer getSubchartaccountNumber() {
		return subchartaccountNumber;
	}

	public void setSubchartaccountNumber(Integer subchartaccountNumber) {
		this.subchartaccountNumber = subchartaccountNumber;
	}

	public String getSubchartaccountName() {
		return subchartaccountName;
	}

	public void setSubchartaccountName(String subchartaccountName) {
		this.subchartaccountName = subchartaccountName;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
    
    
    
}

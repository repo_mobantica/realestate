package com.realestate.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "licensingdocument")
public class LicensingDocument {

	   @Id
	   private int documentId;
	   private String projectId;
	   
	   private String description;
	   private String invertedDate;

	   private String tokenNumber;
	   private String challanAmount;
	   
	   private String tokenDocumentExtension;
	   private String nocDocumentextension;
	   
	   private String status;
	   
	   private Date   creationDate;
	   private Date   updateDate;
	   
	   public LicensingDocument() {}

	public LicensingDocument(int documentId, String projectId, String description, String invertedDate,
			String tokenNumber, String challanAmount, String tokenDocumentExtension, String nocDocumentextension,
			String status, Date creationDate, Date updateDate) {
		super();
		this.documentId = documentId;
		this.projectId = projectId;
		this.description = description;
		this.invertedDate = invertedDate;
		this.tokenNumber = tokenNumber;
		this.challanAmount = challanAmount;
		this.tokenDocumentExtension = tokenDocumentExtension;
		this.nocDocumentextension = nocDocumentextension;
		this.status = status;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
	}

	public int getDocumentId() {
		return documentId;
	}

	public void setDocumentId(int documentId) {
		this.documentId = documentId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getInvertedDate() {
		return invertedDate;
	}

	public void setInvertedDate(String invertedDate) {
		this.invertedDate = invertedDate;
	}

	public String getTokenNumber() {
		return tokenNumber;
	}

	public void setTokenNumber(String tokenNumber) {
		this.tokenNumber = tokenNumber;
	}

	public String getChallanAmount() {
		return challanAmount;
	}

	public void setChallanAmount(String challanAmount) {
		this.challanAmount = challanAmount;
	}

	public String getTokenDocumentExtension() {
		return tokenDocumentExtension;
	}

	public void setTokenDocumentExtension(String tokenDocumentExtension) {
		this.tokenDocumentExtension = tokenDocumentExtension;
	}

	public String getNocDocumentextension() {
		return nocDocumentextension;
	}

	public void setNocDocumentextension(String nocDocumentextension) {
		this.nocDocumentextension = nocDocumentextension;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

}

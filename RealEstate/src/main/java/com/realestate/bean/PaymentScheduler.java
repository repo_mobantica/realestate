package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "paymentschedules")
public class PaymentScheduler
{
	@Id
	private String paymentschedulerId;
	private String projectId;
	private String buildingId;
	private String wingId;
	private String installmentNumber;
	private String paymentDecription;
	private Double percentage;
	private String dueDate;
	private String paidDate;
	private String slabStatus;
	private String architectureSign;
	private String engineerSign;
	private String meSign;
	
	private String architectureSignDate;
	private String engineerSignDate;
	private String meSignDate;
	
	private String creationDate;
	private String updateDate;
	private String userName;
	
	public PaymentScheduler()
	{
		
	}

	public PaymentScheduler(String paymentschedulerId, String projectId, String buildingId, String wingId,
			String installmentNumber, String paymentDecription, Double percentage, String dueDate, String paidDate,
			String slabStatus, String architectureSign, String engineerSign, String meSign, String architectureSignDate,
			String engineerSignDate, String meSignDate, String creationDate, String updateDate, String userName) {
		super();
		this.paymentschedulerId = paymentschedulerId;
		this.projectId = projectId;
		this.buildingId = buildingId;
		this.wingId = wingId;
		this.installmentNumber = installmentNumber;
		this.paymentDecription = paymentDecription;
		this.percentage = percentage;
		this.dueDate = dueDate;
		this.paidDate = paidDate;
		this.slabStatus = slabStatus;
		this.architectureSign = architectureSign;
		this.engineerSign = engineerSign;
		this.meSign = meSign;
		this.architectureSignDate = architectureSignDate;
		this.engineerSignDate = engineerSignDate;
		this.meSignDate = meSignDate;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getPaymentschedulerId() {
		return paymentschedulerId;
	}

	public void setPaymentschedulerId(String paymentschedulerId) {
		this.paymentschedulerId = paymentschedulerId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	public String getWingId() {
		return wingId;
	}

	public void setWingId(String wingId) {
		this.wingId = wingId;
	}

	public String getInstallmentNumber() {
		return installmentNumber;
	}

	public void setInstallmentNumber(String installmentNumber) {
		this.installmentNumber = installmentNumber;
	}

	public String getPaymentDecription() {
		return paymentDecription;
	}

	public void setPaymentDecription(String paymentDecription) {
		this.paymentDecription = paymentDecription;
	}

	public Double getPercentage() {
		return percentage;
	}

	public void setPercentage(Double percentage) {
		this.percentage = percentage;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public String getPaidDate() {
		return paidDate;
	}

	public void setPaidDate(String paidDate) {
		this.paidDate = paidDate;
	}

	public String getSlabStatus() {
		return slabStatus;
	}

	public void setSlabStatus(String slabStatus) {
		this.slabStatus = slabStatus;
	}

	public String getArchitectureSign() {
		return architectureSign;
	}

	public void setArchitectureSign(String architectureSign) {
		this.architectureSign = architectureSign;
	}

	public String getEngineerSign() {
		return engineerSign;
	}

	public void setEngineerSign(String engineerSign) {
		this.engineerSign = engineerSign;
	}

	public String getMeSign() {
		return meSign;
	}

	public void setMeSign(String meSign) {
		this.meSign = meSign;
	}

	public String getArchitectureSignDate() {
		return architectureSignDate;
	}

	public void setArchitectureSignDate(String architectureSignDate) {
		this.architectureSignDate = architectureSignDate;
	}

	public String getEngineerSignDate() {
		return engineerSignDate;
	}

	public void setEngineerSignDate(String engineerSignDate) {
		this.engineerSignDate = engineerSignDate;
	}

	public String getMeSignDate() {
		return meSignDate;
	}

	public void setMeSignDate(String meSignDate) {
		this.meSignDate = meSignDate;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}

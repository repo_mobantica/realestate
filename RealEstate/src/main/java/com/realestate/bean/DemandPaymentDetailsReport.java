package com.realestate.bean;

public class DemandPaymentDetailsReport
{
  private String bookingId;
  private String customerName;
  private String wingId;
  private String flatId;
  private String agreementAmount;
  private String demandAmount;
  private String paidTillDate;
  private String remainingDemandAmount;
  private String remainingAmount;
  
	    public DemandPaymentDetailsReport() 
		{
		    
		}

		public String getBookingId() {
			return bookingId;
		}

		public void setBookingId(String bookingId) {
			this.bookingId = bookingId;
		}

		public String getCustomerName() {
			return customerName;
		}

		public void setCustomerName(String customerName) {
			this.customerName = customerName;
		}

		public String getWingId() {
			return wingId;
		}

		public void setWingId(String wingId) {
			this.wingId = wingId;
		}

		public String getFlatId() {
			return flatId;
		}

		public void setFlatId(String flatId) {
			this.flatId = flatId;
		}

		public String getAgreementAmount() {
			return agreementAmount;
		}

		public void setAgreementAmount(String agreementAmount) {
			this.agreementAmount = agreementAmount;
		}

		public String getDemandAmount() {
			return demandAmount;
		}

		public void setDemandAmount(String demandAmount) {
			this.demandAmount = demandAmount;
		}

		public String getPaidTillDate() {
			return paidTillDate;
		}

		public void setPaidTillDate(String paidTillDate) {
			this.paidTillDate = paidTillDate;
		}

		public String getRemainingDemandAmount() {
			return remainingDemandAmount;
		}

		public void setRemainingDemandAmount(String remainingDemandAmount) {
			this.remainingDemandAmount = remainingDemandAmount;
		}

		public String getRemainingAmount() {
			return remainingAmount;
		}

		public void setRemainingAmount(String remainingAmount) {
			this.remainingAmount = remainingAmount;
		}

}

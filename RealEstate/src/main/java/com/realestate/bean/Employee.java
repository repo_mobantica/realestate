package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "employees")
//@CompoundIndexes({@CompoundIndex(name="employeeIndex", unique= true, def="{'employeeAadharcardno':1}")})
public class Employee 
{
	@Id
	private String employeeId;
	private String employeefirstName;
	private String employeemiddleName;
	private String employeelastName;
	private String employeeGender;
	private String employeeMarried;
	private String employeeDob;
	private String employeeAnniversaryDate;
	private String employeeSpuseName;
	private String employeeEducation;
	private String departmentId;
	private String designationId;
	
	
	private String employeeAddress;
	private String areaPincode;
	private String employeePancardno;
	private String employeeAadharcardno;
	private String employeeEmailId;
	private String employeeMobileno;
	private String companyEmailId;
	private String companyMobileno;
	private String employeeJoiningdate;

	private Double employeeBasicpay;
	private Double employeeHra;
	private Double employeeDa;
	private Double employeeCa;
	
	private String employeePfacno;
	private String employeeEsino;
	
	private Double employeePLleaves;
	private Double employeeSLleaves;
	private Double employeeCLleaves;
	
	private String employeeBankName;
	private String branchName;
	private String bankifscCode;
	private String employeeBankacno;
	private String status;
	
	private String creationDate;
	private String updateDate;
	private String userName;
	
	public Employee()
	{	}

	public Employee(String employeeId, String employeefirstName, String employeemiddleName, String employeelastName,
			String employeeGender, String employeeMarried, String employeeDob, String employeeAnniversaryDate,
			String employeeSpuseName, String employeeEducation, String departmentId, String designationId,
			String employeeAddress, String areaPincode, String employeePancardno, String employeeAadharcardno,
			String employeeEmailId, String employeeMobileno, String companyEmailId, String companyMobileno,
			String employeeJoiningdate, Double employeeBasicpay, Double employeeHra, Double employeeDa,
			Double employeeCa, String employeePfacno, String employeeEsino, Double employeePLleaves,
			Double employeeSLleaves, Double employeeCLleaves, String employeeBankName, String branchName,
			String bankifscCode, String employeeBankacno, String status, String creationDate, String updateDate,
			String userName) {
		super();
		this.employeeId = employeeId;
		this.employeefirstName = employeefirstName;
		this.employeemiddleName = employeemiddleName;
		this.employeelastName = employeelastName;
		this.employeeGender = employeeGender;
		this.employeeMarried = employeeMarried;
		this.employeeDob = employeeDob;
		this.employeeAnniversaryDate = employeeAnniversaryDate;
		this.employeeSpuseName = employeeSpuseName;
		this.employeeEducation = employeeEducation;
		this.departmentId = departmentId;
		this.designationId = designationId;
		this.employeeAddress = employeeAddress;
		this.areaPincode = areaPincode;
		this.employeePancardno = employeePancardno;
		this.employeeAadharcardno = employeeAadharcardno;
		this.employeeEmailId = employeeEmailId;
		this.employeeMobileno = employeeMobileno;
		this.companyEmailId = companyEmailId;
		this.companyMobileno = companyMobileno;
		this.employeeJoiningdate = employeeJoiningdate;
		this.employeeBasicpay = employeeBasicpay;
		this.employeeHra = employeeHra;
		this.employeeDa = employeeDa;
		this.employeeCa = employeeCa;
		this.employeePfacno = employeePfacno;
		this.employeeEsino = employeeEsino;
		this.employeePLleaves = employeePLleaves;
		this.employeeSLleaves = employeeSLleaves;
		this.employeeCLleaves = employeeCLleaves;
		this.employeeBankName = employeeBankName;
		this.branchName = branchName;
		this.bankifscCode = bankifscCode;
		this.employeeBankacno = employeeBankacno;
		this.status = status;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeefirstName() {
		return employeefirstName;
	}

	public void setEmployeefirstName(String employeefirstName) {
		this.employeefirstName = employeefirstName;
	}

	public String getEmployeemiddleName() {
		return employeemiddleName;
	}

	public void setEmployeemiddleName(String employeemiddleName) {
		this.employeemiddleName = employeemiddleName;
	}

	public String getEmployeelastName() {
		return employeelastName;
	}

	public void setEmployeelastName(String employeelastName) {
		this.employeelastName = employeelastName;
	}

	public String getEmployeeGender() {
		return employeeGender;
	}

	public void setEmployeeGender(String employeeGender) {
		this.employeeGender = employeeGender;
	}

	public String getEmployeeMarried() {
		return employeeMarried;
	}

	public void setEmployeeMarried(String employeeMarried) {
		this.employeeMarried = employeeMarried;
	}

	public String getEmployeeDob() {
		return employeeDob;
	}

	public void setEmployeeDob(String employeeDob) {
		this.employeeDob = employeeDob;
	}

	public String getEmployeeAnniversaryDate() {
		return employeeAnniversaryDate;
	}

	public void setEmployeeAnniversaryDate(String employeeAnniversaryDate) {
		this.employeeAnniversaryDate = employeeAnniversaryDate;
	}

	public String getEmployeeSpuseName() {
		return employeeSpuseName;
	}

	public void setEmployeeSpuseName(String employeeSpuseName) {
		this.employeeSpuseName = employeeSpuseName;
	}

	public String getEmployeeEducation() {
		return employeeEducation;
	}

	public void setEmployeeEducation(String employeeEducation) {
		this.employeeEducation = employeeEducation;
	}

	public String getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}

	public String getDesignationId() {
		return designationId;
	}

	public void setDesignationId(String designationId) {
		this.designationId = designationId;
	}

	public String getEmployeeAddress() {
		return employeeAddress;
	}

	public void setEmployeeAddress(String employeeAddress) {
		this.employeeAddress = employeeAddress;
	}

	public String getAreaPincode() {
		return areaPincode;
	}

	public void setAreaPincode(String areaPincode) {
		this.areaPincode = areaPincode;
	}

	public String getEmployeePancardno() {
		return employeePancardno;
	}

	public void setEmployeePancardno(String employeePancardno) {
		this.employeePancardno = employeePancardno;
	}

	public String getEmployeeAadharcardno() {
		return employeeAadharcardno;
	}

	public void setEmployeeAadharcardno(String employeeAadharcardno) {
		this.employeeAadharcardno = employeeAadharcardno;
	}

	public String getEmployeeEmailId() {
		return employeeEmailId;
	}

	public void setEmployeeEmailId(String employeeEmailId) {
		this.employeeEmailId = employeeEmailId;
	}

	public String getEmployeeMobileno() {
		return employeeMobileno;
	}

	public void setEmployeeMobileno(String employeeMobileno) {
		this.employeeMobileno = employeeMobileno;
	}

	public String getCompanyEmailId() {
		return companyEmailId;
	}

	public void setCompanyEmailId(String companyEmailId) {
		this.companyEmailId = companyEmailId;
	}

	public String getCompanyMobileno() {
		return companyMobileno;
	}

	public void setCompanyMobileno(String companyMobileno) {
		this.companyMobileno = companyMobileno;
	}

	public String getEmployeeJoiningdate() {
		return employeeJoiningdate;
	}

	public void setEmployeeJoiningdate(String employeeJoiningdate) {
		this.employeeJoiningdate = employeeJoiningdate;
	}

	public Double getEmployeeBasicpay() {
		return employeeBasicpay;
	}

	public void setEmployeeBasicpay(Double employeeBasicpay) {
		this.employeeBasicpay = employeeBasicpay;
	}

	public Double getEmployeeHra() {
		return employeeHra;
	}

	public void setEmployeeHra(Double employeeHra) {
		this.employeeHra = employeeHra;
	}

	public Double getEmployeeDa() {
		return employeeDa;
	}

	public void setEmployeeDa(Double employeeDa) {
		this.employeeDa = employeeDa;
	}

	public Double getEmployeeCa() {
		return employeeCa;
	}

	public void setEmployeeCa(Double employeeCa) {
		this.employeeCa = employeeCa;
	}

	public String getEmployeePfacno() {
		return employeePfacno;
	}

	public void setEmployeePfacno(String employeePfacno) {
		this.employeePfacno = employeePfacno;
	}

	public String getEmployeeEsino() {
		return employeeEsino;
	}

	public void setEmployeeEsino(String employeeEsino) {
		this.employeeEsino = employeeEsino;
	}

	public Double getEmployeePLleaves() {
		return employeePLleaves;
	}

	public void setEmployeePLleaves(Double employeePLleaves) {
		this.employeePLleaves = employeePLleaves;
	}

	public Double getEmployeeSLleaves() {
		return employeeSLleaves;
	}

	public void setEmployeeSLleaves(Double employeeSLleaves) {
		this.employeeSLleaves = employeeSLleaves;
	}

	public Double getEmployeeCLleaves() {
		return employeeCLleaves;
	}

	public void setEmployeeCLleaves(Double employeeCLleaves) {
		this.employeeCLleaves = employeeCLleaves;
	}

	public String getEmployeeBankName() {
		return employeeBankName;
	}

	public void setEmployeeBankName(String employeeBankName) {
		this.employeeBankName = employeeBankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getBankifscCode() {
		return bankifscCode;
	}

	public void setBankifscCode(String bankifscCode) {
		this.bankifscCode = bankifscCode;
	}

	public String getEmployeeBankacno() {
		return employeeBankacno;
	}

	public void setEmployeeBankacno(String employeeBankacno) {
		this.employeeBankacno = employeeBankacno;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}


}

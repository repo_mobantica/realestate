package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "locationareas")
//@CompoundIndexes({@CompoundIndex(name="locationareanameIndex", unique= true, def="{'locationareaName':1}")})
public class LocationArea 
{
	@Id
	private String locationareaId;
	private String locationareaName;
	private String pinCode;
	private String countryId;
	private String stateId;
	private String cityId;
	private String creationDate;
	private String updateDate;
	private String userName;
	
	public LocationArea() 
	{
		
	}

	public LocationArea(String locationareaId, String locationareaName, String pinCode, String countryId,
			String stateId, String cityId, String creationDate, String updateDate, String userName) {
		super();
		this.locationareaId = locationareaId;
		this.locationareaName = locationareaName;
		this.pinCode = pinCode;
		this.countryId = countryId;
		this.stateId = stateId;
		this.cityId = cityId;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getLocationareaId() {
		return locationareaId;
	}

	public void setLocationareaId(String locationareaId) {
		this.locationareaId = locationareaId;
	}

	public String getLocationareaName() {
		return locationareaName;
	}

	public void setLocationareaName(String locationareaName) {
		this.locationareaName = locationareaName;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getStateId() {
		return stateId;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}

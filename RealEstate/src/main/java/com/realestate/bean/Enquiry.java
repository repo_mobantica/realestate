package com.realestate.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "enquiries")

public class Enquiry
{
	@Id
	private String enquiryId;
	private String enqfirstName;
	private String enqmiddleName;
	private String enqlastName;
	/*
	private String enqDob;
	private String enqGender;
	private String enqAddress;
	private String countryName;
	private String stateName;
	private String cityName;
	private String locationareaName;
	private String enqPincode;
	*/
	private String enqEmail;
	private String enqmobileNumber1;
	private String enqmobileNumber2;
	
	//private String enqPancardno;
	//private String enqAadharno;
	private String enqOccupation;
	//private String enqcompanyName;
	//private String enqofficeNumber;
	//private String projectName;
	//private String buildingName;
	//private String wingName;
	//private String floortypeName;
	private String projectName;
	private String flatType;
	private String flatRemark;
	/*
	private String flatNumber;
	private String flatFacing;
	private String flatareainSqFt;
	private Double flatbasicCost;
	*/
	private String flatBudget;
	//private String bankName;
	//private String branchName;
	//private String bankifscCode;
	private String enquirysourceId;
	private String subsourceId;
	private String followupDate;
	private String enqStatus;
	private String enqRemark;
	
	private Date creationDate;
	private Date updateDate;
	private String userName;
	
	public Enquiry()
	{
		
	}

	public Enquiry(String enquiryId, String enqfirstName, String enqmiddleName, String enqlastName, String enqEmail,
			String enqmobileNumber1, String enqmobileNumber2, String enqOccupation, String flatType, String flatRemark,
			String flatBudget, String enquirysourceId, String subsourceId, String followupDate, String enqStatus,
			String enqRemark, Date creationDate, Date updateDate, String userName) {
		super();
		this.enquiryId = enquiryId;
		this.enqfirstName = enqfirstName;
		this.enqmiddleName = enqmiddleName;
		this.enqlastName = enqlastName;
		this.enqEmail = enqEmail;
		this.enqmobileNumber1 = enqmobileNumber1;
		this.enqmobileNumber2 = enqmobileNumber2;
		this.enqOccupation = enqOccupation;
		this.flatType = flatType;
		this.flatRemark = flatRemark;
		this.flatBudget = flatBudget;
		this.enquirysourceId = enquirysourceId;
		this.subsourceId = subsourceId;
		this.followupDate = followupDate;
		this.enqStatus = enqStatus;
		this.enqRemark = enqRemark;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getEnquiryId() {
		return enquiryId;
	}

	public void setEnquiryId(String enquiryId) {
		this.enquiryId = enquiryId;
	}

	public String getEnqfirstName() {
		return enqfirstName;
	}

	public void setEnqfirstName(String enqfirstName) {
		this.enqfirstName = enqfirstName;
	}

	public String getEnqmiddleName() {
		return enqmiddleName;
	}

	public void setEnqmiddleName(String enqmiddleName) {
		this.enqmiddleName = enqmiddleName;
	}

	public String getEnqlastName() {
		return enqlastName;
	}

	public void setEnqlastName(String enqlastName) {
		this.enqlastName = enqlastName;
	}

	public String getEnqEmail() {
		return enqEmail;
	}

	public void setEnqEmail(String enqEmail) {
		this.enqEmail = enqEmail;
	}

	public String getEnqmobileNumber1() {
		return enqmobileNumber1;
	}

	public void setEnqmobileNumber1(String enqmobileNumber1) {
		this.enqmobileNumber1 = enqmobileNumber1;
	}

	public String getEnqmobileNumber2() {
		return enqmobileNumber2;
	}

	public void setEnqmobileNumber2(String enqmobileNumber2) {
		this.enqmobileNumber2 = enqmobileNumber2;
	}

	public String getEnqOccupation() {
		return enqOccupation;
	}

	public void setEnqOccupation(String enqOccupation) {
		this.enqOccupation = enqOccupation;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getFlatType() {
		return flatType;
	}

	public void setFlatType(String flatType) {
		this.flatType = flatType;
	}

	public String getFlatRemark() {
		return flatRemark;
	}

	public void setFlatRemark(String flatRemark) {
		this.flatRemark = flatRemark;
	}

	public String getFlatBudget() {
		return flatBudget;
	}

	public void setFlatBudget(String flatBudget) {
		this.flatBudget = flatBudget;
	}

	public String getEnquirysourceId() {
		return enquirysourceId;
	}

	public void setEnquirysourceId(String enquirysourceId) {
		this.enquirysourceId = enquirysourceId;
	}

	public String getSubsourceId() {
		return subsourceId;
	}

	public void setSubsourceId(String subsourceId) {
		this.subsourceId = subsourceId;
	}

	public String getFollowupDate() {
		return followupDate;
	}

	public void setFollowupDate(String followupDate) {
		this.followupDate = followupDate;
	}

	public String getEnqStatus() {
		return enqStatus;
	}

	public void setEnqStatus(String enqStatus) {
		this.enqStatus = enqStatus;
	}

	public String getEnqRemark() {
		return enqRemark;
	}

	public void setEnqRemark(String enqRemark) {
		this.enqRemark = enqRemark;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}


}

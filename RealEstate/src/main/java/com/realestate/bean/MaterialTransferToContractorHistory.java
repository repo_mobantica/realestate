package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "materialtransfertocontractorhistories")
public class MaterialTransferToContractorHistory {

	@Id
    private long materialTransferHistoryId;
	private String materialTransferId;
	private String itemId;
    private Double transferPieces;
    private Double transferStock;
    
    @Transient
    private String itemName;
    @Transient
    private String subsuppliertypeId;
    
   public MaterialTransferToContractorHistory() {}

public MaterialTransferToContractorHistory(long materialTransferHistoryId, String materialTransferId, String itemId,
		Double transferPieces) {
	super();
	this.materialTransferHistoryId = materialTransferHistoryId;
	this.materialTransferId = materialTransferId;
	this.itemId = itemId;
	this.transferPieces = transferPieces;
}

public long getMaterialTransferHistoryId() {
	return materialTransferHistoryId;
}

public void setMaterialTransferHistoryId(long materialTransferHistoryId) {
	this.materialTransferHistoryId = materialTransferHistoryId;
}

public String getMaterialTransferId() {
	return materialTransferId;
}

public void setMaterialTransferId(String materialTransferId) {
	this.materialTransferId = materialTransferId;
}

public String getItemId() {
	return itemId;
}

public void setItemId(String itemId) {
	this.itemId = itemId;
}

public Double getTransferPieces() {
	return transferPieces;
}

public void setTransferPieces(Double transferPieces) {
	this.transferPieces = transferPieces;
}

public String getItemName() {
	return itemName;
}

public void setItemName(String itemName) {
	this.itemName = itemName;
}

public String getSubsuppliertypeId() {
	return subsuppliertypeId;
}

public void setSubsuppliertypeId(String subsuppliertypeId) {
	this.subsuppliertypeId = subsuppliertypeId;
}

public Double getTransferStock() {
	return transferStock;
}

public void setTransferStock(Double transferStock) {
	this.transferStock = transferStock;
}
	   
}

package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection ="companyaccountpaymentdetails")
public class CompanyAccountPaymentDetails 
{
   @Id
   private String paymentId;
   private String companyBankId;
   private long   currentBalance;
   
	public CompanyAccountPaymentDetails() 
	{

	}

	public CompanyAccountPaymentDetails(String paymentId, String companyBankId, long currentBalance)
	{
		super();
		this.paymentId = paymentId;
		this.companyBankId = companyBankId;
		this.currentBalance = currentBalance;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public String getCompanyBankId() {
		return companyBankId;
	}

	public void setCompanyBankId(String companyBankId) {
		this.companyBankId = companyBankId;
	}

	public long getCurrentBalance() {
		return currentBalance;
	}

	public void setCurrentBalance(long currentBalance) {
		this.currentBalance = currentBalance;
	}
   
}

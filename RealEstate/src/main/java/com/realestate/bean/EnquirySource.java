package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "enquirysources")
@CompoundIndexes({@CompoundIndex(name="enquirysourceNameIndex", unique= true, def="{'enquirysourceName':1}")})
public class EnquirySource {
	
    @Id
    private String enquirysourceId;
    private String enquirysourceName;
    private String creationDate;
    private String updateDate;
    private String userName;

    public EnquirySource()
    {
    	
    }

	public EnquirySource(String enquirysourceId, String enquirysourceName, String creationDate, String updateDate,
			String userName) {
		super();
		this.enquirysourceId = enquirysourceId;
		this.enquirysourceName = enquirysourceName;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getEnquirysourceId() {
		return enquirysourceId;
	}

	public void setEnquirysourceId(String enquirysourceId) {
		this.enquirysourceId = enquirysourceId;
	}

	public String getEnquirysourceName() {
		return enquirysourceName;
	}

	public void setEnquirysourceName(String enquirysourceName) {
		this.enquirysourceName = enquirysourceName;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
    
    
    
}

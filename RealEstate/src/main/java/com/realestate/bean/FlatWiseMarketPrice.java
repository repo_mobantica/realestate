package com.realestate.bean;

public class FlatWiseMarketPrice {

	private String projectId;
	private String buildingId;
	private String wingId;
	private String flatNumber;  


	private String editRate;
	private String editflatbasicCost;
	private String editInfrastructure;
	private String editaggValue;

	private String stampDutyPer; 
	private String editStampDuty;

	private String registrationPer;
	private String editRegistration;

	private String editLegalCharges;

	private String totalGstPer;
	private String editTotalGst;

	private String edittotalAmount;

	private String tdsAmount;

	public String getTdsAmount() {
		return tdsAmount;
	}

	public void setTdsAmount(String tdsAmount) {
		this.tdsAmount = tdsAmount;
	}

	public FlatWiseMarketPrice() {}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	public String getWingId() {
		return wingId;
	}

	public void setWingId(String wingId) {
		this.wingId = wingId;
	}

	public String getFlatNumber() {
		return flatNumber;
	}

	public void setFlatNumber(String flatNumber) {
		this.flatNumber = flatNumber;
	}

	public String getEditRate() {
		return editRate;
	}

	public void setEditRate(String editRate) {
		this.editRate = editRate;
	}

	public String getEditflatbasicCost() {
		return editflatbasicCost;
	}

	public void setEditflatbasicCost(String editflatbasicCost) {
		this.editflatbasicCost = editflatbasicCost;
	}

	public String getEditInfrastructure() {
		return editInfrastructure;
	}

	public void setEditInfrastructure(String editInfrastructure) {
		this.editInfrastructure = editInfrastructure;
	}

	public String getEditaggValue() {
		return editaggValue;
	}

	public void setEditaggValue(String editaggValue) {
		this.editaggValue = editaggValue;
	}

	public String getStampDutyPer() {
		return stampDutyPer;
	}

	public void setStampDutyPer(String stampDutyPer) {
		this.stampDutyPer = stampDutyPer;
	}

	public String getEditStampDuty() {
		return editStampDuty;
	}

	public void setEditStampDuty(String editStampDuty) {
		this.editStampDuty = editStampDuty;
	}

	public String getRegistrationPer() {
		return registrationPer;
	}

	public void setRegistrationPer(String registrationPer) {
		this.registrationPer = registrationPer;
	}

	public String getEditRegistration() {
		return editRegistration;
	}

	public void setEditRegistration(String editRegistration) {
		this.editRegistration = editRegistration;
	}

	public String getEditLegalCharges() {
		return editLegalCharges;
	}

	public void setEditLegalCharges(String editLegalCharges) {
		this.editLegalCharges = editLegalCharges;
	}

	public String getTotalGstPer() {
		return totalGstPer;
	}

	public void setTotalGstPer(String totalGstPer) {
		this.totalGstPer = totalGstPer;
	}

	public String getEditTotalGst() {
		return editTotalGst;
	}

	public void setEditTotalGst(String editTotalGst) {
		this.editTotalGst = editTotalGst;
	}

	public String getEdittotalAmount() {
		return edittotalAmount;
	}

	public void setEdittotalAmount(String edittotalAmount) {
		this.edittotalAmount = edittotalAmount;
	}

}

package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="usermodel")
public class UserModel 
{
   @Id
   private String userModelId;
   private String employeeId;
   private String role;
   
   //Menu List
   private String administrator;
   
   private String preSaleaManagment;
   private String salesManagment;
   
   private String flatStatusReport;
   private String enquiryReport;
   private String bookingReport;
   private String customerPaymentReport;
   private String demandPaymentReport;
   private String parkingReport;
   private String agentReport;
   private String customerReport;
   
   private String addTask;
   
   private String hrANDpayRollManagement;
   
   private String consultancyMaster;
   private String architecturalSection;
   
   private String contractorMagagement;
   private String projectEngineering;
   private String jrEngineering;
   private String qualityEngineering;
   private String srSupervisor;
   private String jrSupervisor;
   
   private String materialRequisitionList;
   
   private String storeManagement;
   private String storeStocks;
   private String materialTransferToContractor;

   private String customerPaymentStatus;
   private String supplierPaymentBill;
   private String agentPayment;

   private String projectDevelopmentWork;
   private String architectsCertificate;
   private String projectSpecificationMaster;
   
   private String userManagement;
   
   private String companyDocuments;
   private String licensingDepartment;
   private String reraDocuments;
   //Menu List End
   
   private String status;

   @Transient
   private String employeeName;
   
   @Transient
   private String userId;
   
   @Transient
   private String passWord;
   
   private String creationDate;
   private String updateDate;
   private String userName;
   
   public UserModel()
   {
	
   }

public UserModel(String userModelId, String employeeId, String role, String administrator, String preSaleaManagment,
		String salesManagment, String flatStatusReport, String enquiryReport, String bookingReport,
		String customerPaymentReport, String demandPaymentReport, String parkingReport, String agentReport,
		String customerReport, String addTask, String hrANDpayRollManagement, String consultancyMaster,
		String architecturalSection, String contractorMagagement, String projectEngineering, String jrEngineering,
		String qualityEngineering, String srSupervisor, String jrSupervisor, String materialRequisitionList,
		String storeManagement, String storeStocks, String materialTransferToContractor, String customerPaymentStatus,
		String supplierPaymentBill, String agentPayment, String projectDevelopmentWork, String architectsCertificate,
		String projectSpecificationMaster, String userManagement, String companyDocuments,String licensingDepartment, String reraDocuments,
		String status, String creationDate, String updateDate, String userName) {
	super();
	this.userModelId = userModelId;
	this.employeeId = employeeId;
	this.role = role;
	this.administrator = administrator;
	this.preSaleaManagment = preSaleaManagment;
	this.salesManagment = salesManagment;
	this.flatStatusReport = flatStatusReport;
	this.enquiryReport = enquiryReport;
	this.bookingReport = bookingReport;
	this.customerPaymentReport = customerPaymentReport;
	this.demandPaymentReport = demandPaymentReport;
	this.parkingReport = parkingReport;
	this.agentReport = agentReport;
	this.customerReport = customerReport;
	this.addTask = addTask;
	this.hrANDpayRollManagement = hrANDpayRollManagement;
	this.consultancyMaster = consultancyMaster;
	this.architecturalSection = architecturalSection;
	this.contractorMagagement = contractorMagagement;
	this.projectEngineering = projectEngineering;
	this.jrEngineering = jrEngineering;
	this.qualityEngineering = qualityEngineering;
	this.srSupervisor = srSupervisor;
	this.jrSupervisor = jrSupervisor;
	this.materialRequisitionList = materialRequisitionList;
	this.storeManagement = storeManagement;
	this.storeStocks = storeStocks;
	this.materialTransferToContractor = materialTransferToContractor;
	this.customerPaymentStatus = customerPaymentStatus;
	this.supplierPaymentBill = supplierPaymentBill;
	this.agentPayment = agentPayment;
	this.projectDevelopmentWork = projectDevelopmentWork;
	this.architectsCertificate = architectsCertificate;
	this.projectSpecificationMaster = projectSpecificationMaster;
	this.userManagement = userManagement;
	this.companyDocuments = companyDocuments;
	this.licensingDepartment=licensingDepartment;
	this.reraDocuments = reraDocuments;
	this.status = status;
	this.creationDate = creationDate;
	this.updateDate = updateDate;
	this.userName = userName;
}

public String getUserModelId() {
	return userModelId;
}

public void setUserModelId(String userModelId) {
	this.userModelId = userModelId;
}

public String getEmployeeId() {
	return employeeId;
}

public void setEmployeeId(String employeeId) {
	this.employeeId = employeeId;
}

public String getRole() {
	return role;
}

public void setRole(String role) {
	this.role = role;
}

public String getAdministrator() {
	return administrator;
}

public void setAdministrator(String administrator) {
	this.administrator = administrator;
}

public String getPreSaleaManagment() {
	return preSaleaManagment;
}

public void setPreSaleaManagment(String preSaleaManagment) {
	this.preSaleaManagment = preSaleaManagment;
}

public String getSalesManagment() {
	return salesManagment;
}

public void setSalesManagment(String salesManagment) {
	this.salesManagment = salesManagment;
}

public String getFlatStatusReport() {
	return flatStatusReport;
}

public void setFlatStatusReport(String flatStatusReport) {
	this.flatStatusReport = flatStatusReport;
}

public String getEnquiryReport() {
	return enquiryReport;
}

public void setEnquiryReport(String enquiryReport) {
	this.enquiryReport = enquiryReport;
}

public String getBookingReport() {
	return bookingReport;
}

public void setBookingReport(String bookingReport) {
	this.bookingReport = bookingReport;
}

public String getCustomerPaymentReport() {
	return customerPaymentReport;
}

public void setCustomerPaymentReport(String customerPaymentReport) {
	this.customerPaymentReport = customerPaymentReport;
}

public String getDemandPaymentReport() {
	return demandPaymentReport;
}

public void setDemandPaymentReport(String demandPaymentReport) {
	this.demandPaymentReport = demandPaymentReport;
}

public String getParkingReport() {
	return parkingReport;
}

public void setParkingReport(String parkingReport) {
	this.parkingReport = parkingReport;
}

public String getAgentReport() {
	return agentReport;
}

public void setAgentReport(String agentReport) {
	this.agentReport = agentReport;
}

public String getCustomerReport() {
	return customerReport;
}

public void setCustomerReport(String customerReport) {
	this.customerReport = customerReport;
}

public String getAddTask() {
	return addTask;
}

public void setAddTask(String addTask) {
	this.addTask = addTask;
}

public String getHrANDpayRollManagement() {
	return hrANDpayRollManagement;
}

public void setHrANDpayRollManagement(String hrANDpayRollManagement) {
	this.hrANDpayRollManagement = hrANDpayRollManagement;
}

public String getConsultancyMaster() {
	return consultancyMaster;
}

public void setConsultancyMaster(String consultancyMaster) {
	this.consultancyMaster = consultancyMaster;
}

public String getArchitecturalSection() {
	return architecturalSection;
}

public void setArchitecturalSection(String architecturalSection) {
	this.architecturalSection = architecturalSection;
}

public String getContractorMagagement() {
	return contractorMagagement;
}

public void setContractorMagagement(String contractorMagagement) {
	this.contractorMagagement = contractorMagagement;
}

public String getProjectEngineering() {
	return projectEngineering;
}

public void setProjectEngineering(String projectEngineering) {
	this.projectEngineering = projectEngineering;
}

public String getJrEngineering() {
	return jrEngineering;
}

public void setJrEngineering(String jrEngineering) {
	this.jrEngineering = jrEngineering;
}

public String getQualityEngineering() {
	return qualityEngineering;
}

public void setQualityEngineering(String qualityEngineering) {
	this.qualityEngineering = qualityEngineering;
}

public String getSrSupervisor() {
	return srSupervisor;
}

public void setSrSupervisor(String srSupervisor) {
	this.srSupervisor = srSupervisor;
}

public String getJrSupervisor() {
	return jrSupervisor;
}

public void setJrSupervisor(String jrSupervisor) {
	this.jrSupervisor = jrSupervisor;
}

public String getMaterialRequisitionList() {
	return materialRequisitionList;
}

public void setMaterialRequisitionList(String materialRequisitionList) {
	this.materialRequisitionList = materialRequisitionList;
}

public String getStoreManagement() {
	return storeManagement;
}

public void setStoreManagement(String storeManagement) {
	this.storeManagement = storeManagement;
}

public String getStoreStocks() {
	return storeStocks;
}

public void setStoreStocks(String storeStocks) {
	this.storeStocks = storeStocks;
}

public String getMaterialTransferToContractor() {
	return materialTransferToContractor;
}

public void setMaterialTransferToContractor(String materialTransferToContractor) {
	this.materialTransferToContractor = materialTransferToContractor;
}

public String getCustomerPaymentStatus() {
	return customerPaymentStatus;
}

public void setCustomerPaymentStatus(String customerPaymentStatus) {
	this.customerPaymentStatus = customerPaymentStatus;
}

public String getSupplierPaymentBill() {
	return supplierPaymentBill;
}

public void setSupplierPaymentBill(String supplierPaymentBill) {
	this.supplierPaymentBill = supplierPaymentBill;
}

public String getAgentPayment() {
	return agentPayment;
}

public void setAgentPayment(String agentPayment) {
	this.agentPayment = agentPayment;
}

public String getProjectDevelopmentWork() {
	return projectDevelopmentWork;
}

public void setProjectDevelopmentWork(String projectDevelopmentWork) {
	this.projectDevelopmentWork = projectDevelopmentWork;
}

public String getArchitectsCertificate() {
	return architectsCertificate;
}

public void setArchitectsCertificate(String architectsCertificate) {
	this.architectsCertificate = architectsCertificate;
}

public String getProjectSpecificationMaster() {
	return projectSpecificationMaster;
}

public void setProjectSpecificationMaster(String projectSpecificationMaster) {
	this.projectSpecificationMaster = projectSpecificationMaster;
}

public String getUserManagement() {
	return userManagement;
}

public void setUserManagement(String userManagement) {
	this.userManagement = userManagement;
}

public String getCompanyDocuments() {
	return companyDocuments;
}

public void setCompanyDocuments(String companyDocuments) {
	this.companyDocuments = companyDocuments;
}

public String getReraDocuments() {
	return reraDocuments;
}

public void setReraDocuments(String reraDocuments) {
	this.reraDocuments = reraDocuments;
}

public String getStatus() {
	return status;
}

public void setStatus(String status) {
	this.status = status;
}

public String getCreationDate() {
	return creationDate;
}

public void setCreationDate(String creationDate) {
	this.creationDate = creationDate;
}

public String getUpdateDate() {
	return updateDate;
}

public void setUpdateDate(String updateDate) {
	this.updateDate = updateDate;
}

public String getUserName() {
	return userName;
}

public void setUserName(String userName) {
	this.userName = userName;
}

public String getEmployeeName() {
	return employeeName;
}

public void setEmployeeName(String employeeName) {
	this.employeeName = employeeName;
}

public String getUserId() {
	return userId;
}

public void setUserId(String userId) {
	this.userId = userId;
}

public String getPassWord() {
	return passWord;
}

public void setPassWord(String passWord) {
	this.passWord = passWord;
}

public String getLicensingDepartment() {
	return licensingDepartment;
}

public void setLicensingDepartment(String licensingDepartment) {
	this.licensingDepartment = licensingDepartment;
}

}

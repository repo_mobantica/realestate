package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "banks")
public class Bank 
{
	@Id
	private String bankId;
	private String bankName;
	private String branchName;
	private String bankAddress;
	private String countryId;
	private String stateId;
	private String cityId;
	private String locationareaId;
	private String areaPincode;
	private String bankPhoneno;
	private String bankifscCode;
	private String apfnumber;
	
	private String creationDate;
	private String updateDate;
	private String userName;
	
	public Bank() 
	{	}

	public Bank(String bankId, String bankName, String branchName, String bankAddress, String countryId, String stateId,
			String cityId, String locationareaId, String areaPincode, String bankPhoneno, String bankifscCode,
			String apfnumber, String creationDate, String updateDate, String userName) {
		super();
		this.bankId = bankId;
		this.bankName = bankName;
		this.branchName = branchName;
		this.bankAddress = bankAddress;
		this.countryId = countryId;
		this.stateId = stateId;
		this.cityId = cityId;
		this.locationareaId = locationareaId;
		this.areaPincode = areaPincode;
		this.bankPhoneno = bankPhoneno;
		this.bankifscCode = bankifscCode;
		this.apfnumber = apfnumber;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getBankId() {
		return bankId;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getBankAddress() {
		return bankAddress;
	}

	public void setBankAddress(String bankAddress) {
		this.bankAddress = bankAddress;
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getStateId() {
		return stateId;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getLocationareaId() {
		return locationareaId;
	}

	public void setLocationareaId(String locationareaId) {
		this.locationareaId = locationareaId;
	}

	public String getAreaPincode() {
		return areaPincode;
	}

	public void setAreaPincode(String areaPincode) {
		this.areaPincode = areaPincode;
	}

	public String getBankPhoneno() {
		return bankPhoneno;
	}

	public void setBankPhoneno(String bankPhoneno) {
		this.bankPhoneno = bankPhoneno;
	}

	public String getBankifscCode() {
		return bankifscCode;
	}

	public void setBankifscCode(String bankifscCode) {
		this.bankifscCode = bankifscCode;
	}

	public String getApfnumber() {
		return apfnumber;
	}

	public void setApfnumber(String apfnumber) {
		this.apfnumber = apfnumber;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}

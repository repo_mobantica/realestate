package com.realestate.services;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import com.realestate.bean.ProjectBuilding;
import com.realestate.repository.ProjectRepository;

@Controller
@RequestMapping("/")
public class ProjectBuildingDetailsExcelView extends AbstractXlsView
{
    @Autowired
    ProjectRepository projectRepository;
    
    @Autowired
    MongoTemplate mongoTemplate;
	
	
	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
 
		try
		{
			response.setHeader("Content-disposition", "attachment; filename=\"BuildingDetails.xlsx\"");
			
			List<ProjectBuilding> projectBuildngDetails = (List<ProjectBuilding>) model.get("projectsBuildingDetails");
			
			HSSFSheet sheet = (HSSFSheet) workbook.createSheet("Building Details");
			
			HSSFRow header = sheet.createRow(0);
			
			header.createCell(0).setCellValue("Project Name");
			header.createCell(1).setCellValue("Building Name");
			header.createCell(2).setCellValue("No Of Wing");
			header.createCell(3).setCellValue("Status");
			
			int counter = 1;
			
			for (ProjectBuilding projectBuildingDetailsData : projectBuildngDetails)
			{
			  HSSFRow row = sheet.createRow(counter++);

			  row.createCell(0).setCellValue(""+projectBuildingDetailsData.getProjectId());
			  row.createCell(1).setCellValue(""+projectBuildingDetailsData.getBuildingName());
			  row.createCell(2).setCellValue(""+projectBuildingDetailsData.getNumberofWing());
		      row.createCell(3).setCellValue(""+projectBuildingDetailsData.getBuildingStatus());
			  
			}
		 }
		 catch(Exception e)
		 {

		 }
	}
    
}

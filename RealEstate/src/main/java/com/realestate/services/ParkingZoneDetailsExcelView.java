package com.realestate.services;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import com.realestate.bean.ParkingZone;

@Controller
@RequestMapping("/")
public class ParkingZoneDetailsExcelView extends AbstractXlsView
{
	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
 
		try
		{
			response.setHeader("Content-disposition", "attachment; filename=\"ParkingZoneDetails.xlsx\"");
			
			List<ParkingZone> parkingZoneDetails = (List<ParkingZone>) model.get("parkingZoneDetails");
			
			HSSFSheet sheet = (HSSFSheet) workbook.createSheet("Parking Zone Details");
			
			HSSFRow header = sheet.createRow(0);
			
			header.createCell(0).setCellValue("Project Name");
			header.createCell(1).setCellValue("Building Name");
			header.createCell(2).setCellValue("Wing Name");
			header.createCell(3).setCellValue("Floor Name");
			header.createCell(4).setCellValue("Parking Type");
			header.createCell(5).setCellValue("Parking Number");
			header.createCell(6).setCellValue("Length(Sq.Mtr)");
			header.createCell(7).setCellValue("Width(Sq.Mtr)");
			header.createCell(8).setCellValue("Area In(Sq.Mtr)");
			header.createCell(9).setCellValue("Area In(Sq.Ft.)");
			
			int counter = 1;
			
			for (ParkingZone parking : parkingZoneDetails)
			{
			  HSSFRow row = sheet.createRow(counter++);

			  row.createCell(0).setCellValue(""+parking.getProjectId());
			  row.createCell(1).setCellValue(""+parking.getBuildingId());
			  row.createCell(2).setCellValue(""+parking.getWingId());
			  row.createCell(3).setCellValue(""+parking.getFloorId());
		      row.createCell(4).setCellValue(""+parking.getParkingType());
		      row.createCell(5).setCellValue(""+parking.getParkingNumber());
			  row.createCell(6).setCellValue(""+parking.getParkingLength());
		      row.createCell(7).setCellValue(""+parking.getParkingWidth());
		      row.createCell(8).setCellValue(""+parking.getAreaInSqMtr());
			  row.createCell(9).setCellValue(""+parking.getAreaInSqFt());
			  
			}
		 }
		 catch(Exception e)
		 {

		 }
	}
    
}

package com.realestate.services;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import com.realestate.bean.Flat;
import com.realestate.repository.ProjectRepository;

@Controller
@RequestMapping("/")
public class FlatDetailsExcelView extends AbstractXlsView
{
    @Autowired
    ProjectRepository projectRepository;
    
    @Autowired
    MongoTemplate mongoTemplate;
	
	
	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
 
		try
		{
			response.setHeader("Content-disposition", "attachment; filename=\"FlatDetails.xlsx\"");
			
			List<Flat> flatDetails = (List<Flat>) model.get("flatDetails");
			
			HSSFSheet sheet = (HSSFSheet) workbook.createSheet("Flat Details");
			
			HSSFRow header = sheet.createRow(0);
			
			header.createCell(0).setCellValue("Flat Number");
			header.createCell(1).setCellValue("Project Name");
			header.createCell(2).setCellValue("Building Name");
			header.createCell(3).setCellValue("Wing Name");
			header.createCell(4).setCellValue("Floor Name");
			header.createCell(5).setCellValue("Flat Facing Type");
			header.createCell(6).setCellValue("Flat Type");
			header.createCell(7).setCellValue("Flat Carpet Area(Sq.M)");
			header.createCell(8).setCellValue("Open Balcony Area(Sq.M)");
			header.createCell(9).setCellValue("Enclose Balcony Area(Sq.M)");
			header.createCell(10).setCellValue("Dry Terrace Area(Sq.M)");
			header.createCell(11).setCellValue("Flat Area(Sq.M)");
			header.createCell(12).setCellValue("Loading %");
			header.createCell(13).setCellValue("Project Loading");
			header.createCell(14).setCellValue("Net Flat Area(Sq.M)");
			header.createCell(15).setCellValue("Flat Area(Sq.Ft)");
			header.createCell(16).setCellValue("Flat Cost Per Sq.Ft");
			header.createCell(17).setCellValue("Floor Rise");
			header.createCell(18).setCellValue("Net Flat Cost Per Sq.Ft");
			header.createCell(19).setCellValue("Minimum Flat Cost Per Sq.Ft");
			header.createCell(20).setCellValue("Total Flat Basic Cost");
			
			int counter = 1;
			
			for (Flat flat : flatDetails)
			{
			  HSSFRow row = sheet.createRow(counter++);

			  row.createCell(0).setCellValue(""+flat.getFlatNumber());
			  row.createCell(1).setCellValue(""+flat.getProjectId());
			  row.createCell(2).setCellValue(""+flat.getBuildingId());
			  row.createCell(3).setCellValue(""+flat.getWingId());
		      row.createCell(4).setCellValue(""+flat.getFloorId());
		      row.createCell(5).setCellValue(""+flat.getFlatfacingName());
			  row.createCell(6).setCellValue(""+flat.getFlatType());
		      row.createCell(7).setCellValue(""+flat.getCarpetArea());
		      row.createCell(8).setCellValue(""+flat.getTerraceArea());
			  row.createCell(9).setCellValue(""+flat.getBalconyArea());
		      row.createCell(10).setCellValue(""+flat.getDryterraceArea());
		      row.createCell(11).setCellValue(""+flat.getFlatArea());
			  row.createCell(12).setCellValue(""+flat.getLoading());
			  row.createCell(13).setCellValue(""+flat.getLoadingpercentage());
		      row.createCell(14).setCellValue(""+flat.getFlatAreawithLoadingInM());
		      row.createCell(15).setCellValue(""+flat.getFlatAreawithLoadingInFt());
			  row.createCell(16).setCellValue(""+flat.getFlatCostwithotfloorise());
		      row.createCell(17).setCellValue(""+flat.getFloorRise());
		      row.createCell(18).setCellValue(""+flat.getFlatCost());
			  row.createCell(19).setCellValue(""+flat.getFlatminimumCost());
		      row.createCell(20).setCellValue(""+flat.getFlatbasicCost());
			  
			}
		 }
		 catch(Exception e)
		 {

		 }
	}
    
}

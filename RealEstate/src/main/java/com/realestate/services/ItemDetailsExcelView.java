package com.realestate.services;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import com.realestate.bean.Item;

@Controller
@RequestMapping("/")
public class ItemDetailsExcelView extends AbstractXlsView
{
	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
 
		try
		{
			response.setHeader("Content-disposition", "attachment; filename=\"ItemDetails.xlsx\"");
			
			List<Item> itemDetails = (List<Item>) model.get("itemDetails");
			
			HSSFSheet sheet = (HSSFSheet) workbook.createSheet("Item Details");
			
			HSSFRow header = sheet.createRow(0);
			
			header.createCell(0).setCellValue("Item Main Category");
			header.createCell(1).setCellValue("Item Main Sub-Category");
			header.createCell(2).setCellValue("Item Name");
			header.createCell(3).setCellValue("Item Initial Price");
			header.createCell(4).setCellValue("Item Initial Discount %");
			header.createCell(5).setCellValue("Minimum Stock Qty");
			header.createCell(6).setCellValue("Current Stock Qty");
			header.createCell(7).setCellValue("HSN Code");
			header.createCell(8).setCellValue("GST %");
			
			int counter = 1;
			
			for (Item item : itemDetails)
			{
			  HSSFRow row = sheet.createRow(counter++);

			  row.createCell(0).setCellValue(""+item.getSuppliertypeId());
			  row.createCell(1).setCellValue(""+item.getSubsuppliertypeId());
			  row.createCell(2).setCellValue(""+item.getItemName());
			  row.createCell(3).setCellValue(""+item.getIntemInitialPrice());
			  row.createCell(4).setCellValue(""+item.getItemInitialDiscount());
			  row.createCell(5).setCellValue(""+item.getMinimumStock());
			  row.createCell(6).setCellValue(""+item.getCurrentStock());
			  row.createCell(7).setCellValue(""+item.getHsnCode());
			  row.createCell(8).setCellValue(""+item.getGstPer());
			}
		 }
		 catch(Exception e)
		 {

		 }
	}
    
}

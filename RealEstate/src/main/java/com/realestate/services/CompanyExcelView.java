package com.realestate.services;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import com.realestate.bean.Company;

public class CompanyExcelView extends AbstractXlsView
{

	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
 
		response.setHeader("Content-disposition", "attachment; filename=\"CompanyData.xlsx\"");
		
		List<Company> companies = (List<Company>) model.get("companies");
		
		HSSFSheet sheet = (HSSFSheet) workbook.createSheet("Company Data");
		
		HSSFRow header = sheet.createRow(0);
		
		header.createCell(0).setCellValue("Company Name");
		header.createCell(1).setCellValue("Company Type");
		header.createCell(2).setCellValue("PAN No.");
		header.createCell(3).setCellValue("Company Registration No");
		header.createCell(4).setCellValue("TAN Number");
		header.createCell(5).setCellValue("Company PF A/c No");
		header.createCell(6).setCellValue("GST No");
		header.createCell(7).setCellValue("Address");
		header.createCell(8).setCellValue("Country");
		header.createCell(9).setCellValue("State");
		header.createCell(10).setCellValue("City");
		header.createCell(11).setCellValue("Area");
		header.createCell(12).setCellValue("Pin Code");
		header.createCell(13).setCellValue("Office Phone No");
		
		int counter = 1;
		for (Company companyData : companies)
		{
		  HSSFRow row = sheet.createRow(counter++);
		  row.createCell(0).setCellValue(companyData.getCompanyName());
		  row.createCell(1).setCellValue(companyData.getCompanyType());
		  row.createCell(2).setCellValue(companyData.getCompanyPanno());
		  
		  if(companyData.getCompanyRegistrationno()!=null || !companyData.getCompanyRegistrationno().equals(""))
		  {
		    row.createCell(3).setCellValue(companyData.getCompanyRegistrationno());
		  }
		  else
		  {
		    row.createCell(3).setCellValue("");
		  }
		  
		  if(companyData.getCompanyTanno()!=null || !companyData.getCompanyTanno().equals(""))
		  {
		    row.createCell(4).setCellValue(companyData.getCompanyTanno());
		  }
		  else
		  {
		    row.createCell(4).setCellValue("");  
		  }
		  
		  if(companyData.getCompanyPfno()!=null || !companyData.getCompanyPfno().equals(""))
		  {
		    row.createCell(5).setCellValue(companyData.getCompanyPfno());
		  }
		  else
		  {
			row.createCell(5).setCellValue("");
		  }
		  
		  row.createCell(6).setCellValue(companyData.getCompanyGstno());
		  row.createCell(7).setCellValue(companyData.getCompanyAddress());
		  row.createCell(8).setCellValue(companyData.getCountryId());
		  row.createCell(9).setCellValue(companyData.getStateId());
		  row.createCell(10).setCellValue(companyData.getCityId());
		  row.createCell(11).setCellValue(companyData.getLocationareaId());
		  row.createCell(12).setCellValue(companyData.getCompanyPincode());
		  row.createCell(13).setCellValue(companyData.getCompanyPhoneno()); 
		}
	}
    
}

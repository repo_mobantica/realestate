package com.realestate.services;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import com.realestate.bean.ContractorType;

@Controller
@RequestMapping("/")
public class ContractorTypeDetailsExcelView extends AbstractXlsView
{
	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
 
		try
		{
			response.setHeader("Content-disposition", "attachment; filename=\"ContractorTypeDetails.xlsx\"");
			
			List<ContractorType> contractorTypeDetails = (List<ContractorType>) model.get("contractorTypeDetails");
			
			HSSFSheet sheet = (HSSFSheet) workbook.createSheet("Contractor Type Details");
			
			HSSFRow header = sheet.createRow(0);
			
			header.createCell(0).setCellValue("Contractor Type");
			
			int counter = 1;
			
			for (ContractorType contractorType : contractorTypeDetails)
			{
			  HSSFRow row = sheet.createRow(counter++);

			  row.createCell(0).setCellValue(""+contractorType.getContractorType());
			}
		 }
		 catch(Exception e)
		 {

		 }
	}
    
}

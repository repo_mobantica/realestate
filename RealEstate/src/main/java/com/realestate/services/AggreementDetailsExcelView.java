package com.realestate.services;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import com.realestate.bean.Aggreement;

@Controller
@RequestMapping("/")
public class AggreementDetailsExcelView extends AbstractXlsView
{
	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
 
		try
		{
			response.setHeader("Content-disposition", "attachment; filename=\"AgreementDetails.xlsx\"");
			
			List<Aggreement> aggreementDetails = (List<Aggreement>) model.get("aggreementDetails");
			
			HSSFSheet sheet = (HSSFSheet) workbook.createSheet("Agreement Details");
			
			HSSFRow header = sheet.createRow(0);
			
			header.createCell(0).setCellValue("Booking Id");
			header.createCell(1).setCellValue("Agreement Id");
			header.createCell(2).setCellValue("Agreement Number");
			header.createCell(3).setCellValue("Agreement Date");
			header.createCell(4).setCellValue("First Name(1st Applicant)");
			header.createCell(5).setCellValue("Second Name(1st Applicant)");
			header.createCell(6).setCellValue("Last Name(1st Applicant)");
			
			header.createCell(7).setCellValue("EmailId");
			header.createCell(9).setCellValue("Address");
			header.createCell(10).setCellValue("Country");
			header.createCell(11).setCellValue("State");
			header.createCell(12).setCellValue("City");
			header.createCell(13).setCellValue("Area");
			header.createCell(14).setCellValue("Pin Code");
			header.createCell(15).setCellValue("Occupation");
			header.createCell(16).setCellValue("Purpose Of Flat");
			header.createCell(17).setCellValue("Project Name");
			header.createCell(18).setCellValue("Building");
			header.createCell(19).setCellValue("Wing");
			header.createCell(20).setCellValue("Floor Name");
			header.createCell(21).setCellValue("Flat Number");
			header.createCell(22).setCellValue("Flat Type");
			header.createCell(23).setCellValue("Flat Facing");
			header.createCell(24).setCellValue("Flat Area(SQ.FT)");
			header.createCell(25).setCellValue("Flat Cost(Sq.Ft)");
			header.createCell(26).setCellValue("Floor Rise");
			header.createCell(27).setCellValue("Net Flat Cost(Sq.Ft)");
			header.createCell(28).setCellValue("Flat Basic Cost");
			header.createCell(29).setCellValue("Parking Floor");
			header.createCell(30).setCellValue("Parking Zone");
			header.createCell(31).setCellValue("Agent Firm Name");
			header.createCell(32).setCellValue("Infrastructure Charges");
			header.createCell(33).setCellValue("Aggreement Value");
			header.createCell(34).setCellValue("Stamp Duty Per");
			header.createCell(35).setCellValue("Stamp Duty Amt");
			header.createCell(36).setCellValue("Registration Per");
			header.createCell(37).setCellValue("Registration Amt");
			header.createCell(38).setCellValue("Handling Charges");
			header.createCell(39).setCellValue("GST Per");
			header.createCell(40).setCellValue("GST Amount");
			header.createCell(41).setCellValue("Grand Total");
			header.createCell(42).setCellValue("TDS 1% Amount");
			header.createCell(43).setCellValue("Booking Date");
			
			int counter = 1;
			
			for (Aggreement agreement : aggreementDetails)
			{
			  HSSFRow row = sheet.createRow(counter++);

			  /*row.createCell(0).setCellValue(""+agreement.getEnquiryId());
			  row.createCell(1).setCellValue(""+agreement.getBookingId());
			  row.createCell(2).setCellValue(""+agreement.getBookingfirstname());
			  row.createCell(3).setCellValue(""+agreement.getBookingmiddlename());
			  row.createCell(4).setCellValue(""+agreement.getBookinglastname());
			  row.createCell(5).setCellValue(""+agreement.getBookingmobileNumber1());
			  row.createCell(6).setCellValue(""+agreement.getBookingmobileNumber2());
			  row.createCell(7).setCellValue(""+agreement.getBookingEmail());
			  row.createCell(9).setCellValue(""+agreement.getBookingaddress());
			  row.createCell(10).setCellValue(""+agreement.getCountryName());
			  row.createCell(11).setCellValue(""+agreement.getStateName());
			  row.createCell(12).setCellValue(""+agreement.getCityName());
			  row.createCell(13).setCellValue(""+agreement.getLocationareaName());
			  row.createCell(14).setCellValue(""+agreement.getBookingPincode());
			  row.createCell(15).setCellValue(""+agreement.getBookingOccupation());
			  row.createCell(16).setCellValue(""+agreement.getPurposeOfFlat());
			  row.createCell(17).setCellValue(""+agreement.getProjectName());
			  row.createCell(18).setCellValue(""+agreement.getBuildingName());
			  row.createCell(19).setCellValue(""+agreement.getWingName());
			  row.createCell(20).setCellValue(""+agreement.getFloortypeName());
			  row.createCell(21).setCellValue(""+agreement.getFlatNumber());
			  row.createCell(22).setCellValue(""+agreement.getFlatType());
			  row.createCell(23).setCellValue(""+agreement.getFlatFacing());
			  row.createCell(24).setCellValue(""+agreement.getFlatareainSqFt());
			  row.createCell(25).setCellValue(""+agreement.getFlatCostwithotfloorise());
			  row.createCell(26).setCellValue(""+agreement.getFloorRise());
			  row.createCell(27).setCellValue(""+agreement.getFlatCost());
			  row.createCell(28).setCellValue(""+agreement.getFlatbasicCost());
			  row.createCell(29).setCellValue(""+agreement.getParkingFloor());
			  row.createCell(30).setCellValue(""+agreement.getParkingNumber());
			  row.createCell(31).setCellValue(""+agreement.getAgentName());
			  row.createCell(32).setCellValue(""+agreement.getInfrastructureCharge());
			  row.createCell(33).setCellValue(""+agreement.getAggreementValue1());
			  row.createCell(34).setCellValue(""+agreement.getStampDutyPer());
			  row.createCell(35).setCellValue(""+agreement.getStampDuty1());
			  row.createCell(36).setCellValue(""+agreement.getRegistrationPer());
			  row.createCell(37).setCellValue(""+agreement.getRegistrationCost1());
			  row.createCell(38).setCellValue(""+agreement.getHandlingCharges());
			  row.createCell(39).setCellValue(""+agreement.getGstCost());
			  row.createCell(40).setCellValue(""+agreement.getGstAmount1());
			  row.createCell(41).setCellValue(""+agreement.getGrandTotal1());
			  row.createCell(42).setCellValue(""+agreement.getTds());
			  row.createCell(43).setCellValue(""+agreement.getCreationDate());*/
			}
		 }
		 catch(Exception e)
		 {

		 }
	}
    
}

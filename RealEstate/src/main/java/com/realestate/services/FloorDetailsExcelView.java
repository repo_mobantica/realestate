package com.realestate.services;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import com.realestate.bean.Floor;
import com.realestate.bean.ProjectWing;
import com.realestate.repository.ProjectRepository;

@Controller
@RequestMapping("/")
public class FloorDetailsExcelView extends AbstractXlsView
{
    @Autowired
    ProjectRepository projectRepository;
    
    @Autowired
    MongoTemplate mongoTemplate;
	
	
	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
 
		try
		{
			response.setHeader("Content-disposition", "attachment; filename=\"FloorDetails.xlsx\"");
			
			List<Floor> floorDetails = (List<Floor>) model.get("floorDetails");
			
			HSSFSheet sheet = (HSSFSheet) workbook.createSheet("Floor Details");
			
			HSSFRow header = sheet.createRow(0);
			
			header.createCell(0).setCellValue("Project Name");
			header.createCell(1).setCellValue("Building Name");
			header.createCell(2).setCellValue("Wing Name");
			header.createCell(3).setCellValue("Floor Name");
			header.createCell(4).setCellValue("Floor Rise");
			header.createCell(5).setCellValue("Floor Zone Type");
			header.createCell(6).setCellValue("No. of Open Parking");
			header.createCell(7).setCellValue("No. of Close Parking");
			header.createCell(8).setCellValue("No. of Shops");
			header.createCell(9).setCellValue("No. of Flats");
			
			int counter = 1;
			
			for (Floor floor : floorDetails)
			{
			  HSSFRow row = sheet.createRow(counter++);

			  row.createCell(0).setCellValue(""+floor.getProjectId());
			  row.createCell(1).setCellValue(""+floor.getBuildingId());
			  row.createCell(2).setCellValue(""+floor.getWingId());
		      row.createCell(3).setCellValue(""+floor.getFloortypeName());
		      row.createCell(4).setCellValue(""+floor.getFloorRise());
			  row.createCell(5).setCellValue(""+floor.getFloorzoneType());
		      row.createCell(6).setCellValue(""+floor.getNumberofOpenParking());
		      row.createCell(7).setCellValue(""+floor.getNumberofCloseParking());
			  row.createCell(8).setCellValue(""+floor.getNumberofShops());
		      row.createCell(9).setCellValue(""+floor.getNumberofFlats());
			  
			}
		 }
		 catch(Exception e)
		 {

		 }
	}
    
}

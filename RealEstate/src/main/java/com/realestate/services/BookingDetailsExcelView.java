package com.realestate.services;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import com.realestate.bean.Booking;

@Controller
@RequestMapping("/")
public class BookingDetailsExcelView extends AbstractXlsView
{
	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
 
		try
		{
			response.setHeader("Content-disposition", "attachment; filename=\"BookingDetails.xlsx\"");
			
			List<Booking> bookingDetails = (List<Booking>) model.get("bookingDetails");
			
			HSSFSheet sheet = (HSSFSheet) workbook.createSheet("Booking Details");
			
			HSSFRow header = sheet.createRow(0);
			
			header.createCell(0).setCellValue("Enquiry Id");
			header.createCell(1).setCellValue("Booking Id");
			header.createCell(2).setCellValue("First Name");
			header.createCell(3).setCellValue("Middle Name");
			header.createCell(4).setCellValue("Last Name");
			header.createCell(5).setCellValue("Mobile No.1");
			header.createCell(6).setCellValue("Mobile No.2");
			header.createCell(7).setCellValue("EmailId");
			header.createCell(9).setCellValue("Address");
			
			header.createCell(10).setCellValue("Pin Code");
			header.createCell(11).setCellValue("Occupation");
			header.createCell(12).setCellValue("Purpose Of Flat");
			header.createCell(13).setCellValue("Project Name");
			header.createCell(14).setCellValue("Building");
			header.createCell(15).setCellValue("Wing");
			header.createCell(16).setCellValue("Floor Name");
			header.createCell(17).setCellValue("Flat Number");
			header.createCell(18).setCellValue("Flat Type");
			header.createCell(19).setCellValue("Flat Facing");
			header.createCell(20).setCellValue("Flat Area(SQ.FT)");
			header.createCell(21).setCellValue("Flat Cost(Sq.Ft)");
			header.createCell(22).setCellValue("Floor Rise");
			header.createCell(23).setCellValue("Net Flat Cost(Sq.Ft)");
			header.createCell(24).setCellValue("Flat Basic Cost");
			header.createCell(25).setCellValue("Parking Floor");
			header.createCell(26).setCellValue("Parking Zone");
			header.createCell(27).setCellValue("Agent Firm Name");
			header.createCell(28).setCellValue("Infrastructure Charges");
			header.createCell(29).setCellValue("Aggreement Value");
			header.createCell(30).setCellValue("Stamp Duty Per");
			header.createCell(31).setCellValue("Stamp Duty Amt");
			header.createCell(32).setCellValue("Registration Per");
			header.createCell(33).setCellValue("Registration Amt");
			header.createCell(34).setCellValue("Handling Charges");
			header.createCell(35).setCellValue("GST Per");
			header.createCell(36).setCellValue("GST Amount");
			header.createCell(37).setCellValue("Grand Total");
			header.createCell(38).setCellValue("TDS 1% Amount");
			header.createCell(39).setCellValue("Booking Date");
			
			int counter = 1;
			
			for (Booking booking : bookingDetails)
			{
			  HSSFRow row = sheet.createRow(counter++);

			  row.createCell(0).setCellValue(""+booking.getEnquiryId());
			  row.createCell(1).setCellValue(""+booking.getBookingId());
			  row.createCell(2).setCellValue(""+booking.getBookingfirstname());
			  row.createCell(3).setCellValue(""+booking.getBookingmiddlename());
			  row.createCell(4).setCellValue(""+booking.getBookinglastname());
			  row.createCell(5).setCellValue(""+booking.getBookingmobileNumber1());
			  row.createCell(6).setCellValue(""+booking.getBookingmobileNumber2());
			  row.createCell(7).setCellValue(""+booking.getBookingEmail());
			  row.createCell(9).setCellValue(""+booking.getBookingaddress());
			  
			  row.createCell(10).setCellValue(""+booking.getBookingPincode());
			  row.createCell(11).setCellValue(""+booking.getBookingOccupation());
			  row.createCell(12).setCellValue(""+booking.getPurposeOfFlat());
			  row.createCell(13).setCellValue(""+booking.getProjectId());
			  row.createCell(14).setCellValue(""+booking.getBuildingId());
			  row.createCell(15).setCellValue(""+booking.getWingId());
			  row.createCell(16).setCellValue(""+booking.getFloorId());
			  row.createCell(17).setCellValue(""+booking.getFlatId());
			  row.createCell(18).setCellValue(""+booking.getFlatType());
			  row.createCell(19).setCellValue(""+booking.getFlatFacing());
			  row.createCell(20).setCellValue(""+booking.getFlatareainSqFt());
			  row.createCell(21).setCellValue(""+booking.getFlatCostwithotfloorise());
			  row.createCell(22).setCellValue(""+booking.getFloorRise());
			  row.createCell(23).setCellValue(""+booking.getFlatCost());
			  row.createCell(24).setCellValue(""+booking.getFlatbasicCost());
			  row.createCell(25).setCellValue(""+booking.getParkingFloorId());
			  row.createCell(26).setCellValue(""+booking.getParkingZoneId());
			  row.createCell(27).setCellValue(""+booking.getAgentId());
			  row.createCell(28).setCellValue(""+booking.getInfrastructureCharge());
			  row.createCell(29).setCellValue(""+booking.getAggreementValue1());
			  row.createCell(30).setCellValue(""+booking.getStampDutyPer());
			  row.createCell(31).setCellValue(""+booking.getStampDuty1());
			  row.createCell(32).setCellValue(""+booking.getRegistrationPer());
			  row.createCell(33).setCellValue(""+booking.getRegistrationCost1());
			  row.createCell(34).setCellValue(""+booking.getHandlingCharges());
			  row.createCell(35).setCellValue(""+booking.getGstCost());
			  row.createCell(36).setCellValue(""+booking.getGstAmount1());
			  row.createCell(37).setCellValue(""+booking.getGrandTotal1());
			  row.createCell(38).setCellValue(""+booking.getTds());
			  row.createCell(39).setCellValue(""+booking.getCreationDate());
			}
		 }
		 catch(Exception e)
		 {

		 }
	}
    
}

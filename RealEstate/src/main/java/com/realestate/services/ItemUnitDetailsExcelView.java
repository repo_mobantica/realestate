package com.realestate.services;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import com.realestate.bean.Item;
import com.realestate.bean.ItemUnit;

@Controller
@RequestMapping("/")
public class ItemUnitDetailsExcelView extends AbstractXlsView
{
	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
 
		try
		{
			response.setHeader("Content-disposition", "attachment; filename=\"ItemUnitDetails.xlsx\"");
			
			List<ItemUnit> itemUnitDetails = (List<ItemUnit>) model.get("itemUnitDetails");
			
			HSSFSheet sheet = (HSSFSheet) workbook.createSheet("Item Unit Details");
			
			HSSFRow header = sheet.createRow(0);
			
			header.createCell(0).setCellValue("Item Unit Name");
			header.createCell(1).setCellValue("Item Unit Symbol/Sort Name");
			
			int counter = 1;
			
			for (ItemUnit itemunit : itemUnitDetails )
			{
			  HSSFRow row = sheet.createRow(counter++);

			  row.createCell(0).setCellValue(""+itemunit.getItemunitName());
			  row.createCell(1).setCellValue(""+itemunit.getItemunitSymbol());
			}
		 }
		 catch(Exception e)
		 {

		 }
	}
    
}

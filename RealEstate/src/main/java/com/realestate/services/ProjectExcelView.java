package com.realestate.services;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import com.realestate.bean.Project;

public class ProjectExcelView extends AbstractXlsView
{

	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
 
		response.setHeader("Content-disposition", "attachment; filename=\"ProjectData.xlsx\"");
		
		List<Project> projects = (List<Project>) model.get("projects");
		
		HSSFSheet sheet = (HSSFSheet) workbook.createSheet("Project Data");
		
		HSSFRow header = sheet.createRow(0);
		
		header.createCell(0).setCellValue("Project Name");
		header.createCell(1).setCellValue("Project Type");
		header.createCell(2).setCellValue("Company Name");
		header.createCell(3).setCellValue("Gat No./Survey No.");
		header.createCell(4).setCellValue("Project Land Area(SQ.Ft.)");
		header.createCell(5).setCellValue("Project Cost");
		header.createCell(6).setCellValue("No. of Buildings");
		header.createCell(7).setCellValue("Loading %");
		header.createCell(8).setCellValue("RERA / Non-RERA");
		header.createCell(9).setCellValue("RERA Number");
		header.createCell(10).setCellValue("Project Start Date");
		header.createCell(11).setCellValue("Project End Date");
		header.createCell(12).setCellValue("Address");
		header.createCell(13).setCellValue("Country");
		header.createCell(14).setCellValue("State");
		header.createCell(15).setCellValue("City");
		header.createCell(16).setCellValue("Area");
		header.createCell(17).setCellValue("Pin Code");
		header.createCell(18).setCellValue("Office Phone No");
		header.createCell(19).setCellValue("Project Status");
		
		int counter = 1;
		for (Project projectData : projects)
		{
		  HSSFRow row = sheet.createRow(counter++);
		  row.createCell(0).setCellValue(""+projectData.getProjectName());
		  row.createCell(1).setCellValue(""+projectData.getProjectType());
		  row.createCell(2).setCellValue(""+projectData.getCompanyId());
	      row.createCell(3).setCellValue(""+projectData.getGatORserveyNumber());
		  row.createCell(4).setCellValue(""+projectData.getProjectlandArea());  
		  row.createCell(5).setCellValue(""+projectData.getProjectCost());
		  row.createCell(6).setCellValue(""+projectData.getNumberofBuildings());
		  row.createCell(7).setCellValue(""+projectData.getLoading());
		  row.createCell(8).setCellValue(""+projectData.getReraStatus());
		  row.createCell(9).setCellValue(""+projectData.getReraNumber());
		  row.createCell(10).setCellValue(""+projectData.getProjectstartDate());
		  row.createCell(11).setCellValue(""+projectData.getProjectendDate());
		  row.createCell(12).setCellValue(""+projectData.getProjectAddress());
		  row.createCell(13).setCellValue(""+projectData.getCountryId());
		  row.createCell(14).setCellValue(""+projectData.getStateId());
		  row.createCell(15).setCellValue(""+projectData.getCityId());
		  row.createCell(16).setCellValue(""+projectData.getLocationareaId());
		  row.createCell(17).setCellValue(""+projectData.getAreaPincode());
		  row.createCell(18).setCellValue(""+projectData.getProjectofficePhoneno());
		  row.createCell(19).setCellValue(""+projectData.getProjectStatus());
		}
	}
    
}

package com.realestate.services;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import com.realestate.bean.Contractor;

@Controller
@RequestMapping("/")
public class ContractorDetailsExcelView extends AbstractXlsView
{
	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
 
		try
		{
			response.setHeader("Content-disposition", "attachment; filename=\"ContractorDetails.xlsx\"");
			
			List<Contractor> contractorDetails = (List<Contractor>) model.get("contractorDetails");
			
			HSSFSheet sheet = (HSSFSheet) workbook.createSheet("Contractor Details");
			
			HSSFRow header = sheet.createRow(0);
			
			header.createCell(0).setCellValue("Contractor Firm Name");
			header.createCell(1).setCellValue("Firm Type");
			header.createCell(2).setCellValue("Firm PAN No");
			header.createCell(3).setCellValue("Firm GST No");
			header.createCell(4).setCellValue("TDS Per");
			header.createCell(5).setCellValue("Insurance");
			header.createCell(6).setCellValue("Contractor Type");
			header.createCell(7).setCellValue("Sub-Contractor Type");
			header.createCell(8).setCellValue("Check Printing Name");
			header.createCell(9).setCellValue("Payment Terms");
			header.createCell(10).setCellValue("Firm Address");
			header.createCell(11).setCellValue("Country");
			header.createCell(12).setCellValue("State");
			header.createCell(13).setCellValue("City");
			header.createCell(14).setCellValue("Area");
			header.createCell(15).setCellValue("Pin Code");
			header.createCell(16).setCellValue("Firm Bank Name");
			header.createCell(17).setCellValue("Firm Bank Branch");
			header.createCell(18).setCellValue("Firm Bank Branch IFSC");
			header.createCell(19).setCellValue("Firm Bank A/C No");
			
			int counter = 1;
			
			for (Contractor contractor : contractorDetails)
			{
			  HSSFRow row = sheet.createRow(counter++);

			  row.createCell(0).setCellValue(""+contractor.getContractorfirmName());
			  row.createCell(1).setCellValue(""+contractor.getContractorfirmType());
			  row.createCell(2).setCellValue(""+contractor.getFirmpanNumber());
			  row.createCell(3).setCellValue(""+contractor.getFirmgstNumber());
			  row.createCell(4).setCellValue(""+contractor.getTdspercentage());
			  row.createCell(5).setCellValue(""+contractor.getInsuranceStatus());
			  row.createCell(6).setCellValue(""+contractor.getContractortypeId());
			  row.createCell(7).setCellValue(""+contractor.getSubcontractortypeId());
			  row.createCell(8).setCellValue(""+contractor.getCheckPrintingName());
			  row.createCell(9).setCellValue(""+contractor.getContractorpaymentTerms());
			  row.createCell(10).setCellValue(""+contractor.getContractorfirmAddress());
			  row.createCell(11).setCellValue(""+contractor.getCountryId());
			  row.createCell(12).setCellValue(""+contractor.getStateId());
			  row.createCell(13).setCellValue(""+contractor.getCityId());
			  row.createCell(14).setCellValue(""+contractor.getLocationareaId());
			  row.createCell(15).setCellValue(""+contractor.getContractorPincode());
			  row.createCell(16).setCellValue(""+contractor.getBankName());
			  row.createCell(17).setCellValue(""+contractor.getBranchName());
			  row.createCell(18).setCellValue(""+contractor.getBankifscCode());
			  row.createCell(19).setCellValue(""+contractor.getFirmbankacNumber());
			}
		 }
		 catch(Exception e)
		 {

		 }
	}
    
}

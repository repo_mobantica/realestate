package com.realestate.services;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import com.realestate.bean.PaymentScheduler;

@Controller
@RequestMapping("/")
public class PaymementSchedulerDetailsExcelView extends AbstractXlsView
{
	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
 
		try
		{
			response.setHeader("Content-disposition", "attachment; filename=\"PaymentSchedulerDetails.xlsx\"");
			
			List<PaymentScheduler> paymentSchedulersDetails = (List<PaymentScheduler>) model.get("paymentSchedulersDetails");
			
			HSSFSheet sheet = (HSSFSheet) workbook.createSheet("Payment Scheduler Details");
			
			HSSFRow header = sheet.createRow(0);
			
			header.createCell(0).setCellValue("Project Name");
			header.createCell(1).setCellValue("Building Name");
			header.createCell(2).setCellValue("Wing Name");
			header.createCell(3).setCellValue("Installment");
			header.createCell(4).setCellValue("Percentage");
			header.createCell(5).setCellValue("Payment Description");
			header.createCell(6).setCellValue("DueDate");
			header.createCell(7).setCellValue("Paid Date");
			
			int counter = 1;
			
			for (PaymentScheduler paymentScheduler : paymentSchedulersDetails)
			{
			  HSSFRow row = sheet.createRow(counter++);

			  row.createCell(0).setCellValue(""+paymentScheduler.getProjectId());
			  row.createCell(1).setCellValue(""+paymentScheduler.getBuildingId());
			  row.createCell(2).setCellValue(""+paymentScheduler.getWingId());
			  row.createCell(3).setCellValue(""+paymentScheduler.getInstallmentNumber());
		      row.createCell(4).setCellValue(""+paymentScheduler.getPercentage());
		      row.createCell(5).setCellValue(""+paymentScheduler.getPaymentDecription());
			  row.createCell(6).setCellValue(""+paymentScheduler.getDueDate());
		      row.createCell(7).setCellValue(""+paymentScheduler.getPaidDate());
			  
			}
		 }
		 catch(Exception e)
		 {

		 }
	}
    
}

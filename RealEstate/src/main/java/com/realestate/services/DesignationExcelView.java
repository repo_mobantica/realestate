package com.realestate.services;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import com.realestate.bean.Designation;


public class DesignationExcelView extends AbstractXlsView
{

	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
 
		response.setHeader("Content-disposition", "attachment; filename=\"DesignationData.xlsx\"");
		
		List<Designation> designations = (List<Designation>) model.get("designations");
		
		HSSFSheet sheet = (HSSFSheet) workbook.createSheet("Designation Data");
		
		HSSFRow header = sheet.createRow(0);
		
		header.createCell(0).setCellValue("Department Name");
		header.createCell(1).setCellValue("Designation Name");
		
		
		int counter = 1;
		for (Designation designationData : designations)
		{
		  HSSFRow row = sheet.createRow(counter++);
		  row.createCell(0).setCellValue(designationData.getDepartmentId());
		  row.createCell(1).setCellValue(designationData.getDesignationName());
		}
	}
    
}

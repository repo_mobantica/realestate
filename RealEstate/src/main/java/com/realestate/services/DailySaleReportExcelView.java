package com.realestate.services;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.BorderStyle; 
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import com.realestate.bean.Booking;
import com.realestate.bean.CustomerReceiptForm;
import com.realestate.bean.Enquiry;
import com.realestate.bean.Project;
import com.realestate.response.DailyReportResponse;

public class DailySaleReportExcelView  extends AbstractXlsView{


	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception
	{

		response.setHeader("Content-disposition", "attachment; filename=\"Daily Sale Report.xlsx\"");

		List<DailyReportResponse> list = (List<DailyReportResponse>) model.get("list");

		HSSFSheet sheet = (HSSFSheet) workbook.createSheet("Sale Report");

		sheet.setDefaultColumnWidth(30);
		
		int counter = 0;
		int i=1;

        CellStyle style = workbook.createCellStyle();  
        style.setBorderBottom(BorderStyle.THIN);   
        style.setBorderRight(BorderStyle.THIN);   
        style.setBorderLeft(BorderStyle.THIN);
        style.setBorderTop(BorderStyle.THIN);
     
        HSSFFont font=(HSSFFont) workbook.createFont();
        font.setFontName("Arial");
        font.setBold(true);
        CellStyle style1 = workbook.createCellStyle();  
        style1.setBorderBottom(BorderStyle.THIN);   
        style1.setBorderRight(BorderStyle.THIN);   
        style1.setBorderLeft(BorderStyle.THIN);
        style1.setBorderTop(BorderStyle.THIN);
        style1.setFont(font);
		//for Booking List
		counter = counter++;
		HSSFRow header = sheet.createRow(counter);

		//header.createCell(4).setCellStyle(style);
		header.createCell(4).setCellValue("ENQUIRY DETAILS");
		header.getCell(4).setCellStyle(style1);
		counter=counter+2;
		header = sheet.createRow(counter);

		header.createCell(0).setCellValue("Sr.No");
		header.getCell(0).setCellStyle(style1);
		header.createCell(1).setCellValue("Customer Name");
		header.getCell(1).setCellStyle(style1);
		header.createCell(2).setCellValue("Mobile No");
		header.getCell(2).setCellStyle(style1);
		header.createCell(3).setCellValue("Email Id");
		header.getCell(3).setCellStyle(style1);
		header.createCell(4).setCellValue("Project Name");
		header.getCell(4).setCellStyle(style1);
		header.createCell(5).setCellValue("Prefreed");
		header.getCell(5).setCellStyle(style1);
		header.createCell(6).setCellValue("Remark");
		header.getCell(6).setCellStyle(style1);
		i=1;
		counter = counter+1;
		for (Enquiry enquiryData : list.get(0).getEnquiryList())
		{
			HSSFRow row = sheet.createRow(counter++);
			row.createCell(0).setCellValue(""+i);
			row.getCell(0).setCellStyle(style);
			row.createCell(1).setCellValue(""+enquiryData.getEnqfirstName());
			row.getCell(1).setCellStyle(style);
			row.createCell(2).setCellValue(""+enquiryData.getEnqmobileNumber1());
			row.getCell(2).setCellStyle(style);
			row.createCell(3).setCellValue(""+enquiryData.getEnqEmail());
			row.getCell(3).setCellStyle(style);
			row.createCell(4).setCellValue(""+enquiryData.getProjectName()); 
			row.getCell(4).setCellStyle(style); 
			row.createCell(5).setCellValue(""+enquiryData.getFlatType());
			row.getCell(5).setCellStyle(style);
			row.createCell(6).setCellValue(""+enquiryData.getEnqRemark());
			row.getCell(6).setCellStyle(style);
			i++;
		}

		//for Booking List
		counter = counter+3;
		header = sheet.createRow(counter);
		header.createCell(4).setCellValue("BOOKING DETAILS");
		header.getCell(4).setCellStyle(style1);
		counter=counter+2;
		header = sheet.createRow(counter);
		header.createCell(0).setCellValue("Sr.No");
		header.getCell(0).setCellStyle(style1);
		header.createCell(1).setCellValue("Customer Name");
		header.getCell(1).setCellStyle(style1);
		header.createCell(2).setCellValue("Mobile No");
		header.getCell(2).setCellStyle(style1);
		header.createCell(3).setCellValue("Email Id");
		header.getCell(3).setCellStyle(style1);
		header.createCell(4).setCellValue("Flat No");
		header.getCell(4).setCellStyle(style1);
		header.createCell(5).setCellValue("Area");
		header.getCell(5).setCellStyle(style1);
		header.createCell(6).setCellValue("Project Name");
		header.getCell(6).setCellStyle(style1);
		header.createCell(7).setCellValue("Agg Amt");
		header.getCell(7).setCellStyle(style1);
		header.createCell(8).setCellValue("GST");
		header.getCell(8).setCellStyle(style1);
		header.createCell(9).setCellValue("Total");
		header.getCell(9).setCellStyle(style1);

		i=1;
		counter = counter+1;
		for (Booking bookingData : list.get(0).getBookingList())
		{
			HSSFRow row = sheet.createRow(counter++);
			row.createCell(0).setCellValue(""+i);
			row.createCell(1).setCellValue(""+bookingData.getBookingfirstname());
			row.createCell(2).setCellValue(""+bookingData.getBookingmobileNumber1());
			row.createCell(3).setCellValue(""+bookingData.getBookingEmail());
			row.createCell(4).setCellValue(""+bookingData.getFlatNumber()); 
			row.createCell(5).setCellValue(""+bookingData.getArea());  
			row.createCell(6).setCellValue(""+bookingData.getProjectName());
			row.createCell(7).setCellValue(""+bookingData.getAggreementValue1());
			row.createCell(8).setCellValue(""+bookingData.getGstAmount1());
			row.createCell(9).setCellValue(""+bookingData.getGrandTotal1());

			row.getCell(0).setCellStyle(style);
			row.getCell(1).setCellStyle(style);
			row.getCell(2).setCellStyle(style);
			row.getCell(3).setCellStyle(style);
			row.getCell(4).setCellStyle(style);
			row.getCell(5).setCellStyle(style);
			row.getCell(6).setCellStyle(style);
			row.getCell(7).setCellStyle(style);
			row.getCell(8).setCellStyle(style);
			row.getCell(9).setCellStyle(style);
			i++;
		}


		//for Payment List
		sheet.setDefaultColumnWidth(30);
		counter = counter+3;
		header = sheet.createRow(counter);
		header.createCell(6).setCellValue("Collection Details");
		header.getCell(6).setCellStyle(style1);
		
		counter=counter+2;
		header = sheet.createRow(counter);
		header.createCell(0).setCellValue("Sr.No");
		header.createCell(1).setCellValue("Flat No");
		header.createCell(2).setCellValue("Customer Name");
		header.createCell(3).setCellValue("Bank Name");
		header.createCell(4).setCellValue("Branch Name");
		header.createCell(5).setCellValue("Type");
		header.createCell(6).setCellValue("Cheq.No");
		header.createCell(7).setCellValue("Agg Amt");
		header.createCell(8).setCellValue("Stamp");
		header.createCell(9).setCellValue("Regist");
		header.createCell(10).setCellValue("GST");
		header.createCell(11).setCellValue("Amt");
		header.createCell(12).setCellValue("Receipt No");

		header.getCell(0).setCellStyle(style1);
		header.getCell(1).setCellStyle(style1);
		header.getCell(2).setCellStyle(style1);
		header.getCell(3).setCellStyle(style1);
		header.getCell(4).setCellStyle(style1);
		header.getCell(5).setCellStyle(style1);
		header.getCell(6).setCellStyle(style1);
		header.getCell(7).setCellStyle(style1);
		header.getCell(8).setCellStyle(style1);
		header.getCell(9).setCellStyle(style1);
		header.getCell(10).setCellStyle(style1);
		header.getCell(11).setCellStyle(style1);
		header.getCell(12).setCellStyle(style1);
		i=1;
		counter = counter+1;
		for (CustomerReceiptForm paymentData : list.get(0).getPaymentList())
		{
			HSSFRow row = sheet.createRow(counter++);
			row.createCell(0).setCellValue(""+i);
			row.createCell(1).setCellValue(""+paymentData.getFlatNumber());
			row.createCell(2).setCellValue(""+paymentData.getCustomerName());
			row.createCell(3).setCellValue(""+paymentData.getBankName());
			row.createCell(4).setCellValue(""+paymentData.getBranchName());
			row.createCell(5).setCellValue(""+paymentData.getPaymentMode());  
			row.createCell(6).setCellValue(""+paymentData.getChequeNumber());

			if(paymentData.getPaymentType().equalsIgnoreCase("Agreement"))
			{
				row.createCell(7).setCellValue(""+paymentData.getPaymentType());
			}
			else
			{
				row.createCell(7).setCellValue("");
			}

			if(paymentData.getPaymentType().equalsIgnoreCase("Stamp Duty"))
			{
				row.createCell(8).setCellValue(""+paymentData.getPaymentType());
			}
			else
			{
				row.createCell(8).setCellValue("");
			}
			if(paymentData.getPaymentType().equalsIgnoreCase("Registration"))
			{
				row.createCell(9).setCellValue(""+paymentData.getPaymentType());
			}
			else
			{
				row.createCell(9).setCellValue("");
			}
			if(paymentData.getPaymentType().equalsIgnoreCase("GST"))
			{
				row.createCell(10).setCellValue(""+paymentData.getPaymentType());
			}
			else
			{
				row.createCell(10).setCellValue("");
			}

			row.createCell(11).setCellValue(""+paymentData.getPaymentAmount());
			row.createCell(12).setCellValue(""+paymentData.getReceiptId());


			row.getCell(0).setCellStyle(style);
			row.getCell(1).setCellStyle(style);
			row.getCell(2).setCellStyle(style);
			row.getCell(3).setCellStyle(style);
			row.getCell(4).setCellStyle(style);
			row.getCell(5).setCellStyle(style);
			row.getCell(6).setCellStyle(style);
			row.getCell(7).setCellStyle(style);
			row.getCell(8).setCellStyle(style);
			row.getCell(9).setCellStyle(style);
			row.getCell(10).setCellStyle(style);
			row.getCell(11).setCellStyle(style);
			row.getCell(12).setCellStyle(style);
			i++;
		}

		header = sheet.createRow(counter);
		header.createCell(10).setCellValue("Total");
		header.createCell(11).setCellValue(""+list.get(0).getTotalPayment());
		header.getCell(10).setCellStyle(style1);
		header.getCell(11).setCellStyle(style1);
	}

}

package com.realestate.services;

import javax.sql.DataSource;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.mail.*;  
import javax.mail.internet.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Controller;

public class EmailService 
{
	@Autowired
	private JavaMailSender mailSender;
	
	public void setMailSender(JavaMailSender mailSender)
	{
		this.mailSender = mailSender;
	}
	
	public void sendLoginCredentialsMail(String emil_to, String subject, String msgBody) 
	{
		//email = sonigaradeveloper@gmail.com
		//password = 9822197411
		try
		{
			MimeMessageHelper helper = new MimeMessageHelper(mailSender.createMimeMessage(), true);
			helper.setFrom((new InternetAddress("sonigaradevelopers@gmail.com")));
			helper.setTo(emil_to);
			helper.setSubject(subject);
			//helper.addAttachment("invoice", attachment);
			// helper.setText(msgBody,"text/html");
			helper.setText("", msgBody);
			mailSender.send(helper.getMimeMessage());
		}
		catch (MailException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (MessagingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
/*
	public void sendMail(String emil_to, String subject, String msgBody) 
	{
 		final String username="sonigaradeveloper@gmail.com";
		final String password="9822197411";
		String from="sonigaradeveloper@gmail.com";
		
		try
		{

	        Properties prop = new Properties();
			prop.put("mail.smtp.host", "smtp.gmail.com");
	        prop.put("mail.smtp.port", "587");
	        prop.put("mail.smtp.auth", "true");
	        prop.put("mail.smtp.starttls.enable", "true"); //TLS
	        
	        Session session = Session.getInstance(prop,
	                new javax.mail.Authenticator() {
	                    protected PasswordAuthentication getPasswordAuthentication() {
	                        return new PasswordAuthentication(username, password);
	                    }
	                });
	        try {

	        	Message message = new MimeMessage(session);
	            message.setFrom(new InternetAddress(from));
	           // message.addRecipient(Message.RecipientType.TO, emil_to);
	            
	            message.setRecipients(
	                    Message.RecipientType.TO,
	                    InternetAddress.parse(emil_to)
	            );
	            
	            message.setSubject(subject);
	            message.setText(msgBody);

	            Transport.send(message);

	            System.out.println("Done");

	        } catch (MessagingException e) {
	            e.printStackTrace();
	        } 
		}
		catch (MailException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	*/
	/*private Properties configureEmailProperties(Properties props, Properties prop)
	{
		// TODO Auto-generated method stub

		props.put("mail.smtp.auth", prop.getProperty("auth"));
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", prop.getProperty("host"));
		props.put("mail.smtp.port", prop.getProperty("port"));
		props.put("mail.smtp.socketFactory.port", prop.getProperty("socketFactoryPort"));
		props.put("mail.smtp.socketFactory.class",prop.getProperty("socketFactory"));

		return props;
	 }*/
	
	
	
	
	/*
	    public void sendEmail() 
	    {
	 
	        //ProductOrder order = (ProductOrder) object;
	 
	        MimeMessagePreparator preparator = getMessagePreparator();
	 
	        try
	        {
	            mailSender.send(preparator);
	            System.out.println("Message Send...Hurrey");
	        }
	        catch (MailException ex) 
	        {
	            System.err.println(ex.getMessage());
	        }
	    }
	 
	    private MimeMessagePreparator getMessagePreparator() 
	    {
	        MimeMessagePreparator preparator = new MimeMessagePreparator() 
	        {
	            public void prepare(MimeMessage mimeMessage) throws Exception 
	            {
	                mimeMessage.setFrom("omkar@mobantica.com");
	                mimeMessage.setRecipient(Message.RecipientType.TO,new InternetAddress("bahirnath@mobantica.com"));
	                mimeMessage.setText("Hi this test mail");
	                mimeMessage.setSubject("Your order on Demoapp");
	            }
	        };
	        
	        return preparator;
	    }
	 */
}

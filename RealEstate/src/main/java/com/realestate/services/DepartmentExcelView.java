package com.realestate.services;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import com.realestate.bean.Department;

public class DepartmentExcelView extends AbstractXlsView
{

	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
 
		response.setHeader("Content-disposition", "attachment; filename=\"DepartmentData.xlsx\"");
		
		List<Department> departments = (List<Department>) model.get("departments");
		
		HSSFSheet sheet = (HSSFSheet) workbook.createSheet("Department Data");
		
		HSSFRow header = sheet.createRow(0);
		
		header.createCell(0).setCellValue("Department Name");
		
		
		int counter = 1;
		for (Department departmentData : departments)
		{
		  HSSFRow row = sheet.createRow(counter++);
		  row.createCell(0).setCellValue(departmentData.getDepartmentName());
		}
	}
    
}

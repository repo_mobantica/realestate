package com.realestate.services;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import com.realestate.bean.Agent;

@Controller
@RequestMapping("/")
public class AgentDetailsExcelView extends AbstractXlsView
{
	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
 
		try
		{
			response.setHeader("Content-disposition", "attachment; filename=\"AgentDetails.xlsx\"");
			
			List<Agent> agentDetails = (List<Agent>) model.get("agentDetails");
			
			HSSFSheet sheet = (HSSFSheet) workbook.createSheet("Agent Details");
			
			HSSFRow header = sheet.createRow(0);
			
			header.createCell(0).setCellValue("Agent Firm Name");
			header.createCell(1).setCellValue("Firm Type");
			header.createCell(2).setCellValue("Firm PAN No");
			header.createCell(3).setCellValue("Firm GST No");
			header.createCell(4).setCellValue("Brokerage Per");
			header.createCell(5).setCellValue("Agent RERA Number");
			header.createCell(6).setCellValue("Check Printing Name");
			header.createCell(7).setCellValue("Agent Payment Terms");
			header.createCell(8).setCellValue("Firm Address");
			header.createCell(9).setCellValue("Country");
			header.createCell(10).setCellValue("State");
			header.createCell(11).setCellValue("City");
			header.createCell(12).setCellValue("Area");
			header.createCell(13).setCellValue("Pin Code");
			header.createCell(14).setCellValue("Firm Bank Name");
			header.createCell(15).setCellValue("Firm Bank Branch");
			header.createCell(16).setCellValue("Firm Bank Branch IFSC");
			header.createCell(17).setCellValue("Firm Bank A/C No");
			
			int counter = 1;
			
			for (Agent agent : agentDetails)
			{
			  HSSFRow row = sheet.createRow(counter++);

			  row.createCell(0).setCellValue(""+agent.getAgentfirmName());
			  row.createCell(1).setCellValue(""+agent.getAgentfirmType());
			  row.createCell(2).setCellValue(""+agent.getFirmpanNumber());
			  row.createCell(3).setCellValue(""+agent.getFirmgstNumber());
			  row.createCell(4).setCellValue(""+agent.getBrokeragePercentage());
			  row.createCell(5).setCellValue(""+agent.getAgentreraNumber());
			  row.createCell(6).setCellValue(""+agent.getCheckPrintingName());
			  row.createCell(7).setCellValue(""+agent.getAgentpaymentTerms());
			  row.createCell(8).setCellValue(""+agent.getAgentfirmAddress());
			  
			/*  row.createCell(9).setCellValue(""+agent.getCountryId());
			  row.createCell(10).setCellValue(""+agent.getStateId());
			  row.createCell(11).setCellValue(""+agent.getCityId());
			  row.createCell(12).setCellValue(""+agent.getLocationareaId());
			  */
			  row.createCell(13).setCellValue(""+agent.getAgentPincode());
			  row.createCell(14).setCellValue(""+agent.getBankName());
			  row.createCell(15).setCellValue(""+agent.getBranchName());
			  row.createCell(16).setCellValue(""+agent.getBankifscCode());
			  row.createCell(17).setCellValue(""+agent.getFirmbankacNumber());
			}
		 }
		 catch(Exception e)
		 {

		 }
	}
    
}

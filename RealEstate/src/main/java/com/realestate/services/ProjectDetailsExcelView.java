package com.realestate.services;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import com.realestate.bean.ProjectDetails;
import com.realestate.repository.ProjectRepository;

@Controller
@RequestMapping("/")
public class ProjectDetailsExcelView extends AbstractXlsView
{
    @Autowired
    ProjectRepository projectRepository;
    
    @Autowired
    MongoTemplate mongoTemplate;
	
	
	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
 
		try
		{
			response.setHeader("Content-disposition", "attachment; filename=\"ProjectDetails.xlsx\"");
			
			List<ProjectDetails> projectDetails = (List<ProjectDetails>) model.get("projectsDetails");
			
			HSSFSheet sheet = (HSSFSheet) workbook.createSheet("Project Details");
			
			HSSFRow header = sheet.createRow(0);
			
			header.createCell(0).setCellValue("Project Name");
			header.createCell(1).setCellValue("Landowner Name");
			header.createCell(2).setCellValue("Project Type");
			header.createCell(3).setCellValue("Land Area");
			header.createCell(4).setCellValue("Land Price");
			header.createCell(5).setCellValue("Latitude");
			header.createCell(6).setCellValue("Longitude");
			header.createCell(7).setCellValue("Boundaries East By Actual Location");
			header.createCell(8).setCellValue("Boundaries West By Actual Location");
			header.createCell(9).setCellValue("Boundaries North By Actual Location");
			header.createCell(10).setCellValue("Boundaries South By Actual Location");
			header.createCell(11).setCellValue("Boundaries East By Google Location");
			header.createCell(12).setCellValue("Boundaries West By Google Location");
			header.createCell(13).setCellValue("Boundaries North By Google Location");
			header.createCell(14).setCellValue("Boundaries South By Google Location");
			
			int counter = 1;
			
			for (ProjectDetails projectDetailsData : projectDetails)
			{
			  HSSFRow row = sheet.createRow(counter++);

			  row.createCell(0).setCellValue(""+projectDetailsData.getProjectId());
			  row.createCell(1).setCellValue(""+projectDetailsData.getLandownerName());
			  row.createCell(2).setCellValue(""+projectDetailsData.getProjectType());
		      row.createCell(3).setCellValue(""+projectDetailsData.getLandArea());
			  row.createCell(4).setCellValue(""+projectDetailsData.getLandPrice());  
			  row.createCell(5).setCellValue(""+projectDetailsData.getProjectLatitude());
			  row.createCell(6).setCellValue(""+projectDetailsData.getProjectLongitude());
			  
			  row.createCell(7).setCellValue(""+projectDetailsData.getLocationInEast());
			  row.createCell(8).setCellValue(""+projectDetailsData.getLocationInWest());
			  row.createCell(9).setCellValue(""+projectDetailsData.getLocationInNorth());
			  row.createCell(10).setCellValue(""+projectDetailsData.getLocationInSouth());
			  
			  row.createCell(11).setCellValue(""+projectDetailsData.getGoogleLocationInEast());
			  row.createCell(12).setCellValue(""+projectDetailsData.getGoogleLocationInWest());
			  row.createCell(13).setCellValue(""+projectDetailsData.getGoogleLocationInNorth());
			  row.createCell(14).setCellValue(""+projectDetailsData.getGoogleLocationInSouth());
			}
		 }
		 catch(Exception e)
		 {

		 }
	}
    
}

package com.realestate.services;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import com.realestate.bean.Employee;

public class EmployeeExcelView extends AbstractXlsView
{

	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
 
		response.setHeader("Content-disposition", "attachment; filename=\"EmployeeData.xlsx\"");
		
		List<Employee> employees = (List<Employee>) model.get("employees");
		
		HSSFSheet sheet = (HSSFSheet) workbook.createSheet("Employee Data");
		
		HSSFRow header = sheet.createRow(0);
		
		header.createCell(0).setCellValue("First Name");
		header.createCell(1).setCellValue("Middle Name");
		header.createCell(2).setCellValue("Last Name");
		header.createCell(3).setCellValue("Gender");
		header.createCell(4).setCellValue("Marital Status");
		header.createCell(5).setCellValue("Date Of Birth");
		header.createCell(6).setCellValue("Anniversary Date");
		header.createCell(7).setCellValue("Spouse Name");
		header.createCell(8).setCellValue("Education Qualification");
		header.createCell(9).setCellValue("Department");
		header.createCell(10).setCellValue("Designation");
		header.createCell(11).setCellValue("Address");
		header.createCell(12).setCellValue("Country");
		header.createCell(13).setCellValue("State");
		header.createCell(14).setCellValue("City");
		header.createCell(15).setCellValue("Area");
		header.createCell(16).setCellValue("Pin Code");
		header.createCell(17).setCellValue("Pan Card No");
		header.createCell(18).setCellValue("Aadhar Card No");
		header.createCell(19).setCellValue("Personal Email ID");
		header.createCell(20).setCellValue("Personal Mobile No");
		header.createCell(21).setCellValue("Company Email ID");
		header.createCell(22).setCellValue("Company Mobile No");
		header.createCell(23).setCellValue("Joining Date");
		header.createCell(24).setCellValue("Basic Pay");
		header.createCell(25).setCellValue("HRA");
		header.createCell(26).setCellValue("DA");
		header.createCell(27).setCellValue("CA");
		header.createCell(28).setCellValue("P. F. A/C No");
		header.createCell(29).setCellValue("ESI No");
		header.createCell(30).setCellValue("PL");
		header.createCell(31).setCellValue("SL");
		header.createCell(32).setCellValue("CL");
		header.createCell(33).setCellValue("Bank Name");
		header.createCell(34).setCellValue("Bank Branch Name");
		header.createCell(35).setCellValue("IFSC");
		header.createCell(36).setCellValue("Bank A/C No");
		
		int counter = 1;
		for (Employee employeeData : employees)
		{
		  HSSFRow row = sheet.createRow(counter++);
		  row.createCell(0).setCellValue(""+employeeData.getEmployeefirstName());
		  row.createCell(1).setCellValue(""+employeeData.getEmployeemiddleName());
		  row.createCell(2).setCellValue(""+employeeData.getEmployeelastName());
	      row.createCell(3).setCellValue(""+employeeData.getEmployeeGender());
		  row.createCell(4).setCellValue(""+employeeData.getEmployeeMarried());  
		  row.createCell(5).setCellValue(""+employeeData.getEmployeeDob());
		  row.createCell(6).setCellValue(""+employeeData.getEmployeeAnniversaryDate());
		  row.createCell(7).setCellValue(""+employeeData.getEmployeeSpuseName());
		  row.createCell(8).setCellValue(""+employeeData.getEmployeeEducation());
		  row.createCell(9).setCellValue(""+employeeData.getDepartmentId());
		  row.createCell(10).setCellValue(""+employeeData.getDesignationId());
		  row.createCell(11).setCellValue(""+employeeData.getEmployeeAddress());


		  row.createCell(16).setCellValue(""+employeeData.getAreaPincode());
		  row.createCell(17).setCellValue(""+employeeData.getEmployeePancardno());
		  row.createCell(18).setCellValue(""+employeeData.getEmployeeAadharcardno());
		  row.createCell(19).setCellValue(""+employeeData.getEmployeeEmailId());
		  row.createCell(20).setCellValue(""+employeeData.getEmployeeMobileno());
		  row.createCell(21).setCellValue(""+employeeData.getCompanyEmailId());
		  row.createCell(22).setCellValue(""+employeeData.getCompanyMobileno());
		  row.createCell(23).setCellValue(""+employeeData.getEmployeeJoiningdate());
		  row.createCell(24).setCellValue(""+employeeData.getEmployeeBasicpay());
		  row.createCell(25).setCellValue(""+employeeData.getEmployeeHra());
		  row.createCell(26).setCellValue(""+employeeData.getEmployeeDa());
		  row.createCell(27).setCellValue(""+employeeData.getEmployeeCa());
		  row.createCell(28).setCellValue(""+employeeData.getEmployeePfacno());
		  row.createCell(29).setCellValue(""+employeeData.getEmployeeEsino());
		  row.createCell(30).setCellValue(""+employeeData.getEmployeePLleaves());
		  row.createCell(31).setCellValue(""+employeeData.getEmployeeSLleaves());
		  row.createCell(32).setCellValue(""+employeeData.getEmployeeCLleaves());
		  row.createCell(33).setCellValue(""+employeeData.getEmployeeBankName());
		  row.createCell(34).setCellValue(""+employeeData.getBranchName());
		  row.createCell(35).setCellValue(""+employeeData.getBankifscCode());
		  row.createCell(36).setCellValue(""+employeeData.getEmployeeBankacno());
		}
	}
    
}

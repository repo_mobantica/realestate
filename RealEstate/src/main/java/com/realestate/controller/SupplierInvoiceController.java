package com.realestate.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Employee;
import com.realestate.bean.Item;
import com.realestate.bean.MaterialsPurchased;
import com.realestate.bean.MaterialsPurchasedHistory;
import com.realestate.bean.Store;
import com.realestate.bean.StoreStock;
import com.realestate.bean.StoreStockHistory;
import com.realestate.bean.SubSupplierType;
import com.realestate.bean.Supplier;
import com.realestate.repository.BankTransactionHistoryDetailsRepository;
import com.realestate.repository.ChartofAccountRepository;
import com.realestate.repository.CompanyAccountPaymentDetailsRepository;
import com.realestate.repository.MaterialsPurchasedHistoryRepository;
import com.realestate.repository.MaterialsPurchasedRepository;
import com.realestate.repository.StoreRepository;
import com.realestate.repository.SupplierBillPaymentHistoryRepository;
import com.realestate.repository.SupplierBillPaymentRepository;
import com.realestate.repository.SupplierRepository;

@Controller
@RequestMapping("/")
public class SupplierInvoiceController {


	@Autowired
	StoreRepository storeRepository;
	@Autowired
	SupplierRepository supplierRepository;
	@Autowired
	SupplierBillPaymentHistoryRepository supplierbillpaymenthistoryRepository;
	@Autowired
	SupplierBillPaymentRepository supplierbillpaymentRepository;
	@Autowired
	CompanyAccountPaymentDetailsRepository companyaccountpaymentdetailsRepository;
	@Autowired
	MaterialsPurchasedRepository materialspurchasedRepository;
	@Autowired
	ChartofAccountRepository chartofaccountRepository;
	@Autowired
	CompanyAccountPaymentDetailsRepository companyAccountPaymentDetailsRepository;
	@Autowired
	BankTransactionHistoryDetailsRepository banktransactionhistorydetailsRepository;
	@Autowired
	MaterialsPurchasedHistoryRepository materialsPurchasedHistoryRepository;
	@Autowired
	MaterialsPurchasedRepository materialsPurchasedRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	@RequestMapping("/SupplierInvoiceMaster")
	public String SupplierInvoiceMaster(Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<Store> storeList = storeRepository.findAll();
			List<Supplier> supplierList = supplierRepository.findAll();

			Query query = new Query();
			List<MaterialsPurchased> purchesedList = mongoTemplate.find(query.addCriteria(Criteria.where("paymentStatus").ne("Clear")),MaterialsPurchased.class);

			String storeName="";
			String supplierfirmName="";
			int count=1;
			for(int i=purchesedList.size()-1;i>=0;i--)
			{
				purchesedList.get(i).setNumber(count);
				count++;
				for(int j=0;j<storeList.size();j++)
				{
					if(purchesedList.get(i).getStoreId().equals(storeList.get(j).getStoreId()))
					{
						storeName=storeList.get(j).getStoreName();
						break;
					}
				}
				purchesedList.get(i).setStoreId(storeName);

				for(int j=0;j<supplierList.size();j++)
				{
					if(purchesedList.get(i).getSupplierId().equals(supplierList.get(j).getSupplierId()))
					{
						supplierfirmName=supplierList.get(j).getSupplierfirmName();
						break;
					}
				}
				purchesedList.get(i).setSupplierId(supplierfirmName);
			}

			// Collections.sort(purchesedList, new MaterialsPurchased());
			model.addAttribute("supplierList", supplierList);
			model.addAttribute("purchesedList", purchesedList);	 

			return "SupplierInvoiceMaster";

		}catch (Exception e) {
			return "login";
		}
	}



	@RequestMapping("/SupplierInvoiceEntry")
	public String SupplierInvoiceEntry(@RequestParam("materialsPurchasedId") String materialsPurchasedId, ModelMap model)
	{
		try {
			String challanNo="";
			try {
				Query query = new Query();
				List<MaterialsPurchased> purchesedList = mongoTemplate.find(query.addCriteria(Criteria.where("materialsPurchasedId").is(materialsPurchasedId)),MaterialsPurchased.class);

				query = new Query();
				List<StoreStock> storeStockList = mongoTemplate.find(query.addCriteria(Criteria.where("materialsPurchasedId").is(materialsPurchasedId)),StoreStock.class);

				for(int i=0;i<storeStockList.size();i++)
				{
					if(i==0)
					{
						challanNo=""+storeStockList.get(i).getChallanNo();
					}
					else
					{
						challanNo=challanNo+" / "+storeStockList.get(i).getChallanNo();
					}
				}
				query = new Query();
				Store Store = mongoTemplate.findOne(query.addCriteria(Criteria.where("storeId").is(purchesedList.get(0).getStoreId())),Store.class);

				purchesedList.get(0).setStoreName(Store.getStoreName());

				query = new Query();
				List<MaterialsPurchasedHistory> purchesedMaterialList = mongoTemplate.find(query.addCriteria(Criteria.where("materialsPurchasedId").is(materialsPurchasedId)),MaterialsPurchasedHistory.class);

				query = new Query();
				List<StoreStockHistory> storeStockHistory = mongoTemplate.find(query.addCriteria(Criteria.where("materialsPurchasedId").is(materialsPurchasedId)),StoreStockHistory.class);

				double previousQuantity;
				double remainingQuantity;
				long previousNoOfPieces=0;
				double total;
				double discountPer;
				double discountAmount;
				double gstPer;
				double gstAmount;
				double grandTotal;

				double totalAmount=0.0;
				double totalDiscountAmount=0.0;
				double totalgstAmount=0.0;
				double totalGrandAmount=0.0;

				for(int i=0;i<purchesedMaterialList.size();i++)
				{
					try
					{
						query = new Query();
						Item itemDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("itemId").is(purchesedMaterialList.get(i).getItemId())), Item.class);

						query = new Query();
						SubSupplierType subsuppliertypeDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("subsuppliertypeId").is(itemDetails.getSubsuppliertypeId())), SubSupplierType.class);
						purchesedMaterialList.get(i).setSubsuppliertypeName(subsuppliertypeDetails.getSubsupplierType());
					}
					catch (Exception e) {
						// TODO: handle exception
					}

					try {
						previousQuantity=0.0;
						remainingQuantity=0.0;
						previousNoOfPieces=0;
						total=0.0;
						discountAmount=0.0;
						gstAmount=0.0;
						for(int j=0;j<storeStockHistory.size();j++)
						{
							if(storeStockHistory.get(j).getItemId().equalsIgnoreCase(purchesedMaterialList.get(i).getItemId()))
							{
								previousQuantity=previousQuantity+storeStockHistory.get(j).getItemQuantity();
								previousNoOfPieces=previousNoOfPieces+storeStockHistory.get(j).getNoOfPieces();

							}
						}


						remainingQuantity=purchesedMaterialList.get(i).getItemQuantity()-previousQuantity;
						purchesedMaterialList.get(i).setPreviousQuantity(previousQuantity);
						purchesedMaterialList.get(i).setPreviousNoOfPieces(previousNoOfPieces);
						purchesedMaterialList.get(i).setRemainingQuantity(remainingQuantity);

						purchesedMaterialList.get(i).setItemQuantity(previousQuantity);
						total=Math.round(previousQuantity*purchesedMaterialList.get(i).getRate());
						discountAmount=Math.round(total/100)*purchesedMaterialList.get(i).getDiscountPer();
						gstAmount=Math.round((total-discountAmount)/100)*purchesedMaterialList.get(i).getGstPer();
						grandTotal=Math.round(total-discountAmount+gstAmount);

						purchesedMaterialList.get(i).setTotal(total);
						purchesedMaterialList.get(i).setDiscountAmount(discountAmount);
						purchesedMaterialList.get(i).setGstAmount(gstAmount);
						purchesedMaterialList.get(i).setGrandTotal(grandTotal);


						totalAmount=totalAmount+total;
						totalDiscountAmount=totalDiscountAmount+discountAmount;
						totalgstAmount=totalgstAmount+gstAmount;
						totalGrandAmount=totalGrandAmount+grandTotal;

					}catch (Exception e) {
						// TODO: handle exception
					}	

				}

				purchesedList.get(0).setTotalPrice(totalAmount);
				purchesedList.get(0).setTotalDiscount(totalDiscountAmount);
				purchesedList.get(0).setTotalGstAmount(totalgstAmount);
				purchesedList.get(0).setTotalAmount(totalGrandAmount);
				query = new Query();
				Supplier supplier = mongoTemplate.findOne(query.addCriteria(Criteria.where("supplierId").is(purchesedList.get(0).getSupplierId())),Supplier.class);

				model.addAttribute("supplierfirmName", supplier.getSupplierfirmName());
				model.addAttribute("firmpanNumber", supplier.getFirmpanNumber());
				model.addAttribute("firmgstNumber", supplier.getFirmgstNumber()); 


				model.addAttribute("challanNo", challanNo);
				model.addAttribute("purchesedMaterialList", purchesedMaterialList);
				model.addAttribute("purchesedList", purchesedList);
			}
			catch (Exception e) {
				// TODO: handle exception
			}
			return "SupplierInvoiceEntry";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping(value = "/SupplierInvoiceEntry", method = RequestMethod.POST)
	public String SupplierInvoiceEntry(Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{

				String materialPurchesHistoriIds=req.getParameter("materialPurchesHistoriIds");
				String itemQuantitys=req.getParameter("itemQuantitys");
				String rates=req.getParameter("rates");
				String totals=req.getParameter("totals");
				String discountPers=req.getParameter("discountPers");
				String discountAmounts=req.getParameter("discountAmounts");
				String gstPers=req.getParameter("gstPers");
				String gstAmounts=req.getParameter("gstAmounts");
				String grandTotals=req.getParameter("grandTotals");
				String invoiceNo=req.getParameter("invoiceNo");
				String materialsPurchasedId=req.getParameter("materialsPurchasedId");


				String[] materialPurchesHistoriIdsArray = materialPurchesHistoriIds.split("###!###");
				String[] itemQuantitysArray = itemQuantitys.split("###!###");
				String[] ratesArray = rates.split("###!###");
				String[] totalsArray = totals.split("###!###");
				String[] discountPersArray = discountPers.split("###!###");
				String[] discountAmountsArray = discountAmounts.split("###!###");
				String[] gstPersArray = gstPers.split("###!###");
				String[] gstAmountsArray = gstAmounts.split("###!###");
				String[] grandTotalsArray = grandTotals.split("###!###");


				MaterialsPurchasedHistory materialpurches;
				Query query = new Query();
				double quantity;
				double disAmount;
				double gstAmt;
				double gTotal;
				for(int i=0;i<materialPurchesHistoriIdsArray.length;i++)
				{

					if(Double.valueOf(itemQuantitysArray[i])!=0)
					{
						query = new Query();
						materialpurches = mongoTemplate.findOne(query.addCriteria(Criteria.where("materialPurchesHistoriId").is(materialPurchesHistoriIdsArray[i])), MaterialsPurchasedHistory.class);
						quantity=materialpurches.getItemQuantity();
						//materialpurches.setItemQuantity(Double.valueOf(itemQuantitysArray[i]));
						materialpurches.setRate(Double.valueOf(ratesArray[i]));
						materialpurches.setTotal(quantity*Double.valueOf(ratesArray[i]));
						materialpurches.setDiscountPer(Double.valueOf(discountPersArray[i]));
						disAmount=Math.round((quantity*Double.valueOf(ratesArray[i]))/100)*Double.valueOf(discountPersArray[i]);
						materialpurches.setDiscountAmount(disAmount);
						materialpurches.setGstPer(Double.valueOf(gstPersArray[i]));

						gstAmt=Math.round((quantity*Double.valueOf(ratesArray[i])-disAmount)/100)*Double.valueOf(gstPersArray[i]);

						materialpurches.setGstAmount(gstAmt);
						gTotal=Math.round(gstAmt+(quantity*Double.valueOf(ratesArray[i])-disAmount));
						materialpurches.setGrandTotal(gTotal);

						materialsPurchasedHistoryRepository.save(materialpurches);

					}
				}

				query = new Query();
				MaterialsPurchased materialsPurchased = mongoTemplate.findOne(query.addCriteria(Criteria.where("materialsPurchasedId").is(materialsPurchasedId)), MaterialsPurchased.class);
				materialsPurchased.setInvoiceNo(invoiceNo);
				materialsPurchased.setStatus("Completed");
				materialsPurchasedRepository.save(materialsPurchased);

				model.addAttribute("Status","Success");

			}
			catch(DuplicateKeyException de)
			{
				de.printStackTrace();
				model.addAttribute("Status","Fail");
			}





			List<Store> storeList = storeRepository.findAll();
			List<Supplier> supplierList = supplierRepository.findAll();

			Query query = new Query();
			List<MaterialsPurchased> purchesedList = mongoTemplate.find(query.addCriteria(Criteria.where("status").ne("Completed")),MaterialsPurchased.class);

			String storeName="";
			String supplierfirmName="";
			int count=1;
			for(int i=purchesedList.size()-1;i>=0;i--)
			{
				purchesedList.get(i).setNumber(count);
				count++;
				for(int j=0;j<storeList.size();j++)
				{
					if(purchesedList.get(i).getStoreId().equals(storeList.get(j).getStoreId()))
					{
						storeName=storeList.get(j).getStoreName();
						break;
					}
				}
				purchesedList.get(i).setStoreId(storeName);

				for(int j=0;j<supplierList.size();j++)
				{
					if(purchesedList.get(i).getSupplierId().equals(supplierList.get(j).getSupplierId()))
					{
						supplierfirmName=supplierList.get(j).getSupplierfirmName();
						break;
					}
				}
				purchesedList.get(i).setSupplierId(supplierfirmName);
			}
			model.addAttribute("supplierList", supplierList);
			model.addAttribute("purchesedList", purchesedList);	 

			return "SupplierInvoiceMaster";

		}catch (Exception e) {
			e.printStackTrace();
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping(value="/getSupplierWiseInvoiceList",method=RequestMethod.POST)
	public List<MaterialsPurchased> getSupplierWiseInvoiceList(@RequestParam("supplierId") String supplierId, HttpSession session)
	{

		List<Store> storeList = storeRepository.findAll();
		List<Supplier> supplierList = supplierRepository.findAll();

		Query query = new Query();
		List<MaterialsPurchased> purchesedList = mongoTemplate.find(query.addCriteria(Criteria.where("status").ne("Completed").and("supplierId").is(supplierId)),MaterialsPurchased.class);

		String storeName="";
		String supplierfirmName="";
		int count=1;
		for(int i=purchesedList.size()-1;i>=0;i--)
		{
			purchesedList.get(i).setNumber(count);
			count++;
			for(int j=0;j<storeList.size();j++)
			{
				if(purchesedList.get(i).getStoreId().equals(storeList.get(j).getStoreId()))
				{
					storeName=storeList.get(j).getStoreName();
					break;
				}
			}
			purchesedList.get(i).setStoreId(storeName);

			for(int j=0;j<supplierList.size();j++)
			{
				if(purchesedList.get(i).getSupplierId().equals(supplierList.get(j).getSupplierId()))
				{
					supplierfirmName=supplierList.get(j).getSupplierfirmName();
					break;
				}
			}
			purchesedList.get(i).setSupplierId(supplierfirmName);
		}

		return purchesedList;
	}

}

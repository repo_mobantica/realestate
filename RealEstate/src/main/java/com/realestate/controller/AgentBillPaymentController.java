package com.realestate.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.realestate.bean.Agent;
import com.realestate.bean.AgentPayment;
import com.realestate.bean.BankTransactionHistoryDetails;
import com.realestate.bean.Booking;
import com.realestate.bean.ChartofAccount;
import com.realestate.bean.CompanyAccountPaymentDetails;
import com.realestate.bean.CompanyBanks;
import com.realestate.bean.Project;
import com.realestate.repository.AgentRepository;
import com.realestate.repository.ChartofAccountRepository;
import com.realestate.repository.CompanyAccountPaymentDetailsRepository;
import com.realestate.repository.AgentPaymentRepository;
import com.realestate.repository.BankTransactionHistoryDetailsRepository;

@Controller
@RequestMapping("/")
public class AgentBillPaymentController {

	@Autowired
	AgentRepository agentRepository;
	@Autowired
	ChartofAccountRepository chartofaccountRepository;	
	@Autowired
	AgentPaymentRepository agentpaymentRepository;
	@Autowired
	CompanyAccountPaymentDetailsRepository companyaccountpaymentdetailsRepository;
	@Autowired
	BankTransactionHistoryDetailsRepository banktransactionhistorydetailsRepository;
	@Autowired
	MongoTemplate mongoTemplate;


	String agentPaymentId;
	private String AgentPaymentId()
	{
		long cnt  = agentpaymentRepository.count();
		if(cnt<10)
		{
			agentPaymentId = "AP000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			agentPaymentId = "AP00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			agentPaymentId = "AP0"+(cnt+1);
		}
		else if(cnt>=1000)
		{
			agentPaymentId = "AP"+(cnt+1);
		}
		return agentPaymentId;
	}


	@RequestMapping("/AddAgentPayment")
	public String AddAgentPayment(Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<Agent> agentList = agentRepository.findAll(); 

			model.addAttribute("agentList",agentList);

			return "AddAgentPayment";
		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping("/AddAgentBillPayment")
	public String AddAgentBillPayment(@RequestParam("bookingId") String bookingId, ModelMap model)
	{
		try {
			Double agentAmount=0.0,agentPaidAmount=0.0;
			List<ChartofAccount> chartofaccountList = chartofaccountRepository.findAll();
			Query query = new Query();
			List<Booking> bookingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)),Booking.class);

			query = new Query();
			List<Agent> agentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("agentId").is(bookingDetails.get(0).getAgentId())), Agent.class); 

			query = new Query();
			List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(bookingDetails.get(0).getProjectId())), Project.class); 

			query = new Query();
			List<CompanyBanks> companyBankDetails = mongoTemplate.find(query.addCriteria(Criteria.where("companyId").is(projectDetails.get(0).getCompanyId())), CompanyBanks.class);
			if(agentDetails.get(0).getCommissionBased().equalsIgnoreCase("onAgreementValue"))
			{
				agentAmount=(bookingDetails.get(0).getFlatbasicCost()/100)*agentDetails.get(0).getBrokeragePercentage();
			}
			else if(agentDetails.get(0).getCommissionBased().equalsIgnoreCase("onFlatBasicAmount"))
			{
				agentAmount=(bookingDetails.get(0).getAggreementValue1()/100)*agentDetails.get(0).getBrokeragePercentage();
			}
			else
			{
				agentAmount=(bookingDetails.get(0).getGrandTotal1()/100)*agentDetails.get(0).getBrokeragePercentage();
			}

			try 
			{
				query = new Query();
				List<AgentPayment> agentpaymentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId).and("agentId").is(bookingDetails.get(0).getAgentId())), AgentPayment.class);

				if(agentpaymentDetails.size()!=0)
				{
					for(int i=0;i<agentpaymentDetails.size();i++)
					{
						agentPaidAmount=agentPaidAmount+agentpaymentDetails.get(i).getPaymentAmount();
					}
				}

			}
			catch (Exception e) {
				// TODO: handle exception
			}

			model.addAttribute("agentPaidAmount", agentPaidAmount);	  
			model.addAttribute("agentPaymentId", AgentPaymentId());		  
			model.addAttribute("agentAmount", agentAmount);	  
			model.addAttribute("companyBankDetails", companyBankDetails);
			model.addAttribute("agentDetails", agentDetails);  
			model.addAttribute("bookingDetails", bookingDetails);
			model.addAttribute("chartofaccountList", chartofaccountList);
			return "AddAgentBillPayment";
		}catch (Exception e) {
			return "login";
		}
	}



	@RequestMapping(value = "/AddAgentBillPayment", method = RequestMethod.POST)
	public String SaveAgent(@ModelAttribute AgentPayment agentpayment, HttpServletRequest req, Model model)
	{
		try {
			try
			{
				agentpayment = new AgentPayment(agentpayment.getAgentPaymentId(), agentpayment.getBookingId(), agentpayment.getAgentId(), agentpayment.getPaymentAmount(),
						agentpayment.getPaymentMode(),agentpayment.getCompanyBankId(),
						agentpayment.getRefNumber(), agentpayment.getNarration(), agentpayment.getChartaccountId(), agentpayment.getSubchartaccountId(), 
						agentpayment.getCreationDate(), agentpayment.getUpdateDate(), agentpayment.getUserName());

				agentpaymentRepository.save(agentpayment);
			}
			catch(DuplicateKeyException de)
			{
			}

			try
			{

				String userName = (String)req.getSession().getAttribute("user");
				BankTransactionHistoryDetails banktransactionhistorydetails=new BankTransactionHistoryDetails();

				banktransactionhistorydetails.setCompanyBankId(agentpayment.getCompanyBankId());
				banktransactionhistorydetails.setAmount(agentpayment.getPaymentAmount());
				banktransactionhistorydetails.setPaymentType("Agent");
				banktransactionhistorydetails.setPaymentId(agentpayment.getAgentId());
				banktransactionhistorydetails.setCreditOrDebiteType("Debited");
				banktransactionhistorydetails.setCreationDate(new Date());
				banktransactionhistorydetails.setUserName(userName);
				banktransactionhistorydetailsRepository.save(banktransactionhistorydetails);

			}
			catch (Exception e) {
				// TODO: handle exception
			}

			Query query = new Query();
			List<CompanyAccountPaymentDetails> companyBankAccount = mongoTemplate.find(query.addCriteria(Criteria.where("companyBankId").is(agentpayment.getCompanyBankId())),CompanyAccountPaymentDetails.class);
			CompanyAccountPaymentDetails companyaccountpaymentDetails=new CompanyAccountPaymentDetails(); 	

			try {
				if(companyBankAccount.size()!=0)
				{
					long currentBalance;

					companyaccountpaymentDetails = mongoTemplate.findOne(Query.query(Criteria.where("companyBankId").is(agentpayment.getCompanyBankId())), CompanyAccountPaymentDetails.class);
					currentBalance=companyaccountpaymentDetails.getCurrentBalance()-agentpayment.getPaymentAmount().longValue();
					companyaccountpaymentDetails.setCurrentBalance(currentBalance);

					companyaccountpaymentdetailsRepository.save(companyaccountpaymentDetails);
				}

			}
			catch (Exception e) {
			}




			/*	
		    	Double agentAmount=0.0,agentPaidAmount=0.0;
		   	 Query query = new Query();
			 List<Booking> bookingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(agentpayment.getBookingId())),Booking.class);

			 query = new Query();
			 List<Agent> agentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("agentId").is(bookingDetails.get(0).getAgentId())), Agent.class); 
			 agentAmount=(bookingDetails.get(0).getFlatbasicCost()/100)*agentDetails.get(0).getBrokeragePercentage();

				  try 
				  {
					  query = new Query();
					  List<AgentPayment> agentpaymentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(agentpayment.getBookingId()).and("agentId").is(agentpayment.getAgentId())), AgentPayment.class);

					  if(agentpaymentDetails.size()!=0)
					  {
						  for(int i=0;i<agentpaymentDetails.size();i++)
						  {
							  agentPaidAmount=agentPaidAmount+agentpaymentDetails.get(i).getPaymentAmount();
						  }
					  }

				  }
				  catch (Exception e) {
					// TODO: handle exception
				} 	
		    	if(agentAmount<=agentPaidAmount)
		    	{

		    	}
			 */


			// back to AddAgentPayment Page

			List<Agent> agentList = agentRepository.findAll(); 

			model.addAttribute("agentList",agentList);

			return "AddAgentPayment";
		}catch (Exception e) {
			return "login";
		}
	}  

}

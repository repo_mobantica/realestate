package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.realestate.bean.Aggreement;
import com.realestate.bean.Booking;
import com.realestate.bean.BookingCancelForm;
import com.realestate.bean.CustomerFlatCheckList;
import com.realestate.bean.CustomerLoanDetails;
import com.realestate.bean.Enquiry;
import com.realestate.bean.ExtraCharges;
import com.realestate.bean.Flat;
import com.realestate.bean.FlatPossessionCheckList;
import com.realestate.bean.Login;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.FlatPossessionCheckListRepository;
import com.realestate.repository.ProjectRepository;

@Controller
@RequestMapping("/")
public class FlatPossessionCheckListController {

	@Autowired
	FlatPossessionCheckListRepository flatpossessionchecklistRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	MongoTemplate mongoTemplate;


	String possessionId;
	private String PossessionId()
	{
		long Count = flatpossessionchecklistRepository.count();

		if(Count<10)
		{
			possessionId = "FP000"+(Count+1);
		}
		else if((Count>=10) && (Count<100))
		{
			possessionId = "FP00"+(Count+1);
		}
		else if((Count>=100) && (Count<1000))
		{
			possessionId = "FP0"+(Count+1);
		}
		else
		{
			possessionId = "FP"+(Count+1);
		}

		return possessionId;
	}

	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping("/FlatPossessionCheckList")
	public String FlatPossessionCheckList(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{	
		try {
			List<Project> projectList = GetUserAssigenedProjectList(req,res); 
			List<Booking> bookingList; 

			Query query = new Query();
			bookingList = mongoTemplate.find(query.addCriteria(Criteria.where("bookingstatus").ne("Cancel")), Booking.class);

			model.addAttribute("bookingList", bookingList);
			model.addAttribute("projectList",projectList);
			return "FlatPossessionCheckList";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/AddFlatPossessionCheckList")
	public String AddFlatPossessionCheckList(@RequestParam("bookingId") String bookingId, ModelMap model)
	{
		try {
			String possessionId;
			Query query = new Query();
			List<Booking> bookingdetails = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)), Booking.class);

			query = new Query();
			List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(bookingdetails.get(0).getProjectId())), Project.class);
			query = new Query();
			List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(bookingdetails.get(0).getBuildingId())), ProjectBuilding.class);
			query = new Query();
			List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(bookingdetails.get(0).getWingId())), ProjectWing.class);
			query = new Query();
			List<Flat> flatDetails = mongoTemplate.find(query.addCriteria(Criteria.where("flatId").is(bookingdetails.get(0).getFlatId())), Flat.class);


			query = new Query();
			List<FlatPossessionCheckList> flatpossessionchecklistdetails = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)), FlatPossessionCheckList.class);
			if(flatpossessionchecklistdetails.size()!=0)
			{
				possessionId=flatpossessionchecklistdetails.get(0).getPossessionId();
			}
			else
			{
				possessionId=PossessionId();
			}

			model.addAttribute("projectName",projectDetails.get(0).getProjectName()); 
			model.addAttribute("buildingName", buildingDetails.get(0).getBuildingName());
			model.addAttribute("wingName", wingDetails.get(0).getWingName());
			model.addAttribute("flatNumber", flatDetails.get(0).getFlatNumber());

			model.addAttribute("flatpossessionchecklistdetails",flatpossessionchecklistdetails); 
			model.addAttribute("possessionId", possessionId);
			model.addAttribute("bookingdetails", bookingdetails);
			return "AddFlatPossessionCheckList";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping(value="/AddFlatPossessionCheckList", method = RequestMethod.POST)
	public String SaveFlatPossessionCheckList(Model model, @ModelAttribute FlatPossessionCheckList flatpossessionchecklist, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try {

				flatpossessionchecklist = new FlatPossessionCheckList(flatpossessionchecklist.getPossessionId(),flatpossessionchecklist.getBookingId(),
						flatpossessionchecklist.getAllWallCracksFilledUpNeatly(),flatpossessionchecklist.getAllWaterPatchesremovedNeatly(),flatpossessionchecklist.getAllFanHooksInPosition(),
						flatpossessionchecklist.getGoodOverallFinishingOfWall(),flatpossessionchecklist.getFlooringInUniformLevel(),flatpossessionchecklist.getDadoTilesAreaProperlyFinishedandJointFilled(),
						flatpossessionchecklist.getDadoTilesAreProperLineAndInRightAngle(),flatpossessionchecklist.getNoCrackedTilesOnTheWall(),flatpossessionchecklist.getProperSlopehasbeenToBathFlooring(),
						flatpossessionchecklist.getProperSlopeGivenToTerraceOutlet(),flatpossessionchecklist.getKitchenPlatformProperlyFittedallJointsFilled(),
						flatpossessionchecklist.getNoLeakagesIntheSinkOftheKitchen(),flatpossessionchecklist.getWallBelowKitchenOttaProperlyFinished(),flatpossessionchecklist.getAllDoorsAndWindowOpenandShutProperly(),
						flatpossessionchecklist.getAllLocksTowerBoltsStoppersHingesOperateWell(),flatpossessionchecklist.getAllGlassToWindowProperlyFixedAndOperationWellAndCleaned(),
						flatpossessionchecklist.getNamePlateFixedOnMainDoor(),flatpossessionchecklist.getMsFoldingDoorInTerraceIsOperatingProperly(),flatpossessionchecklist.getEyePieceFittingTotheMainDoor(),
						flatpossessionchecklist.getAllDoorFittingAndMortiseLocksareProperlyFixed(),flatpossessionchecklist.getAllDoorMagneticCatcherareProperlyFixed(),
						flatpossessionchecklist.getAllFittingareFreePaintSpots(),flatpossessionchecklist.getCpFittingFixedProperly(),flatpossessionchecklist.getNoLeakagesInCPFittings(),flatpossessionchecklist.getHotAndColdMixerOparatesProperly(),
						flatpossessionchecklist.getFlushTankcanEasilyOprate(),flatpossessionchecklist.getEwcCoverProperlyFitted(),flatpossessionchecklist.getGapBetwwenWallTilesandWashBasinFilled(),flatpossessionchecklist.getAllSanitaryFittingareNeatlyCleaned(),
						flatpossessionchecklist.getWashbasinIsProperlyFittedAndDoesNotMove(),flatpossessionchecklist.getNoSanitaryFittingsareCracked(),flatpossessionchecklist.getBothTapsInKitchenOperative(),
						flatpossessionchecklist.getElectricMeterConnectedTotheFlat(),flatpossessionchecklist.getAllSwitchesOperatedProperly(),flatpossessionchecklist.getMcbAnddcbProperlyFixed(),
						flatpossessionchecklist.getColorOfAllSwitchesIsUniform(),flatpossessionchecklist.getAllswitchboardsareProperlyScrewed(),flatpossessionchecklist.getPaintingOfallWallsAndCeilingsIsProperlyDone(),
						flatpossessionchecklist.getOilPaintingOfWindowGrillsAndTerraceRailingDoneProperly(),
						flatpossessionchecklist.getCreationDate(),flatpossessionchecklist.getUpdateDate(),flatpossessionchecklist.getUserName());

				flatpossessionchecklistRepository.save(flatpossessionchecklist);
			}
			catch (Exception e) {
				// TODO: handle exception
			}

			List<Project> projectList = GetUserAssigenedProjectList(req,res); 
			List<Booking> bookingList; 

			Query query = new Query();
			bookingList = mongoTemplate.find(query.addCriteria(Criteria.where("bookingstatus").ne("Cancel")), Booking.class);

			model.addAttribute("bookingList", bookingList);
			model.addAttribute("projectList",projectList);
			return "FlatPossessionCheckList";

		}catch (Exception e) {
			return "login";
		}
	}

}

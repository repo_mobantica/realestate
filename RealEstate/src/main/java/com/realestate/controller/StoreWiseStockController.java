package com.realestate.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Flat;
import com.realestate.bean.FlatMarketPrice;
import com.realestate.bean.Item;
import com.realestate.bean.MaterialsPurchasedHistory;
import com.realestate.bean.Stock;
import com.realestate.bean.Store;
import com.realestate.bean.SubSupplierType;
import com.realestate.bean.SupplierType;
import com.realestate.repository.StockRepository;
import com.realestate.repository.StoreRepository;
import com.realestate.repository.StoreStockHistoryRepository;
import com.realestate.repository.StoreStockRepository;
import com.realestate.repository.SupplierTypeRepository;

@Controller
@RequestMapping("/")
public class StoreWiseStockController {

	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	SupplierTypeRepository suppliertypeRepository;
	@Autowired
	StoreStockHistoryRepository storestockhistoryRepository;
	@Autowired
	StoreStockRepository storestockRepository;
	@Autowired
	StockRepository stockRepository;
	@Autowired
	StoreRepository storeRepository;

	@RequestMapping("/StoreWiseStock")
	public String StoreWiseStock(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<Store> storeList = storeRepository.findAll();
			List<SupplierType> supplierTypeList=suppliertypeRepository.findAll();
			model.addAttribute("supplierTypeList",supplierTypeList);
			model.addAttribute("storeList",storeList);

			return "StoreWiseStock";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/getStoreStockList")
	public List<Stock> getStoreStockList(@RequestParam("storeId") String storeId, HttpSession session)
	{

		Query query= new Query();
		List<Stock> storestockList = mongoTemplate.find(query.addCriteria(Criteria.where("storeId").is(storeId)),Stock.class);

		List<Stock> storewisestockList = new ArrayList<Stock>();
		List<Item> itemDetails= new ArrayList<Item>();

		Stock storewisestock=new Stock();

		for(int i=0;i<storestockList.size();i++)
		{
			storewisestock=new Stock();
			query = new Query();
			itemDetails = mongoTemplate.find(query.addCriteria(Criteria.where("itemId").is(storestockList.get(i).getItemId())), Item.class);

			try
			{
				if(itemDetails.size()!=0)
				{
					storewisestock.setItemId(storestockList.get(i).getItemId());
					//storewisestock.setItemBrandName(storestockList.get(i).getBrandName());
					storewisestock.setItemName(itemDetails.get(0).getItemName());
					query =new Query();
					List<SupplierType> suppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("suppliertypeId").is(itemDetails.get(0).getSuppliertypeId())), SupplierType.class);

					query =new Query();
					List<SubSupplierType> subsuppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subsuppliertypeId").is(itemDetails.get(0).getSubsuppliertypeId())), SubSupplierType.class);

					storewisestock.setSuppliertypeId(suppliertypeDetails.get(0).getSupplierType());
					storewisestock.setSubsuppliertypeId(subsuppliertypeDetails.get(0).getSubsupplierType());
					storewisestock.setItemQuantity(storestockList.get(i).getCurrentStock());
					storewisestock.setNoOfPieces(storestockList.get(i).getNoOfPieces());
					storewisestockList.add(storewisestock);
					itemDetails.clear();
				}
			} 
			catch (Exception e) {
				// TODO: handle exception
			}

		}

		return storewisestockList;	
	}	


	@ResponseBody
	@RequestMapping("/getStoreAndItemTypeStockList")
	public List<Stock> getStoreAndItemTypeStockList(@RequestParam("storeId") String storeId, @RequestParam("suppliertypeId") String suppliertypeId, HttpSession session)
	{

		System.out.println("suppliertypeId "+suppliertypeId);
		Query query= new Query();
		List<Stock> storestockList = mongoTemplate.find(query.addCriteria(Criteria.where("storeId").is(storeId)),Stock.class);
		query =new Query();
		SupplierType suppliertypeDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("suppliertypeId").is(suppliertypeId)), SupplierType.class);

		List<Stock> storewisestockList = new ArrayList<Stock>();
		List<Item> itemDetails= new ArrayList<Item>();

		Stock storewisestock=new Stock();

		for(int i=0;i<storestockList.size();i++)
		{
			storewisestock=new Stock();
			query = new Query();
			itemDetails = mongoTemplate.find(query.addCriteria(Criteria.where("itemId").is(storestockList.get(i).getItemId()).and("suppliertypeId").is(suppliertypeId)), Item.class);

			try
			{
				if(itemDetails.size()!=0)
				{
					storewisestock.setItemId(storestockList.get(i).getItemId());
					//storewisestock.setItemBrandName(storestockList.get(i).getBrandName());
					storewisestock.setItemName(itemDetails.get(0).getItemName());
					
					query =new Query();
					List<SubSupplierType> subsuppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subsuppliertypeId").is(itemDetails.get(0).getSubsuppliertypeId())), SubSupplierType.class);

					storewisestock.setSuppliertypeId(suppliertypeDetails.getSupplierType());
					storewisestock.setSubsuppliertypeId(subsuppliertypeDetails.get(0).getSubsupplierType());
					storewisestock.setItemQuantity(storestockList.get(i).getCurrentStock());
					storewisestock.setNoOfPieces(storestockList.get(i).getNoOfPieces());
					storewisestockList.add(storewisestock);
					itemDetails.clear();
				}
			} 
			catch (Exception e) {
				// TODO: handle exception
			}

		}

		return storewisestockList;	
	}	

}

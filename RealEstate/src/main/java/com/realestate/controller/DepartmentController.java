package com.realestate.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Department;
import com.realestate.repository.DepartmentRepository;

@Controller
@RequestMapping("/")
public class DepartmentController
{
	@Autowired
	DepartmentRepository departmentRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	//Code for gerating department code
	String departmentCode;
	private String departmentCode()
	{
		long departmentCount = departmentRepository.count();

		if(departmentCount<10)
		{
			departmentCode = "DP000"+(departmentCount+1);
		}
		else if((departmentCount>=10) && (departmentCount<100))
		{
			departmentCode = "DP00"+(departmentCount+1);
		}
		else if((departmentCount>=100) && (departmentCount<1000))
		{
			departmentCode = "DP0"+(departmentCount+1);
		}
		else 
		{
			departmentCode = "DP"+(departmentCount+1);
		}

		return departmentCode; 
	}


	//Request Mapping For Add Department
	@RequestMapping("/DepartmentMaster")
	public String DepartmentMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		List<Department> departmentList;
		departmentList = departmentRepository.findAll();

		model.addAttribute("departmentList",departmentList);

		return "DepartmentMaster";
	}

	@ResponseBody
	@RequestMapping("/searchNameWiseDepartment")
	public List<Department> SearchNameWiseDepartment(@RequestParam("departmentName") String departmentName)
	{
		Query query = new Query();
		List<Department> departmentList = mongoTemplate.find(query.addCriteria(Criteria.where("departmentName").regex("^"+departmentName+".*","i")),Department.class);

		return departmentList;
	}

	//Request Mapping For Add Department
	@RequestMapping("/AddDepartment")
	public String addDepartment(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<Department> departmentList;
			departmentList = departmentRepository.findAll();

			model.addAttribute("departmentCode",departmentCode());
			model.addAttribute("departmentList",departmentList);

			return "AddDepartment";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/AddDepartment", method=RequestMethod.POST)
	public String departmentSave(@ModelAttribute Department department, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				department = new Department(department.getDepartmentId(),department.getDepartmentName(),department.getCreationDate(),department.getUpdateDate(),department.getUserName());
				departmentRepository.save(department);
				model.addAttribute("departmentStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("departmentStatus","Fail");
			}
			List<Department> departmentList;
			departmentList = departmentRepository.findAll();

			model.addAttribute("departmentCode",departmentCode());
			model.addAttribute("departmentList",departmentList);

			return "AddDepartment";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/getdepartmentList")
	public List<Department> AlldepartmentList(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		List<Department> departmentList= departmentRepository.findAll();

		return departmentList;
	}

	@RequestMapping("/EditDepartment")
	public String EditDepartment(@RequestParam("departmentId") String departmentId, ModelMap model)
	{
		try {
			Query query = new Query();

			List<Department> departmentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("departmentId").is(departmentId)),Department.class);

			model.addAttribute("departmentDetails",departmentDetails);
			return "EditDepartment";

		}catch (Exception e) {
			return "login";
		}
	}
	@RequestMapping(value="/EditDepartment",method=RequestMethod.POST)
	public String EditDepartmentPostMethod(@ModelAttribute Department department, ModelMap model)
	{
		try {
			try
			{
				department = new Department(department.getDepartmentId(),department.getDepartmentName(),department.getCreationDate(),department.getUpdateDate(),department.getUserName());
				departmentRepository.save(department);
				model.addAttribute("departmentStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("departmentStatus","Fail");
			}

			List<Department> departmentList = departmentRepository.findAll();

			model.addAttribute("departmentList",departmentList);

			return "DepartmentMaster";

		}catch (Exception e) {
			return "login";
		}
	}
}

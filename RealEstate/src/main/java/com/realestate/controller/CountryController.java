package com.realestate.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Country;
import com.realestate.repository.CountryRepository;

@Controller
@RequestMapping("/")
public class CountryController
{
	@Autowired
	CountryRepository countryRepository;
	@Autowired
	MongoTemplate mongoTemplate;


	String countryCode;

	private String countryCode()
	{
		long countryCount  = countryRepository.count();
		if(countryCount<10)
		{
			countryCode = "CN000"+(countryCount+1);
		}
		else if((countryCount>=10) && (countryCount<100))
		{
			countryCode = "CN00"+(countryCount+1);
		}
		else if((countryCount>=100) && (countryCount<1000))
		{
			countryCode = "CN0"+(countryCount+1);
		}
		else
		{
			countryCode = "CN"+(countryCount+1);
		}
		return countryCode;
	}


	@ResponseBody
	@RequestMapping("/searchCountryNameWiseCountryList")
	public List<Country> SearchCountryNameWise(@RequestParam("countryName") String countryName)
	{
		Query query = new Query();

		List<Country> countryList = mongoTemplate.find(query.addCriteria(Criteria.where("countryName").regex("^"+countryName+".*","i")), Country.class);
		return countryList;
	}

	@RequestMapping("/CountryMaster")
	public String CountryMaster(ModelMap model)
	{
		try {
			List<Country> countryList;
			countryList = countryRepository.findAll();

			model.addAttribute("countryList", countryList);

			return "CountryMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	//Request Mapping For AddCountry
	@RequestMapping("/AddCountry")
	public String addCountry(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			model.addAttribute("countryCode",countryCode());

			return "AddCountry";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping(value = "/AddCountry", method = RequestMethod.POST)
	public String countrySave(@ModelAttribute Country country, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				country = new Country(country.getCountryId(), country.getCountryName(), country.getCreationDate(), country.getUpdateDate(), country.getUserName());
				countryRepository.save(country);

				model.addAttribute("countryStatus", "Success");

			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("countryStatus", "Fail");
			}

			model.addAttribute("countryCode",countryCode());

			List<Country> countryList;
			countryList = countryRepository.findAll();

			model.addAttribute("countryList", countryList);

			return "AddCountry";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/EditCountry",method=RequestMethod.GET)
	public String EditCountry(@RequestParam("countryId") String countryId,ModelMap model)
	{
		try {
			Query query = new Query();
			List<Country> countryDetails = mongoTemplate.find(query.addCriteria(Criteria.where("countryId").is(countryId)), Country.class);

			model.addAttribute("countryDetails",countryDetails);

			return "EditCountry";

		}catch (Exception e) {
			return "login";
		}
	}
	@RequestMapping(value="/EditCountry",method=RequestMethod.POST)
	public String EditCountryPostMethod(@ModelAttribute Country country, ModelMap model)
	{
		try {
			try
			{
				country = new Country(country.getCountryId(), country.getCountryName(), country.getCreationDate(), country.getUpdateDate(), country.getUserName());
				countryRepository.save(country);

				model.addAttribute("countryStatus", "Success");

			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("countryStatus", "Fail");
			}

			List<Country> countryList = countryRepository.findAll();

			model.addAttribute("countryList", countryList);
			return "CountryMaster";

		}catch (Exception e) {
			return "login";
		}
	}
}

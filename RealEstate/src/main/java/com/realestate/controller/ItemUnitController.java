package com.realestate.controller;

import java.io.Serializable;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.ItemUnit;
import com.realestate.repository.ItemUnitRepository;

@Controller
@RequestMapping("/")
public class ItemUnitController implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Autowired
	ItemUnitRepository itemunitRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	ItemUnit c;
	String itemunitcode;
	private String itemunitCode()
	{
		long cnt  = itemunitRepository.count();
		if(cnt<10)
		{
			itemunitcode = "IU000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			itemunitcode = "IU00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			itemunitcode = "IU0"+(cnt+1);
		}
		else
		{
			itemunitcode = "IU"+(cnt+1);
		}
		return itemunitcode;
	}


	@RequestMapping("/AddItemUnit")
	public String addCountry(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<ItemUnit> itemunitList;
			itemunitList = itemunitRepository.findAll();

			model.addAttribute("itemunitcount",itemunitCode());
			model.addAttribute("itemunitList",itemunitList);

			return "AddItemUnit";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value = "/AddItemUnit", method = RequestMethod.POST)
	public String itemunitSave(@ModelAttribute ItemUnit itemunit, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				c = new ItemUnit(itemunit.getItemunitId(), itemunit.getItemunitName(),itemunit.getItemunitSymbol(),itemunit.getCreationDate(), itemunit.getUpdateDate(), itemunit.getUserName());
				itemunitRepository.save(c);
				model.addAttribute("itemunitStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("itemunitStatus","Fail");
			}

			List<ItemUnit> itemunitList = itemunitRepository.findAll();

			model.addAttribute("itemunitcount",itemunitCode());
			model.addAttribute("itemunitList",itemunitList);

			return "AddItemUnit";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping("/ItemUnitMaster")
	public String itemUnitMaster(ModelMap model)
	{
		try {
			List<ItemUnit> itemunitList = itemunitRepository.findAll();
			model.addAttribute("itemunitList",itemunitList);

			return "ItemUnitMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/SearchItemUnitListByName")
	public List<ItemUnit> SearchItemUnitListByName(@RequestParam("itemunitName") String itemunitName)
	{
		Query query = new Query();

		List<ItemUnit> itemunitList = mongoTemplate.find(query.addCriteria(Criteria.where("itemunitName").regex("^"+itemunitName+".*","i")),ItemUnit.class);
		return itemunitList;
	}

	@ResponseBody
	@RequestMapping("/getallitemunitList")
	public List<ItemUnit> AllitemunitList(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		List<ItemUnit> itemunitList= itemunitRepository.findAll();

		return itemunitList;
	}

	@RequestMapping("/EditItemUnit")
	public String EditItemUnit(@RequestParam("itemunitId") String itemunitId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<ItemUnit> itemunitDetails = mongoTemplate.find(query.addCriteria(Criteria.where("itemunitId").is(itemunitId)), ItemUnit.class);

			model.addAttribute("itemunitDetails",itemunitDetails);
			return "EditItemUnit";

		}catch (Exception e) {
			return "login";
		}
	}
	@RequestMapping(value="/EditItemUnit",method=RequestMethod.POST)
	public String EditItemUnitPostMethod(@ModelAttribute ItemUnit itemunit, ModelMap model)
	{
		try {
			try
			{
				c = new ItemUnit(itemunit.getItemunitId(), itemunit.getItemunitName(),itemunit.getItemunitSymbol(),itemunit.getCreationDate(), itemunit.getUpdateDate(), itemunit.getUserName());
				itemunitRepository.save(c);
				model.addAttribute("itemunitStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("itemunitStatus","Fail");
			}

			List<ItemUnit> itemunitList = itemunitRepository.findAll();
			model.addAttribute("itemunitList",itemunitList);

			return "ItemUnitMaster";

		}catch (Exception e) {
			return "login";
		}
	}
}

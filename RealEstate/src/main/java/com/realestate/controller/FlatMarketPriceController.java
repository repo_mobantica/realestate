package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.FlatMarketPrice;
import com.realestate.bean.FlatWiseMarketPrice;
import com.realestate.bean.Booking;
import com.realestate.bean.Flat;
import com.realestate.bean.Floor;
import com.realestate.bean.Login;
import com.realestate.bean.PaymentScheduler;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.Tax;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.FlatRepository;
import com.realestate.repository.ProjectBuildingRepository;
import com.realestate.repository.ProjectRepository;

@Controller
@RequestMapping("/")
public class FlatMarketPriceController 
{
	@Autowired
	ProjectBuildingRepository projectBuildingRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	FlatRepository flatRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	private double getGstCostPer()
	{
		double taxPercentage=0.0;
		Query query = new Query();
		List<Tax>  taxList = mongoTemplate.find(query.addCriteria(Criteria.where("taxName").is("GST").and("taxType").is("BOOKING SALE")), Tax.class);

		for(int i=0;i<taxList.size();i++)
		{
			taxPercentage=taxList.get(i).getTaxPercentage();
		}


		return taxPercentage;
	}

	private String getLastProjectName(HttpServletRequest req, HttpServletResponse res)
	{
		String projectId="";
		List<Project> projectList;
		projectList = GetUserAssigenedProjectList(req,res);
		for(int i=0;i<projectList.size();i++)
		{
			projectId=projectList.get(i).getProjectId();
		}
		return projectId;
	}

	private String getLastProjectBuildingName(HttpServletRequest req, HttpServletResponse res)
	{
		String projectId=getLastProjectName(req, res);
		String buildingId="";
		List<ProjectBuilding> buildingList=new ArrayList<ProjectBuilding>();;
		Query query = new Query();
		buildingList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)), ProjectBuilding.class);
		buildingId=buildingList.get(0).getBuildingId();

		return buildingId;
	}


	private String getLastProjectWingName(HttpServletRequest req, HttpServletResponse res)
	{
		String projectId=getLastProjectName(req,res);
		String buildingId=getLastProjectBuildingName(req, res);
		String wingId="";

		List<ProjectWing> WingList=new ArrayList<ProjectWing>();;
		Query query = new Query();
		WingList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId)), ProjectWing.class);
		wingId=WingList.get(0).getWingId();

		return wingId;
	}

	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping("/FlatMarketPrice")
	public String FlatMarketPrice(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			Query query3 = new Query();
			List<Tax>  taxList = mongoTemplate.find(query3.addCriteria(Criteria.where("taxName").is("GST").and("taxType").is("BOOKING SALE")), Tax.class);

			String projectId=getLastProjectName(req, res);
			String buildingId=getLastProjectBuildingName(req, res);
			String wingId=getLastProjectWingName(req, res);


			double taxPercentage=getGstCostPer();
			double gst=0.0;
			double stampDuty=0.0;
			double registeration=0.0;
			double total=0.0;
			long infrastructureCharges;
			long maintenanceCharges;
			long handlingCharges;
			Double aggreementAmount;
			long netAmount;

			List<Project> projectList = GetUserAssigenedProjectList(req,res);

			List<ProjectWing> wingList= new ArrayList<ProjectWing>();

			List<FlatMarketPrice> flat = new ArrayList<FlatMarketPrice>();

			List<Flat> flatList= new ArrayList<Flat>();

			FlatMarketPrice flatmarketprice = new FlatMarketPrice();
			int flag=0;

			Query query = new Query();

			query = new Query();
			wingList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId)), ProjectWing.class);
			infrastructureCharges=wingList.get(0).getInfrastructureCharges();
			maintenanceCharges=wingList.get(0).getMaintenanceCharges();
			handlingCharges=wingList.get(0).getHandlingCharges();

			query = new Query();
			flatList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingName").is(buildingId).and("wingId").is(wingId).and("flatType").is("RK")), Flat.class);

			if(flatList.size()!=0)
			{
				flatmarketprice.setFlatType(flatList.get(0).getFlatType());
				flatmarketprice.setFlatArea(flatList.get(0).getFlatArea());
				flatmarketprice.setFlatAreawithLoadingInFt(flatList.get(0).getFlatAreawithLoadingInFt());
				flatmarketprice.setFlatCostwithotfloorise(flatList.get(0).getFlatCostwithotfloorise());
				flatmarketprice.setFloorRise(flatList.get(0).getFloorRise());
				flatmarketprice.setInfrastructureCharges(infrastructureCharges);
				aggreementAmount=(flatList.get(0).getFlatbasicCost()+infrastructureCharges);
				flatmarketprice.setAggreementAmount(aggreementAmount);

				stampDuty=((aggreementAmount)/100)*6;
				flatmarketprice.setStampDuty(stampDuty);
				if((aggreementAmount)>3000000)
				{
					registeration=30000.0;  
				}
				else
				{
					registeration=((aggreementAmount)/100)*1;
				}
				flatmarketprice.setRegisteration(registeration);

				flatmarketprice.setHandlingCharge(handlingCharges);

				gst=(aggreementAmount/100)*taxPercentage;
				flatmarketprice.setCgst((gst/2));
				flatmarketprice.setSgst((gst/2));

				total=aggreementAmount+gst+stampDuty+registeration+handlingCharges;
				flatmarketprice.setTotalCost(total);
				flatmarketprice.setMaintenanceCharges(maintenanceCharges);
				netAmount=(long) (total+maintenanceCharges);
				flatmarketprice.setNetAmount(netAmount);
				flat.add(flatmarketprice);

				for(int i=0;i<flatList.size();i++)
				{
					flag=0; 
					flatmarketprice = new FlatMarketPrice();

					for(int j=0;j<flat.size();j++)
					{
						if(Double.compare(flatList.get(i).getFlatArea(), flat.get(j).getFlatArea())==0)
						{
							flag=1;
							break;
						}
					}
					if(flag==0)
					{

						flatmarketprice.setFlatType(flatList.get(i).getFlatType());
						flatmarketprice.setFlatArea(flatList.get(i).getFlatArea());
						flatmarketprice.setFlatAreawithLoadingInFt(flatList.get(i).getFlatAreawithLoadingInFt());
						flatmarketprice.setFlatCostwithotfloorise(flatList.get(i).getFlatCostwithotfloorise());
						flatmarketprice.setFloorRise(flatList.get(i).getFloorRise());
						flatmarketprice.setInfrastructureCharges(infrastructureCharges);
						aggreementAmount=(flatList.get(i).getFlatbasicCost()+infrastructureCharges);
						flatmarketprice.setAggreementAmount(aggreementAmount);

						stampDuty=((aggreementAmount)/100)*6;
						flatmarketprice.setStampDuty(stampDuty);
						if((aggreementAmount)>3000000)
						{
							registeration=30000.0;  
						}
						else
						{
							registeration=((aggreementAmount)/100)*1;
						}
						flatmarketprice.setRegisteration(registeration);

						flatmarketprice.setHandlingCharge(handlingCharges);

						gst=(aggreementAmount/100)*taxPercentage;
						flatmarketprice.setCgst((gst/2));
						flatmarketprice.setSgst((gst/2));

						total=aggreementAmount+gst+stampDuty+registeration+handlingCharges;
						flatmarketprice.setTotalCost(total);
						flatmarketprice.setMaintenanceCharges(maintenanceCharges);
						netAmount=(long) (total+maintenanceCharges);
						flatmarketprice.setNetAmount(netAmount);
						flat.add(flatmarketprice);
					}
				}
			}

			flatList.clear();

			// For 1BHK

			query = new Query();
			flatList = mongoTemplate.find(query.addCriteria(Criteria.where("projectName").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("flatType").is("1BHK")), Flat.class);

			if(flatList.size()!=0)
			{
				flatmarketprice.setFlatType(flatList.get(0).getFlatType());
				flatmarketprice.setFlatArea(flatList.get(0).getFlatArea());
				flatmarketprice.setFlatAreawithLoadingInFt(flatList.get(0).getFlatAreawithLoadingInFt());
				flatmarketprice.setFlatCostwithotfloorise(flatList.get(0).getFlatCostwithotfloorise());
				flatmarketprice.setFloorRise(flatList.get(0).getFloorRise());
				flatmarketprice.setInfrastructureCharges(infrastructureCharges);
				aggreementAmount=(flatList.get(0).getFlatbasicCost()+infrastructureCharges);
				flatmarketprice.setAggreementAmount(aggreementAmount);

				stampDuty=((aggreementAmount)/100)*6;
				flatmarketprice.setStampDuty(stampDuty);
				if((aggreementAmount)>3000000)
				{
					registeration=30000.0;  
				}
				else
				{
					registeration=((aggreementAmount)/100)*1;
				}
				flatmarketprice.setRegisteration(registeration);

				flatmarketprice.setHandlingCharge(handlingCharges);

				gst=(aggreementAmount/100)*taxPercentage;
				flatmarketprice.setCgst((gst/2));
				flatmarketprice.setSgst((gst/2));

				total=aggreementAmount+gst+stampDuty+registeration+handlingCharges;
				flatmarketprice.setTotalCost(total);
				flatmarketprice.setMaintenanceCharges(maintenanceCharges);
				netAmount=(long) (total+maintenanceCharges);
				flatmarketprice.setNetAmount(netAmount);
				flat.add(flatmarketprice);

				for(int i=0;i<flatList.size();i++)
				{
					flag=0; 
					flatmarketprice = new FlatMarketPrice();

					for(int j=0;j<flat.size();j++)
					{
						if(Double.compare(flatList.get(i).getFlatArea(), flat.get(j).getFlatArea())==0)
						{
							flag=1;
							break;
						}
					}
					if(flag==0)
					{

						flatmarketprice.setFlatType(flatList.get(i).getFlatType());
						flatmarketprice.setFlatArea(flatList.get(i).getFlatArea());
						flatmarketprice.setFlatAreawithLoadingInFt(flatList.get(i).getFlatAreawithLoadingInFt());
						flatmarketprice.setFlatCostwithotfloorise(flatList.get(i).getFlatCostwithotfloorise());
						flatmarketprice.setFloorRise(flatList.get(i).getFloorRise());
						flatmarketprice.setInfrastructureCharges(infrastructureCharges);
						aggreementAmount=(flatList.get(i).getFlatbasicCost()+infrastructureCharges);
						flatmarketprice.setAggreementAmount(aggreementAmount);

						stampDuty=((aggreementAmount)/100)*6;
						flatmarketprice.setStampDuty(stampDuty);
						if((aggreementAmount)>3000000)
						{
							registeration=30000.0;  
						}
						else
						{
							registeration=((aggreementAmount)/100)*1;
						}
						flatmarketprice.setRegisteration(registeration);

						flatmarketprice.setHandlingCharge(handlingCharges);

						gst=(aggreementAmount/100)*taxPercentage;
						flatmarketprice.setCgst((gst/2));
						flatmarketprice.setSgst((gst/2));

						total=aggreementAmount+gst+stampDuty+registeration+handlingCharges;
						flatmarketprice.setTotalCost(total);
						flatmarketprice.setMaintenanceCharges(maintenanceCharges);
						netAmount=(long) (total+maintenanceCharges);
						flatmarketprice.setNetAmount(netAmount);
						flat.add(flatmarketprice);
					}
				}
			}
			flatList.clear();

			// For 2BHK
			query = new Query();
			flatList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingName").is(buildingId).and("wingId").is(wingId).and("flatType").is("2BHK")), Flat.class);

			if(flatList.size()!=0)
			{
				flatmarketprice.setFlatType(flatList.get(0).getFlatType());
				flatmarketprice.setFlatArea(flatList.get(0).getFlatArea());
				flatmarketprice.setFlatAreawithLoadingInFt(flatList.get(0).getFlatAreawithLoadingInFt());
				flatmarketprice.setFlatCostwithotfloorise(flatList.get(0).getFlatCostwithotfloorise());
				flatmarketprice.setFloorRise(flatList.get(0).getFloorRise());
				flatmarketprice.setInfrastructureCharges(infrastructureCharges);
				aggreementAmount=(flatList.get(0).getFlatbasicCost()+infrastructureCharges);
				flatmarketprice.setAggreementAmount(aggreementAmount);

				stampDuty=((aggreementAmount)/100)*6;
				flatmarketprice.setStampDuty(stampDuty);
				if((aggreementAmount)>3000000)
				{
					registeration=30000.0;  
				}
				else
				{
					registeration=((aggreementAmount)/100)*1;
				}
				flatmarketprice.setRegisteration(registeration);

				flatmarketprice.setHandlingCharge(handlingCharges);

				gst=(aggreementAmount/100)*taxPercentage;
				flatmarketprice.setCgst((gst/2));
				flatmarketprice.setSgst((gst/2));

				total=aggreementAmount+gst+stampDuty+registeration+handlingCharges;
				flatmarketprice.setTotalCost(total);
				flatmarketprice.setMaintenanceCharges(maintenanceCharges);
				netAmount=(long) (total+maintenanceCharges);
				flatmarketprice.setNetAmount(netAmount);
				flat.add(flatmarketprice);

				for(int i=0;i<flatList.size();i++)
				{
					flag=0; 
					flatmarketprice = new FlatMarketPrice();

					for(int j=0;j<flat.size();j++)
					{
						if(Double.compare(flatList.get(i).getFlatArea(), flat.get(j).getFlatArea())==0)
						{
							flag=1;
							break;
						}
					}
					if(flag==0)
					{

						flatmarketprice.setFlatType(flatList.get(i).getFlatType());
						flatmarketprice.setFlatArea(flatList.get(i).getFlatArea());
						flatmarketprice.setFlatAreawithLoadingInFt(flatList.get(i).getFlatAreawithLoadingInFt());
						flatmarketprice.setFlatCostwithotfloorise(flatList.get(i).getFlatCostwithotfloorise());
						flatmarketprice.setFloorRise(flatList.get(i).getFloorRise());
						flatmarketprice.setInfrastructureCharges(infrastructureCharges);
						aggreementAmount=(flatList.get(i).getFlatbasicCost()+infrastructureCharges);
						flatmarketprice.setAggreementAmount(aggreementAmount);

						stampDuty=((aggreementAmount)/100)*6;
						flatmarketprice.setStampDuty(stampDuty);
						if((aggreementAmount)>3000000)
						{
							registeration=30000.0;  
						}
						else
						{
							registeration=((aggreementAmount)/100)*1;
						}
						flatmarketprice.setRegisteration(registeration);

						flatmarketprice.setHandlingCharge(handlingCharges);

						gst=(aggreementAmount/100)*taxPercentage;
						flatmarketprice.setCgst((gst/2));
						flatmarketprice.setSgst((gst/2));

						total=aggreementAmount+gst+stampDuty+registeration+handlingCharges;
						flatmarketprice.setTotalCost(total);
						flatmarketprice.setMaintenanceCharges(maintenanceCharges);
						netAmount=(long) (total+maintenanceCharges);
						flatmarketprice.setNetAmount(netAmount);
						flat.add(flatmarketprice);
					}
				}
			}
			flatList.clear(); 

			// For 3BHK
			query = new Query();
			flatList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("flatType").is("3BHK")), Flat.class);

			if(flatList.size()!=0)
			{
				flatmarketprice.setFlatType(flatList.get(0).getFlatType());
				flatmarketprice.setFlatArea(flatList.get(0).getFlatArea());
				flatmarketprice.setFlatAreawithLoadingInFt(flatList.get(0).getFlatAreawithLoadingInFt());
				flatmarketprice.setFlatCostwithotfloorise(flatList.get(0).getFlatCostwithotfloorise());
				flatmarketprice.setFloorRise(flatList.get(0).getFloorRise());
				flatmarketprice.setInfrastructureCharges(infrastructureCharges);
				aggreementAmount=(flatList.get(0).getFlatbasicCost()+infrastructureCharges);
				flatmarketprice.setAggreementAmount(aggreementAmount);

				stampDuty=((aggreementAmount)/100)*6;
				flatmarketprice.setStampDuty(stampDuty);
				if((aggreementAmount)>3000000)
				{
					registeration=30000.0;  
				}
				else
				{
					registeration=((aggreementAmount)/100)*1;
				}
				flatmarketprice.setRegisteration(registeration);

				flatmarketprice.setHandlingCharge(handlingCharges);

				gst=(aggreementAmount/100)*taxPercentage;
				flatmarketprice.setCgst((gst/2));
				flatmarketprice.setSgst((gst/2));

				total=aggreementAmount+gst+stampDuty+registeration+handlingCharges;
				flatmarketprice.setTotalCost(total);
				flatmarketprice.setMaintenanceCharges(maintenanceCharges);
				netAmount=(long) (total+maintenanceCharges);
				flatmarketprice.setNetAmount(netAmount);
				flat.add(flatmarketprice);

				for(int i=0;i<flatList.size();i++)
				{
					flag=0; 
					flatmarketprice = new FlatMarketPrice();

					for(int j=0;j<flat.size();j++)
					{
						if(Double.compare(flatList.get(i).getFlatArea(), flat.get(j).getFlatArea())==0)
						{
							flag=1;
							break;
						}
					}
					if(flag==0)
					{

						flatmarketprice.setFlatType(flatList.get(i).getFlatType());
						flatmarketprice.setFlatArea(flatList.get(i).getFlatArea());
						flatmarketprice.setFlatAreawithLoadingInFt(flatList.get(i).getFlatAreawithLoadingInFt());
						flatmarketprice.setFlatCostwithotfloorise(flatList.get(i).getFlatCostwithotfloorise());
						flatmarketprice.setFloorRise(flatList.get(i).getFloorRise());
						flatmarketprice.setInfrastructureCharges(infrastructureCharges);
						aggreementAmount=(flatList.get(i).getFlatbasicCost()+infrastructureCharges);
						flatmarketprice.setAggreementAmount(aggreementAmount);

						stampDuty=((aggreementAmount)/100)*6;
						flatmarketprice.setStampDuty(stampDuty);
						if((aggreementAmount)>3000000)
						{
							registeration=30000.0;  
						}
						else
						{
							registeration=((aggreementAmount)/100)*1;
						}
						flatmarketprice.setRegisteration(registeration);

						flatmarketprice.setHandlingCharge(handlingCharges);

						gst=(aggreementAmount/100)*taxPercentage;
						flatmarketprice.setCgst((gst/2));
						flatmarketprice.setSgst((gst/2));

						total=aggreementAmount+gst+stampDuty+registeration+handlingCharges;
						flatmarketprice.setTotalCost(total);
						flatmarketprice.setMaintenanceCharges(maintenanceCharges);
						netAmount=(long) (total+maintenanceCharges);
						flatmarketprice.setNetAmount(netAmount);
						flat.add(flatmarketprice);
					}
				}
			}
			flatList.clear();

			// for 4BHK

			query = new Query();
			flatList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("flatType").is("RK")), Flat.class);

			if(flatList.size()!=0)
			{
				flatmarketprice.setFlatType(flatList.get(0).getFlatType());
				flatmarketprice.setFlatArea(flatList.get(0).getFlatArea());
				flatmarketprice.setFlatAreawithLoadingInFt(flatList.get(0).getFlatAreawithLoadingInFt());
				flatmarketprice.setFlatCostwithotfloorise(flatList.get(0).getFlatCostwithotfloorise());
				flatmarketprice.setFloorRise(flatList.get(0).getFloorRise());
				flatmarketprice.setInfrastructureCharges(infrastructureCharges);
				aggreementAmount=(flatList.get(0).getFlatbasicCost()+infrastructureCharges);
				flatmarketprice.setAggreementAmount(aggreementAmount);

				stampDuty=((aggreementAmount)/100)*6;
				flatmarketprice.setStampDuty(stampDuty);
				if((aggreementAmount)>3000000)
				{
					registeration=30000.0;  
				}
				else
				{
					registeration=((aggreementAmount)/100)*1;
				}
				flatmarketprice.setRegisteration(registeration);

				flatmarketprice.setHandlingCharge(handlingCharges);

				gst=(aggreementAmount/100)*taxPercentage;
				flatmarketprice.setCgst((gst/2));
				flatmarketprice.setSgst((gst/2));

				total=aggreementAmount+gst+stampDuty+registeration+handlingCharges;
				flatmarketprice.setTotalCost(total);
				flatmarketprice.setMaintenanceCharges(maintenanceCharges);
				netAmount=(long) (total+maintenanceCharges);
				flatmarketprice.setNetAmount(netAmount);
				flat.add(flatmarketprice);

				for(int i=0;i<flatList.size();i++)
				{
					flag=0; 
					flatmarketprice = new FlatMarketPrice();

					for(int j=0;j<flat.size();j++)
					{
						if(Double.compare(flatList.get(i).getFlatArea(), flat.get(j).getFlatArea())==0)
						{
							flag=1;
							break;
						}
					}
					if(flag==0)
					{

						flatmarketprice.setFlatType(flatList.get(i).getFlatType());
						flatmarketprice.setFlatArea(flatList.get(i).getFlatArea());
						flatmarketprice.setFlatAreawithLoadingInFt(flatList.get(i).getFlatAreawithLoadingInFt());
						flatmarketprice.setFlatCostwithotfloorise(flatList.get(i).getFlatCostwithotfloorise());
						flatmarketprice.setFloorRise(flatList.get(i).getFloorRise());
						flatmarketprice.setInfrastructureCharges(infrastructureCharges);
						aggreementAmount=(flatList.get(i).getFlatbasicCost()+infrastructureCharges);
						flatmarketprice.setAggreementAmount(aggreementAmount);

						stampDuty=((aggreementAmount)/100)*6;
						flatmarketprice.setStampDuty(stampDuty);
						if((aggreementAmount)>3000000)
						{
							registeration=30000.0;  
						}
						else
						{
							registeration=((aggreementAmount)/100)*1;
						}
						flatmarketprice.setRegisteration(registeration);

						flatmarketprice.setHandlingCharge(handlingCharges);

						gst=(aggreementAmount/100)*taxPercentage;
						flatmarketprice.setCgst((gst/2));
						flatmarketprice.setSgst((gst/2));

						total=aggreementAmount+gst+stampDuty+registeration+handlingCharges;
						flatmarketprice.setTotalCost(total);
						flatmarketprice.setMaintenanceCharges(maintenanceCharges);
						netAmount=(long) (total+maintenanceCharges);
						flatmarketprice.setNetAmount(netAmount);
						flat.add(flatmarketprice);
					}
				}
			}
			flatList.clear();


			model.addAttribute("taxList",taxList);
			model.addAttribute("flat",flat);
			model.addAttribute("projectName1",projectId);
			model.addAttribute("buildingName1",buildingId);
			model.addAttribute("wingName1",wingId);
			model.addAttribute("projectList",projectList);

			return "FlatMarketPrice";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping(value="/SearchMarkertPrice",method=RequestMethod.POST)
	private List<FlatMarketPrice> SearchMarkertPrice(@RequestParam("wingId") String wingId, @RequestParam("buildingId") String buildingId, @RequestParam("projectId") String projectId, HttpSession session, HttpServletRequest req, HttpServletResponse res)
	{

		double taxPercentage=getGstCostPer();
		double gst=0.0;
		double stampDuty=0.0;
		double registeration=0.0;
		double total=0.0;
		long infrastructureCharges;
		long maintenanceCharges;
		long handlingCharges;
		Double aggreementAmount;
		long netAmount;
		List<FlatMarketPrice> flat = new ArrayList<FlatMarketPrice>();


		List<Project> projectList = GetUserAssigenedProjectList(req,res);

		List<Flat> flatList= new ArrayList<Flat>();
		List<ProjectWing> wingList= new ArrayList<ProjectWing>();


		FlatMarketPrice flatmarketprice = new FlatMarketPrice();
		int flag=0;
		Query query = new Query();

		query = new Query();
		wingList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId)), ProjectWing.class);
		infrastructureCharges=wingList.get(0).getInfrastructureCharges();
		maintenanceCharges=wingList.get(0).getMaintenanceCharges();
		handlingCharges=wingList.get(0).getHandlingCharges();

		query = new Query();
		flatList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("flatType").is("RK")), Flat.class);

		if(flatList.size()!=0)
		{
			flatmarketprice.setFlatType(flatList.get(0).getFlatType());
			flatmarketprice.setFlatArea(flatList.get(0).getFlatArea());
			flatmarketprice.setFlatAreawithLoadingInFt(flatList.get(0).getFlatAreawithLoadingInFt());
			flatmarketprice.setFlatCostwithotfloorise(flatList.get(0).getFlatCostwithotfloorise());
			flatmarketprice.setFloorRise(flatList.get(0).getFloorRise());
			flatmarketprice.setInfrastructureCharges(infrastructureCharges);
			aggreementAmount=(flatList.get(0).getFlatbasicCost()+infrastructureCharges);
			flatmarketprice.setAggreementAmount(aggreementAmount);

			stampDuty=((aggreementAmount)/100)*6;
			flatmarketprice.setStampDuty(stampDuty);
			if((aggreementAmount)>3000000)
			{
				registeration=30000.0;  
			}
			else
			{
				registeration=((aggreementAmount)/100)*1;
			}
			flatmarketprice.setRegisteration(registeration);

			flatmarketprice.setHandlingCharge(handlingCharges);

			gst=(aggreementAmount/100)*taxPercentage;
			flatmarketprice.setCgst((gst/2));
			flatmarketprice.setSgst((gst/2));

			total=aggreementAmount+gst+stampDuty+registeration+handlingCharges;
			flatmarketprice.setTotalCost(total);
			flatmarketprice.setMaintenanceCharges(maintenanceCharges);
			netAmount=(long) (total+maintenanceCharges);
			flatmarketprice.setNetAmount(netAmount);
			flat.add(flatmarketprice);

			for(int i=0;i<flatList.size();i++)
			{
				flag=0; 
				flatmarketprice = new FlatMarketPrice();

				for(int j=0;j<flat.size();j++)
				{
					if(Double.compare(flatList.get(i).getFlatArea(), flat.get(j).getFlatArea())==0)
					{
						flag=1;
						break;
					}
				}
				if(flag==0)
				{

					flatmarketprice.setFlatType(flatList.get(i).getFlatType());
					flatmarketprice.setFlatArea(flatList.get(i).getFlatArea());
					flatmarketprice.setFlatAreawithLoadingInFt(flatList.get(i).getFlatAreawithLoadingInFt());
					flatmarketprice.setFlatCostwithotfloorise(flatList.get(i).getFlatCostwithotfloorise());
					flatmarketprice.setFloorRise(flatList.get(i).getFloorRise());
					flatmarketprice.setInfrastructureCharges(infrastructureCharges);
					aggreementAmount=(flatList.get(i).getFlatbasicCost()+infrastructureCharges);
					flatmarketprice.setAggreementAmount(aggreementAmount);

					stampDuty=((aggreementAmount)/100)*6;
					flatmarketprice.setStampDuty(stampDuty);
					if((aggreementAmount)>3000000)
					{
						registeration=30000.0;  
					}
					else
					{
						registeration=((aggreementAmount)/100)*1;
					}
					flatmarketprice.setRegisteration(registeration);

					flatmarketprice.setHandlingCharge(handlingCharges);

					gst=(aggreementAmount/100)*taxPercentage;
					flatmarketprice.setCgst((gst/2));
					flatmarketprice.setSgst((gst/2));

					total=aggreementAmount+gst+stampDuty+registeration+handlingCharges;
					flatmarketprice.setTotalCost(total);
					flatmarketprice.setMaintenanceCharges(maintenanceCharges);
					netAmount=(long) (total+maintenanceCharges);
					flatmarketprice.setNetAmount(netAmount);
					flat.add(flatmarketprice);
				}
			}
		}

		flatList.clear();

		// For 1BHK

		query = new Query();
		flatList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("flatType").is("1BHK")), Flat.class);

		if(flatList.size()!=0)
		{
			flatmarketprice.setFlatType(flatList.get(0).getFlatType());
			flatmarketprice.setFlatArea(flatList.get(0).getFlatArea());
			flatmarketprice.setFlatAreawithLoadingInFt(flatList.get(0).getFlatAreawithLoadingInFt());
			flatmarketprice.setFlatCostwithotfloorise(flatList.get(0).getFlatCostwithotfloorise());
			flatmarketprice.setFloorRise(flatList.get(0).getFloorRise());
			flatmarketprice.setInfrastructureCharges(infrastructureCharges);
			aggreementAmount=(flatList.get(0).getFlatbasicCost()+infrastructureCharges);
			flatmarketprice.setAggreementAmount(aggreementAmount);

			stampDuty=((aggreementAmount)/100)*6;
			flatmarketprice.setStampDuty(stampDuty);
			if((aggreementAmount)>3000000)
			{
				registeration=30000.0;  
			}
			else
			{
				registeration=((aggreementAmount)/100)*1;
			}
			flatmarketprice.setRegisteration(registeration);

			flatmarketprice.setHandlingCharge(handlingCharges);

			gst=(aggreementAmount/100)*taxPercentage;
			flatmarketprice.setCgst((gst/2));
			flatmarketprice.setSgst((gst/2));

			total=aggreementAmount+gst+stampDuty+registeration+handlingCharges;
			flatmarketprice.setTotalCost(total);
			flatmarketprice.setMaintenanceCharges(maintenanceCharges);
			netAmount=(long) (total+maintenanceCharges);
			flatmarketprice.setNetAmount(netAmount);
			flat.add(flatmarketprice);

			for(int i=0;i<flatList.size();i++)
			{
				flag=0; 
				flatmarketprice = new FlatMarketPrice();

				for(int j=0;j<flat.size();j++)
				{
					if(Double.compare(flatList.get(i).getFlatArea(), flat.get(j).getFlatArea())==0)
					{
						flag=1;
						break;
					}
				}
				if(flag==0)
				{

					flatmarketprice.setFlatType(flatList.get(i).getFlatType());
					flatmarketprice.setFlatArea(flatList.get(i).getFlatArea());
					flatmarketprice.setFlatAreawithLoadingInFt(flatList.get(i).getFlatAreawithLoadingInFt());
					flatmarketprice.setFlatCostwithotfloorise(flatList.get(i).getFlatCostwithotfloorise());
					flatmarketprice.setFloorRise(flatList.get(i).getFloorRise());
					flatmarketprice.setInfrastructureCharges(infrastructureCharges);
					aggreementAmount=(flatList.get(i).getFlatbasicCost()+infrastructureCharges);
					flatmarketprice.setAggreementAmount(aggreementAmount);

					stampDuty=((aggreementAmount)/100)*6;
					flatmarketprice.setStampDuty(stampDuty);
					if((aggreementAmount)>3000000)
					{
						registeration=30000.0;  
					}
					else
					{
						registeration=((aggreementAmount)/100)*1;
					}
					flatmarketprice.setRegisteration(registeration);

					flatmarketprice.setHandlingCharge(handlingCharges);

					gst=(aggreementAmount/100)*taxPercentage;
					flatmarketprice.setCgst((gst/2));
					flatmarketprice.setSgst((gst/2));

					total=aggreementAmount+gst+stampDuty+registeration+handlingCharges;
					flatmarketprice.setTotalCost(total);
					flatmarketprice.setMaintenanceCharges(maintenanceCharges);
					netAmount=(long) (total+maintenanceCharges);
					flatmarketprice.setNetAmount(netAmount);
					flat.add(flatmarketprice);
				}
			}
		}
		flatList.clear();

		// For 2BHK
		query = new Query();
		flatList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("flatType").is("2BHK")), Flat.class);

		if(flatList.size()!=0)
		{
			flatmarketprice.setFlatType(flatList.get(0).getFlatType());
			flatmarketprice.setFlatArea(flatList.get(0).getFlatArea());
			flatmarketprice.setFlatAreawithLoadingInFt(flatList.get(0).getFlatAreawithLoadingInFt());
			flatmarketprice.setFlatCostwithotfloorise(flatList.get(0).getFlatCostwithotfloorise());
			flatmarketprice.setFloorRise(flatList.get(0).getFloorRise());
			flatmarketprice.setInfrastructureCharges(infrastructureCharges);
			aggreementAmount=(flatList.get(0).getFlatbasicCost()+infrastructureCharges);
			flatmarketprice.setAggreementAmount(aggreementAmount);

			stampDuty=((aggreementAmount)/100)*6;
			flatmarketprice.setStampDuty(stampDuty);
			if((aggreementAmount)>3000000)
			{
				registeration=30000.0;  
			}
			else
			{
				registeration=((aggreementAmount)/100)*1;
			}
			flatmarketprice.setRegisteration(registeration);

			flatmarketprice.setHandlingCharge(handlingCharges);

			gst=(aggreementAmount/100)*taxPercentage;
			flatmarketprice.setCgst((gst/2));
			flatmarketprice.setSgst((gst/2));

			total=aggreementAmount+gst+stampDuty+registeration+handlingCharges;
			flatmarketprice.setTotalCost(total);
			flatmarketprice.setMaintenanceCharges(maintenanceCharges);
			netAmount=(long) (total+maintenanceCharges);
			flatmarketprice.setNetAmount(netAmount);
			flat.add(flatmarketprice);

			for(int i=0;i<flatList.size();i++)
			{
				flag=0; 
				flatmarketprice = new FlatMarketPrice();

				for(int j=0;j<flat.size();j++)
				{
					if(Double.compare(flatList.get(i).getFlatArea(), flat.get(j).getFlatArea())==0)
					{
						flag=1;
						break;
					}
				}
				if(flag==0)
				{

					flatmarketprice.setFlatType(flatList.get(i).getFlatType());
					flatmarketprice.setFlatArea(flatList.get(i).getFlatArea());
					flatmarketprice.setFlatAreawithLoadingInFt(flatList.get(i).getFlatAreawithLoadingInFt());
					flatmarketprice.setFlatCostwithotfloorise(flatList.get(i).getFlatCostwithotfloorise());
					flatmarketprice.setFloorRise(flatList.get(i).getFloorRise());
					flatmarketprice.setInfrastructureCharges(infrastructureCharges);
					aggreementAmount=(flatList.get(i).getFlatbasicCost()+infrastructureCharges);
					flatmarketprice.setAggreementAmount(aggreementAmount);

					stampDuty=((aggreementAmount)/100)*6;
					flatmarketprice.setStampDuty(stampDuty);
					if((aggreementAmount)>3000000)
					{
						registeration=30000.0;  
					}
					else
					{
						registeration=((aggreementAmount)/100)*1;
					}
					flatmarketprice.setRegisteration(registeration);

					flatmarketprice.setHandlingCharge(handlingCharges);

					gst=(aggreementAmount/100)*taxPercentage;
					flatmarketprice.setCgst((gst/2));
					flatmarketprice.setSgst((gst/2));

					total=aggreementAmount+gst+stampDuty+registeration+handlingCharges;
					flatmarketprice.setTotalCost(total);
					flatmarketprice.setMaintenanceCharges(maintenanceCharges);
					netAmount=(long) (total+maintenanceCharges);
					flatmarketprice.setNetAmount(netAmount);
					flat.add(flatmarketprice);
				}
			}
		}
		flatList.clear(); 

		// For 3BHK
		query = new Query();
		flatList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("flatType").is("3BHK")), Flat.class);

		if(flatList.size()!=0)
		{
			flatmarketprice.setFlatType(flatList.get(0).getFlatType());
			flatmarketprice.setFlatArea(flatList.get(0).getFlatArea());
			flatmarketprice.setFlatAreawithLoadingInFt(flatList.get(0).getFlatAreawithLoadingInFt());
			flatmarketprice.setFlatCostwithotfloorise(flatList.get(0).getFlatCostwithotfloorise());
			flatmarketprice.setFloorRise(flatList.get(0).getFloorRise());
			flatmarketprice.setInfrastructureCharges(infrastructureCharges);
			aggreementAmount=(flatList.get(0).getFlatbasicCost()+infrastructureCharges);
			flatmarketprice.setAggreementAmount(aggreementAmount);

			stampDuty=((aggreementAmount)/100)*6;
			flatmarketprice.setStampDuty(stampDuty);
			if((aggreementAmount)>3000000)
			{
				registeration=30000.0;  
			}
			else
			{
				registeration=((aggreementAmount)/100)*1;
			}
			flatmarketprice.setRegisteration(registeration);

			flatmarketprice.setHandlingCharge(handlingCharges);

			gst=(aggreementAmount/100)*taxPercentage;
			flatmarketprice.setCgst((gst/2));
			flatmarketprice.setSgst((gst/2));

			total=aggreementAmount+gst+stampDuty+registeration+handlingCharges;
			flatmarketprice.setTotalCost(total);
			flatmarketprice.setMaintenanceCharges(maintenanceCharges);
			netAmount=(long) (total+maintenanceCharges);
			flatmarketprice.setNetAmount(netAmount);
			flat.add(flatmarketprice);

			for(int i=0;i<flatList.size();i++)
			{
				flag=0; 
				flatmarketprice = new FlatMarketPrice();

				for(int j=0;j<flat.size();j++)
				{
					if(Double.compare(flatList.get(i).getFlatArea(), flat.get(j).getFlatArea())==0)
					{
						flag=1;
						break;
					}
				}
				if(flag==0)
				{

					flatmarketprice.setFlatType(flatList.get(i).getFlatType());
					flatmarketprice.setFlatArea(flatList.get(i).getFlatArea());
					flatmarketprice.setFlatAreawithLoadingInFt(flatList.get(i).getFlatAreawithLoadingInFt());
					flatmarketprice.setFlatCostwithotfloorise(flatList.get(i).getFlatCostwithotfloorise());
					flatmarketprice.setFloorRise(flatList.get(i).getFloorRise());
					flatmarketprice.setInfrastructureCharges(infrastructureCharges);
					aggreementAmount=(flatList.get(i).getFlatbasicCost()+infrastructureCharges);
					flatmarketprice.setAggreementAmount(aggreementAmount);

					stampDuty=((aggreementAmount)/100)*6;
					flatmarketprice.setStampDuty(stampDuty);
					if((aggreementAmount)>3000000)
					{
						registeration=30000.0;  
					}
					else
					{
						registeration=((aggreementAmount)/100)*1;
					}
					flatmarketprice.setRegisteration(registeration);

					flatmarketprice.setHandlingCharge(handlingCharges);

					gst=(aggreementAmount/100)*taxPercentage;
					flatmarketprice.setCgst((gst/2));
					flatmarketprice.setSgst((gst/2));

					total=aggreementAmount+gst+stampDuty+registeration+handlingCharges;
					flatmarketprice.setTotalCost(total);
					flatmarketprice.setMaintenanceCharges(maintenanceCharges);
					netAmount=(long) (total+maintenanceCharges);
					flatmarketprice.setNetAmount(netAmount);
					flat.add(flatmarketprice);
				}
			}
		}
		flatList.clear();

		// for 4BHK

		query = new Query();
		flatList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("flatType").is("RK")), Flat.class);

		if(flatList.size()!=0)
		{
			flatmarketprice.setFlatType(flatList.get(0).getFlatType());
			flatmarketprice.setFlatArea(flatList.get(0).getFlatArea());
			flatmarketprice.setFlatAreawithLoadingInFt(flatList.get(0).getFlatAreawithLoadingInFt());
			flatmarketprice.setFlatCostwithotfloorise(flatList.get(0).getFlatCostwithotfloorise());
			flatmarketprice.setFloorRise(flatList.get(0).getFloorRise());
			flatmarketprice.setInfrastructureCharges(infrastructureCharges);
			aggreementAmount=(flatList.get(0).getFlatbasicCost()+infrastructureCharges);
			flatmarketprice.setAggreementAmount(aggreementAmount);

			stampDuty=((aggreementAmount)/100)*6;
			flatmarketprice.setStampDuty(stampDuty);
			if((aggreementAmount)>3000000)
			{
				registeration=30000.0;  
			}
			else
			{
				registeration=((aggreementAmount)/100)*1;
			}
			flatmarketprice.setRegisteration(registeration);

			flatmarketprice.setHandlingCharge(handlingCharges);

			gst=(aggreementAmount/100)*taxPercentage;
			flatmarketprice.setCgst((gst/2));
			flatmarketprice.setSgst((gst/2));

			total=aggreementAmount+gst+stampDuty+registeration+handlingCharges;
			flatmarketprice.setTotalCost(total);
			flatmarketprice.setMaintenanceCharges(maintenanceCharges);
			netAmount=(long) (total+maintenanceCharges);
			flatmarketprice.setNetAmount(netAmount);
			flat.add(flatmarketprice);

			for(int i=0;i<flatList.size();i++)
			{
				flag=0; 
				flatmarketprice = new FlatMarketPrice();

				for(int j=0;j<flat.size();j++)
				{
					if(Double.compare(flatList.get(i).getFlatArea(), flat.get(j).getFlatArea())==0)
					{
						flag=1;
						break;
					}
				}
				if(flag==0)
				{

					flatmarketprice.setFlatType(flatList.get(i).getFlatType());
					flatmarketprice.setFlatArea(flatList.get(i).getFlatArea());
					flatmarketprice.setFlatAreawithLoadingInFt(flatList.get(i).getFlatAreawithLoadingInFt());
					flatmarketprice.setFlatCostwithotfloorise(flatList.get(i).getFlatCostwithotfloorise());
					flatmarketprice.setFloorRise(flatList.get(i).getFloorRise());
					flatmarketprice.setInfrastructureCharges(infrastructureCharges);
					aggreementAmount=(flatList.get(i).getFlatbasicCost()+infrastructureCharges);
					flatmarketprice.setAggreementAmount(aggreementAmount);

					stampDuty=((aggreementAmount)/100)*6;
					flatmarketprice.setStampDuty(stampDuty);
					if((aggreementAmount)>3000000)
					{
						registeration=30000.0;  
					}
					else
					{
						registeration=((aggreementAmount)/100)*1;
					}
					flatmarketprice.setRegisteration(registeration);

					flatmarketprice.setHandlingCharge(handlingCharges);

					gst=(aggreementAmount/100)*taxPercentage;
					flatmarketprice.setCgst((gst/2));
					flatmarketprice.setSgst((gst/2));

					total=aggreementAmount+gst+stampDuty+registeration+handlingCharges;
					flatmarketprice.setTotalCost(total);
					flatmarketprice.setMaintenanceCharges(maintenanceCharges);
					netAmount=(long) (total+maintenanceCharges);
					flatmarketprice.setNetAmount(netAmount);
					flat.add(flatmarketprice);
				}
			}
		}
		flatList.clear();

		return flat;
	}



	@RequestMapping("/FlatWiseMarketPrice")
	public String FlatWiseMarketPrice(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<Project> projectList = GetUserAssigenedProjectList(req,res);
			double taxPercentage=getGstCostPer();
			model.addAttribute("taxPercentage",taxPercentage);
			model.addAttribute("projectList",projectList);
			return "FlatWiseMarketPrice";

		}catch (Exception e) {
			return "login";
		}
	}


	@ResponseBody
	@RequestMapping(value="/SearchFlatWiseMarkertPrice",method=RequestMethod.POST)
	private FlatMarketPrice SearchFlatWiseMarkertPrice(@RequestParam("flatNumber") String flatNumber, @RequestParam("wingId") String wingId, @RequestParam("buildingId") String buildingId, @RequestParam("projectId") String projectId, HttpSession session, HttpServletRequest req, HttpServletResponse res)
	{

		//double taxPercentage=getGstCostPer();
		double gst=0.0;
		double stampDuty=0.0;
		double registeration=0.0;
		double gstPer=0.0;
		double stampDutyPer=0.0;
		double registerationPer=0.0;
		double total=0.0;
		long infrastructureCharges;
		long maintenanceCharges;
		long handlingCharges;

		Double aggreementAmount;
		long netAmount;
		Query query = new Query();
		Project projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectId)), Project.class);
		gstPer=projectDetails.getGstPer();
		stampDutyPer=projectDetails.getStampdutyPer();
		registerationPer=projectDetails.getRegistrationPer();
		List<ProjectWing> wingList= new ArrayList<ProjectWing>();
		query = new Query();
		wingList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId)), ProjectWing.class);
		infrastructureCharges=wingList.get(0).getInfrastructureCharges();
		maintenanceCharges=wingList.get(0).getMaintenanceCharges();
		handlingCharges=wingList.get(0).getHandlingCharges();

		FlatMarketPrice flatmarketprice=new FlatMarketPrice();
		Flat flatDetails=new Flat();
		query = new Query();
		flatDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("flatNumber").is(flatNumber).and("flatstatus").is("Booking Remaninig")), Flat.class);

		flatmarketprice.setFlatArea(flatDetails.getFlatArea());
		flatmarketprice.setFlatAreawithLoadingInFt(flatDetails.getFlatAreawithLoadingInFt());
		flatmarketprice.setFlatCostwithotfloorise(flatDetails.getFlatCostwithotfloorise());
		flatmarketprice.setFlatCost(flatDetails.getFlatCost());
		flatmarketprice.setFloorRise(flatDetails.getFloorRise());
		flatmarketprice.setInfrastructureCharges(infrastructureCharges);
		aggreementAmount=(flatDetails.getFlatbasicCost()+infrastructureCharges);
		flatmarketprice.setAggreementAmount(aggreementAmount);

		stampDuty=((aggreementAmount)/100)*stampDutyPer;
		flatmarketprice.setStampDuty(stampDuty);
		if((aggreementAmount)>3000000)
		{
			registeration=30000.0;  
		}
		else
		{
			registeration=((aggreementAmount)/100)*registerationPer;
		}
		flatmarketprice.setRegisteration(registeration);

		flatmarketprice.setHandlingCharge(handlingCharges);

		gst=(aggreementAmount/100)*gstPer;
		flatmarketprice.setCgst((gst/2));
		flatmarketprice.setSgst((gst/2));

		total=aggreementAmount+gst+stampDuty+registeration+handlingCharges;
		flatmarketprice.setTotalCost(total);
		flatmarketprice.setMaintenanceCharges(maintenanceCharges);
		netAmount=(long) (total+maintenanceCharges);
		flatmarketprice.setNetAmount(netAmount);


		return flatmarketprice;
	}


}

package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Agent;
import com.realestate.bean.AgentEmployees;
import com.realestate.bean.CompanyBanks;
import com.realestate.bean.Designation;
import com.realestate.bean.Login;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBankDetails;
import com.realestate.bean.ProjectDetails;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.CompanyRepository;
import com.realestate.repository.ProjectDetailsRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.ProjectBankDetailsRepository;

@Controller
@RequestMapping("/")
public class ProjectDetailsController {

	@Autowired
	ProjectDetailsRepository projectdetailsRepository;
	@Autowired
	CompanyRepository companyRepository;
	@Autowired
	ProjectRepository projectRepository;

	@Autowired
	ProjectBankDetailsRepository projectbankdetailsRepository;

	@Autowired
	MongoTemplate mongoTemplate;

	String projectBankCode;
	private String ProjectBankCode()
	{
		long bankCount = projectbankdetailsRepository.count();

		if(bankCount<10)
		{
			projectBankCode = "PB000"+(bankCount+1);
		}
		else if((bankCount>=10) && (bankCount<100))
		{
			projectBankCode = "PB00"+(bankCount+1);
		}
		else if((bankCount>=100) && (bankCount<1000))
		{
			projectBankCode = "PB0"+(bankCount+1);
		}
		else if(bankCount>1000)
		{
			projectBankCode = "PB"+(bankCount+1);
		}

		return projectBankCode;
	}

	String detailsCode;
	private String ProjectDetailsCode()
	{
		long detailsCount = projectdetailsRepository.count();

		if(detailsCount<10)
		{
			detailsCode = "PD000"+(detailsCount+1);
		}
		else if((detailsCount>=10) && (detailsCount<100))
		{
			detailsCode = "PD00"+(detailsCount+1);
		}
		else if((detailsCount>=100) && (detailsCount<1000))
		{
			detailsCode = "PD0"+(detailsCount+1);
		}
		else
		{
			detailsCode = "PD"+(detailsCount+1);
		}

		return detailsCode;
	}

	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping("/ProjectDetailsMaster")
	public String ProjectDetailsMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<Project> projectList = GetUserAssigenedProjectList(req,res);
			List<ProjectDetails> projectdetailsList = projectdetailsRepository.findAll();

			List<ProjectDetails> shortedProjectDetailsList = new ArrayList<ProjectDetails>();
			ProjectDetails projectDetails=new ProjectDetails();

			for(int i=0;i<projectdetailsList.size();i++)
			{
				projectDetails=new ProjectDetails();
				for(int j=0;j<projectList.size();j++)
				{
					if(projectdetailsList.get(i).getProjectId().equals(projectList.get(j).getProjectId()))
					{
						projectDetails.setProjectDetailsId(projectdetailsList.get(i).getProjectDetailsId());
						projectDetails.setProjectId(projectList.get(j).getProjectName());
						projectDetails.setProjectType(projectdetailsList.get(i).getProjectType());

						projectDetails.setLocationInEast(projectdetailsList.get(i).getLocationInEast());
						projectDetails.setLocationInWest(projectdetailsList.get(i).getLocationInWest());
						projectDetails.setLocationInNorth(projectdetailsList.get(i).getLocationInNorth());
						projectDetails.setLocationInSouth(projectdetailsList.get(i).getLocationInSouth());
						shortedProjectDetailsList.add(projectDetails);
						break;
					}
				}


			}

			model.addAttribute("projectList",projectList);
			model.addAttribute("projectdetailsList",shortedProjectDetailsList);
			return "ProjectDetailsMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/AddProjectDetails")
	public String AddProjectDetails(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<Project> projectList = GetUserAssigenedProjectList(req,res);
			List<ProjectDetails> projectdetailsList = projectdetailsRepository.findAll();
			List<Project> shortedProjectList = new ArrayList<Project>();

			Project project=new Project();
			int flag=0;
			for(int i=0;i<projectList.size();i++)
			{
				project=new Project();
				flag =0;
				for(int j=0;j<projectdetailsList.size();j++)
				{
					if(projectList.get(i).getProjectId().equals(projectdetailsList.get(j).getProjectId()))
					{
						flag=1;
						break;
					}
				}
				if(flag==0)
				{

					project.setProjectId(projectList.get(i).getProjectId());
					project.setProjectName(projectList.get(i).getProjectName());
					shortedProjectList.add(project);
				}
			}
			model.addAttribute("projectDetailsId",ProjectDetailsCode());
			model.addAttribute("projectList",shortedProjectList);

			return "AddProjectDetails";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping(value="/AddProjectDetails",method=RequestMethod.POST)
	public String AddProjectDetails(@ModelAttribute ProjectDetails projectdetails, Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				projectdetails = new ProjectDetails(projectdetails.getProjectDetailsId(),projectdetails.getProjectId(),projectdetails.getLandownerName(),
						projectdetails.getProjectType(),projectdetails.getProjectLatitude(),projectdetails.getProjectLongitude(),
						projectdetails.getLocationInEast(),projectdetails.getLocationInWest(),projectdetails.getLocationInSouth(), projectdetails.getLocationInNorth(),
						projectdetails.getGoogleLocationInEast(),projectdetails.getGoogleLocationInWest(),projectdetails.getGoogleLocationInSouth(), projectdetails.getGoogleLocationInNorth(),
						projectdetails.getLandArea(),projectdetails.getLandPrice(),
						projectdetails.getCreationDate(),projectdetails.getUpdateDate(),projectdetails.getUserName());
				projectdetailsRepository.save(projectdetails);
				model.addAttribute("projectbuildingdbStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("projectbuildingdbStatus","Fail");
			}


			List<Project> projectList = GetUserAssigenedProjectList(req,res);
			List<ProjectDetails> projectdetailsList = projectdetailsRepository.findAll();

			List<ProjectDetails> shortedProjectDetailsList = new ArrayList<ProjectDetails>();
			ProjectDetails projectDetails=new ProjectDetails();

			for(int i=0;i<projectdetailsList.size();i++)
			{
				projectDetails=new ProjectDetails();
				for(int j=0;j<projectList.size();j++)
				{
					if(projectdetailsList.get(i).getProjectId().equals(projectList.get(j).getProjectId()))
					{
						projectDetails.setProjectDetailsId(projectdetailsList.get(i).getProjectDetailsId());
						projectDetails.setProjectId(projectList.get(j).getProjectName());
						projectDetails.setProjectType(projectdetailsList.get(i).getProjectType());

						projectDetails.setLocationInEast(projectdetailsList.get(i).getLocationInEast());
						projectDetails.setLocationInWest(projectdetailsList.get(i).getLocationInWest());
						projectDetails.setLocationInNorth(projectdetailsList.get(i).getLocationInNorth());
						projectDetails.setLocationInSouth(projectdetailsList.get(i).getLocationInSouth());
						shortedProjectDetailsList.add(projectDetails);
						break;
					}
				}


			}

			model.addAttribute("projectList",projectList);
			model.addAttribute("projectdetailsList",shortedProjectDetailsList);
			return "ProjectDetailsMaster";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping("/EditProjectDetails")
	public String EditProjectDetails(@RequestParam("projectDetailsId") String projectDetailsId, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			Query query = new Query();
			try
			{
				List<ProjectDetails> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectDetailsId").is(projectDetailsId)),ProjectDetails.class);
				query = new Query();
				List<Project> projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectDetails.get(0).getProjectId())), Project.class); 
				String projectName=projectList1.get(0).getProjectName();


				List<Project> projectList = GetUserAssigenedProjectList(req,res);
				List<ProjectDetails> projectdetailsList = projectdetailsRepository.findAll();
				List<Project> shortedProjectList = new ArrayList<Project>();

				Project project=new Project();
				int flag=0;
				for(int i=0;i<projectList.size();i++)
				{
					project=new Project();
					flag =0;
					for(int j=0;j<projectdetailsList.size();j++)
					{
						if(projectList.get(i).getProjectId().equals(projectdetailsList.get(j).getProjectId()))
						{
							flag=1;
							break;
						}
					}
					if(flag==0)
					{

						project.setProjectId(projectList.get(i).getProjectId());
						project.setProjectName(projectList.get(i).getProjectName());
						shortedProjectList.add(project);
					}
				}

				query = new Query();
				List<ProjectBankDetails> projectBankList = mongoTemplate.find(query.addCriteria(Criteria.where("projectDetailsId").is(projectDetailsId).and("status").is("Active")), ProjectBankDetails.class);

				model.addAttribute("projectList",shortedProjectList);
				model.addAttribute("projectName",projectName);
				model.addAttribute("projectDetails", projectDetails);
				model.addAttribute("projectBankList",projectBankList);
			}
			catch (Exception e) {
			}
			return "EditProjectDetails";

		}catch (Exception e) {
			return "login";
		}
	}



	@RequestMapping(value="/EditProjectDetails",method=RequestMethod.POST)
	public String EditProjectDetails(@ModelAttribute ProjectDetails projectdetails, Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				projectdetails = new ProjectDetails(projectdetails.getProjectDetailsId(),projectdetails.getProjectId(),projectdetails.getLandownerName(),
						projectdetails.getProjectType(),projectdetails.getProjectLatitude(),projectdetails.getProjectLongitude(),
						projectdetails.getLocationInEast(),projectdetails.getLocationInWest(),projectdetails.getLocationInSouth(), projectdetails.getLocationInNorth(),
						projectdetails.getGoogleLocationInEast(),projectdetails.getGoogleLocationInWest(),projectdetails.getGoogleLocationInSouth(), projectdetails.getGoogleLocationInNorth(),
						projectdetails.getLandArea(),projectdetails.getLandPrice(),
						projectdetails.getCreationDate(),projectdetails.getUpdateDate(),projectdetails.getUserName());
				projectdetailsRepository.save(projectdetails);
				model.addAttribute("projectbuildingdbStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("projectbuildingdbStatus","Fail");
			}


			List<Project> projectList = GetUserAssigenedProjectList(req,res);
			List<ProjectDetails> projectdetailsList = projectdetailsRepository.findAll();

			List<ProjectDetails> shortedProjectDetailsList = new ArrayList<ProjectDetails>();
			ProjectDetails projectDetails=new ProjectDetails();

			for(int i=0;i<projectdetailsList.size();i++)
			{
				projectDetails=new ProjectDetails();
				for(int j=0;j<projectList.size();j++)
				{
					if(projectdetailsList.get(i).getProjectId().equals(projectList.get(j).getProjectId()))
					{
						projectDetails.setProjectDetailsId(projectdetailsList.get(i).getProjectDetailsId());
						projectDetails.setProjectId(projectList.get(j).getProjectName());
						projectDetails.setProjectType(projectdetailsList.get(i).getProjectType());

						projectDetails.setLocationInEast(projectdetailsList.get(i).getLocationInEast());
						projectDetails.setLocationInWest(projectdetailsList.get(i).getLocationInWest());
						projectDetails.setLocationInNorth(projectdetailsList.get(i).getLocationInNorth());
						projectDetails.setLocationInSouth(projectdetailsList.get(i).getLocationInSouth());
						shortedProjectDetailsList.add(projectDetails);
						break;
					}
				}


			}

			model.addAttribute("projectList",projectList);
			model.addAttribute("projectdetailsList",shortedProjectDetailsList);
			return "ProjectDetailsMaster";

		}catch (Exception e) {
			return "login";
		}
	}



	@ResponseBody
	@RequestMapping(value="/AddProjectBankDetails",method=RequestMethod.POST)
	public List<ProjectBankDetails> AddProjectBankDetails(@RequestParam("projectDetailsId") String projectDetailsId, @RequestParam("projectId") String projectId, @RequestParam("accountHolderName") String accountHolderName,
			@RequestParam("bankName") String bankName,  @RequestParam("branchName") String branchName, 
			@RequestParam("ifscCode") String ifscCode,  @RequestParam("accountNumber") String accountNumber, 
			HttpSession session)
	{

		try {
			ProjectBankDetails projectbanks=new ProjectBankDetails();

			projectbanks.setProjectBankDetailsId(ProjectBankCode());
			projectbanks.setProjectDetailsId(projectDetailsId);
			projectbanks.setProjectId(projectId);
			projectbanks.setAccountHolderName(accountHolderName);
			projectbanks.setBankName(bankName);
			projectbanks.setBranchName(branchName);
			projectbanks.setIfscCode(ifscCode);
			projectbanks.setAccountNumber(accountNumber);
			projectbanks.setStatus("Active");

			projectbankdetailsRepository.save(projectbanks);
		}
		catch(Exception ee)
		{
			System.out.println(ee);
		}


		List<ProjectBankDetails> projectBankList= new ArrayList<ProjectBankDetails>();
		Query query = new Query();
		projectBankList = mongoTemplate.find(query.addCriteria(Criteria.where("projectDetailsId").is(projectDetailsId).and("status").is("Active")), ProjectBankDetails.class);

		return projectBankList;
	} 


	@ResponseBody
	@RequestMapping("/DeleteProjectBankDetails")
	public List<ProjectBankDetails> DeleteProjectBankDetails(@RequestParam("projectBankDetailsId") String projectBankDetailsId, @RequestParam("projectDetailsId") String projectDetailsId, HttpSession session)
	{
		try
		{
			ProjectBankDetails projectbanks=new ProjectBankDetails();
			projectbanks = mongoTemplate.findOne(Query.query(Criteria.where("projectBankDetailsId").is(projectBankDetailsId).and("projectDetailsId").is(projectDetailsId)), ProjectBankDetails.class);
			projectbanks.setStatus("Inactive");

			projectbankdetailsRepository.save(projectbanks);
			Query query = new Query();
			List<ProjectBankDetails> projectBankList = mongoTemplate.find(query.addCriteria(Criteria.where("projectDetailsId").is(projectDetailsId).and("status").is("Active")), ProjectBankDetails.class);
			return projectBankList;
		}
		catch(Exception e)
		{
			return null;
		}

	}
}

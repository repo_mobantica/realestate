package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Floor;
import com.realestate.bean.Login;
import com.realestate.bean.ParkingZone;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.ParkingZoneRepository;
import com.realestate.repository.ProjectRepository;

@Controller
@RequestMapping("/")
public class ParkingZoneController 
{
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	ParkingZoneRepository parkingZoneRepository;
	@Autowired
	ProjectRepository projectRepository;

	String parkingZoneCode = null;

	public String  GenerateParkingZoneCode()
	{
		long parkingZoneCount = parkingZoneRepository.count();

		if(parkingZoneCount<10)
		{
			parkingZoneCode = "PRK000"+(parkingZoneCount+1);
		}
		else if((parkingZoneCount<100) && (parkingZoneCount>=10))
		{
			parkingZoneCode = "PRK00"+(parkingZoneCount+1);
		}
		else if((parkingZoneCount<1000) && (parkingZoneCount>=100))
		{
			parkingZoneCode = "PRK0"+(parkingZoneCount+1);
		}
		else 
		{
			parkingZoneCode = "PRK"+(parkingZoneCount+1);
		}

		return parkingZoneCode;
	}

	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@ResponseBody
	@RequestMapping("/getParkingFloorList")
	public List<Floor> getParkingFloorList(@RequestParam("projectId") String projectId, @RequestParam("buildingId") String buildingId, @RequestParam("wingId") String wingId)
	{
		Query query = new Query();
		List<Floor> parkingFloorList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("floorzoneType").is("Parking")), Floor.class);

		return parkingFloorList;
	}

	@ResponseBody
	@RequestMapping("/getOpenParkingsList")
	public List<Floor> getOpenParkingsList(@RequestParam("projectId") String projectId, @RequestParam("buildingId") String buildingId, @RequestParam("wingId") String wingId, @RequestParam("floorId") String floorId)
	{
		Query query = new Query();
		List<Floor> openParkingsList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("floorId").is(floorId)), Floor.class);

		return openParkingsList;
	}

	@ResponseBody
	@RequestMapping("/getCloseParkingsList")
	public List<Floor> getCloseParkingsList(@RequestParam("projectId") String projectId, @RequestParam("buildingId") String buildingId, @RequestParam("wingId") String wingId, @RequestParam("floorId") String floorId)
	{
		Query query = new Query();
		List<Floor> openParkingsList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("floorId").is(floorId)), Floor.class);

		return openParkingsList;
	}

	@ResponseBody
	@RequestMapping("/checkParkingNumberDetailStatus")
	public List<ParkingZone> checkParkingNumberDetailStatus(@RequestParam("projectId") String projectId, @RequestParam("buildingId") String buildingId, @RequestParam("wingId") String wingId, @RequestParam("floorId") String floorId, @RequestParam("parkingNumber") String parkingNumber)
	{
		Query query = new Query();
		List<ParkingZone> parkingList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("floorId").is(floorId).and("parkingNumber").is(parkingNumber)), ParkingZone.class);

		return parkingList;
	}

	@ResponseBody
	@RequestMapping("/getWingWiseFloorNameList")
	public List<Floor> GetWingWiseFloorNameList(@RequestParam("projectId") String projectId, @RequestParam("buildingId") String buildingId, @RequestParam("wingId") String wingId)
	{
		Query query = new Query();
		List<Floor> floorList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("floorzoneType").is("Parking")), Floor.class);
		return floorList;
	}

	@ResponseBody
	@RequestMapping("/getProjectWiseParkingZoneList")
	public List<ParkingZone> GetProjectWiseParkingZoneList(@RequestParam("projectId") String projectId)
	{
		Query query = new Query();
		List<ParkingZone> parkingZoneList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)), ParkingZone.class);

		for(int i=0;i<parkingZoneList.size();i++)
		{
			try
			{
				query = new Query();
				List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(parkingZoneList.get(i).getProjectId())), Project.class);
				parkingZoneList.get(i).setProjectId(projectDetails.get(0).getProjectName());
				query = new Query();
				List<ProjectBuilding> BuildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(parkingZoneList.get(i).getBuildingId())), ProjectBuilding.class);
				parkingZoneList.get(i).setBuildingId(BuildingDetails.get(0).getBuildingName());
				query = new Query();
				List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(parkingZoneList.get(i).getWingId())), ProjectWing.class);
				parkingZoneList.get(i).setWingId(wingDetails.get(0).getWingName());
				query = new Query();
				List<Floor> floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("floorId").is(parkingZoneList.get(i).getFloorId())), Floor.class);
				parkingZoneList.get(i).setFloorId(floorDetails.get(0).getFloortypeName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}
		return parkingZoneList;
	}

	@ResponseBody
	@RequestMapping("/getBuildingWiseParkingZoneList")
	public List<ParkingZone> GetBuildingWiseParkingZoneList(@RequestParam("projectId") String projectId, @RequestParam("buildingId") String buildingId)
	{
		Query query = new Query();
		List<ParkingZone> parkingZoneList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId)), ParkingZone.class);

		for(int i=0;i<parkingZoneList.size();i++)
		{
			try
			{
				query = new Query();
				List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(parkingZoneList.get(i).getProjectId())), Project.class);
				parkingZoneList.get(i).setProjectId(projectDetails.get(0).getProjectName());
				query = new Query();
				List<ProjectBuilding> BuildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(parkingZoneList.get(i).getBuildingId())), ProjectBuilding.class);
				parkingZoneList.get(i).setBuildingId(BuildingDetails.get(0).getBuildingName());
				query = new Query();
				List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(parkingZoneList.get(i).getWingId())), ProjectWing.class);
				parkingZoneList.get(i).setWingId(wingDetails.get(0).getWingName());
				query = new Query();
				List<Floor> floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("floorId").is(parkingZoneList.get(i).getFloorId())), Floor.class);
				parkingZoneList.get(i).setFloorId(floorDetails.get(0).getFloortypeName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}
		return parkingZoneList;
	}

	@ResponseBody
	@RequestMapping("/getWingWiseParkingZoneList")
	public List<ParkingZone> GetWingWiseParkingZoneList(@RequestParam("projectId") String projectId, @RequestParam("buildingId") String buildingId, @RequestParam("wingId") String wingId)
	{
		Query query = new Query();
		List<ParkingZone> parkingZoneList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId)), ParkingZone.class);

		for(int i=0;i<parkingZoneList.size();i++)
		{
			try
			{
				query = new Query();
				List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(parkingZoneList.get(i).getProjectId())), Project.class);
				parkingZoneList.get(i).setProjectId(projectDetails.get(0).getProjectName());
				query = new Query();
				List<ProjectBuilding> BuildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(parkingZoneList.get(i).getBuildingId())), ProjectBuilding.class);
				parkingZoneList.get(i).setBuildingId(BuildingDetails.get(0).getBuildingName());
				query = new Query();
				List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(parkingZoneList.get(i).getWingId())), ProjectWing.class);
				parkingZoneList.get(i).setWingId(wingDetails.get(0).getWingName());
				query = new Query();
				List<Floor> floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("floorId").is(parkingZoneList.get(i).getFloorId())), Floor.class);
				parkingZoneList.get(i).setFloorId(floorDetails.get(0).getFloortypeName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}
		return parkingZoneList;
	}

	@ResponseBody
	@RequestMapping("/getFloorWiseParkingZoneList")
	public List<ParkingZone> GetFloorWiseParkingZoneList(@RequestParam("projectId") String projectId, @RequestParam("buildingId") String buildingId, @RequestParam("wingId") String wingId, @RequestParam("floorId") String floorId)
	{
		Query query = new Query();
		List<ParkingZone> parkingZoneList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("floorId").is(floorId)), ParkingZone.class);

		for(int i=0;i<parkingZoneList.size();i++)
		{
			try
			{
				query = new Query();
				List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(parkingZoneList.get(i).getProjectId())), Project.class);
				parkingZoneList.get(i).setProjectId(projectDetails.get(0).getProjectName());
				query = new Query();
				List<ProjectBuilding> BuildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(parkingZoneList.get(i).getBuildingId())), ProjectBuilding.class);
				parkingZoneList.get(i).setBuildingId(BuildingDetails.get(0).getBuildingName());
				query = new Query();
				List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(parkingZoneList.get(i).getWingId())), ProjectWing.class);
				parkingZoneList.get(i).setWingId(wingDetails.get(0).getWingName());
				query = new Query();
				List<Floor> floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("floorId").is(parkingZoneList.get(i).getFloorId())), Floor.class);
				parkingZoneList.get(i).setFloorId(floorDetails.get(0).getFloortypeName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}
		return parkingZoneList;
	}

	@RequestMapping("/ParkingZoneMaster")
	public String ParkingZoneMaster(Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<ParkingZone> parkingZoneList = parkingZoneRepository.findAll();
			List<Project> projectList =  GetUserAssigenedProjectList(req,res);
			Query query = new Query();

			for(int i=0;i<parkingZoneList.size();i++)
			{
				try
				{
					query = new Query();
					List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(parkingZoneList.get(i).getProjectId())), Project.class);
					parkingZoneList.get(i).setProjectId(projectDetails.get(0).getProjectName());
					query = new Query();
					List<ProjectBuilding> BuildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(parkingZoneList.get(i).getBuildingId())), ProjectBuilding.class);
					parkingZoneList.get(i).setBuildingId(BuildingDetails.get(0).getBuildingName());
					query = new Query();
					List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(parkingZoneList.get(i).getWingId())), ProjectWing.class);
					parkingZoneList.get(i).setWingId(wingDetails.get(0).getWingName());
					query = new Query();
					List<Floor> floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("floorId").is(parkingZoneList.get(i).getFloorId())), Floor.class);
					parkingZoneList.get(i).setFloorId(floorDetails.get(0).getFloortypeName());
				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}

			model.addAttribute("parkingZoneList", parkingZoneList);
			model.addAttribute("projectList",projectList);
			return "ParkingZoneMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/AddParkingZoneDetails")
	public String AddParkingZoneDetailsGetMethod(Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<Project> projectList = GetUserAssigenedProjectList(req,res);

			model.addAttribute("parkingZoneId",GenerateParkingZoneCode());
			model.addAttribute("projectList", projectList);
			return "AddParkingZoneDetails";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/AddParkingZoneDetails", method=RequestMethod.POST)
	public String AddParkingZoneDetailsPostMethod(@ModelAttribute ParkingZone parkingZone,Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			String parkingZoneStatus = null; 
			try
			{
				parkingZone.setParkingZoneStatus("Not Assigned");
				parkingZone = new ParkingZone(parkingZone.getParkingZoneId(), parkingZone.getProjectId(), parkingZone.getBuildingId(), parkingZone.getWingId(), parkingZone.getFloorId(), parkingZone.getParkingType(), parkingZone.getParkingNumber(), parkingZone.getParkingLength(), parkingZone.getParkingWidth(), parkingZone.getAreaInSqMtr(), parkingZone.getAreaInSqFt(), parkingZone.getParkingZoneStatus(), parkingZone.getCreationDate(), parkingZone.getUpdateDate(), parkingZone.getUserName());
				parkingZoneRepository .save(parkingZone);
				parkingZoneStatus = "Success";
			}
			catch(Exception e)
			{
				e.printStackTrace();
				parkingZoneStatus = "Fail";
			}

			List<Project> projectList = GetUserAssigenedProjectList(req,res);

			model.addAttribute("parkingZoneId",GenerateParkingZoneCode());
			model.addAttribute("projectList", projectList);
			model.addAttribute("parkingZoneStatus", parkingZoneStatus);

			return "AddParkingZoneDetails";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/EditParkingZoneDetails")
	public String EditParkingZoneDetailsGetMethod(@RequestParam("parkingZoneId") String parkingZoneId,Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			Query query = new Query();
			ParkingZone parkingZoneDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("parkingZoneId").is(parkingZoneId)), ParkingZone.class);

			query = new Query();
			Project projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(parkingZoneDetails.getProjectId())), Project.class);

			query = new Query();
			ProjectBuilding buildingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(parkingZoneDetails.getBuildingId())), ProjectBuilding.class);

			query = new Query();
			ProjectWing wingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(parkingZoneDetails.getWingId())), ProjectWing.class);

			query = new Query();
			Floor floorDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("floorId").is(parkingZoneDetails.getFloorId())), Floor.class);

			model.addAttribute("projectName", projectDetails.getProjectName());
			model.addAttribute("buildingName", buildingDetails.getBuildingName());
			model.addAttribute("wingName", wingDetails.getWingName());
			model.addAttribute("floortypeName", floorDetails.getFloortypeName());

			List<Project> projectList = GetUserAssigenedProjectList(req,res);

			model.addAttribute("projectList",projectList);
			model.addAttribute("parkingZoneDetails", parkingZoneDetails);

			return "EditParkingZoneDetails";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/EditParkingZoneDetails", method=RequestMethod.POST)
	public String EditParkingZoneDetailsPostMethod(@ModelAttribute ParkingZone parkingZone,Model model)
	{
		try {
			String parkingZoneStatus = null; 
			try
			{
				parkingZone.setParkingZoneStatus("Not Assigned");
				parkingZone = new ParkingZone(parkingZone.getParkingZoneId(), parkingZone.getProjectId(), parkingZone.getBuildingId(), parkingZone.getWingId(), parkingZone.getFloorId(), parkingZone.getParkingType(), parkingZone.getParkingNumber(), parkingZone.getParkingLength(), parkingZone.getParkingWidth(), parkingZone.getAreaInSqMtr(), parkingZone.getAreaInSqFt(), parkingZone.getParkingZoneStatus(), parkingZone.getCreationDate(), parkingZone.getUpdateDate(), parkingZone.getUserName());
				parkingZoneRepository .save(parkingZone);
				parkingZoneStatus = "Success";
			}
			catch(Exception e)
			{
				e.printStackTrace();
				parkingZoneStatus = "Fail";
			}

			List<ParkingZone> parkingZoneList = parkingZoneRepository.findAll();

			model.addAttribute("parkingZoneList", parkingZoneList);
			model.addAttribute("parkingZoneStatus", parkingZoneStatus);

			return "ParkingZoneMaster";

		}catch (Exception e) {
			return "login";
		}
	}

}

package com.realestate.controller;

import java.time.LocalDate;
import java.time.ZoneId;

import static java.time.DayOfWeek.SUNDAY;
import static java.time.DayOfWeek.MONDAY;
import static java.time.DayOfWeek.TUESDAY;
import static java.time.DayOfWeek.WEDNESDAY;
import static java.time.DayOfWeek.THURSDAY;
import static java.time.DayOfWeek.FRIDAY;
import static java.time.DayOfWeek.SATURDAY;

import static java.time.temporal.TemporalAdjusters.nextOrSame;
import static java.time.temporal.TemporalAdjusters.previousOrSame;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.LabourAttendance;
import com.realestate.bean.LabourWeekAttendance;
import com.realestate.bean.Login;
import com.realestate.bean.Project;
import com.realestate.bean.UserAssignedProject;
import com.realestate.configuration.CommanController;
import com.realestate.bean.Agent;
import com.realestate.bean.AgentEmployees;
import com.realestate.bean.Aggreement;
import com.realestate.bean.Booking;
import com.realestate.bean.Company;
import com.realestate.bean.Country;
import com.realestate.bean.Labour;

import com.realestate.repository.CountryRepository;
import com.realestate.repository.LabourRepository;
import com.realestate.repository.ProjectRepository;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;

import com.realestate.repository.LabourAttendanceRepository;

@Controller
@RequestMapping("/")
public class LabourAttendanceController
{
	@Autowired
	LabourRepository labourRepository;
	@Autowired
	CountryRepository countryRepository;
	@Autowired
	LabourAttendanceRepository labourattendanceRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}


	@RequestMapping("/LabourAttendance")
	public String LabourAttendance(Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<Project> projectList = GetUserAssigenedProjectList(req,res); 

			model.addAttribute("projectList",projectList);
			return "LabourAttendance";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/AddLabourAttendance")
	public String AddLabourAttendance(@RequestParam("projectId") String projectId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<Labour> labourList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)),Labour.class);


			List<LabourAttendance> labourattendanceList = labourattendanceRepository.findAll();


			long labourAttendanceId=0;
			if(labourattendanceList.size()!=0)
			{
				for(int i=0;i<labourattendanceList.size();i++)
				{
					if(labourattendanceList.get(i).getLabourAttendanceId()>labourAttendanceId)
					{
						labourAttendanceId=labourattendanceList.get(i).getLabourAttendanceId();
					}
				}
			}
			//labourAttendanceId=labourAttendanceId+1;
			model.addAttribute("labourAttendanceId",labourAttendanceId);
			model.addAttribute("labourList",labourList);
			return "AddLabourAttendance";

		}catch (Exception e) {
			return "login";
		}
	}



	@ResponseBody
	@RequestMapping("/AddAttendanceForLabour")
	public List<LabourAttendance> AddAttendanceForLabour(@RequestParam("labourAttendanceId") long labourAttendanceId, @RequestParam("labourId") String labourId, @RequestParam("todayWork") String todayWork, @RequestParam("projectId") String projectId,  @RequestParam("todaySalary") Double todaySalary, @RequestParam("attendanceDate") String attendanceDate, @RequestParam("presentStatus") String presentStatus, @RequestParam("creationDate") String creationDate, @RequestParam("updateDate") String updateDate, @RequestParam("userName") String userName, HttpSession session) throws ParseException
	{
		// String sDate1="31/12/1998";  
		Date attendanceDate1=new SimpleDateFormat("d/M/yyyy").parse(attendanceDate);  

		try
		{
			Query query = new Query();
			List<LabourAttendance> labourAttendanceList = mongoTemplate.find(query.addCriteria(Criteria.where("labourId").is(labourId).and("attendanceDate").is(attendanceDate1)), LabourAttendance.class); 
			if(labourAttendanceList.size()==0)
			{
				LabourAttendance labourattendance = new LabourAttendance();
				labourattendance.setLabourAttendanceId(labourAttendanceId);
				labourattendance.setLabourId(labourId);
				labourattendance.setTodayWork(todayWork);
				labourattendance.setProjectId(projectId);
				labourattendance.setPresentStatus(presentStatus);
				labourattendance.setAttendanceDate(attendanceDate1);
				labourattendance.setTodaySalary(todaySalary);
				labourattendance.setCreationDate(creationDate);
				labourattendance.setUpdateDate(updateDate);
				labourattendance.setUserName(userName);
				labourattendanceRepository.save(labourattendance);
			}
		}
		catch(Exception e)
		{
		}

		return null;
	}	


	@RequestMapping(value = "/AddLabourAttendance", method = RequestMethod.POST)
	public String AddLabourAttendance(@ModelAttribute LabourAttendance labourattendance, Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {

			List<Project> projectList = GetUserAssigenedProjectList(req,res); 
			model.addAttribute("projectList",projectList);
			return "LabourAttendance";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping("/LabourAttendanceWeekReport")
	public String LabourAttendanceWeekReport(@RequestParam("projectId") String projectId, ModelMap model)
	{
		try {
			LocalDate today = LocalDate.now();

			LocalDate sunday = today.with(previousOrSame(SUNDAY));
			/*  LocalDate monday = today.with(previousOrSame(MONDAY));
			    LocalDate tuesday = today.with(previousOrSame(TUESDAY));
			    LocalDate wednesday = today.with(previousOrSame(WEDNESDAY));
			    LocalDate thursday = today.with(previousOrSame(THURSDAY));
			    LocalDate friday = today.with(previousOrSame(FRIDAY));
			    LocalDate saturday = today.with(previousOrSame(SATURDAY));
			 */
			final Date date = Date.from(sunday.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
			final Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);

			calendar.add(Calendar.DAY_OF_YEAR, 1);
			Date monday = calendar.getTime();
			LocalDate monday1 = monday.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

			calendar.add(Calendar.DAY_OF_YEAR, 1);
			Date tuesday = calendar.getTime();
			LocalDate tuesday1 = tuesday.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

			calendar.add(Calendar.DAY_OF_YEAR, 1);
			Date wednesday = calendar.getTime();
			LocalDate wednesday1 = wednesday.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

			calendar.add(Calendar.DAY_OF_YEAR, 1);
			Date thursday = calendar.getTime();
			LocalDate thursday1 = thursday.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

			calendar.add(Calendar.DAY_OF_YEAR, 1);
			Date friday = calendar.getTime();
			LocalDate friday1 = friday.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

			calendar.add(Calendar.DAY_OF_YEAR, 1);
			Date saturday =calendar.getTime();
			LocalDate saturday1 = saturday.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();


			Query query = new Query();
			List<Labour> labourList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)),Labour.class);

			query = new Query();
			List<Project> projectList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)),Project.class);

			model.addAttribute("sunday",sunday);
			model.addAttribute("monday",monday1);
			model.addAttribute("tuesday",tuesday1);
			model.addAttribute("wednesday",wednesday1);
			model.addAttribute("thursday",thursday1);
			model.addAttribute("friday",friday1);
			model.addAttribute("saturday",saturday1);

			LabourWeekAttendance labourweekattendance=new LabourWeekAttendance();
			List<LabourWeekAttendance> LabourWeekAttendanceList=new ArrayList<LabourWeekAttendance>();

			String labourName="";
			Double sundaySalary=0.0;
			Double mondaySalary=0.0;
			Double tuesdaySalary=0.0;
			Double wednesdaySalary=0.0;
			Double thursdaySalary=0.0;
			Double fridaySalary=0.0;
			Double saturdaySalary=0.0;
			Double totalWeekSalary=0.0;

			for(int i=0;i<labourList.size();i++)
			{
				sundaySalary=0.0;
				mondaySalary=0.0;
				tuesdaySalary=0.0;
				wednesdaySalary=0.0;
				thursdaySalary=0.0;
				fridaySalary=0.0;
				saturdaySalary=0.0;
				totalWeekSalary=0.0;

				labourweekattendance=new LabourWeekAttendance();

				labourweekattendance.setLabourId(labourList.get(i).getLabourId());
				labourName=labourList.get(i).getLabourfirstName()+" "+labourList.get(i).getLabourmiddleName()+" "+labourList.get(i).getLabourlastName();
				labourweekattendance.setLabourName(labourName);

				//Sunday
				query = new Query();
				List<LabourAttendance> labourattendanceSunday = mongoTemplate.find(query.addCriteria(Criteria.where("attendanceDate").is(sunday).and("labourId").is(labourList.get(i).getLabourId())),LabourAttendance.class);
				if(labourattendanceSunday.size()!=0)
				{
					sundaySalary=labourattendanceSunday.get(0).getTodaySalary();
				}

				labourweekattendance.setSunday(sundaySalary);

				//Monday
				query = new Query();
				List<LabourAttendance> labourattendanceMonday = mongoTemplate.find(query.addCriteria(Criteria.where("attendanceDate").is(monday).and("labourId").is(labourList.get(i).getLabourId())),LabourAttendance.class);
				if(labourattendanceMonday.size()!=0)
				{
					mondaySalary=labourattendanceMonday.get(0).getTodaySalary();
				}

				labourweekattendance.setMonday(mondaySalary);

				//Tuesday
				query = new Query();
				List<LabourAttendance> labourattendanceTuesday = mongoTemplate.find(query.addCriteria(Criteria.where("attendanceDate").is(tuesday).and("labourId").is(labourList.get(i).getLabourId())),LabourAttendance.class);
				if(labourattendanceTuesday.size()!=0)
				{
					tuesdaySalary=labourattendanceTuesday.get(0).getTodaySalary();
				}

				labourweekattendance.setTuesday(tuesdaySalary);

				//Wednesday
				query = new Query();
				List<LabourAttendance> labourattendanceWednesday = mongoTemplate.find(query.addCriteria(Criteria.where("attendanceDate").is(wednesday).and("labourId").is(labourList.get(i).getLabourId())),LabourAttendance.class);
				if(labourattendanceWednesday.size()!=0)
				{
					wednesdaySalary=labourattendanceWednesday.get(0).getTodaySalary();
				}

				labourweekattendance.setWednesday(wednesdaySalary);

				//Thursday
				query = new Query();
				List<LabourAttendance> labourattendanceThursday = mongoTemplate.find(query.addCriteria(Criteria.where("attendanceDate").is(thursday).and("labourId").is(labourList.get(i).getLabourId())),LabourAttendance.class);
				if(labourattendanceThursday.size()!=0)
				{
					thursdaySalary=labourattendanceThursday.get(0).getTodaySalary();
				}

				labourweekattendance.setThursday(thursdaySalary);

				//Friday
				query = new Query();
				List<LabourAttendance> labourattendanceFriday = mongoTemplate.find(query.addCriteria(Criteria.where("attendanceDate").is(friday).and("labourId").is(labourList.get(i).getLabourId())),LabourAttendance.class);
				if(labourattendanceFriday.size()!=0)
				{
					fridaySalary=labourattendanceFriday.get(0).getTodaySalary();
				}

				labourweekattendance.setFriday(fridaySalary);

				//Saturday
				query = new Query();
				List<LabourAttendance> labourattendanceSaturday = mongoTemplate.find(query.addCriteria(Criteria.where("attendanceDate").is(saturday).and("labourId").is(labourList.get(i).getLabourId())),LabourAttendance.class);
				if(labourattendanceSaturday.size()!=0)
				{
					saturdaySalary=labourattendanceSaturday.get(0).getTodaySalary();
				}

				labourweekattendance.setSaturday(saturdaySalary);

				//Total Week Salary

				totalWeekSalary= sundaySalary+mondaySalary+tuesdaySalary+wednesdaySalary+thursdaySalary+fridaySalary+saturdaySalary;
				labourweekattendance.setTotalThisWeek(totalWeekSalary);

				LabourWeekAttendanceList.add(labourweekattendance);

			}

			model.addAttribute("projectName",projectList.get(0).getProjectName());
			model.addAttribute("LabourWeekAttendanceList",LabourWeekAttendanceList);
			model.addAttribute("projectId",projectId);

			return "LabourAttendanceWeekReport";

		}catch (Exception e) {
			return "login";
		}
	}


	@ResponseBody
	@RequestMapping("/PrintWeeklyLabourAttendanceReport")
	public ResponseEntity<byte[]> PrintWeeklyLabourAttendanceReport(@RequestParam("projectId") String projectId, HttpServletRequest req, ModelMap model, HttpServletResponse response)
	{
		try
		{
			LocalDate today = LocalDate.now();

			LocalDate sunday = today.with(previousOrSame(SUNDAY));

			final Date date = Date.from(sunday.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
			final Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);

			calendar.add(Calendar.DAY_OF_YEAR, 1);
			Date monday = calendar.getTime();
			LocalDate monday1 = monday.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

			calendar.add(Calendar.DAY_OF_YEAR, 1);
			Date tuesday = calendar.getTime();
			LocalDate tuesday1 = tuesday.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

			calendar.add(Calendar.DAY_OF_YEAR, 1);
			Date wednesday = calendar.getTime();
			LocalDate wednesday1 = wednesday.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

			calendar.add(Calendar.DAY_OF_YEAR, 1);
			Date thursday = calendar.getTime();
			LocalDate thursday1 = thursday.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

			calendar.add(Calendar.DAY_OF_YEAR, 1);
			Date friday = calendar.getTime();
			LocalDate friday1 = friday.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

			calendar.add(Calendar.DAY_OF_YEAR, 1);
			Date saturday =calendar.getTime();
			LocalDate saturday1 = saturday.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();


			Query query = new Query();
			List<Labour> labourList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)),Labour.class);

			query = new Query();
			List<Project> projectList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)),Project.class);


			String labourName="";
			Double sundaySalary=0.0, mondaySalary=0.0, tuesdaySalary=0.0, wednesdaySalary=0.0, thursdaySalary=0.0, fridaySalary=0.0, saturdaySalary=0.0;
			Double totalWeekSalary=0.0, grandTotal=0.0;
			Double sunTotal=0.0, monTotal=0.0, tueTotal=0.0, wedTotal=0.0, thuTotal=0.0, friTotal=0.0, satTotal=0.0;

			LabourWeekAttendance labourweekattendance=new LabourWeekAttendance();

			JasperPrint print;

			HashMap jmap = new HashMap();
			Collection c = new ArrayList();

			for(int i=0;i<labourList.size();i++)
			{
				jmap = new HashMap();

				jmap.put("srno",""+(i+1));

				sundaySalary=0.0;
				mondaySalary=0.0;
				tuesdaySalary=0.0;
				wednesdaySalary=0.0;
				thursdaySalary=0.0;
				fridaySalary=0.0;
				saturdaySalary=0.0;
				totalWeekSalary=0.0;

				labourweekattendance=new LabourWeekAttendance();

				labourweekattendance.setLabourId(labourList.get(i).getLabourId());
				labourName=labourList.get(i).getLabourfirstName()+" "+labourList.get(i).getLabourmiddleName()+" "+labourList.get(i).getLabourlastName();

				jmap.put("labourName",""+labourName);
				//labourweekattendance.setLabourName(labourName);

				//Sunday
				query = new Query();
				List<LabourAttendance> labourattendanceSunday = mongoTemplate.find(query.addCriteria(Criteria.where("attendanceDate").is(sunday).and("labourId").is(labourList.get(i).getLabourId())),LabourAttendance.class);
				if(labourattendanceSunday.size()!=0)
				{
					sundaySalary=labourattendanceSunday.get(0).getTodaySalary();
					sunTotal+= sundaySalary;
					jmap.put("day1",""+sundaySalary);
				}
				else
				{
					jmap.put("day1","0.0");
				}

				labourweekattendance.setSunday(sundaySalary);

				//Monday
				query = new Query();
				List<LabourAttendance> labourattendanceMonday = mongoTemplate.find(query.addCriteria(Criteria.where("attendanceDate").is(monday).and("labourId").is(labourList.get(i).getLabourId())),LabourAttendance.class);
				if(labourattendanceMonday.size()!=0)
				{
					mondaySalary=labourattendanceMonday.get(0).getTodaySalary();
					monTotal+= mondaySalary;
					jmap.put("day2",""+mondaySalary);
				}
				else
				{
					jmap.put("day2","0.0");
				}

				labourweekattendance.setMonday(mondaySalary);

				//Tuesday
				query = new Query();
				List<LabourAttendance> labourattendanceTuesday = mongoTemplate.find(query.addCriteria(Criteria.where("attendanceDate").is(tuesday).and("labourId").is(labourList.get(i).getLabourId())),LabourAttendance.class);
				if(labourattendanceTuesday.size()!=0)
				{
					tuesdaySalary=labourattendanceTuesday.get(0).getTodaySalary();
					tueTotal+=tuesdaySalary;
					jmap.put("day3",""+tuesdaySalary);
				}
				else
				{
					jmap.put("day3","0.0");
				}

				labourweekattendance.setTuesday(tuesdaySalary);

				//Wednesday
				query = new Query();
				List<LabourAttendance> labourattendanceWednesday = mongoTemplate.find(query.addCriteria(Criteria.where("attendanceDate").is(wednesday).and("labourId").is(labourList.get(i).getLabourId())),LabourAttendance.class);
				if(labourattendanceWednesday.size()!=0)
				{
					wednesdaySalary=labourattendanceWednesday.get(0).getTodaySalary();
					wedTotal+=wednesdaySalary;
					jmap.put("day4",""+wednesdaySalary);
				}
				else
				{
					jmap.put("day4","0.0");
				}

				labourweekattendance.setWednesday(wednesdaySalary);

				//Thursday
				query = new Query();
				List<LabourAttendance> labourattendanceThursday = mongoTemplate.find(query.addCriteria(Criteria.where("attendanceDate").is(thursday).and("labourId").is(labourList.get(i).getLabourId())),LabourAttendance.class);
				if(labourattendanceThursday.size()!=0)
				{
					thursdaySalary=labourattendanceThursday.get(0).getTodaySalary();
					thuTotal+=thursdaySalary;
					jmap.put("day5",""+thursdaySalary);
				}
				else
				{
					jmap.put("day5","0.0");
				}

				labourweekattendance.setThursday(thursdaySalary);

				//Friday
				query = new Query();
				List<LabourAttendance> labourattendanceFriday = mongoTemplate.find(query.addCriteria(Criteria.where("attendanceDate").is(friday).and("labourId").is(labourList.get(i).getLabourId())),LabourAttendance.class);
				if(labourattendanceFriday.size()!=0)
				{
					fridaySalary=labourattendanceFriday.get(0).getTodaySalary();
					friTotal+=fridaySalary;
					jmap.put("day6",""+fridaySalary);
				}
				else
				{
					jmap.put("day6","0.0");
				}

				labourweekattendance.setFriday(fridaySalary);

				//Saturday
				query = new Query();
				List<LabourAttendance> labourattendanceSaturday = mongoTemplate.find(query.addCriteria(Criteria.where("attendanceDate").is(saturday).and("labourId").is(labourList.get(i).getLabourId())),LabourAttendance.class);
				if(labourattendanceSaturday.size()!=0)
				{
					saturdaySalary=labourattendanceSaturday.get(0).getTodaySalary();
					satTotal+=saturdaySalary;
					jmap.put("day7",""+saturdaySalary);
				}
				else
				{
					jmap.put("day7","0.0");
				}

				labourweekattendance.setSaturday(saturdaySalary);

				//Total Week Salary
				totalWeekSalary= sundaySalary+mondaySalary+tuesdaySalary+wednesdaySalary+thursdaySalary+fridaySalary+saturdaySalary;

				grandTotal += totalWeekSalary;

				jmap.put("labourSubTotal",""+totalWeekSalary);

				c.add(jmap);
				jmap = null;
			}

			String realPath =CommanController.GetLogoImagePath();
			//String realPath ="//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img";

			JRDataSource dataSource = new JRMapCollectionDataSource(c);
			Map<String, Object> parameterMap = new HashMap<String, Object>();


			Date date1 = new Date();  
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
			String strDate= formatter.format(date1);

			//Booking Receipt Generation Code
			/*1*/  parameterMap.put("realPath", ""+realPath);
			/*2*/  parameterMap.put("printDate",""+strDate);
			/*3*/  parameterMap.put("reportWeek"," From "+sunday+" To "+saturday1);
			/*4*/  parameterMap.put("projectName", ""+projectList.get(0).getProjectName());
			/*5*/  parameterMap.put("day1",""+sunday);
			/*6*/  parameterMap.put("day2",""+monday1);
			/*7*/  parameterMap.put("day3",""+tuesday1);
			/*8*/  parameterMap.put("day4",""+wednesday1);
			/*9*/  parameterMap.put("day5",""+thursday1);
			/*10*/ parameterMap.put("day6",""+friday1);
			/*11*/ parameterMap.put("day7",""+saturday1);
			/*12*/ parameterMap.put("grandTotal",""+grandTotal);
			/*13*/ parameterMap.put("day1Total",""+sunTotal);
			/*14*/ parameterMap.put("day2Total",""+monTotal);
			/*15*/ parameterMap.put("day3Total",""+tueTotal);
			/*16*/ parameterMap.put("day4Total",""+wedTotal);
			/*17*/ parameterMap.put("day5Total",""+thuTotal);
			/*18*/ parameterMap.put("day6Total",""+friTotal);
			/*19*/ parameterMap.put("day7Total",""+satTotal);


			/**/ //parameterMap.put("realPath",realPath);

			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/weeklyLabourAttendanceReport.jasper");

			print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(print, baos);

			byte[] contents = baos.toByteArray();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			String filename = "WeeklyLabourAttendanceReport.pdf";

			JasperExportManager.exportReportToPdfStream(print, baos);

			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
			ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
			response.setHeader("Content-Disposition", "inline; filename=" + filename );
			return resp;
		}
		catch(Exception e)
		{
			System.out.println("Jasper Exception = "+e.toString());
			return null;
		}
	}
}
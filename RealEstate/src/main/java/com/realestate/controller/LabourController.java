package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.realestate.bean.Agent;
import com.realestate.bean.AgentEmployees;
import com.realestate.bean.City;
import com.realestate.bean.Country;
import com.realestate.bean.Designation;
import com.realestate.bean.Labour;
import com.realestate.bean.LocationArea;
import com.realestate.bean.Login;
import com.realestate.bean.Project;
import com.realestate.bean.State;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.LabourRepository;
import com.realestate.repository.LocationAreaRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.StateRepository;
import com.realestate.repository.CityRepository;
import com.realestate.repository.CountryRepository;


@Controller
@RequestMapping("/")
public class LabourController {

	@Autowired
	LocationAreaRepository locationAreaRepository;
	@Autowired
	CityRepository cityRepository;
	@Autowired
	StateRepository stateRepository;
	@Autowired
	LabourRepository labourRepository;
	@Autowired
	CountryRepository countryRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	MongoTemplate mongoTemplate;


	String labourCode;
	private String LabourCode()
	{
		long cnt  = labourRepository.count();
		if(cnt<10)
		{
			labourCode = "LB000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			labourCode = "LB00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			labourCode = "LB0"+(cnt+1);
		}
		else
		{
			labourCode = "LB"+(cnt+1);
		}
		return labourCode;
	}

	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping("/LabourMaster")
	public String LabourMaster(Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<Labour> labourList = labourRepository.findAll(); 

			List<LocationArea> locationareaList= locationAreaRepository.findAll();
			List<City> cityList = cityRepository.findAll();
			List<State> stateList= stateRepository.findAll();
			List<Country> countryList= countryRepository.findAll();


			int i,j;
			for(i=0;i<labourList.size();i++)
			{
				try 
				{
					for(j=0;j<countryList.size();j++)
					{
						if(labourList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
						{
							labourList.get(i).setCountryId(countryList.get(j).getCountryName());
							break;
						}
					}

					for(j=0;j<stateList.size();j++)
					{
						if(labourList.get(i).getStateId().equals(stateList.get(j).getStateId()))
						{
							labourList.get(i).setStateId(stateList.get(j).getStateName());
							break;
						}
					}

					for(j=0;j<cityList.size();j++)
					{
						if(labourList.get(i).getCityId().equals(cityList.get(j).getCityId()))
						{
							labourList.get(i).setCityId(cityList.get(j).getCityName());
							break;
						}
					}

					for(j=0;j<locationareaList.size();j++)
					{
						if(labourList.get(i).getLocationareaId().equals(locationareaList.get(j).getLocationareaId()))
						{
							labourList.get(i).setLocationareaId(locationareaList.get(j).getLocationareaName());
							break;
						}
					}

				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}
			model.addAttribute("labourList",labourList);
			return "LabourMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/AddLabour")
	public String AddLabour(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<Country> countryList = countryRepository.findAll(); 
			List<Project> projectList = GetUserAssigenedProjectList(req,res); 

			model.addAttribute("projectList",projectList);	
			model.addAttribute("countryList",countryList);
			model.addAttribute("labourCode",LabourCode());
			return "AddLabour";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping(value = "/AddLabour", method = RequestMethod.POST)
	public String AddLabour(@ModelAttribute Labour labour, Model model)
	{
		try {
			try
			{
				labour = new Labour(labour.getLabourId(), labour.getLabourfirstName(), labour.getLabourmiddleName(), labour.getLabourlastName(), labour.getLabourGender(), 
						labour.getLabourAge(), labour.getProjectId(), labour.getLabourAddress(), labour.getCountryId(), labour.getStateId(),
						labour.getCityId(), labour.getLocationareaId(), labour.getAreaPincode(),
						labour.getLabourMobileno(), labour.getLabourAadharcardno(), labour.getLabourJoiningdate(), 
						labour.getLabourBasicpay(),labour.getStatus(), 
						labour.getCreationDate(), labour.getUpdateDate(), labour.getUserName());
				labourRepository.save(labour);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}


			List<Labour> labourList = labourRepository.findAll(); 

			model.addAttribute("labourList",labourList);
			return "LabourMaster";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping("/EditLabour")
	public String EditLabour(@RequestParam("labourId") String labourId, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			Query query = new Query();
			List<Labour> labourDetails = mongoTemplate.find(query.addCriteria(Criteria.where("labourId").is(labourId)),Labour.class);
			List<Country> countryList = countryRepository.findAll(); 
			List<Project> projectList = GetUserAssigenedProjectList(req,res); 
			try {

				query = new Query();
				List<Project> projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(labourDetails.get(0).getProjectId())),Project.class);

				query = new Query();
				Country countryDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("countryId").is(labourDetails.get(0).getCountryId())),Country.class);
				query = new Query();
				State stateDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("stateId").is(labourDetails.get(0).getStateId())),State.class);
				query = new Query();
				City cityDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("cityId").is(labourDetails.get(0).getCityId())),City.class);
				query = new Query();
				LocationArea locationareaDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("locationareaId").is(labourDetails.get(0).getLocationareaId())),LocationArea.class);

				model.addAttribute("countryName", countryDetails.getCountryName());
				model.addAttribute("stateName", stateDetails.getStateName());
				model.addAttribute("cityName", cityDetails.getCityName());
				model.addAttribute("locationareaName", locationareaDetails.getLocationareaName());
				model.addAttribute("projectName",projectList1.get(0).getProjectName());
			}catch (Exception e) {
				// TODO: handle exception
			}

			model.addAttribute("countryList",countryList);
			model.addAttribute("projectList",projectList);
			model.addAttribute("labourDetails",labourDetails);
			return "EditLabour";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping(value = "/EditLabour", method = RequestMethod.POST)
	public String EditLabour(@ModelAttribute Labour labour, Model model)
	{
		try {
			try
			{
				labour = new Labour(labour.getLabourId(), labour.getLabourfirstName(), labour.getLabourmiddleName(), labour.getLabourlastName(), labour.getLabourGender(), 
						labour.getLabourAge(), labour.getProjectId(), labour.getLabourAddress(), labour.getCountryId(), labour.getStateId(),
						labour.getCityId(), labour.getLocationareaId(), labour.getAreaPincode(),
						labour.getLabourMobileno(), labour.getLabourAadharcardno(), labour.getLabourJoiningdate(), 
						labour.getLabourBasicpay(),labour.getStatus(), 
						labour.getCreationDate(), labour.getUpdateDate(), labour.getUserName());
				labourRepository.save(labour);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}


			List<Labour> labourList = labourRepository.findAll(); 

			List<LocationArea> locationareaList= locationAreaRepository.findAll();
			List<City> cityList = cityRepository.findAll();
			List<State> stateList= stateRepository.findAll();
			List<Country> countryList= countryRepository.findAll();


			int i,j;
			for(i=0;i<labourList.size();i++)
			{
				try 
				{
					for(j=0;j<countryList.size();j++)
					{
						if(labourList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
						{
							labourList.get(i).setCountryId(countryList.get(j).getCountryName());
							break;
						}
					}

					for(j=0;j<stateList.size();j++)
					{
						if(labourList.get(i).getStateId().equals(stateList.get(j).getStateId()))
						{
							labourList.get(i).setStateId(stateList.get(j).getStateName());
							break;
						}
					}

					for(j=0;j<cityList.size();j++)
					{
						if(labourList.get(i).getCityId().equals(cityList.get(j).getCityId()))
						{
							labourList.get(i).setCityId(cityList.get(j).getCityName());
							break;
						}
					}

					for(j=0;j<locationareaList.size();j++)
					{
						if(labourList.get(i).getLocationareaId().equals(locationareaList.get(j).getLocationareaId()))
						{
							labourList.get(i).setLocationareaId(locationareaList.get(j).getLocationareaName());
							break;
						}
					}

				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}
			model.addAttribute("labourList",labourList);
			return "LabourMaster";

		}catch (Exception e) {
			return "login";
		}
	}

}

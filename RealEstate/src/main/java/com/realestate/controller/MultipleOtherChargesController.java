package com.realestate.controller;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Booking;
import com.realestate.bean.City;
import com.realestate.bean.Flat;
import com.realestate.bean.LocationArea;
import com.realestate.bean.MultipleOtherCharges;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.State;
import com.realestate.configuration.CommanController;
import com.realestate.repository.MultipleOtherChargesRepository;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;

@Controller
@RequestMapping("/")
public class MultipleOtherChargesController 
{
	@Autowired
	MultipleOtherChargesRepository multipleOtherChargesRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	int otherChargesId;
	public int GenerateMultipleOtherChargesId()
	{
		otherChargesId=0;
		List<MultipleOtherCharges> countList = multipleOtherChargesRepository.findAll();

		if(countList.size()!=0)
		{
			otherChargesId = countList.get(countList.size()-1).getOtherChargesId()+1;
		}
		else
		{
			otherChargesId = 1;
		}

		return otherChargesId;
	}

	@ResponseBody
	@RequestMapping("/AddExtraOtherChagresPayment")
	public List<MultipleOtherCharges> AddExtraOtherChagresPayment(@RequestParam("chargesId") String chargesId, @RequestParam("bookingId") String bookingId, @RequestParam("chargesDescription") String chargesDescription, @RequestParam("amount") String amount)
	{
		Query query = new Query();
		try
		{
			MultipleOtherCharges multipleOtherCharges = new MultipleOtherCharges();
			multipleOtherCharges.setOtherChargesId(GenerateMultipleOtherChargesId());
			multipleOtherCharges.setChargesId(chargesId);
			multipleOtherCharges.setBookingId(bookingId);
			multipleOtherCharges.setChargesDescription(chargesDescription);
			multipleOtherCharges.setAmount(Double.parseDouble(amount));

			multipleOtherChargesRepository.save(multipleOtherCharges);

			List<MultipleOtherCharges> multipleOtherChargesList = mongoTemplate.find(query.addCriteria(Criteria.where("chargesId").is(chargesId).and("bookingId").is(bookingId)), MultipleOtherCharges.class);
			return multipleOtherChargesList;
		}
		catch(Exception e)
		{
			return null;
		}
	}

	@ResponseBody
	@RequestMapping("/UpdateExtraOtherChagresPayment")
	public List<MultipleOtherCharges> UpdateExtraOtherChagresPayment(@RequestParam("otherChargesId") String otherChargesId, @RequestParam("chargesId") String chargesId, @RequestParam("bookingId") String bookingId, @RequestParam("chargesDescription") String chargesDescription, @RequestParam("amount") String amount)
	{
		Query query = new Query();
		try
		{
			MultipleOtherCharges multipleOtherCharges = new MultipleOtherCharges();
			multipleOtherCharges.setOtherChargesId(Integer.parseInt(otherChargesId));
			multipleOtherCharges.setChargesId(chargesId);
			multipleOtherCharges.setBookingId(bookingId);
			multipleOtherCharges.setChargesDescription(chargesDescription);
			multipleOtherCharges.setAmount(Double.parseDouble(amount));

			multipleOtherChargesRepository.save(multipleOtherCharges);

			List<MultipleOtherCharges> multipleOtherChargesList = mongoTemplate.find(query.addCriteria(Criteria.where("chargesId").is(chargesId).and("bookingId").is(bookingId)), MultipleOtherCharges.class);
			return multipleOtherChargesList;
		}
		catch(Exception e)
		{
			return null;
		}
	}

	@ResponseBody
	@RequestMapping("/EditExtraOtherChagresPayment")
	public List<MultipleOtherCharges> EditExtraOtherChagresPayment(@RequestParam("otherChargesId") String otherChargesId, @RequestParam("chargesId") String chargesId, @RequestParam("bookingId") String bookingId)
	{
		Query query = new Query();
		try
		{
			List<MultipleOtherCharges> multipleOtherCharges = mongoTemplate.find(query.addCriteria(Criteria.where("otherChargesId").is(Integer.parseInt(otherChargesId))), MultipleOtherCharges.class);

			return multipleOtherCharges;
		}
		catch(Exception e)
		{
			return null;
		}
	}

	@ResponseBody
	@RequestMapping("/DeleteExtraOtherChagresPayment")
	public List<MultipleOtherCharges> DeleteExtraOtherChagresPayment(@RequestParam("otherChargesId") String otherChargesId, @RequestParam("chargesId") String chargesId, @RequestParam("bookingId") String bookingId)
	{
		Query query = new Query();
		try
		{
			mongoTemplate.remove(query.addCriteria(Criteria.where("otherChargesId").is(Integer.parseInt(otherChargesId)).and("chargesId").is(chargesId).and("bookingId").is(bookingId)),MultipleOtherCharges.class);

			query = new Query();
			List<MultipleOtherCharges> multipleOtherChargesList = mongoTemplate.find(query.addCriteria(Criteria.where("chargesId").is(chargesId).and("bookingId").is(bookingId)), MultipleOtherCharges.class);

			return multipleOtherChargesList;
		}
		catch(Exception e)
		{
			return null;
		}
	}

	@ResponseBody
	@RequestMapping(value="/PrintCustomerExtraChargesList")
	public ResponseEntity<byte[]> PrintCustomerExtraChargesList(@RequestParam("bookingId") String bookingId, ModelMap model, HttpServletResponse response)
	{
		JasperPrint print;
		String customerName="";

		try 
		{	
			Query query = new Query();
			HashMap jmap = new HashMap();
			Collection c = new ArrayList();

			List<MultipleOtherCharges> otherCharges = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)), MultipleOtherCharges.class);

			query = new Query();
			List<Booking> bookingList = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)), Booking.class);


			query = new Query();
			List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(bookingList.get(0).getProjectId())), Project.class);

			query =new Query();
			List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(bookingList.get(0).getBuildingId())), ProjectBuilding.class);

			query =new Query();
			List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(bookingList.get(0).getWingId())), ProjectWing.class);

			query = new Query();
			List<Flat> flatDetails = mongoTemplate.find(query.addCriteria(Criteria.where("flatId").is(bookingList.get(0).getFlatId())), Flat.class);

			//Booking address

			customerName =  bookingList.get(0).getBookingfirstname()+" "+bookingList.get(0).getBookingmiddlename()+" "+bookingList.get(0).getBookinglastname();

			Date date = new Date();  
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
			String strDate= formatter.format(date);

			try
			{
				if(otherCharges.size()!=0)
				{
					try
					{
						Double totalAmount =0.0;

						for(int i=0;i<otherCharges.size();i++)
						{
							jmap = new HashMap();
							jmap.put("srno",""+(i+1));
							jmap.put("desc",""+otherCharges.get(i).getChargesDescription());
							jmap.put("amount", ""+(long)otherCharges.get(i).getAmount());

							totalAmount+=otherCharges.get(i).getAmount();

							c.add(jmap);
							jmap = null;
						}

						//String realPath = "//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img";
						String realPath =CommanController.GetLogoImagePath();
						JRDataSource dataSource = new JRMapCollectionDataSource(c);
						Map<String, Object> parameterMap = new HashMap<String, Object>();

						/*1*/  parameterMap.put("bookingId", ""+bookingList.get(0).getBookingId());
						/*2*/  parameterMap.put("customerName", ""+customerName);
						/*3*/  parameterMap.put("printDate", ""+strDate);
						/*4*/  parameterMap.put("realPath", ""+realPath);
						/*5*/  parameterMap.put("customerAddress",""+bookingList.get(0).getBookingaddress()+", "+bookingList.get(0).getBookingPincode());
						/*6*/  parameterMap.put("projectName",""+projectDetails.get(0).getProjectName());
						/*7*/  parameterMap.put("buildingName",""+buildingDetails.get(0).getBuildingName());
						/*8*/  parameterMap.put("wingName",""+wingDetails.get(0).getWingName());
						/*9*/  parameterMap.put("flatNumber",""+flatDetails.get(0).getFlatNumber());
						/*10*/ parameterMap.put("totalAmount",""+totalAmount.longValue());

						InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/CustomerExtraChargesList.jasper");

						print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

						ByteArrayOutputStream baos = new ByteArrayOutputStream();
						JasperExportManager.exportReportToPdfStream(print, baos);

						byte[] contents = baos.toByteArray();

						HttpHeaders headers = new HttpHeaders();
						headers.setContentType(MediaType.parseMediaType("application/pdf"));
						String filename = "Customer Extra Charges List.pdf";

						JasperExportManager.exportReportToPdfStream(print, baos);


						headers.setContentType(MediaType.parseMediaType("application/pdf"));
						headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

						ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
						response.setHeader("Content-Disposition", "inline; filename=" + filename );

						return resp;
					}
					catch(Exception e)
					{
						return null;
					}
				}
				else
				{
					try
					{
						jmap = new HashMap();

						jmap.put("srno","N.A.");
						jmap.put("desc","N.A.");
						jmap.put("amount", "N.A.");

						c.add(jmap);
						jmap = null;

						//String realPath = "//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img";
						String realPath =CommanController.GetLogoImagePath();
						JRDataSource dataSource = new JRMapCollectionDataSource(c);
						Map<String, Object> parameterMap = new HashMap<String, Object>();

						/*1*/  parameterMap.put("bookingId", ""+bookingList.get(0).getBookingId());
						/*2*/  parameterMap.put("customerName", ""+customerName);
						/*3*/  parameterMap.put("printDate", ""+strDate);
						/*4*/  parameterMap.put("realPath", ""+realPath);
						/*5*/  parameterMap.put("customerAddress",""+bookingList.get(0).getBookingaddress()+", "+bookingList.get(0).getBookingPincode());
						/*6*/  parameterMap.put("projectName",""+projectDetails.get(0).getProjectName());
						/*7*/  parameterMap.put("buildingName",""+buildingDetails.get(0).getBuildingName());
						/*8*/  parameterMap.put("wingName",""+wingDetails.get(0).getWingName());
						/*9*/  parameterMap.put("flatNumber",""+flatDetails.get(0).getFlatNumber());
						/*10*/ parameterMap.put("totalAmount","0.0");

						InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/CustomerExtraChargesList.jasper");

						print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

						ByteArrayOutputStream baos = new ByteArrayOutputStream();
						JasperExportManager.exportReportToPdfStream(print, baos);

						byte[] contents = baos.toByteArray();

						HttpHeaders headers = new HttpHeaders();
						headers.setContentType(MediaType.parseMediaType("application/pdf"));
						String filename = "Customer Extra Charges List.pdf";

						JasperExportManager.exportReportToPdfStream(print, baos);


						headers.setContentType(MediaType.parseMediaType("application/pdf"));
						headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

						ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
						response.setHeader("Content-Disposition", "inline; filename=" + filename );

						return resp;
					}
					catch(Exception e)
					{
						return null;
					}
				}
			}
			catch(Exception e)
			{
				System.out.println("Exception = "+e.toString());
				return null;
			}

		}
		catch(Exception ex)
		{
			return null;
		}

	}

}

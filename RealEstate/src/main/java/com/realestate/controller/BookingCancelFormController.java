package com.realestate.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.realestate.bean.Agent;
import com.realestate.bean.Aggreement;
import com.realestate.bean.Bank;
import com.realestate.bean.Booking;
import com.realestate.bean.BookingCancelForm;
import com.realestate.bean.Country;
import com.realestate.bean.CustomerFlatCheckList;
import com.realestate.bean.CustomerLoanDetails;
import com.realestate.bean.CustomerPaymentDetails;
import com.realestate.bean.Enquiry;
import com.realestate.bean.ExtraCharges;
import com.realestate.bean.Flat;
import com.realestate.bean.Login;
import com.realestate.bean.Occupation;
import com.realestate.bean.ParkingZone;
import com.realestate.bean.Project;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.BookingRepository;
import com.realestate.repository.BudgetRepository;
import com.realestate.repository.CountryRepository;
import com.realestate.repository.CustomerFlatCheckListRepository;
import com.realestate.repository.CustomerLoanDetailsRepository;
import com.realestate.repository.CustomerPaymentDetailsRepository;
import com.realestate.repository.DesignationRepository;
import com.realestate.repository.EnquiryRepository;
import com.realestate.repository.EnquirySourceRepository;
import com.realestate.repository.ExtraChargeRepository;
import com.realestate.repository.FlatRepository;
import com.realestate.repository.GeneratePaymentSchedulerRepository;
import com.realestate.repository.OccupationRepository;
import com.realestate.repository.ParkingZoneRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.AgentRepository;
import com.realestate.repository.AggreementRepository;
import com.realestate.repository.BankRepository;
import com.realestate.repository.BookingCancelFormRepository;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

@Controller
@RequestMapping("/")
public class BookingCancelFormController 
{
	@Autowired 
	AggreementRepository aggreementRepository;
	@Autowired
	BookingCancelFormRepository bookingcancelformRepository;
	@Autowired
	BookingRepository bookingRepository;
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	FlatRepository flatRepository;
	@Autowired
	DesignationRepository designationRepository;
	@Autowired 
	OccupationRepository occupationRepository;
	@Autowired 
	BudgetRepository budgetRepository;
	@Autowired
	EnquirySourceRepository enquirySourceRepository;
	@Autowired
	EnquiryRepository enquiryRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	CountryRepository countryRepository;
	@Autowired
	BankRepository bankRepository;
	@Autowired
	GeneratePaymentSchedulerRepository generatepaymentschedulerRepository;
	@Autowired
	CustomerPaymentDetailsRepository customerpaymentdetailsrepository;
	@Autowired
	AgentRepository agentRepository;
	@Autowired 
	ExtraChargeRepository extrachargesRepository;
	@Autowired
	CustomerLoanDetailsRepository customerLoandetailsRepository;
	@Autowired
	ParkingZoneRepository parkingZoneRepository;

	@Autowired
	CustomerFlatCheckListRepository customerflatchecklistRepository;
	String cancelCode;
	String code;
	int cancelCount;
	private String CancelCode()
	{
		List<BookingCancelForm> cancelCount1;

		cancelCount1 = bookingcancelformRepository.findAll();
		if(cancelCount1.size()!=0) {
			for(int i=0;i<cancelCount1.size();i++)
			{
				code=cancelCount1.get(i).getCancelId();
			}
			String[] part = code.split("(?<=\\D)(?=\\d)");
			cancelCount=Integer.parseInt(part[1]);
		}
		else {
			cancelCount=0;
		}
		if(cancelCount<10)
		{
			cancelCode = "BK000"+(cancelCount+1);
		}
		else if((cancelCount>=10) && (cancelCount<100))
		{
			cancelCode = "BK00"+(cancelCount+1);
		}
		else if((cancelCount>=100) && (cancelCount<1000))
		{
			cancelCode = "BK0"+(cancelCount+1);
		}
		else if(cancelCount>=1000)
		{
			cancelCode = "BK"+(cancelCount+1);
		}
		return cancelCode;
	}


	String bookingCode;
	int year = Calendar.getInstance().get(Calendar.YEAR);
	int month=Calendar.getInstance().get(Calendar.MONTH)+1;
	int y1,y2;
	private String BookingCode()
	{
		long bookingCount = bookingRepository.count();
		if(month>3) 
		{
			y1=year;
			y2=year+1;
			if(bookingCount<10)
			{
				bookingCode = "BKG/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/000"+(bookingCount+1);
			}
			else if((bookingCount>=10) && (bookingCount<100))
			{
				bookingCode = "BKG/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/00"+(bookingCount+1);
			}
			else if((bookingCount>=100) && (bookingCount<1000))
			{
				bookingCode = "BKG/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/0"+(bookingCount+1);
			}
			else if(bookingCount>1000)
			{
				bookingCode = "BKG/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/"+(bookingCount+1);
			}
		}
		else
		{
			y1=year-1;
			y2=year;
			if(bookingCount<10)
			{
				bookingCode = "BKG/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/000"+(bookingCount+1);
			}
			else if((bookingCount>=10) && (bookingCount<100))
			{
				bookingCode = "BKG/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/00"+(bookingCount+1);
			}
			else if((bookingCount>=100) && (bookingCount<1000))
			{
				bookingCode = "BKG/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/0"+(bookingCount+1);
			}
			else if(bookingCount>1000)
			{
				bookingCode = "BKG/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/"+(bookingCount+1);
			}
		}
		return bookingCode;
	}

	String payAmountCode;
	private String customerpayAmountCode()
	{
		long Count = generatepaymentschedulerRepository.count();

		if(Count<10)
		{
			payAmountCode = "TP000"+(Count+1);
		}
		else if((Count>=10) && (Count<100))
		{
			payAmountCode = "TP00"+(Count+1);
		}
		else if((Count>=100) && (Count<1000))
		{
			payAmountCode = "TP0"+(Count+1);
		}
		else if(Count>1000)
		{
			payAmountCode = "TP"+(Count+1);
		}

		return payAmountCode;
	}


	private List<Country> findAllCountryId()
	{
		List<Country> countryList;

		countryList = countryRepository.findAll(new Sort(Sort.Direction.ASC,"countryName"));
		return countryList;
	}
	//code for retriving all occupation retriving
	private List<Occupation> getAllOccupationName()
	{
		List<Occupation> occupationList;

		occupationList = occupationRepository.findAll(new Sort(Sort.Direction.ASC,"occupationName"));
		return occupationList;
	}
	private List<Bank> getAllBankName()
	{
		List<Bank> bankList;

		bankList = bankRepository.findAll(new Sort(Sort.Direction.ASC,"bankName"));
		return bankList;
	}

	//Request Mapping For Home
	@RequestMapping("/BookingCancellationform")
	public String Bookingcancel(@RequestParam("bookingId") String bookingId, ModelMap model) 
	{
		try {
			Query query = new Query();
			List<Booking> bookingList = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)),Booking.class);

			model.addAttribute("bookingList", bookingList);
			model.addAttribute("cancelCode", CancelCode());
			return "BookingCancellationform";

		}catch (Exception e) {
			return "login";
		}
	}
	//code getting all Agent
	private List<Agent> AgentList()
	{
		List<Agent> agentList;

		agentList = agentRepository.findAll(new Sort(Sort.Direction.ASC,"agentName"));
		return agentList;
	}

	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping(value="/BookingCancellationform", method=RequestMethod.POST)
	public String bookingcancelSave(Model model, @ModelAttribute BookingCancelForm bookingcancelform, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			Flat flat;
			Booking booking;
			try
			{
				bookingcancelform = new BookingCancelForm(bookingcancelform.getCancelId(),bookingcancelform.getBookingId(),bookingcancelform.getBookingDate(),bookingcancelform.getCancelDate(),bookingcancelform.getCustomerName(),bookingcancelform.getCustomerEmail(),bookingcancelform.getCustomerMobile(),
						bookingcancelform.getProjectId(),bookingcancelform.getBuildingId(),bookingcancelform.getWingId(),bookingcancelform.getFloorId(),bookingcancelform.getFlatType(),bookingcancelform.getFlatId(), bookingcancelform.getFlatFacing(),bookingcancelform.getFlatareainSqFt(),bookingcancelform.getFlatbasicCost(),bookingcancelform.getAggreementValue1(),bookingcancelform.getCancelCharge(),bookingcancelform.getGstTaxes(),bookingcancelform.getNetAmount(),bookingcancelform.getCreationDate(),bookingcancelform.getUpdateDate(),bookingcancelform.getUserName());
				bookingcancelformRepository.save(bookingcancelform);

				flat = mongoTemplate.findOne(Query.query(Criteria.where("flatId").is(bookingcancelform.getFlatId())), Flat.class);
				flat.setFlatstatus("Booking Remaninig");
				flatRepository.save(flat);

				booking = mongoTemplate.findOne(Query.query(Criteria.where("bookingId").is(bookingcancelform.getBookingId())), Booking.class);
				booking.setBookingstatus("Cancel");
				bookingRepository.save(booking);

				try {
					List<Booking> bookingList; 
					Query query1 = new Query();
					bookingList = mongoTemplate.find(query1.addCriteria(Criteria.where("bookingId").is(bookingcancelform.getBookingId())), Booking.class);

					ParkingZone parkingzone;
					parkingzone = mongoTemplate.findOne(Query.query(Criteria.where("parkingZoneId").is(bookingList.get(0).getParkingZoneId())), ParkingZone.class);
					parkingzone.setParkingZoneStatus("Not Assigned");
					parkingZoneRepository.save(parkingzone);
				}
				catch(Exception e) {}
			}
			catch(DuplicateKeyException de)
			{
				//model.addAttribute("bankStatus","Fail");
			}


			List<Enquiry> enquiryList;
			List<Project> projectList = GetUserAssigenedProjectList(req,res); 
			List<Booking> bookingList; 
			List<Aggreement> aggreementList = aggreementRepository.findAll();
			List<ExtraCharges> extrachargesList = extrachargesRepository.findAll(); 
			List<CustomerLoanDetails> loanList = customerLoandetailsRepository.findAll();
			List<BookingCancelForm> cancelList = bookingcancelformRepository.findAll();
			List<CustomerFlatCheckList> CustomercheckList = customerflatchecklistRepository.findAll();
			Query query = new Query();
			enquiryList = mongoTemplate.find(query.addCriteria(Criteria.where("enqStatus").is("Booking")), Enquiry.class);
			Query query1 = new Query();
			bookingList = mongoTemplate.find(query1.addCriteria(Criteria.where("bookingstatus").ne("Cancel")), Booking.class);

			model.addAttribute("CustomercheckList",CustomercheckList);
			model.addAttribute("aggreementList",aggreementList);
			model.addAttribute("cancelList",cancelList);
			model.addAttribute("loanList",loanList);
			model.addAttribute("extrachargesList",extrachargesList);
			model.addAttribute("bookingList", bookingList);
			model.addAttribute("projectList",projectList);
			model.addAttribute("enquiryList", enquiryList);
			return "BookingMaster";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping("/BookingCancelList")
	public String AllBooingCancelList(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{		
		try {
			List<BookingCancelForm> cancelList = bookingcancelformRepository.findAll();
			model.addAttribute("cancelList", cancelList);

			return "BookingCancelList";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("CancelAgainBooking")
	public String CancelAgainBooking(@RequestParam("bookingId") String bookingId, ModelMap model, HttpServletRequest req,HttpServletResponse res)
	{
		try {
			Query query = new Query();
			List<Booking> bookingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)),Booking.class);

			model.addAttribute("bookingDetails", bookingDetails);

			List<Project> projectList = GetUserAssigenedProjectList(req,res); 

			model.addAttribute("projectList",projectList);
			model.addAttribute("occupationList",getAllOccupationName()); 
			model.addAttribute("bankList",getAllBankName());
			model.addAttribute("agentList",AgentList());
			model.addAttribute("bookingCode",BookingCode());
			return "CancelAgainBooking";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/CancelAgainBooking", method = RequestMethod.POST)
	public String CancelBooking(Model model, @ModelAttribute Booking booking,@RequestParam("previousbookingId") String previousbookingId, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try {

				CustomerPaymentDetails customerpaymentdetails=new CustomerPaymentDetails();
				customerpaymentdetails.setId(customerpayAmountCode());
				customerpaymentdetails.setBookingId(booking.getBookingId());
				customerpaymentdetails.setTotalpayAggreement(0.0);
				customerpaymentdetails.setTotalpayStampDuty(0.0);
				customerpaymentdetails.setTotalpayRegistration(0.0);
				customerpaymentdetails.setTotalpaygstAmount(0.0);
				customerpaymentdetails.setAmountStatus("pending");
				customerpaymentdetails.getCreationDate();
				customerpaymentdetails.getUpdateDate();
				customerpaymentdetails.getUserName();
				customerpaymentdetailsrepository.save(customerpaymentdetails);
			}
			catch(Exception es)
			{}


			Flat flat;
			Enquiry enquiry;
			try
			{

				Booking booking1=new Booking();
				booking1.setBookingId(booking.getBookingId());
				booking1.setEnquiryId(booking.getEnquiryId());

				booking1.setBookingfirstname(booking.getBookingfirstname().toUpperCase());
				booking1.setBookingmiddlename(booking.getBookingmiddlename());
				booking1.setBookinglastname(booking.getBookinglastname());

				booking1.setBookingaddress(booking.getBookingaddress());
				booking1.setBookingPincode(booking.getBookingPincode());

				booking1.setBookingEmail(booking.getBookingEmail());
				booking1.setBookingmobileNumber1(booking.getBookingmobileNumber1());
				booking1.setBookingmobileNumber2(booking.getBookingmobileNumber2());
				booking1.setBookingOccupation(booking.getBookingOccupation());
				booking1.setPurposeOfFlat(booking.getPurposeOfFlat());

				booking1.setProjectId(booking.getProjectId());
				booking1.setBuildingId(booking.getBuildingId());
				booking1.setWingId(booking.getWingId());
				booking1.setFloorId(booking.getFloorId());
				booking1.setFlatType(booking.getFlatType());
				booking1.setFlatId(booking.getFlatId());
				booking1.setFlatFacing(booking.getFlatFacing());
				booking1.setFlatareainSqFt(booking.getFlatareainSqFt());
				booking1.setFlatCostwithotfloorise(booking.getFlatCostwithotfloorise());
				booking1.setFloorRise(booking.getFloorRise());
				booking1.setFlatCost(booking.getFlatCost());
				booking1.setFlatbasicCost(booking.getFlatbasicCost());
				booking1.setParkingFloorId(booking.getParkingFloorId());
				booking1.setParkingZoneId(booking.getParkingZoneId());
				booking1.setAgentId(booking.getAgentId());
				booking1.setInfrastructureCharge(booking.getInfrastructureCharge());
				booking1.setAggreementValue1(booking.getAggreementValue1());
				booking1.setHandlingCharges(booking.getHandlingCharges());
				booking1.setStampDutyPer(booking.getStampDutyPer());
				booking1.setRegistrationPer(booking.getRegistrationPer());
				booking1.setStampDuty1(booking.getStampDuty1());
				booking1.setRegistrationCost1(booking.getRegistrationCost1());
				booking1.setGstCost(booking.getGstCost());
				booking1.setGstAmount1(booking.getGstAmount1());
				booking1.setGrandTotal1(booking.getGrandTotal1());
				booking1.setTds(booking.getTds());
				booking1.setBookingstatus(booking.getBookingstatus());
				booking1.setCreationDate(new Date());
				booking1.setUpdateDate(new Date());
				booking1.setUserName(booking.getUserName());

				bookingRepository.save(booking1);

				flat = mongoTemplate.findOne(Query.query(Criteria.where("flatId").is(booking.getFlatId())), Flat.class);
				flat.setFlatstatus("Booking Completed");
				flatRepository.save(flat);

				mongoTemplate.remove(Query.query(Criteria.where("bookingId").is(previousbookingId)), BookingCancelForm.class);

				enquiry = mongoTemplate.findOne(Query.query(Criteria.where("enquiryId").is(booking.getEnquiryId())), Enquiry.class);
				enquiry.setEnqStatus("Booking Completed");
				enquiryRepository.save(enquiry);

				ParkingZone parkingzone=new ParkingZone();
				parkingzone = mongoTemplate.findOne(Query.query(Criteria.where("parkingZoneId").is(booking.getParkingZoneId())), ParkingZone.class);
				parkingzone.setParkingZoneStatus("Assigned");
				parkingZoneRepository.save(parkingzone);

			}
			catch(Exception ww)
			{
			}  


			List<Enquiry> enquiryList;
			List<Project> projectList = GetUserAssigenedProjectList(req,res); 
			List<Booking> bookingList; 
			List<Aggreement> aggreementList = aggreementRepository.findAll();
			List<ExtraCharges> extrachargesList = extrachargesRepository.findAll(); 
			List<CustomerLoanDetails> loanList = customerLoandetailsRepository.findAll();
			List<BookingCancelForm> cancelList = bookingcancelformRepository.findAll();
			List<CustomerFlatCheckList> CustomercheckList = customerflatchecklistRepository.findAll();
			Query query = new Query();
			enquiryList = mongoTemplate.find(query.addCriteria(Criteria.where("enqStatus").is("Booking")), Enquiry.class);
			Query query1 = new Query();
			bookingList = mongoTemplate.find(query1.addCriteria(Criteria.where("bookingstatus").ne("Cancel")), Booking.class);

			model.addAttribute("CustomercheckList",CustomercheckList);
			model.addAttribute("aggreementList",aggreementList);
			model.addAttribute("cancelList",cancelList);
			model.addAttribute("loanList",loanList);
			model.addAttribute("extrachargesList",extrachargesList);
			model.addAttribute("bookingList", bookingList);
			model.addAttribute("projectList",projectList);
			model.addAttribute("enquiryList", enquiryList);

			return "BookingMaster";

		}catch (Exception e) {
			return "login";
		}
	}

}

package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Contractor;
import com.realestate.bean.ContractorPaymentSchedule;
import com.realestate.bean.ContractorType;
import com.realestate.bean.ContractorWorkListHistory;
import com.realestate.bean.ContractorWorkOrder;
import com.realestate.bean.Login;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.SubContractorType;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.ContractorPaymentScheduleRepository;
import com.realestate.repository.ContractorRepository;
import com.realestate.repository.ContractorTypeRepository;
import com.realestate.repository.ContractorWorkOrderRepository;
import com.realestate.repository.ProjectRepository;

@Controller
@RequestMapping("/")
public class ContractorPaymentSchedulerController {
	@Autowired
	ContractorRepository contractorRepository;
	@Autowired
	ContractorPaymentScheduleRepository contractorpaymentscheduleRepository;
	@Autowired
	ContractorTypeRepository contractortypeRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	ContractorWorkOrderRepository contractorworkorderRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping("/ContractorPaymentScheduler")
	public String ContractorPaymentScheduler(Model model, HttpServletRequest req, HttpServletResponse res) {
		try {
			List<Project> projectList = GetUserAssigenedProjectList(req,res);
			List<ContractorWorkOrder> contarctorworkorderList = contractorworkorderRepository.findAll();
			Query query = new Query();
			for (int i = 0; i < contarctorworkorderList.size(); i++) {
				try {
					query = new Query();
					List<Project> projectDetails = mongoTemplate.find(
							query.addCriteria(
									Criteria.where("projectId").is(contarctorworkorderList.get(i).getProjectId())),
							Project.class);
					query = new Query();
					List<ProjectBuilding> buildingDetails = mongoTemplate.find(
							query.addCriteria(
									Criteria.where("buildingId").is(contarctorworkorderList.get(i).getBuildingId())),
							ProjectBuilding.class);
					query = new Query();
					List<ProjectWing> wingDetails = mongoTemplate.find(
							query.addCriteria(Criteria.where("wingId").is(contarctorworkorderList.get(i).getWingId())),
							ProjectWing.class);

					query = new Query();
					List<ContractorType> contractorDetails = mongoTemplate.find(query.addCriteria(
							Criteria.where("contractortypeId").is(contarctorworkorderList.get(i).getContractortypeId())),
							ContractorType.class);

					contarctorworkorderList.get(i).setProjectId(projectDetails.get(0).getProjectName());
					contarctorworkorderList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
					contarctorworkorderList.get(i).setWorkId(wingDetails.get(0).getWingName());
					contarctorworkorderList.get(i).setContractortypeId(contractorDetails.get(0).getContractorType());
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
			model.addAttribute("contarctorworkorderList", contarctorworkorderList);
			model.addAttribute("projectList", projectList);

			return "ContractorPaymentScheduler";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/ViewContractorWorkOrderPaymentScheduler")
	public String ViewContractorWorkOrderPaymentScheduler(@RequestParam("workOrderId") String workOrderId,
			ModelMap model) {
		try {
			Query query = new Query();
			List<ContractorWorkOrder> contractorworkorderDetails = mongoTemplate
					.find(query.addCriteria(Criteria.where("workOrderId").is(workOrderId)), ContractorWorkOrder.class);

			query = new Query();
			List<ContractorPaymentSchedule> contractorpaymentscheduleList = mongoTemplate.find(
					query.addCriteria(Criteria.where("workOrderId").is(workOrderId)), ContractorPaymentSchedule.class);

			try {

				query = new Query();
				List<Project> projectDetails = mongoTemplate.find(
						query.addCriteria(Criteria.where("projectId").is(contractorworkorderDetails.get(0).getProjectId())),
						Project.class);
				query = new Query();
				List<ProjectBuilding> buildingDetails = mongoTemplate.find(
						query.addCriteria(
								Criteria.where("buildingId").is(contractorworkorderDetails.get(0).getBuildingId())),
						ProjectBuilding.class);
				query = new Query();
				List<ProjectWing> wingDetails = mongoTemplate.find(
						query.addCriteria(Criteria.where("wingId").is(contractorworkorderDetails.get(0).getWingId())),
						ProjectWing.class);

				query = new Query();
				List<ContractorType> contractorTypeDetails = mongoTemplate.find(query.addCriteria(
						Criteria.where("contractortypeId").is(contractorworkorderDetails.get(0).getContractortypeId())),
						ContractorType.class);
				query = new Query();
				List<Contractor> contractorDetails = mongoTemplate.find(
						query.addCriteria(
								Criteria.where("contractorId").is(contractorworkorderDetails.get(0).getContractorId())),
						Contractor.class);

				query = new Query();
				List<ContractorWorkListHistory> contractorworkhistoryList = mongoTemplate.find(
						query.addCriteria(Criteria.where("workId").is(contractorworkorderDetails.get(0).getWorkId())),
						ContractorWorkListHistory.class);

				for (int i = 0; i < contractorworkhistoryList.size(); i++) {
					query = new Query();
					List<SubContractorType> subcontractortypeDetails = mongoTemplate.find(
							query.addCriteria(Criteria.where("subcontractortypeId")
									.is(contractorworkhistoryList.get(i).getSubcontractortypeId())),
							SubContractorType.class);
					contractorworkhistoryList.get(i)
					.setSubcontractortypeId(subcontractortypeDetails.get(0).getSubcontractorType());
				}

				model.addAttribute("contractorworkhistoryList", contractorworkhistoryList);
				model.addAttribute("projectName", projectDetails.get(0).getProjectName());
				model.addAttribute("buildingName", buildingDetails.get(0).getBuildingName());
				model.addAttribute("wingName", wingDetails.get(0).getWingName());
				model.addAttribute("contractorType", contractorTypeDetails.get(0).getContractorType());
				model.addAttribute("contractorDetails", contractorDetails);
			} catch (Exception e) {
				// TODO: handle exception
			}

			model.addAttribute("contractorpaymentscheduleList", contractorpaymentscheduleList);
			model.addAttribute("contractorworkorderDetails", contractorworkorderDetails);
			return "ViewContractorWorkOrderPaymentScheduler";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/TochangesContrctorPaymentStatus")
	public List<ContractorPaymentSchedule> TochangesContrctorPaymentStatus(
			@RequestParam("contractorrschedulerId") String contractorrschedulerId,
			@RequestParam("workOrderId") String workOrderId, @RequestParam("status") String status,
			HttpSession session) {
		try {
			ContractorPaymentSchedule chnageStatus = new ContractorPaymentSchedule();
			chnageStatus = mongoTemplate.findOne(
					Query.query(Criteria.where("contractorrschedulerId").is(Integer.parseInt(contractorrschedulerId))),
					ContractorPaymentSchedule.class);
			chnageStatus.setStatus(status);
			contractorpaymentscheduleRepository.save(chnageStatus);

			Query query = new Query();
			List<ContractorPaymentSchedule> contractorpaymentList = mongoTemplate.find(
					query.addCriteria(Criteria.where("workOrderId").is(workOrderId)), ContractorPaymentSchedule.class);
			return contractorpaymentList;
		} catch (Exception e) {
			return null;
		}

	}

	@ResponseBody
	@RequestMapping("/getProjectwiseContractorWorkOrderList")
	public List<ContractorWorkOrder> getProjectwiseContractorWorkOrderList(@RequestParam("projectId") String projectId,
			HttpSession session) {
		try {
			Query query = new Query();
			List<ContractorWorkOrder> contarctorworkorderList = mongoTemplate
					.find(query.addCriteria(Criteria.where("projectId").is(projectId)), ContractorWorkOrder.class);
			for (int i = 0; i < contarctorworkorderList.size(); i++) {
				try {
					query = new Query();
					List<Project> projectDetails = mongoTemplate.find(
							query.addCriteria(
									Criteria.where("projectId").is(contarctorworkorderList.get(i).getProjectId())),
							Project.class);
					query = new Query();
					List<ProjectBuilding> buildingDetails = mongoTemplate.find(
							query.addCriteria(
									Criteria.where("buildingId").is(contarctorworkorderList.get(i).getBuildingId())),
							ProjectBuilding.class);
					query = new Query();
					List<ProjectWing> wingDetails = mongoTemplate.find(
							query.addCriteria(Criteria.where("wingId").is(contarctorworkorderList.get(i).getWingId())),
							ProjectWing.class);

					query = new Query();
					List<ContractorType> contractorDetails = mongoTemplate.find(query.addCriteria(Criteria
							.where("contractortypeId").is(contarctorworkorderList.get(i).getContractortypeId())),
							ContractorType.class);

					contarctorworkorderList.get(i).setProjectId(projectDetails.get(0).getProjectName());
					contarctorworkorderList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
					contarctorworkorderList.get(i).setWingId(wingDetails.get(0).getWingName());
					contarctorworkorderList.get(i).setContractortypeId(contractorDetails.get(0).getContractorType());
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
			return contarctorworkorderList;
		} catch (Exception e) {
			return null;
		}

	}

	@ResponseBody
	@RequestMapping("/getBuildingwiseWingContractorWorkOrderList")
	public List<ContractorWorkOrder> getBuildingwiseWingContractorWorkOrderList(
			@RequestParam("buildingId") String buildingId, @RequestParam("projectId") String projectId,
			HttpSession session) {
		try {
			Query query = new Query();
			List<ContractorWorkOrder> contarctorworkorderList = mongoTemplate.find(
					query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId)),
					ContractorWorkOrder.class);
			for (int i = 0; i < contarctorworkorderList.size(); i++) {
				try {
					query = new Query();
					List<Project> projectDetails = mongoTemplate.find(
							query.addCriteria(
									Criteria.where("projectId").is(contarctorworkorderList.get(i).getProjectId())),
							Project.class);
					query = new Query();
					List<ProjectBuilding> buildingDetails = mongoTemplate.find(
							query.addCriteria(
									Criteria.where("buildingId").is(contarctorworkorderList.get(i).getBuildingId())),
							ProjectBuilding.class);
					query = new Query();
					List<ProjectWing> wingDetails = mongoTemplate.find(
							query.addCriteria(Criteria.where("wingId").is(contarctorworkorderList.get(i).getWingId())),
							ProjectWing.class);

					query = new Query();
					List<ContractorType> contractorDetails = mongoTemplate.find(query.addCriteria(Criteria
							.where("contractortypeId").is(contarctorworkorderList.get(i).getContractortypeId())),
							ContractorType.class);

					contarctorworkorderList.get(i).setProjectId(projectDetails.get(0).getProjectName());
					contarctorworkorderList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
					contarctorworkorderList.get(i).setWingId(wingDetails.get(0).getWingName());
					contarctorworkorderList.get(i).setContractortypeId(contractorDetails.get(0).getContractorType());
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
			return contarctorworkorderList;
		} catch (Exception e) {
			return null;
		}

	}

	@ResponseBody
	@RequestMapping("/getWingwiseWingContractorWorkOrderList")
	public List<ContractorWorkOrder> getWingwiseWingContractorWorkOrderList(@RequestParam("wingId") String wingId,
			@RequestParam("buildingId") String buildingId, @RequestParam("projectId") String projectId,
			HttpSession session) {
		try {
			Query query = new Query();
			List<ContractorWorkOrder> contarctorworkorderList = mongoTemplate.find(query.addCriteria(Criteria
					.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId)),
					ContractorWorkOrder.class);
			for (int i = 0; i < contarctorworkorderList.size(); i++) {
				try {
					query = new Query();
					List<Project> projectDetails = mongoTemplate.find(
							query.addCriteria(
									Criteria.where("projectId").is(contarctorworkorderList.get(i).getProjectId())),
							Project.class);
					query = new Query();
					List<ProjectBuilding> buildingDetails = mongoTemplate.find(
							query.addCriteria(
									Criteria.where("buildingId").is(contarctorworkorderList.get(i).getBuildingId())),
							ProjectBuilding.class);
					query = new Query();
					List<ProjectWing> wingDetails = mongoTemplate.find(
							query.addCriteria(Criteria.where("wingId").is(contarctorworkorderList.get(i).getWingId())),
							ProjectWing.class);

					query = new Query();
					List<ContractorType> contractorDetails = mongoTemplate.find(query.addCriteria(Criteria
							.where("contractortypeId").is(contarctorworkorderList.get(i).getContractortypeId())),
							ContractorType.class);

					contarctorworkorderList.get(i).setProjectId(projectDetails.get(0).getProjectName());
					contarctorworkorderList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
					contarctorworkorderList.get(i).setWingId(wingDetails.get(0).getWingName());
					contarctorworkorderList.get(i).setContractortypeId(contractorDetails.get(0).getContractorType());
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
			return contarctorworkorderList;
		} catch (Exception e) {
			return null;
		}

	}

}

package com.realestate.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
@Controller
@RequestMapping("/")
public class menuController {


	//Request Mapping For Home
	@RequestMapping("/menu")
	public String homeRedirect(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{

		return "menu";
	}
}

package com.realestate.controller;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.stereotype.Controller;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.realestate.bean.Bank;
import com.realestate.bean.Booking;
import com.realestate.bean.City;
import com.realestate.bean.Company;
import com.realestate.bean.Country;
import com.realestate.bean.CustomerReceiptForm;
import com.realestate.bean.Department;
import com.realestate.bean.Designation;
import com.realestate.bean.Employee;
import com.realestate.bean.Flat;
import com.realestate.bean.Floor;
import com.realestate.bean.LocationArea;
import com.realestate.bean.Login;
import com.realestate.bean.NumberToWordConversion;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.State;
import com.realestate.configuration.CommanController;
import com.realestate.repository.BankRepository;
import com.realestate.repository.CityRepository;
import com.realestate.repository.CompanyRepository;
import com.realestate.repository.CountryRepository;
import com.realestate.repository.DepartmentRepository;
import com.realestate.repository.EmployeeRepository;
import com.realestate.repository.LocationAreaRepository;
import com.realestate.repository.StateRepository;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;

@Controller
@RequestMapping("/")
public class EmployeeController 
{
	@Autowired
	LocationAreaRepository locationAreaRepository;
	@Autowired
	CityRepository cityRepository;
	@Autowired
	StateRepository stateRepository;
	@Autowired
	EmployeeRepository employeeRepository;
	@Autowired
	CountryRepository countryRepository;
	@Autowired
	BankRepository bankRepository;
	@Autowired
	DepartmentRepository departmentRepository;
	@Autowired
	CompanyRepository companyRepository;
	@Autowired
	MongoTemplate mongoTemplate;


	//Code generation for company code
	String employeeCode;
	private String employeeCode()
	{
		long employeeCount = employeeRepository.count();

		if(employeeCount<10)
		{
			employeeCode = "EM000"+(employeeCount+1);
		}
		else if((employeeCount>=10) && (employeeCount<100))
		{
			employeeCode = "EM00"+(employeeCount+1);
		}
		else if((employeeCount>=100) && (employeeCount<1000))
		{
			employeeCode = "EM0"+(employeeCount+1);
		}
		else 
		{
			employeeCode = "EM"+(employeeCount+1);
		}

		return employeeCode;
	}

	//code for getting all country name
	private List<Country> findAllCountryId()
	{
		List<Country> countryList;

		countryList = countryRepository.findAll(new Sort(Sort.Direction.ASC,"countryId"));
		return countryList;
	}

	//code for getting all bank names
	private List<Bank> getAllBankName()
	{

		List<Bank> bankList= new ArrayList<Bank>();
		Query query = new Query();
		bankList = bankRepository.findAll(new Sort(Sort.Direction.ASC,"bankName"));
		return bankList;
	}

	//code for retriving all designations
	private List<Department> getAllDepartment()
	{
		List<Department> departmentList;

		departmentList = departmentRepository.findAll(new Sort(Sort.Direction.ASC,"departmentName"));
		return departmentList;
	}

	//Request Mapping For Add Employee
	@RequestMapping("/AddEmployee")
	public String addEmployee(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<Employee> empolyeeList = employeeRepository.findAll(); 

			model.addAttribute("employeeCode",employeeCode());
			model.addAttribute("countryList",findAllCountryId());
			model.addAttribute("bankList",getAllBankName());
			model.addAttribute("departmentList", getAllDepartment());
			model.addAttribute("empolyeeList",empolyeeList);
			return "AddEmployee";

		}catch (Exception e) {
			return "login";
		}
	}

	public boolean UploadImageToPath(byte[] image, String path)
	{

		try {

			DiskFileItemFactory factory = new DiskFileItemFactory();

			//byte[] bytes = profile_img.getBytes();
			BufferedOutputStream stream =new BufferedOutputStream(new FileOutputStream(new File(path)));
			stream.write(image);
			stream.flush();
			stream.close();

		}catch (Exception e) {
			System.out.println("Eooror = "+e);
			// TODO: handle exception
		}

		File f = new File(path);

		if (f.exists() && !f.isDirectory()) 
		{
			return true;
		} 
		else 
		{
			return false;
		}
	}

	@RequestMapping(value="/AddEmployee", method=RequestMethod.POST)
	public String employeeSave(@RequestParam("profile_img") MultipartFile profile_img, @ModelAttribute Employee employee, Model model) throws IOException
	{

		try {
			try
			{
				String path = CommanController.GetProfileImagePath()+ employee.getEmployeeId() + ".png";
				//String path = "D:/Data1/" + employee.getEmployeeId() + ".png";

				byte [] image = profile_img.getBytes();

				if (image.length != 0) 
				{
					UploadImageToPath(image, path);
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}


			try
			{
				employee = new Employee(employee.getEmployeeId(),employee.getEmployeefirstName(),employee.getEmployeemiddleName(),employee.getEmployeelastName(),
						employee.getEmployeeGender(),employee.getEmployeeMarried(),employee.getEmployeeDob(),
						employee.getEmployeeAnniversaryDate(),employee.getEmployeeSpuseName(),employee.getEmployeeEducation(),employee.getDepartmentId(),employee.getDesignationId(),
						employee.getEmployeeAddress(),employee.getAreaPincode(),
						employee.getEmployeePancardno(),employee.getEmployeeAadharcardno(),
						employee.getEmployeeEmailId(),employee.getEmployeeMobileno(),employee.getCompanyEmailId(),employee.getCompanyMobileno(),
						employee.getEmployeeJoiningdate(),employee.getEmployeeBasicpay(),employee.getEmployeeHra(),employee.getEmployeeDa(),employee.getEmployeeCa(),
						employee.getEmployeePfacno(),employee.getEmployeeEsino(),employee.getEmployeePLleaves(),employee.getEmployeeSLleaves(),employee.getEmployeeCLleaves(),
						employee.getEmployeeBankName(),employee.getBranchName(),employee.getBankifscCode(),employee.getEmployeeBankacno(),
						employee.getStatus(),employee.getCreationDate(),employee.getUpdateDate(),employee.getUserName());

				employeeRepository.save(employee);

				model.addAttribute("employeeStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("employeeStatus","Fail");
			}

			model.addAttribute("employeeCode",employeeCode());
			model.addAttribute("countryList",findAllCountryId());
			model.addAttribute("bankList",getAllBankName());
			model.addAttribute("departmentList", getAllDepartment());
			return "AddEmployee";

		}catch (Exception e) {
			return "login";
		}
	}

	//Retrive all employee in Employee Master
	@RequestMapping("/EmployeeMaster")
	public String EmployeeMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<Employee> empolyeeList = employeeRepository.findAll();

			model.addAttribute("empolyeeList",empolyeeList);
			return "EmployeeMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	//to Retive All Employee in Employee Master by country name
	@ResponseBody
	@RequestMapping(value="/getCountryEmployeeList",method=RequestMethod.POST)
	public List<Employee> EmployeeWiseEmployeeList(@RequestParam("countryId") String countryId, HttpSession session)
	{
		List<Employee> employeeList= new ArrayList<Employee>();

		Query query = new Query();
		employeeList = mongoTemplate.find(query.addCriteria(Criteria.where("countryId").is(countryId)), Employee.class);
		for(int i=0;i<employeeList.size();i++)
		{
			try {

				query = new Query();
				List<Department> departmentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("departmentId").is(employeeList.get(i).getDepartmentId())),Department.class);

				query = new Query();
				List<Designation> designationDetails = mongoTemplate.find(query.addCriteria(Criteria.where("designationId").is(employeeList.get(i).getDesignationId())),Designation.class);

				employeeList.get(i).setDepartmentId(departmentDetails.get(0).getDepartmentName());
				employeeList.get(i).setDesignationId(designationDetails.get(0).getDesignationName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}
		return employeeList;
	}

	//to Retive All Employee in Employee Master by State name
	@ResponseBody
	@RequestMapping(value="/getCityWiseEmployeeList",method=RequestMethod.POST)
	public List<Employee> EmployeeListByState(@RequestParam("stateId") String stateId, @RequestParam("countryId") String countryId,HttpSession session)
	{
		List<Employee> employeeList= new ArrayList<Employee>();

		Query query = new Query();
		employeeList = mongoTemplate.find(query.addCriteria(Criteria.where("countryId").is(countryId).and("stateId").is(stateId)), Employee.class);
		for(int i=0;i<employeeList.size();i++)
		{
			try {

				query = new Query();
				List<Department> departmentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("departmentId").is(employeeList.get(i).getDepartmentId())),Department.class);

				query = new Query();
				List<Designation> designationDetails = mongoTemplate.find(query.addCriteria(Criteria.where("designationId").is(employeeList.get(i).getDesignationId())),Designation.class);

				employeeList.get(i).setDepartmentId(departmentDetails.get(0).getDepartmentName());
				employeeList.get(i).setDesignationId(designationDetails.get(0).getDesignationName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}
		return employeeList;
	}

	//to Retive All Employee in Employee Master by city name
	@ResponseBody
	@RequestMapping(value="/getLocationAreaWiseEmployeeList",method=RequestMethod.POST)
	public List<Employee> EmployeeListByCity(@RequestParam("cityId") String cityId, @RequestParam("stateId") String stateId, @RequestParam("countryId") String countryId, HttpSession session)
	{
		List<Employee> employeeList= new ArrayList<Employee>();

		Query query = new Query();
		employeeList = mongoTemplate.find(query.addCriteria(Criteria.where("countryId").is(countryId).and("stateId").is(stateId).and("cityId").is(cityId)), Employee.class);
		for(int i=0;i<employeeList.size();i++)
		{
			try {

				query = new Query();
				List<Department> departmentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("departmentId").is(employeeList.get(i).getDepartmentId())),Department.class);

				query = new Query();
				List<Designation> designationDetails = mongoTemplate.find(query.addCriteria(Criteria.where("designationId").is(employeeList.get(i).getDesignationId())),Designation.class);

				employeeList.get(i).setDepartmentId(departmentDetails.get(0).getDepartmentName());
				employeeList.get(i).setDesignationId(designationDetails.get(0).getDesignationName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}
		return employeeList;
	}

	//to Retive All Employee in Employee Master by city name
	@ResponseBody
	@RequestMapping(value="/getEmployeeList",method=RequestMethod.POST)
	public List<Employee> EmployeeListByLocation(@RequestParam("locationareaId") String locationareaId, @RequestParam("cityId") String cityId, @RequestParam("stateId") String stateId, @RequestParam("countryId") String countryId,  HttpSession session)
	{
		List<Employee> employeeList= new ArrayList<Employee>();

		Query query = new Query();
		employeeList = mongoTemplate.find(query.addCriteria(Criteria.where("countryId").is(countryId).and("stateId").is(stateId).and("cityId").is(cityId).and("locationareaId").is(locationareaId)), Employee.class);
		for(int i=0;i<employeeList.size();i++)
		{
			try {

				query = new Query();
				List<Department> departmentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("departmentId").is(employeeList.get(i).getDepartmentId())),Department.class);

				query = new Query();
				List<Designation> designationDetails = mongoTemplate.find(query.addCriteria(Criteria.where("designationId").is(employeeList.get(i).getDesignationId())),Designation.class);

				employeeList.get(i).setDepartmentId(departmentDetails.get(0).getDepartmentName());
				employeeList.get(i).setDesignationId(designationDetails.get(0).getDesignationName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}
		return employeeList;
	}

	//Search employee name wise list
	@ResponseBody
	@RequestMapping("/EmployeeNameWiseList")
	public List<Employee> EmployeeNameWiseList(@RequestParam("employeeName") String employeeName)
	{
		Query query = new Query();

		List<Employee> employeeList = mongoTemplate.find(query.addCriteria(Criteria.where("employeefirstName").regex("^"+employeeName+".*","i")),Employee.class);
		return employeeList;
	}

	@ResponseBody
	@RequestMapping("/getaadharList")
	public List<Employee> AllempolyeeList(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		List<Employee> employeeList= employeeRepository.findAll();

		return employeeList;
	}

	@RequestMapping("GetImage")
	public void GetImageForEdit(@RequestParam("profileImgPath") String profileImgPath,HttpServletResponse response)
	{

		try
		{
			if(profileImgPath !=null && !profileImgPath.equals(""))
			{

				File file=new File(profileImgPath);

				if(file.exists())
				{
					FileInputStream input=new FileInputStream(file);
					byte[] image=IOUtils.toByteArray(input);
					response.getOutputStream().write(image);	
				}
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	@RequestMapping("/EditEmployee")
	public String EditEmployee(@RequestParam("employeeId") String employeeId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<Employee> employeeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(employeeId)),Employee.class);
			try {
				String path = CommanController.GetProfileImagePath()+ employeeDetails.get(0).getEmployeeId() + ".png";

				query = new Query();
				List<Department> departmentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("departmentId").is(employeeDetails.get(0).getDepartmentId())),Department.class);

				query = new Query();
				List<Designation> designationDetails = mongoTemplate.find(query.addCriteria(Criteria.where("designationId").is(employeeDetails.get(0).getDesignationId())),Designation.class);

				model.addAttribute("departmentName",departmentDetails.get(0).getDepartmentName());
				model.addAttribute("designationName",designationDetails.get(0).getDesignationName());

				model.addAttribute("profileImgPath", path);
			}
			catch (Exception e) {
				// TODO: handle exception
			}
			model.addAttribute("employeeDetails",employeeDetails);
			model.addAttribute("departmentList", getAllDepartment());
			model.addAttribute("bankList",getAllBankName());
			return "EditEmployee";

		}catch (Exception e) {
			return "login";
		}
	}
	@RequestMapping(value="/EditEmployee", method=RequestMethod.POST)
	public String EditEmployeePostMethod(@RequestParam("profile_img") MultipartFile profile_img, @ModelAttribute Employee employee, Model model)
	{
		try {
			try
			{
				String path =CommanController.GetProfileImagePath() + employee.getEmployeeId() + ".png";
				//String path = "D:/Data1/" + employee.getEmployeeId() + ".jpg";

				byte [] image = profile_img.getBytes();

				if (image.length != 0) 
				{
					UploadImageToPath(image, path);
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}

			try
			{
				employee = new Employee(employee.getEmployeeId(),employee.getEmployeefirstName(),employee.getEmployeemiddleName(),employee.getEmployeelastName(),
						employee.getEmployeeGender(),employee.getEmployeeMarried(),employee.getEmployeeDob(),
						employee.getEmployeeAnniversaryDate(),employee.getEmployeeSpuseName(),employee.getEmployeeEducation(),employee.getDepartmentId(),employee.getDesignationId(),
						employee.getEmployeeAddress(),employee.getAreaPincode(),
						employee.getEmployeePancardno(),employee.getEmployeeAadharcardno(),
						employee.getEmployeeEmailId(),employee.getEmployeeMobileno(),employee.getCompanyEmailId(),employee.getCompanyMobileno(),
						employee.getEmployeeJoiningdate(),employee.getEmployeeBasicpay(),employee.getEmployeeHra(),employee.getEmployeeDa(),employee.getEmployeeCa(),
						employee.getEmployeePfacno(),employee.getEmployeeEsino(),employee.getEmployeePLleaves(),employee.getEmployeeSLleaves(),employee.getEmployeeCLleaves(),
						employee.getEmployeeBankName(),employee.getBranchName(),employee.getBankifscCode(),employee.getEmployeeBankacno(),
						employee.getStatus(),employee.getCreationDate(),employee.getUpdateDate(),employee.getUserName());
				employeeRepository.save(employee);
				model.addAttribute("employeeStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("employeeStatus","Fail");
			}

			

			List<Employee> empolyeeList = employeeRepository.findAll();

			model.addAttribute("empolyeeList",empolyeeList);

			return "EmployeeMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping(value="/getDesignationList",method=RequestMethod.POST)
	public List<Designation> getDesignationList(@RequestParam("departmentId") String departmentId, HttpSession session)
	{
		List<Designation> designationList= new ArrayList<Designation>();

		Query query = new Query();
		designationList = mongoTemplate.find(query.addCriteria(Criteria.where("departmentId").is(departmentId)), Designation.class);

		return designationList;
	}

	@ResponseBody
	@RequestMapping(value="/getDesignationWiseEmployeeList",method=RequestMethod.POST)
	public List<Employee> getDesignationWiseEmployeeList(@RequestParam("departmentId") String departmentId, @RequestParam("designationId") String designationId, HttpSession session)
	{
		Query query = new Query();
		List<Employee> employeeList = mongoTemplate.find(query.addCriteria(Criteria.where("departmentId").is(departmentId).and("designationId").is(designationId)), Employee.class);

		List<Employee> empolyeeList = employeeRepository.findAll();

		List<LocationArea> locationareaList= locationAreaRepository.findAll();
		List<City> cityList = cityRepository.findAll();
		List<State> stateList= stateRepository.findAll();
		List<Country> countryList= countryRepository.findAll();
		int j;
		query = new Query();
		for(int i=0;i<empolyeeList.size();i++)
		{
			try {

				query = new Query();
				List<Department> departmentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("departmentId").is(empolyeeList.get(i).getDepartmentId())),Department.class);

				query = new Query();
				List<Designation> designationDetails = mongoTemplate.find(query.addCriteria(Criteria.where("designationId").is(empolyeeList.get(i).getDesignationId())),Designation.class);

				empolyeeList.get(i).setDepartmentId(departmentDetails.get(0).getDepartmentName());
				empolyeeList.get(i).setDesignationId(designationDetails.get(0).getDesignationName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		return employeeList;
	}


	@ResponseBody
	@RequestMapping(value="/PrintEmployeeDetails")
	public ResponseEntity<byte[]> PrintEmployeeDetails(@RequestParam("employeeId") String employeeId, ModelMap model, HttpServletRequest req, HttpServletResponse res, HttpServletResponse response)
	{

		int index=0;
		JasperPrint print;
		try 
		{	

			Query query = new Query();
			String userId = (String)req.getSession().getAttribute("user");

			query = new Query();
			List<Login> loginDetails = mongoTemplate.find(query.addCriteria(Criteria.where("userId").is(userId)),Login.class);

			query = new Query();
			List<Employee> userDetails = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.get(0).getEmployeeId())),Employee.class);
			query = new Query();
			Department departmentDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("departmentId").is(userDetails.get(0).getDepartmentId())),Department.class);
			query = new Query();
			Designation designationDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("designationId").is(userDetails.get(0).getDesignationId())),Designation.class);


			Collection c = new ArrayList();
			HashMap jmap = new HashMap();
			jmap.put("index", ""+(index++));
			c.add(jmap);

			SimpleDateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");
			List<Company> companyDetails= companyRepository.findAll();

			jmap = null;

			query = new Query();
			List<Employee> employeeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(employeeId)),Employee.class);


			JRDataSource dataSource = new JRMapCollectionDataSource(c);
			Map<String, Object> parameterMap = new HashMap();

			Date date = new Date();  
			String strDate= createDateFormat.format(date);

			parameterMap.put("employeeId",""+employeeDetails.get(0).getEmployeeId());

			parameterMap.put("date", ""+strDate);

			parameterMap.put("employeeName", ""+employeeDetails.get(0).getEmployeefirstName()+" "+employeeDetails.get(0).getEmployeemiddleName()+" "+employeeDetails.get(0).getEmployeelastName());

			parameterMap.put("employeeDob",""+employeeDetails.get(0).getEmployeeDob());

			parameterMap.put("employeeEducation",""+employeeDetails.get(0).getEmployeeEducation());

			parameterMap.put("departmentId",""+departmentDetails.getDepartmentName());

			parameterMap.put("designationId",""+designationDetails.getDesignationName());

			parameterMap.put("employeeAddress", ""+employeeDetails.get(0).getEmployeeAddress()+", "+employeeDetails.get(0).getAreaPincode()+",");

			parameterMap.put("addressDetails1", "");

			parameterMap.put("addressDetails2", "");


			parameterMap.put("employeePancardno", ""+employeeDetails.get(0).getEmployeePancardno());

			parameterMap.put("employeeAadharcardno", ""+employeeDetails.get(0).getEmployeeAadharcardno());

			parameterMap.put("emailId",""+employeeDetails.get(0).getEmployeeEmailId());

			parameterMap.put("mobileNumber", ""+employeeDetails.get(0).getEmployeeMobileno());

			parameterMap.put("employeeJoiningdate", ""+employeeDetails.get(0).getEmployeeJoiningdate());

			parameterMap.put("employeeBasicpay", ""+employeeDetails.get(0).getEmployeeBasicpay());



			parameterMap.put("employeeHra", ""+employeeDetails.get(0).getEmployeeHra());

			parameterMap.put("employeeDa", ""+employeeDetails.get(0).getEmployeeDa());

			parameterMap.put("employeeCa",""+employeeDetails.get(0).getEmployeeCa());

			parameterMap.put("employeePfacno", ""+employeeDetails.get(0).getEmployeePfacno());

			parameterMap.put("employeeEsino", ""+employeeDetails.get(0).getEmployeeEsino());



			parameterMap.put("companyName", ""+companyDetails.get(0).getCompanyName());

			parameterMap.put("companyAddress", ""+companyDetails.get(0).getCompanyAddress()+", "+companyDetails.get(0).getLocationareaId()+", "+companyDetails.get(0).getCityId());


			parameterMap.put("userName", ""+userDetails.get(0).getEmployeefirstName()+" "+userDetails.get(0).getEmployeelastName());



			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/EmployeeDetails.jasper");

			print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(print, baos);

			byte[] contents = baos.toByteArray();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			String filename = "EmployeeDetails.pdf";

			JasperExportManager.exportReportToPdfStream(print, baos);


			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

			ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
			response.setHeader("Content-Disposition", "inline; filename=" + filename );

			return resp;					    
		}
		catch(Exception ex)
		{
			return null;
		}

	}


	@ResponseBody
	@RequestMapping(value="/CheckEmailId",method=RequestMethod.POST)
	public Employee CheckEmailId(@RequestParam("employeeEmailId") String employeeEmailId, HttpSession session)
	{
		Query query = new Query();
		Employee email = mongoTemplate.findOne(query.addCriteria(Criteria.where("employeeEmailId").is(employeeEmailId)), Employee.class);
		return email;
	}

}

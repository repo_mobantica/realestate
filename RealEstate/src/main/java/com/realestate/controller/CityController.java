package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.City;
import com.realestate.bean.Country;
import com.realestate.bean.State;
import com.realestate.repository.CityRepository;
import com.realestate.repository.CountryRepository;
import com.realestate.repository.StateRepository;

@Controller
@RequestMapping("/")
public class CityController
{
	@Autowired
	CityRepository cityRepository;
	@Autowired
	StateRepository stateRepository;
	@Autowired
	CountryRepository countryRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	//code city code generation
	String cityCode;

	private String CityCode()
	{
		long cityCount  = cityRepository.count();

		if(cityCount<10)
		{
			cityCode = "CT000"+(cityCount+1);
		}
		else if((cityCount>=10) && (cityCount<100))
		{
			cityCode = "CT00"+(cityCount+1);
		}
		else if((cityCount>=100) && (cityCount<1000))
		{
			cityCode = "CT0"+(cityCount+1);
		}
		else
		{
			cityCode = "CT"+(cityCount+1);
		}

		return cityCode;
	}
	@ResponseBody
	@RequestMapping("/SearchNameWiseCityList")
	public List<City> SearchNameWiseCityList(@RequestParam("cityName") String cityName)
	{
		Query query = new Query();
		List<City> cityList = mongoTemplate.find(query.addCriteria(Criteria.where("cityName").regex("^"+cityName+".*","i")),City.class);

		return cityList;
	}
	//code for getting all country name
	private List<Country> findAllCountryId()
	{
		List<Country> countryList;

		countryList = countryRepository.findAll(new Sort(Sort.Direction.ASC,"countryId"));
		return countryList;
	}

	@RequestMapping("/CityMaster")
	public String CityMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<City> cityList = cityRepository.findAll();
			List<State> stateList= stateRepository.findAll();
			List<Country> countryList= countryRepository.findAll();

			int i,j;
			for(i=0;i<cityList.size();i++)
			{
				try
				{
					for(j=0;j<countryList.size();j++)
					{
						if(cityList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
						{
							cityList.get(i).setCountryId(countryList.get(j).getCountryName());
							break;
						}
					}

					for(j=0;j<stateList.size();j++)
					{
						if(cityList.get(i).getStateId().equals(stateList.get(j).getStateId()))
						{
							cityList.get(i).setStateId(stateList.get(j).getStateName());
							break;
						}
					}

				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}

			model.addAttribute("countryList",findAllCountryId());
			model.addAttribute("cityList",cityList);
			return "CityMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	// to get all city list by country
	@ResponseBody
	@RequestMapping(value="/getCountryCityList",method=RequestMethod.POST)
	public List<City> CountryCityList(@RequestParam("countryId") String countryId, HttpSession session)
	{
		List<City> cityList= new ArrayList<City>();

		Query query = new Query();
		cityList = mongoTemplate.find(query.addCriteria(Criteria.where("countryId").is(countryId)), City.class);
		query = new Query();
		List<Country> countryList = mongoTemplate.find(query.addCriteria(Criteria.where("countryId").is(countryId)), Country.class);

		List<State> stateList= stateRepository.findAll();
		int i,j;
		for(i=0;i<cityList.size();i++)
		{
			try
			{
				cityList.get(i).setCountryId(countryList.get(0).getCountryName());

				for(j=0;j<stateList.size();j++)
				{
					if(cityList.get(i).getStateId().equals(stateList.get(j).getStateId()))
					{
						cityList.get(i).setStateId(stateList.get(j).getStateName());
						break;
					}
				}

			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		return cityList;
	}


	//Request Mapping For Add City
	@RequestMapping("/AddCity")
	public String addCity(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<City> cityList;
			cityList = cityRepository.findAll();

			model.addAttribute("countryList",findAllCountryId());
			model.addAttribute("cityCode", CityCode());
			model.addAttribute("cityList",cityList);

			return "AddCity";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value = "/AddCity", method = RequestMethod.POST)
	public String citySave(@ModelAttribute City city, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				city = new City(city.getCityId(), city.getCityName(), city.getCountryId(), city.getStateId(), city.getCreationDate(), city.getUpdateDate(), city.getUserName());
				cityRepository.save(city);
				model.addAttribute("cityStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("cityStatus","Fail");
			}

			List<City> cityList;
			cityList = cityRepository.findAll();

			model.addAttribute("countryList",findAllCountryId());
			model.addAttribute("cityCode", CityCode());
			model.addAttribute("cityList",cityList);

			return "AddCity";

		}catch (Exception e) {
			return "login";
		}
	}
	@ResponseBody
	@RequestMapping("/getallCityList")
	public List<City> AllCityList(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		List<City> cityList= cityRepository.findAll();

		List<State> stateList= stateRepository.findAll();
		List<Country> countryList= countryRepository.findAll();
		int i,j;
		for(i=0;i<cityList.size();i++)
		{
			try
			{
				for(j=0;j<countryList.size();j++)
				{
					if(cityList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
					{
						cityList.get(i).setCountryId(countryList.get(j).getCountryName());
						break;
					}
				}

				for(j=0;j<stateList.size();j++)
				{
					if(cityList.get(i).getStateId().equals(stateList.get(j).getStateId()))
					{
						cityList.get(i).setStateId(stateList.get(j).getStateName());
						break;
					}
				}

			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		return cityList;
	}

	@RequestMapping("/EditCity")
	public String EditCity(@RequestParam("cityId") String cityId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<City> cityDetails = mongoTemplate.find(query.addCriteria(Criteria.where("cityId").is(cityId)),City.class);

			query = new Query();
			Country countryDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("countryId").is(cityDetails.get(0).getCountryId())),Country.class);

			query = new Query();
			State stateDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("stateId").is(cityDetails.get(0).getStateId())),State.class);

			model.addAttribute("countryName",countryDetails.getCountryName());
			model.addAttribute("stateName",stateDetails.getStateName());
			model.addAttribute("countryList",findAllCountryId());
			model.addAttribute("cityDetails",cityDetails);

			return "EditCity";

		}catch (Exception e) {
			return "login";
		}
	}
	@RequestMapping(value="/EditCity",method=RequestMethod.POST)
	public String EditCityPostMethod(@ModelAttribute City city, ModelMap model)
	{
		try {
			try
			{
				city = new City(city.getCityId(), city.getCityName(), city.getCountryId(), city.getStateId(), city.getCreationDate(), city.getUpdateDate(), city.getUserName());
				cityRepository.save(city);
				model.addAttribute("cityStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("cityStatus","Fail");
			}

			List<City> cityList;
			cityList = cityRepository.findAll();

			List<State> stateList= stateRepository.findAll();
			List<Country> countryList= countryRepository.findAll();
			int i,j;
			for(i=0;i<cityList.size();i++)
			{
				try
				{
					for(j=0;j<countryList.size();j++)
					{
						if(cityList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
						{
							cityList.get(i).setCountryId(countryList.get(j).getCountryName());
							break;
						}
					}

					for(j=0;j<stateList.size();j++)
					{
						if(cityList.get(i).getStateId().equals(stateList.get(j).getStateId()))
						{
							cityList.get(i).setStateId(stateList.get(j).getStateName());
							break;
						}
					}

				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}

			model.addAttribute("countryList",findAllCountryId());
			model.addAttribute("cityList",cityList);

			return "CityMaster";

		}catch (Exception e) {
			return "login";
		}
	}
}
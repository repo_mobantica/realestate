package com.realestate.controller;


import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.CriteriaDefinition;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.realestate.bean.Aggreement;
import com.realestate.bean.Booking;
import com.realestate.bean.BookingCancelForm;
import com.realestate.bean.City;
import com.realestate.bean.Company;
import com.realestate.bean.Country;
import com.realestate.bean.CustomerFlatCheckList;
import com.realestate.bean.CustomerLoanDetails;
import com.realestate.bean.Enquiry;
import com.realestate.bean.ExtraCharges;
import com.realestate.bean.Flat;
import com.realestate.bean.Floor;
import com.realestate.bean.GeneratePaymentScheduler;
import com.realestate.bean.LocationArea;
import com.realestate.bean.Login;
import com.realestate.bean.NumberToWordConversion;
import com.realestate.bean.Occupation;
import com.realestate.bean.PaymentScheduler;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.State;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.AgentRepository;
import com.realestate.repository.AggreementRepository;
import com.realestate.repository.BankRepository;
import com.realestate.repository.BookingCancelFormRepository;
import com.realestate.repository.BookingRepository;
import com.realestate.repository.BudgetRepository;
import com.realestate.repository.CountryRepository;
import com.realestate.repository.CustomerFlatCheckListRepository;
import com.realestate.repository.CustomerLoanDetailsRepository;
import com.realestate.repository.CustomerPaymentDetailsRepository;
import com.realestate.repository.DesignationRepository;
import com.realestate.repository.EnquiryRepository;
import com.realestate.repository.EnquirySourceRepository;
import com.realestate.repository.ExtraChargeRepository;
import com.realestate.repository.FlatRepository;
import com.realestate.repository.FloorRepository;
import com.realestate.repository.GeneratePaymentSchedulerRepository;
import com.realestate.repository.OccupationRepository;
import com.realestate.repository.PaymentSchedulerRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.TaxRepository;
import com.realestate.services.AggreementDetailsExcelView;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;

@Controller
@RequestMapping("/")
public class AggreementController
{
	@Autowired 
	AggreementRepository aggreementRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	CountryRepository countryRepository;
	@Autowired
	BankRepository bankRepository;
	@Autowired
	TaxRepository taxRepository;
	@Autowired
	DesignationRepository designationRepository;
	@Autowired 
	OccupationRepository occupationRepository;
	@Autowired 
	BookingRepository bookingRepository;
	@Autowired
	FlatRepository flatRepository;
	@Autowired
	BudgetRepository budgetRepository;
	@Autowired
	EnquirySourceRepository enquirySourceRepository;
	@Autowired
	PaymentSchedulerRepository paymentSchedulerRepository;
	@Autowired
	EnquiryRepository enquiryRepository;
	@Autowired
	GeneratePaymentSchedulerRepository generatepaymentschedulerRepository;
	@Autowired 
	ExtraChargeRepository extrachargesRepository;
	@Autowired
	CustomerLoanDetailsRepository customerLoandetailsRepository;
	@Autowired
	BookingCancelFormRepository bookingcancelformRepository;
	@Autowired
	FloorRepository floorRepository;
	@Autowired
	CustomerPaymentDetailsRepository customerpaymentdetailsrepository;
	@Autowired
	AgentRepository agentRepository;
	@Autowired
	CustomerFlatCheckListRepository customerflatchecklistRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	String paymentCode;
	private String PaymentCode()
	{
		long paymentCount = generatepaymentschedulerRepository.count();

		if(paymentCount<10)
		{
			paymentCode = "PS000"+(paymentCount+1);
		}
		else if((paymentCount>=10) && (paymentCount<100))
		{
			paymentCode = "PS00"+(paymentCount+1);
		}
		else if((paymentCount>=100) && (paymentCount<1000))
		{
			paymentCode = "PS0"+(paymentCount+1);
		}
		else if(paymentCount>=1000)
		{
			paymentCode = "PS"+(paymentCount+1);
		}

		return paymentCode;
	}

	//String aggreementCode;
	private String AggreementCode(String projectName, String buildingName, String wingName, String flatNumber)
	{
		String agreementId="AGG/";

		String flatNumber1 = flatNumber.replaceAll("\\s", "");
		String projectName1[]=null, buildingName1[]=null, wingName1[]=null;

		projectName1  = projectName.split(" ");
		buildingName1 = buildingName.split(" ");
		wingName1 	  = wingName.split(" ");

		if(projectName1.length==1)
		{
			for(int i=0; i<projectName1[0].length() && i<4;i++)
			{
				agreementId = agreementId + projectName1[0].charAt(i);
			}
		}
		else
		{
			for(int i=0; i<projectName1[0].length() && i<2;i++)
			{
				agreementId = agreementId + projectName1[0].charAt(i);
			}

			for(int i=0; i<projectName1[1].length() && i<2;i++)
			{
				agreementId = agreementId + projectName1[1].charAt(i);
			}
		}

		agreementId = agreementId +"/";

		if(buildingName1.length==1)
		{
			for(int i=0; i<buildingName1[0].length() && i<4;i++)
			{
				agreementId = agreementId + buildingName1[0].charAt(i);
			}
		}
		else
		{
			for(int i=0; i<buildingName1[0].length() && i<2;i++)
			{
				agreementId = agreementId + buildingName1[0].charAt(i);
			}

			for(int i=0; i<buildingName1[1].length() && i<2;i++)
			{
				agreementId = agreementId + buildingName1[1].charAt(i);
			}
		}

		agreementId = agreementId +"/";

		if(wingName1.length==1)
		{
			for(int i=0; i<wingName1[0].length() && i<4;i++)
			{
				agreementId = agreementId + wingName1[0].charAt(i);
			}
		}
		else
		{
			for(int i=0; i<wingName1[0].length() && i<2;i++)
			{
				agreementId = agreementId + wingName1[0].charAt(i);
			}

			for(int i=0; i<wingName1[1].length() && i<2;i++)
			{
				agreementId = agreementId + wingName1[1].charAt(i);
			}
		}
		agreementId=agreementId+"/"+flatNumber1;


		return agreementId;
	}


	private List<Country> findAllCountryId()
	{
		List<Country> countryList;

		countryList = countryRepository.findAll(new Sort(Sort.Direction.ASC,"countryId"));
		return countryList;
	}

	//code for retriving all occupation retriving
	private List<Occupation> getAllOccupationName()
	{
		List<Occupation> occupationList;

		occupationList = occupationRepository.findAll(new Sort(Sort.Direction.ASC,"occupationName"));
		return occupationList;
	}

	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping("/AddAggreementForm")
	public String AddAggreementForm(@RequestParam("bookingId") String bookingId, ModelMap model)
	{
		try {
			//String bookingId="BKG/1819/0029";
			Query query = new Query();
			List<Booking> bookingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)),Booking.class);

			query =new Query();
			List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(bookingDetails.get(0).getProjectId())), Project.class);

			query =new Query();
			List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(bookingDetails.get(0).getBuildingId())), ProjectBuilding.class);

			query =new Query();
			List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(bookingDetails.get(0).getWingId())), ProjectWing.class);

			query =new Query();
			List<Floor> floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("floorId").is(bookingDetails.get(0).getFloorId())), Floor.class);

			query =new Query();
			List<Flat> flatDetails = mongoTemplate.find(query.addCriteria(Criteria.where("flatId").is(bookingDetails.get(0).getFlatId())), Flat.class);

			model.addAttribute("projectName",projectDetails.get(0).getProjectName());
			model.addAttribute("buildingName",buildingDetails.get(0).getBuildingName());
			model.addAttribute("wingName",wingDetails.get(0).getWingName());
			model.addAttribute("floortypeName",floorDetails.get(0).getFloortypeName());
			model.addAttribute("flatNumber",flatDetails.get(0).getFlatNumber());

			model.addAttribute("bookingDetails",bookingDetails);
			model.addAttribute("aggreementCode",AggreementCode(projectDetails.get(0).getProjectName(),buildingDetails.get(0).getBuildingName(),wingDetails.get(0).getWingName(),flatDetails.get(0).getFlatNumber()));
			model.addAttribute("occupationList",getAllOccupationName());
			model.addAttribute("countryList",findAllCountryId());
			return "AddAggreementForm";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value = "/AddAggreementForm", method = RequestMethod.POST)
	public String SaveAddAggreementForm(@ModelAttribute Aggreement aggreement, Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				Aggreement aggreement1=new Aggreement();
				aggreement1.setAggreementId(aggreement.getAggreementId());
				aggreement1.setBookingId(aggreement.getBookingId());
				aggreement1.setAggreementNumber(aggreement.getAggreementNumber());
				aggreement1.setAggreementDate(aggreement.getAggreementDate());

				aggreement1.setFirstApplicantfirstname(aggreement.getFirstApplicantfirstname().toUpperCase());
				aggreement1.setFirstApplicantmiddlename(aggreement.getFirstApplicantmiddlename().toUpperCase());
				aggreement1.setFirstApplicantlastname(aggreement.getFirstApplicantlastname().toUpperCase());

				aggreement1.setFirstApplicantmaidenfirstname(aggreement.getFirstApplicantmaidenfirstname().toUpperCase());
				aggreement1.setFirstApplicantmaidenmiddlename(aggreement.getFirstApplicantmaidenmiddlename().toUpperCase());
				aggreement1.setFirstApplicantmaidenlastname(aggreement.getFirstApplicantmaidenlastname().toUpperCase());

				aggreement1.setFirstApplicantGender(aggreement.getFirstApplicantGender());
				aggreement1.setFirstApplicantMarried(aggreement.getFirstApplicantMarried());
				aggreement1.setFirstApplicantDob(aggreement.getFirstApplicantDob());
				aggreement1.setFirstApplicantmobileNumber1(aggreement.getFirstApplicantmobileNumber1());
				aggreement1.setFirstApplicantmobileNumber2(aggreement.getFirstApplicantmobileNumber2());
				aggreement1.setFirstApplicantSpuseName(aggreement.getFirstApplicantSpuseName());
				aggreement1.setFirstApplicantSpouseDob(aggreement.getFirstApplicantSpouseDob());

				aggreement1.setFirstApplicantAnniversaryDate(aggreement.getFirstApplicantAnniversaryDate());
				aggreement1.setFirstApplicantFatherName(aggreement.getFirstApplicantFatherName().toUpperCase());
				aggreement1.setFirstApplicantEmailId(aggreement.getFirstApplicantEmailId());
				aggreement1.setFirstApplicantPanCardNo(aggreement.getFirstApplicantPanCardNo().toUpperCase());
				aggreement1.setFirstApplicantAadharno(aggreement.getFirstApplicantAadharno());
				aggreement1.setFirstApplicantMotherTonque(aggreement.getFirstApplicantMotherTonque());

				aggreement1.setFirstApplicantPresentAddress(aggreement.getFirstApplicantPresentAddress());
				aggreement1.setFirstApplicantPresentPincode(aggreement.getFirstApplicantPresentPincode());

				aggreement1.setFirstApplicantPermanentaddress(aggreement.getFirstApplicantPermanentaddress());
				aggreement1.setFirstApplicantPermanentPincode(aggreement.getFirstApplicantPermanentPincode());

				aggreement1.setFirstApplicantEducation(aggreement.getFirstApplicantEducation());
				aggreement1.setFirstApplicantOccupation(aggreement.getFirstApplicantOccupation());
				aggreement1.setFirstApplicantOrganizationName(aggreement.getFirstApplicantOrganizationName());
				aggreement1.setFirstApplicantOrganizationType(aggreement.getFirstApplicantOrganizationType());
				aggreement1.setFirstApplicantOrganizationaddress(aggreement.getFirstApplicantOrganizationaddress());
				aggreement1.setFirstApplicantofficeNumber(aggreement.getFirstApplicantofficeNumber());
				aggreement1.setFirstApplicantofficeEmail(aggreement.getFirstApplicantofficeEmail());
				aggreement1.setFirstApplicantIndustrySector(aggreement.getFirstApplicantIndustrySector());
				aggreement1.setFirstApplicantWorkFunction(aggreement.getFirstApplicantWorkFunction());
				aggreement1.setFirstApplicantExperience(aggreement.getFirstApplicantExperience());
				aggreement1.setFirstApplicantIncome(aggreement.getFirstApplicantIncome());

				aggreement1.setSecondApplicantfirstname(aggreement.getSecondApplicantfirstname().toUpperCase());
				aggreement1.setSecondApplicantmiddlename(aggreement.getSecondApplicantmiddlename().toUpperCase());
				aggreement1.setSecondApplicantlastname(aggreement.getSecondApplicantlastname().toUpperCase());

				aggreement1.setSecondApplicantmaidenfirstname(aggreement.getSecondApplicantmaidenfirstname().toUpperCase());
				aggreement1.setSecondApplicantmaidenmiddlename(aggreement.getSecondApplicantmaidenmiddlename().toUpperCase());
				aggreement1.setSecondApplicantmaidenlastname(aggreement.getSecondApplicantmaidenlastname().toUpperCase());

				aggreement1.setSecondApplicantGender(aggreement.getSecondApplicantGender());
				aggreement1.setSecondApplicantMarried(aggreement.getSecondApplicantMarried());
				aggreement1.setSecondApplicantDob(aggreement.getSecondApplicantDob());
				aggreement1.setSecondApplicantmobileNumber1(aggreement.getSecondApplicantmobileNumber1());
				aggreement1.setSecondApplicantmobileNumber2(aggreement.getSecondApplicantmobileNumber2());
				aggreement1.setSecondApplicantSpouseName(aggreement.getSecondApplicantSpouseName());
				aggreement1.setSecondApplicantSpouseDob(aggreement.getSecondApplicantSpouseDob());

				aggreement1.setSecondApplicantAnnivaversaryDate(aggreement.getSecondApplicantAnnivaversaryDate());
				aggreement1.setSecondApplicantFatherName(aggreement.getSecondApplicantFatherName());
				aggreement1.setSecondApplicantEmail(aggreement.getSecondApplicantEmail());
				aggreement1.setSecondApplicantPancardno(aggreement.getSecondApplicantPancardno().toUpperCase());
				aggreement1.setSecondApplicantAadharno(aggreement.getSecondApplicantAadharno());
				aggreement1.setSecondApplicantMotherTongue(aggreement.getSecondApplicantMotherTongue());
				aggreement1.setSecondApplicantRelation(aggreement.getSecondApplicantRelation());


				aggreement1.setSecondApplicantPresentAddress(aggreement.getSecondApplicantPresentAddress());
				aggreement1.setSecondApplicantPresentPincode(aggreement.getSecondApplicantPresentPincode());

				aggreement1.setSecondApplicantPermanentaddress(aggreement.getSecondApplicantPermanentaddress());
				aggreement1.setSecondApplicantPermanentPincode(aggreement.getSecondApplicantPermanentPincode());

				aggreement1.setSecondApplicantEducation(aggreement.getSecondApplicantEducation());
				aggreement1.setSecondApplicantOccupation(aggreement.getSecondApplicantOccupation());
				aggreement1.setSecondApplicantOrganizationName(aggreement.getSecondApplicantOrganizationName());
				aggreement1.setSecondApplicantOrganizationType(aggreement.getSecondApplicantOrganizationType());
				aggreement1.setSecondApplicantOrganizationaddress(aggreement.getSecondApplicantOrganizationaddress());
				aggreement1.setSecondApplicantofficeNumber(aggreement.getSecondApplicantofficeNumber());
				aggreement1.setSecondApplicantofficeEmail(aggreement.getSecondApplicantofficeEmail());
				aggreement1.setSecondApplicantIndustrySector(aggreement.getSecondApplicantIndustrySector());
				aggreement1.setSecondApplicantWorkFunction(aggreement.getSecondApplicantWorkFunction());
				aggreement1.setSecondApplicantExperience(aggreement.getSecondApplicantExperience());
				aggreement1.setSecondApplicantIncome(aggreement.getSecondApplicantIncome());

				aggreement1.setAggreementstatus(aggreement.getAggreementstatus());
				aggreement1.setCreationDate(new Date());
				aggreement1.setUpdateDate(new Date());
				aggreement1.setUserName(aggreement.getUserName());

				aggreementRepository.save(aggreement1);


				List<Booking> bookingList=new ArrayList<Booking>();
				Query query1 = new Query();
				bookingList = mongoTemplate.find(query1.addCriteria(Criteria.where("bookingId").is(aggreement.getBookingId())), Booking.class);

				Flat flat;
				flat = mongoTemplate.findOne(Query.query(Criteria.where("flatId").is(bookingList.get(0).getFlatId())), Flat.class);
				flat.setFlatstatus("Aggreement Completed");
				flatRepository.save(flat);

				Booking booking;
				booking = mongoTemplate.findOne(Query.query(Criteria.where("bookingId").is(aggreement.getBookingId())), Booking.class);
				booking.setBookingstatus("Aggreement Completed");
				bookingRepository.save(booking);


				List<PaymentScheduler> paymentscheduleList=new ArrayList<PaymentScheduler>(); 

				// for Payment Scheduler

				Query query2 = new Query();
				paymentscheduleList = mongoTemplate.find(query2.addCriteria(Criteria.where("projectId").is(bookingList.get(0).getProjectId()).and("buildingId").is(bookingList.get(0).getBuildingId()).and("wingId").is(bookingList.get(0).getWingId())), PaymentScheduler.class);

				String status;
				double percentage=0;
				String schedule="";
				int count=0;
				for(int i=0;i<paymentscheduleList.size();i++)
				{
					status=paymentscheduleList.get(i).getSlabStatus();
					if(status.equals("Completed"))
					{
						percentage=percentage+paymentscheduleList.get(i).getPercentage();
						schedule=schedule+paymentscheduleList.get(i).getPaymentDecription();
						count++;
					}

				}

				try {
					GeneratePaymentScheduler generatepaymentscheduler=new GeneratePaymentScheduler();
					if(count!=0)
					{

						generatepaymentscheduler.setCustomerschedulerId(PaymentCode());
						generatepaymentscheduler.setBookingId(booking.getBookingId());
						generatepaymentscheduler.setInstallmentNumber("1st Installment");
						generatepaymentscheduler.setPaymentDecription(schedule);
						generatepaymentscheduler.setPercentage(percentage);
						generatepaymentscheduler.setCreationDate(booking.getCreationDate());
						generatepaymentscheduler.setUserName(booking.getUserName());
						generatepaymentschedulerRepository.save(generatepaymentscheduler);
					}
					int f=2;
					String installmentNumber="";
					for(int i=count;i<paymentscheduleList.size();i++)
					{
						status=paymentscheduleList.get(i).getSlabStatus();
						if(!status.equals("Completed"))
						{
							installmentNumber=f+"st Installment";
							generatepaymentscheduler.setCustomerschedulerId(PaymentCode());
							generatepaymentscheduler.setBookingId(booking.getBookingId());
							generatepaymentscheduler.setInstallmentNumber(installmentNumber);
							generatepaymentscheduler.setPaymentDecription(paymentscheduleList.get(i).getPaymentDecription());
							generatepaymentscheduler.setPercentage(paymentscheduleList.get(i).getPercentage());
							generatepaymentscheduler.setCreationDate(booking.getCreationDate());
							generatepaymentscheduler.setUserName(booking.getUserName());
							generatepaymentschedulerRepository.save(generatepaymentscheduler);
							f++;
						}

					}
				}
				catch(Exception ww)
				{
					System.out.println(ww);
				}   

				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}

			Query query = new Query();
			List<Project> projectList = GetUserAssigenedProjectList(req,res); 
			List<Booking> bookingList1 = mongoTemplate.find(query.addCriteria(Criteria.where("bookingstatus").ne("Cancel")), Booking.class);


			List<Booking> bookingList=new ArrayList<Booking>();
			Booking booking1=new Booking();

			for(int i=0;i<projectList.size();i++)
			{

				for(int j=0;j<bookingList1.size();j++)
				{
					if(projectList.get(i).getProjectId().equals(bookingList1.get(j).getProjectId()))
					{

						booking1=new Booking();

						booking1.setBookingId(bookingList1.get(j).getBookingId());
						booking1.setEnquiryId(bookingList1.get(j).getEnquiryId());
						booking1.setBookingfirstname(bookingList1.get(j).getBookingfirstname().toUpperCase());
						booking1.setBookingmiddlename(bookingList1.get(j).getBookingmiddlename().toUpperCase());
						booking1.setBookinglastname(bookingList1.get(j).getBookinglastname().toUpperCase());
						booking1.setBookingaddress(bookingList1.get(j).getBookingaddress());
						booking1.setBookingPincode(bookingList1.get(j).getBookingPincode());

						booking1.setBookingEmail(bookingList1.get(j).getBookingEmail());
						booking1.setBookingmobileNumber1(bookingList1.get(j).getBookingmobileNumber1());
						booking1.setBookingmobileNumber2(bookingList1.get(j).getBookingmobileNumber2());
						booking1.setBookingOccupation(bookingList1.get(j).getBookingOccupation());
						booking1.setPurposeOfFlat(bookingList1.get(j).getPurposeOfFlat());

						booking1.setProjectId(bookingList1.get(j).getProjectId());
						booking1.setBuildingId(bookingList1.get(j).getBuildingId());
						booking1.setWingId(bookingList1.get(j).getWingId());
						booking1.setFloorId(bookingList1.get(j).getFloorId());
						booking1.setFlatType(bookingList1.get(j).getFlatType());
						booking1.setFlatId(bookingList1.get(j).getFlatId());
						booking1.setFlatFacing(bookingList1.get(j).getFlatFacing());
						booking1.setFlatareainSqFt(bookingList1.get(j).getFlatareainSqFt());
						booking1.setFlatCostwithotfloorise(bookingList1.get(j).getFlatCostwithotfloorise());
						booking1.setFloorRise(bookingList1.get(j).getFloorRise());
						booking1.setFlatCost(bookingList1.get(j).getFlatCost());
						booking1.setFlatbasicCost(bookingList1.get(j).getFlatbasicCost());
						booking1.setParkingFloorId(bookingList1.get(j).getParkingFloorId());
						booking1.setParkingZoneId(bookingList1.get(j).getParkingZoneId());
						booking1.setAgentId(bookingList1.get(j).getAgentId());

						booking1.setInfrastructureCharge(bookingList1.get(j).getInfrastructureCharge());
						booking1.setAggreementValue1(bookingList1.get(j).getAggreementValue1());
						booking1.setHandlingCharges(bookingList1.get(j).getHandlingCharges());
						booking1.setStampDuty1(bookingList1.get(j).getStampDuty1());
						booking1.setStampDutyPer(bookingList1.get(j).getStampDutyPer());
						booking1.setRegistrationPer(bookingList1.get(j).getRegistrationPer());
						booking1.setRegistrationCost1(bookingList1.get(j).getRegistrationCost1());
						booking1.setGstCost(bookingList1.get(j).getGstCost());
						booking1.setGstAmount1(bookingList1.get(j).getGstAmount1());
						booking1.setGrandTotal1(bookingList1.get(j).getGrandTotal1());
						booking1.setTds(bookingList1.get(j).getTds());
						booking1.setBookingstatus(bookingList1.get(j).getBookingstatus());
						booking1.setUserName(bookingList1.get(j).getUserName());
						booking1.setCreationDate(bookingList1.get(j).getCreationDate());

						bookingList.add(booking1);

					}

				}

			}


			List<Enquiry> enquiryList;

			List<Aggreement> aggreementList = aggreementRepository.findAll();
			List<ExtraCharges> extrachargesList = extrachargesRepository.findAll(); 
			List<CustomerLoanDetails> loanList = customerLoandetailsRepository.findAll();
			List<CustomerFlatCheckList> CustomercheckList = customerflatchecklistRepository.findAll();
			query = new Query();
			enquiryList = mongoTemplate.find(query.addCriteria(Criteria.where("enqStatus").is("Booking")), Enquiry.class);

			model.addAttribute("CustomercheckList",CustomercheckList);
			model.addAttribute("aggreementList",aggreementList);
			model.addAttribute("loanList",loanList);
			model.addAttribute("extrachargesList",extrachargesList);
			model.addAttribute("bookingList", bookingList);
			model.addAttribute("projectList",projectList);
			model.addAttribute("enquiryList", enquiryList);
			return "BookingMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("EditAggreementForm")
	public String EditAggreementForm(@RequestParam("aggreementId") String aggreementId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<Aggreement> aggreementDetails = mongoTemplate.find(query.addCriteria(Criteria.where("aggreementId").is(aggreementId)),Aggreement.class);
			String bookingId=aggreementDetails.get(0).getBookingId();
			Query query1 = new Query();
			List<Booking> bookingDetails = mongoTemplate.find(query1.addCriteria(Criteria.where("bookingId").is(bookingId)),Booking.class);
			try
			{
				query =new Query();
				List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(bookingDetails.get(0).getProjectId())), Project.class);

				query =new Query();
				List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(bookingDetails.get(0).getBuildingId())), ProjectBuilding.class);

				query =new Query();
				List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(bookingDetails.get(0).getWingId())), ProjectWing.class);

				query =new Query();
				List<Floor> floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("floorId").is(bookingDetails.get(0).getFloorId())), Floor.class);

				query =new Query();
				List<Flat> flatDetails = mongoTemplate.find(query.addCriteria(Criteria.where("flatId").is(bookingDetails.get(0).getFlatId())), Flat.class);
				model.addAttribute("projectName",projectDetails.get(0).getProjectName());
				model.addAttribute("buildingName",buildingDetails.get(0).getBuildingName());
				model.addAttribute("wingName",wingDetails.get(0).getWingName());
				model.addAttribute("floortypeName",floorDetails.get(0).getFloortypeName());
				model.addAttribute("flatNumber",flatDetails.get(0).getFlatNumber());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
			model.addAttribute("bookingDetails",bookingDetails);
			model.addAttribute("aggreementDetails", aggreementDetails);
			model.addAttribute("occupationList",getAllOccupationName());
			model.addAttribute("countryList",findAllCountryId());
			return "EditAggreementForm";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/EditAggreementForm", method = RequestMethod.POST)
	public String EditAggreementForm(Model model, @ModelAttribute Aggreement aggreement, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			Query query1=new Query();
			Aggreement aggreement2=mongoTemplate.findOne(query1.addCriteria(Criteria.where("aggreementId").is(aggreement.getAggreementId())), Aggreement.class);
			try
			{

				Aggreement aggreement1=new Aggreement();
				aggreement1.setAggreementId(aggreement.getAggreementId());
				aggreement1.setBookingId(aggreement.getBookingId());
				aggreement1.setAggreementNumber(aggreement.getAggreementNumber());
				aggreement1.setAggreementDate(aggreement.getAggreementDate());

				aggreement1.setFirstApplicantfirstname(aggreement.getFirstApplicantfirstname().toUpperCase());
				aggreement1.setFirstApplicantmiddlename(aggreement.getFirstApplicantmiddlename().toUpperCase());
				aggreement1.setFirstApplicantlastname(aggreement.getFirstApplicantlastname().toUpperCase());

				aggreement1.setFirstApplicantmaidenfirstname(aggreement.getFirstApplicantmaidenfirstname().toUpperCase());
				aggreement1.setFirstApplicantmaidenmiddlename(aggreement.getFirstApplicantmaidenmiddlename().toUpperCase());
				aggreement1.setFirstApplicantmaidenlastname(aggreement.getFirstApplicantmaidenlastname().toUpperCase());

				aggreement1.setFirstApplicantGender(aggreement.getFirstApplicantGender());
				aggreement1.setFirstApplicantMarried(aggreement.getFirstApplicantMarried());
				aggreement1.setFirstApplicantDob(aggreement.getFirstApplicantDob());
				aggreement1.setFirstApplicantmobileNumber1(aggreement.getFirstApplicantmobileNumber1());
				aggreement1.setFirstApplicantmobileNumber2(aggreement.getFirstApplicantmobileNumber2());
				aggreement1.setFirstApplicantSpuseName(aggreement.getFirstApplicantSpuseName());
				aggreement1.setFirstApplicantSpouseDob(aggreement.getFirstApplicantSpouseDob());

				aggreement1.setFirstApplicantAnniversaryDate(aggreement.getFirstApplicantAnniversaryDate());
				aggreement1.setFirstApplicantFatherName(aggreement.getFirstApplicantFatherName().toUpperCase());
				aggreement1.setFirstApplicantEmailId(aggreement.getFirstApplicantEmailId());
				aggreement1.setFirstApplicantPanCardNo(aggreement.getFirstApplicantPanCardNo().toUpperCase());
				aggreement1.setFirstApplicantAadharno(aggreement.getFirstApplicantAadharno());
				aggreement1.setFirstApplicantMotherTonque(aggreement.getFirstApplicantMotherTonque());

				aggreement1.setFirstApplicantPresentAddress(aggreement.getFirstApplicantPresentAddress());
				aggreement1.setFirstApplicantPresentPincode(aggreement.getFirstApplicantPresentPincode());

				aggreement1.setFirstApplicantPermanentaddress(aggreement.getFirstApplicantPermanentaddress());
				aggreement1.setFirstApplicantPermanentPincode(aggreement.getFirstApplicantPermanentPincode());

				aggreement1.setFirstApplicantEducation(aggreement.getFirstApplicantEducation());
				aggreement1.setFirstApplicantOccupation(aggreement.getFirstApplicantOccupation());
				aggreement1.setFirstApplicantOrganizationName(aggreement.getFirstApplicantOrganizationName());
				aggreement1.setFirstApplicantOrganizationType(aggreement.getFirstApplicantOrganizationType());
				aggreement1.setFirstApplicantOrganizationaddress(aggreement.getFirstApplicantOrganizationaddress());
				aggreement1.setFirstApplicantofficeNumber(aggreement.getFirstApplicantofficeNumber());
				aggreement1.setFirstApplicantofficeEmail(aggreement.getFirstApplicantofficeEmail());
				aggreement1.setFirstApplicantIndustrySector(aggreement.getFirstApplicantIndustrySector());
				aggreement1.setFirstApplicantWorkFunction(aggreement.getFirstApplicantWorkFunction());
				aggreement1.setFirstApplicantExperience(aggreement.getFirstApplicantExperience());
				aggreement1.setFirstApplicantIncome(aggreement.getFirstApplicantIncome());

				aggreement1.setSecondApplicantfirstname(aggreement.getSecondApplicantfirstname().toUpperCase());
				aggreement1.setSecondApplicantmiddlename(aggreement.getSecondApplicantmiddlename().toUpperCase());
				aggreement1.setSecondApplicantlastname(aggreement.getSecondApplicantlastname().toUpperCase());

				aggreement1.setSecondApplicantmaidenfirstname(aggreement.getSecondApplicantmaidenfirstname().toUpperCase());
				aggreement1.setSecondApplicantmaidenmiddlename(aggreement.getSecondApplicantmaidenmiddlename().toUpperCase());
				aggreement1.setSecondApplicantmaidenlastname(aggreement.getSecondApplicantmaidenlastname().toUpperCase());

				aggreement1.setSecondApplicantGender(aggreement.getSecondApplicantGender());
				aggreement1.setSecondApplicantMarried(aggreement.getSecondApplicantMarried());
				aggreement1.setSecondApplicantDob(aggreement.getSecondApplicantDob());
				aggreement1.setSecondApplicantmobileNumber1(aggreement.getSecondApplicantmobileNumber1());
				aggreement1.setSecondApplicantmobileNumber2(aggreement.getSecondApplicantmobileNumber2());
				aggreement1.setSecondApplicantSpouseName(aggreement.getSecondApplicantSpouseName());
				aggreement1.setSecondApplicantSpouseDob(aggreement.getSecondApplicantSpouseDob());

				aggreement1.setSecondApplicantAnnivaversaryDate(aggreement.getSecondApplicantAnnivaversaryDate());
				aggreement1.setSecondApplicantFatherName(aggreement.getSecondApplicantFatherName().toUpperCase());
				aggreement1.setSecondApplicantEmail(aggreement.getSecondApplicantEmail());
				aggreement1.setSecondApplicantPancardno(aggreement.getSecondApplicantPancardno().toUpperCase());
				aggreement1.setSecondApplicantAadharno(aggreement.getSecondApplicantAadharno());
				aggreement1.setSecondApplicantMotherTongue(aggreement.getSecondApplicantMotherTongue());
				aggreement1.setSecondApplicantRelation(aggreement.getSecondApplicantRelation());


				aggreement1.setSecondApplicantPresentAddress(aggreement.getSecondApplicantPresentAddress());
				aggreement1.setSecondApplicantPresentPincode(aggreement.getSecondApplicantPresentPincode());

				aggreement1.setSecondApplicantPermanentaddress(aggreement.getSecondApplicantPermanentaddress());
				aggreement1.setSecondApplicantPermanentPincode(aggreement.getSecondApplicantPermanentPincode());

				aggreement1.setSecondApplicantEducation(aggreement.getSecondApplicantEducation());
				aggreement1.setSecondApplicantOccupation(aggreement.getSecondApplicantOccupation());
				aggreement1.setSecondApplicantOrganizationName(aggreement.getSecondApplicantOrganizationName());
				aggreement1.setSecondApplicantOrganizationType(aggreement.getSecondApplicantOrganizationType());
				aggreement1.setSecondApplicantOrganizationaddress(aggreement.getSecondApplicantOrganizationaddress());
				aggreement1.setSecondApplicantofficeNumber(aggreement.getSecondApplicantofficeNumber());
				aggreement1.setSecondApplicantofficeEmail(aggreement.getSecondApplicantofficeEmail());
				aggreement1.setSecondApplicantIndustrySector(aggreement.getSecondApplicantIndustrySector());
				aggreement1.setSecondApplicantWorkFunction(aggreement.getSecondApplicantWorkFunction());
				aggreement1.setSecondApplicantExperience(aggreement.getSecondApplicantExperience());
				aggreement1.setSecondApplicantIncome(aggreement.getSecondApplicantIncome());

				aggreement1.setAggreementstatus(aggreement.getAggreementstatus());
				aggreement1.setCreationDate(aggreement2.getCreationDate());
				aggreement1.setUpdateDate(new Date());
				aggreement1.setUserName(aggreement.getUserName());

				aggreementRepository.save(aggreement1);



				// model.addAttribute("bankStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				//model.addAttribute("bankStatus","Fail");
			}


			Query query = new Query();
			List<Project> projectList = GetUserAssigenedProjectList(req,res); 
			List<Booking> bookingList1 = mongoTemplate.find(query.addCriteria(Criteria.where("bookingstatus").ne("Cancel")), Booking.class);


			List<Booking> bookingList=new ArrayList<Booking>();
			Booking booking=new Booking();

			int i=0,j=0;

			for( i=0;i<projectList.size();i++)
			{

				for( j=0;j<bookingList1.size();j++)
				{
					if(projectList.get(i).getProjectId().equals(bookingList1.get(j).getProjectId()))
					{
						booking=new Booking();

						booking.setBookingId(bookingList1.get(j).getBookingId());
						booking.setEnquiryId(bookingList1.get(j).getEnquiryId());
						booking.setBookingfirstname(bookingList1.get(j).getBookingfirstname().toUpperCase());
						booking.setBookingmiddlename(bookingList1.get(j).getBookingmiddlename().toUpperCase());
						booking.setBookinglastname(bookingList1.get(j).getBookinglastname().toUpperCase());
						booking.setBookingaddress(bookingList1.get(j).getBookingaddress());
						booking.setBookingPincode(bookingList1.get(j).getBookingPincode());

						booking.setBookingEmail(bookingList1.get(j).getBookingEmail());
						booking.setBookingmobileNumber1(bookingList1.get(j).getBookingmobileNumber1());
						booking.setBookingmobileNumber2(bookingList1.get(j).getBookingmobileNumber2());
						booking.setBookingOccupation(bookingList1.get(j).getBookingOccupation());
						booking.setPurposeOfFlat(bookingList1.get(j).getPurposeOfFlat());

						booking.setProjectId(bookingList1.get(j).getProjectId());
						booking.setBuildingId(bookingList1.get(j).getBuildingId());
						booking.setWingId(bookingList1.get(j).getWingId());
						booking.setFloorId(bookingList1.get(j).getFloorId());
						booking.setFlatType(bookingList1.get(j).getFlatType());
						booking.setFlatId(bookingList1.get(j).getFlatId());
						booking.setFlatFacing(bookingList1.get(j).getFlatFacing());
						booking.setFlatareainSqFt(bookingList1.get(j).getFlatareainSqFt());
						booking.setFlatCostwithotfloorise(bookingList1.get(j).getFlatCostwithotfloorise());
						booking.setFloorRise(bookingList1.get(j).getFloorRise());
						booking.setFlatCost(bookingList1.get(j).getFlatCost());
						booking.setFlatbasicCost(bookingList1.get(j).getFlatbasicCost());
						booking.setParkingFloorId(bookingList1.get(j).getParkingFloorId());
						booking.setParkingZoneId(bookingList1.get(j).getParkingZoneId());
						booking.setAgentId(bookingList1.get(j).getAgentId());

						booking.setInfrastructureCharge(bookingList1.get(j).getInfrastructureCharge());
						booking.setAggreementValue1(bookingList1.get(j).getAggreementValue1());
						booking.setHandlingCharges(bookingList1.get(j).getHandlingCharges());
						booking.setStampDuty1(bookingList1.get(j).getStampDuty1());
						booking.setStampDutyPer(bookingList1.get(j).getStampDutyPer());
						booking.setRegistrationPer(bookingList1.get(j).getRegistrationPer());
						booking.setRegistrationCost1(bookingList1.get(j).getRegistrationCost1());
						booking.setGstCost(bookingList1.get(j).getGstCost());
						booking.setGstAmount1(bookingList1.get(j).getGstAmount1());
						booking.setGrandTotal1(bookingList1.get(j).getGrandTotal1());
						booking.setTds(bookingList1.get(j).getTds());
						booking.setBookingstatus(bookingList1.get(j).getBookingstatus());
						booking.setUserName(bookingList1.get(j).getUserName());
						booking.setCreationDate(bookingList1.get(j).getCreationDate());

						bookingList.add(booking);

					}

				}

			}

			List<CustomerFlatCheckList> CustomercheckList = customerflatchecklistRepository.findAll();
			List<Aggreement> aggreementList1 = aggreementRepository.findAll();

			List<Aggreement> aggreementList=new ArrayList<Aggreement>();
			Aggreement aggreement1=new Aggreement();

			for(i=0;i<bookingList.size();i++)
			{
				for(j=0;j<aggreementList1.size();j++)
				{
					if(bookingList.get(i).getBookingId().equals(aggreementList1.get(j).getBookingId()))
					{
						aggreement1=new Aggreement();
						aggreement1.setAggreementId(aggreementList1.get(j).getAggreementId());
						aggreement1.setBookingId(aggreementList1.get(j).getBookingId());
						aggreement1.setFirstApplicantfirstname(aggreementList1.get(j).getFirstApplicantfirstname().toUpperCase());
						aggreement1.setFirstApplicantmaidenmiddlename(aggreementList1.get(j).getFirstApplicantmiddlename().toUpperCase());
						aggreement1.setFirstApplicantlastname(aggreementList1.get(j).getFirstApplicantlastname().toUpperCase());
						aggreement1.setAggreementDate(aggreementList1.get(j).getAggreementDate());
						aggreement1.setAggreementNumber(aggreementList1.get(j).getAggreementNumber());

						aggreementList.add(aggreement1);
					}

				}

			}

			model.addAttribute("CustomercheckList",CustomercheckList);
			model.addAttribute("aggreementList",aggreementList);
			return "AllAgreementList";

		}catch (Exception e) {
			return "login";
		}
	}		





	@RequestMapping("/ViewAggreementForm")
	public String ViewAggreementForm(@RequestParam("aggreementId") String aggreementId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<Aggreement> aggreementDetails = mongoTemplate.find(query.addCriteria(Criteria.where("aggreementId").is(aggreementId)),Aggreement.class);
			String bookingId=aggreementDetails.get(0).getBookingId();
			Query query1 = new Query();
			List<Booking> bookingDetails = mongoTemplate.find(query1.addCriteria(Criteria.where("bookingId").is(bookingId)),Booking.class);

			query =new Query();
			List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(bookingDetails.get(0).getProjectId())), Project.class);

			query =new Query();
			List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(bookingDetails.get(0).getBuildingId())), ProjectBuilding.class);

			query =new Query();
			List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(bookingDetails.get(0).getWingId())), ProjectWing.class);

			query =new Query();
			List<Floor> floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("floorId").is(bookingDetails.get(0).getFloorId())), Floor.class);

			query =new Query();
			List<Flat> flatDetails = mongoTemplate.find(query.addCriteria(Criteria.where("flatId").is(bookingDetails.get(0).getFlatId())), Flat.class);

			model.addAttribute("projectName",projectDetails.get(0).getProjectName());
			model.addAttribute("buildingName",buildingDetails.get(0).getBuildingName());
			model.addAttribute("wingName",wingDetails.get(0).getWingName());
			model.addAttribute("floortypeName",floorDetails.get(0).getFloortypeName());
			model.addAttribute("flatNumber",flatDetails.get(0).getFlatNumber());

			model.addAttribute("bookingDetails",bookingDetails);
			model.addAttribute("aggreementDetails", aggreementDetails);
			model.addAttribute("occupationList",getAllOccupationName());
			model.addAttribute("countryList",findAllCountryId());
			return "ViewAggreementForm";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/getAllCountryList")
	public List<Country> getAllCountryList()
	{
		List<Country> countryList = countryRepository.findAll(new Sort(Sort.Direction.ASC,"countryId"));
		return countryList;
	}


	@RequestMapping(value="/ExportAgreementList", method=RequestMethod.GET)
	public ModelAndView generateExcel(HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		List<Aggreement> aggreementDetails = aggreementRepository.findAll();

		ModelAndView modelAndView = new ModelAndView(new AggreementDetailsExcelView(), "aggreementDetails" , aggreementDetails);

		return modelAndView;
	}


	String CustomerAddress(String countryId, String stateId, String cityId, String locationareaId)
	{
		String customerAdreess="";

		Query query = new Query();
		State stateDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("stateId").is(stateId)),State.class);

		query = new Query();
		City cityDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("cityId").is(cityId)),City.class);

		query = new Query();
		LocationArea locationareaDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("locationareaId").is(locationareaId)),LocationArea.class);

		customerAdreess=stateDetails.getStateName()+" "+cityDetails.getCityName()+" "+locationareaDetails.getLocationareaName();

		customerAdreess = ""+locationareaDetails.getLocationareaName()+","+cityDetails.getCityName()+","+stateDetails.getStateName()+",";
		return customerAdreess;
	}

	String CountryName(String countryId)
	{
		Query query = new Query();
		Country countryDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("countryId").is(countryId)),Country.class);

		return countryDetails.getCountryName();
	}

	String StateName(String stateId)
	{

		Query query = new Query();
		State stateDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("stateId").is(stateId)),State.class);
		return stateDetails.getStateName();
	}
	String CityName(String cityId)
	{
		Query query = new Query();
		City cityDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("cityId").is(cityId)),City.class);

		return cityDetails.getCityName();
	}

	String LocationAreaName(String locationareaId)
	{
		Query query = new Query();
		LocationArea locationareaDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("locationareaId").is(locationareaId)),LocationArea.class);

		return locationareaDetails.getLocationareaName();
	}
	@ResponseBody
	@RequestMapping(value="PrintCustomerAgreement")
	public ResponseEntity<byte[]> PrintCustomerAgreement(@RequestParam("aggreementId") String aggreementId, ModelMap model, HttpServletResponse response)
	{

		int index=0;
		JasperPrint print;
		String customerName="";
		try 
		{	
			Query query = new Query();
			HashMap jmap = new HashMap();
			Collection c = new ArrayList();

			query = new Query();
			Aggreement aggreementDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("aggreementId").is(aggreementId)),Aggreement.class);

			query = new Query();
			Booking bookingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("bookingId").is(aggreementDetails.getBookingId())),Booking.class);

			query = new Query();
			Project projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(bookingDetails.getProjectId())),Project.class);

			query = new Query();
			ProjectBuilding buildingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(bookingDetails.getBuildingId())),ProjectBuilding.class);

			query = new Query();
			ProjectWing wingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(bookingDetails.getWingId())),ProjectWing.class);

			query = new Query();
			Floor floorDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("floorId").is(bookingDetails.getFloorId())),Floor.class);

			query = new Query();
			Flat flatDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("flatId").is(bookingDetails.getFlatId())),Flat.class);

			query = new Query();
			Company companyDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("companyId").is(projectDetails.getCompanyId())),Company.class);


			query =new Query();
			State projectStateDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("stateId").is(projectDetails.getStateId())), State.class);
			query =new Query();
			City projectCityDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("cityId").is(projectDetails.getCityId())), City.class);
			query =new Query();
			LocationArea projectLocationareaDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("locationareaId").is(projectDetails.getLocationareaId())), LocationArea.class);
			/*
			query =new Query();
			City agreementCityDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("cityId").is(aggreementDetails.getFirstApplicantPermanentcityId())), City.class);
			query =new Query();
			LocationArea agreementLocationareaDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("locationareaId").is(aggreementDetails.getFirstApplicantPermanentlocationareaId())), LocationArea.class);
			query =new Query();
			State agreementStateDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("stateId").is(aggreementDetails.getFirstApplicantPermanentstateId())), State.class);
			 */

			String compnayAddress=CustomerAddress(companyDetails.getCountryId(),companyDetails.getStateId(),companyDetails.getCityId(),companyDetails.getLocationareaId());

			NumberToWordConversion run = new NumberToWordConversion();
			String totalinWords = run.convertToIndianCurrency(bookingDetails.getGrandTotal1().toString());



			/*
			 	 for(int i=0;i<bookingList.size();i++)
			 	 {
			 		 if(bookingList.get(i).getBookingId().equals(CustomerPaymentScheduleDetail.get(0).getBookingId()))
			 		 {
			 			customerName =  bookingList.get(i).getBookingfirstname()+" "+bookingList.get(i).getBookingmiddlename()+" "+bookingList.get(i).getBookinglastname();
			 			break;
			 		 }
			 	  }

			 */
			/*
			 	 Query query1 = new Query();
			 	 List<GeneratePaymentScheduler> paymentScheduleDetails = mongoTemplate.find(query1.addCriteria(Criteria.where("bookingId").is(bookingId)), GeneratePaymentScheduler.class);

			 	 for(int i=0;i<paymentScheduleDetails.size();i++)
				 {
			 		    jmap = new HashMap();
						jmap.put("srno",""+(i+1));
					    //jmap.put("srno","1");
						jmap.put("installmentNo",""+paymentScheduleDetails.get(i).getInstallmentNumber());
						jmap.put("paymentDescription", ""+paymentScheduleDetails.get(i).getPaymentDecription());
						jmap.put("per",""+paymentScheduleDetails.get(i).getPercentage());

						c.add(jmap);

						jmap = null;
				 }*/

			JRDataSource dataSource = new JRMapCollectionDataSource(c);
			Map<String, Object> parameterMap = new HashMap<String, Object>();

			Date date = new Date();  
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
			String printDate= formatter.format(date);

			String bookingDate=formatter.format(bookingDetails.getCreationDate());

			String companyNameAndAddress=companyDetails.getCompanyName().toUpperCase() +" \r\n"+companyDetails.getCompanyAddress()+",\n"+compnayAddress+"\n"+companyDetails.getCompanyPincode();
			String companyOwnerNameAndInfo="MR. DHANRAJ KESARIMAL SONIGARA "+"PAN NO. AHYPS3788E \r\n" + 
					"Age about- 60 yrs, Occ.- Business/Agriculturist\r\n" + 
					"R/at- 209, Main Bazar Dehu Road,\r\n" + 
					"Pune- 412 101\r\n";
			String aggName1=aggreementDetails.getFirstApplicantfirstname().toUpperCase() +" "+ aggreementDetails.getFirstApplicantmiddlename().toUpperCase() +" "+aggreementDetails.getFirstApplicantlastname().toUpperCase()+" \r\n"+
					" Occ- "+aggreementDetails.getFirstApplicantOccupation() +" \r\n"+
					"PAN NO- "+aggreementDetails.getFirstApplicantPanCardNo().toUpperCase();

			String aggName2=aggreementDetails.getSecondApplicantfirstname().toUpperCase() +" "+ aggreementDetails.getSecondApplicantmiddlename().toUpperCase() +" "+aggreementDetails.getSecondApplicantlastname().toUpperCase()+ " \r\n"+
					"Occ - "+aggreementDetails.getSecondApplicantOccupation() +" \r\n"+
					"PAN NO- "+aggreementDetails.getSecondApplicantPancardno().toUpperCase();

			String passage1="WHEREAS all that piece and parcel of land as given below situated at village "+projectCityDetails+" within the jurisdiction of Joint- Sub Registrar Haveli Pune and within the limits of Pimpri Chinchwad Municipal Corporation"
					+ " and which is more particularly described in Schedule A was owned by Mr. Sopan Ramkrishna More & 21 others. Mr. Sopan Ramkrishna More executed irrevocable Power of Attorney in favour of Mr. Sanjay Kumar Bhanwarlal Sonigara"
					+ " R/O at : Dapodi, Pune 411 012, which is duly notarised by Notary on 27/08/1995. By virtue of the said Power of Attorney"
					+ " Mr. Sopan Ramkrishna More & 21 others transferred all the rights in the land given below to Mr. Sanjaykumar Bhanwarlal Sonigara." +" "				;

			String passage2="" + 
					"\t Sr.No.     S.No.          Gat No.            Area               Akar \n" + 
					"\t 1              429             1593                41 Aar            0.91 Paisa \n" + 
					"\t 2              429             1596                60 Aar            1.34 Paisa \n" + 
					"\t 3              429             1600                34 Aar            0.72 Paisa \n" + 
					"\t 4              429             1601                04 Aar            0.06 Paisa \n" + 
					"\t 5              429             1597/1             31 Aar            0.72 Paisa \n" + 
					"\t 6              429             1597/2             14 Aar            0.34 Paisa \n" + 
					"\t                                                 Total 184 Aar i.e. 1 H. 84 Aar \n" + 
					"";

			String passage3=""+"WHEREAS  by virtue of said  Power of Attorney Mr. Sanjaykumar Bhanwarlal Sonigara with the consent of Mrs. Changunabai Sopan More & 20 others sold 41 Aar having Aakar 91 paisa out of Gat No. 1593, 50 Aar having "
					+ "Aakar 1.34 Paisa out of Gat No. 1596total area 60 Aar, 24 Aar from 34 Aar "
					+ "out of Gat No. 1600,  4 Aar having Aakar 0.06 Paisa from Gat No. 1601, 21 Aar from total area 31 Aar from Gat No. 1597/1 and 14 Aar from Gat No. 1597/2 total area admeasuring 1 H. 54 Aar "
					+ "to Mr. Dhanraj Kesarimal Sonigara and executed a Sale Deed which is duly registered in the office of Sub-Registrar Haveli No. 8 vide Sr. No. 7009/1996 dated 27/12/1996. \r\n \n"
					+ "WHEREAS by virtue of Power of Attorney Mr. Sanjaykumar Bhanwarlal Sonigara with the consent of Mrs. Changunabai Sopan Morte & 20 others sold remaining 10 Aar from Gat No. 1600 out of 34 Aar, 10 Aar from "
					+ "Gat No. 1597/1 and 10 Aar from Gat No. 1596 total admeasuring area 30 Aar to Mr. Vijaykumar Rameshwardas Agarwal, Sau Neelam Rajendra Agarwal and Mr. Sunil Dharampal Agarwal and executed Sale Deed in "
					+ "favour of them which is duly registered in the office of Sub-Registrar Haveli No. 8 vide Sr. No. 7010/1996 dated 27/12/1996. Their name came on the 7/12 extract of the village vide Mutation Entry No. 5104.\r\n \n" + 
					"WHEREAS Mr.Dhanraj Kesarimal Sonigara sold 21350 Sq. Ft. out of Gat No. 1593 having 41 Aar and 9650 Sq. Ft. out of Gat No. 1596 having 50 Aar to Sidarth Magasvargiya Housing Societyand executed a Sale Deed which is "
					+ "duly registered in the office of Sub-Registrar Haveli No. 8 vide Sr. No. 5578/2001 dated 11/09/2001.\r\n \n" + 
					"WHEREAS Mr. Vijaykumar Rameshwardas Agarwal , Sau Neelam Rajendra Agarwal and Mr. Sunil Dharampal Agarwal sold their share 10 Aar in Gat No. 1596 , 10 Aar in Gat No. 1597/1 and 10 Aar in Gat No. 1600 to Mrs. Shwetali Jitendra Sonigara and executed a Sale Deed which is duly registered in the office of Sub-Registrar Haveli No. 18 vide Sr. No. 11901/2010  dated 31/12/2010 and her name came on the 7/12 extract of the village vide Mutation Entry No. 20887.\r\n" + 

					"\n "+"WHEREAS  Mr. Dhanraj Kesarimal Sonigara and Mrs. Shwetali Jitendra Sonigara after consultation of better development of the plots have agreed to exchange their share in plots.  Mr. Dhanraj Kesarimal Sonigara transferred his share in Gat No. 1593 have an area of 21.16 Aar to Mrs. Shwetali Jitendra Sonigara in the exchange of her share i.e. 10 Aar in Gat No. 1600, 10 Aar in Gat No. 1597/1 and 200 Sq. Mtrs. in Gat No. 1596 by executing a Exchange Deed which is duly registered in the office of Sub-Registrar Haveli No. 17 vide Sr. No. 2739/2013 dated 13/03/2013.\r\n" + 
					"\n "+"WHEREAS Mr. Dhanraj Kesarimal Sonigara and Mrs. Shwetali Jitendra Sonigara amalgamated the plot containing Gat No. 1593, 1596, 1597/1, 1597/2, 1600  and  1601 in one plot as per mutual understanding. \r\n" + 
					"\r\n " + 
					"WHEREAS Mr. Dhanraj Kesarimal Sonigara and Mrs. Shwetali Jitendra Sonigara submitted an application and lay out plan regarding Gat No. 1593 Part, 1596, 1597/1, 1597/2, 1600 and 1601 for an area 8891.22 Sq. Mtrs. "
					+ "from that amalgamated plots and the same was sanctioned vide B.P./Chikhli/20/2012  dated  03/08/2012 and the same was revised by commencement certificate No. B.P./ Chikhli/32/2013 dated 7/10/2013 same is again"
					+ " revised vide Commencement Certificate No. B.P./Chikhli/75/2014 dated 24/12/2014. Similarly put up an application before Collector of Pune for the permission of N.A. order of Gat No. 1593 Part, 1596, 1597/1, "
					+ "1597/2, 1600 and 1601of village Chikhali and N.A. permission granted by Collector of Pune by it�s order bearing No. PMH/N.A./SR/808/12 dated 9/05/2013.\r\n" + 
					"\n"+"WHEREAS  Mrs. Shwetali Jitendra Sonigara sold the property admeasuring about 91.22 Sq. Mtrs. out of her share in Gat No. 1596 to M/s. D. K. Associates through its Proprietor Mr. Dhanraj Kesrimal Sonigara "
					+ "for better development of the plots.  The said Sale Deed is duly registered in the office of Sub-Registrar Haveli No. 24 vide Sr. No. 815 /2013 dated 7/12/2013.\r\n" + 
					"WHEREAS in this manner Mr. Dhanraj Kesarimal Sonigara became the owner of the land admeasuring about 4391.22 Sq. Mtrs. out of Gat No. 1596, admeasuring about out 3100 Sq. Mtrs out of Gat No. 1597/1 and "
					+ "admeasuring about 1400 Sq. Mtrs. out of Gat No. 1597/2 which comes to total area admeasuring about 8891.22 Sq. Mtrs.. \r\n" + 
					"\r\n" + 
					"WHEREAS this multistoried building is being constructed solely by Mr. Dhanraj Kesarimal Sonigara in the name of M/s. D.K. Associates having project name as �BLUE DICE�. On the plot having total area of 8891.22 Sq. "
					+ "Mtrs. out of Gat No. 1596 (Part), Gat No. 1597/1 and Gat No. 1597/2 as per sanctioned plan and out of the sanctioned area of N.A..  \r\n" + 

					"\n"+"AND WHEREAS M/s. D.K. Associates through it�s proprietor Mr. Dhanraj Kesarimal Sonigara entered into the standard agreement with an Architect namely Mr. Abhijeet Bhaskar Konde having it�s office "
					+ "at :-  Pune 411 005. Later on the Promoter/Vendor has obtained NOC from Architect Mr. Abhijeet Bhaskar Konde and appointed Architect namely Mr.Rishikesh Bhutkar having it�s office "
					+ "at :- Kasba Peth, Manik Chowk, Shimpi Aali, Pune - 411011. The Promoter has entered into standard agreement with Architect Mr. Rishikesh Bhutkar duly registered with the Council of Architects"
					+ " and such agreement is as per the agreement prescribed by the Council of Architects of India in respect of the property mentioned herein under schedule of the property. Wherein M/s. D.K. Associates appointed"
					+ " Mr. Rahul Kapse having his office :  Pimpri, Pune 411 018 as a Structural Engineer who accepted the professional supervision of the Architect and the Structural Engineer till completion of project so decide. \r\n"
					+ "\n"+"AND WHEREAS by virtue of the Sale Deed the Promoter / vendor alone has the sole and exclusive right to sell the units in the said buildings to be constructed by the Promoter /vendor on the said property and "
					+ "to enter into agreement/s for sale with the suitable purchaser/s of the units and receive sale price in respect of them. \r\n" + 
					"\r\n" + 
					"AND WHEREAS Promoter/ Vendor has decided to construct multi-storied residential units and commercial shops and offices. And to form an Association of Apartments/Society of all flat purchasers of the buildings/project as the case may be. \r\n \n" + 

					"AND WHEREAS Promoter appointed Adv. Prashant Jagtap who took search of the record and issued Title Certificate to that effect. The Promoter later on change his Advocate and appointed Adv. Savita Bapu Avaghade "
					+ "for further search of record.\r\n " + 

					"\n"+"AND WHEREAS as per 7/12 extract Promoter has ownership rights and Development rights in respect of the land admeasuring about 8891.22 Sq. Mtrs.\r\n" + 

					"\n"+"AND WHEREAS the said sanctioned lay out in respect of the said land hereinafter referred to as �the said sanctioned lay out�.\r\n " + 

					"\n"+"AND WHEREAS the said sanctioned layout consists D1and D2 Building. \r\n" + 

					"\n"+"AND WHEREAS the Promoter has proposes to develop the said land in Phases as per sanctioned and proposed plans by the PCMC time to time\r\n" + 

					"\n"+"AND WHEREAS the said �Phase I i.e. Building D1 & D2  of Project� to be implemented by the Promoter on the said part of land of the sanction layout shall be as under :\r\n \n" + 
					"� Phase I- D1- building having Parking Floor + 11 floor containing residential 73 flats. \r\n \n" + 
					"� Phase I � D2 building having Parking Floor + 11 floor containing residential 73 flats. \r\n \n" + 
					""+"The FSI available as per rules will be consumed on the said Phase I.\r\n \n" + 
					"AND WHEREAS the construction of D1 is complete and D2 building is partly complete. The PCMC has issued Part Completion Certificate vide No. 270/2017 on  26/05/2017 for 73 flats in building D1 and 53 flats in building D2.\r\n" + 

					"\n"+"AND WHEREAS the Promoter has to construct 43 flats in D3 building and complete the remaining 20 flats of D2 building. \r\n" + 
					"\n"+"AND WHEREAS Phase I & Phase II shall have open space and internal roads which shall   be common for both the Phases and the amenities provided in it shall also remain common. \r\n" + 
					"\n"+"AND WHEREAS developer has proposed Phase II i.e. building D3 on the remaining part of land by uploading T.D.R. and F.S.I. permissible under the rules of the PCMC on prevailing date. Proposed Phase II i.e. building D3 shall consist of Parking + 11 Upper floors consisting 43 residential flats. \r\n" + 
					"\n"+"Thus the total project shall have 189 residential flats. \r\n" + 
					"\n"+"AND WHEREAS  the Promoter put up lay out plan of the proposed Phase II i.e. building No. D3 to Pimpri Chinchwad Municipal Corporation and got sanction vide Commencement Certificate No. BP/Chikhali/103/2017 Dated  04/02/2017\r\n" + 
					"\n"+"AND WHEREAS the Promoter has procured all the sanctions under the provisions of law from different departments, applicable to the development of the said land and implementation of the said project Phase II thereon.\r\n" + 
					"\n"+"AND WHEREAS in pursuant to laws under the Real Estate (Regulation and Development) Act 2016 the Promoter has registered the Project with the Real Estate Regulatory Authorityvide registration No. P52100004228 .on 09/08/2017; authenticated copy is attached in Annexure �F�;\r\n" + 
					"\n"+"AND WHEREAS the Promoter has displayed all the relevant details required as per Act on the website Nomaharerait.mahaonline.gov.in allotted to him and also regularly update the same as per RERA rules. \r\n" + 
					"\n"+"AND WHEREAS the Allottee is offered an Apartment bearing number 301 on the 3rd floor, ( herein after referred to as the said �Apartment�) in the D3 Building (herein after referred to as the said �Building�) being constructed in the D3  phase of the said project, known as BLUE DICE by the Promoter\r\n" + 
					"\n"+"AND WHEREAS the Allottee demanded from the Promoter and the Promoter has given inspection to the Allottee of all the documents of title relating to the said land and the plans, designs and specifications prepared by the Promoter's Architects Messrs Rishikesh Bhutkar and of such other documents as are specified under the Real Estate (Regulation and Development) Act 2016 (hereinafter referred to as \"the said Act\") and the rules made thereunder; and also provided draft copy of agreement for sale to the allotee.\r\n" + 
					"\n"+"AND WHEREAS the copies of Certificate of Title issued by the attorney at law or advocate of the Promoter, copies of Property card or extract of Village Forms VI and VII and XII or any other relevant revenue record showing the nature of the title of the Promoter to the said land on which the Apartments are constructed or are to be constructed have been annexed hereto and marked Annexure 'A' and 'B', respectively. AND WHEREAS the copies of the plans of the Layout as approved by the concerned Local Authority have been annexed hereto and marked Annexure C.\r\n" + 
					"\n"+"AND WHEREAS the copies of the plans of the Apartment agreed to be purchased by the Allottee, as proposed by the Promoter and as approved by the concerned local authority have been annexed hereto and marked Annexure D. Similarly the specification of the Apartment agreed to be purchased by the Allottee is particularly described in the Schedule I written hereunder. \r\n" + 
					"\n"+"AND WHEREAS the copies of Commencement as issued by the concerned local authority have been annexed hereto and marked Annexure E. \r\n" + 
					"\n"+"AND WHEREAS the Promoter has got some of approvals from the concerned local authority the plans, the specifications, elevations, sections and of the said building D3 and shall obtain the balance approvals from various authorities from time to time, so as to obtain Building Completion Certificate or Occupation Certificate of the said Building .\r\n" + 
					"\n"+"AND WHEREAS while sanctioning the said plans concerned local authority and/or Government has laid down certain terms, conditions, stipulations and restrictions which are to be observed and performed by the Promoter while developing the said land and the said building and upon due observance and performance of which only the completion or occupation certificates in respect of the said building/s shall be granted by the concerned local authority. \r\n" + 
					"\n"+"AND WHEREAS the Promoter has accordingly commenced construction of the said building/s in accordance with the said sanctioned plans.\r\n" + 
					"\n"+"AND WHEREAS the Allottee has applied to the Promoter for allotment of an Apartment No. "+wingDetails.getWingName()+" on "+floorDetails.getFloortypeName()+" floor situated in the building Blue Dice of Phase D3being constructed by him. \r\n"+
					"\n"+ "AND WHEREAS the carpet area of the said Apartment is _____ square meter  and \"carpet area\" means the net usable floor area of an apartment, excluding the area covered by the external walls, areas under services shafts, exclusive balcony appurtenant to the said Apartment for exclusive use of the Allottee or verandah area and exclusive open terrace area appurtenant to the said Apartment for exclusive use of the Allottee, but includes the area covered by the internal partition walls of the apartment.\r\n" + 
					"\n"+ "AND WHEREAS the parties relying on confirmation, representations and assurance of each other to faithfully abide by all the terms, conditions and stipulations contained in this Agreement and all applicable laws, are now willing to enter into his Agreement on the terms and conditions appearing hereinafter;\r\n" + 
					"\n"+ "AND WHEREAS under section 13 of the said Act the Promoter is required to execute a written Agreement for sale of said Apartment to the Allottee, being in fact these presents and also to register said Agreement under the Registration Act, 1908.In accordance with the terms and conditions set out in this Agreement and as mutually agreed upon by and between the parties, the Promoter hereby agrees to sell and the Allottee hereby agrees to purchase the Apartment .\r\n" + 
					"\n"+ "NOW THIS AGREEMENT WITNESSETH AND IT IS HEREBY AGREED BY AND BETWEEN THE PARTIES HERETO AS FOLLOWS:- \r\n" + 
					"\n"+ "1. The Promoter shall construct the said building/s consisting of Parking on ground, and on upper ground floors on the said land in accordance with the plans, designs and specifications as approved by the concerned local authority from time to time. \r\n" + 
					"\n"+ "Provided that the Promoter shall have to obtain prior consent in writing to the Allottee in respect of variations or modifications which may adversely affect the Apartment of the Allottee except any alteration or addition required by any Government authorities or due change in law.    \r\n" + 

					"\n"+ "1(a)  The Allottee hereby agrees to purchase from the Promoter and the Promoter hereby agrees to sell to the Allottee Flat No."+flatDetails.getFlatNumber()+" carpet area admeasuring "+flatDetails.getCarpetArea()+" sq. mtrs. on "+floorDetails.getFloortypeName()+" Floor in the  "+wingDetails.getWingName()+" Buildingalong with (hereinafter referred to as \"the Flat \") as shown in the Floor plan thereof hereto annexed and marked Annexure 4 along with right to use the same level Terrace admeasuring about"+flatDetails.getTerraceArea()+"sq. mtrs ,"
					+ " Dry Tarrace "+flatDetails.getDryterraceArea()+" sq.mtrs and encl. Balcony "+flatDetails.getBalconyArea()+" sq.mtr for the consideration of Rs. "+bookingDetails.getGrandTotal1()+"/- (Rupees "+totalinWords+" ) being proportionate price of the common areas, M.S.E.D.C.L. charges, society formation and facilities appurtenant to the premises, (including all the charges applicable as per rules of RERA) but excluding expenses for stamp duty, registration fees, CGST & SGST or any other taxes levied by State/Central/Local/Gov./Semi Govt. "
					+ "or any competent authority which shall be paid by purchaser separately. The nature, extent and description of the common /limited common areas and facilities which are more particularly described in the Schedule annexed here with .\r\n"

					+ "\n"+ "1(b) The Allottee has paid Rs. "+bookingDetails.getGrandTotal1()+"/- (Rupees "+totalinWords+") by "+bookingDate+" dated _______drawn on ________ bank as Earnest Money Deposit as application fee and the same will remain with Promoter as advance deposit interest free unsecured loan which will be adjusted in flat cost after execution of Agreement and hereby agrees to pay to the Promoter the balance amount of purchase consideration in the following manner :- \r\n" + 
					"\n"+ "10% On booking\r\n" + 
					"\n"+ "20% On execution of agreement\r\n" + 
					"\n"+ "25% On Casting of Plinth\r\n" + 
					"\n"+ "5 % On Casting of Fourth Slab\r\n" + 
					"\n"+ "10% On Casting of Eighth Slab\r\n" + 
					"\n"+ "10% On Casting of Twelfth Slab\r\n" + 
					"\n"+ "10% On Casting staircase, Lift wells, Lobbies upto floor level\r\n" + 
					"\n"+ "5 % On Casting External Plumbing , Plaster Elevation, Terrace. \r\n" + 
					"\n"+ "10% On completion of Lift, water pumbps, electrical fitting, electro mechanical & environment Requirement, Entrance Lobby, Paving of areas  \r\n" + 
					"\n"+ "5 % at the time of Possession\r\n" + 

					"\n"+ "In the case of Allottee purchases flat in ready possession position or after completion certificate obtained from PCMC, above payment schedule will not apply and he has to pay total consideration within 30 days from the date of agreement.  \r\n" + 

					"\n"+ "1(c) The Total Price above excludes Taxes (consisting of tax paid or payable by the Promoter by way of CGST & SGST or any other similar taxes which may be levied, in connection with the construction of and carrying out the Project and levied by State/Central/Local/Gov./Semi Govt. or any competent authority payable by the Promoter up to the date of handing over the possession of the said Flat.  \r\n" + 
					"\n"+ "1(d) The Total Price is escalation-free, save and except escalations/increases, due to increase on account of development charges payable to the competent authority and/or any other increase in charges which may be levied or imposed by the competent authority Local Bodies/Government from time to time. The Promoter undertakes and agrees that while raising a demand on the Allottee for increase in development charges, cost, or levies imposed by the competent authorities etc., the Promoter shall enclose the said notification/order/rule/regulation published/issued in that behalf to that effect along with the demand letter being issued to the Purchaser, which shall only be applicable on subsequent payments. \r\n" + 
					"\n"+ "1(e) The Promoter shall confirm the final carpet area that has been allotted to the Allottee after the construction of the Building is complete and the completion certificate is granted by the competent authority, by furnishing details of the changes, if any, in the carpet area.  Allottee has been made aware and has agreed that there may be variation in area all the three sides due to skirting and plaster not more than 5 % in this case agreed consideration will not change. If the variation is more than 5 % then the total price payable for the carpet area shall be recalculated upon confirmation by the Promoter. If there is any reduction in the carpet area beyond the defined limit then Promoter shall refund the excess money paid by Allottee within forty-five days with annual interest at the rate specified in the Rules, from the date when such an excess amount was paid by the Allottee. If there is any increase in the carpet area allotted to Allottee, the Promoter shall demand that from the Allottee as per the next milestone of the Payment Plan. \r\n" + 
					"\n"+ "1(f) The Allottee authorizes the Promoter to adjust/appropriate all payments made by him/her under any head(s) of dues against lawful outstanding, if any, in his/her/them name as the Promoter may in its sole discretion deem fit and the Allottee undertakes not to object/demand /direct the Promoter to adjust his payments in any manner. \r\n" + 
					"\n"+ "1(g) If the cost exceeds Rs.. 50,00,000/- or above then Allottee have to make payment after deducting of 1% T.D.S. u/s 194-1A and submit certificate of such deductions made at the time of taking possession. If Allottee fails to do so he has to pay an equal amount to the Promoter as deposit for the period of 4 months during which he has to submit the required certificate. In case he do not submit Certificate within the stipulated time period, Promoter shall be at liberty to utilize the amount for paying income tax.\r\n" + 
					"\n"+ "2.1  The Promoter hereby agrees to observe, perform and comply with all the terms, conditions, stipulations and restrictions if any, which may have been imposed by the concerned local authority at the time of sanctioning the said plans or thereafter and shall, before handing over possession of the said flat to the Allottee, obtain from the concerned local authority occupation and/or completion certificates in respect of the said Flat. \r\n" + 
					"\n"+ "2.2  Time is the essence for the Promoter as well as for the Purchaser. The Promoter shall abide by the time schedule for completing the project and handing over the said Flat to the Purchaser and the common areas to the association of the Purchasers after receiving the occupancy certificate or the completion certificate or both, as the case may be. Similarly, the Purchaser shall make timely payments of the installment and other dues payable by him/her and meeting the other obligations under the Agreement subject to the simultaneous completion of construction by the Promoter as provided in clause 1 (b) herein above. (�Payment Plan�). \r\n" + 
					"\n"+ "3.   The Promoter hereby declares that the Floor Space Index available as on date in respect of the said land is as per development rules of the corporation and Promoter has planned to utilize floating Floor Space Index as permission by Competent authority by availing of TDR or FSI available on payment of premiums or FSI available as incentive FSI by implementing various scheme as mentioned in the Development Control Regulation or based on expectation of increased FSI which may be available in future on modification to Development Control Regulations, which are applicable to the said Project. The Promoter has disclosed the Floor Space Index as proposed to be utilized by him on the said Land in the said Project and Purchaser has agreed to purchase the said Flat based on the proposed construction and sale of flats to be carried out by the Promoter by utilizing the proposed FSI and on the understanding that the declared proposed FSI shall belong to Promoter only. In case T.D.R. purchased by floating F.S.I. is found in excess Promoter shall be at liberty to utilize the same in his own discretion. The Purchaser shall not raise any objection. \r\n" + 
					"\n"+ "4.1  If the Promoter fails to abide by the time schedule for completing the project and handing over the Apartment to the Allottee, the Promoter agrees to pay to the Allottee, who does not intend to withdraw from the project, interest as specified in the Rule, on all the amounts paid by the Allottee, for every month of delay , till the handing over of the possession. The Allottee agrees to pay to the Promoter, interest as specified in the Rule, on all the delay payment which become due and payable  by the Allottee to thePromoter under the terms of this Agreement from the date the said amount is payable by the Allottee(s) to the Promoter.\r\n" + 
					"\n"+ "4.2  Without prejudice to the right of Promoter to charge interest in terms of sub clause 4.1 above, on the Allottee committing default in payment on due date of any amount due and payable by the Allottee to the Promoter under this Agreement (including his/her proportionate share of taxes levied by concerned local authority and other outgoings) and on the allottee committing three defaults of payment of instalments, the Promoter shall t his own option, may terminate this Agreement:\r\n" + 
					"\n"+ "Provided that, the Promoter shall give notice of fifteen days in writing to the Allottee, by Registered Post AD at the address provided by the Allottee and mail at the e-mail address provided by the Allottee, of his intention to terminate this Agreement and of the specific breach or breaches of terms and conditions in respect of which it is intended to terminate the Agreement. If the Allottee fails to rectify the breach or breaches mentioned by the Promoter within the period of notice then at the end of such notice period, Promoter shall be entitled to terminate this Agreement.\r\n" + 
					"\n"+ "Provided further that upon termination of this Agreement as aforesaid, the Promoter shall refund to the Allottee (Subject to adjustment and recovery of any agreed liquidated damages or any other amount which may payable to Promoter) within a period of thirty days of the termination of sale consideration of the Apartment which may till then have been paid by the Allottee to the Promoter without any interest.  \r\n" + 
					"\n"+ "5.  The fixtures and fittings with regard to the flooring and sanitary fittings and amenities like one or more lifts with particulars like brand, or price range (if unbranded)to be provided by the Promoter in the said building and the Apartment are those that are set out in Annexure 'E' annexed hereto.\r\n" + 
					"\n"+ "6.  The Promoter shall give possession of the remaining Apartment of D2 building to the Allottee on or before 14/01/2020 and the Promoter shall give possession of the Apartment of D3 building to the Allottee on or before 14/01/2020 If the Promoter fails or neglects to give possession of the Apartment to the Allottee on account of reasons beyond his control and of his agents by the aforesaid date then the Promoter shall be liable on demand to refund to the Allottee the amounts already received by him in respect of the Apartment with interest at the same rate as may mentioned in the clause 6 herein above from the date the Promoter received the sum till the date the amounts and interest thereon is repaid,\r\n" + 
					"\n"+ "Provided that the Promoter shall be entitled to reasonable extension of time for giving delivery of Apartment on the aforesaid date, if the completion of building in which the Apartment is to be situated is delayed on account of � \r\n" + 
					"\n"+ "(i)  non-availability of steel, other building material, water or electric supply ; \r\n" + 
					"\n"+ "(ii) war, civil commotion or act of God ; \r\n" + 
					"\n"+ "(iii)any notice, order, rule, notification of the Government and/or other public or competent authority. \r\n" + 

					"\n "+"7.1 PROCEDURE FOR TAKING POSSESSION \r\n"+
					"The Promoter, upon obtaining the Occupancy Certificate/Completion Certificate from the competent authority shall offer in writing the possession of the Apartment, to the Allottee in terms of this Agreement to be taken within 3 (three) months from the date of issue of such notice and the Promoter shall give possession of the Apartment to the Allottee. The Promoter agrees and undertakes to indemnify the Allottee in case of failure of fulfillment of any of the provisions, formalities, documentation on part of the Promoter. The Allottee agree(s) to pay the maintenance charges as determined by the Promoter or association of allottees, as the case may be. The Promoter on its behalf shall offer the possession to the Allottee in writing within 7 days of receiving the Occupancy Certificate/Completion Certificate of the Project. \r\n" + 
					"\n"+"7.2 The Allottee shall take possession of the Apartment within 15 days of the Promoters giving written notice to the Allottee intimating that the said Apartments are ready for use and occupation: \r\n" + 
					"\n"+"7.3 FAILURE OF ALLOTTEE TO TAKE POSSESSION OF APARTMENT: \r\n" + 
					"Upon receiving a written intimation from the Promoter as per clause 7.1, the Allottee shall take possession of the Apartment from the Promoter by executing necessary indemnities, undertakings & such other documentation as prescribed in this Agreement, and the Promoter shall give possession of the Apartment to the allottee. In case the Allottee fails to take possession within the time provided in clause 7.1 such Allottee shall continue to be liable to pay maintenance charges as applicable. \r\n" + 
					"\n"+"7.4 If within a period of five years from the date of handing over the Apartment to the Allottee, the Allottee brings to the notice of the Promoter any defect in the Apartment or the building in which the Apartment are situated or any defect on account of workmanship, quality or provision of service, then wherever possible such defects shall be rectified by the Promoter at his own cost and in case it is not possible to rectify such defects, then the Allottee shall be entitled to receive from the Promoter compensation for such defect in the manner as provided under the Act.  But the Promoter shall not be held responsible for any colour variation, size variation in the marble, ceramic or granite.   \r\n" + 
					"\n"+"8.  The Allottee shall use the Apartment or any part thereof or permit the same to be used only for purpose of residence. He shall use the parking space only for purpose of keeping or parking the Allottee's owned vehicle. \r\n" + 
					"\n"+"9.  The Allottee along with other allottee(s)s of Apartments in the building shall join in forming and registering the Society or a Limited Company to be known by such name as the Promoter may decide and for this purpose also from time to time sign and execute the application for registration and/or membership and the other papers and documents necessary for the formation and the registration of the Society or Limited Company and for becoming a member, including the bye-laws of the proposed Society and duly fill in, sign and return to the Promoter within seven days of the same being forwarded by the Promoter to the Allottee, so as to enable the Promoter to register the common organisation of Allottee. No objection shall be taken by the Allottee if any changes or modifications are made in the draft bye-laws, or the Memorandum and/orArticles of Association, as may be required by the Registrar of Co-operative Societies or the Registrar of Companies, as the case may be, or any other Competent Authority. \r\n" + 
					"\n"+"9.1 The Promoter shall, within three months of registration of the Society or Limited Company, as aforesaid, cause to be transferred to the society or Limited Company all the right, title and the interest of the Vendor/Lessor /Original Owner/Promoter and/or the owners in the said structure of the Building or wing in which the said Apartment is situated. \r\n" + 
					"\n"+"9.2 The Promoter shall, within three months of registration of the Federation/apex body of the Societies or Limited Company, as aforesaid, cause to be transferred to the Federation/Apex body all the right, title and the interest of the Vendor /Lessor/Original Owner/Promoter and/or the owners in the said land on which the building with multiple wings or buildings are constructed or after completion of the whole project. \r\n" + 
					"\n"+"9.3 Within 15 days after notice in writing is given by the Promoter to the Allottee that the Apartment is ready for use and occupation, the Allottee shall be liable to bear and pay the proportionate share (i.e. in proportion to the floor area of the Apartment) of outgoings in respect of the said land and Building/s namely local taxes, betterment charges or such other levies by the concerned local authority and/or Government water charges, insurance, common lights, repairs and salaries of clerks bill collectors, chowkidars, sweepers and all other expenses necessary and incidental to the management and maintenance of the said land and building/s. Until the Society or Limited Company is formed and the said structure of the building/s or wings is transferred to it, the Allottee shall pay to the Promoter such proportionate share of outgoings as may be determined. The Allottee further agrees that till the Allottee's share is so determined the Allottee shall pay to the Promoter provisional a lump sum contribution of Rs. 60,000/- for 36 months towards the outgoings. The amounts so paid by the Allottee to the Promoter shall not carry any interest and remain with the Promoter until a conveyance/assignment of lease of the structure of the building or wing is executed in favour of the society or a limited company as aforesaid. On such conveyance/assignment of lease being executed for the structure of the building or wing the aforesaid deposits (less deduction provided for in this Agreement) shall be paid over by the Promoter to the Society or the Limited Company, as the case may be. \r\n" + 
					"\n"+"10. The Total aggregate amount of the apartment including share money, application entrance fee of the Society or Limited Company/Federation/ Apex body, fees for formation and registration of the Society or Limited Company/Federation/ Apex body, proportionate share of taxes and other charges/levies in respect of the Society or Limited Company/Federation/ Apex body, Deposit towards Water, Electric, and other utility and services connection charges, deposits of electrical receiving and Sub Station provided in Layout and price of covered parking spaces and all other charges applicable as per rules of RERA, except Rs. 60,000/-  for 36  months towards provisional lumsum contribution towards outgoings of Society or Limited Company/Federation/ Apex body. \r\n" + 
					"\n"+"11. The Allottee shall pay to the Promoter proportionate share of the amount for meeting all legal costs, charges and expenses, including professional costs of the Attorney-at-Law/Advocates of the Promoter in connection with formation of the said Society, or Limited Company, or Apex Body or Federation and for preparing its rules,regulations and bye-laws and the cost of preparing and engrossing the conveyance or assignment of lease. \r\n" + 
					"\n"+"12. At the time of registration of conveyance or Lease of the structure of the building or wing of the building, the Allottee shall pay to the Promoter, the Allottees' share of stamp duty and registration charges payable, by the said Society / Limited Company/ Apex Body /Federation on such conveyance or lease or any document or instrument of transfer in respect of the structure of the said Building /wing/land to be executed in favour of the said Society / Limited Company/ Apex Body /Federation. \r\n" + 

					"\n"+"13. REPRESENTATIONS AND WARRANTIES OF THE PROMOTER \r\n"+
					"The Promoter hereby represents and warrants to the Allottee as follows:\r\n" + 
					"\n"+"i.  The Promoter has clear and marketable title with respect to the said Land; as declared in the title report annexed to this agreement and has the requisite rights to carry out development upon the said Land and also has actual, physical and legal possession of the said Land for the implementation of the Project;\r\n" + 
					"\n"+"ii. The Promoter has lawful rights and requisite approvals from the competent Authorities to carry out development of the Project and shall obtain requisite approvals from time to time to complete the development of the project; \r\n" + 
					"\n"+"iii.There are no encumbrances upon the said Land or the Project except those disclosed in the title report; \r\n" + 
					"\n"+"iv. There are no litigations pending before any Court of law with respect to the said Land or Project except those disclosed in the title report; \r\n" + 
					"\n"+"v.  All approvals, licenses and permits issued by the competent authorities with respect to the Project, said Land and said building/wing are valid and subsisting and have been obtained by following due process of law. Further, all approvals, licenses and permits t o b e issued by the competent authorities with respect to the Project, said Land and said building/wing shall be obtained by following due process of law and the Promoter has been and shall, at all times, remain to be in compliance with all applicable laws in relation to the Project, said Land, Building/wing and common areas; \r\n" + 
					"\n"+"vi. The Promoter has the right to enter into this Agreement and has not committed or omitted to perform any act or thing, whereby the right, title and interest of the Allottee created herein, may prejudicially be affected; \r\n" + 
					"\n"+"vii.The Promoter has not entered into any agreement for sale and/or development agreement or any other agreement / arrangement with any person or party with respect to the said Land, including the Project and the said Apartment which will, in any manner, affect the rights of Allottee under this Agreement; \r\n" + 
					"\n"+"viii.The Promoter confirms that the Promoter is not restricted in any manner whatsoever from selling the said [Apartment/Plot]to the Allottee in the manner contemplated in this Agreement;\r\n" + 
					"\n"+"ix. At the time of execution of the conveyance deed of the structure to the association of allottees the Promoter shall handover lawful, vacant, peaceful, physical possession of the common areas of the Structure to the Association of the Allottees; \r\n" + 
					"\n"+"x. The Promoter has duly paid and shall continue to pay and discharge undisputed  d governmental dues, rates, charges and taxes and other monies, levies, impositions, premiums, damages and/or penalties and other outgoings, whatsoever, payable with respect to the said project to the competent Authorities upto the date of completion certificate; \r\n" + 
					"\n"+"xi.No notice from the Government or any other local body or authority or any legislative enactment, government ordinance, order, notification (including any notice for acquisition or requisition of the said property) has been received or served upon the Promoter in respect of the said Land and/or the Project except those disclosed in the title report. \r\n" + 

					"\n"+"14. The Allottee/s or himself/themselves with intention to bring all persons into whosoever is hands the Apartment may come, hereby covenants with the Promoter as follows :-\r\n" + 
					"\n"+"i. To maintain the Apartment at the Allottee's own cost in good and tenantable repair and condition from the date that of possession of the Apartment is taken and shall not do or suffer to be done anything in or to the building in which the Apartment is situated which may be against the rules, regulations or bye-laws or change/alter or make addition in or to the building in which the Apartment is situated and the Apartment itself or any part thereof without the consent of the local authorities, if required.\r\n" + 
					"\n"+"ii. Not to store in the Apartment any goods which are of hazardous, combustible or dangerous nature or are so heavy as to damage the construction or structure of the building in which the Apartment is situated or storing of which goods is objected to by the concerned local or other authority and shall take care while carrying heavy packages which may damage or likely to damage the staircases, common passages or any other structure of the building in which the Apartment is situated, including entrances of the building in which the Apartment is situated and in case any damage is caused to the building in which the Apartment is situated or the Apartment on account of negligence or default of the Allottee in this behalf, the Allottee shall be liable for the consequences of the breach. \r\n" + 
					"\n"+"iii. To carry out at his own cost all internal repairs to the said Apartment and maintain the Apartment in the same condition, state and order in which it was delivered by the Promoter to the Allottee and shall not do or suffer to be done anything in or to the building in which the Apartment is situated or the Apartment which may be contrary to the rules and regulations and bye-laws of the concerned local authority or other public authority. In the event of the Allottee committing any act in contravention of the above provision, the Allottee shall be responsible and liable for the consequences thereof to the concerned local authority and/or other public authority.\r\n" + 
					"\n"+"iv. Not to demolish or cause to be demolished the Apartment or any part thereof, nor at any time make or cause to be made any addition or alteration of whatever nature in or to the Apartment or any part thereof, nor any alteration in the elevation and outside colour scheme of the building in which the Apartment is situated and shall keep the portion, sewers, drains and pipes in the Apartment and the appurtenances thereto in good tenantable repair and condition, and in particular, so as to support shelter and protect the other parts of the building in which the Apartment is situated and shall not chisel or in any other manner cause damage to columns, beams, walls, slabs or RCC, Pardis or other structural members in the Apartment without the prior written permission of the Promoter and/or the Society or the Limited Company. \r\n" + 
					"\n"+"v.  Not to do or permit to be done any act or thing which may render void or voidable any insurance of the said land and the building in which the Apartment is situated or any part thereof or whereby any increased premium shall become payable in respect of the insurance. \r\n" + 
					"\n"+"vi. Not to throw dirt, rubbish, rags, garbage or other refuse or permit the same to be thrown from the said Apartment in the compound or any portion of the said land and the building in which the Apartment is situated. \r\n" + 
					"\n"+"vii.Pay to the Promoter within fifteen days of demand by the Promoter, his share of security deposit demanded by the concerned local authority or Government or giving water, electricity or any other service connection to the building in which the Apartment is situated. \r\n" + 
					"\n"+"viii.To bear and pay increase in local taxes. Water charges, insurance and such other levies, if any, which are imposed by the concerned local authority and/or Government and/or other public authority, on account of change of user of the Apartment by the Allottee to any purposes other than for purpose for which it is sold.\r\n" + 
					"\n"+"ix. The Allottee shall not let, sub-let, transfer, assign or part with interest or benefit factor of this Agreement or part with the possession of the Apartment until all the dues payable by the Allottee to the Promoter under this Agreement are fully paid up and only if the Allottee had not been guilty of breach of or non-observance of any of the terms and conditions of this Agreement and until the Allottee has intimated in writing to the Promoter and obtained the written consent of the Promoter for such transfer, assign or part with the interest etc. and payment of premium as per the Co-operative Societies Act, 1960. \r\n" + 
					"\n"+"x.  The Allottee shall observe and perform all the rules and regulations which the Society or the Limited Company or Apex Body or Federation may adopt at its inception and the additions, alterations or amendments thereof that may be made from time to time for protection and maintenance of the said building and the Apartments therein and for the observance and performance of the Building Rules, Regulations and Bye-laws for the time being of the concerned local authority and of Government and other public bodies. The Allottee shall also observe and perform all the stipulations and conditions laid down by the Society/Limited Company/Apex Body/ Federation regarding the occupation and use of the Apartment in the Building and shall pay and contribute regularly and punctually towards the taxes, expenses or other out-goings in accordance with the terms of this Agreement. \r\n" + 
					"\n"+"xi. Till a conveyance of the structure of the building in which Apartment is situated is executed in favour of Society/Limited Society, the Allottee shall permit the Promoter and their surveyors and agents, with or without workmen and others, at all reasonable times, to enter into and upon the said buildings or any part thereof to view and examine the state and condition thereof. \r\n" + 
					"\n"+"xii.Till a conveyance of the said Land on which the building in which Apartment is situated is executed in favour of Apex Body or Federation, the Allottee shall permit the Promoter and their surveyors and agents, with or without workmen and others, at all reasonable times, to enter into and upon the said Land or any part thereof to view and examine the state and condition thereof. \r\n" + 
					"\n"+"xiii.Common areas like internal roads, U.G.C. & S.T.P.s will be used by all the flat holders of all the buildings as per decision of Promoter/Builder and purchasers will raise no objection until conveyance deed is executed.\r\n" + 

					"\n"+"15. The Promoter shall maintain a separate account in respect of sums received by the Promoter from the Allottee as advance or deposit, sums received on account of the share capital for the promotion of the Co-operative Society or a Company or towards the out goings, legal charges and shall utilize the amounts only for the purposes for which they have been received. \r\n" + 
					"\n"+"16. Nothing contained in this Agreement is intended to be nor shall be construed as a grant, demise or assignment in law, of the said Apartments or of the said Plot and Building or any part thereof. The Allottee shall have no claim save and except in respect of the Apartment hereby agreed to be sold to him and all open spaces, parking spaces, lobbies, staircases, terraces recreation spaces , will remain the property of the Promoter until the said structure of the building is transferred to the Society/Limited Company or other body and until the said Land is transferred to the Apex Body /Federation as hereinbefore mentioned. \r\n" + 

					"\n"+"17. PROMOTER SHALL NOT MORTGAGE OR CREATE A CHARGE \r\n"+
					"\n"+"After the Promoter executes this Agreement he shall not mortgage or create a charge on the [Apartment/] and if any such mortgage or charge is made or created then notwithstanding anything contained in any other law for the time being in force, such mortgage or charge shall not affect the right and interest of the Allottee who has taken or agreed to take such Apartment. \r\n" + 
					"\n"+"18. BINDING EFFECT \r\n" + 
					"\n"+"Forwarding this Agreement to the Allottee by the Promoter does not create a binding obligation on the part of the Promoter or the Allottee until, firstly, the Allottee signs and delivers this Agreement with all the schedules along with the payments due as stipulated in the Payment Plan within 30 (thirty) days from the date of receipt by the Allottee and secondly, appears for registration of the same before the concerned Sub- Registrar as and when intimated by the Promoter. If the Allottee(s) fails to execute and deliver to the Promoter this Agreement within 30 (thirty) days from the date of its receipt by the Allottee and/or appear before the Sub-Registrar for its registration as and when intimated by the Promoter, then the Promoter shall serve a notice to the Allottee for rectifying the default, which if not rectified within 15(Fifteen) days from the date of its receipt by the Allottee, application of the Allottee shall be treated as cancelled and all sums deposited by the Allottee in connection therewith including the booking amount shall be returned to the Allottee without any interest or compensation whatsoever. \r\n" + 
					"\n"+"19. ENTIRE AGREEMENT \r\n"
					+"This Agreement, along with its schedules, constitutes the entire Agreement between the Parties with respect to the subject matter hereof and supersedes any and all understandings, any other agreements, allotment letter, correspondences, arrangements whether written or oral, if any, between the Parties in regard to the said apartment/plot/building, as the case may be. \r\n" + 

					"\n"+"20. RIGHT TO AHEND \r\n"+
					"\n"+"This Agreement may only be amended through written consent of the Parties. \r\n" + 

					"\n"+"21. PROVISIONS OF THIS AGREEMENT APPLICABLE ON ALLOTTEE / SUBSEQUENT ALLOTTEES \r\n"+
					"\n"+"It is clearly understood and so agreed by and between the Parties hereto that all the provisions contained herein and the obligations arising hereunder in respect of the Project shall equally be applicable to and enforceable against any subsequent Allottees of the Apartment, in case of a transfer, as the said obligations go along with the Apartment for all intents and purposes. \r\n" + 
					"\n"+"22.SEVERABILITY \r\n" + 
					"\n"+"If any provision of this Agreement shall be determined to be void or unenforceable under the Act or the Rules and Regulations made thereunder or under other applicable laws, such provisions of the Agreement shall be deemed amended or deleted in so far as reasonably inconsistent with the purpose of this Agreement and to the extent necessary to conform to Act or the Rules and Regulations made thereunder or the applicable law, as the case may be, and the remaining provisions of this Agreement shall remain valid and enforceable as applicable at the time of execution of this Agreement. \r\n" + 

					"\n"+"23. METHOD OF CALCULATION OF PROPORTIONATE SHARE WHEREVER REFERRED TO IN THE AGREEMENT \r\n"+
					"\n"+"Wherever in this Agreement it is stipulated that the Allottee has to make any payment, in common with other Allottee(s) in Project, the same shall be the proportion which the carpet area of the Apartment bears to the total carpet area of all the Apartments in the Project. \r\n" + 
					"\n"+"24. FURTHER ASSURANCES\r\n" + 
					"\n"+"Both Parties agree that they shall execute, acknowledge and deliver to the other such instruments and take such other actions, in additions to the instruments and actions specifically provided for herein, as may be reasonably required in order to effectuate the provisions of this Agreement or of any transaction contemplated herein or to confirm or perfect any right to be created or transferred hereunder or pursuant to any such transaction. \r\n" + 
					"\n"+"25. PLACE OF EXECUTION \r\n" + 
					"\n"+"The execution of this Agreement shall be complete only upon its execution by the Promoter through its authorized signatory at the Promoter�s Office, or at some other place, which may be mutually agreed between the Promoter and the Allottee, in office of Promoter after the Agreement is duly executed by the Allottee and the Promoter or simultaneously with the execution the said Agreement shall be registered at the office of the Sub-Registrar. Hence this Agreement shall be deemed to have been executed at Haveli, Pune. \r\n" + 
					"\n"+"26. The Allottee and/or Promoter shall present this Agreement as well as the conveyance/ assignment of lease at the proper registration office of registration within the time limit prescribed by the Registration Act and the Promoter will attend such office and admit execution thereof. \r\n" + 
					"\n"+"27. That all notices to be served on the Allottee and the Promoter as contemplated by this Agreement shall be deemed to have been duly served if sent to the Allottee or the Promoter by Registered Post A. D. or notified Email ID/Under Certificate of Posting at their respective addresses specified below:" + 

					"\r\n \n"+"Name of Allottee : "+aggreementDetails.getFirstApplicantfirstname() +" "+ aggreementDetails.getFirstApplicantmiddlename() +" "+ aggreementDetails.getFirstApplicantmiddlename().replaceAll("\\s", "").replace("\n", "").replace("\r", "") +
					"\r\n"+"Allottee Address : Present Address :"+aggreementDetails.getFirstApplicantPresentAddress()+
					"\r\n \t \t       "+aggreementDetails.getFirstApplicantPresentPincode()+"\r\n" + 

					"Notified Email ID :"+aggreementDetails.getFirstApplicantEmailId()+"\r\n" + 
					"\r\n" + 
					"\n"+"Promoter name : D. K. ASSOCIATES Through its proprietor \r\n" + 
					"\t \t    "+" Dhanraj Kesarimal Sonigara\r\n" + 
					"Promoter Address : 11, Laxmitara Market, Thergaon, Dange Chowk, Pune\r\n" + 
					"Notified Email ID : infra@sonigara.in\r\n" + 

					"\n"+"It shall be the duty of the Allottee and the promoter to inform each other of any change in address subsequent to the execution of this Agreement in the above address by Registered Post failing which all communications and letters posted at the above address shall be deemed to have been received by the promoter or the Allottee, as the case may be. \r\n"+
					"\n"+"28. JOINT ALLOTTEES \r\n" + 
					"That in case there are Joint Allottees all communications shall be sent by the Promoter to the Allottee whose name appears first and at the address given by him/her which shall for all intents and purposes to consider as properly served on all the Allottees. \r\n" + 
					"\n"+"29. STAMP DUTY AND REGISTRATION \r\n" + 
					"\n"+"The Charges towards stamp duty and registration of this Agreement shall be borne by the Allottee.\r\n" + 

					"\n"+"30. DISPUTE RESOLUTION \r\n"+
					"\n"+"Any dispute between parties shall be settled amicably. In case of failure to settled the dispute amicably, which shall be referred to the Competent Authority as per the provisions of the Real Estate (Regulation and Development) Act, 2016, Rules and Regulation, thereunder.\r\n" + 

					"\n"+"31. GOVERNING LAW \r\n"+
					"\n"+"That the rights and obligations of the parties under or arising out of this Agreement shall be construed and enforced in accordance with the laws of India for the time being in force and the Competent Courts will have the jurisdiction for this Agreement \r\n" + 
					"\n"+"IN WITNESS WHEREOF parties hereinabove named have set their respective hands and signed this Agreement for sale at (city/town name) in the presence of attesting witness, signing as such on the day first above written. \r\n" + 
					"\n"+"\r\n" + 

					"\n"+"\t \t \t \t  SCHEDULE �A� OF THEPROPERTY \r" + 
					"\n"+"All that piece and parcel of land bearing Gat No. 1596 (Part), 1597/1 and 1597/2  admeasuring about 8891.22 Sq. Mtrs. situated at Village Chikhali Tal Haveli, Pune within the local limits of Pimpri Chinchwad Municipal Corporation within the limits of "+
					"Sub- Registrar Hveli is bounded as under :- \r\n"	+	

					"\r\n"+"ON OR TOWARDS EAST : Remaining part of Gat No. 1596,1593 \r\n"+
					"ON OR TOWARDS WEST : Gat No. 1600 \r\n"+
					"ON OR TOWARDS SOUTH : Gat No. 1595 \r\n"+
					"ON OR TOWARDS NORTH : Gat No. 1592 \r\n"+

					"\r\n \t \t \t \t "+"SCHEDULE �B� OF THE FLAT \r\n" + 
					"\n"+"All that piece and parcel of the bearing Flat No."+flatDetails.getFlatNumber()+", on "+floorDetails.getFloortypeName()+" Floor, in the building No. "+wingDetails.getWingName()+" admeasuring area "+flatDetails.getCarpetArea()+"Sq. Mtrs. Carpetand right to use same level Terrace admeasuring about "+flatDetails.getTerraceArea()+" sq. mtrs , Dry Tarrace "+flatDetails.getDryterraceArea()+" sq.mtrs and encl. Balcony "+flatDetails.getBalconyArea()+" sq.mtrincluding "
					+ "the area of proportionate share in the building to be known as BLUE DICE to be constructed/ is constructed or being constructed on the property more particularly described in the Schedule �A� here in above mentioned.  \r\n" + 
					"\n"+"Along with all the common facility and common use of staircase, water connection, and common drainage line & W.C. as per the rules.\r\n" + 
					"\n"+"IN WITNESS WHERE OF the parties here to have hereunto signed and subscribed their respective hands on the day and year first here in above mentioned.\r\n" + 
					"\r\n" + 
					"\r\n" + 
					"SIGNED, SEALED AND DELIVERED \r\n"+
					"By the PROMOTER/OWNERS above names\r\n" + 
					"\t M/S D. K. ASSOCIATES \r\n"+
					"\t  Through it�s Proprietor\r\n" + 
					"MR. DHANRAJ KESARIMAL SONIGARA\r\n" + 
					"\t\t (VENDOR)\r\n" + 
					"\n"+
					"SIGNED, SEALED & RECEIVED \n"+
					"\t PURCHASER \n \n"+
					"1)__________________________________ \n"+
					"\n"+
					"SIGNED, SEALED & RECEIVED \n"+
					"\t PURCHASER \n \n"+
					"2)__________________________________ \n"+

					"\n \n WITNESSES : - "+

					"\r \n 1.SIGN. : "+
					"\r NAME : "+aggreementDetails.getFirstApplicantfirstname().toUpperCase() +" "+ aggreementDetails.getFirstApplicantmiddlename().toUpperCase() +" "+aggreementDetails.getFirstApplicantlastname().toUpperCase()+" "+
					"\r ADD. :"+"\n"+

					"\r 2.SIGN. : "+
					"\r NAME : "+aggreementDetails.getSecondApplicantfirstname().toUpperCase() +" "+ aggreementDetails.getSecondApplicantmiddlename().toUpperCase() +" "+aggreementDetails.getSecondApplicantlastname().toUpperCase()+" "+
					"\r ADD. : "+"\n \n "+

					" \n  \n"+
					"\t \t \t \t \t   SCHEDULE II \n" + 
					"\t \t \t \t \t SPECIFICATIONS "+"\n"+
					"\n"+

					"�	Standard RCC structure\r\n" + 
					"�	Elegant Elevation\r\n" + 
					"�	Vitrified tile flooring for the entire flat\r\n" + 
					"�	Antiskid /Rustic Tiles in dry terrace\r\n" + 
					"�	Aluminium threeway sliding windows with grill & marble sil and masquito net\r\n" + 
					"�	Internal walls painting with oil bond distemper\r\n" + 
					"�	Decorative entrance door with good quality fittings\r\n" + 
					"�	Granite kitchen platform with dado of glazed tiles dado upto 7� height \r\n" + 
					"�	Concealed plumbing with standard quality fitting\r\n" + 
					"�	Concealed copper electrical wiring with modular switches\r\n" + 
					"�	Well illuminated common area\r\n" + 
					"�	Compound wall and gate for entire campus\r\n" + 
					"�	Provision for Inverter point in each flat \r\n" + 
					"�	Telephone & T.V. point connection in hall & bedroom\r\n" + 
					"�	Provision for Water Purifier\r\n" + 
					"\n"+"(All the plans, drawings, amenities as well as common amenities, facilities, colour scheme, elevation shown in the brochure or elsewhere are indicative and artist�s  persecution and are subject to the approval of the respective authorities and may be changed at the discretion of the developers as and when required).\r\n" + 

					"\r\n" + 
					"\t \t \t \t \t  SCHEDULE II \r" + 
					"\t \t \t \t \t   AMENITIES \r\n" + 
					"\r\n" + 
					"�	Party Lawn\r\n" + 
					"�	Club House\r\n" + 
					"�	Gazebo\r\n" + 
					"�	Senior Citizen Sit out\r\n" + 
					"�	Palm Court\r\n" + 
					"�	Temple\r\n" + 
					"�	Jogging Track\r\n" + 
					"\r\n" + 
					"\r\n" + 

					"\t \t \t \t \t   RESEIPT \r\n"+
					"\r\n" + 
					"	Received of and from the Allottee above named the sum of Rupees "+bookingDetails.getGrandTotal1()+"/- ( Rupees "+totalinWords+" )  on execution of this agreement towards Earnest Money Deposit or application fee.\r\n" + 
					"\r\n" + 
					"\t \t \t \t \t \t \t I say received. \r\n"+

					"\r\n" + 

					"\t \t \t \t \t \t \t The Promoter/s. ";


			parameterMap.put("printDate", ""+printDate);
			parameterMap.put("aggDate", ""+aggreementDetails.getAggreementDate());
			parameterMap.put("companyNameAndAddress", ""+companyNameAndAddress);
			parameterMap.put("companyOwnerNameAndInfo", ""+companyOwnerNameAndInfo);
			parameterMap.put("aggName1", ""+aggName1);
			parameterMap.put("aggName2", ""+aggName2);

			parameterMap.put("passage1", ""+passage1);
			parameterMap.put("passage2", ""+passage2);
			parameterMap.put("passage3", ""+passage3);


			/*
				 //Booking Receipt Generation Code
				 1 parameterMap.put("aggDate", ""+CustomerPaymentScheduleDetail.get(0).getBookingId());
				 2 parameterMap.put("customerName", ""+customerName);
				 3 parameterMap.put("receiptDate", ""+strDate);

			 */
			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/customerAgreement.jasper");

			print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(print, baos);

			byte[] contents = baos.toByteArray();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			String filename = "customerAgreement.pdf";

			JasperExportManager.exportReportToPdfStream(print, baos);


			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

			ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
			response.setHeader("Content-Disposition", "inline; filename=" + filename );

			return resp;					    
		}
		catch(Exception ex)
		{
			return null;
		}

	}

	@RequestMapping("/AddAggreement")
	public String AddAggreement(@RequestParam("bookingId") String bookingId, ModelMap model)
	{
		//String bookingId="BKG/1819/0029";
		try {
			try {

				Query query = new Query();
				Aggreement aggreement = mongoTemplate.findOne(query.addCriteria(Criteria.where("bookingId").is(bookingId)),Aggreement.class);
				if(aggreement==null) {
					query = new Query();
					List<Booking> bookingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)),Booking.class);

					query =new Query();
					List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(bookingDetails.get(0).getProjectId())), Project.class);

					query =new Query();
					List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(bookingDetails.get(0).getBuildingId())), ProjectBuilding.class);

					query =new Query();
					List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(bookingDetails.get(0).getWingId())), ProjectWing.class);

					query =new Query();
					List<Floor> floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("floorId").is(bookingDetails.get(0).getFloorId())), Floor.class);

					query =new Query();
					List<Flat> flatDetails = mongoTemplate.find(query.addCriteria(Criteria.where("flatId").is(bookingDetails.get(0).getFlatId())), Flat.class);

					model.addAttribute("projectName",projectDetails.get(0).getProjectName());
					model.addAttribute("buildingName",buildingDetails.get(0).getBuildingName());
					model.addAttribute("wingName",wingDetails.get(0).getWingName());
					model.addAttribute("floortypeName",floorDetails.get(0).getFloortypeName());
					model.addAttribute("flatNumber",flatDetails.get(0).getFlatNumber());

					model.addAttribute("bookingDetails",bookingDetails);
					model.addAttribute("aggreementCode",AggreementCode(projectDetails.get(0).getProjectName(),buildingDetails.get(0).getBuildingName(),wingDetails.get(0).getWingName(),flatDetails.get(0).getFlatNumber()));
					model.addAttribute("occupationList",getAllOccupationName());
					model.addAttribute("countryList",findAllCountryId());
					model.addAttribute("userName","self");

					return "AddAggreement";

				}
				else
				{
					query = new Query();
					List<Aggreement> aggreementDetails = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)),Aggreement.class);
					Query query1 = new Query();
					List<Booking> bookingDetails = mongoTemplate.find(query1.addCriteria(Criteria.where("bookingId").is(bookingId)),Booking.class);
					try
					{
						query =new Query();
						List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(bookingDetails.get(0).getProjectId())), Project.class);

						query =new Query();
						List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(bookingDetails.get(0).getBuildingId())), ProjectBuilding.class);

						query =new Query();
						List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(bookingDetails.get(0).getWingId())), ProjectWing.class);

						query =new Query();
						List<Floor> floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("floorId").is(bookingDetails.get(0).getFloorId())), Floor.class);

						query =new Query();
						List<Flat> flatDetails = mongoTemplate.find(query.addCriteria(Criteria.where("flatId").is(bookingDetails.get(0).getFlatId())), Flat.class);
						
						model.addAttribute("projectName",projectDetails.get(0).getProjectName());
						model.addAttribute("buildingName",buildingDetails.get(0).getBuildingName());
						model.addAttribute("wingName",wingDetails.get(0).getWingName());
						model.addAttribute("floortypeName",floorDetails.get(0).getFloortypeName());
						model.addAttribute("flatNumber",flatDetails.get(0).getFlatNumber());
					}
					catch (Exception e) {
						// TODO: handle exception
					}
					model.addAttribute("bookingDetails",bookingDetails);
					model.addAttribute("aggreementDetails", aggreementDetails);
					model.addAttribute("occupationList",getAllOccupationName());
					model.addAttribute("countryList",findAllCountryId());

					return "EditAggreement";
				}
			}catch (Exception e) {
				e.printStackTrace();
				return "CustomerAggreementSucessPage";
				// TODO: handle exception
			}

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping(value = "/AddAggreement", method = RequestMethod.POST)
	public String SaveAddAggreement(@ModelAttribute Aggreement aggreement, Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				Aggreement aggreement1=new Aggreement();
				aggreement1.setAggreementId(aggreement.getAggreementId());
				aggreement1.setBookingId(aggreement.getBookingId());
				aggreement1.setAggreementNumber(aggreement.getAggreementNumber());
				aggreement1.setAggreementDate(aggreement.getAggreementDate());

				aggreement1.setFirstApplicantfirstname(aggreement.getFirstApplicantfirstname().toUpperCase());
				aggreement1.setFirstApplicantmiddlename(aggreement.getFirstApplicantmiddlename().toUpperCase());
				aggreement1.setFirstApplicantlastname(aggreement.getFirstApplicantlastname().toUpperCase());

				aggreement1.setFirstApplicantmaidenfirstname(aggreement.getFirstApplicantmaidenfirstname().toUpperCase());
				aggreement1.setFirstApplicantmaidenmiddlename(aggreement.getFirstApplicantmaidenmiddlename().toUpperCase());
				aggreement1.setFirstApplicantmaidenlastname(aggreement.getFirstApplicantmaidenlastname().toUpperCase());

				aggreement1.setFirstApplicantGender(aggreement.getFirstApplicantGender());
				aggreement1.setFirstApplicantMarried(aggreement.getFirstApplicantMarried());
				aggreement1.setFirstApplicantDob(aggreement.getFirstApplicantDob());
				aggreement1.setFirstApplicantmobileNumber1(aggreement.getFirstApplicantmobileNumber1());
				aggreement1.setFirstApplicantmobileNumber2(aggreement.getFirstApplicantmobileNumber2());
				aggreement1.setFirstApplicantSpuseName(aggreement.getFirstApplicantSpuseName());
				aggreement1.setFirstApplicantSpouseDob(aggreement.getFirstApplicantSpouseDob());

				aggreement1.setFirstApplicantAnniversaryDate(aggreement.getFirstApplicantAnniversaryDate());
				aggreement1.setFirstApplicantFatherName(aggreement.getFirstApplicantFatherName());
				aggreement1.setFirstApplicantEmailId(aggreement.getFirstApplicantEmailId());
				aggreement1.setFirstApplicantPanCardNo(aggreement.getFirstApplicantPanCardNo().toUpperCase());
				aggreement1.setFirstApplicantAadharno(aggreement.getFirstApplicantAadharno());
				aggreement1.setFirstApplicantMotherTonque(aggreement.getFirstApplicantMotherTonque());

				aggreement1.setFirstApplicantPresentAddress(aggreement.getFirstApplicantPresentAddress());
				aggreement1.setFirstApplicantPresentPincode(aggreement.getFirstApplicantPresentPincode());

				aggreement1.setFirstApplicantPermanentaddress(aggreement.getFirstApplicantPermanentaddress());
				aggreement1.setFirstApplicantPermanentPincode(aggreement.getFirstApplicantPermanentPincode());

				aggreement1.setFirstApplicantEducation(aggreement.getFirstApplicantEducation());
				aggreement1.setFirstApplicantOccupation(aggreement.getFirstApplicantOccupation());
				aggreement1.setFirstApplicantOrganizationName(aggreement.getFirstApplicantOrganizationName());
				aggreement1.setFirstApplicantOrganizationType(aggreement.getFirstApplicantOrganizationType());
				aggreement1.setFirstApplicantOrganizationaddress(aggreement.getFirstApplicantOrganizationaddress());
				aggreement1.setFirstApplicantofficeNumber(aggreement.getFirstApplicantofficeNumber());
				aggreement1.setFirstApplicantofficeEmail(aggreement.getFirstApplicantofficeEmail());
				aggreement1.setFirstApplicantIndustrySector(aggreement.getFirstApplicantIndustrySector());
				aggreement1.setFirstApplicantWorkFunction(aggreement.getFirstApplicantWorkFunction());
				aggreement1.setFirstApplicantExperience(aggreement.getFirstApplicantExperience());
				aggreement1.setFirstApplicantIncome(aggreement.getFirstApplicantIncome());

				aggreement1.setSecondApplicantfirstname(aggreement.getSecondApplicantfirstname());
				aggreement1.setSecondApplicantmiddlename(aggreement.getSecondApplicantmiddlename());
				aggreement1.setSecondApplicantlastname(aggreement.getSecondApplicantlastname());

				aggreement1.setSecondApplicantmaidenfirstname(aggreement.getSecondApplicantmaidenfirstname().toUpperCase());
				aggreement1.setSecondApplicantmaidenmiddlename(aggreement.getSecondApplicantmaidenmiddlename().toUpperCase());
				aggreement1.setSecondApplicantmaidenlastname(aggreement.getSecondApplicantmaidenlastname().toUpperCase());

				aggreement1.setSecondApplicantGender(aggreement.getSecondApplicantGender());
				aggreement1.setSecondApplicantMarried(aggreement.getSecondApplicantMarried());
				aggreement1.setSecondApplicantDob(aggreement.getSecondApplicantDob());
				aggreement1.setSecondApplicantmobileNumber1(aggreement.getSecondApplicantmobileNumber1());
				aggreement1.setSecondApplicantmobileNumber2(aggreement.getSecondApplicantmobileNumber2());
				aggreement1.setSecondApplicantSpouseName(aggreement.getSecondApplicantSpouseName());
				aggreement1.setSecondApplicantSpouseDob(aggreement.getSecondApplicantSpouseDob());

				aggreement1.setSecondApplicantAnnivaversaryDate(aggreement.getSecondApplicantAnnivaversaryDate());
				aggreement1.setSecondApplicantFatherName(aggreement.getSecondApplicantFatherName().toUpperCase());
				aggreement1.setSecondApplicantEmail(aggreement.getSecondApplicantEmail());
				aggreement1.setSecondApplicantPancardno(aggreement.getSecondApplicantPancardno().toUpperCase());
				aggreement1.setSecondApplicantAadharno(aggreement.getSecondApplicantAadharno());
				aggreement1.setSecondApplicantMotherTongue(aggreement.getSecondApplicantMotherTongue());
				aggreement1.setSecondApplicantRelation(aggreement.getSecondApplicantRelation().toUpperCase());


				aggreement1.setSecondApplicantPresentAddress(aggreement.getSecondApplicantPresentAddress());
				aggreement1.setSecondApplicantPresentPincode(aggreement.getSecondApplicantPresentPincode());

				aggreement1.setSecondApplicantPermanentaddress(aggreement.getSecondApplicantPermanentaddress());
				aggreement1.setSecondApplicantPermanentPincode(aggreement.getSecondApplicantPermanentPincode());

				aggreement1.setSecondApplicantEducation(aggreement.getSecondApplicantEducation());
				aggreement1.setSecondApplicantOccupation(aggreement.getSecondApplicantOccupation());
				aggreement1.setSecondApplicantOrganizationName(aggreement.getSecondApplicantOrganizationName());
				aggreement1.setSecondApplicantOrganizationType(aggreement.getSecondApplicantOrganizationType());
				aggreement1.setSecondApplicantOrganizationaddress(aggreement.getSecondApplicantOrganizationaddress());
				aggreement1.setSecondApplicantofficeNumber(aggreement.getSecondApplicantofficeNumber());
				aggreement1.setSecondApplicantofficeEmail(aggreement.getSecondApplicantofficeEmail());
				aggreement1.setSecondApplicantIndustrySector(aggreement.getSecondApplicantIndustrySector());
				aggreement1.setSecondApplicantWorkFunction(aggreement.getSecondApplicantWorkFunction());
				aggreement1.setSecondApplicantExperience(aggreement.getSecondApplicantExperience());
				aggreement1.setSecondApplicantIncome(aggreement.getSecondApplicantIncome());

				aggreement1.setAggreementstatus(aggreement.getAggreementstatus());
				aggreement1.setCreationDate(new Date());
				aggreement1.setUpdateDate(new Date());
				aggreement1.setUserName(aggreement.getUserName());

				aggreementRepository.save(aggreement1);

				List<Booking> bookingList=new ArrayList<Booking>();
				Query query1 = new Query();
				bookingList = mongoTemplate.find(query1.addCriteria(Criteria.where("bookingId").is(aggreement.getBookingId())), Booking.class);

				Flat flat;
				flat = mongoTemplate.findOne(Query.query(Criteria.where("flatId").is(bookingList.get(0).getFlatId())), Flat.class);
				flat.setFlatstatus("Aggreement Completed");
				flatRepository.save(flat);

				Booking booking;
				booking = mongoTemplate.findOne(Query.query(Criteria.where("bookingId").is(aggreement.getBookingId())), Booking.class);
				booking.setBookingstatus("Aggreement Completed");
				bookingRepository.save(booking);


				List<PaymentScheduler> paymentscheduleList=new ArrayList<PaymentScheduler>(); 

				// for Payment Scheduler

				Query query2 = new Query();
				paymentscheduleList = mongoTemplate.find(query2.addCriteria(Criteria.where("projectId").is(bookingList.get(0).getProjectId()).and("buildingId").is(bookingList.get(0).getBuildingId()).and("wingId").is(bookingList.get(0).getWingId())), PaymentScheduler.class);

				String status;
				double percentage=0;
				String schedule="";
				int count=0;
				for(int i=0;i<paymentscheduleList.size();i++)
				{
					status=paymentscheduleList.get(i).getSlabStatus();
					if(status.equals("Completed"))
					{
						percentage=percentage+paymentscheduleList.get(i).getPercentage();
						schedule=schedule+paymentscheduleList.get(i).getPaymentDecription();
						count++;
					}

				}

				try {
					GeneratePaymentScheduler generatepaymentscheduler=new GeneratePaymentScheduler();
					if(count!=0)
					{

						generatepaymentscheduler.setCustomerschedulerId(PaymentCode());
						generatepaymentscheduler.setBookingId(booking.getBookingId());
						generatepaymentscheduler.setInstallmentNumber("1st Installment");
						generatepaymentscheduler.setPaymentDecription(schedule);
						generatepaymentscheduler.setPercentage(percentage);
						generatepaymentscheduler.setCreationDate(booking.getCreationDate());
						generatepaymentscheduler.setUserName(booking.getUserName());
						generatepaymentschedulerRepository.save(generatepaymentscheduler);
					}
					int f=2;
					String installmentNumber="";
					for(int i=count;i<paymentscheduleList.size();i++)
					{
						status=paymentscheduleList.get(i).getSlabStatus();
						if(!status.equals("Completed"))
						{
							installmentNumber=f+"st Installment";
							generatepaymentscheduler.setCustomerschedulerId(PaymentCode());
							generatepaymentscheduler.setBookingId(booking.getBookingId());
							generatepaymentscheduler.setInstallmentNumber(installmentNumber);
							generatepaymentscheduler.setPaymentDecription(paymentscheduleList.get(i).getPaymentDecription());
							generatepaymentscheduler.setPercentage(paymentscheduleList.get(i).getPercentage());
							generatepaymentscheduler.setCreationDate(booking.getCreationDate());
							generatepaymentscheduler.setUserName(booking.getUserName());
							generatepaymentschedulerRepository.save(generatepaymentscheduler);
							f++;
						}

					}
				}
				catch(Exception ww)
				{
					System.out.println(ww);
				}   

				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}

			return "CustomerAggreementSucessPage";

		}catch (Exception e) {
			return "CustomerAggreementSucessPage";
		}
	}


	@RequestMapping(value="/EditAggreement", method = RequestMethod.POST)
	public String EditAggreement(Model model, @ModelAttribute Aggreement aggreement, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			Query query1=new Query();
			Aggreement aggreement2=mongoTemplate.findOne(query1.addCriteria(Criteria.where("aggreementId").is(aggreement.getAggreementId())), Aggreement.class);
			try
			{

				Aggreement aggreement1=new Aggreement();
				aggreement1.setAggreementId(aggreement.getAggreementId());
				aggreement1.setBookingId(aggreement.getBookingId());
				aggreement1.setAggreementNumber(aggreement.getAggreementNumber());
				aggreement1.setAggreementDate(aggreement.getAggreementDate());

				aggreement1.setFirstApplicantfirstname(aggreement.getFirstApplicantfirstname().toUpperCase());
				aggreement1.setFirstApplicantmiddlename(aggreement.getFirstApplicantmiddlename().toUpperCase());
				aggreement1.setFirstApplicantlastname(aggreement.getFirstApplicantlastname().toUpperCase());

				aggreement1.setFirstApplicantmaidenfirstname(aggreement.getFirstApplicantmaidenfirstname().toUpperCase());
				aggreement1.setFirstApplicantmaidenmiddlename(aggreement.getFirstApplicantmaidenmiddlename().toUpperCase());
				aggreement1.setFirstApplicantmaidenlastname(aggreement.getFirstApplicantmaidenlastname().toUpperCase());

				aggreement1.setFirstApplicantGender(aggreement.getFirstApplicantGender());
				aggreement1.setFirstApplicantMarried(aggreement.getFirstApplicantMarried());
				aggreement1.setFirstApplicantDob(aggreement.getFirstApplicantDob());
				aggreement1.setFirstApplicantmobileNumber1(aggreement.getFirstApplicantmobileNumber1());
				aggreement1.setFirstApplicantmobileNumber2(aggreement.getFirstApplicantmobileNumber2());
				aggreement1.setFirstApplicantSpuseName(aggreement.getFirstApplicantSpuseName());
				aggreement1.setFirstApplicantSpouseDob(aggreement.getFirstApplicantSpouseDob());

				aggreement1.setFirstApplicantAnniversaryDate(aggreement.getFirstApplicantAnniversaryDate());
				aggreement1.setFirstApplicantFatherName(aggreement.getFirstApplicantFatherName().toUpperCase());
				aggreement1.setFirstApplicantEmailId(aggreement.getFirstApplicantEmailId());
				aggreement1.setFirstApplicantPanCardNo(aggreement.getFirstApplicantPanCardNo().toUpperCase());
				aggreement1.setFirstApplicantAadharno(aggreement.getFirstApplicantAadharno());
				aggreement1.setFirstApplicantMotherTonque(aggreement.getFirstApplicantMotherTonque());

				aggreement1.setFirstApplicantPresentAddress(aggreement.getFirstApplicantPresentAddress());
				aggreement1.setFirstApplicantPresentPincode(aggreement.getFirstApplicantPresentPincode());

				aggreement1.setFirstApplicantPermanentaddress(aggreement.getFirstApplicantPermanentaddress());
				aggreement1.setFirstApplicantPermanentPincode(aggreement.getFirstApplicantPermanentPincode());

				aggreement1.setFirstApplicantEducation(aggreement.getFirstApplicantEducation());
				aggreement1.setFirstApplicantOccupation(aggreement.getFirstApplicantOccupation());
				aggreement1.setFirstApplicantOrganizationName(aggreement.getFirstApplicantOrganizationName());
				aggreement1.setFirstApplicantOrganizationType(aggreement.getFirstApplicantOrganizationType());
				aggreement1.setFirstApplicantOrganizationaddress(aggreement.getFirstApplicantOrganizationaddress());
				aggreement1.setFirstApplicantofficeNumber(aggreement.getFirstApplicantofficeNumber());
				aggreement1.setFirstApplicantofficeEmail(aggreement.getFirstApplicantofficeEmail());
				aggreement1.setFirstApplicantIndustrySector(aggreement.getFirstApplicantIndustrySector());
				aggreement1.setFirstApplicantWorkFunction(aggreement.getFirstApplicantWorkFunction());
				aggreement1.setFirstApplicantExperience(aggreement.getFirstApplicantExperience());
				aggreement1.setFirstApplicantIncome(aggreement.getFirstApplicantIncome());

				aggreement1.setSecondApplicantfirstname(aggreement.getSecondApplicantfirstname().toUpperCase());
				aggreement1.setSecondApplicantmiddlename(aggreement.getSecondApplicantmiddlename().toUpperCase());
				aggreement1.setSecondApplicantlastname(aggreement.getSecondApplicantlastname().toUpperCase());

				aggreement1.setSecondApplicantmaidenfirstname(aggreement.getSecondApplicantmaidenfirstname().toUpperCase());
				aggreement1.setSecondApplicantmaidenmiddlename(aggreement.getSecondApplicantmaidenmiddlename().toUpperCase());
				aggreement1.setSecondApplicantmaidenlastname(aggreement.getSecondApplicantmaidenlastname().toUpperCase());

				aggreement1.setSecondApplicantGender(aggreement.getSecondApplicantGender());
				aggreement1.setSecondApplicantMarried(aggreement.getSecondApplicantMarried());
				aggreement1.setSecondApplicantDob(aggreement.getSecondApplicantDob());
				aggreement1.setSecondApplicantmobileNumber1(aggreement.getSecondApplicantmobileNumber1());
				aggreement1.setSecondApplicantmobileNumber2(aggreement.getSecondApplicantmobileNumber2());
				aggreement1.setSecondApplicantSpouseName(aggreement.getSecondApplicantSpouseName());
				aggreement1.setSecondApplicantSpouseDob(aggreement.getSecondApplicantSpouseDob());

				aggreement1.setSecondApplicantAnnivaversaryDate(aggreement.getSecondApplicantAnnivaversaryDate());
				aggreement1.setSecondApplicantFatherName(aggreement.getSecondApplicantFatherName().toUpperCase());
				aggreement1.setSecondApplicantEmail(aggreement.getSecondApplicantEmail());
				aggreement1.setSecondApplicantPancardno(aggreement.getSecondApplicantPancardno().toUpperCase());
				aggreement1.setSecondApplicantAadharno(aggreement.getSecondApplicantAadharno());
				aggreement1.setSecondApplicantMotherTongue(aggreement.getSecondApplicantMotherTongue());
				aggreement1.setSecondApplicantRelation(aggreement.getSecondApplicantRelation());


				aggreement1.setSecondApplicantPresentAddress(aggreement.getSecondApplicantPresentAddress());
				aggreement1.setSecondApplicantPresentPincode(aggreement.getSecondApplicantPresentPincode());

				aggreement1.setSecondApplicantPermanentaddress(aggreement.getSecondApplicantPermanentaddress());
				aggreement1.setSecondApplicantPermanentPincode(aggreement.getSecondApplicantPermanentPincode());

				aggreement1.setSecondApplicantEducation(aggreement.getSecondApplicantEducation());
				aggreement1.setSecondApplicantOccupation(aggreement.getSecondApplicantOccupation());
				aggreement1.setSecondApplicantOrganizationName(aggreement.getSecondApplicantOrganizationName());
				aggreement1.setSecondApplicantOrganizationType(aggreement.getSecondApplicantOrganizationType());
				aggreement1.setSecondApplicantOrganizationaddress(aggreement.getSecondApplicantOrganizationaddress());
				aggreement1.setSecondApplicantofficeNumber(aggreement.getSecondApplicantofficeNumber());
				aggreement1.setSecondApplicantofficeEmail(aggreement.getSecondApplicantofficeEmail());
				aggreement1.setSecondApplicantIndustrySector(aggreement.getSecondApplicantIndustrySector());
				aggreement1.setSecondApplicantWorkFunction(aggreement.getSecondApplicantWorkFunction());
				aggreement1.setSecondApplicantExperience(aggreement.getSecondApplicantExperience());
				aggreement1.setSecondApplicantIncome(aggreement.getSecondApplicantIncome());

				aggreement1.setAggreementstatus(aggreement.getAggreementstatus());
				aggreement1.setCreationDate(aggreement2.getCreationDate());
				aggreement1.setUpdateDate(new Date());
				aggreement1.setUserName(aggreement.getUserName());

				aggreementRepository.save(aggreement1);



				// model.addAttribute("bankStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				//model.addAttribute("bankStatus","Fail");
			}

			return "CustomerAggreementSucessPage";

		}catch (Exception e) {
			return "login";
		}
	}		





}

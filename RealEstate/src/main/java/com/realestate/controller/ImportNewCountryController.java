package com.realestate.controller;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.realestate.bean.Country;
import com.realestate.repository.CountryRepository;

@Controller
@RequestMapping("/")
public class ImportNewCountryController {

	@Autowired
	ServletContext context;

	@Autowired
	CountryRepository countryRepository;
	@Autowired
	MongoTemplate mongoTemplate;
	Country country = new Country();
	HSSFWorkbook workbook;
	XSSFWorkbook workbook1;

	HSSFSheet worksheet;
	XSSFSheet worksheet1;

	String countryCode;

	private String CountryCode()
	{
		long countryCount  = countryRepository.count();
		if(countryCount<10)
		{
			countryCode = "CN000"+(countryCount+1);
		}
		else if((countryCount>=10) && (countryCount<100))
		{
			countryCode = "CN00"+(countryCount+1);
		}
		else if((countryCount>=100) && (countryCount<1000))
		{
			countryCode = "CN0"+(countryCount+1);
		}
		else
		{
			countryCode = "CN"+(countryCount+1);
		}
		return countryCode;
	}

	@RequestMapping(value="/ImportNewCountry")
	public String importNewCountry(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		return "ImportNewCountry";
	}


	@SuppressWarnings({ "deprecation"})
	@RequestMapping(value = "/ImportNewCountry", method = RequestMethod.POST)
	public String uploadCountryDetails(@RequestParam("country_excel") MultipartFile country_excel, Model model, HttpServletRequest request, RedirectAttributes redirectAttributes, HttpSession session) 
	{

		String user = (String)request.getSession().getAttribute("user");
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/M/yyyy");
		LocalDate localDate = LocalDate.now();
		String todayDate=dtf.format(localDate);

		if (!country_excel.isEmpty() || country_excel != null )
		{
			try 
			{
				if(country_excel.getOriginalFilename().endsWith("xls") || country_excel.getOriginalFilename().endsWith("xlsx") || country_excel.getOriginalFilename().endsWith("csv"))
				{
					if(country_excel.getOriginalFilename().endsWith("xlsx"))
					{
						InputStream stream = country_excel.getInputStream();
						XSSFWorkbook workbook = new XSSFWorkbook(stream);

						XSSFSheet sheet = workbook.getSheet("Sheet1");  /// this will read 1st workbook of ExcelSheet

						int firstRow = sheet.getFirstRowNum();

						XSSFRow firstrow = sheet.getRow(firstRow);

						@SuppressWarnings("unused")
						int lastColumnCount = firstrow.getLastCellNum();

						@SuppressWarnings("unused")
						Iterator<Row> rowIterator = sheet.iterator();   
						int last_no=sheet.getLastRowNum();

						Country country = new Country();
						for(int i=0;i<last_no;i++)
						{

							XSSFRow row = sheet.getRow(i+1);

							// Skip read heading 
							if (row.getRowNum() == 0) 
							{
								continue;
							}

							country.setCountryId(CountryCode());

							row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
							country.setCountryName(row.getCell(0).getStringCellValue());

							country.setCreationDate(todayDate);
							country.setUpdateDate(todayDate);
							country.setUserName(user);
							countryRepository.save(country);
						}
						workbook.close();
					}

				}//if after inner if

			}
			catch(Exception e)
			{

			}
		}

		return "ImportNewCountry";
	}

	//private static final String INTERNAL_FILE="Newcountry.xlsx";
	@RequestMapping(value = "/DownloadCountryTemplate")
	public String downloadCountryTemplate(Model model, HttpServletResponse response, HttpServletRequest request, RedirectAttributes redirectAttributes, HttpSession session) throws IOException 
	{

		String filename = "NewCountry.xlsx";
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		String filepath = "//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"Template/";
		response.setContentType("APPLICATION/OCTET-STREAM");
		response.setHeader("Content-Disposition", "attachment; filename=\""+ filename + "\"");


		FileInputStream fileInputStream = new FileInputStream(filepath+filename);

		int i;
		while ((i = fileInputStream.read()) != -1) {
			out.write(i);
		}
		fileInputStream.close();
		out.close();

		return "ImportNewCountry";
	}
}

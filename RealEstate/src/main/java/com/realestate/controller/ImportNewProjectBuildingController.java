package com.realestate.controller;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;

import java.io.InputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.Floor;
import com.realestate.bean.Login;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.ProjectBuildingRepository;
import com.realestate.repository.FloorRepository;
import com.realestate.repository.ProjectBuildingRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.ProjectWingRepository;

import java.io.FileInputStream;
import java.io.IOException;


@Controller
@RequestMapping("/")
public class ImportNewProjectBuildingController {

	@Autowired
	ServletContext context;

	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	ProjectBuildingRepository projectBuildingRepository;

	@Autowired
	MongoTemplate mongoTemplate;

	ProjectBuilding projectBuilding = new ProjectBuilding();
	HSSFWorkbook workbook;
	XSSFWorkbook workbook1;

	HSSFSheet worksheet;
	XSSFSheet worksheet1; 

	//Code for generating project code
	String buildingCode;
	private String BuildingCode()
	{
		long buildingCount = projectBuildingRepository.count();

		if(buildingCount<10)
		{
			buildingCode = "BD000"+(buildingCount+1);
		}
		else if((buildingCount>=10) && (buildingCount<100))
		{
			buildingCode = "BD00"+(buildingCount+1);
		}
		else if((buildingCount>=100) && (buildingCount<1000))
		{
			buildingCode = "BD0"+(buildingCount+1);
		}
		else
		{
			buildingCode = "BD"+(buildingCount+1);
		}

		return buildingCode;
	}


	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping(value="/ImportNewProjectBuilding")
	public String ImportNewProjectBuilding(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		return "ImportNewProjectBuilding";
	}



	@SuppressWarnings({ "deprecation"})
	@RequestMapping(value = "/ImportNewProjectBuilding", method = RequestMethod.POST)
	public String ImportNewProjectBuilding(@RequestParam("projectBuilding_excel") MultipartFile projectBuilding_excel, Model model, HttpServletRequest req, HttpServletResponse res, RedirectAttributes redirectAttributes, HttpSession session) 
	{

		String user = (String)req.getSession().getAttribute("user");
		List<Project> projectList = GetUserAssigenedProjectList(req,res);
		Query query = new Query();

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/M/yyyy");
		LocalDate localDate = LocalDate.now();
		String todayDate=dtf.format(localDate);
		String projectId="";

		if (!projectBuilding_excel.isEmpty() || projectBuilding_excel != null )
		{
			try 
			{
				if(projectBuilding_excel.getOriginalFilename().endsWith("xls") || projectBuilding_excel.getOriginalFilename().endsWith("xlsx") || projectBuilding_excel.getOriginalFilename().endsWith("csv"))
				{
					if(projectBuilding_excel.getOriginalFilename().endsWith("xlsx"))
					{
						InputStream stream = projectBuilding_excel.getInputStream();
						XSSFWorkbook workbook = new XSSFWorkbook(stream);

						XSSFSheet sheet = workbook.getSheet("Sheet1");  /// this will read 1st workbook of ExcelSheet

						int firstRow = sheet.getFirstRowNum();

						XSSFRow firstrow = sheet.getRow(firstRow);

						@SuppressWarnings("unused")
						int lastColumnCount = firstrow.getLastCellNum();

						@SuppressWarnings("unused")
						Iterator<Row> rowIterator = sheet.iterator();   
						int last_no=sheet.getLastRowNum();

						ProjectBuilding projectBuilding = new ProjectBuilding();


						for(int i=0;i<last_no;i++)
						{
							projectId="";

							try {
								XSSFRow row = sheet.getRow(i+1);

								// Skip read heading 
								if (row.getRowNum() == 0) 
								{
									continue;
								}


								for(int j=0; j<projectList.size();j++)
								{
									if((projectList.get(j).getProjectName().trim()).equalsIgnoreCase((row.getCell(1).getStringCellValue().trim())))
									{
										projectId=projectList.get(j).getProjectId();
										break;
									}
								}


								if(!projectId.equals(""))
								{

									projectBuilding.setBuildingId(BuildingCode());

									row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
									projectBuilding.setBuildingName(row.getCell(0).getStringCellValue());

									projectBuilding.setProjectId(projectId);

									row.getCell(2).setCellType(Cell.CELL_TYPE_STRING);
									projectBuilding.setNumberofWing(row.getCell(2).getStringCellValue());

									row.getCell(3).setCellType(Cell.CELL_TYPE_STRING);
									projectBuilding.setBuildingStatus(row.getCell(3).getStringCellValue());

									projectBuilding.setCreationDate(todayDate);
									projectBuilding.setUpdateDate(todayDate);
									projectBuilding.setUserName(user);
									projectBuildingRepository.save(projectBuilding);
								}

							}
							catch (Exception ee) {
							}

						}
						workbook.close();
					}

				}//if after inner if

			}
			catch(Exception e)
			{

			}
		}
		/*	
		List<ProjectBuilding> projectBuildingDetails;
		projectBuildingDetails = projectBuildingRepository.findAll();

		model.addAttribute("projectBuildingDetails", projectBuildingDetails);
		 */	
		return "ImportNewProjectBuilding";
	}


	@RequestMapping(value = "/DownloadProjectBuildingTemplate")
	public String DownloadProjectBuildingTemplate(Model model, HttpServletResponse response, HttpServletRequest request, RedirectAttributes redirectAttributes, HttpSession session) throws IOException 
	{

		String filename = "NewProjectBuilding.xlsx";
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		String filepath = "//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"Template/";
		response.setContentType("APPLICATION/OCTET-STREAM");
		response.setHeader("Content-Disposition", "attachment; filename=\""+ filename + "\"");


		FileInputStream fileInputStream = new FileInputStream(filepath+filename);

		int i;
		while ((i = fileInputStream.read()) != -1) {
			out.write(i);
		}
		fileInputStream.close();
		out.close();


		/*try 
		{
            String downloadFolder = request.getSession().getServletContext().getRealPath("/webapp/Template");
            //context.getRealPath("src/main/webapp/Template");
            System.out.println("path="+downloadFolder);
            File file = new File(downloadFolder + File.separator + filename);

            if (file.exists()) 
            {
                String mimeType = context.getMimeType(file.getPath());

                if (mimeType == null) 
                {
                    mimeType = "application/octet-stream";
                }

                response.setContentType(mimeType);
                response.addHeader("Content-Disposition", "attachment; filename=" + filename);
                response.setContentLength((int) file.length());

                OutputStream os = response.getOutputStream();
                FileInputStream fis = new FileInputStream(file);
                byte[] buffer = new byte[4096];
                int b = -1;

                while ((b = fis.read(buffer)) != -1) 
                {
                    os.write(buffer, 0, b);
                }

                fis.close();
                os.close();
            } 
            else 
            {
                System.out.println("Requested " + filename + " file not found!!");
            }
        } 
		catch (IOException e) 
		{
            System.out.println("Error:- " + e.getMessage());
        }*/


		/*File file = new File(fileName);
		FileInputStream in = new FileInputStream(file);
		byte[] content = new byte[(int) file.length()];
		in.read(content);
		ServletContext sc = request.getSession().getServletContext();
		String mimetype = sc.getMimeType(file.getName());
		response.reset();
		response.setContentType(mimetype);
		response.setContentLength(content.length);
		response.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");
		org.springframework.util.FileCopyUtils.copy(content, response.getOutputStream());*/

		return "ImportNewProjectBuilding";
	}

}

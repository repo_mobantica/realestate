package com.realestate.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.realestate.bean.Company;
import com.realestate.bean.Login;
import com.realestate.bean.ReraDocuments;
import com.realestate.bean.UserAssignedProject;
import com.realestate.configuration.CommanController;
import com.realestate.bean.Project;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.ReraDocumentRepository;

@Controller
@RequestMapping("/")
public class ReraDocumentController
{
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	ReraDocumentRepository reradocumentRepository;

	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping("/ReraDocumentMaster")
	public String ReraDocumentMaster(Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<Project> projectList = GetUserAssigenedProjectList(req,res);

			model.addAttribute("projectList", projectList);
			return "ReraDocumentMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/AddReraDocuments")
	public String ReraDocuments(@RequestParam("projectId") String projectId, Model model)
	{
		try {
			Query query = new Query();
			Company companyDetails =  mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectId)), Company.class);

			query = new Query();
			List<ReraDocuments> documentList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)), ReraDocuments.class);

			model.addAttribute("companyDetails", companyDetails);
			model.addAttribute("projectId", projectId);
			model.addAttribute("documentList", documentList);

			return "AddReraDocuments";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/checkDocumentExtension")
	public String getFileExtension(@RequestParam("fileName") String fileName)
	{
		try
		{
			int dotIndex = fileName.lastIndexOf('.');

			String extension =  (dotIndex == -1) ? "" : fileName.substring(dotIndex + 1);

			if(extension.equalsIgnoreCase("pdf") || extension.equalsIgnoreCase("jpg") || extension.equalsIgnoreCase("jpeg") || extension.equalsIgnoreCase("png"))
			{
				return "OK";
			}
			else
			{
				return "NOT OK";
			}
		}
		catch(Exception e)
		{
			e.toString();

			return "NOT OK";
		}
	}

	public String knowFileExtension(@RequestParam("fileName") String fileName)
	{
		try
		{
			String fileName1 = new File(fileName).getName();
			int dotIndex = fileName1.lastIndexOf('.');
			String extension =  (dotIndex == -1) ? "" : fileName.substring(dotIndex + 1);

			return extension;
		}
		catch(Exception e)
		{
			e.toString();

			return null;
		}
	}


	public boolean UploadFileToPath(MultipartFile file, String path)
	{
		try
		{
			if (!file.isEmpty())
			{
				try {

					byte[] bytes = file.getBytes();
					BufferedOutputStream stream =new BufferedOutputStream(new FileOutputStream(new File(path)));
					stream.write(bytes);
					stream.flush();
					stream.close();

				}catch (Exception e) {
					System.out.println("Eooror = "+e);
					// TODO: handle exception
				}


			}
		}
		catch(Exception e)
		{

		}

		File f = new File(path);

		if (f.exists() && !f.isDirectory()) 
		{
			return true;
		} 
		else 
		{
			return false;
		}
	}

	@RequestMapping(value="/uploadReraDocument",method=RequestMethod.POST)
	public String uploadReraDocument(@ModelAttribute ReraDocuments documentDetails,@RequestParam("document") MultipartFile file, Model model)
	{
		try {
			String extension="."+knowFileExtension(file.getOriginalFilename());
			try
			{
				String path = CommanController.GetReraDocumentPath() + "Rera_" +documentDetails.getProjectId()+"_"+documentDetails.getDocumentName()+"."+knowFileExtension(file.getOriginalFilename());
				UploadFileToPath(file, path);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}

			try
			{
				documentDetails = new ReraDocuments("Rera_"+documentDetails.getProjectId()+"_"+documentDetails.getDocumentName(), documentDetails.getProjectId(), documentDetails.getDocumentName(), extension , documentDetails.getCreationDate(), documentDetails.getUpdateDate());
				reradocumentRepository.save(documentDetails);
				model.addAttribute("documentStatus", "Success");
			}
			catch(Exception e)
			{

			}

			Query query = new Query();
			List<ReraDocuments> documentList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(documentDetails.getProjectId())), ReraDocuments.class);

			model.addAttribute("documentList", documentList);
			model.addAttribute("projectId", documentDetails.getProjectId());
			return "AddReraDocuments";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/ViewReraDocument")
	public String ViewCompanyDocument(@RequestParam("documentId") String documentId, Model model)
	{
		try {
			Query query = new Query();
			ReraDocuments documentDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("documentId").is(documentId)), ReraDocuments.class);

			String fileAddress = CommanController.GetReraDocumentPath()+documentId+documentDetails.getExtension();

			if(documentDetails.getExtension().equals(".jpg") || documentDetails.getExtension().equals(".jpeg") || documentDetails.getExtension().equals(".png"))
			{
				model.addAttribute("fileAddress", fileAddress);
				return "DisplayImage";	
			}	
			else
			{
				model.addAttribute("fileAddress", fileAddress);
				return "DisplayPDF";
			}

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/DeleteReraDocument")
	public List<ReraDocuments> DeleteReraDocument(@RequestParam("documentId") String documentId, @RequestParam("projectId") String projectId , Model model)
	{
		Query query = new Query();

		try
		{
			ReraDocuments documentDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("documentId").is(documentId)), ReraDocuments.class);

			String fileAddress = CommanController.GetReraDocumentPath()+documentId+documentDetails.getExtension();

			File file = new File(fileAddress);

			file.delete();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		try
		{
			query = new Query();
			mongoTemplate.remove(query.addCriteria(Criteria.where("documentId").is(documentId)), ReraDocuments.class);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		query = new Query();
		List<ReraDocuments> documentList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)), ReraDocuments.class);

		return documentList;
	}
}

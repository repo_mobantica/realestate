package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.realestate.bean.Bank;
import com.realestate.bean.City;
import com.realestate.bean.Company;
import com.realestate.bean.CompanyBanks;
import com.realestate.bean.Country;
import com.realestate.bean.Flat;
import com.realestate.bean.LocationArea;
import com.realestate.bean.State;
import com.realestate.repository.BankRepository;
import com.realestate.repository.CityRepository;
import com.realestate.repository.CompanyBanksRepository;
import com.realestate.repository.CompanyRepository;
import com.realestate.repository.CountryRepository;
import com.realestate.repository.LocationAreaRepository;
import com.realestate.repository.StateRepository;
import com.realestate.services.CompanyExcelView;

@Controller
@RequestMapping("/")
public class CompanyController 
{

	@Autowired
	LocationAreaRepository locationAreaRepository;
	@Autowired
	CityRepository cityRepository;
	@Autowired
	StateRepository stateRepository;
	@Autowired
	CountryRepository countryRepository;
	@Autowired
	CompanyRepository companyRepository;
	@Autowired
	BankRepository bankRepository;
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	CompanyBanksRepository companybanksRepository;
	//Code generation for company code
	String companyCode;
	private String CompanyCode()
	{
		long companyCount = companyRepository.count();

		if(companyCount<10)
		{
			companyCode = "CO000"+(companyCount+1);
		}
		else if((companyCount>=10) && (companyCount<100))
		{
			companyCode = "CO00"+(companyCount+1);
		}
		else if((companyCount>=100) && (companyCount<1000))
		{
			companyCode = "CO0"+(companyCount+1);
		}
		else
		{
			companyCode = "CO"+(companyCount+1);
		}

		return companyCode;
	}

	String companyBankCode;
	private String CompanyBankCode()
	{
		long companyCount = companybanksRepository.count();

		if(companyCount<10)
		{
			companyBankCode = "CB000"+(companyCount+1);
		}
		else if((companyCount>=10) && (companyCount<100))
		{
			companyBankCode = "CB00"+(companyCount+1);
		}
		else if((companyCount>=100) && (companyCount<1000))
		{
			companyBankCode = "CB0"+(companyCount+1);
		}
		else if(companyCount>1000)
		{
			companyBankCode = "CB"+(companyCount+1);
		}

		return companyBankCode;
	}

	//code for getting all country name
	private List<Country> findAllCountryId()
	{
		List<Country> countryList;

		countryList = countryRepository.findAll(new Sort(Sort.Direction.ASC,"countryId"));
		return countryList;
	}

	//code for getting all bank names
	private List<Bank> getAllBankName()
	{
		List<Bank> bankList= new ArrayList<Bank>();
		Query query = new Query();
		bankList = bankRepository.findAll(new Sort(Sort.Direction.ASC,"bankName"));
		return bankList;
	}


	//Request Mapping For Add Company
	@RequestMapping("/AddCompany")
	public String addCompany(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			String companyId=CompanyCode();
			List<Company> companyList = companyRepository.findAll(); 
			model.addAttribute("companyCode",companyId);

			List<CompanyBanks> companyBankList= new ArrayList<CompanyBanks>();
			Query query1 = new Query();  
			Query query2 = new Query();
			companyBankList = mongoTemplate.find(query2.addCriteria(Criteria.where("companyId").is(companyId)), CompanyBanks.class);
			if(companyBankList.size()!=0)
			{
				mongoTemplate.remove(query1.addCriteria(Criteria.where("companyId").is(companyId)), CompanyBanks.class);	
			}

			//model.addAttribute("companyBankList", companyBankList);
			model.addAttribute("countryList",findAllCountryId());
			model.addAttribute("bankList",getAllBankName());
			model.addAttribute("companyList",companyList);
			return "AddCompany";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/AddCompany",method=RequestMethod.POST)
	public String companySave(@ModelAttribute Company company, Model model)
	{
		try {
			try
			{
				company = new Company(company.getCompanyId(),company.getCompanyName().toUpperCase(),company.getCompanyType(),company.getCompanyAddress(),company.getCountryId(),company.getStateId(),company.getCityId(),company.getLocationareaId(),company.getCompanyPincode(),company.getCompanyPhoneno(),company.getCompanyPanno(),company.getCompanyRegistrationno(),company.getCompanyTanno(),company.getCompanyPfno(),company.getCompanyGstno(),company.getCompanyFinancialyear(),company.getCreationDate(),company.getUpdateDate(),company.getUserName());
				companyRepository.save(company);
				model.addAttribute("companyStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("companyStatus","Fail");
			}

			model.addAttribute("companyCode",CompanyCode());
			model.addAttribute("countryList",findAllCountryId());
			model.addAttribute("bankList",getAllBankName());
			return "AddCompany";

		}catch (Exception e) {
			return "login";
		}
	}

	//Request Mapping For Add Company
	@RequestMapping("/CompanyMaster")
	public String CompanyMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<Company> companyList = companyRepository.findAll(); 

			List<LocationArea> locationareaList= locationAreaRepository.findAll();
			List<City> cityList = cityRepository.findAll();
			List<State> stateList= stateRepository.findAll();
			List<Country> countryList= countryRepository.findAll();

			int i,j;
			for(i=0;i<companyList.size();i++)
			{
				try 
				{
					for(j=0;j<countryList.size();j++)
					{
						if(companyList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
						{
							companyList.get(i).setCountryId(countryList.get(j).getCountryName());
							break;
						}
					}

					for(j=0;j<stateList.size();j++)
					{
						if(companyList.get(i).getStateId().equals(stateList.get(j).getStateId()))
						{
							companyList.get(i).setStateId(stateList.get(j).getStateName());
							break;
						}
					}

					for(j=0;j<cityList.size();j++)
					{
						if(companyList.get(i).getCityId().equals(cityList.get(j).getCityId()))
						{
							companyList.get(i).setCityId(cityList.get(j).getCityName());
							break;
						}
					}

					for(j=0;j<locationareaList.size();j++)
					{
						if(companyList.get(i).getLocationareaId().equals(locationareaList.get(j).getLocationareaId()))
						{
							companyList.get(i).setLocationareaId(locationareaList.get(j).getLocationareaName());
							break;
						}
					}

				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}

			model.addAttribute("countryList",findAllCountryId());
			model.addAttribute("companyList",companyList);

			return "CompanyMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/searchCompanyNameWiseList")
	public List<Company> SearchCompanyNameWiseList(@RequestParam("companyName") String companyName)
	{
		Query query = new Query();

		List<Company> companyList = mongoTemplate.find(query.addCriteria(Criteria.where("companyName").regex("^"+companyName+".*","i")),Company.class);

		List<LocationArea> locationareaList= locationAreaRepository.findAll();
		List<City> cityList = cityRepository.findAll();
		List<State> stateList= stateRepository.findAll();
		List<Country> countryList= countryRepository.findAll();


		int i,j;
		for(i=0;i<companyList.size();i++)
		{
			try 
			{
				for(j=0;j<countryList.size();j++)
				{
					if(companyList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
					{
						companyList.get(i).setCountryId(countryList.get(j).getCountryName());
						break;
					}
				}

				for(j=0;j<stateList.size();j++)
				{
					if(companyList.get(i).getStateId().equals(stateList.get(j).getStateId()))
					{
						companyList.get(i).setStateId(stateList.get(j).getStateName());
						break;
					}
				}

				for(j=0;j<cityList.size();j++)
				{
					if(companyList.get(i).getCityId().equals(cityList.get(j).getCityId()))
					{
						companyList.get(i).setCityId(cityList.get(j).getCityName());
						break;
					}
				}

				for(j=0;j<locationareaList.size();j++)
				{
					if(companyList.get(i).getLocationareaId().equals(locationareaList.get(j).getLocationareaId()))
					{
						companyList.get(i).setLocationareaId(locationareaList.get(j).getLocationareaName());
						break;
					}
				}

			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		return companyList;
	}

	//to Retive All Company in Company Master by country name
	@ResponseBody
	@RequestMapping(value="/getCountryCompanyList",method=RequestMethod.POST)
	public List<Company> CountryWiseCompanyList(@RequestParam("countryId") String countryId, HttpSession session)
	{
		List<Company> companyList= new ArrayList<Company>();

		Query query = new Query();
		companyList = mongoTemplate.find(query.addCriteria(Criteria.where("countryId").is(countryId)), Company.class);

		List<LocationArea> locationareaList= locationAreaRepository.findAll();
		List<City> cityList = cityRepository.findAll();
		List<State> stateList= stateRepository.findAll();
		List<Country> countryList= countryRepository.findAll();


		int i,j;
		for(i=0;i<companyList.size();i++)
		{
			try 
			{
				for(j=0;j<countryList.size();j++)
				{
					if(companyList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
					{
						companyList.get(i).setCountryId(countryList.get(j).getCountryName());
						break;
					}
				}

				for(j=0;j<stateList.size();j++)
				{
					if(companyList.get(i).getStateId().equals(stateList.get(j).getStateId()))
					{
						companyList.get(i).setStateId(stateList.get(j).getStateName());
						break;
					}
				}

				for(j=0;j<cityList.size();j++)
				{
					if(companyList.get(i).getCityId().equals(cityList.get(j).getCityId()))
					{
						companyList.get(i).setCityId(cityList.get(j).getCityName());
						break;
					}
				}

				for(j=0;j<locationareaList.size();j++)
				{
					if(companyList.get(i).getLocationareaId().equals(locationareaList.get(j).getLocationareaId()))
					{
						companyList.get(i).setLocationareaId(locationareaList.get(j).getLocationareaName());
						break;
					}
				}

			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		return companyList;
	} 

	//to Retive All Company in Company Master by country name
	@ResponseBody
	@RequestMapping(value="/getCityWiseCompanyList",method=RequestMethod.POST)
	public List<Company> CityWiseCompanyList(@RequestParam("stateId") String stateId, HttpSession session)
	{
		List<Company> companyList= new ArrayList<Company>();

		Query query = new Query();
		companyList = mongoTemplate.find(query.addCriteria(Criteria.where("stateId").is(stateId)), Company.class);

		List<LocationArea> locationareaList= locationAreaRepository.findAll();
		List<City> cityList = cityRepository.findAll();
		List<State> stateList= stateRepository.findAll();
		List<Country> countryList= countryRepository.findAll();


		int i,j;
		for(i=0;i<companyList.size();i++)
		{
			try 
			{
				for(j=0;j<countryList.size();j++)
				{
					if(companyList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
					{
						companyList.get(i).setCountryId(countryList.get(j).getCountryName());
						break;
					}
				}

				for(j=0;j<stateList.size();j++)
				{
					if(companyList.get(i).getStateId().equals(stateList.get(j).getStateId()))
					{
						companyList.get(i).setStateId(stateList.get(j).getStateName());
						break;
					}
				}

				for(j=0;j<cityList.size();j++)
				{
					if(companyList.get(i).getCityId().equals(cityList.get(j).getCityId()))
					{
						companyList.get(i).setCityId(cityList.get(j).getCityName());
						break;
					}
				}

				for(j=0;j<locationareaList.size();j++)
				{
					if(companyList.get(i).getLocationareaId().equals(locationareaList.get(j).getLocationareaId()))
					{
						companyList.get(i).setLocationareaId(locationareaList.get(j).getLocationareaName());
						break;
					}
				}

			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		return companyList;
	} 

	//to Retive All Company in Company Master by country name
	@ResponseBody
	@RequestMapping(value="/getLocationAreaWiseCompanyList",method=RequestMethod.POST)
	public List<Company> AreaWiseCompanyList(@RequestParam("cityId") String cityId, HttpSession session)
	{
		List<Company> companyList= new ArrayList<Company>();

		Query query = new Query();
		companyList = mongoTemplate.find(query.addCriteria(Criteria.where("cityId").is(cityId)), Company.class);

		List<LocationArea> locationareaList= locationAreaRepository.findAll();
		List<City> cityList = cityRepository.findAll();
		List<State> stateList= stateRepository.findAll();
		List<Country> countryList= countryRepository.findAll();


		int i,j;
		for(i=0;i<companyList.size();i++)
		{
			try 
			{
				for(j=0;j<countryList.size();j++)
				{
					if(companyList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
					{
						companyList.get(i).setCountryId(countryList.get(j).getCountryName());
						break;
					}
				}

				for(j=0;j<stateList.size();j++)
				{
					if(companyList.get(i).getStateId().equals(stateList.get(j).getStateId()))
					{
						companyList.get(i).setStateId(stateList.get(j).getStateName());
						break;
					}
				}

				for(j=0;j<cityList.size();j++)
				{
					if(companyList.get(i).getCityId().equals(cityList.get(j).getCityId()))
					{
						companyList.get(i).setCityId(cityList.get(j).getCityName());
						break;
					}
				}

				for(j=0;j<locationareaList.size();j++)
				{
					if(companyList.get(i).getLocationareaId().equals(locationareaList.get(j).getLocationareaId()))
					{
						companyList.get(i).setLocationareaId(locationareaList.get(j).getLocationareaName());
						break;
					}
				}

			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		return companyList;
	} 

	@ResponseBody
	@RequestMapping(value="/getCompanyList",method=RequestMethod.POST)
	public List<Company> CompanyList(@RequestParam("locationareaId") String locationareaId, HttpSession session)
	{
		List<Company> companyList= new ArrayList<Company>();

		Query query = new Query();
		companyList = mongoTemplate.find(query.addCriteria(Criteria.where("locationareaId").is(locationareaId)), Company.class);

		List<LocationArea> locationareaList= locationAreaRepository.findAll();
		List<City> cityList = cityRepository.findAll();
		List<State> stateList= stateRepository.findAll();
		List<Country> countryList= countryRepository.findAll();


		int i,j;
		for(i=0;i<companyList.size();i++)
		{
			try 
			{
				for(j=0;j<countryList.size();j++)
				{
					if(companyList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
					{
						companyList.get(i).setCountryId(countryList.get(j).getCountryName());
						break;
					}
				}

				for(j=0;j<stateList.size();j++)
				{
					if(companyList.get(i).getStateId().equals(stateList.get(j).getStateId()))
					{
						companyList.get(i).setStateId(stateList.get(j).getStateName());
						break;
					}
				}

				for(j=0;j<cityList.size();j++)
				{
					if(companyList.get(i).getCityId().equals(cityList.get(j).getCityId()))
					{
						companyList.get(i).setCityId(cityList.get(j).getCityName());
						break;
					}
				}

				for(j=0;j<locationareaList.size();j++)
				{
					if(companyList.get(i).getLocationareaId().equals(locationareaList.get(j).getLocationareaId()))
					{
						companyList.get(i).setLocationareaId(locationareaList.get(j).getLocationareaName());
						break;
					}
				}

			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		return companyList;
	} 


	@ResponseBody
	@RequestMapping("/getcompanyList")
	public List<Company> AllcompanyList(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		List<Company> companyList= companyRepository.findAll();

		return companyList;
	}

	@RequestMapping("/EditCompany")
	public String EditCompany(@RequestParam("companyId") String companyId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<Company> companyDetails = mongoTemplate.find(query.addCriteria(Criteria.where("companyId").is(companyId)),Company.class);
			List<CompanyBanks> companyBankList= new ArrayList<CompanyBanks>();
			Query query2 = new Query();
			companyBankList = mongoTemplate.find(query2.addCriteria(Criteria.where("companyId").is(companyId).and("status").ne("Inactive")), CompanyBanks.class);

			try {
				query = new Query();
				Country countryDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("countryId").is(companyDetails.get(0).getCountryId())),Country.class);
				query = new Query();
				State stateDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("stateId").is(companyDetails.get(0).getStateId())),State.class);
				query = new Query();
				City cityDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("cityId").is(companyDetails.get(0).getCityId())),City.class);
				query = new Query();
				LocationArea locationareaDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("locationareaId").is(companyDetails.get(0).getLocationareaId())),LocationArea.class);

				model.addAttribute("countryName", countryDetails.getCountryName());
				model.addAttribute("stateName", stateDetails.getStateName());
				model.addAttribute("cityName", cityDetails.getCityName());
				model.addAttribute("locationareaName", locationareaDetails.getLocationareaName());
			}catch (Exception e) {
				// TODO: handle exception
			}

			model.addAttribute("companyBankList", companyBankList);
			model.addAttribute("companyDetails", companyDetails);
			model.addAttribute("countryList",findAllCountryId());
			model.addAttribute("bankList",getAllBankName());

			return "EditCompany";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/EditCompany",method=RequestMethod.POST)
	public String EditCompanyPostMethod(@ModelAttribute Company company, Model model)
	{
		try {
			try
			{
				company = new Company(company.getCompanyId(),company.getCompanyName().toUpperCase(),company.getCompanyType(),company.getCompanyAddress(),company.getCountryId(),company.getStateId(),company.getCityId(),company.getLocationareaId(),company.getCompanyPincode(),company.getCompanyPhoneno(),company.getCompanyPanno(),company.getCompanyRegistrationno(),company.getCompanyTanno(),company.getCompanyPfno(),company.getCompanyGstno(),company.getCompanyFinancialyear(),company.getCreationDate(),company.getUpdateDate(),company.getUserName());
				companyRepository.save(company);
				model.addAttribute("companyStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("companyStatus","Fail");
			}

			List<Company> companyList = companyRepository.findAll(); 

			List<LocationArea> locationareaList= locationAreaRepository.findAll();
			List<City> cityList = cityRepository.findAll();
			List<State> stateList= stateRepository.findAll();
			List<Country> countryList= countryRepository.findAll();


			int i,j;
			for(i=0;i<companyList.size();i++)
			{
				try 
				{
					for(j=0;j<countryList.size();j++)
					{
						if(companyList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
						{
							companyList.get(i).setCountryId(countryList.get(j).getCountryName());
							break;
						}
					}

					for(j=0;j<stateList.size();j++)
					{
						if(companyList.get(i).getStateId().equals(stateList.get(j).getStateId()))
						{
							companyList.get(i).setStateId(stateList.get(j).getStateName());
							break;
						}
					}

					for(j=0;j<cityList.size();j++)
					{
						if(companyList.get(i).getCityId().equals(cityList.get(j).getCityId()))
						{
							companyList.get(i).setCityId(cityList.get(j).getCityName());
							break;
						}
					}

					for(j=0;j<locationareaList.size();j++)
					{
						if(companyList.get(i).getLocationareaId().equals(locationareaList.get(j).getLocationareaId()))
						{
							companyList.get(i).setLocationareaId(locationareaList.get(j).getLocationareaName());
							break;
						}
					}

				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}

			model.addAttribute("countryList",findAllCountryId());
			model.addAttribute("companyList",companyList);

			return "CompanyMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping(value="/AddCompanyBank",method=RequestMethod.POST)
	public List<CompanyBanks> AddCompanyBank(@RequestParam("companyId") String companyId, @RequestParam("bankName") String bankName, @RequestParam("branchName") String branchName, @RequestParam("bankifscCode") String bankifscCode,  @RequestParam("companyBankacno") String companyBankacno, HttpSession session)
	{

		try {
			CompanyBanks companybanks=new CompanyBanks();
			companybanks.setCompanyBankId(CompanyBankCode());
			companybanks.setCompanyId(companyId);
			companybanks.setCompanyBankname(bankName);
			companybanks.setBranchName(branchName);
			companybanks.setBankifscCode(bankifscCode);
			companybanks.setCompanyBankacno(companyBankacno);
			companybanksRepository.save(companybanks);
		}
		catch(Exception ee)
		{
			System.out.println(ee);
		}


		List<CompanyBanks> companyBankList= new ArrayList<CompanyBanks>();
		Query query = new Query();
		companyBankList = mongoTemplate.find(query.addCriteria(Criteria.where("companyId").is(companyId)), CompanyBanks.class);
		return companyBankList;
	} 

	@ResponseBody
	@RequestMapping("/DeleteCompanyBank")
	public List<CompanyBanks> DeleteCompanyBank(@RequestParam("companyId") String companyId, @RequestParam("companyBankId") String companyBankId, HttpSession session)
	{
		try
		{
			CompanyBanks companybanks=new CompanyBanks();
			companybanks = mongoTemplate.findOne(Query.query(Criteria.where("companyId").is(companyId).and("companyBankId").is(companyBankId)), CompanyBanks.class);
			companybanks.setStatus("Inactive");
			companybanksRepository.save(companybanks);
			Query query = new Query();
			List<CompanyBanks> companyBankList = mongoTemplate.find(query.addCriteria(Criteria.where("companyId").is(companyId).and("status").ne("Inactive")), CompanyBanks.class);
			return companyBankList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping(value="/ExportAllCompanies", method=RequestMethod.GET)
	public ModelAndView generateExcel(HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		List<Company> companies = companyRepository.findAll();

		ModelAndView modelAndView = new ModelAndView(new CompanyExcelView(), "companies" ,companies);

		return modelAndView;
	}
}

package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.CriteriaDefinition;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.repository.ProjectRepository;
import com.realestate.repository.UserAssignedProjectRepository;
import com.realestate.repository.DepartmentRepository;
import com.realestate.repository.LoginRepository;
import com.realestate.repository.UserModelRepository;
import com.realestate.services.EmailService;
import com.realestate.bean.Department;
import com.realestate.bean.Employee;
import com.realestate.bean.Login;
import com.realestate.bean.Project;
import com.realestate.bean.UserAssignedProject;
import com.realestate.bean.UserModel;

@Controller
@RequestMapping("/")
public class UserModelController extends EmailService
{
	@Autowired
	UserModelRepository userModelRepository;
	@Autowired
	DepartmentRepository departmentRepository;
	@Autowired
	LoginRepository loginRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	UserAssignedProjectRepository userassignedprojectRepository;
	@Autowired
	MongoTemplate mongoTemplate;


	String userModelId = "";
	private String UserModelId()
	{
		long count = userModelRepository.count();

		if(count<10)
		{
			userModelId = "UM000"+(count+1);	 
		}
		else if(count>=10 && count<100)
		{
			userModelId = "UM00"+(count+1);
		}
		else if(count>=100 && count<1000)
		{
			userModelId = "UM0"+(count+1);
		}
		else
		{
			userModelId = "UM"+(count+1);
		}

		return userModelId;
	}

	String userId = "";
	public String UserId()
	{
		long count = loginRepository.count();

		if(count<10)
		{
			userId = "U000"+(count+1);	 
		}
		else if(count>=10 && count<100)
		{
			userId = "U00"+(count+1);
		}
		else if(count>=100 && count<1000)
		{
			userId = "U0"+(count+1);
		}
		else
		{
			userId = "U"+(count+1);
		}

		return userId;
	}

	protected String generatePassword()
	{
		String RandChar = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		StringBuilder pass = new StringBuilder();
		Random rnd = new Random();

		while ( pass.length()<=8 ) 
		{ 
			int index = (int) (rnd.nextFloat() * RandChar.length());
			pass.append(RandChar.charAt(index));
		}

		String passwordString = pass.toString();

		return passwordString;
	}

	@RequestMapping("/UserMaster")
	public String UserMaster(Model model)
	{
		try {
			Query query = new Query();
			List<UserModel> usersList1 = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("Active")), UserModel.class);

			List<UserModel> usersList = new ArrayList<UserModel>();

			query = new Query();
			List<Employee> employeeList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("Active")), Employee.class);

			query = new Query();
			List<Login> loginList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("Active")), Login.class);

			UserModel user = new UserModel();

			for(int i=0;i<usersList1.size();i++)
			{
				user = new UserModel();
				for(int j=0;j<employeeList.size();j++)
				{
					if(usersList1.get(i).getEmployeeId().equals(employeeList.get(j).getEmployeeId()))
					{
						user.setUserModelId(usersList1.get(i).getUserModelId());
						user.setEmployeeId(employeeList.get(j).getEmployeeId());
						user.setEmployeeName(employeeList.get(j).getEmployeefirstName()+" "+employeeList.get(j).getEmployeemiddleName()+" "+employeeList.get(j).getEmployeelastName());
						user.setRole(usersList1.get(i).getRole());

						usersList.add(user);
					}
				}
			}

			for(int i=0; i<usersList.size();i++)
			{
				for(int j=0; j<loginList.size(); j++)
				{
					if(usersList.get(i).getEmployeeId().equals(loginList.get(j).getEmployeeId()))
					{
						usersList.get(i).setUserId(loginList.get(j).getUserName());
						usersList.get(i).setPassWord(loginList.get(j).getPassWord());
					}
				}
			}

			List<Department> departmentList = departmentRepository.findAll();

			model.addAttribute("departmentList",departmentList);
			model.addAttribute("usersList", usersList);
			return "UserMaster";	

		}catch (Exception e) {
			return "login";
		}  
	}

	@RequestMapping(value="/AddUserModel")
	public String UserModel(Model model)
	{
		try {
			int f=0;
			Query query = new Query();
			List<Employee> employeeList1 = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("Active")), Employee.class);
			List<Employee> employeeList = new ArrayList<Employee>();

			List<Login> loginDetails = loginRepository.findAll();
			List<Project> projectList = projectRepository.findAll();

			Employee employee = new Employee();

			for(int i=0;i<employeeList1.size();i++)
			{
				f=0;
				employee = new Employee();
				for(int j=0;j<loginDetails.size();j++)
				{
					if(employeeList1.get(i).getEmployeeId().equals(loginDetails.get(j).getEmployeeId()))
					{
						f=1;
						break;
					}
				}

				if(f==0)
				{
					employee.setEmployeeId(employeeList1.get(i).getEmployeeId());
					employee.setEmployeefirstName(employeeList1.get(i).getEmployeefirstName());
					employee.setEmployeemiddleName(employeeList1.get(i).getEmployeemiddleName());
					employee.setEmployeelastName(employeeList1.get(i).getEmployeelastName());

					employeeList.add(employee);
				}
			}

			List<Department> departmentList = departmentRepository.findAll();

			query = new Query();
			List<UserAssignedProject> assignedProjectList = mongoTemplate.find(query.addCriteria(Criteria.where("userModelId").is(UserModelId())), UserAssignedProject.class);

			if(assignedProjectList.size()!=0)
			{
				query = new Query();
				mongoTemplate.remove(query.addCriteria(Criteria.where("userModelId").is(UserModelId())), UserAssignedProject.class);
			}

			model.addAttribute("userModelId", UserModelId());
			model.addAttribute("employeeList",employeeList);
			model.addAttribute("departmentList",departmentList);
			model.addAttribute("projectList",projectList);


			return "AddUserModel";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/AddUserModel", method=RequestMethod.POST)
	public String UserModelSave(@ModelAttribute UserModel userModel, Model model)
	{
		try {
			Login loginUser = new Login();
			try
			{
				userModel = new UserModel(userModel.getUserModelId(), userModel.getEmployeeId(), userModel.getRole(), 
						userModel.getAdministrator(), userModel.getPreSaleaManagment(), userModel.getSalesManagment(), 
						userModel.getFlatStatusReport(), userModel.getEnquiryReport(), userModel.getBookingReport(), userModel.getCustomerPaymentReport(), 
						userModel.getDemandPaymentReport(), userModel.getParkingReport(), userModel.getAgentReport(), userModel.getCustomerReport(), 
						userModel.getAddTask(), userModel.getHrANDpayRollManagement(), 
						userModel.getConsultancyMaster(), userModel.getArchitecturalSection(), 
						userModel.getContractorMagagement(), userModel.getProjectEngineering(), userModel.getJrEngineering(), userModel.getQualityEngineering(), userModel.getSrSupervisor(), userModel.getJrSupervisor(), 
						userModel.getMaterialRequisitionList(), 
						userModel.getStoreManagement(),userModel.getStoreStocks(), userModel.getMaterialTransferToContractor(),
						userModel.getCustomerPaymentStatus(), userModel.getSupplierPaymentBill(), userModel.getAgentPayment(), 
						userModel.getProjectDevelopmentWork(), userModel.getArchitectsCertificate(), userModel.getProjectSpecificationMaster(), 
						userModel.getUserManagement(), 
						userModel.getCompanyDocuments(),userModel.getLicensingDepartment(),userModel.getReraDocuments(), 
						userModel.getStatus(),userModel.getCreationDate(), userModel.getUpdateDate(), userModel.getUserName());
				userModelRepository.save(userModel);

				loginUser = new Login();

				Query query = new Query();
				Employee employeeDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("employeeId").is(userModel.getEmployeeId())), Employee.class);

				loginUser.setUserId(UserId());
				loginUser.setUserName(employeeDetails.getEmployeeEmailId());
				String pass = generatePassword();
				loginUser.setPassWord(pass);
				loginUser.setRole(userModel.getRole());
				loginUser.setEmployeeId(userModel.getEmployeeId());
				loginUser.setStatus("Active");
				loginUser.setCreationDate(userModel.getCreationDate());
				loginUser.setUpdateDate(userModel.getUpdateDate());

				loginRepository.save(loginUser);

				query = new Query();

				try
				{

					String Msgbody1 = "Hi, "+employeeDetails.getEmployeefirstName()+" "+employeeDetails.getEmployeelastName();
					String Msgbody2 = "\nYour account registered successfully. You can now login to qaerp.genieiot.in/";
					String Msgbody3 = "\nYou can use further login Credentials : ";
					String Msgbody4 =  "\nUsername : "+userModel.getEmployeeId()+"\n Password : "+pass;

					String emilId = employeeDetails.getEmployeeEmailId();

					sendLoginCredentialsMail(emilId,"Login Credentials", Msgbody1+"\n\t"+Msgbody2+"\n"+Msgbody3+"\n"+Msgbody4+" \n\n\t Please, do not reply to the mail.");
				}
				catch(Exception e)
				{
					System.out.println("Mail Sending Exception = "+e.toString());
				}


				//Code for user list

				query = new Query();
				List<UserModel> usersList1 = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("Active")), UserModel.class);

				List<UserModel> usersList = new ArrayList<UserModel>();

				query = new Query();
				List<Employee> employeeList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("Active")), Employee.class);

				query = new Query();
				List<Login> loginList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("Active")), Login.class);

				UserModel user = new UserModel();

				for(int i=0;i<usersList1.size();i++)
				{
					user = new UserModel();
					for(int j=0;j<employeeList.size();j++)
					{
						if(usersList1.get(i).getEmployeeId().equals(employeeList.get(j).getEmployeeId()))
						{
							user.setUserModelId(usersList1.get(i).getUserModelId());
							user.setEmployeeId(employeeList.get(j).getEmployeeId());
							user.setEmployeeName(employeeList.get(j).getEmployeefirstName()+" "+employeeList.get(j).getEmployeemiddleName()+" "+employeeList.get(j).getEmployeelastName());
							user.setRole(usersList1.get(i).getRole());

							usersList.add(user);
						}
					}
				}

				for(int i=0; i<usersList.size();i++)
				{
					for(int j=0; j<loginList.size(); j++)
					{
						if(usersList.get(i).getEmployeeId().equals(loginList.get(j).getEmployeeId()))
						{
							usersList.get(i).setUserId(loginList.get(j).getUserName());
							usersList.get(i).setPassWord(loginList.get(j).getPassWord());
						}
					}
				}

				List<Department> departmentList = departmentRepository.findAll();

				model.addAttribute("departmentList",departmentList);
				model.addAttribute("usersList", usersList);
				return "UserMaster";
			}
			catch(Exception e)
			{
				model.addAttribute("userStatus","Fail");
				model.addAttribute("userModelId", UserModelId());

				return "AddUserModel"; 
			}
			}catch (Exception e) {
				return "login";
			}
		
	}

	@RequestMapping(value="/EditUserModel")
	public String EditUserModel(@RequestParam("userModelId") String userModelId,Model model)
	{
		try {
			try
			{
				Query query = new Query();
				List<UserModel> userDetail = mongoTemplate.find(query.addCriteria(Criteria.where("userModelId").is(userModelId)), UserModel.class);

				query = new Query();
				List<Employee> employeeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(userDetail.get(0).getEmployeeId())), Employee.class);

				userDetail.get(0).setEmployeeName(employeeDetails.get(0).getEmployeefirstName()+" "+employeeDetails.get(0).getEmployeemiddleName()+" "+employeeDetails.get(0).getEmployeelastName());

				List<Department> departmentList = departmentRepository.findAll();

				List<Project> projectList = projectRepository.findAll();

				query = new Query();
				List<UserAssignedProject> assignedProjectList = mongoTemplate.find(query.addCriteria(Criteria.where("userModelId").is(userModelId)), UserAssignedProject.class);

				for(int i=0;i<assignedProjectList.size();i++)
				{
					query = new Query();
					Project projectDetails = new Project();
					projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(assignedProjectList.get(i).getProjectId())), Project.class);

					if(projectDetails != null)
					{
						assignedProjectList.get(i).setProjectName(projectDetails.getProjectName());
					}
				}

				model.addAttribute("projectList",projectList);
				model.addAttribute("departmentList",departmentList);
				model.addAttribute("userDetail", userDetail);
				model.addAttribute("assignedProjectList", assignedProjectList);

				return "EditUserModel";

			}
			catch(Exception e)
			{
				return "EditUserModel";
			}

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/EditUserModel",method=RequestMethod.POST)
	public String UpdateUserModel(@ModelAttribute UserModel userModel, Model model)
	{
		try {
			Query query = new Query();

			Login loginUser = new Login();

			try
			{
				userModel = new UserModel(userModel.getUserModelId(), userModel.getEmployeeId(), userModel.getRole(), 
						userModel.getAdministrator(), userModel.getPreSaleaManagment(), userModel.getSalesManagment(), 
						userModel.getFlatStatusReport(), userModel.getEnquiryReport(), userModel.getBookingReport(), userModel.getCustomerPaymentReport(), 
						userModel.getDemandPaymentReport(), userModel.getParkingReport(), userModel.getAgentReport(), userModel.getCustomerReport(), 
						userModel.getAddTask(), userModel.getHrANDpayRollManagement(), 
						userModel.getConsultancyMaster(), userModel.getArchitecturalSection(), 
						userModel.getContractorMagagement(), userModel.getProjectEngineering(), userModel.getJrEngineering(), userModel.getQualityEngineering(), userModel.getSrSupervisor(), userModel.getJrSupervisor(), 
						userModel.getMaterialRequisitionList(), 
						userModel.getStoreManagement(),userModel.getStoreStocks(), userModel.getMaterialTransferToContractor(),
						userModel.getCustomerPaymentStatus(), userModel.getSupplierPaymentBill(), userModel.getAgentPayment(), 
						userModel.getProjectDevelopmentWork(), userModel.getArchitectsCertificate(), userModel.getProjectSpecificationMaster(), 
						userModel.getUserManagement(), 
						userModel.getCompanyDocuments(),userModel.getLicensingDepartment(), userModel.getReraDocuments(), 
						userModel.getStatus(),userModel.getCreationDate(), userModel.getUpdateDate(), userModel.getUserName());
				userModelRepository.save(userModel);

				query = new Query();
				loginUser = mongoTemplate.findOne(query.addCriteria(Criteria.where("employeeId").is(userModel.getEmployeeId())), Login.class);

				loginUser.setRole(userModel.getRole());
				loginUser.setStatus(userModel.getStatus());
				loginUser.setCreationDate(userModel.getCreationDate());
				loginUser.setUpdateDate(userModel.getUpdateDate());

				loginRepository.save(loginUser);

				//Code for user list
				query = new Query();
				List<UserModel> usersList1 = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("Active")), UserModel.class);

				List<UserModel> usersList = new ArrayList<UserModel>();

				query = new Query();
				List<Employee> employeeList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("Active")), Employee.class);

				UserModel user = new UserModel();

				for(int i=0;i<usersList1.size();i++)
				{
					user = new UserModel();

					for(int j=0;j<employeeList.size();j++)
					{
						if(usersList1.get(i).getEmployeeId().equals(employeeList.get(j).getEmployeeId()))
						{
							user.setUserModelId(usersList1.get(i).getUserModelId());
							user.setEmployeeId(employeeList.get(j).getEmployeeId());
							user.setEmployeeName(employeeList.get(j).getEmployeefirstName()+" "+employeeList.get(j).getEmployeemiddleName()+" "+employeeList.get(j).getEmployeelastName());
							user.setRole(usersList1.get(i).getRole());

							usersList.add(user);
						}
					}
				}


				model.addAttribute("usersList", usersList);
				return "redirect:UserMaster";
			}
			catch(Exception e)
			{
				return "redirect:UserMaster";
			}

		}catch (Exception e) {
			return "login";
		}
	}


	public long ProjectAssignId()
	{
		long cnt = userassignedprojectRepository.count();

		if(cnt==0)
		{
			return (cnt+1);
		}
		else
		{
			List<UserAssignedProject> assignedProject = userassignedprojectRepository.findAll();
			long count = assignedProject.get(assignedProject.size()-1).getProjectassignId()+1;
			return count;
		}
	}

	@ResponseBody
	@RequestMapping("/AssignProject")
	public List<UserAssignedProject> AssignProject(@RequestParam("userModelId") String userModelId, @RequestParam("projectId") String projectId, @RequestParam("employeeId") String employeeId)
	{
		Query query = new Query();
		List<UserAssignedProject> assignedProjectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("userModelId").is(userModelId)), UserAssignedProject.class);
		int flag=0;

		try
		{
			if(projectId.equalsIgnoreCase("ALL") )
			{
				List<Project> projectList = projectRepository.findAll();

				for(int i=0;i<projectList.size();i++)
				{
					flag=0;
					for(int j=0;i<assignedProjectList1.size();j++)
					{
						if(projectList.get(i).getProjectId().equals(assignedProjectList1.get(j).getProjectId()))
						{
							flag=1;
							break;
						}
					}
					if(flag==0)
					{
						UserAssignedProject assignProject = new UserAssignedProject(ProjectAssignId(), userModelId, projectList.get(i).getProjectId(), employeeId);
						userassignedprojectRepository.save(assignProject);  
					}
				}
			}
			else
			{
				flag=0;
				for(int i=0;i<assignedProjectList1.size();i++)
				{
					if(assignedProjectList1.get(i).getProjectId().equals(projectId))
					{
						flag=1;
						break;
					}
				}

				if(flag==0)
				{
					UserAssignedProject assignProject = new UserAssignedProject(ProjectAssignId(), userModelId, projectId, employeeId);
					userassignedprojectRepository.save(assignProject);
				}
			}
		}
		catch(Exception e)
		{
			System.out.println("Exception While Saving = "+e.toString());
		}

		query = new Query();
		List<UserAssignedProject> assignedProjectList = mongoTemplate.find(query.addCriteria(Criteria.where("userModelId").is(userModelId)), UserAssignedProject.class);

		for(int i=0;i<assignedProjectList.size();i++)
		{
			query = new Query();
			Project projectDetails = new Project();
			projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(assignedProjectList.get(i).getProjectId())), Project.class);

			if(projectDetails != null)
			{
				assignedProjectList.get(i).setProjectName(projectDetails.getProjectName());
			}
		}

		return assignedProjectList;
	}

	@ResponseBody
	@RequestMapping("/DeleteAssignedProject")
	public List<UserAssignedProject> DeleteAssignedProject(@RequestParam("projectassignId") String projectassignId , @RequestParam("userModelId") String userModelId)
	{
		Query query = new Query();

		try
		{
			mongoTemplate.remove(query.addCriteria(Criteria.where("projectassignId").is(Long.parseLong(projectassignId))), UserAssignedProject.class);
		}
		catch(Exception e)
		{
			System.out.println("Exception While Deleting = "+e.toString());
		}

		query = new Query();
		List<UserAssignedProject> assignedProjectList = mongoTemplate.find(query.addCriteria(Criteria.where("userModelId").is(userModelId)), UserAssignedProject.class);

		for(int i=0;i<assignedProjectList.size();i++)
		{
			query = new Query();
			Project projectDetails = new Project();
			projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(assignedProjectList.get(i).getProjectId())), Project.class);

			if(projectDetails != null)
			{
				assignedProjectList.get(i).setProjectName(projectDetails.getProjectName());
			}
		}

		return assignedProjectList;
	}
}
package com.realestate.controller;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.realestate.bean.City;
import com.realestate.bean.Enquiry;
import com.realestate.bean.EnquirySource;
import com.realestate.bean.SubEnquirySource;
import com.realestate.repository.BankRepository;
import com.realestate.repository.BudgetRepository;
import com.realestate.repository.CountryRepository;
import com.realestate.repository.EnquiryRepository;
import com.realestate.repository.EnquirySourceRepository;
import com.realestate.repository.FlatRepository;
import com.realestate.repository.OccupationRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.SubEnquirySourceRepository;

@Controller
@RequestMapping("/")
public class ImportNewEnquiryController {


	@Autowired
	ServletContext context;

	@Autowired
	EnquirySourceRepository enquirysourceRepository;
	@Autowired
	SubEnquirySourceRepository subEnquirysourcerepository;
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	EnquiryRepository enquiryRepository;

	Enquiry enquiry = new Enquiry();
	HSSFWorkbook workbook;
	XSSFWorkbook workbook1;

	HSSFSheet worksheet;
	XSSFSheet worksheet1;


	int year = Calendar.getInstance().get(Calendar.YEAR);
	int month=Calendar.getInstance().get(Calendar.MONTH)+1;
	int y1,y2;
	//Code generation for enquiry
	String enquiryCode;
	private String EnquiryCode()
	{
		long enquiryCount = enquiryRepository.count();
		if(month>3) 
		{
			y1=year;
			y2=year+1;
			if(enquiryCount<10)
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/000"+(enquiryCount+1);
			}
			else if((enquiryCount>=10) && (enquiryCount<100))
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/00"+(enquiryCount+1);
			}
			else if((enquiryCount>=100) && (enquiryCount<1000))
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/0"+(enquiryCount+1);
			}
			else
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/"+(enquiryCount+1);
			}
		}
		else
		{
			y1=year-1;
			y2=year;
			if(enquiryCount<10)
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/000"+(enquiryCount+1);
			}
			else if((enquiryCount>=10) && (enquiryCount<100))
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/00"+(enquiryCount+1);
			}
			else if((enquiryCount>=100) && (enquiryCount<1000))
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/0"+(enquiryCount+1);
			}
			else 
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/"+(enquiryCount+1);
			}
		}
		return enquiryCode;
	}


	@RequestMapping(value="/ImportNewEnquiry")
	public String ImportNewEnquiry(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		return "ImportNewEnquiry";
	}


	@SuppressWarnings({ "deprecation"})
	@RequestMapping(value = "/ImportNewEnquiry", method = RequestMethod.POST)
	public String uploadCityDetails(@RequestParam("enquiry_excel") MultipartFile enquiry_excel, Model model, HttpServletRequest request, RedirectAttributes redirectAttributes, HttpSession session) 
	{
		String user = (String)request.getSession().getAttribute("user");

		List<EnquirySource> enquirysourceList = enquirysourceRepository.findAll();
		List<SubEnquirySource> subenquirysourceList = subEnquirysourcerepository.findAll();

		String subsourceId="",subsourceName="";
		String enquirysourceId="",enquirysourceName="";

		DateFormat dateFormat = new SimpleDateFormat("d/M/yyyy");

		String followupDate = dateFormat.format(new Date());

		if (!enquiry_excel.isEmpty() || enquiry_excel != null )
		{
			try 
			{
				if(enquiry_excel.getOriginalFilename().endsWith("xls") || enquiry_excel.getOriginalFilename().endsWith("xlsx") || enquiry_excel.getOriginalFilename().endsWith("csv"))
				{
					if(enquiry_excel.getOriginalFilename().endsWith("xlsx"))
					{
						InputStream stream = enquiry_excel.getInputStream();
						XSSFWorkbook workbook = new XSSFWorkbook(stream);

						XSSFSheet sheet = workbook.getSheet("Sheet1");  /// this will read 1st workbook of ExcelSheet

						int firstRow = sheet.getFirstRowNum();

						XSSFRow firstrow = sheet.getRow(firstRow);

						@SuppressWarnings("unused")
						int lastColumnCount = firstrow.getLastCellNum();

						@SuppressWarnings("unused")
						Iterator<Row> rowIterator = sheet.iterator();   
						int last_no=sheet.getLastRowNum();

						Enquiry enquiry = new Enquiry();

						for(int i=0;i<last_no;i++)
						{
							subsourceId="";
							subsourceName="";
							enquirysourceId="";
							enquirysourceName="";
							XSSFRow row = sheet.getRow(i+1);

							// Skip read heading 
							if (row.getRowNum() == 0) 
							{
								continue;
							}


							try {
								enquiry.setEnquiryId(EnquiryCode());

								row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
								enquiry.setEnqfirstName(row.getCell(0).getStringCellValue());

								row.getCell(1).setCellType(Cell.CELL_TYPE_STRING);
								enquiry.setEnqmobileNumber1(row.getCell(1).getStringCellValue());
								try
								{
									row.getCell(2).setCellType(Cell.CELL_TYPE_STRING);
									enquiry.setEnqmobileNumber2(row.getCell(2).getStringCellValue());
								}
								catch (Exception e) {
									// TODO: handle exception
								}

								try
								{
									//!= null ? row.getCell(5).toString() : ""
									row.getCell(3).setCellType(Cell.CELL_TYPE_STRING);
									enquiry.setEnqEmail(row.getCell(3).getStringCellValue()!= null ? row.getCell(3).toString() : "");
								}
								catch (Exception e) {
									// TODO: handle exception
								}
								try {
									row.getCell(4).setCellType(Cell.CELL_TYPE_STRING);
									enquiry.setFlatType(row.getCell(4).getStringCellValue());
								}catch (Exception e) {
									// TODO: handle exception
								}

								try
								{

									row.getCell(5).setCellType(Cell.CELL_TYPE_STRING);
									enquiry.setFlatBudget(row.getCell(5).getStringCellValue());

									enquirysourceId="";
									row.getCell(6).setCellType(Cell.CELL_TYPE_STRING);
									enquirysourceName=row.getCell(6).getStringCellValue();
									for(int k=0;k<enquirysourceList.size();k++)
									{
										if(enquirysourceName.equalsIgnoreCase(enquirysourceList.get(k).getEnquirysourceName()))
										{
											enquirysourceId=enquirysourceList.get(k).getEnquirysourceId();
										}
									}

									subsourceId="";
									row.getCell(7).setCellType(Cell.CELL_TYPE_STRING);
									subsourceName=row.getCell(7).getStringCellValue();
									for(int k=0;k<subenquirysourceList.size();k++)
									{
										if(subsourceName.equalsIgnoreCase(subenquirysourceList.get(k).getSubenquirysourceName()))
										{
											subsourceId=subenquirysourceList.get(k).getSubsourceId();
											break;
										}
									}

									enquiry.setEnquirysourceId(enquirysourceId);
									enquiry.setSubsourceId(subsourceId);
								}
								catch (Exception e) {
									// TODO: handle exception
								}
								try
								{
									row.getCell(8).setCellType(Cell.CELL_TYPE_STRING);
									enquiry.setEnqOccupation(row.getCell(8).getStringCellValue());
								}
								catch (Exception e) {
									// TODO: handle exception
								}

								enquiry.setEnqStatus("In-Process");

								enquiry.setCreationDate(new Date());
								enquiry.setFollowupDate(followupDate);

								enquiry.setUpdateDate(new Date());
								enquiry.setUserName(user);
								enquiryRepository.save(enquiry);
							}
							catch (Exception e) {
							}

						}
						workbook.close();
					}

				}//if after inner if

			}
			catch(Exception e)
			{

			}
		}

		return "ImportNewEnquiry";
	}


	//private static final String INTERNAL_FILE="Newcity.xlsx";
	@RequestMapping(value = "/DownloadEnquiryTemplate")
	public String DownloadEnquiryTemplate(Model model, HttpServletResponse response, HttpServletRequest request, RedirectAttributes redirectAttributes, HttpSession session) throws IOException 
	{

		String filename = "NewEnquiry.xlsx";
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		String filepath = "//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"Template/";
		response.setContentType("APPLICATION/OCTET-STREAM");
		response.setHeader("Content-Disposition", "attachment; filename=\""+ filename + "\"");


		FileInputStream fileInputStream = new FileInputStream(filepath+filename);

		int i;
		while ((i = fileInputStream.read()) != -1) {
			out.write(i);
		}
		fileInputStream.close();
		out.close();

		return "ImportNewEnquiry";
	}

}

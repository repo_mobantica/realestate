package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.realestate.bean.Bank;
import com.realestate.bean.BankEmployees;
import com.realestate.bean.City;
import com.realestate.bean.Contractor;
import com.realestate.bean.ContractorEmployees;
import com.realestate.bean.Country;
import com.realestate.bean.Designation;
import com.realestate.bean.LocationArea;
import com.realestate.bean.State;
import com.realestate.bean.SubContractorType;
import com.realestate.bean.Tax;
import com.realestate.bean.ContractorType;
import com.realestate.repository.BankRepository;
import com.realestate.repository.CityRepository;
import com.realestate.repository.ContractorEmployeesRepository;
import com.realestate.repository.ContractorRepository;
import com.realestate.repository.ContractorTypeRepository;
import com.realestate.repository.CountryRepository;
import com.realestate.repository.DesignationRepository;
import com.realestate.repository.LocationAreaRepository;
import com.realestate.repository.StateRepository;
import com.realestate.repository.SubContractorTypeRepository;
import com.realestate.services.ContractorDetailsExcelView;
import com.realestate.repository.ContractorTypeRepository;

@Controller
@RequestMapping("/")
public class ContractorController {

	@Autowired
	SubContractorTypeRepository subcontractortypeRepository;
	@Autowired
	ContractorRepository contractorRepository;
	@Autowired
	LocationAreaRepository locationAreaRepository;
	@Autowired
	CityRepository cityRepository;
	@Autowired
	StateRepository stateRepository;
	@Autowired
	CountryRepository countryRepository;
	@Autowired
	ContractorTypeRepository contractortypeRepository;
	@Autowired
	BankRepository bankRepository;
	@Autowired
	ContractorEmployeesRepository contractorEmployeesRepository;
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	DesignationRepository designationRepository;

	String contractorCode;
	private String ContratorCode()
	{
		long cnt  = contractorRepository.count();
		if(cnt<10)
		{
			contractorCode = "CR000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			contractorCode = "CR00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			contractorCode = "CR0"+(cnt+1);
		}
		else
		{
			contractorCode = "CR"+(cnt+1);
		}
		return contractorCode;
	}
	//Code For Retrieving Contractor Type List
	private List<ContractorType> findAllContractorType()
	{
		List<ContractorType> contractortypeList;

		contractortypeList = contractortypeRepository.findAll(new Sort(Sort.Direction.ASC,"contractorType"));
		return contractortypeList;
	}
	//Code For Retrieving Country Names
	private List<Country> findAllCountryName()
	{
		List<Country> countryList;

		countryList = countryRepository.findAll(new Sort(Sort.Direction.ASC,"countryId"));
		return countryList;
	}

	//code for retrieving all bank names
	private List<Bank> getAllBankNames()
	{
		List<Bank> bankList= new ArrayList<Bank>();
		Query query = new Query();
		bankList = bankRepository.findAll(new Sort(Sort.Direction.ASC,"bankName"));
		return bankList;
	}

	@RequestMapping("/AddContractor")
	public String addContractor(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
		try
		{
			Query query = new Query();
			Query query1 = new Query();

			List<Contractor> contractorList = contractorRepository.findAll(); 
			List<Designation> designationList = designationRepository.findAll();

			model.addAttribute("countryList",findAllCountryName());
			model.addAttribute("contractorCode",ContratorCode());
			model.addAttribute("bankList",getAllBankNames());
			model.addAttribute("designationList",designationList);
			model.addAttribute("contractorList",contractorList);
			model.addAttribute("contractortypeList",findAllContractorType());

			String contractorId = ContratorCode();

			List<ContractorEmployees> contractorEmployeeList = mongoTemplate.find(query.addCriteria(Criteria.where("contractorId").is(contractorId)), ContractorEmployees.class);

			if(contractorEmployeeList.size()!=0)
			{
				mongoTemplate.remove(query1.addCriteria(Criteria.where("contractorId").is(contractorId)), ContractorEmployees.class);	
			}
		}
		catch(Exception e)
		{

		}

		return "AddContractor";
		
		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value = "/AddContractor", method = RequestMethod.POST)
	public String contractorSave(@ModelAttribute Contractor contractor, Model model)
	{
		try {
		try
		{
			contractor = new Contractor(contractor.getContractorId(), contractor.getContractorfirmName(), contractor.getContractorfirmType(), 
					contractor.getFirmpanNumber(), contractor.getFirmgstNumber(),contractor.getTdspercentage(), contractor.getInsuranceStatus(),
					contractor.getContractorfirmAddress(), contractor.getCountryId(), contractor.getStateId(), contractor.getCityId(), contractor.getLocationareaId(), contractor.getContractorPincode(), 
					contractor.getBankName(), contractor.getBranchName(), contractor.getBankifscCode(), contractor.getFirmbankacNumber(), contractor.getContractortypeId(), contractor.getSubcontractortypeId(), contractor.getContractorpaymentTerms(), contractor.getCheckPrintingName(), contractor.getCreationDate(), contractor.getUpdateDate(), contractor.getUserName());
			contractorRepository.save(contractor);
			model.addAttribute("Status","Success");
		}
		catch(DuplicateKeyException de)
		{
			model.addAttribute("Status","Fail");
		}

		model.addAttribute("contractorCode",ContratorCode());
		model.addAttribute("countryList",findAllCountryName());
		model.addAttribute("bankList",getAllBankNames());
		return "AddContractor";
		
		}catch (Exception e) {
			return "login";
		}
	}

	//Request Mapping For  Contractor Master
	@RequestMapping("/ContractorMaster")
	public String contractorMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
		List<Contractor> contractorList = contractorRepository.findAll(); 

		List<LocationArea> locationareaList= locationAreaRepository.findAll();
		List<City> cityList = cityRepository.findAll();
		List<State> stateList= stateRepository.findAll();
		List<Country> countryList= countryRepository.findAll();

		int i,j;
		for(i=0;i<contractorList.size();i++)
		{
			try 
			{
				for(j=0;j<countryList.size();j++)
				{
					if(contractorList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
					{
						contractorList.get(i).setCountryId(countryList.get(j).getCountryName());
						break;
					}
				}

				for(j=0;j<stateList.size();j++)
				{
					if(contractorList.get(i).getStateId().equals(stateList.get(j).getStateId()))
					{
						contractorList.get(i).setStateId(stateList.get(j).getStateName());
						break;
					}
				}

				for(j=0;j<cityList.size();j++)
				{
					if(contractorList.get(i).getCityId().equals(cityList.get(j).getCityId()))
					{
						contractorList.get(i).setCityId(cityList.get(j).getCityName());
						break;
					}
				}

				for(j=0;j<locationareaList.size();j++)
				{
					if(contractorList.get(i).getLocationareaId().equals(locationareaList.get(j).getLocationareaId()))
					{
						contractorList.get(i).setLocationareaId(locationareaList.get(j).getLocationareaName());
						break;
					}
				}

			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		model.addAttribute("countryList", findAllCountryName());
		model.addAttribute("contractorList",contractorList);
		return "ContractorMaster";
		
		}catch (Exception e) {
			return "login";
		}
	}
	@ResponseBody
	@RequestMapping(value="/getCountryContractorList",method=RequestMethod.POST)
	public List<Contractor> CountryContractorList(@RequestParam("countryId") String countryId, HttpSession session)
	{
		List<Contractor> contractorList= new ArrayList<Contractor>();

		Query query = new Query();
		contractorList = mongoTemplate.find(query.addCriteria(Criteria.where("countryId").is(countryId)), Contractor.class);

		List<LocationArea> locationareaList= locationAreaRepository.findAll();
		List<City> cityList = cityRepository.findAll();
		List<State> stateList= stateRepository.findAll();
		List<Country> countryList= countryRepository.findAll();

		int i,j;
		for(i=0;i<contractorList.size();i++)
		{
			try 
			{
				for(j=0;j<countryList.size();j++)
				{
					if(contractorList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
					{
						contractorList.get(i).setCountryId(countryList.get(j).getCountryName());
						break;
					}
				}

				for(j=0;j<stateList.size();j++)
				{
					if(contractorList.get(i).getStateId().equals(stateList.get(j).getStateId()))
					{
						contractorList.get(i).setStateId(stateList.get(j).getStateName());
						break;
					}
				}

				for(j=0;j<cityList.size();j++)
				{
					if(contractorList.get(i).getCityId().equals(cityList.get(j).getCityId()))
					{
						contractorList.get(i).setCityId(cityList.get(j).getCityName());
						break;
					}
				}

				for(j=0;j<locationareaList.size();j++)
				{
					if(contractorList.get(i).getLocationareaId().equals(locationareaList.get(j).getLocationareaId()))
					{
						contractorList.get(i).setLocationareaId(locationareaList.get(j).getLocationareaName());
						break;
					}
				}

			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}
		return contractorList;
	}

	@ResponseBody
	@RequestMapping(value="/getStateWiseContractorList",method=RequestMethod.POST)
	public List<Contractor> stateContractorList(@RequestParam("stateId") String stateId, @RequestParam("countryId") String countryId, HttpSession session)
	{

		List<Contractor> contractorList= new ArrayList<Contractor>();

		Query query = new Query();
		// contractorList = mongoTemplate.find(query.addCriteria(Criteria.where("stateId").is(stateId)), Contractor.class);
		contractorList = mongoTemplate.find(query.addCriteria(Criteria.where("countryId").is(countryId).and("stateId").is(stateId)), Contractor.class);

		List<LocationArea> locationareaList= locationAreaRepository.findAll();
		List<City> cityList = cityRepository.findAll();
		List<State> stateList= stateRepository.findAll();
		List<Country> countryList= countryRepository.findAll();

		int i,j;
		for(i=0;i<contractorList.size();i++)
		{
			try 
			{
				for(j=0;j<countryList.size();j++)
				{
					if(contractorList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
					{
						contractorList.get(i).setCountryId(countryList.get(j).getCountryName());
						break;
					}
				}

				for(j=0;j<stateList.size();j++)
				{
					if(contractorList.get(i).getStateId().equals(stateList.get(j).getStateId()))
					{
						contractorList.get(i).setStateId(stateList.get(j).getStateName());
						break;
					}
				}

				for(j=0;j<cityList.size();j++)
				{
					if(contractorList.get(i).getCityId().equals(cityList.get(j).getCityId()))
					{
						contractorList.get(i).setCityId(cityList.get(j).getCityName());
						break;
					}
				}

				for(j=0;j<locationareaList.size();j++)
				{
					if(contractorList.get(i).getLocationareaId().equals(locationareaList.get(j).getLocationareaId()))
					{
						contractorList.get(i).setLocationareaId(locationareaList.get(j).getLocationareaName());
						break;
					}
				}

			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}
		return contractorList;
	}

	@ResponseBody
	@RequestMapping(value="/getCityWiseContratorList",method=RequestMethod.POST)
	public List<Contractor> cityContractorList(@RequestParam("cityId") String cityId, @RequestParam("stateId") String stateId, @RequestParam("countryId") String countryId, HttpSession session)
	{

		List<Contractor> contractorList= new ArrayList<Contractor>();

		Query query = new Query();
		contractorList = mongoTemplate.find(query.addCriteria(Criteria.where("countryId").is(countryId).and("stateId").is(stateId).and("cityId").is(cityId)), Contractor.class);

		List<LocationArea> locationareaList= locationAreaRepository.findAll();
		List<City> cityList = cityRepository.findAll();
		List<State> stateList= stateRepository.findAll();
		List<Country> countryList= countryRepository.findAll();

		int i,j;
		for(i=0;i<contractorList.size();i++)
		{
			try 
			{
				for(j=0;j<countryList.size();j++)
				{
					if(contractorList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
					{
						contractorList.get(i).setCountryId(countryList.get(j).getCountryName());
						break;
					}
				}

				for(j=0;j<stateList.size();j++)
				{
					if(contractorList.get(i).getStateId().equals(stateList.get(j).getStateId()))
					{
						contractorList.get(i).setStateId(stateList.get(j).getStateName());
						break;
					}
				}

				for(j=0;j<cityList.size();j++)
				{
					if(contractorList.get(i).getCityId().equals(cityList.get(j).getCityId()))
					{
						contractorList.get(i).setCityId(cityList.get(j).getCityName());
						break;
					}
				}

				for(j=0;j<locationareaList.size();j++)
				{
					if(contractorList.get(i).getLocationareaId().equals(locationareaList.get(j).getLocationareaId()))
					{
						contractorList.get(i).setLocationareaId(locationareaList.get(j).getLocationareaName());
						break;
					}
				}

			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}
		return contractorList;
	}

	@ResponseBody
	@RequestMapping(value="/getContratorList",method=RequestMethod.POST)
	public List<Contractor> areaContractorList(@RequestParam("locationareaId") String locationareaId, @RequestParam("cityId") String cityId, @RequestParam("stateId") String stateId, @RequestParam("countryId") String countryId, HttpSession session)
	{
		List<Contractor> contractorList= new ArrayList<Contractor>();

		Query query = new Query();

		contractorList = mongoTemplate.find(query.addCriteria(Criteria.where("countryId").is(countryId).and("stateId").is(stateId).and("cityId").is(cityId).and("locationareaId").is(locationareaId)), Contractor.class);

		List<LocationArea> locationareaList= locationAreaRepository.findAll();
		List<City> cityList = cityRepository.findAll();
		List<State> stateList= stateRepository.findAll();
		List<Country> countryList= countryRepository.findAll();

		int i,j;
		for(i=0;i<contractorList.size();i++)
		{
			try 
			{
				for(j=0;j<countryList.size();j++)
				{
					if(contractorList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
					{
						contractorList.get(i).setCountryId(countryList.get(j).getCountryName());
						break;
					}
				}

				for(j=0;j<stateList.size();j++)
				{
					if(contractorList.get(i).getStateId().equals(stateList.get(j).getStateId()))
					{
						contractorList.get(i).setStateId(stateList.get(j).getStateName());
						break;
					}
				}

				for(j=0;j<cityList.size();j++)
				{
					if(contractorList.get(i).getCityId().equals(cityList.get(j).getCityId()))
					{
						contractorList.get(i).setCityId(cityList.get(j).getCityName());
						break;
					}
				}

				for(j=0;j<locationareaList.size();j++)
				{
					if(contractorList.get(i).getLocationareaId().equals(locationareaList.get(j).getLocationareaId()))
					{
						contractorList.get(i).setLocationareaId(locationareaList.get(j).getLocationareaName());
						break;
					}
				}

			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}
		return contractorList;
	}

	@ResponseBody
	@RequestMapping("/ContractorNameWiseList")
	public List<Contractor> ContractorNameWiseSearch(@RequestParam("contractorName") String contractorName)
	{
		Query query = new Query();

		List<Contractor> contractorList= mongoTemplate.find(query.addCriteria(Criteria.where("contractorfirstName").regex("^"+contractorName+".*","i")),Contractor.class);
		return contractorList;
	}

	@RequestMapping("/EditContractor")
	public String EditContractor(@RequestParam("contractorId") String contractorId, ModelMap model)
	{
		try {
		Query query = new Query();
		List<Contractor> contractorDetails =  mongoTemplate.find(query.addCriteria(Criteria.where("contractorId").is(contractorId)),Contractor.class);

		query = new Query();
		List<ContractorType> contractortypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("contractortypeId").is(contractorDetails.get(0).getContractortypeId())), ContractorType.class);

		query = new Query();
		List<SubContractorType> subcontractortypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subcontractortypeId").is(contractorDetails.get(0).getSubcontractortypeId())), SubContractorType.class);

		query = new Query();
		List<ContractorEmployees> contractorEmployeesList = mongoTemplate.find(query.addCriteria(Criteria.where("contractorId").is(contractorId)), ContractorEmployees.class);

		List<Designation> designationList = designationRepository.findAll();

		try {
			query = new Query();
			Country countryDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("countryId").is(contractorDetails.get(0).getCountryId())),Country.class);
			query = new Query();
			State stateDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("stateId").is(contractorDetails.get(0).getStateId())),State.class);
			query = new Query();
			City cityDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("cityId").is(contractorDetails.get(0).getCityId())),City.class);
			query = new Query();
			LocationArea locationareaDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("locationareaId").is(contractorDetails.get(0).getLocationareaId())),LocationArea.class);

			model.addAttribute("countryName", countryDetails.getCountryName());
			model.addAttribute("stateName", stateDetails.getStateName());
			model.addAttribute("cityName", cityDetails.getCityName());
			model.addAttribute("locationareaName", locationareaDetails.getLocationareaName());
		}catch (Exception e) {
			// TODO: handle exception
		}

		model.addAttribute("contractorType",contractortypeDetails.get(0).getContractorType());
		model.addAttribute("subcontractorType",subcontractortypeDetails.get(0).getSubcontractorType());	

		model.addAttribute("contractortypeList",findAllContractorType());
		model.addAttribute("contractorDetails",contractorDetails);
		model.addAttribute("countryList",findAllCountryName());
		model.addAttribute("bankList",getAllBankNames());
		model.addAttribute("contractorEmployeesList", contractorEmployeesList);
		model.addAttribute("designationList", designationList);

		return "EditContractor";
		
		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/EditContractor", method=RequestMethod.POST)
	public String EditContractorPostMethod(@ModelAttribute Contractor contractor, Model model)
	{
		try {
		try
		{
			contractor = new Contractor(contractor.getContractorId(), contractor.getContractorfirmName(), contractor.getContractorfirmType(), 
					contractor.getFirmpanNumber(), contractor.getFirmgstNumber(),contractor.getTdspercentage(),contractor.getInsuranceStatus(),
					contractor.getContractorfirmAddress(), contractor.getCountryId(), contractor.getStateId(), contractor.getCityId(), contractor.getLocationareaId(), contractor.getContractorPincode(), 
					contractor.getBankName(), contractor.getBranchName(), contractor.getBankifscCode(), contractor.getFirmbankacNumber(), contractor.getContractortypeId(), contractor.getSubcontractortypeId(), contractor.getContractorpaymentTerms(), contractor.getCheckPrintingName(), contractor.getCreationDate(), contractor.getUpdateDate(), contractor.getUserName());
			contractorRepository.save(contractor);
			model.addAttribute("Status","Success");
		}
		catch(DuplicateKeyException de)
		{
			model.addAttribute("Status","Fail");
		}

		List<Contractor> contractorList = contractorRepository.findAll(); 

		List<LocationArea> locationareaList= locationAreaRepository.findAll();
		List<City> cityList = cityRepository.findAll();
		List<State> stateList= stateRepository.findAll();
		List<Country> countryList= countryRepository.findAll();

		int i,j;
		for(i=0;i<contractorList.size();i++)
		{
			try 
			{
				for(j=0;j<countryList.size();j++)
				{
					if(contractorList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
					{
						contractorList.get(i).setCountryId(countryList.get(j).getCountryName());
						break;
					}
				}

				for(j=0;j<stateList.size();j++)
				{
					if(contractorList.get(i).getStateId().equals(stateList.get(j).getStateId()))
					{
						contractorList.get(i).setStateId(stateList.get(j).getStateName());
						break;
					}
				}

				for(j=0;j<cityList.size();j++)
				{
					if(contractorList.get(i).getCityId().equals(cityList.get(j).getCityId()))
					{
						contractorList.get(i).setCityId(cityList.get(j).getCityName());
						break;
					}
				}

				for(j=0;j<locationareaList.size();j++)
				{
					if(contractorList.get(i).getLocationareaId().equals(locationareaList.get(j).getLocationareaId()))
					{
						contractorList.get(i).setLocationareaId(locationareaList.get(j).getLocationareaName());
						break;
					}
				}

			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}
		model.addAttribute("countryList", findAllCountryName());
		model.addAttribute("contractorList",contractorList);

		return "ContractorMaster";
		
		}catch (Exception e) {
			return "login";
		}
	}

	public int Generate_ContractorEmployeeId()
	{
		int contractorEmployeeId = 0;

		List<ContractorEmployees> contractorList = contractorEmployeesRepository.findAll();

		int size = contractorList.size();

		if(size!=0)
		{
			contractorEmployeeId = contractorList.get(size-1).getContractorEmployeeId();
		}

		return contractorEmployeeId+1;
	}

	@ResponseBody
	@RequestMapping("/AddContractorEmployee")
	public List<ContractorEmployees> AddContractorEmployee(@RequestParam("contractorId") String contractorId, @RequestParam("employeeName") String employeeName, @RequestParam("employeeDesignation") String employeeDesignation, @RequestParam("employeeEmail") String employeeEmail, @RequestParam("employeeMobileno") String employeeMobileno, HttpSession session)
	{
		try
		{
			ContractorEmployees contractorEmployees = new ContractorEmployees();
			contractorEmployees.setContractorEmployeeId(Generate_ContractorEmployeeId());
			contractorEmployees.setContractorId(contractorId);
			contractorEmployees.setEmployeeName(employeeName);
			contractorEmployees.setEmployeeDesignation(employeeDesignation);
			contractorEmployees.setEmployeeEmail(employeeEmail);
			contractorEmployees.setEmployeeMobileno(employeeMobileno);

			contractorEmployeesRepository.save(contractorEmployees);

			Query query = new Query();
			List<ContractorEmployees> contractorEmployeesList = mongoTemplate.find(query.addCriteria(Criteria.where("contractorId").is(contractorId)), ContractorEmployees.class);

			return contractorEmployeesList;
		}
		catch(Exception e)
		{

			return null;
		}

	}

	@ResponseBody
	@RequestMapping("/DeleteContractorEmployee")
	public List<ContractorEmployees> DeleteBankEmployee(@RequestParam("contractorId") String contractorId, @RequestParam("contractorEmployeeId") String contractorEmployeeId, HttpSession session)
	{
		try
		{
			Query query = new Query();
			mongoTemplate.remove(query.addCriteria(Criteria.where("contractorEmployeeId").is(Integer.parseInt(contractorEmployeeId)).and("contractorId").is(contractorId)), ContractorEmployees.class);

			query = new Query();
			List<ContractorEmployees> contractorEmployeeList = mongoTemplate.find(query.addCriteria(Criteria.where("contractorId").is(contractorId)), ContractorEmployees.class);

			return contractorEmployeeList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@ResponseBody
	@RequestMapping(value="/getContractorGstCostList",method=RequestMethod.POST)
	public List<Tax> getGstCostList(HttpSession session)
	{
		List<Tax> taxList=new ArrayList<Tax>();
		Query query1 = new Query();
		taxList = mongoTemplate.find(query1.addCriteria(Criteria.where("taxType").is("CONTRACTOR")), Tax.class);
		return taxList;
	}

	@RequestMapping(value="/ExportContractorList", method=RequestMethod.GET)
	public ModelAndView generateExcel(HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		List<Contractor> contractorDetails = contractorRepository.findAll();
		List<ContractorType> contractortypeList = contractortypeRepository.findAll();
		List<SubContractorType> subcontractortypeList = subcontractortypeRepository.findAll();

		String contractorType="",contractorSubType="";

		for(int i=0;i<contractorDetails.size();i++)
		{
			contractorType="";contractorSubType="";

			for(int j=0;j<contractortypeList.size();j++)
			{
				if(contractorDetails.get(i).getContractortypeId().equals(contractortypeList.get(j).getContractorType()))
				{
					contractorType=contractortypeList.get(j).getContractorType();
					break;
				}
			}
			for(int j=0;j<subcontractortypeList.size();j++)
			{
				if(contractorDetails.get(i).getSubcontractortypeId().equals(subcontractortypeList.get(j).getSubcontractortypeId()))
				{
					contractorSubType=subcontractortypeList.get(j).getSubcontractorType();
					break;
				}
			}
			contractorDetails.get(i).setContractortypeId(contractorType);
			contractorDetails.get(i).setSubcontractortypeId(contractorSubType);
		}

		ModelAndView modelAndView = new ModelAndView(new ContractorDetailsExcelView(), "contractorDetails" , contractorDetails);

		return modelAndView;
	}	    
}

package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Agent;
import com.realestate.bean.City;
import com.realestate.bean.Country;
import com.realestate.bean.LocationArea;
import com.realestate.bean.State;
import com.realestate.repository.CityRepository;
import com.realestate.repository.CountryRepository;
import com.realestate.repository.LocationAreaRepository;
import com.realestate.repository.StateRepository;


@Controller
@RequestMapping("/")
public class LocationAreaController 
{
	@Autowired
	LocationAreaRepository locationAreaRepository;
	@Autowired
	CityRepository cityRepository;
	@Autowired
	StateRepository stateRepository;
	@Autowired
	CountryRepository countryRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	//Request Mapping For Location Area Code Geration
	String locationAreaCode;
	private String locationAreaCode()
	{
		long locationAreaCount  = locationAreaRepository.count();

		if(locationAreaCount<10)
		{
			locationAreaCode = "LA000"+(locationAreaCount+1);
		}
		else if((locationAreaCount>=10) && (locationAreaCount<100))
		{
			locationAreaCode = "LA00"+(locationAreaCount+1);
		}
		else if((locationAreaCount>=100) && (locationAreaCount<1000))
		{
			locationAreaCode = "LA0"+(locationAreaCount+1);
		}
		else 
		{
			locationAreaCode = "LA"+(locationAreaCount+1);
		}

		return locationAreaCode;
	}

	//code for getting all country name
	private List<Country> findAllCountryName()
	{
		List<Country> countryList;

		countryList = countryRepository.findAll(new Sort(Sort.Direction.ASC,"countryId"));
		return countryList;
	}
	/*	@ResponseBody
	 @RequestMapping("/searchLocationAreaNameWiseList")
	 public List<LocationArea> SearchLocationAreaNameWiseList(@RequestParam("locationareaName") String locationareaName)
	 {
		 Query query = new Query();

		 List<LocationArea> locationareaList= mongoTemplate.find(query.addCriteria(Criteria.where("locationareaName").regex("^"+locationareaName+".*","i")),LocationArea.class);

		 return locationareaList;
	 }*/
	//mapping for Locatipon Area Master
	@RequestMapping("/LocationAreaMaster")
	public String LocationAreaMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<LocationArea> locationareaList= locationAreaRepository.findAll();
			List<City> cityList = cityRepository.findAll();
			List<State> stateList= stateRepository.findAll();
			List<Country> countryList= countryRepository.findAll();
			int i,j;
			for(i=0;i<locationareaList.size();i++)
			{
				try 
				{
					for(j=0;j<countryList.size();j++)
					{
						if(locationareaList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
						{
							locationareaList.get(i).setCountryId(countryList.get(j).getCountryName());
							break;
						}
					}

					for(j=0;j<stateList.size();j++)
					{
						if(locationareaList.get(i).getStateId().equals(stateList.get(j).getStateId()))
						{
							locationareaList.get(i).setStateId(stateList.get(j).getStateName());
							break;
						}
					}

					for(j=0;j<cityList.size();j++)
					{
						if(locationareaList.get(i).getCityId().equals(cityList.get(j).getCityId()))
						{
							locationareaList.get(i).setCityId(cityList.get(j).getCityName());
							break;
						}
					}

				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}

			model.addAttribute("countryList",findAllCountryName());
			model.addAttribute("locationareaList",locationareaList);

			return "LocationAreaMaster";

		}catch (Exception e) {
			return "login";
		}
	}
	// to get all location list by country in Location Area Master
	@ResponseBody
	@RequestMapping(value="/getCountryLocationList",method=RequestMethod.POST)
	public List<LocationArea> CountryLocationList(@RequestParam("countryId") String countryId, HttpSession session)
	{
		List<LocationArea> locationareaList= new ArrayList<LocationArea>();

		Query query = new Query();
		locationareaList = mongoTemplate.find(query.addCriteria(Criteria.where("countryId").is(countryId)), LocationArea.class);

		List<City> cityList = cityRepository.findAll();
		List<State> stateList= stateRepository.findAll();
		List<Country> countryList= countryRepository.findAll();
		int i,j;
		for(i=0;i<locationareaList.size();i++)
		{
			try 
			{
				for(j=0;j<countryList.size();j++)
				{
					if(locationareaList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
					{
						locationareaList.get(i).setCountryId(countryList.get(j).getCountryName());
						break;
					}
				}

				for(j=0;j<stateList.size();j++)
				{
					if(locationareaList.get(i).getStateId().equals(stateList.get(j).getStateId()))
					{
						locationareaList.get(i).setStateId(stateList.get(j).getStateName());
						break;
					}
				}

				for(j=0;j<cityList.size();j++)
				{
					if(locationareaList.get(i).getCityId().equals(cityList.get(j).getCityId()))
					{
						locationareaList.get(i).setCityId(cityList.get(j).getCityName());
						break;
					}
				}

			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}
		return locationareaList;
	}


	// to get all location list by country in Location Area Master
	@ResponseBody
	@RequestMapping(value="/getStateLocationList",method=RequestMethod.POST)
	public List<LocationArea> StateWiseLocationList(@RequestParam("stateId") String stateId, @RequestParam("countryId") String countryId, HttpSession session)
	{
		List<LocationArea> locationareaList= new ArrayList<LocationArea>();

		Query query = new Query();
		locationareaList = mongoTemplate.find(query.addCriteria(Criteria.where("countryId").is(countryId).and("stateId").is(stateId)), LocationArea.class);

		List<City> cityList = cityRepository.findAll();
		List<State> stateList= stateRepository.findAll();
		List<Country> countryList= countryRepository.findAll();
		int i,j;
		for(i=0;i<locationareaList.size();i++)
		{
			try 
			{
				for(j=0;j<countryList.size();j++)
				{
					if(locationareaList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
					{
						locationareaList.get(i).setCountryId(countryList.get(j).getCountryName());
						break;
					}
				}

				for(j=0;j<stateList.size();j++)
				{
					if(locationareaList.get(i).getStateId().equals(stateList.get(j).getStateId()))
					{
						locationareaList.get(i).setStateId(stateList.get(j).getStateName());
						break;
					}
				}

				for(j=0;j<cityList.size();j++)
				{
					if(locationareaList.get(i).getCityId().equals(cityList.get(j).getCityId()))
					{
						locationareaList.get(i).setCityId(cityList.get(j).getCityName());
						break;
					}
				}

			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}
		return locationareaList;
	}

	@ResponseBody
	@RequestMapping(value="/searchLocationAreaNameWiseList",method=RequestMethod.POST)
	public List<LocationArea> searchLocationAreaNameWiseList(@RequestParam("cityId") String cityId, @RequestParam("stateId") String stateId, @RequestParam("countryId") String countryId, HttpSession session)
	{
		List<LocationArea> locationareaList= new ArrayList<LocationArea>();

		Query query = new Query();
		locationareaList = mongoTemplate.find(query.addCriteria(Criteria.where("countryId").is(countryId).and("stateId").is(stateId).and("cityId").is(cityId)), LocationArea.class);

		List<City> cityList = cityRepository.findAll();
		List<State> stateList= stateRepository.findAll();
		List<Country> countryList= countryRepository.findAll();
		int i,j;
		for(i=0;i<locationareaList.size();i++)
		{
			try 
			{
				for(j=0;j<countryList.size();j++)
				{
					if(locationareaList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
					{
						locationareaList.get(i).setCountryId(countryList.get(j).getCountryName());
						break;
					}
				}

				for(j=0;j<stateList.size();j++)
				{
					if(locationareaList.get(i).getStateId().equals(stateList.get(j).getStateId()))
					{
						locationareaList.get(i).setStateId(stateList.get(j).getStateName());
						break;
					}
				}

				for(j=0;j<cityList.size();j++)
				{
					if(locationareaList.get(i).getCityId().equals(cityList.get(j).getCityId()))
					{
						locationareaList.get(i).setCityId(cityList.get(j).getCityName());
						break;
					}
				}

			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}
		return locationareaList;
	}

	//Request Mapping For Add Area Location 
	@RequestMapping("/AddLocationArea")
	public String addLocationArea(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<LocationArea> locationareaList;
			locationareaList = locationAreaRepository.findAll();

			model.addAttribute("locationAreaCode",locationAreaCode());
			model.addAttribute("CountryList", findAllCountryName());

			model.addAttribute("locationareaList",locationareaList);

			return "AddLocationArea";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/AddLocationArea", method = RequestMethod.POST)
	public String locationareaSave(@ModelAttribute LocationArea locationarea, Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				locationarea = new LocationArea(locationarea.getLocationareaId(), locationarea.getLocationareaName(),locationarea.getPinCode(),locationarea.getCountryId(), locationarea.getStateId(), locationarea.getCityId(), locationarea.getCreationDate(), locationarea.getUpdateDate(), locationarea.getUserName());
				locationAreaRepository.save(locationarea);

				model.addAttribute("locationAreaStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("locationAreaStatus","Fail");
			}

			List<LocationArea> locationareaList;
			locationareaList = locationAreaRepository.findAll();

			model.addAttribute("locationAreaCode",locationAreaCode());
			model.addAttribute("CountryList", findAllCountryName());
			model.addAttribute("locationareaList",locationareaList);

			return "AddLocationArea";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/getallAreaList")
	public List<LocationArea> AllAreaList(@RequestParam("locationareaId") String locationareaId,@RequestParam("cityId") String cityId,@RequestParam("stateId") String stateId, @RequestParam("countryId") String countryId) 
	{
		Query query =new Query();
		List<LocationArea> locationareaList = mongoTemplate.find(query.addCriteria(Criteria.where("countryId").is(countryId).and("stateId").is(stateId).and("cityId").is(cityId).and("locationareaId").is(locationareaId)), LocationArea.class);
		/*
		List<City> cityList = cityRepository.findAll();
    	List<State> stateList= stateRepository.findAll();
    	List<Country> countryList= countryRepository.findAll();
    	int i,j;
    	for(i=0;i<locationareaList.size();i++)
    	{
    		try 
    		{
    			for(j=0;j<countryList.size();j++)
    			{
    				if(locationareaList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
    				{
    					locationareaList.get(i).setCountryId(countryList.get(j).getCountryName());
    					break;
    				}
    			}

    			for(j=0;j<stateList.size();j++)
    			{
    				if(locationareaList.get(i).getStateId().equals(stateList.get(j).getStateId()))
    				{
    					locationareaList.get(i).setStateId(stateList.get(j).getStateName());
    					break;
    				}
    			}

    			for(j=0;j<cityList.size();j++)
    			{
    				if(locationareaList.get(i).getCityId().equals(cityList.get(j).getCityId()))
    				{
    					locationareaList.get(i).setCityId(cityList.get(j).getCityName());
    					break;
    				}
    			}

    		}
    		catch (Exception e) {
				// TODO: handle exception
			}
    	} */
		return locationareaList;
	}

	@RequestMapping("/EditLocationArea")
	public String EditLocationArea(@RequestParam("locationareaId") String locationareaId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<LocationArea> locationDetails = mongoTemplate.find(query.addCriteria(Criteria.where("locationareaId").is(locationareaId)), LocationArea.class);
			try
			{
				query = new Query();
				Country countryDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("countryId").is(locationDetails.get(0).getCountryId())), Country.class);

				query = new Query();
				State stateDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("stateId").is(locationDetails.get(0).getStateId())), State.class);

				query = new Query();
				City cityDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("cityId").is(locationDetails.get(0).getCityId())), City.class);

				model.addAttribute("countryName", countryDetails.getCountryName());
				model.addAttribute("stateName", stateDetails.getStateName());
				model.addAttribute("cityName", cityDetails.getCityName());
			}catch (Exception e) {
				// TODO: handle exception
			}
			model.addAttribute("CountryList", findAllCountryName());
			model.addAttribute("locationDetails", locationDetails);
			return "EditLocationArea";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/EditLocationArea",method=RequestMethod.POST)
	public String EditLocationAreaPostMethod(@ModelAttribute LocationArea locationarea, Model model)
	{
		try {
			try
			{
				locationarea = new LocationArea(locationarea.getLocationareaId(), locationarea.getLocationareaName(),locationarea.getPinCode(),locationarea.getCountryId(), locationarea.getStateId(), locationarea.getCityId(), locationarea.getCreationDate(), locationarea.getUpdateDate(), locationarea.getUserName());
				locationAreaRepository.save(locationarea);

				model.addAttribute("locationAreaStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("locationAreaStatus","Fail");
			}


			List<LocationArea> locationareaList= locationAreaRepository.findAll();
			List<City> cityList = cityRepository.findAll();
			List<State> stateList= stateRepository.findAll();
			List<Country> countryList= countryRepository.findAll();

			int i,j;
			for(i=0;i<locationareaList.size();i++)
			{
				try 
				{
					for(j=0;j<countryList.size();j++)
					{
						if(locationareaList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
						{
							locationareaList.get(i).setCountryId(countryList.get(j).getCountryName());
							break;
						}
					}

					for(j=0;j<stateList.size();j++)
					{
						if(locationareaList.get(i).getStateId().equals(stateList.get(j).getStateId()))
						{
							locationareaList.get(i).setStateId(stateList.get(j).getStateName());
							break;
						}
					}

					for(j=0;j<countryList.size();j++)
					{
						if(locationareaList.get(i).getCityId().equals(cityList.get(j).getCityId()))
						{
							locationareaList.get(i).setCityId(cityList.get(j).getCityName());
							break;
						}
					}

				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}

			model.addAttribute("countryList",findAllCountryName());
			model.addAttribute("locationareaList",locationareaList);

			return "LocationAreaMaster";

		}catch (Exception e) {
			return "login";
		}
	}
}

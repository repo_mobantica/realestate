package com.realestate.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.SupplierType;
import com.realestate.repository.SupplierTypeRepository;

@Controller
@RequestMapping("/")
public class SupplierTypeController {
	@Autowired
	SupplierTypeRepository suppliertypeRepository;
	@Autowired
	MongoTemplate mongoTemplate;
	String suppliertypeCode;
	private String SupplierTypeCode()
	{
		long cnt  = suppliertypeRepository.count();
		if(cnt<10)
		{
			suppliertypeCode = "ST000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			suppliertypeCode = "ST00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			suppliertypeCode = "ST0"+(cnt+1);
		}
		else
		{
			suppliertypeCode = "ST"+(cnt+1);
		}
		return suppliertypeCode;
	}


	@RequestMapping("/AddSupplierType")
	public String addSupplierType(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			model.addAttribute("suppliertypeCode",SupplierTypeCode());
			return "AddSupplierType";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value = "/AddSupplierType", method = RequestMethod.POST)
	public String suppliertypeSave(@ModelAttribute SupplierType suppliertype, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				suppliertype = new SupplierType(suppliertype.getSuppliertypeId(), suppliertype.getSupplierType(),suppliertype.getCreationDate(), suppliertype.getUpdateDate(), suppliertype.getUserName());
				suppliertypeRepository.save(suppliertype);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}

			model.addAttribute("suppliertypeCode",SupplierTypeCode());
			return "AddSupplierType";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/SupplierTypeMaster")
	public String supplierTypeMaster(ModelMap model)
	{
		try {
			List<SupplierType> suppliertypeList = suppliertypeRepository.findAll();
			model.addAttribute("suppliertypeList",suppliertypeList);

			return "SupplierTypeMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/SearchsupplierTypeListByName")
	public List<SupplierType> SearchItemUnitListByName(@RequestParam("supplierType") String supplierType)
	{
		Query query = new Query();

		List<SupplierType> suppliertypeList = mongoTemplate.find(query.addCriteria(Criteria.where("supplierType").regex("^"+supplierType+".*","i")),SupplierType.class);
		return suppliertypeList;
	}

	@ResponseBody
	@RequestMapping("/getsupplierTypeList")
	public List<SupplierType> SupplierTypeList(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		List<SupplierType> suppliertypeList= suppliertypeRepository.findAll();

		return suppliertypeList;
	}

	@RequestMapping("/EditSupplierType")
	public String EditSupplierType(@RequestParam("suppliertypeId") String suppliertypeId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<SupplierType> suppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("suppliertypeId").is(suppliertypeId)),SupplierType.class);

			model.addAttribute("suppliertypeDetails",suppliertypeDetails);
			return "EditSupplierType";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/EditSupplierType", method=RequestMethod.POST)
	public String EditSupplierTypePostMethod(@ModelAttribute SupplierType suppliertype, ModelMap model)
	{
		try {
			try
			{
				suppliertype = new SupplierType(suppliertype.getSuppliertypeId(), suppliertype.getSupplierType(),suppliertype.getCreationDate(), suppliertype.getUpdateDate(), suppliertype.getUserName());
				suppliertypeRepository.save(suppliertype);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}

			List<SupplierType> suppliertypeList = suppliertypeRepository.findAll();
			model.addAttribute("suppliertypeList",suppliertypeList);

			return "SupplierTypeMaster";

		}catch (Exception e) {
			return "login";
		}
	}
}

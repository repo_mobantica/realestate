package com.realestate.controller;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.EmployeeSalaryDetails;
import com.realestate.bean.LocationArea;
import com.realestate.bean.City;
import com.realestate.bean.Company;
import com.realestate.bean.Country;
import com.realestate.bean.Department;
import com.realestate.bean.Designation;
import com.realestate.bean.Employee;
import com.realestate.bean.EmployeeLeaveDetails;
import com.realestate.bean.Login;
import com.realestate.bean.NumberToWordConversion;
import com.realestate.bean.State;
import com.realestate.repository.BankRepository;
import com.realestate.repository.CompanyRepository;
import com.realestate.repository.CountryRepository;
import com.realestate.repository.DepartmentRepository;
import com.realestate.repository.EmployeeRepository;
import com.realestate.repository.EmployeeSalaryDetailsRepository;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;

@Controller
@RequestMapping("/")
public class EmployeeSalaryDetailsController {


	@Autowired
	EmployeeRepository employeeRepository;
	@Autowired
	CountryRepository countryRepository;
	@Autowired
	BankRepository bankRepository;
	@Autowired
	DepartmentRepository departmentRepository;
	@Autowired
	CompanyRepository companyRepository;
	@Autowired
	EmployeeSalaryDetailsRepository employeesalarydetailsRepository;
	@Autowired
	MongoTemplate mongoTemplate;


	@RequestMapping("/EmployeeSalaryDetails")
	public String EmployeeSalaryDetails(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			Query query = new Query();
			List<Employee> employeeList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("Active")),Employee.class);

			query = new Query();
			for(int i=0;i<employeeList.size();i++)
			{
				try {

					query = new Query();
					List<Department> departmentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("departmentId").is(employeeList.get(i).getDepartmentId())),Department.class);

					query = new Query();
					List<Designation> designationDetails = mongoTemplate.find(query.addCriteria(Criteria.where("designationId").is(employeeList.get(i).getDesignationId())),Designation.class);

					employeeList.get(i).setDepartmentId(departmentDetails.get(0).getDepartmentName());
					employeeList.get(i).setDesignationId(designationDetails.get(0).getDesignationName());
				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}

			model.addAttribute("employeeList",employeeList);
			return "EmployeeSalaryDetails";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping("/AddEmployeeMonthlySalary")
	public String AddEmployeeMonthlySalary(@RequestParam("employeeId") String employeeId, ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			try {
				Query query = new Query();
				List<Employee> employeeList = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(employeeId)),Employee.class);

				query = new Query();
				Department departmentDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("departmentId").is(employeeList.get(0).getDepartmentId())),Department.class);

				query = new Query();
				Designation designationDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("designationId").is(employeeList.get(0).getDesignationId())),Designation.class);

				double totalAddition=0;

				if(employeeList.get(0).getEmployeeBasicpay()!=0)
				{
					totalAddition=employeeList.get(0).getEmployeeBasicpay();
				}
				try {
					totalAddition=totalAddition+employeeList.get(0).getEmployeeHra();
				}catch (Exception e) {
					// TODO: handle exception
				}
				try {
					totalAddition=totalAddition+employeeList.get(0).getEmployeeCa();
				}catch (Exception e) {
					// TODO: handle exception
				}
				try {
					totalAddition=totalAddition+employeeList.get(0).getEmployeeDa();
				}catch (Exception e) {
					// TODO: handle exception
				}
				model.addAttribute("totalAddition",totalAddition);
				model.addAttribute("employeeList",employeeList);
				model.addAttribute("designationName",designationDetails.getDesignationName());
				model.addAttribute("departmentName",departmentDetails.getDepartmentName());
			}catch (Exception e) {
				// TODO: handle exception
			}
			return "AddEmployeeMonthlySalary";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping(value="/getEmployeeMonthLeave",method=RequestMethod.POST)
	public List<EmployeeLeaveDetails> getEmployeeMonthLeave(@RequestParam("employeeId") String employeeId, @RequestParam("month") String month, @RequestParam("year") String year, HttpSession session) throws ParseException
	{
		String monthStartDate1="1/"+month+"/"+year;
		String monthEndDate1="31/"+month+"/"+year;

		int daysPresent =0;

		YearMonth yearMonthObject = YearMonth.of(Integer.parseInt(year), Integer.parseInt(month));
		daysPresent = yearMonthObject.lengthOfMonth();
		Date currentYearStartDate=new SimpleDateFormat("d/M/yyyy").parse(monthStartDate1);
		Date currentYearEndDate=new SimpleDateFormat("d/M/yyyy").parse(monthEndDate1);
		Query query=new Query();
		List<EmployeeLeaveDetails> employeeLeaveList=new ArrayList<EmployeeLeaveDetails>();

		employeeLeaveList = mongoTemplate.find(query.addCriteria(Criteria.where("leaveFromDate").gte( currentYearStartDate).lt(currentYearEndDate).and("employeeId").is(employeeId)), EmployeeLeaveDetails.class);

		if(employeeLeaveList.size()==0)
		{
			EmployeeLeaveDetails employeeleaveDetails=new EmployeeLeaveDetails();
			employeeleaveDetails.setDaysPresent(daysPresent);
			employeeleaveDetails.setNoOfDays(0);
			employeeLeaveList.add(employeeleaveDetails);
		}
		for(int i=0;i<employeeLeaveList.size();i++)
		{
			employeeLeaveList.get(i).setDaysPresent(daysPresent);
		}

		return employeeLeaveList;
	}


	@RequestMapping(value = "/AddEmployeeMonthlySalary", method = RequestMethod.POST)
	public String SaveAgent(@ModelAttribute EmployeeSalaryDetails employeesalarydetails, HttpServletRequest req, Model model)
	{
		try {
			String userName = (String)req.getSession().getAttribute("user");
			try
			{
				String salaryDetailsId="";
				salaryDetailsId=employeesalarydetails.getMonth()+"/"+employeesalarydetails.getYear()+"/"+employeesalarydetails.getEmployeeId();

				EmployeeSalaryDetails employeesalarydetails1=new EmployeeSalaryDetails();
				employeesalarydetails1.setSalaryDetailsId(salaryDetailsId);
				employeesalarydetails1.setMonth(employeesalarydetails.getMonth());
				employeesalarydetails1.setYear(employeesalarydetails.getYear());
				employeesalarydetails1.setEmployeeId(employeesalarydetails.getEmployeeId());
				employeesalarydetails1.setDaysPresent(employeesalarydetails.getDaysPresent());
				employeesalarydetails1.setPaidLeave(employeesalarydetails.getPaidLeave());
				employeesalarydetails1.setLeaveDeduction(employeesalarydetails.getLeaveDeduction());
				employeesalarydetails1.setConveyance(employeesalarydetails.getConveyance());
				employeesalarydetails1.setProvidentFund(employeesalarydetails.getProvidentFund());
				employeesalarydetails1.setEsiAmount(employeesalarydetails.getEsiAmount());
				employeesalarydetails1.setLoanAmount(employeesalarydetails.getLoanAmount());
				employeesalarydetails1.setProfessionalTax(employeesalarydetails.getProfessionalTax());
				employeesalarydetails1.setTds(employeesalarydetails.getTds());
				employeesalarydetails1.setTotalDeduction(employeesalarydetails.getTotalDeduction());
				employeesalarydetails1.setNetSalary(employeesalarydetails.getNetSalary());
				employeesalarydetails1.setTotalAddition(employeesalarydetails.getTotalAddition());
				employeesalarydetails1.setCreationDate(new Date());
				employeesalarydetails1.setUpdateDate(new Date());
				employeesalarydetails1.setUserName(userName);
				employeesalarydetailsRepository.save(employeesalarydetails1);
			}
			catch (Exception e) {

			}


			Query query = new Query();
			List<Employee> employeeList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("Active")),Employee.class);

			query = new Query();
			for(int i=0;i<employeeList.size();i++)
			{
				try {

					query = new Query();
					List<Department> departmentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("departmentId").is(employeeList.get(i).getDepartmentId())),Department.class);

					query = new Query();
					List<Designation> designationDetails = mongoTemplate.find(query.addCriteria(Criteria.where("designationId").is(employeeList.get(i).getDesignationId())),Designation.class);

					employeeList.get(i).setDepartmentId(departmentDetails.get(0).getDepartmentName());
					employeeList.get(i).setDesignationId(designationDetails.get(0).getDesignationName());
				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}

			model.addAttribute("employeeList",employeeList);
			return "EmployeeSalaryDetails";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/EmployeeSalarySlip")
	public String EmployeeSalarySlip(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			Query query = new Query();
			List<Employee> employeeList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("Active")),Employee.class);

			model.addAttribute("employeeList",employeeList);
			return "EmployeeSalarySlip";

		}catch (Exception e) {
			return "login";
		}
	}


	@ResponseBody
	@RequestMapping(value = "/PrintEmployeeSalarySlip", method = RequestMethod.POST)
	public ResponseEntity<byte[]> PrintEmployeeSalarySlip(@RequestParam("employeeId") String employeeId, @RequestParam("month") String month, @RequestParam("year") String year, ModelMap model, HttpServletRequest req, HttpServletResponse res, HttpServletResponse response)
	{
		int index=0;
		JasperPrint print;
		try 
		{	

			Query query = new Query();
			String userId = (String)req.getSession().getAttribute("user");

			query = new Query();
			List<Login> loginDetails = mongoTemplate.find(query.addCriteria(Criteria.where("userId").is(userId)),Login.class);

			query = new Query();
			List<Employee> userDetails = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.get(0).getEmployeeId())),Employee.class);
			query = new Query();
			Department departmentDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("departmentId").is(userDetails.get(0).getDepartmentId())),Department.class);
			query = new Query();
			Designation designationDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("designationId").is(userDetails.get(0).getDesignationId())),Designation.class);

			Collection c = new ArrayList();
			HashMap jmap = new HashMap();
			jmap.put("index", ""+(index++));
			c.add(jmap);

			SimpleDateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");
			List<Company> companyDetails= companyRepository.findAll();

			query =new Query();
			City companyCityDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("cityId").is(companyDetails.get(0).getCityId())), City.class);
			query =new Query();
			LocationArea companyLocationareaDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("locationareaId").is(companyDetails.get(0).getLocationareaId())), LocationArea.class);


			jmap = null;

			query = new Query();
			List<Employee> employeeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(employeeId)),Employee.class);

			JRDataSource dataSource = new JRMapCollectionDataSource(c);
			Map<String, Object> parameterMap = new HashMap();

			Date date = new Date();  
			String todayDate= createDateFormat.format(date);

			Calendar cal = Calendar.getInstance();
			int newYear = Integer.parseInt(year);
			int newMonth = Integer.parseInt(month);
			int totaDayInMonth = cal.get(Calendar.DAY_OF_MONTH);	
			String salaryDetailsId=month+"/"+year+"/"+employeeId;

			query = new Query();
			List<EmployeeSalaryDetails> employeesalaryDetails = mongoTemplate.find(query.addCriteria(Criteria.where("salaryDetailsId").is(salaryDetailsId)),EmployeeSalaryDetails.class);

			String month1="";

			if(newMonth==1)
			{
				month1="January";
			}
			else if(newMonth==2)
			{
				month1="February";
			}
			else if(newMonth==3)
			{
				month1="March";
			}
			else if(newMonth==4)
			{
				month1="April";
			}
			else if(newMonth==5)
			{
				month1="May";
			}
			else if(newMonth==6)
			{
				month1="June";
			}
			else if(newMonth==7)
			{
				month1="July";
			}
			else if(newMonth==8)
			{
				month1="August";
			}
			else if(newMonth==9)
			{
				month1="September";
			}
			else if(newMonth==10)
			{
				month1="October";
			}
			else if(newMonth==11)
			{
				month1="November";
			}
			else if(newMonth==12)
			{
				month1="December";
			}


			NumberToWordConversion run = new NumberToWordConversion();

			String netSalaryinWords = run.convertToIndianCurrency(""+employeesalaryDetails.get(0).getNetSalary());

			netSalaryinWords= "Net Payable Rs. "+employeesalaryDetails.get(0).getNetSalary()+"/- ( "+netSalaryinWords+" Rupees).";

			String currentMonth=month1+" "+year;

			parameterMap.put("date", ""+todayDate);

			parameterMap.put("currentMonth",""+currentMonth);

			parameterMap.put("employeeId",""+employeeDetails.get(0).getEmployeeId());

			parameterMap.put("employeeName", ""+employeeDetails.get(0).getEmployeefirstName()+" "+employeeDetails.get(0).getEmployeemiddleName()+" "+employeeDetails.get(0).getEmployeelastName());

			parameterMap.put("pfAccountNumber",""+employeeDetails.get(0).getEmployeePfacno());

			parameterMap.put("mobileNumber",""+employeeDetails.get(0).getEmployeeMobileno());

			parameterMap.put("employeePancardno",""+employeeDetails.get(0).getEmployeePancardno());

			parameterMap.put("employeeDob",""+employeeDetails.get(0).getEmployeeDob());

			parameterMap.put("dateOfJoining",""+employeeDetails.get(0).getEmployeeJoiningdate());

			parameterMap.put("designationName",""+designationDetails.getDesignationName());

			parameterMap.put("daysPaid", ""+employeesalaryDetails.get(0).getDaysPresent());

			parameterMap.put("daysPresent", ""+employeesalaryDetails.get(0).getDaysPresent());

			parameterMap.put("paidLeave", ""+0);

			parameterMap.put("basicPay", ""+employeeDetails.get(0).getEmployeeBasicpay());

			parameterMap.put("hra", ""+employeeDetails.get(0).getEmployeeHra());

			parameterMap.put("conveyance",""+employeesalaryDetails.get(0).getConveyance());

			parameterMap.put("leaveDeduction", ""+employeesalaryDetails.get(0).getLeaveDeduction());

			parameterMap.put("providentFund", ""+employeesalaryDetails.get(0).getProvidentFund());

			parameterMap.put("esi", ""+employeesalaryDetails.get(0).getEsiAmount());

			parameterMap.put("loan", ""+employeesalaryDetails.get(0).getLoanAmount());

			parameterMap.put("profesionTax", ""+employeesalaryDetails.get(0).getProfessionalTax());

			parameterMap.put("tds", ""+employeesalaryDetails.get(0).getTds());

			parameterMap.put("totalDeduction",""+employeesalaryDetails.get(0).getTotalDeduction());

			parameterMap.put("netSalary", ""+employeesalaryDetails.get(0).getNetSalary());

			parameterMap.put("totalAddition", ""+employeesalaryDetails.get(0).getTotalAddition());

			parameterMap.put("paySalaryInWord", ""+netSalaryinWords);

			parameterMap.put("employeeBankDeatils", ""+"Account Number : "+employeeDetails.get(0).getEmployeeBankacno()+", Bank Name : "+employeeDetails.get(0).getEmployeeBankName());

			parameterMap.put("companyName", ""+companyDetails.get(0).getCompanyName());

			parameterMap.put("companyAddress", ""+companyDetails.get(0).getCompanyAddress()+", "+companyLocationareaDetails.getLocationareaName()+", "+companyCityDetails.getCityName());


			parameterMap.put("userName", ""+userDetails.get(0).getEmployeefirstName()+" "+userDetails.get(0).getEmployeelastName());


			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/EmployeeSalarySlip.jasper");

			print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(print, baos);

			byte[] contents = baos.toByteArray();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			String filename = "EmployeeSalarySlip.pdf";

			JasperExportManager.exportReportToPdfStream(print, baos);

			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

			ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
			response.setHeader("Content-Disposition", "inline; filename=" + filename );

			return resp;					    
		}
		catch(Exception ex)
		{
			return null;
		}

	}

}

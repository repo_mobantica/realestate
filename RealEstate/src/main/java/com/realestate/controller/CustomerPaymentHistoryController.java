package com.realestate.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.realestate.bean.CustomerReceiptForm;
import com.realestate.repository.CustomerReceiptFormRepository;

@Controller
@RequestMapping("/")
public class CustomerPaymentHistoryController
{

	@Autowired
	CustomerReceiptFormRepository customerreceiptformrepository;
	@Autowired
	MongoTemplate mongoTemplate;

	@RequestMapping("/CustomerPaymentHistory")
	public String CustomerPaymentHistory(@RequestParam("bookingId") String bookingId, Model model) 
	{
		try {
		Query query = new Query();

		//List<CustomerReceiptForm> paymentList = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId).and("status").is("Deposited")), CustomerReceiptForm.class);
		List<CustomerReceiptForm> paymentList = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)), CustomerReceiptForm.class);

		model.addAttribute("paymentList",paymentList);
		return "CustomerPaymentHistory";
		
		}catch (Exception e) {
			return "login";
		}
	}
}

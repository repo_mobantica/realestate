package com.realestate.controller;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;

import java.io.InputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.realestate.bean.Floor;
import com.realestate.bean.Login;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.FloorRepository;
import com.realestate.repository.ProjectBuildingRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.ProjectWingRepository;

import java.io.FileInputStream;
import java.io.IOException;


@Controller
@RequestMapping("/")
public class ImportNewFloorController {


	@Autowired
	ServletContext context;

	@Autowired
	FloorRepository floorRepository;
	@Autowired
	ProjectRepository projectRepository;

	@Autowired
	ProjectBuildingRepository projectbuildingRepository;
	@Autowired
	ProjectWingRepository projectwingRepository;

	@Autowired
	MongoTemplate mongoTemplate;

	Floor floor = new Floor();
	HSSFWorkbook workbook;
	XSSFWorkbook workbook1;

	HSSFSheet worksheet;
	XSSFSheet worksheet1; 

	//Code geration for floor code
	String floorCode;
	private String FloorCode()
	{
		long floorCount = floorRepository.count();

		if(floorCount<10)
		{
			floorCode = "FL000"+(floorCount+1);
		}
		else if((floorCount>=10) && (floorCount<100))
		{
			floorCode = "FL00"+(floorCount+1);
		}
		else if((floorCount>=100) && (floorCount<1000))
		{
			floorCode = "FL0"+(floorCount+1);
		}
		else 
		{
			floorCode = "FL"+(floorCount+1);
		}
		return floorCode;
	}


	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping(value="/ImportNewFloor")
	public String ImportNewFloor(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		return "ImportNewFloor";
	}


	@SuppressWarnings({ "deprecation"})
	@RequestMapping(value = "/ImportNewFloor", method = RequestMethod.POST)
	public String uploadFloorDetails(@RequestParam("floor_excel") MultipartFile floor_excel, Model model, HttpServletRequest req, HttpServletResponse res, RedirectAttributes redirectAttributes, HttpSession session) 
	{

		String user = (String)req.getSession().getAttribute("user");
		List<Project> projectList = GetUserAssigenedProjectList(req,res);
		List<ProjectBuilding> buildingList = projectbuildingRepository.findAll();
		List<ProjectWing> wingList = projectwingRepository.findAll();

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/M/yyyy");
		LocalDate localDate = LocalDate.now();
		String todayDate=dtf.format(localDate);
		String projectId="",buildingId="",wingId="";

		if (!floor_excel.isEmpty() || floor_excel != null )
		{
			try 
			{
				if(floor_excel.getOriginalFilename().endsWith("xls") || floor_excel.getOriginalFilename().endsWith("xlsx") || floor_excel.getOriginalFilename().endsWith("csv"))
				{
					if(floor_excel.getOriginalFilename().endsWith("xlsx"))
					{
						InputStream stream = floor_excel.getInputStream();
						XSSFWorkbook workbook = new XSSFWorkbook(stream);

						XSSFSheet sheet = workbook.getSheet("Sheet1");  /// this will read 1st workbook of ExcelSheet

						int firstRow = sheet.getFirstRowNum();

						XSSFRow firstrow = sheet.getRow(firstRow);

						@SuppressWarnings("unused")
						int lastColumnCount = firstrow.getLastCellNum();

						@SuppressWarnings("unused")
						Iterator<Row> rowIterator = sheet.iterator();   
						int last_no=sheet.getLastRowNum();

						Floor floor = new Floor();

						for(int i=0;i<last_no;i++)
						{
							projectId="";
							buildingId="";
							wingId="";

							try {
								XSSFRow row = sheet.getRow(i+1);

								// Skip read heading 
								if (row.getRowNum() == 0) 
								{
									continue;
								}

								if(row.getCell(5).getStringCellValue().equalsIgnoreCase("Parking") || row.getCell(5).getStringCellValue().equalsIgnoreCase("Shops") || row.getCell(5).getStringCellValue().equalsIgnoreCase("Flats"))
								{ 
									for(int j=0; j<projectList.size();j++)
									{
										if((projectList.get(j).getProjectName().trim()).equalsIgnoreCase((row.getCell(3).getStringCellValue().trim())))
										{
											projectId=projectList.get(j).getProjectId();
											break;
										}
									}

									for(int j=0; j<buildingList.size();j++)
									{
										if((buildingList.get(j).getBuildingName().trim()).equalsIgnoreCase((row.getCell(2).getStringCellValue().trim())))
										{
											buildingId=buildingList.get(j).getBuildingId();
											break;
										}
									}

									for(int j=0; j<wingList.size();j++)
									{
										if((wingList.get(j).getWingName().trim()).equalsIgnoreCase((row.getCell(1).getStringCellValue().trim())))
										{
											wingId=wingList.get(j).getWingId();
											break;
										}
									}

									if(!projectId.equals("") && !buildingId.equals("") && !wingId.equals(""))
									{

										floor.setFloorId(FloorCode());

										row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
										floor.setFloortypeName(row.getCell(0).getStringCellValue());

										floor.setWingId(wingId);
										floor.setBuildingId(buildingId);
										floor.setProjectId(projectId);

										row.getCell(4).setCellType(Cell.CELL_TYPE_NUMERIC);
										floor.setFloorRise((int)row.getCell(4).getNumericCellValue());
										try {
											row.getCell(5).setCellType(Cell.CELL_TYPE_STRING);
											floor.setFloorzoneType(row.getCell(5).getStringCellValue());
										}catch (Exception e) {
											// TODO: handle exception
										}
										try {
											row.getCell(6).setCellType(Cell.CELL_TYPE_NUMERIC);
											floor.setNumberofFlats((int)row.getCell(6).getNumericCellValue());
										}catch (Exception e) {
											// TODO: handle exception
										}
										try {
											row.getCell(7).setCellType(Cell.CELL_TYPE_NUMERIC);
											floor.setNumberofShops((int)row.getCell(7).getNumericCellValue());
										}catch (Exception e) {
											// TODO: handle exception
										}
										try {
											row.getCell(8).setCellType(Cell.CELL_TYPE_NUMERIC);
											floor.setNumberofOpenParking((int)row.getCell(8).getNumericCellValue());

											row.getCell(9).setCellType(Cell.CELL_TYPE_NUMERIC);
											floor.setNumberofCloseParking((int)row.getCell(9).getNumericCellValue());
										}catch (Exception e) {
											// TODO: handle exception
										}
										floor.setCreationDate(todayDate);
										floor.setUpdateDate(todayDate);
										floor.setUserName(user);
										floorRepository.save(floor);
									}
								}
							}
							catch (Exception ee) {
							}

						}
						workbook.close();
					}

				}//if after inner if

			}
			catch(Exception e)
			{
				System.out.println("hdsgf = "+e);
			}
		}
		/*	
		List<Floor> floorDetails;
		floorDetails = floorRepository.findAll();

		model.addAttribute("floorDetails", floorDetails);
		 */	
		return "ImportNewFloor";
	}

	//private static final String INTERNAL_FILE="Newflat.xlsx";
	@RequestMapping(value = "/DownloadFloorTemplate")
	public String downloadFlatTemplate(Model model, HttpServletResponse response, HttpServletRequest request, RedirectAttributes redirectAttributes, HttpSession session) throws IOException 
	{

		String filename = "NewFloor.xlsx";
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		String filepath = "//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"Template/";
		response.setContentType("APPLICATION/OCTET-STREAM");
		response.setHeader("Content-Disposition", "attachment; filename=\""+ filename + "\"");


		FileInputStream fileInputStream = new FileInputStream(filepath+filename);

		int i;
		while ((i = fileInputStream.read()) != -1) {
			out.write(i);
		}
		fileInputStream.close();
		out.close();


		/*try 
			{
	            String downloadFolder = request.getSession().getServletContext().getRealPath("/webapp/Template");
	            //context.getRealPath("src/main/webapp/Template");
	            System.out.println("path="+downloadFolder);
	            File file = new File(downloadFolder + File.separator + filename);

	            if (file.exists()) 
	            {
	                String mimeType = context.getMimeType(file.getPath());

	                if (mimeType == null) 
	                {
	                    mimeType = "application/octet-stream";
	                }

	                response.setContentType(mimeType);
	                response.addHeader("Content-Disposition", "attachment; filename=" + filename);
	                response.setContentLength((int) file.length());

	                OutputStream os = response.getOutputStream();
	                FileInputStream fis = new FileInputStream(file);
	                byte[] buffer = new byte[4096];
	                int b = -1;

	                while ((b = fis.read(buffer)) != -1) 
	                {
	                    os.write(buffer, 0, b);
	                }

	                fis.close();
	                os.close();
	            } 
	            else 
	            {
	                System.out.println("Requested " + filename + " file not found!!");
	            }
	        } 
			catch (IOException e) 
			{
	            System.out.println("Error:- " + e.getMessage());
	        }*/


		/*File file = new File(fileName);
			FileInputStream in = new FileInputStream(file);
			byte[] content = new byte[(int) file.length()];
			in.read(content);
			ServletContext sc = request.getSession().getServletContext();
			String mimetype = sc.getMimeType(file.getName());
			response.reset();
			response.setContentType(mimetype);
			response.setContentLength(content.length);
			response.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");
			org.springframework.util.FileCopyUtils.copy(content, response.getOutputStream());*/

		return "ImportNewFloor";
	}

}

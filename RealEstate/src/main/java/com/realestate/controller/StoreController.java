package com.realestate.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Store;
import com.realestate.repository.StoreRepository;

@Controller
@RequestMapping("/")
public class StoreController 
{

	@Autowired
	StoreRepository storeRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	String storeCode;
	private String StoreCode()
	{
		long cnt  = storeRepository.count();
		if(cnt<10)
		{
			storeCode = "SO000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			storeCode = "SO00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			storeCode = "SO0"+(cnt+1);
		}
		else 
		{
			storeCode = "SO"+(cnt+1);
		}
		return storeCode;
	}


	@RequestMapping("/AddStore")
	public String addStore(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			model.addAttribute("storeCode",StoreCode());

			List<Store> storeDetails;
			storeDetails = storeRepository.findAll();
			model.addAttribute("storeList",storeDetails);

			return "AddStore";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping(value = "/AddStore", method = RequestMethod.POST)
	public String storeSave(@ModelAttribute Store store, ModelMap model,  HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				store = new Store(store.getStoreId(), store.getStoreName(),store.getCreationDate(), store.getUpdateDate(), store.getUserName());
				storeRepository.save(store);
				model.addAttribute("storeStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("storeStatus","Fail");
			}
			model.addAttribute("storeCode",StoreCode());

			List<Store> storeDetails;
			storeDetails = storeRepository.findAll();
			model.addAttribute("storeList",storeDetails);

			return "AddStore";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping("/StoreMaster")
	public String StoreMaster(ModelMap model)
	{
		try {
			List<Store> storeDetails = storeRepository.findAll();
			model.addAttribute("storeList",storeDetails);

			return "StoreMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/SearchStoreListByName")
	public List<Store> SearchStoreListByName(@RequestParam("storeName") String storeName)
	{
		Query query = new Query();

		List<Store> storeList = mongoTemplate.find(query.addCriteria(Criteria.where("storeName").regex("^"+storeName+".*","i")),Store.class);
		return storeList;
	}

	@ResponseBody
	@RequestMapping("/getallStoreList")
	public List<Store> AllStoreList(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		List<Store> storeList= storeRepository.findAll();

		return storeList;
	}

	@RequestMapping("/EditStore")
	public String EditStore(@RequestParam("storeId") String storeId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<Store> storeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("storeId").is(storeId)),Store.class);

			model.addAttribute("storeDetails", storeDetails);
			return "EditStore";

		}catch (Exception e) {
			return "login";
		}
	}
	@RequestMapping(value="/EditStore", method=RequestMethod.POST)
	public String EditStorePostMethod(@ModelAttribute Store store, ModelMap model)
	{
		try {
			try
			{
				store = new Store(store.getStoreId(), store.getStoreName(),store.getCreationDate(), store.getUpdateDate(), store.getUserName());
				storeRepository.save(store);
				model.addAttribute("storeStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("storeStatus","Fail");
			}

			List<Store> storeDetails = storeRepository.findAll();
			model.addAttribute("storeList",storeDetails);

			return "StoreMaster";

		}catch (Exception e) {
			return "login";
		}
	}
}

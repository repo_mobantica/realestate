package com.realestate.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Budget;
import com.realestate.repository.BudgetRepository;

@Controller
@RequestMapping("/")
public class BudgetContoller 
{
	@Autowired
	BudgetRepository budgetRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	String budgetCode;
	private String BudgetCode()
	{
		long cnt  = budgetRepository.count();
		if(cnt<10)
		{
			budgetCode = "BG000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			budgetCode = "BG00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			budgetCode = "BG0"+(cnt+1);
		}
		else
		{
			budgetCode = "BG"+(cnt+1);
		}
		return budgetCode;
	}

	@RequestMapping("/AddBudget")
	public String addBudget(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			model.addAttribute("budgetCode",BudgetCode());

			return "AddBudget";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value = "/AddBudget", method = RequestMethod.POST)
	public String budgetSave(@ModelAttribute Budget budget, Model model)
	{
		try {
			try
			{
				String budgetCost = budget.getMinimumBudget()+" - "+budget.getMaximumBudget();

				budget = new Budget(budget.getBudgetId(), budget.getMinimumBudget(), budget.getMaximumBudget(),budgetCost,budget.getCreationDate(),budget.getUpdateDate(), budget.getUserName());
				budgetRepository.save(budget);

				model.addAttribute("budgetStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("budgetStatus","Fail");
			}

			model.addAttribute("budgetCode",BudgetCode());
			return "AddBudget";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/BudgetMaster")
	public String BudgetMaster(Model model)
	{
		try {
			List<Budget> budgetList = budgetRepository.findAll();

			model.addAttribute("budgetList",budgetList);
			return "BudgetMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/SearchBudgetByminimum")
	public List<Budget> getBudgetListByminimum(@RequestParam("minimumBudget") String minimumBudget)
	{
		List<Budget> budgetList;

		Query query = new Query();
		budgetList = mongoTemplate.find(query.addCriteria(Criteria.where("minimumBudget").regex("^"+minimumBudget+".*","i")), Budget.class);

		return budgetList;
	}

	@RequestMapping("EditBudget")
	public String EditBudget(@RequestParam("budgetId") String budgetId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<Budget> budgetDetails = mongoTemplate.find(query.addCriteria(Criteria.where("budgetId").is(budgetId)),Budget.class);

			model.addAttribute("budgetDetails", budgetDetails);

			return "EditBudget";

		}catch (Exception e) {
			return "login";
		}
	}
	@RequestMapping(value="/EditBudget", method=RequestMethod.POST)
	public String EditBudgetPostMethod(@ModelAttribute Budget budget, Model model)
	{
		try {
			try
			{
				String budgetCost = budget.getMinimumBudget()+" - "+budget.getMaximumBudget();

				budget = new Budget(budget.getBudgetId(), budget.getMinimumBudget(), budget.getMaximumBudget(),budgetCost,budget.getCreationDate(),budget.getUpdateDate(), budget.getUserName());
				budgetRepository.save(budget);

				model.addAttribute("budgetStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("budgetStatus","Fail");
			}

			List<Budget> budgetList = budgetRepository.findAll();
			model.addAttribute("budgetList",budgetList);

			return "BudgetMaster";

		}catch (Exception e) {
			return "login";
		}
	}
}

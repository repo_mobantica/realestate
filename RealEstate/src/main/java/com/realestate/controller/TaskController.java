package com.realestate.controller;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.City;
import com.realestate.bean.Company;
import com.realestate.bean.Employee;
import com.realestate.bean.EmployeeSalaryDetails;
import com.realestate.bean.Flat;
import com.realestate.bean.Floor;
import com.realestate.bean.LocationArea;
import com.realestate.bean.Login;
import com.realestate.bean.MonthlyEmployeeSalaryReport;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.Task;
import com.realestate.bean.TaskHistory;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.CompanyRepository;
import com.realestate.repository.EmployeeRepository;
import com.realestate.repository.FlatRepository;
import com.realestate.repository.FloorRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.TaskHistoryRepository;
import com.realestate.repository.TaskRepository;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;

@Controller
@RequestMapping("/")
public class TaskController
{
	@Autowired
	CompanyRepository companyRepository;
	@Autowired
	TaskRepository taskRepository;
	@Autowired
	FlatRepository flatRepository;
	@Autowired
	FloorRepository floorRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	EmployeeRepository employeeRepository;
	@Autowired
	TaskHistoryRepository taskHistoryRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	String taskCode;
	private String TaskCode()
	{
		long cnt  = taskRepository.count();
		if(cnt<10)
		{
			taskCode = "TS000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			taskCode = "TS00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			taskCode = "TS0"+(cnt+1);
		}
		else 
		{
			taskCode = "TS"+(cnt+1);
		}
		return taskCode;
	}


	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}   

	//code for retrieving all project
	private List<Project> getAllProjectName(HttpServletRequest req,HttpServletResponse res)
	{
		List<Project> projectList;

		projectList = GetUserAssigenedProjectList(req,res);
		return projectList;
	}

	//code for retrieving all Employee
	private List<Employee> getAllEmployeeName()
	{
		List<Employee> employeeList;

		employeeList = employeeRepository.findAll(new Sort(Sort.Direction.ASC,"employeefirstName"));
		return employeeList;
	}


	//Request Mapping For Home
	@RequestMapping("/AddTask")
	public String addTask(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			try
			{
				String taskId = TaskCode();

				Query query = new Query();
				List<TaskHistory> taskHistories= mongoTemplate.find(query.addCriteria(Criteria.where("taskId").is(taskId)), TaskHistory.class);

				if(taskHistories.size()!=0)
				{
					query = new Query();
					mongoTemplate.remove(query.addCriteria(Criteria.where("taskId").is(taskId)), TaskHistory.class);
				}
			}
			catch(Exception e)
			{

			}

			model.addAttribute("taskCode",TaskCode());
			model.addAttribute("projectList", getAllProjectName(req, res));
			model.addAttribute("employeeList", getAllEmployeeName());

			return "AddTask";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value = "/AddTask", method = RequestMethod.POST)
	public String taskSave(@ModelAttribute Task task, Model model, HttpServletRequest req,HttpServletResponse res)
	{
		try {
			try
			{
				task = new Task(task.getTaskId(), task.getProjectId(), task.getBuildingId(),  task.getWingId(), task.getFloorId(), task.getFlatId(),task.getTaskName(), task.getTaskassignFrom(),task.getTaskassignTo(),  task.getTaskassingDate(),  task.getTaskendDate(), task.getTaskStatus(), task.getCreationDate(),task.getUpdateDate(), task.getUserName());
				taskRepository.save(task);

				model.addAttribute("taskStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("taskStatus","Fail");
			}

			model.addAttribute("taskCode",TaskCode());
			model.addAttribute("projectList", getAllProjectName(req, res));
			model.addAttribute("employeeList", getAllEmployeeName());

			return "AddTask";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/EditTask")
	public String EditTaskGetMethod(@RequestParam("taskId") String taskId, Model model, HttpServletRequest req,HttpServletResponse res)
	{
		try {
			try
			{
				Query query = new Query();
				List<Task> taskDetails = mongoTemplate.find(query.addCriteria(Criteria.where("taskId").is(taskId)), Task.class);

				if(taskDetails.size()!=0)
				{
					if(!taskDetails.get(0).getProjectId().equals("ALL"))
					{
						query = new Query();
						Project projectDetails = new Project();
						projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(taskDetails.get(0).getProjectId())), Project.class);
						taskDetails.get(0).setProjectName(projectDetails.getProjectName());
					}
					else
					{
						taskDetails.get(0).setProjectName("ALL");	
					}

					if(!taskDetails.get(0).getBuildingId().equals("ALL"))
					{
						query = new Query();
						ProjectBuilding projectBuilding = new ProjectBuilding();
						projectBuilding = mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(taskDetails.get(0).getBuildingId())), ProjectBuilding.class);
						taskDetails.get(0).setBuildingName(projectBuilding.getBuildingName());
					}
					else
					{
						taskDetails.get(0).setBuildingName("ALL");
					}

					if(!taskDetails.get(0).getWingId().equals("ALL"))
					{
						query = new Query();
						ProjectWing projectWing = new ProjectWing();
						projectWing = mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(taskDetails.get(0).getWingId())), ProjectWing.class);
						taskDetails.get(0).setWingName(projectWing.getWingName());
					}
					else
					{
						taskDetails.get(0).setWingName("ALL");
					}

					if(!taskDetails.get(0).getFloorId().equals("ALL"))
					{
						query = new Query();
						Floor floorDetails = new Floor();
						floorDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("floorId").is(taskDetails.get(0).getFloorId())), Floor.class);
						taskDetails.get(0).setFloorName(floorDetails.getFloortypeName());
					}
					else
					{
						taskDetails.get(0).setFloorName("ALL");
					}

					if(!taskDetails.get(0).getFlatId().equals("ALL"))
					{
						query = new Query();
						Flat flatDetails = new Flat();
						flatDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("flatId").is(taskDetails.get(0).getFlatId())), Flat.class);
						taskDetails.get(0).setFlatNumber(flatDetails.getFlatNumber());
					}
					else
					{
						taskDetails.get(0).setFlatNumber("ALL");
					}

					DateFormat dateFormat = new SimpleDateFormat("d/M/yyy");
					for(int i=0; i<taskDetails.size(); i++)
					{
						dateFormat = new SimpleDateFormat("d/M/yyy");
						taskDetails.get(i).setTaskassingDate1(dateFormat.format(taskDetails.get(i).getTaskassingDate()));

						dateFormat = new SimpleDateFormat("d/M/yyy");
						taskDetails.get(i).setTaskendDate1(dateFormat.format(taskDetails.get(i).getTaskendDate()));

						Employee employee = new Employee();
						query = new Query();
						employee = mongoTemplate.findOne(query.addCriteria(Criteria.where("employeeId").is(taskDetails.get(i).getTaskassignFrom())), Employee.class);

						taskDetails.get(i).setAssignFrom(employee.getEmployeefirstName()+" "+employee.getEmployeemiddleName()+" "+employee.getEmployeelastName());

						employee = new Employee();
						query = new Query();
						employee = mongoTemplate.findOne(query.addCriteria(Criteria.where("employeeId").is(taskDetails.get(i).getTaskassignTo())), Employee.class);

						taskDetails.get(i).setAssignTo(employee.getEmployeefirstName()+" "+employee.getEmployeemiddleName()+" "+employee.getEmployeelastName());
					}
				}

				query = new Query();
				List<TaskHistory> taskHistoryDetails = mongoTemplate.find(query.addCriteria(Criteria.where("taskId").is(taskId)), TaskHistory.class);

				DateFormat dateFormat = new SimpleDateFormat("d/M/yyy");

				for(int i=0;i<taskHistoryDetails.size();i++)
				{
					dateFormat = new SimpleDateFormat("d/M/yyy");
					taskHistoryDetails.get(i).setStartDate1(dateFormat.format(taskHistoryDetails.get(i).getStartDate()));

					dateFormat = new SimpleDateFormat("d/M/yyy");
					taskHistoryDetails.get(i).setExpectedEndDate1(dateFormat.format(taskHistoryDetails.get(i).getExpectedEndDate()));

					dateFormat = new SimpleDateFormat("d/M/yyy");
					taskHistoryDetails.get(i).setActualEndDate1(dateFormat.format(taskHistoryDetails.get(i).getActualEndDate()));
				}

				List<Project> projectList = GetUserAssigenedProjectList(req,res);

				model.addAttribute("taskDetails", taskDetails);
				model.addAttribute("taskHistoryDetails", taskHistoryDetails);
				model.addAttribute("employeeList", getAllEmployeeName());
				model.addAttribute("projectList", projectList);

				return "EditTask";
			}
			catch(Exception e)
			{
				return null;
			}

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value = "/EditTask", method = RequestMethod.POST)
	public String EditTask(@ModelAttribute Task task, Model model)
	{
		try {
			try
			{
				task = new Task(task.getTaskId(), task.getProjectId(), task.getBuildingId(),  task.getWingId(), task.getFloorId(), task.getFlatId(),task.getTaskName(), task.getTaskassignFrom(),task.getTaskassignTo(),  task.getTaskassingDate(),  task.getTaskendDate(), task.getTaskStatus(), task.getCreationDate(),task.getUpdateDate(), task.getUserName());
				taskRepository.save(task);
			}
			catch(DuplicateKeyException de)
			{

			}

			List<Employee> employeeList = employeeRepository.findAll();
			List<Task> taskDetails = taskRepository.findAll();

			try
			{
				for(int i=0; i<taskDetails.size();i++)
				{
					Query query = new Query();
					if(!taskDetails.get(i).getProjectId().equals("ALL"))
					{
						query = new Query();
						Project projectDetails = new Project();
						projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(taskDetails.get(i).getProjectId())), Project.class);
						taskDetails.get(i).setProjectName(projectDetails.getProjectName());
					}
					else
					{
						taskDetails.get(i).setProjectName("ALL");	
					}

					if(!taskDetails.get(i).getBuildingId().equals("ALL"))
					{
						query = new Query();
						ProjectBuilding projectBuilding = new ProjectBuilding();
						projectBuilding = mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(taskDetails.get(i).getBuildingId())), ProjectBuilding.class);
						taskDetails.get(i).setBuildingName(projectBuilding.getBuildingName());
					}
					else
					{
						taskDetails.get(i).setBuildingName("ALL");
					}

					if(!taskDetails.get(i).getWingId().equals("ALL"))
					{
						query = new Query();
						ProjectWing projectWing = new ProjectWing();
						projectWing = mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(taskDetails.get(i).getWingId())), ProjectWing.class);
						taskDetails.get(i).setWingName(projectWing.getWingName());
					}
					else
					{
						taskDetails.get(i).setWingName("ALL");
					}

					if(!taskDetails.get(i).getFloorId().equals("ALL"))
					{
						query = new Query();
						Floor floorDetails = new Floor();
						floorDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("floorId").is(taskDetails.get(i).getFloorId())), Floor.class);
						taskDetails.get(i).setFloorName(floorDetails.getFloortypeName());
					}
					else
					{
						taskDetails.get(i).setFloorName("ALL");
					}

					if(!taskDetails.get(i).getFlatId().equals("ALL"))
					{
						query = new Query();
						Flat flatDetails = new Flat();
						flatDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("flatId").is(taskDetails.get(i).getFlatId())), Flat.class);
						taskDetails.get(i).setFlatNumber(flatDetails.getFlatNumber());
					}
					else
					{
						taskDetails.get(i).setFlatNumber("ALL");
					}

					for(int j=0;j<employeeList.size();j++)
					{
						if(taskDetails.get(i).getTaskassignFrom().equals(employeeList.get(j).getEmployeeId()))
						{
							taskDetails.get(i).setAssignFrom(employeeList.get(j).getEmployeefirstName()+" "+employeeList.get(j).getEmployeemiddleName()+" "+employeeList.get(j).getEmployeelastName());
						}

						if(taskDetails.get(i).getTaskassignTo().equals(employeeList.get(j).getEmployeeId()))
						{
							taskDetails.get(i).setAssignTo(employeeList.get(j).getEmployeefirstName()+" "+employeeList.get(j).getEmployeemiddleName()+" "+employeeList.get(j).getEmployeelastName());
						}
					}

				}
			}
			catch(Exception e)
			{
				System.out.println("Excepion = "+e.toString());
			}

			model.addAttribute("taskDetails", taskDetails);
			return "TaskMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/getEmployeeListByDepartment")
	public List<Employee> getEmployeeListByDepartment(@RequestParam("employeeId") String employeeId)
	{
		String departmentId="";
		Query query = new Query();
		List<Employee> employeeList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(employeeId)), Employee.class);

		if(employeeList1.size()!=0)
		{
			departmentId = employeeList1.get(0).getDepartmentId();
		}

		query = new Query();
		List<Employee> employeeList = mongoTemplate.find(query.addCriteria(Criteria.where("departmentId").is(departmentId)), Employee.class);

		return employeeList;
	}

	@RequestMapping("/TaskMaster")
	public String TaskMaster(Model model)
	{
		try {
			List<Employee> employeeList = employeeRepository.findAll();
			List<Task> taskDetails = taskRepository.findAll();

			try
			{
				for(int i=0; i<taskDetails.size();i++)
				{
					Query query = new Query();
					if(!taskDetails.get(i).getProjectId().equals("ALL"))
					{
						query = new Query();
						Project projectDetails = new Project();
						projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(taskDetails.get(i).getProjectId())), Project.class);
						taskDetails.get(i).setProjectName(projectDetails.getProjectName());
					}
					else
					{
						taskDetails.get(i).setProjectName("ALL");	
					}

					if(!taskDetails.get(i).getBuildingId().equals("ALL"))
					{
						query = new Query();
						ProjectBuilding projectBuilding = new ProjectBuilding();
						projectBuilding = mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(taskDetails.get(i).getBuildingId())), ProjectBuilding.class);
						taskDetails.get(i).setBuildingName(projectBuilding.getBuildingName());
					}
					else
					{
						taskDetails.get(i).setBuildingName("ALL");
					}

					if(!taskDetails.get(i).getWingId().equals("ALL"))
					{
						query = new Query();
						ProjectWing projectWing = new ProjectWing();
						projectWing = mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(taskDetails.get(i).getWingId())), ProjectWing.class);
						taskDetails.get(i).setWingName(projectWing.getWingName());
					}
					else
					{
						taskDetails.get(i).setWingName("ALL");
					}

					if(!taskDetails.get(i).getFloorId().equals("ALL"))
					{
						query = new Query();
						Floor floorDetails = new Floor();
						floorDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("floorId").is(taskDetails.get(i).getFloorId())), Floor.class);
						taskDetails.get(i).setFloorName(floorDetails.getFloortypeName());
					}
					else
					{
						taskDetails.get(i).setFloorName("ALL");
					}

					if(!taskDetails.get(i).getFlatId().equals("ALL"))
					{
						query = new Query();
						Flat flatDetails = new Flat();
						flatDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("flatId").is(taskDetails.get(i).getFlatId())), Flat.class);
						taskDetails.get(i).setFlatNumber(flatDetails.getFlatNumber());
					}
					else
					{
						taskDetails.get(i).setFlatNumber("ALL");
					}

					for(int j=0;j<employeeList.size();j++)
					{
						if(taskDetails.get(i).getTaskassignFrom().equals(employeeList.get(j).getEmployeeId()))
						{
							taskDetails.get(i).setAssignFrom(employeeList.get(j).getEmployeefirstName()+" "+employeeList.get(j).getEmployeemiddleName()+" "+employeeList.get(j).getEmployeelastName());
						}

						if(taskDetails.get(i).getTaskassignTo().equals(employeeList.get(j).getEmployeeId()))
						{
							taskDetails.get(i).setAssignTo(employeeList.get(j).getEmployeefirstName()+" "+employeeList.get(j).getEmployeemiddleName()+" "+employeeList.get(j).getEmployeelastName());
						}
					}

				}
			}
			catch(Exception e)
			{
				System.out.println("Excepion = "+e.toString());
			}

			model.addAttribute("taskDetails", taskDetails);
			return "TaskMaster";

		}catch (Exception e) {
			return "login";
		}
	}


	public long TaskHistoryId()
	{
		long taskHistoryId=0;
		long cnt  = taskHistoryRepository.count();

		if(cnt==0) 
		{
			taskHistoryId = 1;
			return taskHistoryId;    		
		}
		else
		{
			List<TaskHistory> taskHistories = taskHistoryRepository.findAll();
			taskHistoryId = cnt + taskHistories.get(taskHistories.size()-1).getTaskHistoryId();
			return taskHistoryId;
		}

	}

	@ResponseBody
	@RequestMapping("/AddOtherTasks")
	public List<TaskHistory> AddOtherTasks(@RequestParam("taskId") String taskId, @RequestParam("taskDescription") String taskDescription, @RequestParam("otherTaskStatus") String otherTaskStatus, @RequestParam("startDate") String startDate, @RequestParam("expectedEndDate") String expectedEndDate, @RequestParam("actualEndDate") String actualEndDate)
	{
		try
		{
			TaskHistory taskHistory = new TaskHistory(TaskHistoryId(), taskId, taskDescription, otherTaskStatus, new SimpleDateFormat("d/M/yyyy").parse(startDate), new SimpleDateFormat("d/M/yyyy").parse(expectedEndDate), new SimpleDateFormat("d/M/yyyy").parse(actualEndDate));
			taskHistoryRepository.save(taskHistory);

			Query query = new Query();
			List<TaskHistory> taskHistoryDetails = mongoTemplate.find(query.addCriteria(Criteria.where("taskId").is(taskId)), TaskHistory.class);

			DateFormat dateFormat = new SimpleDateFormat("d/M/yyy");

			for(int i=0;i<taskHistoryDetails.size();i++)
			{
				dateFormat = new SimpleDateFormat("d/M/yyy");
				taskHistoryDetails.get(i).setStartDate1(dateFormat.format(taskHistoryDetails.get(i).getStartDate()));

				dateFormat = new SimpleDateFormat("d/M/yyy");
				taskHistoryDetails.get(i).setExpectedEndDate1(dateFormat.format(taskHistoryDetails.get(i).getExpectedEndDate()));

				dateFormat = new SimpleDateFormat("d/M/yyy");
				taskHistoryDetails.get(i).setActualEndDate1(dateFormat.format(taskHistoryDetails.get(i).getActualEndDate()));
			}

			return taskHistoryDetails; 
		}
		catch(Exception e)
		{
			return null;
		}
	}

	@ResponseBody
	@RequestMapping("/EditOtherTask")
	public List<TaskHistory> EditOtherTask(@RequestParam("taskId") String taskId, @RequestParam("taskHistoryId") String taskHistoryId)
	{
		try
		{
			Query query = new Query();
			List<TaskHistory> taskHistories = mongoTemplate.find(query.addCriteria(Criteria.where("taskHistoryId").is(Long.parseLong(taskHistoryId)).and("taskId").is(taskId)), TaskHistory.class);

			if(taskHistories.size()!=0)
			{
				DateFormat dateFormat = new SimpleDateFormat("d/M/yyy");

				for(int i=0;i<taskHistories.size();i++)
				{
					dateFormat = new SimpleDateFormat("d/M/yyy");
					taskHistories.get(i).setStartDate1(dateFormat.format(taskHistories.get(i).getStartDate()));

					dateFormat = new SimpleDateFormat("d/M/yyy");
					taskHistories.get(i).setExpectedEndDate1(dateFormat.format(taskHistories.get(i).getExpectedEndDate()));

					dateFormat = new SimpleDateFormat("d/M/yyy");
					taskHistories.get(i).setActualEndDate1(dateFormat.format(taskHistories.get(i).getActualEndDate()));
				}

				return taskHistories;
			}
			else
			{
				return null;
			}
		}
		catch(Exception e)
		{
			return null;
		}
	}

	@ResponseBody
	@RequestMapping("/UpdateOtherTasks")
	public List<TaskHistory> UpdateOtherTasks(@RequestParam("taskId") String taskId, @RequestParam("taskHistoryId") String taskHistoryId, @RequestParam("taskDescription") String taskDescription, @RequestParam("otherTaskStatus") String otherTaskStatus, @RequestParam("startDate") String startDate, @RequestParam("expectedEndDate") String expectedEndDate, @RequestParam("actualEndDate") String actualEndDate)
	{
		try
		{
			Query query = new Query();
			TaskHistory taskHistory = mongoTemplate.findOne(query.addCriteria(Criteria.where("taskHistoryId").is(Long.parseLong(taskHistoryId)).and("taskId").is(taskId)), TaskHistory.class);

			if(taskHistory!=null)
			{
				//TaskHistory taskHistory = new TaskHistory(Long.parseLong(taskHistoryId) ,taskId, taskDescription, otherTaskStatus, new SimpleDateFormat("d/M/yyyy").parse(startDate), new SimpleDateFormat("d/M/yyyy").parse(expectedEndDate), new SimpleDateFormat("d/M/yyyy").parse(actualEndDate));

				taskHistory.setTaskDescription(taskDescription);
				taskHistory.setOtherTaskStatus(otherTaskStatus);
				taskHistory.setStartDate(new SimpleDateFormat("d/M/yyyy").parse(startDate));
				taskHistory.setExpectedEndDate(new SimpleDateFormat("d/M/yyyy").parse(expectedEndDate));
				taskHistory.setActualEndDate(new SimpleDateFormat("d/M/yyyy").parse(actualEndDate));

				taskHistoryRepository.save(taskHistory);

				query = new Query();
				List<TaskHistory> taskHistoryDetails = mongoTemplate.find(query.addCriteria(Criteria.where("taskId").is(taskId)), TaskHistory.class);

				DateFormat dateFormat = new SimpleDateFormat("d/M/yyy");

				for(int i=0;i<taskHistoryDetails.size();i++)
				{
					dateFormat = new SimpleDateFormat("d/M/yyy");
					taskHistoryDetails.get(i).setStartDate1(dateFormat.format(taskHistoryDetails.get(i).getStartDate()));

					dateFormat = new SimpleDateFormat("d/M/yyy");
					taskHistoryDetails.get(i).setExpectedEndDate1(dateFormat.format(taskHistoryDetails.get(i).getExpectedEndDate()));

					dateFormat = new SimpleDateFormat("d/M/yyy");
					taskHistoryDetails.get(i).setActualEndDate1(dateFormat.format(taskHistoryDetails.get(i).getActualEndDate()));
				}

				return taskHistoryDetails;
			}
			else
			{
				return null;
			}

		}
		catch(Exception e)
		{
			return null;
		}
	}


	//@ResponseBody
	@RequestMapping(value = "/EmployeeTaskReport")
	public ResponseEntity<byte[]> EmployeeTaskReport(@RequestParam("taskId") String taskId, ModelMap model, HttpServletRequest req, HttpServletResponse res, HttpServletResponse response)
	{

		int index=0;
		JasperPrint print;
		try 
		{	

			Query query = new Query();
			String userId = (String)req.getSession().getAttribute("user");

			query = new Query();
			List<Login> loginDetails = mongoTemplate.find(query.addCriteria(Criteria.where("userId").is(userId)),Login.class);

			query = new Query();
			List<Employee> userDetails = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.get(0).getEmployeeId())),Employee.class);

			Collection c = new ArrayList();
			HashMap jmap = new HashMap();

			SimpleDateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");
			List<Company> companyDetails= companyRepository.findAll();
			try {
				query =new Query();
				City companyCityDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("cityId").is(companyDetails.get(0).getCityId())), City.class);
				companyDetails.get(0).setCityId(companyCityDetails.getCityName());
				query =new Query();
				LocationArea companyLocationareaDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("locationareaId").is(companyDetails.get(0).getLocationareaId())), LocationArea.class);
				companyDetails.get(0).setLocationareaId(companyLocationareaDetails.getLocationareaName());
			}catch (Exception e) {	
				e.printStackTrace();
			}

			jmap = null;

			Date date = new Date();  
			String todayDate= createDateFormat.format(date);

			query = new Query();
			List<Employee> employeeList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("Active")),Employee.class);

			query = new Query();
			List<Task> employeetaskDetails = mongoTemplate.find(query.addCriteria(Criteria.where("taskId").is(taskId)),Task.class);

			query = new Query();
			if(!employeetaskDetails.get(0).getProjectId().equals("ALL"))
			{
				query = new Query();
				Project projectDetails = new Project();
				projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(employeetaskDetails.get(0).getProjectId())), Project.class);
				employeetaskDetails.get(0).setProjectName(projectDetails.getProjectName());
			}
			else
			{
				employeetaskDetails.get(0).setProjectName("ALL");	
			}

			if(!employeetaskDetails.get(0).getBuildingId().equals("ALL"))
			{
				query = new Query();
				ProjectBuilding projectBuilding = new ProjectBuilding();
				projectBuilding = mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(employeetaskDetails.get(0).getBuildingId())), ProjectBuilding.class);
				employeetaskDetails.get(0).setBuildingName(projectBuilding.getBuildingName());
			}
			else
			{
				employeetaskDetails.get(0).setBuildingName("ALL");
			}

			if(!employeetaskDetails.get(0).getWingId().equals("ALL"))
			{
				query = new Query();
				ProjectWing projectWing = new ProjectWing();
				projectWing = mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(employeetaskDetails.get(0).getWingId())), ProjectWing.class);
				employeetaskDetails.get(0).setWingName(projectWing.getWingName());
			}
			else
			{
				employeetaskDetails.get(0).setWingName("ALL");
			}

			if(!employeetaskDetails.get(0).getFloorId().equals("ALL"))
			{
				query = new Query();
				Floor floorDetails = new Floor();
				floorDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("floorId").is(employeetaskDetails.get(0).getFloorId())), Floor.class);
				employeetaskDetails.get(0).setFloorName(floorDetails.getFloortypeName());
			}
			else
			{
				employeetaskDetails.get(0).setFloorName("ALL");
			}

			if(!employeetaskDetails.get(0).getFlatId().equals("ALL"))
			{
				query = new Query();
				Flat flatDetails = new Flat();
				flatDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("flatId").is(employeetaskDetails.get(0).getFlatId())), Flat.class);
				employeetaskDetails.get(0).setFlatNumber(flatDetails.getFlatNumber());
			}
			else
			{
				employeetaskDetails.get(0).setFlatNumber("ALL");
			}

			DateFormat dateFormat = new SimpleDateFormat("d/M/yyy");

			employeetaskDetails.get(0).setTaskassingDate1(dateFormat.format(employeetaskDetails.get(0).getTaskassingDate()));

			employeetaskDetails.get(0).setTaskendDate1(dateFormat.format(employeetaskDetails.get(0).getTaskendDate()));

			String taskassignTo="",taskassignFrom="";

			String projectDetails="";
			if(employeetaskDetails.get(0).getProjectName().equalsIgnoreCase("ALL"))
			{
				projectDetails="All project";
			}
			else
			{
				projectDetails=employeetaskDetails.get(0).getProjectName();

				if(employeetaskDetails.get(0).getBuildingName().equalsIgnoreCase("ALL"))
				{
					projectDetails=projectDetails+", All Building";
				}
				else
				{
					projectDetails=projectDetails+", "+employeetaskDetails.get(0).getBuildingName();
					if(employeetaskDetails.get(0).getWingName().equalsIgnoreCase("ALL"))
					{
						projectDetails=projectDetails+", All Wing";
					} 
					else
					{
						projectDetails=projectDetails+", "+employeetaskDetails.get(0).getWingName();
						if(employeetaskDetails.get(0).getFloorName().equalsIgnoreCase("ALL"))
						{
							projectDetails=projectDetails+", All Floor";
						}
						else
						{
							projectDetails=projectDetails+", "+employeetaskDetails.get(0).getFloorName();
							if(employeetaskDetails.get(0).getFlatId().equalsIgnoreCase("ALL"))
							{
								projectDetails=projectDetails+", All Flats.";
							}
							else
							{
								projectDetails=projectDetails+" "+employeetaskDetails.get(0).getFlatNumber();
							}
						}

					}
				}

			}

			for(int i=0;i<employeeList.size();i++)
			{
				if(employeeList.get(i).getEmployeeId().equalsIgnoreCase(employeetaskDetails.get(0).getTaskassignFrom()))
				{
					taskassignFrom=employeeList.get(i).getEmployeefirstName()+" "+employeeList.get(i).getEmployeelastName();
				}

				if(employeeList.get(i).getEmployeeId().equalsIgnoreCase(employeetaskDetails.get(0).getTaskassignTo()))
				{
					taskassignTo=employeeList.get(i).getEmployeefirstName()+" "+employeeList.get(i).getEmployeelastName();
				}

			}

			query = new Query();
			List<TaskHistory> taskHistoryDetails = mongoTemplate.find(query.addCriteria(Criteria.where("taskId").is(taskId)), TaskHistory.class);

			for(int i=0;i<taskHistoryDetails.size();i++)
			{
				taskHistoryDetails.get(i).setStartDate1(dateFormat.format(taskHistoryDetails.get(i).getStartDate()));

				taskHistoryDetails.get(i).setExpectedEndDate1(dateFormat.format(taskHistoryDetails.get(i).getExpectedEndDate()));

				taskHistoryDetails.get(i).setActualEndDate1(dateFormat.format(taskHistoryDetails.get(i).getActualEndDate()));
			}

			if(taskHistoryDetails.size()==0)
			{
				jmap = new HashMap();

				jmap.put("srno","");
				jmap.put("taskDescription","");
				jmap.put("startDate","");
				jmap.put("expectedEndDate","");
				jmap.put("otherTaskStatus","");
				jmap.put("actualEndDate","");

				c.add(jmap);
				jmap = null;
			}
			for(int i=0;i<taskHistoryDetails.size();i++)
			{
				jmap = new HashMap();

				jmap.put("srno",""+(i+1));
				jmap.put("taskDescription",""+taskHistoryDetails.get(i).getTaskDescription());
				jmap.put("startDate",""+taskHistoryDetails.get(i).getStartDate1());
				jmap.put("expectedEndDate",""+taskHistoryDetails.get(i).getExpectedEndDate1());
				jmap.put("otherTaskStatus",""+taskHistoryDetails.get(i).getOtherTaskStatus());
				if(taskHistoryDetails.get(i).getOtherTaskStatus().equalsIgnoreCase("Complete"))
				{
					jmap.put("actualEndDate",""+taskHistoryDetails.get(i).getActualEndDate1());
				}
				else
				{
					jmap.put("actualEndDate","");
				}

				c.add(jmap);
				jmap = null;
			}

			JRDataSource dataSource = new JRMapCollectionDataSource(c);
			Map<String, Object> parameterMap = new HashMap();

			parameterMap.put("date", ""+todayDate);

			parameterMap.put("companyName", ""+companyDetails.get(0).getCompanyName());

			parameterMap.put("companyAddress", ""+companyDetails.get(0).getCompanyAddress()+", "+companyDetails.get(0).getLocationareaId()+", "+companyDetails.get(0).getCityId());

			// parameterMap.put("userName", ""+userDetails.get(0).getEmployeefirstName()+" "+userDetails.get(0).getEmployeelastName());

			parameterMap.put("userName", ""+taskassignFrom);

			parameterMap.put("taskassignTo", ""+taskassignTo);

			parameterMap.put("taskassignFrom", ""+taskassignFrom);

			parameterMap.put("taskassingDate", ""+employeetaskDetails.get(0).getTaskassingDate1());

			parameterMap.put("taskendDate", ""+employeetaskDetails.get(0).getTaskendDate1());

			parameterMap.put("projectDetails", ""+projectDetails);


			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/EmployeeTaskReport.jasper");

			print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(print, baos);

			byte[] contents = baos.toByteArray();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			String filename = "EmployeeTaskReport.pdf";

			JasperExportManager.exportReportToPdfStream(print, baos);

			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
			ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
			response.setHeader("Content-Disposition", "inline; filename=" + filename );
			return resp;				    
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			return null;
		}  

	}

}

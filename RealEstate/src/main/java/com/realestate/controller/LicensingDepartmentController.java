package com.realestate.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.realestate.bean.Company;
import com.realestate.bean.Country;
import com.realestate.bean.
LicensingDocument;
import com.realestate.bean.Login;
import com.realestate.bean.Project;
import com.realestate.bean.ReraDocuments;
import com.realestate.bean.UserAssignedProject;
import com.realestate.configuration.CommanController;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.LicensingDocumentRepository;

@Controller
@RequestMapping("/")
public class LicensingDepartmentController {

	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	LicensingDocumentRepository LicensingdepartmentRepository;


	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping("/ProvisionalLicensingDepartmentMaster")
	public String LicensingDepartmentMaster(Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<Project> projectList = GetUserAssigenedProjectList(req,res);

			model.addAttribute("projectList", projectList);
			return "ProvisionalLicensingDepartmentMaster";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping("/AddProvisionalLicensingDocument")
	public String AddLagalDocument(@RequestParam("projectId") String projectId, Model model)
	{
		try {
			Query query = new Query();
			Company companyDetails =  mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectId)), Company.class);

			query = new Query();
			List<LicensingDocument> provisionalDocumentList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("status").is("Provisional")), LicensingDocument.class);

			model.addAttribute("provisionalDocumentList", provisionalDocumentList);

			model.addAttribute("companyDetails", companyDetails);
			model.addAttribute("projectId", projectId);

			return "AddProvisionalLicensingDocument";

		}catch (Exception e) {
			return "login";
		}
	}


	public String knowFileExtension(@RequestParam("fileName") String fileName)
	{
		try
		{
			String fileName1 = new File(fileName).getName();
			int dotIndex = fileName1.lastIndexOf('.');
			String extension =  (dotIndex == -1) ? "" : fileName.substring(dotIndex + 1);

			return extension;
		}
		catch(Exception e)
		{
			e.toString();

			return null;
		}
	}


	public boolean UploadFileToPath(MultipartFile file, String path)
	{

		try {

			byte[] bytes = file.getBytes();
			BufferedOutputStream stream =new BufferedOutputStream(new FileOutputStream(new File(path)));
			stream.write(bytes);
			stream.flush();
			stream.close();

		}catch (Exception e) {
			System.out.println("Eooror = "+e);
			// TODO: handle exception
		}


		File f = new File(path);

		if (f.exists() && !f.isDirectory()) 
		{
			return true;
		} 
		else 
		{
			return false;
		}
	}

	@RequestMapping(value="/AddProvisionalLicensingDocument",method=RequestMethod.POST)
	public String AddLicensingDocument(@ModelAttribute LicensingDocument LicensingDepartment, @RequestParam("tokenDocument") MultipartFile tokenDocument, @RequestParam("nocDocument") MultipartFile nocDocument, Model model)
	{
		try {
			String tokenDocumentExtension="."+knowFileExtension(tokenDocument.getOriginalFilename());
			String nocDocumentextension="."+knowFileExtension(nocDocument.getOriginalFilename());

			int documentId=1;
			List<LicensingDocument> documentList1= LicensingdepartmentRepository.findAll();

			if(documentList1.size()!=0)
			{
				documentId=documentList1.get(documentList1.size()-1).getDocumentId()+1;
			}
			try
			{

				//for Token File
				String path = CommanController.GetLicensingDocumentsPath() +  "Licensing_" +LicensingDepartment.getProjectId()+"_"+documentId+"tokenDocument."+knowFileExtension(tokenDocument.getOriginalFilename());
				//String path = "D:/Data1/" +  "Licensing_" +LicensingDepartment.getProjectId()+"_"+documentId+"tokenDocument."+knowFileExtension(tokenDocument.getOriginalFilename());
				UploadFileToPath(tokenDocument, path);

				//for nocDocument File
				String path1 = CommanController.GetLicensingDocumentsPath() +  "Licensing_" +LicensingDepartment.getProjectId()+"_"+documentId+"nocDocument."+knowFileExtension(nocDocument.getOriginalFilename());
				//String path1 = "D:/Data1/" +  "Licensing_" +LicensingDepartment.getProjectId()+"_"+documentId+"nocDocument."+knowFileExtension(nocDocument.getOriginalFilename());
				UploadFileToPath(nocDocument, path1);

			}
			catch(Exception e)
			{
				System.out.println("1.File upload exception = "+e.toString());
			}

			try
			{
				LicensingDocument Licensingdepartment=new LicensingDocument();

				Licensingdepartment.setDocumentId(documentId);
				Licensingdepartment.setProjectId(LicensingDepartment.getProjectId());
				Licensingdepartment.setDescription(LicensingDepartment.getDescription());
				Licensingdepartment.setInvertedDate(LicensingDepartment.getInvertedDate());
				Licensingdepartment.setTokenNumber(LicensingDepartment.getTokenNumber());
				Licensingdepartment.setChallanAmount(LicensingDepartment.getChallanAmount());

				Licensingdepartment.setTokenDocumentExtension(tokenDocumentExtension);
				Licensingdepartment.setNocDocumentextension(nocDocumentextension);
				Licensingdepartment.setStatus("Provisional");
				Licensingdepartment.setCreationDate(new Date());
				Licensingdepartment.setUpdateDate(new Date());

				LicensingdepartmentRepository.save(Licensingdepartment);

				model.addAttribute("documentStatus", "Success");
			}
			catch(Exception e)
			{

			}

			Query query = new Query();
			List<LicensingDocument> provisionalDocumentList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(LicensingDepartment.getProjectId()).and("status").is("Provisional")), LicensingDocument.class);

			model.addAttribute("provisionalDocumentList", provisionalDocumentList);
			model.addAttribute("projectId", LicensingDepartment.getProjectId());
			return "AddProvisionalLicensingDocument";

		}catch (Exception e) {
			return "login";
		}
	}






	@RequestMapping("/FinalLicensingDepartmentMaster")
	public String FinalLicensingDepartmentMaster(Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<Project> projectList = GetUserAssigenedProjectList(req,res);

			model.addAttribute("projectList", projectList);
			return "FinalLicensingDepartmentMaster";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping("/AddFinalLicensingDocument")
	public String AddFinalLicensingDocument(@RequestParam("projectId") String projectId, Model model)
	{
		try {
			Query query = new Query();
			Company companyDetails =  mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectId)), Company.class);

			query = new Query();
			List<LicensingDocument> finalDocumentList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("status").is("Final")), LicensingDocument.class);

			model.addAttribute("finalDocumentList", finalDocumentList);

			model.addAttribute("companyDetails", companyDetails);
			model.addAttribute("projectId", projectId);

			return "AddFinalLicensingDocument";

		}catch (Exception e) {
			return "login";
		}
	}






	@RequestMapping(value="/AddFinalLicensingDocument",method=RequestMethod.POST)
	public String AddLicensingFinalDocument(@ModelAttribute LicensingDocument LicensingDepartment, @RequestParam("tokenDocument") MultipartFile tokenDocument, @RequestParam("nocDocument") MultipartFile nocDocument, Model model)
	{
		try {
			String tokenDocumentExtension="."+knowFileExtension(tokenDocument.getOriginalFilename());
			String nocDocumentextension="."+knowFileExtension(nocDocument.getOriginalFilename());

			int documentId=1;
			List<LicensingDocument> documentList1= LicensingdepartmentRepository.findAll();

			if(documentList1.size()!=0)
			{
				documentId=documentList1.get(documentList1.size()-1).getDocumentId()+1;
			}
			try
			{

				//for Token File
				String path =CommanController.GetLicensingDocumentsPath() +  "Licensing_" +LicensingDepartment.getProjectId()+"_"+documentId+"tokenDocument."+knowFileExtension(tokenDocument.getOriginalFilename());
				//String path = "D:/Data1/" +  "Licensing_" +LicensingDepartment.getProjectId()+"_"+documentId+"tokenDocument."+knowFileExtension(tokenDocument.getOriginalFilename());
				UploadFileToPath(tokenDocument, path);

				//for nocDocument File
				String path1 = CommanController.GetLicensingDocumentsPath() +  "Licensing_" +LicensingDepartment.getProjectId()+"_"+documentId+"nocDocument."+knowFileExtension(nocDocument.getOriginalFilename());
				//String path1 = "D:/Data1/" +  "Licensing_" +LicensingDepartment.getProjectId()+"_"+documentId+"nocDocument."+knowFileExtension(nocDocument.getOriginalFilename());
				UploadFileToPath(nocDocument, path1);

			}
			catch(Exception e)
			{
				System.out.println("1.File upload exception = "+e.toString());
			}

			try
			{
				LicensingDocument Licensingdepartment=new LicensingDocument();

				Licensingdepartment.setDocumentId(documentId);
				Licensingdepartment.setProjectId(LicensingDepartment.getProjectId());
				Licensingdepartment.setDescription(LicensingDepartment.getDescription());
				Licensingdepartment.setInvertedDate(LicensingDepartment.getInvertedDate());
				Licensingdepartment.setTokenNumber(LicensingDepartment.getTokenNumber());
				Licensingdepartment.setChallanAmount(LicensingDepartment.getChallanAmount());

				Licensingdepartment.setTokenDocumentExtension(tokenDocumentExtension);
				Licensingdepartment.setNocDocumentextension(nocDocumentextension);
				Licensingdepartment.setStatus("Final");
				Licensingdepartment.setCreationDate(new Date());
				Licensingdepartment.setUpdateDate(new Date());

				LicensingdepartmentRepository.save(Licensingdepartment);

				model.addAttribute("documentStatus", "Success");
			}
			catch(Exception e)
			{

			}

			Query query = new Query();
			List<LicensingDocument> finalDocumentList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(LicensingDepartment.getProjectId()).and("status").is("Final")), LicensingDocument.class);

			model.addAttribute("finalDocumentList", finalDocumentList);

			model.addAttribute("projectId", LicensingDepartment.getProjectId());
			return "AddFinalLicensingDocument";

		}catch (Exception e) {
			return "login";
		}
	}



	@RequestMapping("/ViewLicensingTokenDocument")
	public String ViewLicensingTokenDocument(@RequestParam("documentId") int documentId, Model model)
	{
		try {
			Query query = new Query();
			LicensingDocument documentDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("documentId").is(documentId)), LicensingDocument.class);

			String tokenfileAddress = CommanController.GetLicensingDocumentsPath() +"Licensing_" +documentDetails.getProjectId()+"_"+documentId+"tokenDocument"+documentDetails.getTokenDocumentExtension();

			//String tokenfileAddress = "D:/Data1/" + "Licensing_" +documentDetails.getProjectId()+"_"+documentId+"tokenDocument"+documentDetails.getTokenDocumentExtension();

			if(documentDetails.getTokenDocumentExtension().equals(".jpg") || documentDetails.getTokenDocumentExtension().equals(".jpeg") || documentDetails.getTokenDocumentExtension().equals(".png"))
			{
				model.addAttribute("fileAddress", tokenfileAddress);
				return "DisplayImage";	
			}	
			else
			{
				model.addAttribute("fileAddress", tokenfileAddress);
				return "DisplayPDF";
			}

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/ViewLicensingnocDocument")
	public String ViewLicensingnocDocument(@RequestParam("documentId") int documentId, Model model)
	{
		try {
			Query query = new Query();
			LicensingDocument documentDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("documentId").is(documentId)), LicensingDocument.class);

			String tokenfileAddress = CommanController.GetLicensingDocumentsPath() +"Licensing_" +documentDetails.getProjectId()+"_"+documentId+"nocDocument"+documentDetails.getNocDocumentextension();

			//String tokenfileAddress = "D:/Data1/" + "Licensing_" +documentDetails.getProjectId()+"_"+documentId+"nocDocument"+documentDetails.getNocDocumentextension();

			if(documentDetails.getTokenDocumentExtension().equals(".jpg") || documentDetails.getTokenDocumentExtension().equals(".jpeg") || documentDetails.getTokenDocumentExtension().equals(".png"))
			{
				model.addAttribute("fileAddress", tokenfileAddress);
				return "DisplayImage";	
			}	
			else
			{
				model.addAttribute("fileAddress", tokenfileAddress);
				return "DisplayPDF";
			}

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/DeleteLicensingProvisinalDocument")
	public List<LicensingDocument> DeleteLicensingProvisinalDocument(@RequestParam("documentId") int documentId, @RequestParam("projectId") String projectId , Model model)
	{
		Query query = new Query();

		try
		{
			LicensingDocument documentDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("documentId").is(documentId)), LicensingDocument.class);

			String tokenfileAddress = CommanController.GetLicensingDocumentsPath() +"Licensing_" +projectId+"_"+documentId+"tokenDocument"+documentDetails.getTokenDocumentExtension();

			String nocfileAddress = CommanController.GetLicensingDocumentsPath() +  "Licensing_" +projectId+"_"+documentId+"nocDocument"+documentDetails.getNocDocumentextension();

			//String tokenfileAddress = "D:/Data1/" + "Licensing_" +projectId+"_"+documentId+"tokenDocument"+documentDetails.getTokenDocumentExtension();
			// String nocfileAddress = "D:/Data1/" + "Licensing_" +projectId+"_"+documentId+"nocDocument"+documentDetails.getNocDocumentextension();

			//String fileAddress="D:/Data1/"+documentId+documentDetails.getExtension();

			File file1 = new File(tokenfileAddress);
			file1.delete();

			File file2 = new File(nocfileAddress);
			file2.delete();

		}
		catch(Exception e)
		{
			System.out.println("File Delete Exception = "+e.toString());
		}

		try
		{
			query = new Query();
			mongoTemplate.remove(query.addCriteria(Criteria.where("documentId").is(documentId)), LicensingDocument.class);
		}
		catch(Exception e)
		{
			System.out.println("Record delete exception = "+e.toString());
		}

		query = new Query();
		List<LicensingDocument> documentList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("status").is("Provisional")), LicensingDocument.class);

		return documentList;
	}

	@ResponseBody
	@RequestMapping("/DeleteLicensingFinalDocument")
	public List<LicensingDocument> DeleteLicensingFinalDocument(@RequestParam("documentId") int documentId, @RequestParam("projectId") String projectId , Model model)
	{
		Query query = new Query();

		try
		{
			LicensingDocument documentDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("documentId").is(documentId)), LicensingDocument.class);

			String tokenfileAddress = CommanController.GetLicensingDocumentsPath() +"Licensing_" +projectId+"_"+documentId+"tokenDocument"+documentDetails.getTokenDocumentExtension();

			String nocfileAddress = CommanController.GetLicensingDocumentsPath() +  "Licensing_" +projectId+"_"+documentId+"nocDocument"+documentDetails.getNocDocumentextension();

			// String tokenfileAddress = "D:/Data1/" + "Licensing_" +projectId+"_"+documentId+"tokenDocument"+documentDetails.getTokenDocumentExtension();
			// String nocfileAddress = "D:/Data1/" + "Licensing_" +projectId+"_"+documentId+"nocDocument"+documentDetails.getNocDocumentextension();

			//String fileAddress="D:/Data1/"+documentId+documentDetails.getExtension();

			File file1 = new File(tokenfileAddress);
			file1.delete();

			File file2 = new File(nocfileAddress);
			file2.delete();

		}
		catch(Exception e)
		{
			System.out.println("File Delete Exception = "+e.toString());
		}

		try
		{
			query = new Query();
			mongoTemplate.remove(query.addCriteria(Criteria.where("documentId").is(documentId)), LicensingDocument.class);
		}
		catch(Exception e)
		{
			System.out.println("Record delete exception = "+e.toString());
		}

		query = new Query();
		List<LicensingDocument> documentList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("status").is("Final")), LicensingDocument.class);

		return documentList;
	}




	@RequestMapping("/PartCompletionDepartmentMaster")
	public String PartCompletionDepartmentMaster(Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<Project> projectList = GetUserAssigenedProjectList(req,res);

			model.addAttribute("projectList", projectList);
			return "PartCompletionDepartmentMaster";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping("/AddPartCompletionLicensingDocument")
	public String AddPartCompletionLicensingDocument(@RequestParam("projectId") String projectId, Model model)
	{
		try {
			Query query = new Query();
			Company companyDetails =  mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectId)), Company.class);

			query = new Query();
			List<LicensingDocument> provisionalDocumentList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("status").is("PartCompletion")), LicensingDocument.class);

			model.addAttribute("provisionalDocumentList", provisionalDocumentList);

			model.addAttribute("companyDetails", companyDetails);
			model.addAttribute("projectId", projectId);

			return "AddPartCompletionLicensingDocument";

		}catch (Exception e) {
			return "login";
		}
	}



	@RequestMapping(value="/AddPartCompletionLicensingDocument",method=RequestMethod.POST)
	public String AddPartCompletionLicensingDocument(@ModelAttribute LicensingDocument LicensingDepartment, @RequestParam("tokenDocument") MultipartFile tokenDocument, @RequestParam("nocDocument") MultipartFile nocDocument, Model model)
	{
		try {
			String tokenDocumentExtension="."+knowFileExtension(tokenDocument.getOriginalFilename());
			String nocDocumentextension="."+knowFileExtension(nocDocument.getOriginalFilename());

			int documentId=1;
			List<LicensingDocument> documentList1= LicensingdepartmentRepository.findAll();

			if(documentList1.size()!=0)
			{
				documentId=documentList1.get(documentList1.size()-1).getDocumentId()+1;
			}
			try
			{

				//for Token File
				String path = CommanController.GetLicensingDocumentsPath() +  "Licensing_" +LicensingDepartment.getProjectId()+"_"+documentId+"tokenDocument."+knowFileExtension(tokenDocument.getOriginalFilename());
				//String path = "D:/Data1/" +  "Licensing_" +LicensingDepartment.getProjectId()+"_"+documentId+"tokenDocument."+knowFileExtension(tokenDocument.getOriginalFilename());
				UploadFileToPath(tokenDocument, path);

				//for nocDocument File
				String path1 = CommanController.GetLicensingDocumentsPath() + "Licensing_" +LicensingDepartment.getProjectId()+"_"+documentId+"nocDocument."+knowFileExtension(nocDocument.getOriginalFilename());
				//String path1 = "D:/Data1/" +  "Licensing_" +LicensingDepartment.getProjectId()+"_"+documentId+"nocDocument."+knowFileExtension(nocDocument.getOriginalFilename());
				UploadFileToPath(nocDocument, path1);

			}
			catch(Exception e)
			{
				System.out.println("1.File upload exception = "+e.toString());
			}

			try
			{
				LicensingDocument Licensingdepartment=new LicensingDocument();

				Licensingdepartment.setDocumentId(documentId);
				Licensingdepartment.setProjectId(LicensingDepartment.getProjectId());
				Licensingdepartment.setDescription(LicensingDepartment.getDescription());
				Licensingdepartment.setInvertedDate(LicensingDepartment.getInvertedDate());
				Licensingdepartment.setTokenNumber(LicensingDepartment.getTokenNumber());
				Licensingdepartment.setChallanAmount(LicensingDepartment.getChallanAmount());

				Licensingdepartment.setTokenDocumentExtension(tokenDocumentExtension);
				Licensingdepartment.setNocDocumentextension(nocDocumentextension);
				Licensingdepartment.setStatus("PartCompletion");
				Licensingdepartment.setCreationDate(new Date());
				Licensingdepartment.setUpdateDate(new Date());

				LicensingdepartmentRepository.save(Licensingdepartment);

				model.addAttribute("documentStatus", "Success");
			}
			catch(Exception e)
			{

			}

			Query query = new Query();
			List<LicensingDocument> provisionalDocumentList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(LicensingDepartment.getProjectId()).and("status").is("PartCompletion")), LicensingDocument.class);

			model.addAttribute("provisionalDocumentList", provisionalDocumentList);
			model.addAttribute("projectId", LicensingDepartment.getProjectId());
			return "AddProvisionalLicensingDocument";

		}catch (Exception e) {
			return "login";
		}
	}


	@ResponseBody
	@RequestMapping("/DeleteLicensingPartCompletionDocument")
	public List<LicensingDocument> DeleteLicensingPartCompletionDocument(@RequestParam("documentId") int documentId, @RequestParam("projectId") String projectId , Model model)
	{
		Query query = new Query();
		try
		{
			LicensingDocument documentDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("documentId").is(documentId)), LicensingDocument.class);

			String tokenfileAddress =CommanController.GetLicensingDocumentsPath() +"Licensing_" +projectId+"_"+documentId+"tokenDocument"+documentDetails.getTokenDocumentExtension();

			String nocfileAddress = CommanController.GetLicensingDocumentsPath() +  "Licensing_" +projectId+"_"+documentId+"nocDocument"+documentDetails.getNocDocumentextension();

			/* String tokenfileAddress = "D:/Data1/" + "Licensing_" +projectId+"_"+documentId+"tokenDocument"+documentDetails.getTokenDocumentExtension();
		    	  String nocfileAddress = "D:/Data1/" + "Licensing_" +projectId+"_"+documentId+"nocDocument"+documentDetails.getNocDocumentextension();
			 */
			//String fileAddress="D:/Data1/"+documentId+documentDetails.getExtension();
			File file1 = new File(tokenfileAddress);
			file1.delete();

			File file2 = new File(nocfileAddress);
			file2.delete();

		}
		catch(Exception e)
		{
			System.out.println("File Delete Exception = "+e.toString());
		}

		try
		{
			query = new Query();
			mongoTemplate.remove(query.addCriteria(Criteria.where("documentId").is(documentId)), LicensingDocument.class);
		}
		catch(Exception e)
		{
			System.out.println("Record delete exception = "+e.toString());
		}

		query = new Query();
		List<LicensingDocument> documentList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("status").is("PartCompletion")), LicensingDocument.class);

		return documentList;
	}

}

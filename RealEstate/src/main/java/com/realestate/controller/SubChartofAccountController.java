package com.realestate.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.ChartofAccount;
import com.realestate.bean.SubChartofAccount;
import com.realestate.repository.ChartofAccountRepository;
import com.realestate.repository.SubChartofAccountRepository;


@Controller
@RequestMapping("/")
public class SubChartofAccountController 
{

	@Autowired
	SubChartofAccountRepository subchartofaccountRepository;
	@Autowired
	ChartofAccountRepository chartofaccountRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	String subchartofaccountCode;

	private String SubChartofAccountCode()
	{
		long cnt  = subchartofaccountRepository.count();
		if(cnt<10)
		{
			subchartofaccountCode = "SCOA000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			subchartofaccountCode = "SCOA00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			subchartofaccountCode = "SCOA0"+(cnt+1);
		}
		else
		{
			subchartofaccountCode = "SCOA"+(cnt+1);
		}
		return subchartofaccountCode;
	}

	//code to reterive all chart account name

	private List<ChartofAccount> findAllChartofAccount()
	{
		List<ChartofAccount> chartofaccountList;
		chartofaccountList = chartofaccountRepository.findAll(new Sort(Sort.Direction.ASC,"chartaccountName"));
		return chartofaccountList;
	}


	@ResponseBody
	@RequestMapping(value="/getSubChartAccountList",method=RequestMethod.POST)
	public List<SubChartofAccount> SubChartAccountList(@RequestParam("chartaccountId") String chartaccountId, HttpSession session)
	{
		Query query = new Query();
		List<SubChartofAccount> subchartaccountList = mongoTemplate.find(query.addCriteria(Criteria.where("chartaccountId").is(chartaccountId)), SubChartofAccount.class);
		for(int i=0;i<subchartaccountList.size();i++)
		{
			try
			{
				query = new Query();
				List<ChartofAccount> chartofaccountDetails = mongoTemplate.find(query.addCriteria(Criteria.where("chartaccountId").is(subchartaccountList.get(i).getChartaccountId())),ChartofAccount.class);
				subchartaccountList.get(i).setChartaccountId(chartofaccountDetails.get(0).getChartaccountName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}
		return subchartaccountList;
	}

	@ResponseBody
	@RequestMapping(value="/getSubChartAccountNumberList",method=RequestMethod.POST)
	public List<SubChartofAccount> SubChartNumberList(@RequestParam("chartaccountId") Integer chartaccountId, HttpSession session)
	{
		Query query = new Query();
		List<SubChartofAccount> subchartaccountList = mongoTemplate.find(query.addCriteria(Criteria.where("chartaccountId").is(chartaccountId)), SubChartofAccount.class);
		for(int i=0;i<subchartaccountList.size();i++)
		{
			try
			{
				query = new Query();
				List<ChartofAccount> chartofaccountDetails = mongoTemplate.find(query.addCriteria(Criteria.where("chartaccountId").is(subchartaccountList.get(i).getChartaccountId())),ChartofAccount.class);
				subchartaccountList.get(i).setChartaccountId(chartofaccountDetails.get(0).getChartaccountName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}
		return subchartaccountList;
	}

	//SubChartofAccountMaster
	@RequestMapping("/SubChartofAccountMaster")
	public String SubChartofAccountMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<SubChartofAccount> subchartaccountList = subchartofaccountRepository.findAll();

			Query query = new Query();
			for(int i=0;i<subchartaccountList.size();i++)
			{
				try
				{
					query = new Query();
					List<ChartofAccount> chartofaccountDetails = mongoTemplate.find(query.addCriteria(Criteria.where("chartaccountId").is(subchartaccountList.get(i).getChartaccountId())),ChartofAccount.class);
					subchartaccountList.get(i).setChartaccountId(chartofaccountDetails.get(0).getChartaccountName());
				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}

			model.addAttribute("subchartofaccountCode",SubChartofAccountCode());
			model.addAttribute("chartofaccountList", findAllChartofAccount());
			model.addAttribute("subchartaccountList", subchartaccountList);
			return "SubChartofAccountMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/AddSubChartofAccount")
	public String addSubChartofAccount(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<SubChartofAccount> subchartaccountList = subchartofaccountRepository.findAll();

			model.addAttribute("subchartofaccountCode",SubChartofAccountCode());
			model.addAttribute("chartofaccountList", findAllChartofAccount());
			model.addAttribute("subchartaccountList", subchartaccountList);
			return "AddSubChartofAccount";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value = "/AddSubChartofAccount", method = RequestMethod.POST)
	public String subchartofaccountSave(@ModelAttribute SubChartofAccount subchartofaccount, Model model)
	{
		try {
			try
			{
				subchartofaccount = new SubChartofAccount(subchartofaccount.getSubchartaccountId(), subchartofaccount.getChartaccountId(), subchartofaccount.getChartaccountNumber(),subchartofaccount.getSubchartaccountNumber(), subchartofaccount.getSubchartaccountName(),subchartofaccount.getCreationDate(), subchartofaccount.getUpdateDate(), subchartofaccount.getUserName());
				subchartofaccountRepository.save(subchartofaccount);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}

			List<SubChartofAccount> subchartaccountList = subchartofaccountRepository.findAll();

			model.addAttribute("subchartofaccountCode",SubChartofAccountCode());
			model.addAttribute("chartofaccountList", findAllChartofAccount());
			model.addAttribute("subchartaccountList", subchartaccountList);

			return "AddSubChartofAccount";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/EditSubChartofAccount")
	public String EditSubChartofAccount(@RequestParam("subchartaccountId") String subchartaccountId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<SubChartofAccount> subchartofaccountDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subchartaccountId").is(subchartaccountId)),SubChartofAccount.class);

			query = new Query();
			List<ChartofAccount> chartofaccountDetails = mongoTemplate.find(query.addCriteria(Criteria.where("chartaccountId").is(subchartofaccountDetails.get(0).getChartaccountId())),ChartofAccount.class);

			model.addAttribute("chartaccountName", chartofaccountDetails.get(0).getChartaccountName());	
			model.addAttribute("subchartofaccountDetails", subchartofaccountDetails);
			model.addAttribute("chartofaccountList", findAllChartofAccount());
			return "EditSubChartofAccount";

		}catch (Exception e) {
			return "login";
		}
	}
	@RequestMapping(value="/EditSubChartofAccount", method=RequestMethod.POST)
	public String EditSubChartOfAccount(@ModelAttribute SubChartofAccount subchartofaccount, Model model)
	{
		try {
			try
			{
				subchartofaccount = new SubChartofAccount(subchartofaccount.getSubchartaccountId(), subchartofaccount.getChartaccountId(), subchartofaccount.getChartaccountNumber(),subchartofaccount.getSubchartaccountNumber(), subchartofaccount.getSubchartaccountName(),subchartofaccount.getCreationDate(), subchartofaccount.getUpdateDate(), subchartofaccount.getUserName());
				subchartofaccountRepository.save(subchartofaccount);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}

			List<SubChartofAccount> subchartaccountList = subchartofaccountRepository.findAll();

			model.addAttribute("subchartofaccountCode",SubChartofAccountCode());
			model.addAttribute("chartofaccountList", findAllChartofAccount());
			model.addAttribute("subchartaccountList", subchartaccountList);

			return "SubChartofAccountMaster";

		}catch (Exception e) {
			return "login";
		}
	}
}

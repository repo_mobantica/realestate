package com.realestate.controller;
import java.text.ParseException;  
import java.text.SimpleDateFormat;  
import java.util.Date;  
import java.util.Locale;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.realestate.bean.Agent;
import com.realestate.bean.Aggreement;
import com.realestate.bean.Bank;
import com.realestate.bean.Booking;
import com.realestate.bean.BookingCancelForm;
import com.realestate.bean.Budget;
import com.realestate.bean.City;
import com.realestate.bean.Country;
import com.realestate.bean.CustomerFlatCheckList;
import com.realestate.bean.CustomerLoanDetails;
import com.realestate.bean.CustomerPaymentDetails;
import com.realestate.bean.Enquiry;
import com.realestate.bean.EnquirySource;
import com.realestate.bean.ExtraCharges;
import com.realestate.bean.Flat;
import com.realestate.bean.FlatWiseMarketPrice;
import com.realestate.bean.Floor;
import com.realestate.bean.LocationArea;
import com.realestate.bean.Login;
import com.realestate.bean.Occupation;
import com.realestate.bean.ParkingZone;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.State;
import com.realestate.bean.Tax;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.AgentRepository;
import com.realestate.repository.AggreementRepository;
import com.realestate.repository.BankRepository;
import com.realestate.repository.BookingCancelFormRepository;
import com.realestate.repository.BookingRepository;
import com.realestate.repository.BudgetRepository;
import com.realestate.repository.CountryRepository;
import com.realestate.repository.CustomerFlatCheckListRepository;
import com.realestate.repository.CustomerLoanDetailsRepository;
import com.realestate.repository.CustomerPaymentDetailsRepository;
import com.realestate.repository.DesignationRepository;
import com.realestate.repository.EnquiryRepository;
import com.realestate.repository.EnquirySourceRepository;
import com.realestate.repository.ExtraChargeRepository;
import com.realestate.repository.FlatRepository;
import com.realestate.repository.FloorRepository;
import com.realestate.repository.GeneratePaymentSchedulerRepository;
import com.realestate.repository.OccupationRepository;
import com.realestate.repository.ParkingZoneRepository;
import com.realestate.repository.PaymentSchedulerRepository;
import com.realestate.repository.ProjectBuildingRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.ProjectWingRepository;
import com.realestate.repository.TaxRepository;
import com.realestate.services.BookingDetailsExcelView;
import com.realestate.services.SmsServices;

@Controller
@RequestMapping("/")
public class BookingController 
{
	@Autowired 
	AggreementRepository aggreementRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	ProjectBuildingRepository projectbuildingRepository;
	@Autowired
	ProjectWingRepository wingRepository;

	@Autowired
	CountryRepository countryRepository;
	@Autowired
	BankRepository bankRepository;
	@Autowired
	TaxRepository taxRepository;
	@Autowired
	DesignationRepository designationRepository;
	@Autowired 
	OccupationRepository occupationRepository;
	@Autowired 
	BookingRepository bookingRepository;
	@Autowired
	FlatRepository flatRepository;
	@Autowired
	BudgetRepository budgetRepository;
	@Autowired
	EnquirySourceRepository enquirySourceRepository;
	@Autowired
	PaymentSchedulerRepository paymentSchedulerRepository;
	@Autowired
	EnquiryRepository enquiryRepository;
	@Autowired
	GeneratePaymentSchedulerRepository generatepaymentschedulerRepository;
	@Autowired 
	ExtraChargeRepository extrachargesRepository;
	@Autowired
	CustomerLoanDetailsRepository customerLoandetailsRepository;
	@Autowired
	BookingCancelFormRepository bookingcancelformRepository;
	@Autowired
	FloorRepository floorRepository;
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	CustomerPaymentDetailsRepository customerpaymentdetailsrepository;
	@Autowired
	AgentRepository agentRepository;
	@Autowired
	ParkingZoneRepository parkingZoneRepository;
	@Autowired
	CustomerFlatCheckListRepository customerflatchecklistRepository;

	int year = Calendar.getInstance().get(Calendar.YEAR);
	int month=Calendar.getInstance().get(Calendar.MONTH)+1;
	int y1,y2;


	String enquiryCode;
	private String EnquiryCode()
	{
		long enquiryCount = enquiryRepository.count();
		if(month>3) 
		{
			y1=year;
			y2=year+1;
			if(enquiryCount<10)
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/000"+(enquiryCount+1);
			}
			else if((enquiryCount>=10) && (enquiryCount<100))
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/00"+(enquiryCount+1);
			}
			else if((enquiryCount>=100) && (enquiryCount<1000))
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/0"+(enquiryCount+1);
			}
			else 
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/"+(enquiryCount+1);
			}
		}
		else
		{
			y1=year-1;
			y2=year;
			if(enquiryCount<10)
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/000"+(enquiryCount+1);
			}
			else if((enquiryCount>=10) && (enquiryCount<100))
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/00"+(enquiryCount+1);
			}
			else if((enquiryCount>=100) && (enquiryCount<1000))
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/0"+(enquiryCount+1);
			}
			else 
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/"+(enquiryCount+1);
			}
		}
		return enquiryCode;
	}


	String payAmountCode;
	private String customerpayAmountCode()
	{
		long Count = customerpaymentdetailsrepository.count();

		if(Count<10)
		{
			payAmountCode = "TP000"+(Count+1);
		}
		else if((Count>=10) && (Count<100))
		{
			payAmountCode = "TP00"+(Count+1);
		}
		else if((Count>=100) && (Count<1000))
		{
			payAmountCode = "TP0"+(Count+1);
		}
		else
		{
			payAmountCode = "TP"+(Count+1);
		}

		return payAmountCode;
	}

	//Code for budget limit generation 
	private List<Budget> getBudgetList()
	{
		List<Budget> budgetList;

		budgetList = budgetRepository.findAll(new Sort(Sort.Direction.ASC,"budgetCost"));

		return budgetList;
	}
	private List<Country> findAllCountryId()
	{
		List<Country> countryList;

		countryList = countryRepository.findAll(new Sort(Sort.Direction.ASC,"LocationareaId"));
		return countryList;
	}
	//code for retriving all occupation retriving
	private List<Occupation> getAllOccupationName()
	{
		List<Occupation> occupationList;

		occupationList = occupationRepository.findAll(new Sort(Sort.Direction.ASC,"occupationName"));
		return occupationList;
	}
	private List<Bank> getAllBankName()
	{
		List<Bank> bankList= new ArrayList<Bank>();
		Query query = new Query();
		bankList = bankRepository.findAll(new Sort(Sort.Direction.ASC,"bankName"));
		return bankList;
	}

	//code getting all enquiry sources
	private List<EnquirySource> getAllEnquirySourceName()
	{
		List<EnquirySource> enquirySourceList;

		enquirySourceList = enquirySourceRepository.findAll(new Sort(Sort.Direction.ASC,"enquirysourceName"));
		return enquirySourceList;
	}

	//code getting all Agent
	private List<Agent> AgentList()
	{
		List<Agent> agentList;

		agentList = agentRepository.findAll(new Sort(Sort.Direction.ASC,"agentName"));
		return agentList;
	}

	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	public List<Booking> GetAllBookingList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			Query query = new Query();
			List<Project> projectList = GetUserAssigenedProjectList(req,res); 
			List<Booking> bookingList1 = mongoTemplate.find(query.addCriteria(Criteria.where("bookingstatus").ne("Cancel")), Booking.class);


			List<Booking> bookingList=new ArrayList<Booking>();
			Booking booking=new Booking();

			for(int i=0;i<projectList.size();i++)
			{

				for(int j=0;j<bookingList1.size();j++)
				{
					if(projectList.get(i).getProjectId().equals(bookingList1.get(j).getProjectId()))
					{

						booking=new Booking();

						booking.setBookingId(bookingList1.get(j).getBookingId());
						booking.setEnquiryId(bookingList1.get(j).getEnquiryId());
						booking.setBookingfirstname(bookingList1.get(j).getBookingfirstname());
						booking.setBookingmiddlename(bookingList1.get(j).getBookingmiddlename());
						booking.setBookinglastname(bookingList1.get(j).getBookinglastname());
						booking.setBookingaddress(bookingList1.get(j).getBookingaddress());
						booking.setBookingPincode(bookingList1.get(j).getBookingPincode());

						booking.setBookingEmail(bookingList1.get(j).getBookingEmail());
						booking.setBookingmobileNumber1(bookingList1.get(j).getBookingmobileNumber1());
						booking.setBookingmobileNumber2(bookingList1.get(j).getBookingmobileNumber2());
						booking.setBookingOccupation(bookingList1.get(j).getBookingOccupation());
						booking.setPurposeOfFlat(bookingList1.get(j).getPurposeOfFlat());

						booking.setFloorId(bookingList1.get(j).getFloorId());
						booking.setFlatType(bookingList1.get(j).getFlatType());
						booking.setFlatId(bookingList1.get(j).getFlatId());
						booking.setFlatFacing(bookingList1.get(j).getFlatFacing());
						booking.setFlatareainSqFt(bookingList1.get(j).getFlatareainSqFt());
						booking.setFlatCostwithotfloorise(bookingList1.get(j).getFlatCostwithotfloorise());
						booking.setFloorRise(bookingList1.get(j).getFloorRise());
						booking.setFlatCost(bookingList1.get(j).getFlatCost());
						booking.setFlatbasicCost(bookingList1.get(j).getFlatbasicCost());
						booking.setParkingFloorId(bookingList1.get(j).getParkingFloorId());
						booking.setParkingZoneId(bookingList1.get(j).getParkingZoneId());
						booking.setAgentId(bookingList1.get(j).getAgentId());

						booking.setInfrastructureCharge(bookingList1.get(j).getInfrastructureCharge());
						booking.setAggreementValue1(bookingList1.get(j).getAggreementValue1());
						booking.setHandlingCharges(bookingList1.get(j).getHandlingCharges());
						booking.setStampDuty1(bookingList1.get(j).getStampDuty1());
						booking.setStampDutyPer(bookingList1.get(j).getStampDutyPer());
						booking.setRegistrationPer(bookingList1.get(j).getRegistrationPer());
						booking.setRegistrationCost1(bookingList1.get(j).getRegistrationCost1());
						booking.setGstCost(bookingList1.get(j).getGstCost());
						booking.setGstAmount1(bookingList1.get(j).getGstAmount1());
						booking.setGrandTotal1(bookingList1.get(j).getGrandTotal1());
						booking.setTds(bookingList1.get(j).getTds());
						booking.setBookingstatus(bookingList1.get(j).getBookingstatus());
						booking.setUserName(bookingList1.get(j).getUserName());
						//  booking.setCreationDate(bookingList1.get(j).getCreationDate());


						//booking.setProjectId(bookingList1.get(j).getProjectId());
						//booking.setBuildingId(bookingList1.get(j).getBuildingId());
						//booking.setWingId(bookingList1.get(j).getWingId());


						try {


							query = new Query();
							Project project=mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(bookingList1.get(j).getProjectId())), Project.class);
							booking.setProjectId(project.getProjectName());

							query = new Query();
							ProjectBuilding projectBuilding=mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(bookingList1.get(j).getBuildingId())),ProjectBuilding.class);
							booking.setBuildingId(projectBuilding.getBuildingName());
							query = new Query();
							ProjectWing projectWing=mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(bookingList1.get(j).getWingId())), ProjectWing.class);
							booking.setWingId(projectWing.getWingName());

							query = new Query();
							Flat flat=mongoTemplate.findOne(query.addCriteria(Criteria.where("flatId").is(bookingList1.get(j).getFlatId())), Flat.class);
							booking.setFlatNumber(flat.getFlatNumber());
						}catch (Exception e) {
							// TODO: handle exception
						}
						bookingList.add(booking);

					}

				}

			}


			return bookingList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	public List<Aggreement> GetAllAggreementList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<Booking> bookingList = GetAllBookingList(req,res); 

			List<Aggreement> aggreementList1 = aggreementRepository.findAll();

			List<Aggreement> aggreementList=new ArrayList<Aggreement>();
			Aggreement aggreement=new Aggreement();

			for(int i=0;i<bookingList.size();i++)
			{
				for(int j=0;j<aggreementList1.size();j++)
				{
					if(bookingList.get(i).getBookingId().equals(aggreementList1.get(j).getBookingId()))
					{
						aggreement=new Aggreement();
						aggreement.setAggreementId(aggreementList1.get(j).getAggreementId());
						aggreement.setBookingId(aggreementList1.get(j).getBookingId());
						aggreement.setFirstApplicantfirstname(aggreementList1.get(j).getFirstApplicantfirstname());
						aggreement.setFirstApplicantmaidenmiddlename(aggreementList1.get(j).getFirstApplicantmiddlename());
						aggreement.setFirstApplicantlastname(aggreementList1.get(j).getFirstApplicantlastname());
						aggreement.setAggreementDate(aggreementList1.get(j).getAggreementDate());
						aggreement.setAggreementNumber(aggreementList1.get(j).getAggreementNumber());

						aggreementList.add(aggreement);
					}

				}

			}
			return aggreementList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@ResponseBody
	@RequestMapping(value="/getGstCostList",method=RequestMethod.POST)
	public List<Tax> getGstCostList(HttpSession session)
	{
		List<Tax> taxList=new ArrayList<Tax>();
		Query query1 = new Query();
		taxList = mongoTemplate.find(query1.addCriteria(Criteria.where("taxType").is("BOOKING SALE")), Tax.class);
		return taxList;
	}

	@ResponseBody
	@RequestMapping(value="/getParkingFloor",method=RequestMethod.POST)
	private List<Floor> ParkingFloors(@RequestParam("projectId") String projectId,@RequestParam("buildingId") String buildingId, @RequestParam("wingId") String wingId,  HttpSession session)
	{
		List<Floor> parkingFloorList=new ArrayList<Floor>();

		Query query = new Query();
		parkingFloorList = mongoTemplate.find(query.addCriteria(Criteria.where("floorzoneType").is("Parking").and("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId)), Floor.class);

		return parkingFloorList;
	}


	@RequestMapping("/AllBookingCancelList")
	public String AllBookingCancelList(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{	
		try {
			Query query = new Query();
			List<Project> projectList = GetUserAssigenedProjectList(req,res); 
			List<Booking> bookingList1 = mongoTemplate.find(query.addCriteria(Criteria.where("bookingstatus").is("Cancel")), Booking.class);

			List<BookingCancelForm> cancelList= new ArrayList<BookingCancelForm>();
			BookingCancelForm bookingcancelForm=new BookingCancelForm();
			List<Booking> bookingList=new ArrayList<Booking>();
			Booking booking=new Booking();

			for(int i=0;i<projectList.size();i++)
			{

				for(int j=0;j<bookingList1.size();j++)
				{
					if(projectList.get(i).getProjectId().equals(bookingList1.get(j).getProjectId()))
					{
						try {
							query = new Query();
							BookingCancelForm cancle = mongoTemplate.findOne(query.addCriteria(Criteria.where("bookingId").is(bookingList1.get(j).getBookingId())), BookingCancelForm.class);
							bookingcancelForm=new BookingCancelForm();
							if(cancle!=null)
							{
								bookingcancelForm.setBookingId(cancle.getBookingId());
								bookingcancelForm.setCustomerName(cancle.getCustomerName());
								bookingcancelForm.setCustomerMobile(cancle.getCustomerMobile());
								bookingcancelForm.setCustomerEmail(cancle.getCustomerEmail());
								bookingcancelForm.setBookingDate(cancle.getBookingDate());
								bookingcancelForm.setCancelDate(cancle.getCancelDate());
								bookingcancelForm.setAggreementValue1(cancle.getAggreementValue1());
								bookingcancelForm.setCancelCharge(cancle.getCancelCharge());
							}
							cancelList.add(bookingcancelForm);
						}catch (Exception e) {
							e.printStackTrace();
							// TODO: handle exception
						}

					}

				}

			}

			model.addAttribute("cancelList",cancelList);
			return "AllBookingCancelList";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping("/AddCustomerPayment")
	public String AddCustomerPayment(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{	
		try {
			List<Booking> bookingList =GetAllBookingList(req,res);

			model.addAttribute("bookingList", bookingList);
			return "AddCustomerPayment";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping("/AllAgreementList")
	public String AllAgreementList(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{	
		try {
			List<CustomerFlatCheckList> CustomercheckList = customerflatchecklistRepository.findAll();

			List<Aggreement> aggreementList = GetAllAggreementList(req,res); 

			model.addAttribute("CustomercheckList",CustomercheckList);
			model.addAttribute("aggreementList",aggreementList);
			return "AllAgreementList";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping("/AllCustomerExtraChargesList")
	public String AllCustomerExtraChargesList(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{	
		try {
			int i=0,j=0;
			List<Booking> bookingList =GetAllBookingList(req,res);

			List<ExtraCharges> extrachargesList1 = extrachargesRepository.findAll(); 

			List<ExtraCharges> extrachargesList=new ArrayList<ExtraCharges>();
			ExtraCharges extracharges=new ExtraCharges();

			for(i=0;i<bookingList.size();i++)
			{
				for(j=0;j<extrachargesList1.size();j++)
				{
					if(bookingList.get(i).getBookingId().equals(extrachargesList1.get(j).getBookingId()))
					{
						extracharges=new ExtraCharges();
						extracharges.setChargesId(extrachargesList1.get(j).getChargesId());
						extracharges.setBookingId(extrachargesList1.get(j).getBookingId());
						extracharges.setBookingDate(extrachargesList1.get(j).getBookingDate());
						extracharges.setCustomerName(extrachargesList1.get(j).getCustomerName());
						extracharges.setAggreementValue1(extrachargesList1.get(j).getAggreementValue1());
						extracharges.setMaintenanceCost(extrachargesList1.get(j).getMaintenanceCost());
						extracharges.setOtherCharges(extrachargesList1.get(j).getOtherCharges());
						extracharges.setCreationDate(extrachargesList1.get(j).getCreationDate());
						extracharges.setUpdateDate(extrachargesList1.get(j).getUpdateDate());
						extracharges.setUserName(extrachargesList1.get(j).getUserName());

						extrachargesList.add(extracharges);

					}

				}
			}

			model.addAttribute("extrachargesList",extrachargesList);
			return "AllCustomerExtraChargesList";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/AllCustomerLoanList")
	public String AllCustomerLoanList(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{	
		try {
			List<Booking> bookingList =GetAllBookingList(req,res);

			List<CustomerLoanDetails> loanList1 = customerLoandetailsRepository.findAll();

			List<CustomerLoanDetails> loanList=new ArrayList<CustomerLoanDetails>();
			CustomerLoanDetails customerloanDetails=new CustomerLoanDetails();

			for(int i=0;i<bookingList.size();i++)
			{
				for(int j=0;j<loanList1.size();j++)
				{
					if(bookingList.get(i).getBookingId().equals(loanList1.get(j).getBookingId()))
					{
						customerloanDetails=new CustomerLoanDetails();
						customerloanDetails.setCustomerloanId(loanList1.get(j).getCustomerloanId());
						customerloanDetails.setBookingId(loanList1.get(j).getBookingId());
						customerloanDetails.setCustomerName(loanList1.get(j).getCustomerName());
						customerloanDetails.setTotalAmount(loanList1.get(j).getTotalAmount());
						customerloanDetails.setLoanAmount(loanList1.get(j).getLoanAmount());
						customerloanDetails.setRemainingAmount(loanList1.get(j).getRemainingAmount());
						customerloanDetails.setBankName(loanList1.get(j).getBankName());
						customerloanDetails.setFileNumber(loanList1.get(j).getFileNumber());
						customerloanDetails.setBankPersonName(loanList1.get(j).getBankPersonName());
						customerloanDetails.setBankPersonEmailId(loanList1.get(j).getBankPersonEmailId());
						customerloanDetails.setBankPersonMobNo(loanList1.get(j).getBankPersonMobNo());
						customerloanDetails.setAgainstAggreement(loanList1.get(j).getAgainstAggreement());
						customerloanDetails.setAgainstGST(loanList1.get(j).getAgainstGST());
						customerloanDetails.setAgainstRegistration(loanList1.get(j).getAgainstRegistration());
						customerloanDetails.setAgainstStampDuty(loanList1.get(j).getAgainstStampDuty());
						loanList.add(customerloanDetails);
					}

				}

			}

			model.addAttribute("loanList",loanList);
			return "AllCustomerLoanList";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping("/BookingMaster")
	public String BooingMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{	
		try {
			Query query = new Query();
			List<Project> projectList = GetUserAssigenedProjectList(req,res); 
			List<Booking> bookingList =GetAllBookingList(req,res);

			List<Aggreement> aggreementList = GetAllAggreementList(req,res);
			List<ExtraCharges> extrachargesList = extrachargesRepository.findAll(); 
			List<CustomerLoanDetails> loanList = customerLoandetailsRepository.findAll();

			List<CustomerFlatCheckList> CustomercheckList = customerflatchecklistRepository.findAll();

			query = new Query();
			List<Enquiry> enquiryList = mongoTemplate.find(query.addCriteria(Criteria.where("enqStatus").is("Booking")), Enquiry.class);

			model.addAttribute("CustomercheckList",CustomercheckList);
			model.addAttribute("aggreementList",aggreementList);
			model.addAttribute("loanList",loanList);
			model.addAttribute("extrachargesList",extrachargesList);
			model.addAttribute("bookingList", bookingList);
			model.addAttribute("projectList",projectList);
			model.addAttribute("enquiryList", enquiryList);
			return "BookingMaster";

		}catch (Exception e) {
			return "login";
		}
	}



	@RequestMapping("/AddBooking")
	public String Enquirybooking(@RequestParam("enquiryId") String enquiryId, Model model, HttpServletRequest req,HttpServletResponse res)
	{
		try {
			List<Enquiry> enquiryDetails;

			Query query = new Query();
			enquiryDetails = mongoTemplate.find(query.addCriteria(Criteria.where("enquiryId").is(enquiryId)), Enquiry.class);

			List<Project> projectList = GetUserAssigenedProjectList(req,res); 

			model.addAttribute("projectList",projectList);
			model.addAttribute("enquiryDetails",enquiryDetails);
			model.addAttribute("occupationList",getAllOccupationName());
			model.addAttribute("agentList",AgentList()); 
			model.addAttribute("bankList",getAllBankName());

			return "AddBooking";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/AddNewBooking")
	public String Newbooking(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<Project> projectList = GetUserAssigenedProjectList(req,res); 

			model.addAttribute("projectList",projectList);
			model.addAttribute("enquiryCode",EnquiryCode());
			model.addAttribute("occupationList",getAllOccupationName());
			model.addAttribute("bankList",getAllBankName());
			model.addAttribute("enquirySourceList", getAllEnquirySourceName());
			model.addAttribute("agentList",AgentList());
			model.addAttribute("budgetList",getBudgetList());

			return "AddNewBooking";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/AddBooking", method = RequestMethod.POST)
	public String bookingSave(Model model, @ModelAttribute Booking booking, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try 
			{
				CustomerPaymentDetails customerpaymentdetails=new CustomerPaymentDetails();
				customerpaymentdetails.setId(customerpayAmountCode());
				customerpaymentdetails.setBookingId(booking.getBookingId());
				customerpaymentdetails.setTotalpayAggreement(0.0);
				customerpaymentdetails.setTotalpayStampDuty(0.0);
				customerpaymentdetails.setTotalpayRegistration(0.0);
				customerpaymentdetails.setTotalpaygstAmount(0.0);
				customerpaymentdetails.setAmountStatus("pending");
				customerpaymentdetails.getCreationDate();
				customerpaymentdetails.getUpdateDate();
				customerpaymentdetails.getUserName();
				customerpaymentdetailsrepository.save(customerpaymentdetails);
			}
			catch(Exception es)
			{

			}

			Flat flat;
			Enquiry enquiry;

			try
			{
				Booking booking1=new Booking();
				booking1.setBookingId(booking.getBookingId());
				booking1.setEnquiryId(booking.getEnquiryId());

				booking1.setBookingfirstname(booking.getBookingfirstname().toUpperCase());
				//booking1.setBookingmiddlename(booking.getBookingmiddlename().toUpperCase());
				//booking1.setBookinglastname(booking.getBookinglastname().toUpperCase());

				booking1.setBookingaddress(booking.getBookingaddress());
				booking1.setBookingPincode(booking.getBookingPincode());

				booking1.setBookingEmail(booking.getBookingEmail());
				booking1.setBookingmobileNumber1(booking.getBookingmobileNumber1());
				booking1.setBookingmobileNumber2(booking.getBookingmobileNumber2());
				booking1.setBookingOccupation(booking.getBookingOccupation());
				booking1.setPurposeOfFlat(booking.getPurposeOfFlat());

				booking1.setProjectId(booking.getProjectId());
				booking1.setBuildingId(booking.getBuildingId());
				booking1.setWingId(booking.getWingId());
				booking1.setFloorId(booking.getFloorId());
				booking1.setFlatType(booking.getFlatType());
				booking1.setFlatId(booking.getFlatId());
				booking1.setFlatFacing(booking.getFlatFacing());
				booking1.setFlatareainSqFt(booking.getFlatareainSqFt());
				booking1.setFlatCostwithotfloorise(booking.getFlatCostwithotfloorise());
				booking1.setFloorRise(booking.getFloorRise());
				booking1.setFlatCost(booking.getFlatCost());
				booking1.setFlatbasicCost(booking.getFlatbasicCost());
				booking1.setParkingFloorId(booking.getParkingFloorId());
				booking1.setParkingZoneId(booking.getParkingZoneId());
				booking1.setAgentId(booking.getAgentId());
				booking1.setInfrastructureCharge(booking.getInfrastructureCharge());
				booking1.setAggreementValue1(booking.getAggreementValue1());
				booking1.setHandlingCharges(booking.getHandlingCharges());
				booking1.setStampDutyPer(booking.getStampDutyPer());
				booking1.setRegistrationPer(booking.getRegistrationPer());
				booking1.setStampDuty1(booking.getStampDuty1());
				booking1.setRegistrationCost1(booking.getRegistrationCost1());
				booking1.setGstCost(booking.getGstCost());
				booking1.setGstAmount1(booking.getGstAmount1());
				booking1.setGrandTotal1(booking.getGrandTotal1());
				booking1.setTds(booking.getTds());
				booking1.setBookingstatus(booking.getBookingstatus());
				booking1.setCreationDate(new Date());
				booking1.setUpdateDate(new Date());
				booking1.setUserName(booking.getUserName());

				bookingRepository.save(booking1);


				flat = mongoTemplate.findOne(Query.query(Criteria.where("flatId").is(booking.getFlatId())), Flat.class);
				flat.setFlatstatus("Booking Completed");
				flatRepository.save(flat);


				try
				{
					enquiry = mongoTemplate.findOne(Query.query(Criteria.where("enquiryId").is(booking.getEnquiryId())), Enquiry.class);
					enquiry.setEnqStatus("Booking Completed");
					enquiryRepository.save(enquiry);
				}
				catch(Exception ee)
				{
ee.printStackTrace();
				}

				try
				{   	
					ParkingZone parkingzone=new ParkingZone();
					parkingzone = mongoTemplate.findOne(Query.query(Criteria.where("parkingZoneId").is(booking.getParkingZoneId())), ParkingZone.class);
					parkingzone.setParkingZoneStatus("Assigned");
					parkingZoneRepository.save(parkingzone);
				}
				catch(Exception ee)
				{

				}

			}
			catch(DuplicateKeyException de)
			{
				//model.addAttribute("bankStatus","Fail");
			}

			Query query = new Query();
			List<Project> projectList = GetUserAssigenedProjectList(req,res); 
			List<Booking> bookingList =GetAllBookingList(req,res);

			List<Aggreement> aggreementList = GetAllAggreementList(req,res);
			List<ExtraCharges> extrachargesList = extrachargesRepository.findAll(); 
			List<CustomerLoanDetails> loanList = customerLoandetailsRepository.findAll();
			List<CustomerFlatCheckList> CustomercheckList = customerflatchecklistRepository.findAll();

			query = new Query();
			List<Enquiry> enquiryList = mongoTemplate.find(query.addCriteria(Criteria.where("enqStatus").is("Booking")), Enquiry.class);

			model.addAttribute("CustomercheckList",CustomercheckList);
			model.addAttribute("aggreementList",aggreementList);
			model.addAttribute("loanList",loanList);
			model.addAttribute("extrachargesList",extrachargesList);
			model.addAttribute("bookingList", bookingList);
			model.addAttribute("projectList",projectList);
			model.addAttribute("enquiryList", enquiryList);
			return "BookingMaster";

		}catch (Exception e) {
			e.printStackTrace();
			return "login";
		}
	}

	@RequestMapping(value="/AddNewBooking", method = RequestMethod.POST)
	public String NewbookingSave(Model model, @ModelAttribute Booking booking,  @ModelAttribute Enquiry enquiry, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try {

				CustomerPaymentDetails customerpaymentdetails=new CustomerPaymentDetails();
				customerpaymentdetails.setId(customerpayAmountCode());
				customerpaymentdetails.setBookingId(booking.getBookingId());
				customerpaymentdetails.setTotalpayAggreement(0.0);
				customerpaymentdetails.setTotalpayStampDuty(0.0);
				customerpaymentdetails.setTotalpayRegistration(0.0);
				customerpaymentdetails.setTotalpaygstAmount(0.0);
				customerpaymentdetails.setAmountStatus("pending");
				customerpaymentdetails.getCreationDate();
				customerpaymentdetails.getUpdateDate();
				customerpaymentdetails.getUserName();
				customerpaymentdetailsrepository.save(customerpaymentdetails);
			}
			catch(Exception es)
			{

			}



			Flat flat;

			try
			{
				Booking booking1=new Booking();
				booking1.setBookingId(booking.getBookingId());
				booking1.setEnquiryId(booking.getEnquiryId());

				booking1.setBookingfirstname(booking.getBookingfirstname());
				booking1.setBookingmiddlename(booking.getBookingmiddlename());
				booking1.setBookinglastname(booking.getBookinglastname());

				booking1.setBookingaddress(booking.getBookingaddress());
				booking1.setBookingPincode(booking.getBookingPincode());

				booking1.setBookingEmail(booking.getBookingEmail());
				booking1.setBookingmobileNumber1(booking.getBookingmobileNumber1());
				booking1.setBookingmobileNumber2(booking.getBookingmobileNumber2());
				booking1.setBookingOccupation(booking.getBookingOccupation());
				booking1.setPurposeOfFlat(booking.getPurposeOfFlat());

				booking1.setProjectId(booking.getProjectId());
				booking1.setBuildingId(booking.getBuildingId());
				booking1.setWingId(booking.getWingId());
				booking1.setFloorId(booking.getFloorId());
				booking1.setFlatType(booking.getFlatType());
				booking1.setFlatId(booking.getFlatId());
				booking1.setFlatFacing(booking.getFlatFacing());
				booking1.setFlatareainSqFt(booking.getFlatareainSqFt());
				booking1.setFlatCostwithotfloorise(booking.getFlatCostwithotfloorise());
				booking1.setFloorRise(booking.getFloorRise());
				booking1.setFlatCost(booking.getFlatCost());
				booking1.setFlatbasicCost(booking.getFlatbasicCost());
				booking1.setParkingFloorId(booking.getParkingFloorId());
				booking1.setParkingZoneId(booking.getParkingZoneId());
				booking1.setAgentId(booking.getAgentId());
				booking1.setInfrastructureCharge(booking.getInfrastructureCharge());
				booking1.setAggreementValue1(booking.getAggreementValue1());
				booking1.setHandlingCharges(booking.getHandlingCharges());
				booking1.setStampDutyPer(booking.getStampDutyPer());
				booking1.setRegistrationPer(booking.getRegistrationPer());
				booking1.setStampDuty1(booking.getStampDuty1());
				booking1.setRegistrationCost1(booking.getRegistrationCost1());
				booking1.setGstCost(booking.getGstCost());
				booking1.setGstAmount1(booking.getGstAmount1());
				booking1.setGrandTotal1(booking.getGrandTotal1());
				booking1.setTds(booking.getTds());
				booking1.setBookingstatus(booking.getBookingstatus());
				booking1.setCreationDate(new Date());
				booking1.setUpdateDate(new Date());
				booking1.setUserName(booking.getUserName());

				bookingRepository.save(booking1);


				// for add enquiry
				try {

					Query query = new Query();
					Project projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(booking.getProjectId())), Project.class);

					enquiry.setEnquiryId(booking.getEnquiryId());
					enquiry.setEnqfirstName(booking.getBookingfirstname());
					enquiry.setEnqmiddleName(booking.getBookingmiddlename());
					enquiry.setEnqlastName(booking.getBookinglastname());
					enquiry.setEnqEmail(booking.getBookingEmail());
					enquiry.setEnqmobileNumber1(booking.getBookingmobileNumber1());
					enquiry.setEnqmobileNumber2(booking.getBookingmobileNumber2());
					enquiry.setProjectName(projectDetails.getProjectName());
					enquiry.setEnqOccupation(booking.getBookingOccupation());

					enquiry.getFlatType();
					enquiry.getFlatBudget();
					enquiry.getEnquirysourceId();
					enquiry.getSubsourceId();
					enquiry.getFollowupDate();
					enquiry.setEnqStatus("Booking completed");
					enquiry.setEnqRemark("Booking completed");
					enquiry.setCreationDate(new Date());
					enquiry.setUpdateDate(new Date());
					enquiry.getUserName();
					enquiryRepository.save(enquiry);	
				}
				catch(Exception ee)
				{}


				flat = mongoTemplate.findOne(Query.query(Criteria.where("flatId").is(booking.getFlatId())), Flat.class);
				flat.setFlatstatus("Booking Completed");
				flatRepository.save(flat);

				try {   	

					ParkingZone parkingzone=new ParkingZone();
					parkingzone = mongoTemplate.findOne(Query.query(Criteria.where("parkingZoneId").is(booking.getParkingZoneId())), ParkingZone.class);
					parkingzone.setParkingZoneStatus("Assigned");
					parkingZoneRepository.save(parkingzone);
				}
				catch(Exception ee)
				{
				}

				// model.addAttribute("bankStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				//model.addAttribute("bankStatus","Fail");
			}


			Query query = new Query();
			// for Send SMS to customer
			try {
				Date date = new Date();  
				SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy"); 
				formatter = new SimpleDateFormat("dd MMMM yyyy");  
				String strDate = formatter.format(date);  

				query = new Query();
				Project projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(booking.getProjectId())), Project.class);

				query = new Query();
				ProjectBuilding buildingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(booking.getBuildingId())), ProjectBuilding.class);

				query = new Query();
				ProjectWing wingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(booking.getWingId())), ProjectWing.class);

				query = new Query();
				Floor floorDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("floorId").is(booking.getFloorId())), Floor.class);

				query = new Query();
				Flat flatDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("flatId").is(booking.getFlatId())), Flat.class);
				String sms="";
				sms=" Dear "+booking.getBookingfirstname()+", Project "+projectDetails.getProjectName()+" flat no."+flatDetails.getFlatNumber()+" has been booked for you on "+strDate+". ";
				SmsServices.sendSMS(booking.getBookingmobileNumber1(),sms);
			}
			catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}



			query = new Query();
			List<Project> projectList = GetUserAssigenedProjectList(req,res); 
			List<Booking> bookingList =GetAllBookingList(req,res);

			List<Aggreement> aggreementList = GetAllAggreementList(req,res);
			List<ExtraCharges> extrachargesList = extrachargesRepository.findAll(); 
			List<CustomerLoanDetails> loanList = customerLoandetailsRepository.findAll();
			List<CustomerFlatCheckList> CustomercheckList = customerflatchecklistRepository.findAll();

			query = new Query();
			List<Enquiry> enquiryList = mongoTemplate.find(query.addCriteria(Criteria.where("enqStatus").is("Booking")), Enquiry.class);

			model.addAttribute("CustomercheckList",CustomercheckList);
			model.addAttribute("aggreementList",aggreementList);
			model.addAttribute("loanList",loanList);
			model.addAttribute("extrachargesList",extrachargesList);
			model.addAttribute("bookingList", bookingList);
			model.addAttribute("projectList",projectList);
			model.addAttribute("enquiryList", enquiryList);
			return "BookingMaster";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping("/AllBookingList")
	public String AllBooingList(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{		


		List<Project> projectList = GetUserAssigenedProjectList(req,res); 
		List<Booking> bookingList =GetAllBookingList(req,res);

		model.addAttribute("projectList",projectList);
		model.addAttribute("bookingList", bookingList);

		return "AllBookingList";
	}


	@ResponseBody
	@RequestMapping(value="/getProjectWisebookingremaingList",method=RequestMethod.POST)
	public List<Enquiry> ProjectWisebookingremaingList(@RequestParam("projectId") String projectId, HttpSession session)
	{
		List<Enquiry> enquiryList= new ArrayList<Enquiry>();

		Query query = new Query();
		enquiryList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("enqStatus").is("Booking")), Enquiry.class);

		return enquiryList;
	} 



	@ResponseBody
	@RequestMapping(value="/getProjectWiseBookingList",method=RequestMethod.POST)
	public List<Booking> ProjectWiseBookingList(@RequestParam("projectId") String projectId, HttpSession session)
	{
		List<Booking> bookingList= new ArrayList<Booking>();

		Query query = new Query();
		bookingList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)), Booking.class);

		return bookingList;
	} 



	@ResponseBody
	@RequestMapping(value="/getBuildingWiseBookingList",method=RequestMethod.POST)
	public List<Enquiry> BuildingWiseBookingList(@RequestParam("projectId") String projectId, @RequestParam("buildingId") String buildingId, HttpSession session)
	{
		List<Enquiry> enquiryList= new ArrayList<Enquiry>();

		Query query = new Query();
		enquiryList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("enqStatus").is("Booking")), Enquiry.class);

		return enquiryList;
	} 

	@ResponseBody
	@RequestMapping(value="/getWingWiseBookingList",method=RequestMethod.POST)
	public List<Enquiry> WingWiseBookingList(@RequestParam("projectId") String projectId, @RequestParam("buildingId") String buildingId, @RequestParam("wingId") String wingId, HttpSession session)
	{
		List<Enquiry> enquiryList= new ArrayList<Enquiry>();

		Query query = new Query();
		enquiryList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("enqStatus").is("Booking")), Enquiry.class);

		return enquiryList;
	} 

	@ResponseBody
	@RequestMapping(value="/getFloorWiseAllBookingList",method=RequestMethod.POST)
	public List<Enquiry> FloorWiseAllBookingList(@RequestParam("projectId") String projectId, @RequestParam("buildingId") String buildingId, @RequestParam("wingId") String wingId,  @RequestParam("floortypeName") String floortypeName, HttpSession session)
	{
		List<Enquiry> enquiryList= new ArrayList<Enquiry>();

		Query query = new Query();
		enquiryList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("floortypeName").is(floortypeName).and("enqStatus").is("Booking")), Enquiry.class);

		return enquiryList;
	} 


	@RequestMapping("EditBooking")
	public String EditBooking(@RequestParam("bookingId") String bookingId, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<Project> projectList = GetUserAssigenedProjectList(req,res); 
			Query query = new Query();
			List<Booking> bookingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)),Booking.class);


			try
			{
				query = new Query();
				List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(bookingDetails.get(0).getProjectId())), Project.class);

				query = new Query();
				List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(bookingDetails.get(0).getBuildingId())), ProjectBuilding.class);

				query = new Query();
				List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(bookingDetails.get(0).getWingId())), ProjectWing.class);

				query = new Query();
				List<Floor> floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("floorId").is(bookingDetails.get(0).getFloorId())), Floor.class);

				query = new Query();
				List<Flat> flatDetails = mongoTemplate.find(query.addCriteria(Criteria.where("flatId").is(bookingDetails.get(0).getFlatId())), Flat.class);

				String parkingfloortypeName="None";
				String parkingNumber="None";
				String agentName="None";

				query = new Query();
				List<Floor> parkingFloorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("floorId").is(bookingDetails.get(0).getParkingFloorId())), Floor.class);
				if(parkingFloorDetails.size()!=0)
				{
					parkingfloortypeName=parkingFloorDetails.get(0).getFloortypeName();
				}

				query = new Query();
				List<ParkingZone> parkingZoneDetails = mongoTemplate.find(query.addCriteria(Criteria.where("parkingZoneId").is(bookingDetails.get(0).getParkingZoneId())), ParkingZone.class);
				if(parkingZoneDetails.size()!=0)
				{
					parkingNumber=parkingZoneDetails.get(0).getParkingNumber();
				}

				query = new Query();
				List<Agent> agentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("agentId").is(bookingDetails.get(0).getAgentId())), Agent.class);
				if(agentDetails.size()!=0)
				{
					agentName=agentDetails.get(0).getAgentfirmName();
				}

				try
				{
					List<Floor> parkingFloorList=new ArrayList<Floor>();

					query = new Query();
					parkingFloorList = mongoTemplate.find(query.addCriteria(Criteria.where("floorzoneType").is("Parking").and("wingId").is(bookingDetails.get(0).getWingId())), Floor.class);
					model.addAttribute("parkingFloorList", parkingFloorList);

				}
				catch (Exception e) {
					// TODO: handle exception
				}


				model.addAttribute("agentList",AgentList());
				model.addAttribute("projectName",projectDetails.get(0).getProjectName());
				model.addAttribute("buildingName",buildingDetails.get(0).getBuildingName());
				model.addAttribute("wingName",wingDetails.get(0).getWingName());
				model.addAttribute("floortypeName",floorDetails.get(0).getFloortypeName());
				model.addAttribute("flatNumber",flatDetails.get(0).getFlatNumber());
				model.addAttribute("parkingfloortypeName",parkingfloortypeName);
				model.addAttribute("parkingNumber",parkingNumber);
				model.addAttribute("agentName", agentName);
				model.addAttribute("flatDetails", flatDetails);
			}
			catch (Exception e) {
			}

			model.addAttribute("bookingDetails", bookingDetails);

			model.addAttribute("occupationList",getAllOccupationName());
			model.addAttribute("projectList",projectList); 
			model.addAttribute("bankList",getAllBankName());
			return "EditBooking";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/EditBooking", method = RequestMethod.POST)
	public String bookingEdit(Model model, @ModelAttribute Booking booking, @RequestParam("priviousflatId") String priviousflatId, @RequestParam("priviousparkingZoneId") String priviousparkingZoneId, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			Query query1=new Query();
			Booking booking2=mongoTemplate.findOne(query1.addCriteria(Criteria.where("bookingId").is(booking.getBookingId())), Booking.class);
			Flat flat;
			ParkingZone parkingzone;
			try
			{
				Booking booking1=new Booking();
				booking1.setBookingId(booking.getBookingId());
				booking1.setEnquiryId(booking.getEnquiryId());

				booking1.setBookingfirstname(booking.getBookingfirstname());
				booking1.setBookingmiddlename(booking.getBookingmiddlename());
				booking1.setBookinglastname(booking.getBookinglastname());

				booking1.setBookingaddress(booking.getBookingaddress());
				booking1.setBookingPincode(booking.getBookingPincode());

				booking1.setBookingEmail(booking.getBookingEmail());
				booking1.setBookingmobileNumber1(booking.getBookingmobileNumber1());
				booking1.setBookingmobileNumber2(booking.getBookingmobileNumber2());
				booking1.setBookingOccupation(booking.getBookingOccupation());
				booking1.setPurposeOfFlat(booking.getPurposeOfFlat());

				booking1.setProjectId(booking.getProjectId());
				booking1.setBuildingId(booking.getBuildingId());
				booking1.setWingId(booking.getWingId());
				booking1.setFloorId(booking.getFloorId());
				booking1.setFlatType(booking.getFlatType());
				booking1.setFlatId(booking.getFlatId());
				booking1.setFlatFacing(booking.getFlatFacing());
				booking1.setFlatareainSqFt(booking.getFlatareainSqFt());
				booking1.setFlatCostwithotfloorise(booking.getFlatCostwithotfloorise());
				booking1.setFloorRise(booking.getFloorRise());
				booking1.setFlatCost(booking.getFlatCost());
				booking1.setFlatbasicCost(booking.getFlatbasicCost());
				booking1.setParkingFloorId(booking.getParkingFloorId());
				booking1.setParkingZoneId(booking.getParkingZoneId());
				booking1.setAgentId(booking.getAgentId());
				booking1.setInfrastructureCharge(booking.getInfrastructureCharge());
				booking1.setAggreementValue1(booking.getAggreementValue1());
				booking1.setHandlingCharges(booking.getHandlingCharges());
				booking1.setStampDutyPer(booking.getStampDutyPer());
				booking1.setRegistrationPer(booking.getRegistrationPer());
				booking1.setStampDuty1(booking.getStampDuty1());
				booking1.setRegistrationCost1(booking.getRegistrationCost1());
				booking1.setGstCost(booking.getGstCost());
				booking1.setGstAmount1(booking.getGstAmount1());
				booking1.setGrandTotal1(booking.getGrandTotal1());
				booking1.setTds(booking.getTds());
				booking1.setBookingstatus(booking.getBookingstatus());
				booking1.setCreationDate(booking2.getCreationDate());
				booking1.setUpdateDate(new Date());
				booking1.setUserName(booking.getUserName());

				bookingRepository.save(booking1);


				try {
					if(priviousflatId.equals(booking.getFlatId()))
					{   }
					else
					{
						try {
							flat = mongoTemplate.findOne(Query.query(Criteria.where("flatId").is(priviousflatId)), Flat.class);
							flat.setFlatstatus("Booking Remaninig");
							flatRepository.save(flat);   
						}
						catch (Exception e) {
						}
					}
					if(priviousparkingZoneId.equals(booking.getParkingZoneId()))
					{ }
					else
					{
						try
						{
							parkingzone = mongoTemplate.findOne(Query.query(Criteria.where("parkingZoneId").is(priviousparkingZoneId)), ParkingZone.class);
							parkingzone.setParkingZoneStatus("Not Assigned");
							parkingZoneRepository.save(parkingzone);
						}
						catch (Exception e) {
						}

					}
					try {
						parkingzone = mongoTemplate.findOne(Query.query(Criteria.where("parkingZoneId").is(booking.getParkingZoneId())), ParkingZone.class);
						parkingzone.setParkingZoneStatus("Assigned");
						parkingZoneRepository.save(parkingzone);
					}
					catch (Exception e) {
					}
					flat = mongoTemplate.findOne(Query.query(Criteria.where("flatId").is(booking.getFlatId())), Flat.class);
					flat.setFlatstatus("Booking Completed");
					flatRepository.save(flat);

				}
				catch (Exception e) {

				}

			}
			catch(DuplicateKeyException de)
			{
				//model.addAttribute("bankStatus","Fail");
			}

			Query query = new Query();
			List<Project> projectList = GetUserAssigenedProjectList(req,res); 
			List<Booking> bookingList =GetAllBookingList(req,res);
			List<Aggreement> aggreementList = GetAllAggreementList(req,res);

			List<ExtraCharges> extrachargesList = extrachargesRepository.findAll(); 
			List<CustomerLoanDetails> loanList = customerLoandetailsRepository.findAll();
			List<CustomerFlatCheckList> CustomercheckList = customerflatchecklistRepository.findAll();
			query = new Query();
			List<Enquiry> enquiryList = mongoTemplate.find(query.addCriteria(Criteria.where("enqStatus").is("Booking")), Enquiry.class);

			model.addAttribute("CustomercheckList",CustomercheckList);
			model.addAttribute("aggreementList",aggreementList);
			model.addAttribute("loanList",loanList);
			model.addAttribute("extrachargesList",extrachargesList);
			model.addAttribute("bookingList", bookingList);
			model.addAttribute("projectList",projectList);
			model.addAttribute("enquiryList", enquiryList);
			return "BookingMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping(value="/getParkingNumber",method=RequestMethod.POST)
	public List<ParkingZone> getParkingNumber(@RequestParam("projectId") String projectId, @RequestParam("buildingId") String buildingId, @RequestParam("wingId") String wingId, @RequestParam("parkingFloorId") String parkingFloorId, HttpSession session)
	{

		Query query =new Query();
		List<ParkingZone> floorList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("floorId").is(parkingFloorId).and("parkingZoneStatus").is("Not Assigned")), ParkingZone.class);

		return floorList;
	} 			  

	@ResponseBody
	@RequestMapping(value="/getDateWiseBookingList",method=RequestMethod.POST)
	public List<Booking> getDateWiseBookingList(@RequestParam("creationDate") String creationDate, HttpSession session)
	{

		Query query =new Query();
		List<Booking> bookingList = mongoTemplate.find(query.addCriteria(Criteria.where("creationDate").is(creationDate).and("bookingstatus").ne("Cancel")), Booking.class);

		return bookingList;
	}

	@ResponseBody
	@RequestMapping(value="/getBookingId")
	public List<String> getBookingId(@RequestParam("projectId") String projectId, @RequestParam("buildingId") String buildingId, @RequestParam("wingId") String wingId, @RequestParam("flatId") String flatId)
	{
		String bookingId="BKG/";
		String projectName1[]=null, buildingName1[]=null, wingName1[]=null;
		Query query =new Query();
		Flat flatDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("flatId").is(flatId)), Flat.class);

		String flatNumber=flatDetails.getFlatNumber();
		String flatNumber1 = flatNumber.replaceAll("\\s", "");

		query = new Query();
		long bookingCount =0;
		bookingCount= mongoTemplate.count(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("flatId").is(flatId)), Booking.class);

		query =new Query();
		List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)), Project.class);

		query =new Query();
		List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(buildingId)), ProjectBuilding.class);

		query =new Query();
		List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(wingId)), ProjectWing.class);

		String projectName=projectDetails.get(0).getProjectName();
		String buildingName=buildingDetails.get(0).getBuildingName();
		String wingName=wingDetails.get(0).getWingName();
		projectName1  = projectName.split(" ");
		buildingName1 = buildingName.split(" ");
		wingName1 	  = wingName.split(" ");

		if(projectName1.length==1)
		{
			for(int i=0; i<projectName1[0].length() && i<4;i++)
			{
				bookingId = bookingId + projectName1[0].charAt(i);
			}
		}
		else
		{
			for(int i=0; i<projectName1[0].length() && i<2;i++)
			{
				bookingId = bookingId + projectName1[0].charAt(i);
			}

			for(int i=0; i<projectName1[1].length() && i<2;i++)
			{
				bookingId = bookingId + projectName1[1].charAt(i);
			}
		}

		bookingId = bookingId +"/";

		if(buildingName1.length==1)
		{
			for(int i=0; i<buildingName1[0].length() && i<4;i++)
			{
				bookingId = bookingId + buildingName1[0].charAt(i);
			}
		}
		else
		{
			for(int i=0; i<buildingName1[0].length() && i<2;i++)
			{
				bookingId = bookingId + buildingName1[0].charAt(i);
			}

			for(int i=0; i<buildingName1[1].length() && i<2;i++)
			{
				bookingId = bookingId + buildingName1[1].charAt(i);
			}
		}

		bookingId = bookingId +"/";

		if(wingName1.length==1)
		{
			for(int i=0; i<wingName1[0].length() && i<4;i++)
			{
				bookingId = bookingId + wingName1[0].charAt(i);
			}
		}
		else
		{
			for(int i=0; i<wingName1[0].length() && i<2;i++)
			{
				bookingId = bookingId + wingName1[0].charAt(i);
			}

			for(int i=0; i<wingName1[1].length() && i<2;i++)
			{
				bookingId = bookingId + wingName1[1].charAt(i);
			}
		}

		bookingId = bookingId +"/"+flatNumber1+"/"+bookingCount;
		/*if(bookingCount<10)
					{
						bookingId = bookingId +"/00"+(bookingCount+1);	
					}
					else if(bookingCount>=10 || bookingCount<100)
					{
					    bookingId = bookingId +"/0"+(bookingCount+1);
					}
					else
					{
						bookingId = bookingId +"/"+(bookingCount+1);
					}
		 */



		List<String> bookingId1 = new ArrayList();
		bookingId1.add(bookingId);
		return bookingId1;
	}

	@ResponseBody
	@RequestMapping("/getInfrastructureAndHandlingCharge")
	public List<ProjectWing> getInfrastructureAndHandlingCharge(@RequestParam("projectId") String projectId, @RequestParam("buildingId") String buildingId, @RequestParam("wingId") String wingId)
	{
		Query query = new Query();
		List<ProjectWing> chargesList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId)), ProjectWing.class);

		return chargesList;
	}

	//Request Mapping for getting all flat numbers   getFlatDetail
	@ResponseBody
	@RequestMapping(value="/getFlatNumberList",method=RequestMethod.POST)
	public List<Flat> FlatNumberList(@RequestParam("floorId") String floorId, @RequestParam("wingId") String wingId, @RequestParam("buildingId") String buildingId, @RequestParam("projectId") String projectId, HttpSession session)
	{
		List<Flat> flatnumberList= new ArrayList<Flat>();
		Query query = new Query();
		flatnumberList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("floorId").is(floorId).and("flatstatus").is("Booking Remaninig")), Flat.class);

		return flatnumberList;
	}

	//Request Mapping for getting flat details
	@ResponseBody
	@RequestMapping(value="/getFlatDetail",method=RequestMethod.POST)
	public List<Flat> FlatDetails(@RequestParam("flatId") String flatId, @RequestParam("wingId") String wingId, @RequestParam("buildingId") String buildingId, @RequestParam("projectId") String projectId, HttpSession session)
	{
		List<Flat> flatDetails= new ArrayList<Flat>();

		Query query = new Query();
		flatDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("flatId").is(flatId)), Flat.class);
		return flatDetails;
	}

	@RequestMapping(value="/ExportBookingList", method=RequestMethod.GET)
	public ModelAndView generateExcel(HttpServletRequest req, HttpServletResponse res) throws Exception 
	{
		List<Booking> bookingDetails = bookingRepository.findAll();
		List<Project> projectList = GetUserAssigenedProjectList(req,res);
		List<ProjectBuilding> buildingList = projectbuildingRepository.findAll();
		List<ProjectWing> wingList = wingRepository.findAll();
		List<Floor> floorList = floorRepository.findAll();
		List<Flat> flatList = flatRepository.findAll();

		List<ParkingZone> parkingZoneList = parkingZoneRepository.findAll();
		List<Agent> agentList = agentRepository.findAll();

		String projectName="",buildingName="",wingName="",floorName="",flatNumber="",parkingFloorName="",parkingFloorZone="",agentName="";

		for(int i=0;i<bookingDetails.size();i++)
		{
			projectName="";
			buildingName="";
			wingName="";
			floorName="";
			flatNumber="";
			parkingFloorName="";
			parkingFloorZone="";
			agentName="";

			for(int j=0;j<projectList.size();j++)
			{
				if(bookingDetails.get(i).getProjectId().equals(projectList.get(j).getProjectId()))
				{
					projectName=projectList.get(j).getProjectName();
					break;
				}
			}

			for(int j=0;j<buildingList.size();j++)
			{
				if(bookingDetails.get(i).getBuildingId().equals(buildingList.get(j).getBuildingId()))
				{
					buildingName=buildingList.get(j).getBuildingName();
					break;
				}
			}

			for(int j=0;j<wingList.size();j++)
			{
				if(bookingDetails.get(i).getWingId().equals(wingList.get(j).getWingId()))
				{
					wingName=wingList.get(j).getWingName();
					break;
				}
			}

			for(int j=0;j<floorList.size();j++)
			{
				if(bookingDetails.get(i).getFloorId().equals(floorList.get(j).getFloorId()))
				{
					floorName=floorList.get(j).getFloortypeName();
					break;
				}
			}

			for(int j=0;j<flatList.size();j++)
			{
				if(bookingDetails.get(i).getFlatId().equals(flatList.get(j).getFlatId()))
				{
					flatNumber=flatList.get(j).getFlatNumber();
					break;
				}
			}

			for(int j=0;j<floorList.size();j++)
			{
				if(bookingDetails.get(i).getParkingFloorId().equals(floorList.get(j).getFloorId()))
				{
					parkingFloorName=floorList.get(j).getFloortypeName();
					break;
				}
			}

			for(int j=0;j<parkingZoneList.size();j++)
			{
				if(bookingDetails.get(i).getParkingZoneId().equals(parkingZoneList.get(j).getParkingZoneId()))
				{
					parkingFloorZone=parkingZoneList.get(j).getParkingNumber();
					break;
				}
			}

			for(int j=0;j<agentList.size();j++)
			{
				if(bookingDetails.get(i).getAgentId().equals(agentList.get(j).getAgentId()))
				{
					agentName=agentList.get(j).getAgentfirmName();
					break;
				}
			}

			bookingDetails.get(i).setProjectId(projectName);
			bookingDetails.get(i).setBuildingId(buildingName);
			bookingDetails.get(i).setWingId(wingName);
			bookingDetails.get(i).setFloorId(floorName);
			bookingDetails.get(i).setFlatId(flatNumber);

			bookingDetails.get(i).setParkingFloorId(parkingFloorName);
			bookingDetails.get(i).setParkingZoneId(parkingFloorZone);
			bookingDetails.get(i).setAgentId(agentName);

		}


		ModelAndView modelAndView = new ModelAndView(new BookingDetailsExcelView(), "bookingDetails" , bookingDetails);

		return modelAndView;
	}

	String GetBookingId(String projectId, String buildingId, String wingId, String flatId)
	{

		String bookingId="BKG/";
		String projectName1[]=null, buildingName1[]=null, wingName1[]=null;
		Query query =new Query();
		List<Flat> flatDetails = mongoTemplate.find(query.addCriteria(Criteria.where("flatId").is(flatId)), Flat.class);

		String flatNumber=flatDetails.get(0).getFlatNumber();
		String flatNumber1 = flatNumber.replaceAll("\\s", "");

		query = new Query();
		long bookingCount =0;
		bookingCount= mongoTemplate.count(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("flatId").is(flatId)), Booking.class);

		query =new Query();
		List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)), Project.class);

		query =new Query();
		List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(buildingId)), ProjectBuilding.class);

		query =new Query();
		List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(wingId)), ProjectWing.class);

		String projectName=projectDetails.get(0).getProjectName();
		String buildingName=buildingDetails.get(0).getBuildingName();
		String wingName=wingDetails.get(0).getWingName();
		projectName1  = projectName.split(" ");
		buildingName1 = buildingName.split(" ");
		wingName1 	  = wingName.split(" ");

		if(projectName1.length==1)
		{
			for(int i=0; i<projectName1[0].length() && i<4;i++)
			{
				bookingId = bookingId + projectName1[0].charAt(i);
			}
		}
		else
		{
			for(int i=0; i<projectName1[0].length() && i<2;i++)
			{
				bookingId = bookingId + projectName1[0].charAt(i);
			}

			for(int i=0; i<projectName1[1].length() && i<2;i++)
			{
				bookingId = bookingId + projectName1[1].charAt(i);
			}
		}

		bookingId = bookingId +"/";

		if(buildingName1.length==1)
		{
			for(int i=0; i<buildingName1[0].length() && i<4;i++)
			{
				bookingId = bookingId + buildingName1[0].charAt(i);
			}
		}
		else
		{
			for(int i=0; i<buildingName1[0].length() && i<2;i++)
			{
				bookingId = bookingId + buildingName1[0].charAt(i);
			}

			for(int i=0; i<buildingName1[1].length() && i<2;i++)
			{
				bookingId = bookingId + buildingName1[1].charAt(i);
			}
		}

		bookingId = bookingId +"/";

		if(wingName1.length==1)
		{
			for(int i=0; i<wingName1[0].length() && i<4;i++)
			{
				bookingId = bookingId + wingName1[0].charAt(i);
			}
		}
		else
		{
			for(int i=0; i<wingName1[0].length() && i<2;i++)
			{
				bookingId = bookingId + wingName1[0].charAt(i);
			}

			for(int i=0; i<wingName1[1].length() && i<2;i++)
			{
				bookingId = bookingId + wingName1[1].charAt(i);
			}
		}

		bookingId = bookingId +"/"+flatNumber1+"/"+bookingCount;

		return bookingId;
	}

	@RequestMapping(value = "/FlatWiseMarketPrice", method = RequestMethod.POST)
	public String FlatWiseMarketPrice(Model model, @ModelAttribute FlatWiseMarketPrice flatwisemarketprice, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			Query query = new Query();
			Flat flatDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(flatwisemarketprice.getProjectId()).and("buildingId").is(flatwisemarketprice.getBuildingId()).and("wingId").is(flatwisemarketprice.getWingId()).and("flatNumber").is(flatwisemarketprice.getFlatNumber())), Flat.class);

			query = new Query();
			Project projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(flatwisemarketprice.getProjectId())), Project.class);

			query = new Query();
			ProjectBuilding buildingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(flatwisemarketprice.getBuildingId())), ProjectBuilding.class);

			query = new Query();
			ProjectWing wingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(flatwisemarketprice.getWingId())), ProjectWing.class);

			query = new Query();
			Floor floorDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("floorId").is(flatDetails.getFloorId())), Floor.class);

			List<Project> projectList = GetUserAssigenedProjectList(req,res); 

			List<Floor> parkingFloorList=new ArrayList<Floor>();

			query = new Query();
			parkingFloorList = mongoTemplate.find(query.addCriteria(Criteria.where("floorzoneType").is("Parking").and("projectId").is(flatwisemarketprice.getProjectId()).and("buildingId").is(flatwisemarketprice.getBuildingId()).and("wingId").is(flatwisemarketprice.getWingId())), Floor.class);

			String bookingId=GetBookingId(flatwisemarketprice.getProjectId(),flatwisemarketprice.getBuildingId(),flatwisemarketprice.getWingId(),flatDetails.getFlatId());

			model.addAttribute("bookingId",bookingId);
			model.addAttribute("parkingFloorList",parkingFloorList);
			model.addAttribute("projectName",projectDetails.getProjectName());
			model.addAttribute("buildingName",buildingDetails.getBuildingName());
			model.addAttribute("wingName",wingDetails.getWingName());
			model.addAttribute("floortypeName",floorDetails.getFloortypeName());
			model.addAttribute("flatNumber",flatDetails.getFlatNumber());
			model.addAttribute("flatDetails", flatDetails);
			model.addAttribute("flatwisemarketprice", flatwisemarketprice);

			model.addAttribute("projectList",projectList);
			model.addAttribute("enquiryCode",EnquiryCode());
			model.addAttribute("occupationList",getAllOccupationName());
			model.addAttribute("countryList",findAllCountryId());  
			model.addAttribute("bankList",getAllBankName());
			model.addAttribute("enquirySourceList", getAllEnquirySourceName());
			model.addAttribute("agentList",AgentList());
			model.addAttribute("budgetList",getBudgetList());

			return "AddBookingForm";

		}catch (Exception e) {
			return "login";
		}
	}

}

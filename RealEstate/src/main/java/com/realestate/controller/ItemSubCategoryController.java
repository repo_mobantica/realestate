package com.realestate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.ItemSubCategory;
import com.realestate.bean.SubSupplierType;
import com.realestate.bean.SupplierType;
import com.realestate.repository.ItemSubCategoryRepository;
import com.realestate.repository.SupplierTypeRepository;

@Controller
@RequestMapping("/")
public class ItemSubCategoryController 
{
	@Autowired
	SupplierTypeRepository supplierTypeRepository;
	@Autowired
	ItemSubCategoryRepository itemSubCategoryRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	String subItemId = null;
	public String SubItemIdGenerate()
	{
		long count = itemSubCategoryRepository.count();
		if(count<10)
		{
			subItemId = "SI000"+(count+1);
		}
		else if((count<100) && (count>=10))
		{
			subItemId = "SI00"+(count+1);
		}
		else if((count<1000) && (count>=100))
		{
			subItemId = "SI0"+(count+1);
		}
		else 
		{
			subItemId = "SI"+(count+1);
		}

		return subItemId;
	}

	@RequestMapping("/ItemSubCategoryMaster")
	public String ItemSubCategoryMaster(Model model)
	{
		try {
			List<SupplierType> suppliertypeList = supplierTypeRepository.findAll();
			List<ItemSubCategory> itemSubCategoryList =  itemSubCategoryRepository.findAll();

			model.addAttribute("suppliertypeList",suppliertypeList);
			model.addAttribute("itemSubCategoryList", itemSubCategoryList);
			return "ItemSubCategoryMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/GetItemCategoryList")
	public List<ItemSubCategory> GetSubItemCategoryList(@RequestParam("itemMainCategoryName") String itemMainCategoryName)
	{
		Query query = new Query();
		List<ItemSubCategory> itemSubCategoryList = mongoTemplate.find(query.addCriteria(Criteria.where("itemMainCategoryName").is(itemMainCategoryName)), ItemSubCategory.class);

		return itemSubCategoryList;
	}

	@ResponseBody
	@RequestMapping("/GetSubItemCategoryList")
	public List<ItemSubCategory> SubItemListCategory(@RequestParam("itemMainCategoryName") String itemMainCategoryName, @RequestParam("itemCategoryName") String itemCategoryName)
	{
		Query query = new Query();
		List<ItemSubCategory> itemSubCategoryList = mongoTemplate.find(query.addCriteria(Criteria.where("itemMainCategoryName").is(itemMainCategoryName).and("itemCategoryName").is(itemCategoryName)), ItemSubCategory.class);

		return itemSubCategoryList;
	}

	@ResponseBody
	@RequestMapping("/GetSubItemCategory")
	public List<SubSupplierType> GetSubItemCategory(@RequestParam("itemMainCategoryName") String supplierType)
	{
		Query query = new Query();
		List<SubSupplierType> subSupplierTypeList = mongoTemplate.find(query.addCriteria(Criteria.where("supplierType").is(supplierType)), SubSupplierType.class);
		return subSupplierTypeList;
	}

	@RequestMapping("/AddItemSubCategory")
	public String AddItemSubCategory(Model model)
	{
		try {
			List<SupplierType> suppliertypeList = supplierTypeRepository.findAll();

			model.addAttribute("subItemId",SubItemIdGenerate());
			model.addAttribute("suppliertypeList", suppliertypeList);

			return "AddItemSubCategory";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/AddItemSubCategory", method=RequestMethod.POST)
	public String AddItemSubCategoryPostMethod(@ModelAttribute ItemSubCategory itemSubCategory , Model model)
	{
		try {
			String Status = null;
			try
			{
				itemSubCategory = new ItemSubCategory(itemSubCategory.getSubItemCategoryId(), itemSubCategory.getItemMainCategoryName(), itemSubCategory.getItemCategoryName(), itemSubCategory.getSubItemCategoryName(), itemSubCategory.getCreationDate(), itemSubCategory.getUpdateDate(), itemSubCategory.getUserName());
				itemSubCategoryRepository.save(itemSubCategory);
				Status = "Success";
			}
			catch(Exception e)
			{
				e.printStackTrace();
				Status = "Fail";
			}

			List<SupplierType> suppliertypeList = supplierTypeRepository.findAll();

			model.addAttribute("subItemId",SubItemIdGenerate());
			model.addAttribute("Status", Status);
			model.addAttribute("suppliertypeList", suppliertypeList);

			return "AddItemSubCategory";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("EditItemSubCategory")
	public String EditItemSubCategoryGetMethod(@RequestParam("subItemCategoryId") String subItemCategoryId, Model model)
	{
		try {
			List<SupplierType> suppliertypeList = supplierTypeRepository.findAll();

			Query query = new Query();
			ItemSubCategory itemSubCategoryDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("subItemCategoryId").is(subItemCategoryId)), ItemSubCategory.class);

			model.addAttribute("itemSubCategoryDetails", itemSubCategoryDetails);
			model.addAttribute("suppliertypeList", suppliertypeList);
			return "EditItemSubCategory";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/EditItemSubCategory", method=RequestMethod.POST)
	public String EditItemSubCategoryPostMethod(@ModelAttribute ItemSubCategory itemSubCategory , Model model)
	{
		try {
			try
			{
				itemSubCategory = new ItemSubCategory(itemSubCategory.getSubItemCategoryId(), itemSubCategory.getItemMainCategoryName(), itemSubCategory.getItemCategoryName(), itemSubCategory.getSubItemCategoryName(), itemSubCategory.getCreationDate(), itemSubCategory.getUpdateDate(), itemSubCategory.getUserName());
				itemSubCategoryRepository.save(itemSubCategory);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}

			List<SupplierType> suppliertypeList = supplierTypeRepository.findAll();
			List<ItemSubCategory> itemSubCategoryList =  itemSubCategoryRepository.findAll();

			model.addAttribute("suppliertypeList",suppliertypeList);
			model.addAttribute("itemSubCategoryList", itemSubCategoryList);

			return "ItemSubCategoryMaster";

		}catch (Exception e) {
			return "login";
		}
	}

}

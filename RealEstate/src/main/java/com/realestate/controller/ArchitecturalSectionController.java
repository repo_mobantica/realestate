package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.realestate.bean.Consultancy;
import com.realestate.bean.Login;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectArchitecturalSectionForm;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.CountryRepository;
import com.realestate.repository.DesignationRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.ConsultancyRepository;
import com.realestate.repository.ProjectArchitecturalSectionFormRepository;
import com.realestate.repository.ConsultancyEmployeesRepository;
@Controller
@RequestMapping("/")
public class ArchitecturalSectionController 
{
	@Autowired
	DesignationRepository designationRepository;
	@Autowired
	CountryRepository countryRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	ConsultancyEmployeesRepository architecturalEmployeesRepository;
	@Autowired
	ConsultancyRepository architecturalRepository;
	@Autowired
	ProjectArchitecturalSectionFormRepository projectarchitecturalsectionformRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	String projectArchitecturalCode;

	private String ProjectArchitecturalCode(String projectId)
	{
		Query query = new Query();
		List<ProjectArchitecturalSectionForm> projectArchitecturallist = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)),ProjectArchitecturalSectionForm.class);
		query = new Query();
		List<Project> projectlist = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)),Project.class);
		String projectName=projectlist.get(0).getProjectName();
		long count= projectArchitecturallist.size()+1;
		projectArchitecturalCode=projectName +"/" + count;
		return projectArchitecturalCode;
	}

	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping("/ArchitecturalSectionMaster")
	public String ArchitecturalSection(Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<Project> projectList = GetUserAssigenedProjectList(req,res); 

			model.addAttribute("projectList",projectList);
			return "ArchitecturalSectionMaster";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping("/AllProjectArchitecturalSection")
	public String AllProjectArchitecturalSection(@RequestParam("projectId") String projectId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<ProjectArchitecturalSectionForm> projectWiseList1 = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)),ProjectArchitecturalSectionForm.class);

			query = new Query();
			List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)),Project.class);

			List<ProjectArchitecturalSectionForm> projectWiseList=new ArrayList<ProjectArchitecturalSectionForm>();

			int i,j,flag=0;

			try
			{
				if(projectWiseList1.size()!=0)
				{
					ProjectArchitecturalSectionForm projectarchitecturalsectionform=new ProjectArchitecturalSectionForm();

					projectarchitecturalsectionform.setProjectArchitecturalId(projectWiseList1.get(0).getProjectArchitecturalId());
					projectarchitecturalsectionform.setProjectId(projectDetails.get(0).getProjectName());
					projectarchitecturalsectionform.setConsultancyId(projectWiseList1.get(0).getConsultancyId());

					query = new Query();
					List<Consultancy> consultancyDetails = mongoTemplate.find(query.addCriteria(Criteria.where("consultancyId").is(projectWiseList1.get(0).getConsultancyId())),Consultancy.class);
					projectarchitecturalsectionform.setFirmName(consultancyDetails.get(0).getConsultancyfirmName());

					projectarchitecturalsectionform.setCreationDate(projectWiseList1.get(0).getCreationDate());

					projectWiseList.add(projectarchitecturalsectionform);


					flag =0;
					for(i=0;i<projectWiseList1.size();i++)
					{
						flag =0;
						for(j=0;j<projectWiseList.size();j++)
						{
							if(projectWiseList1.get(i).getConsultancyId().equals(projectWiseList.get(j).getConsultancyId()))
							{
								flag =1;
								break;
							}

						}
						if(flag ==0)
						{
							projectarchitecturalsectionform=new ProjectArchitecturalSectionForm();
							projectarchitecturalsectionform.setProjectArchitecturalId(projectWiseList1.get(i).getProjectArchitecturalId());
							projectarchitecturalsectionform.setProjectId(projectDetails.get(0).getProjectName());
							projectarchitecturalsectionform.setConsultancyId(projectWiseList1.get(i).getConsultancyId());
							query = new Query();
							consultancyDetails = mongoTemplate.find(query.addCriteria(Criteria.where("consultancyId").is(projectWiseList1.get(i).getConsultancyId())),Consultancy.class);
							projectarchitecturalsectionform.setFirmName(consultancyDetails.get(0).getConsultancyfirmName());

							projectarchitecturalsectionform.setCreationDate(projectWiseList1.get(i).getCreationDate());
							projectWiseList.add(projectarchitecturalsectionform);
						}
					}


				}

			}
			catch (Exception e) {
				// TODO: handle exception
			}



			/*
	    	for(int i=0;i<projectWiseList.size();i++) 
	    	{
	    		try
	    		{
	    		query = new Query();
		    	List<Consultancy> consultancyDetails = mongoTemplate.find(query.addCriteria(Criteria.where("consultancyId").is(projectWiseList.get(i).getConsultancyId())),Consultancy.class);

		    	projectWiseList.get(i).setProjectId(projectDetails.get(0).getProjectName());
		    	projectWiseList.get(i).setConsultancyId(consultancyDetails.get(0).getConsultancyfirmName());
	    		}
	    		catch (Exception e) {
					// TODO: handle exception
				}
	    	}
			 */
			model.addAttribute("projectName",projectDetails.get(0).getProjectName());
			model.addAttribute("projectWiseList",projectWiseList);
			model.addAttribute("projectId",projectId);
			return "AllProjectArchitecturalSection";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/ArchitecturalSectionForm")
	public String ArchitecturalSectionForm(@RequestParam("projectId") String projectId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<Project> projectList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)),Project.class);

			List<Consultancy> consultancyList = architecturalRepository.findAll(); 

			model.addAttribute("projectArchitecturalCode",ProjectArchitecturalCode(projectId));
			model.addAttribute("consultancyList",consultancyList);
			model.addAttribute("projectList",projectList);
			return "ArchitecturalSectionForm";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value = "/ArchitecturalSectionForm", method = RequestMethod.POST)
	public String SaveArchitecturalSectionForm(@ModelAttribute ProjectArchitecturalSectionForm projectarchitecturalsectionform, Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				projectarchitecturalsectionform = new ProjectArchitecturalSectionForm(projectarchitecturalsectionform.getProjectArchitecturalId(), projectarchitecturalsectionform.getProjectId(), projectarchitecturalsectionform.getConsultancyId(), 
						projectarchitecturalsectionform.getAeraOfPlotAsPer7_12ExtractPer(), projectarchitecturalsectionform.getAreaOfPlotAsPerPropetyPer(), projectarchitecturalsectionform.getAreaOfPlotAsPerDemarcationPer(), projectarchitecturalsectionform.getAreaAsPerSanctionedLayoutPer(), 
						projectarchitecturalsectionform.getAreaAsPerULCOrderPer(), projectarchitecturalsectionform.getAreaAsPerDevelopmentAgreementPer(),projectarchitecturalsectionform.getAreaOfPlotMinimumConsiderationPer(), 
						projectarchitecturalsectionform.getAreaUnderBRTCorridorPer(), projectarchitecturalsectionform.getAreaUnder1_50MRoadWideningPer(),
						projectarchitecturalsectionform.getAreaUnder3_00MRoadWideningPer(), projectarchitecturalsectionform.getAreaUnder4_50MRoadWideningPer(), projectarchitecturalsectionform.getAreaUnder6_00MRoadWideningPer(), projectarchitecturalsectionform.getAreaUnder7_50MRoadWideningPer(),
						projectarchitecturalsectionform.getAreaUnder9_00MRoadWideningPer(), projectarchitecturalsectionform.getAreaUnder12_00MRoadWideningPer(),
						projectarchitecturalsectionform.getAreaUnder15_00MRoadWideningPer(), projectarchitecturalsectionform.getAreaUnder18_00MRoadWideningPer(), projectarchitecturalsectionform.getAreaUnder24_00MRoadWideningPer(), projectarchitecturalsectionform.getAreaUnder30_00MRoadWideningPer(),
						projectarchitecturalsectionform.getAreaUnderServiceRoadPer(),projectarchitecturalsectionform.getAreaUnderReservationPer(), projectarchitecturalsectionform.getAreaUnderGreenBeltPer(), projectarchitecturalsectionform.getAreaUnderNalaPer(), 
						projectarchitecturalsectionform.getRecreationGround10Per(),
						projectarchitecturalsectionform.getAmenitySpace5Per(), projectarchitecturalsectionform.getInternalRoadsPer(), projectarchitecturalsectionform.getTransformerPer(), 
						projectarchitecturalsectionform.getTdr40Per(),projectarchitecturalsectionform.getAdditionsForFAR4a_RecreationGroundPer(), projectarchitecturalsectionform.getAdditionForFAR4b_AmenitySpacePer(), projectarchitecturalsectionform.getAdditionForFAR4c_InternalsRoadPer(), projectarchitecturalsectionform.getAdditionForFAR4d_TransformerPer(),
						projectarchitecturalsectionform.getFarPermissiblePer(), projectarchitecturalsectionform.getPermissibleResidentialPer(), projectarchitecturalsectionform.getExistingResidentialPer(),
						projectarchitecturalsectionform.getProposedResidentialPer(), projectarchitecturalsectionform.getPermissibleCommercial_IndustrialPer(),projectarchitecturalsectionform.getExistingCommercial_IndustrialPer(), projectarchitecturalsectionform.getProposedCommercial_IndustrialPer(),
						projectarchitecturalsectionform.getExcessBalconyAreaTakenInFSIPer(), projectarchitecturalsectionform.getTotalFSIConSumedPer(), projectarchitecturalsectionform.getPermissibleBalconyPer(),
						projectarchitecturalsectionform.getProposedBalconyPer(), 
						projectarchitecturalsectionform.getNetGrossAreaOfPlot_A7_Per(),projectarchitecturalsectionform.getLessDeductionForNon_ResidentialPer(),projectarchitecturalsectionform.getTenenmentsPermissiblePer(), projectarchitecturalsectionform.getTotalTenementsProposedPer(), projectarchitecturalsectionform.getNetPlotAreaPer(),
						projectarchitecturalsectionform.getPermissibleGroundCoveragePer(), projectarchitecturalsectionform.getExistingGroundCoveragePer(),
						projectarchitecturalsectionform.getProposedGroundCoveragePer(), projectarchitecturalsectionform.getPermissibleGroundCoverageInPremium15Per(), 

						projectarchitecturalsectionform.getAeraOfPlotAsPer7_12ExtractArea(), projectarchitecturalsectionform.getAreaOfPlotAsPerPropetyArea(), projectarchitecturalsectionform.getAreaOfPlotAsPerDemarcationArea(), projectarchitecturalsectionform.getAreaAsPerSanctionedLayoutArea(), 
						projectarchitecturalsectionform.getAreaAsPerULCOrderArea(), projectarchitecturalsectionform.getAreaAsPerDevelopmentAgreementArea(),projectarchitecturalsectionform.getAreaOfPlotMinimumConsiderationArea(), 
						projectarchitecturalsectionform.getAreaUnderBRTCorridorArea(), projectarchitecturalsectionform.getAreaUnder1_50MRoadWideningArea(),
						projectarchitecturalsectionform.getAreaUnder3_00MRoadWideningArea(), projectarchitecturalsectionform.getAreaUnder4_50MRoadWideningArea(), projectarchitecturalsectionform.getAreaUnder6_00MRoadWideningArea(), projectarchitecturalsectionform.getAreaUnder7_50MRoadWideningArea(),
						projectarchitecturalsectionform.getAreaUnder9_00MRoadWideningArea(), projectarchitecturalsectionform.getAreaUnder12_00MRoadWideningArea(),
						projectarchitecturalsectionform.getAreaUnder15_00MRoadWideningArea(), projectarchitecturalsectionform.getAreaUnder18_00MRoadWideningArea(), projectarchitecturalsectionform.getAreaUnder24_00MRoadWideningArea(), projectarchitecturalsectionform.getAreaUnder30_00MRoadWideningArea(),
						projectarchitecturalsectionform.getAreaUnderServiceRoadArea(),projectarchitecturalsectionform.getAreaUnderReservationArea(), projectarchitecturalsectionform.getAreaUnderGreenBeltArea(), projectarchitecturalsectionform.getAreaUnderNalaArea(), projectarchitecturalsectionform.getTotal2Area(),
						projectarchitecturalsectionform.getNetGrossAreaOfPlot1_2Area(), projectarchitecturalsectionform.getRecreationGround10Area(),
						projectarchitecturalsectionform.getAmenitySpace5Area(), projectarchitecturalsectionform.getInternalRoadsArea(), projectarchitecturalsectionform.getTransformerArea(), projectarchitecturalsectionform.getTotal4Area(),
						projectarchitecturalsectionform.getNetAreaOfPlot3_4Area(),projectarchitecturalsectionform.getTdr40Area(),projectarchitecturalsectionform.getAdditionsForFAR4a_RecreationGroundArea(), projectarchitecturalsectionform.getAdditionForFAR4b_AmenitySpaceArea(), projectarchitecturalsectionform.getAdditionForFAR4c_InternalsRoadArea(), projectarchitecturalsectionform.getAdditionForFAR4d_TransformerArea(),
						projectarchitecturalsectionform.getTotal6Area(), projectarchitecturalsectionform.getTotalArea5_6Area(),
						projectarchitecturalsectionform.getFarPermissibleArea(), projectarchitecturalsectionform.getTotalPermissibleFloorArea7_8Area(), projectarchitecturalsectionform.getPermissibleResidentialArea(), projectarchitecturalsectionform.getExistingResidentialArea(),
						projectarchitecturalsectionform.getProposedResidentialArea(), projectarchitecturalsectionform.getTotalResidentialArea11_12Area(),projectarchitecturalsectionform.getPermissibleCommercial_IndustrialArea(),projectarchitecturalsectionform.getExistingCommercial_IndustrialArea(), projectarchitecturalsectionform.getProposedCommercial_IndustrialArea(), projectarchitecturalsectionform.getTotalCommercial_IndustrialArea15_16Area(), projectarchitecturalsectionform.getTotalExistingArea11_15(),
						projectarchitecturalsectionform.getTotalProposedArea12_16Area(), projectarchitecturalsectionform.getTotalExistingArea_Proposed18_19Area(),
						projectarchitecturalsectionform.getExcessBalconyAreaTakenInFSIArea(), projectarchitecturalsectionform.getTotalBupArea20_21Area(), projectarchitecturalsectionform.getTotalFSIConSumedArea(), projectarchitecturalsectionform.getPermissibleBalconyArea(),
						projectarchitecturalsectionform.getProposedBalconyArea(), 
						projectarchitecturalsectionform.getExcessBalconyArea(), projectarchitecturalsectionform.getNetGrossAreaOfPlot_A7_Area(),projectarchitecturalsectionform.getLessDeductionForNon_ResidentialArea(),projectarchitecturalsectionform.getAreaOfTenementsa_bArea(), projectarchitecturalsectionform.getTenenmentsPermissibleArea(), projectarchitecturalsectionform.getTotalTenementsProposedArea(), projectarchitecturalsectionform.getNetPlotAreaArea(),
						projectarchitecturalsectionform.getPermissibleGroundCoverageArea(), projectarchitecturalsectionform.getExistingGroundCoverageArea(),
						projectarchitecturalsectionform.getProposedGroundCoverageArea(), projectarchitecturalsectionform.getPermissibleGroundCoverageInPremium15Area(), projectarchitecturalsectionform.getExcessGroundConveraged_bArea(), 

						projectarchitecturalsectionform.getCreationDate(), projectarchitecturalsectionform.getUpdateDate(), projectarchitecturalsectionform.getUserName());
				projectarchitecturalsectionformRepository.save(projectarchitecturalsectionform);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}

			List<Project> projectList = GetUserAssigenedProjectList(req,res); 

			model.addAttribute("projectList",projectList);
			return "ArchitecturalSectionMaster";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping("/EditArchitecturalSectionForm")
	public String EditArchitecturalSectionForm(@RequestParam("projectArchitecturalId") String projectArchitecturalId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<ProjectArchitecturalSectionForm> projectarchitecturalsectionFormDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectArchitecturalId").is(projectArchitecturalId)),ProjectArchitecturalSectionForm.class);

			String projectId=projectarchitecturalsectionFormDetails.get(0).getProjectId();
			String consultancyId=projectarchitecturalsectionFormDetails.get(0).getConsultancyId();

			query = new Query();
			List<ProjectArchitecturalSectionForm> projectarchitecturalsectionFormDetails1 = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("consultancyId").is(consultancyId)),ProjectArchitecturalSectionForm.class);

			System.out.println(projectarchitecturalsectionFormDetails1.size());
			query = new Query();
			List<Project> projectList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectarchitecturalsectionFormDetails.get(0).getProjectId())),Project.class);

			query = new Query();
			List<Consultancy> consultancyDetails = mongoTemplate.find(query.addCriteria(Criteria.where("consultancyId").is(projectarchitecturalsectionFormDetails.get(0).getConsultancyId())),Consultancy.class);
			//String architecturalFirmName=architecturalList1.get(0).getArchitecturalfirmName();


			model.addAttribute("projectName",projectList.get(0).getProjectName());
			model.addAttribute("consultancyfirmName",consultancyDetails.get(0).getConsultancyfirmName());
			model.addAttribute("projectarchitecturalsectionFormDetails",projectarchitecturalsectionFormDetails);

			model.addAttribute("projectArchitecturalCode",ProjectArchitecturalCode(projectId));
			model.addAttribute("projectarchitecturalsectionFormDetails",projectarchitecturalsectionFormDetails1);

			return "EditArchitecturalSectionForm";

		}catch (Exception e) {
			return "login";
		}
	}



	@RequestMapping(value = "/EditArchitecturalSectionForm", method = RequestMethod.POST)
	public String EditArchitecturalSectionForm(@ModelAttribute ProjectArchitecturalSectionForm projectarchitecturalsectionform, Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				projectarchitecturalsectionform = new ProjectArchitecturalSectionForm(projectarchitecturalsectionform.getProjectArchitecturalId(), projectarchitecturalsectionform.getProjectId(), projectarchitecturalsectionform.getConsultancyId(), 
						projectarchitecturalsectionform.getAeraOfPlotAsPer7_12ExtractPer(), projectarchitecturalsectionform.getAreaOfPlotAsPerPropetyPer(), projectarchitecturalsectionform.getAreaOfPlotAsPerDemarcationPer(), projectarchitecturalsectionform.getAreaAsPerSanctionedLayoutPer(), 
						projectarchitecturalsectionform.getAreaAsPerULCOrderPer(), projectarchitecturalsectionform.getAreaAsPerDevelopmentAgreementPer(),projectarchitecturalsectionform.getAreaOfPlotMinimumConsiderationPer(), 
						projectarchitecturalsectionform.getAreaUnderBRTCorridorPer(), projectarchitecturalsectionform.getAreaUnder1_50MRoadWideningPer(),
						projectarchitecturalsectionform.getAreaUnder3_00MRoadWideningPer(), projectarchitecturalsectionform.getAreaUnder4_50MRoadWideningPer(), projectarchitecturalsectionform.getAreaUnder6_00MRoadWideningPer(), projectarchitecturalsectionform.getAreaUnder7_50MRoadWideningPer(),
						projectarchitecturalsectionform.getAreaUnder9_00MRoadWideningPer(), projectarchitecturalsectionform.getAreaUnder12_00MRoadWideningPer(),
						projectarchitecturalsectionform.getAreaUnder15_00MRoadWideningPer(), projectarchitecturalsectionform.getAreaUnder18_00MRoadWideningPer(), projectarchitecturalsectionform.getAreaUnder24_00MRoadWideningPer(), projectarchitecturalsectionform.getAreaUnder30_00MRoadWideningPer(),
						projectarchitecturalsectionform.getAreaUnderServiceRoadPer(),projectarchitecturalsectionform.getAreaUnderReservationPer(), projectarchitecturalsectionform.getAreaUnderGreenBeltPer(), projectarchitecturalsectionform.getAreaUnderNalaPer(), 
						projectarchitecturalsectionform.getRecreationGround10Per(),
						projectarchitecturalsectionform.getAmenitySpace5Per(), projectarchitecturalsectionform.getInternalRoadsPer(), projectarchitecturalsectionform.getTransformerPer(), 
						projectarchitecturalsectionform.getTdr40Per(),projectarchitecturalsectionform.getAdditionsForFAR4a_RecreationGroundPer(), projectarchitecturalsectionform.getAdditionForFAR4b_AmenitySpacePer(), projectarchitecturalsectionform.getAdditionForFAR4c_InternalsRoadPer(), projectarchitecturalsectionform.getAdditionForFAR4d_TransformerPer(),
						projectarchitecturalsectionform.getFarPermissiblePer(), projectarchitecturalsectionform.getPermissibleResidentialPer(), projectarchitecturalsectionform.getExistingResidentialPer(),
						projectarchitecturalsectionform.getProposedResidentialPer(), projectarchitecturalsectionform.getPermissibleCommercial_IndustrialPer(),projectarchitecturalsectionform.getExistingCommercial_IndustrialPer(), projectarchitecturalsectionform.getProposedCommercial_IndustrialPer(),
						projectarchitecturalsectionform.getExcessBalconyAreaTakenInFSIPer(), projectarchitecturalsectionform.getTotalFSIConSumedPer(), projectarchitecturalsectionform.getPermissibleBalconyPer(),
						projectarchitecturalsectionform.getProposedBalconyPer(), 
						projectarchitecturalsectionform.getNetGrossAreaOfPlot_A7_Per(),projectarchitecturalsectionform.getLessDeductionForNon_ResidentialPer(),projectarchitecturalsectionform.getTenenmentsPermissiblePer(), projectarchitecturalsectionform.getTotalTenementsProposedPer(), projectarchitecturalsectionform.getNetPlotAreaPer(),
						projectarchitecturalsectionform.getPermissibleGroundCoveragePer(), projectarchitecturalsectionform.getExistingGroundCoveragePer(),
						projectarchitecturalsectionform.getProposedGroundCoveragePer(), projectarchitecturalsectionform.getPermissibleGroundCoverageInPremium15Per(), 

						projectarchitecturalsectionform.getAeraOfPlotAsPer7_12ExtractArea(), projectarchitecturalsectionform.getAreaOfPlotAsPerPropetyArea(), projectarchitecturalsectionform.getAreaOfPlotAsPerDemarcationArea(), projectarchitecturalsectionform.getAreaAsPerSanctionedLayoutArea(), 
						projectarchitecturalsectionform.getAreaAsPerULCOrderArea(), projectarchitecturalsectionform.getAreaAsPerDevelopmentAgreementArea(),projectarchitecturalsectionform.getAreaOfPlotMinimumConsiderationArea(), 
						projectarchitecturalsectionform.getAreaUnderBRTCorridorArea(), projectarchitecturalsectionform.getAreaUnder1_50MRoadWideningArea(),
						projectarchitecturalsectionform.getAreaUnder3_00MRoadWideningArea(), projectarchitecturalsectionform.getAreaUnder4_50MRoadWideningArea(), projectarchitecturalsectionform.getAreaUnder6_00MRoadWideningArea(), projectarchitecturalsectionform.getAreaUnder7_50MRoadWideningArea(),
						projectarchitecturalsectionform.getAreaUnder9_00MRoadWideningArea(), projectarchitecturalsectionform.getAreaUnder12_00MRoadWideningArea(),
						projectarchitecturalsectionform.getAreaUnder15_00MRoadWideningArea(), projectarchitecturalsectionform.getAreaUnder18_00MRoadWideningArea(), projectarchitecturalsectionform.getAreaUnder24_00MRoadWideningArea(), projectarchitecturalsectionform.getAreaUnder30_00MRoadWideningArea(),
						projectarchitecturalsectionform.getAreaUnderServiceRoadArea(),projectarchitecturalsectionform.getAreaUnderReservationArea(), projectarchitecturalsectionform.getAreaUnderGreenBeltArea(), projectarchitecturalsectionform.getAreaUnderNalaArea(), projectarchitecturalsectionform.getTotal2Area(),
						projectarchitecturalsectionform.getNetGrossAreaOfPlot1_2Area(), projectarchitecturalsectionform.getRecreationGround10Area(),
						projectarchitecturalsectionform.getAmenitySpace5Area(), projectarchitecturalsectionform.getInternalRoadsArea(), projectarchitecturalsectionform.getTransformerArea(), projectarchitecturalsectionform.getTotal4Area(),
						projectarchitecturalsectionform.getNetAreaOfPlot3_4Area(),projectarchitecturalsectionform.getTdr40Area(),projectarchitecturalsectionform.getAdditionsForFAR4a_RecreationGroundArea(), projectarchitecturalsectionform.getAdditionForFAR4b_AmenitySpaceArea(), projectarchitecturalsectionform.getAdditionForFAR4c_InternalsRoadArea(), projectarchitecturalsectionform.getAdditionForFAR4d_TransformerArea(),
						projectarchitecturalsectionform.getTotal6Area(), projectarchitecturalsectionform.getTotalArea5_6Area(),
						projectarchitecturalsectionform.getFarPermissibleArea(), projectarchitecturalsectionform.getTotalPermissibleFloorArea7_8Area(), projectarchitecturalsectionform.getPermissibleResidentialArea(), projectarchitecturalsectionform.getExistingResidentialArea(),
						projectarchitecturalsectionform.getProposedResidentialArea(), projectarchitecturalsectionform.getTotalResidentialArea11_12Area(),projectarchitecturalsectionform.getPermissibleCommercial_IndustrialArea(),projectarchitecturalsectionform.getExistingCommercial_IndustrialArea(), projectarchitecturalsectionform.getProposedCommercial_IndustrialArea(), projectarchitecturalsectionform.getTotalCommercial_IndustrialArea15_16Area(), projectarchitecturalsectionform.getTotalExistingArea11_15(),
						projectarchitecturalsectionform.getTotalProposedArea12_16Area(), projectarchitecturalsectionform.getTotalExistingArea_Proposed18_19Area(),
						projectarchitecturalsectionform.getExcessBalconyAreaTakenInFSIArea(), projectarchitecturalsectionform.getTotalBupArea20_21Area(), projectarchitecturalsectionform.getTotalFSIConSumedArea(), projectarchitecturalsectionform.getPermissibleBalconyArea(),
						projectarchitecturalsectionform.getProposedBalconyArea(), 
						projectarchitecturalsectionform.getExcessBalconyArea(), projectarchitecturalsectionform.getNetGrossAreaOfPlot_A7_Area(),projectarchitecturalsectionform.getLessDeductionForNon_ResidentialArea(),projectarchitecturalsectionform.getAreaOfTenementsa_bArea(), projectarchitecturalsectionform.getTenenmentsPermissibleArea(), projectarchitecturalsectionform.getTotalTenementsProposedArea(), projectarchitecturalsectionform.getNetPlotAreaArea(),
						projectarchitecturalsectionform.getPermissibleGroundCoverageArea(), projectarchitecturalsectionform.getExistingGroundCoverageArea(),
						projectarchitecturalsectionform.getProposedGroundCoverageArea(), projectarchitecturalsectionform.getPermissibleGroundCoverageInPremium15Area(), projectarchitecturalsectionform.getExcessGroundConveraged_bArea(), 

						projectarchitecturalsectionform.getCreationDate(), projectarchitecturalsectionform.getUpdateDate(), projectarchitecturalsectionform.getUserName());
				projectarchitecturalsectionformRepository.save(projectarchitecturalsectionform);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}

			List<Project> projectList = GetUserAssigenedProjectList(req,res); 

			model.addAttribute("projectList",projectList);
			return "ArchitecturalSectionMaster";

		}catch (Exception e) {
			return "login";
		}
	}

}

package com.realestate.controller;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Booking;
import com.realestate.bean.City;
import com.realestate.bean.Company;
import com.realestate.bean.CompanyBanks;
import com.realestate.bean.Country;
import com.realestate.bean.CustomerReceiptForm;
import com.realestate.bean.Flat;
import com.realestate.bean.FlatMarketPrice;
import com.realestate.bean.Floor;
import com.realestate.bean.LocationArea;
import com.realestate.bean.Login;
import com.realestate.bean.NumberToWordConversion;
import com.realestate.bean.PaymentScheduler;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.State;
import com.realestate.bean.Tax;
import com.realestate.bean.UserAssignedProject;
import com.realestate.configuration.CommanController;
import com.realestate.repository.FlatRepository;
import com.realestate.repository.ProjectRepository;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;

@Controller
@RequestMapping("/")
public class FlatMarketPriceReportController
{
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	FlatRepository flatRepository;
	@Autowired
	ServletContext context;
	@Autowired
	ProjectRepository projectRepository;


	private double getGstCostPer()
	{
		double taxPercentage=0.0;
		List<Tax> taxList=new ArrayList<Tax>();
		Query query1 = new Query();
		taxList = mongoTemplate.find(query1.addCriteria(Criteria.where("taxName").is("GST")), Tax.class);

		for(int i=0;i<taxList.size();i++)
		{
			taxPercentage=taxList.get(i).getTaxPercentage();
		}


		return taxPercentage;
	}

	private String getLastprojectId(HttpServletRequest req, HttpServletResponse res)
	{
		String projectId="";
		List<Project> projectList;
		projectList = GetUserAssigenedProjectList(req,res);
		for(int i=0;i<projectList.size();i++)
		{
			projectId=projectList.get(i).getProjectId();
		}
		return projectId;
	}

	private String getLastProjectbuildingId(HttpServletRequest req, HttpServletResponse res)
	{
		String projectId=getLastprojectId(req, res);
		String buildingId="";
		List<ProjectBuilding> buildingList=new ArrayList<ProjectBuilding>();;
		Query query = new Query();
		buildingList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)), ProjectBuilding.class);
		buildingId=buildingList.get(0).getBuildingId();

		return buildingId;
	}


	private String getLastProjectwingId(HttpServletRequest req, HttpServletResponse res)
	{
		String projectId=getLastprojectId(req, res);
		String buildingId=getLastProjectbuildingId(req, res);
		String wingId="";

		List<ProjectWing> WingList=new ArrayList<ProjectWing>();;
		Query query = new Query();
		WingList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId)), ProjectWing.class);
		wingId=WingList.get(0).getWingId();

		return wingId;
	}

	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@ResponseBody
	@RequestMapping(value="/PrintFlatMarketPrice", method=RequestMethod.POST)
	public ResponseEntity<byte[]> PrintFlatMarketPrice(HttpServletRequest req, ModelMap model, HttpServletResponse res)
	{
		String projectId	= "";   
		String buildingId = "";
		String wingId     = "";

		String projectId1  = req.getParameter("projectId");
		String buildingId1 = req.getParameter("buildingId");
		String wingId1     = req.getParameter("wingId");

		FlatMarketPriceController fmpc = new FlatMarketPriceController();

		double taxPercentage=getGstCostPer();
		double gst=0.0;
		double stampDuty=0.0;
		double registeration=0.0;
		double total=0.0;
		long infrastructureCharges;
		long maintenanceCharges;
		long handlingCharges;
		Double aggreementAmount;
		long netAmount;

		if(projectId1.equals("Default") && buildingId1.equals("Default") && wingId1.equals("Default"))
		{
			projectId  = getLastprojectId(req, res);
			buildingId = getLastProjectbuildingId(req, res);
			wingId     = getLastProjectwingId(req, res);
		}
		else
		{
			projectId  = projectId1;
			buildingId = buildingId1;
			wingId     = wingId1;
		}

		List<Project> projectList = GetUserAssigenedProjectList(req,res);

		List<ProjectWing> wingList= new ArrayList<ProjectWing>();

		List<FlatMarketPrice> flat = new ArrayList<FlatMarketPrice>();

		List<Flat> flatList= new ArrayList<Flat>();

		FlatMarketPrice flatmarketprice = new FlatMarketPrice();
		int flag=0;

		Query query = new Query();

		query = new Query();
		Project projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectId)), Project.class);

		query = new Query();
		ProjectBuilding projectBuildingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(buildingId)), ProjectBuilding.class);

		query = new Query();
		wingList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId)), ProjectWing.class);
		infrastructureCharges=wingList.get(0).getInfrastructureCharges();
		maintenanceCharges=wingList.get(0).getMaintenanceCharges();
		handlingCharges=wingList.get(0).getHandlingCharges();

		query = new Query();
		flatList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("flatType").is("RK")), Flat.class);

		//for 1RK

		if(flatList.size()!=0)
		{

			try
			{

				flatmarketprice.setFlatType(flatList.get(0).getFlatType());
				flatmarketprice.setFlatArea(flatList.get(0).getFlatArea());
				flatmarketprice.setFlatAreawithLoadingInFt(flatList.get(0).getFlatAreawithLoadingInFt());
				flatmarketprice.setFlatCostwithotfloorise(flatList.get(0).getFlatCostwithotfloorise());
				flatmarketprice.setFloorRise(flatList.get(0).getFloorRise());
				flatmarketprice.setInfrastructureCharges(infrastructureCharges);
				aggreementAmount=(flatList.get(0).getFlatbasicCost()+infrastructureCharges);
				flatmarketprice.setAggreementAmount(aggreementAmount);

				stampDuty=((aggreementAmount)/100)*6;
				flatmarketprice.setStampDuty(stampDuty);

				if((aggreementAmount)>3000000)
				{
					registeration=30000.0;  
				}
				else
				{
					registeration=((aggreementAmount)/100)*1;
				}

				flatmarketprice.setRegisteration(registeration);
				flatmarketprice.setHandlingCharge(handlingCharges);
				gst=(aggreementAmount/100)*taxPercentage;

				flatmarketprice.setCgst((gst/2));
				flatmarketprice.setSgst((gst/2));

				total=aggreementAmount+gst+stampDuty+registeration+handlingCharges;
				flatmarketprice.setTotalCost(total);

				flatmarketprice.setMaintenanceCharges(maintenanceCharges);
				netAmount=(long) (total+maintenanceCharges);
				flatmarketprice.setNetAmount(netAmount);
				flat.add(flatmarketprice);


				for(int i=0;i<flatList.size();i++)
				{
					flag=0; 
					flatmarketprice = new FlatMarketPrice();

					for(int j=0;j<flat.size();j++)
					{
						if(Double.compare(flatList.get(i).getFlatArea(), flat.get(j).getFlatArea())==0)
						{
							flag=1;
							break;
						}
					}
					if(flag==0)
					{

						flatmarketprice.setFlatType(flatList.get(i).getFlatType());
						flatmarketprice.setFlatArea(flatList.get(i).getFlatArea());
						flatmarketprice.setFlatAreawithLoadingInFt(flatList.get(i).getFlatAreawithLoadingInFt());
						flatmarketprice.setFlatCostwithotfloorise(flatList.get(i).getFlatCostwithotfloorise());
						flatmarketprice.setFloorRise(flatList.get(i).getFloorRise());
						flatmarketprice.setInfrastructureCharges(infrastructureCharges);
						aggreementAmount=(flatList.get(i).getFlatbasicCost()+infrastructureCharges);
						flatmarketprice.setAggreementAmount(aggreementAmount);
						stampDuty=((aggreementAmount)/100)*6;
						flatmarketprice.setStampDuty(stampDuty);

						if((aggreementAmount)>3000000)
						{
							registeration=30000.0;  
						}
						else
						{
							registeration=((aggreementAmount)/100)*1;
						}

						flatmarketprice.setRegisteration(registeration);

						flatmarketprice.setHandlingCharge(handlingCharges);

						gst=(aggreementAmount/100)*taxPercentage;
						flatmarketprice.setCgst((gst/2));

						flatmarketprice.setSgst((gst/2));

						total=aggreementAmount+gst+stampDuty+registeration+handlingCharges;
						flatmarketprice.setTotalCost(total);

						flatmarketprice.setMaintenanceCharges(maintenanceCharges);
						netAmount=(long) (total+maintenanceCharges);
						flatmarketprice.setNetAmount(netAmount);
						flat.add(flatmarketprice);

					}
				}
			}
			catch(Exception e)
			{

			}
		}

		flatList.clear();

		//for 1BHK
		query = new Query();
		flatList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("flatType").is("1BHK")), Flat.class);

		if(flatList.size()!=0)
		{
			try
			{
				flatmarketprice.setFlatType(flatList.get(0).getFlatType());
				flatmarketprice.setFlatArea(flatList.get(0).getFlatArea());
				flatmarketprice.setFlatAreawithLoadingInFt(flatList.get(0).getFlatAreawithLoadingInFt());
				flatmarketprice.setFlatCostwithotfloorise(flatList.get(0).getFlatCostwithotfloorise());
				flatmarketprice.setFloorRise(flatList.get(0).getFloorRise());
				flatmarketprice.setInfrastructureCharges(infrastructureCharges);
				aggreementAmount=(flatList.get(0).getFlatbasicCost()+infrastructureCharges);
				flatmarketprice.setAggreementAmount(aggreementAmount);
				stampDuty=((aggreementAmount)/100)*6;
				flatmarketprice.setStampDuty(stampDuty);

				if((aggreementAmount)>3000000)
				{
					registeration=30000.0;  
				}
				else
				{
					registeration=((aggreementAmount)/100)*1;
				}

				flatmarketprice.setRegisteration(registeration);
				flatmarketprice.setHandlingCharge(handlingCharges);
				gst=(aggreementAmount/100)*taxPercentage;

				flatmarketprice.setCgst((gst/2));
				flatmarketprice.setSgst((gst/2));

				total=aggreementAmount+gst+stampDuty+registeration+handlingCharges;
				flatmarketprice.setTotalCost(total);

				flatmarketprice.setMaintenanceCharges(maintenanceCharges);
				netAmount=(long) (total+maintenanceCharges);
				flatmarketprice.setNetAmount(netAmount);
				flat.add(flatmarketprice);


				for(int i=0;i<flatList.size();i++)
				{
					flag=0; 
					flatmarketprice = new FlatMarketPrice();

					for(int j=0;j<flat.size();j++)
					{
						if(Double.compare(flatList.get(i).getFlatArea(), flat.get(j).getFlatArea())==0)
						{
							flag=1;
							break;
						}
					}
					if(flag==0)
					{

						flatmarketprice.setFlatType(flatList.get(i).getFlatType());
						flatmarketprice.setFlatArea(flatList.get(i).getFlatArea());
						flatmarketprice.setFlatAreawithLoadingInFt(flatList.get(i).getFlatAreawithLoadingInFt());
						flatmarketprice.setFlatCostwithotfloorise(flatList.get(i).getFlatCostwithotfloorise());
						flatmarketprice.setFloorRise(flatList.get(i).getFloorRise());

						flatmarketprice.setInfrastructureCharges(infrastructureCharges);
						aggreementAmount=(flatList.get(i).getFlatbasicCost()+infrastructureCharges);
						flatmarketprice.setAggreementAmount(aggreementAmount);
						stampDuty=((aggreementAmount)/100)*6;
						flatmarketprice.setStampDuty(stampDuty);

						if((aggreementAmount)>3000000)
						{
							registeration=30000.0;  
						}
						else
						{
							registeration=((aggreementAmount)/100)*1;
						}
						flatmarketprice.setRegisteration(registeration);

						flatmarketprice.setHandlingCharge(handlingCharges);

						gst=(aggreementAmount/100)*taxPercentage;
						flatmarketprice.setCgst((gst/2));

						flatmarketprice.setSgst((gst/2));

						total=aggreementAmount+gst+stampDuty+registeration+handlingCharges;
						flatmarketprice.setTotalCost(total);

						flatmarketprice.setMaintenanceCharges(maintenanceCharges);
						netAmount=(long) (total+maintenanceCharges);
						flatmarketprice.setNetAmount(netAmount);
						flat.add(flatmarketprice);

					}
				}
			} 
			catch(Exception e)
			{
				return null;
			}
		}
		flatList.clear();

		// For 2BHK
		query = new Query();
		flatList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("flatType").is("2BHK")), Flat.class);

		if(flatList.size()!=0)
		{
			try
			{
				flatmarketprice.setFlatType(flatList.get(0).getFlatType());
				flatmarketprice.setFlatArea(flatList.get(0).getFlatArea());
				flatmarketprice.setFlatAreawithLoadingInFt(flatList.get(0).getFlatAreawithLoadingInFt());
				flatmarketprice.setFlatCostwithotfloorise(flatList.get(0).getFlatCostwithotfloorise());
				flatmarketprice.setFloorRise(flatList.get(0).getFloorRise());
				flatmarketprice.setInfrastructureCharges(infrastructureCharges);

				aggreementAmount=(flatList.get(0).getFlatbasicCost()+infrastructureCharges);
				flatmarketprice.setAggreementAmount(aggreementAmount);

				stampDuty=((aggreementAmount)/100)*6;
				flatmarketprice.setStampDuty(stampDuty);

				if((aggreementAmount)>3000000)
				{
					registeration=30000.0;  
				}
				else
				{
					registeration=((aggreementAmount)/100)*1;
				}
				flatmarketprice.setRegisteration(registeration);

				flatmarketprice.setHandlingCharge(handlingCharges);

				gst=(aggreementAmount/100)*taxPercentage;
				flatmarketprice.setCgst((gst/2));

				flatmarketprice.setSgst((gst/2));

				total=aggreementAmount+gst+stampDuty+registeration+handlingCharges;
				flatmarketprice.setTotalCost(total);

				flatmarketprice.setMaintenanceCharges(maintenanceCharges);
				netAmount=(long) (total+maintenanceCharges);
				flatmarketprice.setNetAmount(netAmount);
				flat.add(flatmarketprice);

				for(int i=0;i<flatList.size();i++)
				{
					flag=0; 
					flatmarketprice = new FlatMarketPrice();

					for(int j=0;j<flat.size();j++)
					{
						if(Double.compare(flatList.get(i).getFlatArea(), flat.get(j).getFlatArea())==0)
						{
							flag=1;
							break;
						}
					}
					if(flag==0)
					{

						flatmarketprice.setFlatType(flatList.get(i).getFlatType());
						flatmarketprice.setFlatArea(flatList.get(i).getFlatArea());

						flatmarketprice.setFlatAreawithLoadingInFt(flatList.get(i).getFlatAreawithLoadingInFt());
						flatmarketprice.setFlatCostwithotfloorise(flatList.get(i).getFlatCostwithotfloorise());

						flatmarketprice.setFloorRise(flatList.get(i).getFloorRise());

						flatmarketprice.setInfrastructureCharges(infrastructureCharges);

						aggreementAmount=(flatList.get(i).getFlatbasicCost()+infrastructureCharges);
						flatmarketprice.setAggreementAmount(aggreementAmount);

						stampDuty=((aggreementAmount)/100)*6;
						flatmarketprice.setStampDuty(stampDuty);

						if((aggreementAmount)>3000000)
						{
							registeration=30000.0;  
						}
						else
						{
							registeration=((aggreementAmount)/100)*1;
						}
						flatmarketprice.setRegisteration(registeration);

						flatmarketprice.setHandlingCharge(handlingCharges);

						gst=(aggreementAmount/100)*taxPercentage;
						flatmarketprice.setCgst((gst/2));

						flatmarketprice.setSgst((gst/2));

						total=aggreementAmount+gst+stampDuty+registeration+handlingCharges;
						flatmarketprice.setTotalCost(total);

						flatmarketprice.setMaintenanceCharges(maintenanceCharges);
						netAmount=(long) (total+maintenanceCharges);
						flatmarketprice.setNetAmount(netAmount);
						flat.add(flatmarketprice);

					}
				}
			}
			catch(Exception e)
			{
				return null;
			}

		}
		flatList.clear();

		// For 3BHK
		query = new Query();
		flatList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("flatType").is("3BHK")), Flat.class);

		if(flatList.size()!=0)
		{
			try
			{
				flatmarketprice.setFlatType(flatList.get(0).getFlatType());
				flatmarketprice.setFlatArea(flatList.get(0).getFlatArea());

				flatmarketprice.setFlatAreawithLoadingInFt(flatList.get(0).getFlatAreawithLoadingInFt());

				flatmarketprice.setFlatCostwithotfloorise(flatList.get(0).getFlatCostwithotfloorise());

				flatmarketprice.setFloorRise(flatList.get(0).getFloorRise());

				flatmarketprice.setInfrastructureCharges(infrastructureCharges);

				aggreementAmount=(flatList.get(0).getFlatbasicCost()+infrastructureCharges);
				flatmarketprice.setAggreementAmount(aggreementAmount);

				stampDuty=((aggreementAmount)/100)*6;
				flatmarketprice.setStampDuty(stampDuty);

				if((aggreementAmount)>3000000)
				{
					registeration=30000.0;  
				}
				else
				{
					registeration=((aggreementAmount)/100)*1;
				}
				flatmarketprice.setRegisteration(registeration);

				flatmarketprice.setHandlingCharge(handlingCharges);

				gst=(aggreementAmount/100)*taxPercentage;
				flatmarketprice.setCgst((gst/2));

				flatmarketprice.setSgst((gst/2));

				total=aggreementAmount+gst+stampDuty+registeration+handlingCharges;
				flatmarketprice.setTotalCost(total);

				flatmarketprice.setMaintenanceCharges(maintenanceCharges);
				netAmount=(long) (total+maintenanceCharges);
				flatmarketprice.setNetAmount(netAmount);
				flat.add(flatmarketprice);

				for(int i=0;i<flatList.size();i++)
				{
					flag=0; 
					flatmarketprice = new FlatMarketPrice();

					for(int j=0;j<flat.size();j++)
					{
						if(Double.compare(flatList.get(i).getFlatArea(), flat.get(j).getFlatArea())==0)
						{
							flag=1;
							break;
						}
					}
					if(flag==0)
					{

						flatmarketprice.setFlatType(flatList.get(i).getFlatType());

						flatmarketprice.setFlatArea(flatList.get(i).getFlatArea());

						flatmarketprice.setFlatAreawithLoadingInFt(flatList.get(i).getFlatAreawithLoadingInFt());

						flatmarketprice.setFlatCostwithotfloorise(flatList.get(i).getFlatCostwithotfloorise());

						flatmarketprice.setFloorRise(flatList.get(i).getFloorRise());

						flatmarketprice.setInfrastructureCharges(infrastructureCharges);

						aggreementAmount=(flatList.get(i).getFlatbasicCost()+infrastructureCharges);
						flatmarketprice.setAggreementAmount(aggreementAmount);

						stampDuty=((aggreementAmount)/100)*6;
						flatmarketprice.setStampDuty(stampDuty);

						if((aggreementAmount)>3000000)
						{
							registeration=30000.0;  
						}
						else
						{
							registeration=((aggreementAmount)/100)*1;
						}
						flatmarketprice.setRegisteration(registeration);

						flatmarketprice.setHandlingCharge(handlingCharges);

						gst=(aggreementAmount/100)*taxPercentage;
						flatmarketprice.setCgst((gst/2));

						flatmarketprice.setSgst((gst/2));

						total=aggreementAmount+gst+stampDuty+registeration+handlingCharges;
						flatmarketprice.setTotalCost(total);

						flatmarketprice.setMaintenanceCharges(maintenanceCharges);
						netAmount=(long) (total+maintenanceCharges);
						flatmarketprice.setNetAmount(netAmount);
						flat.add(flatmarketprice);

					}
				}
			}
			catch(Exception e)
			{
				return null;
			}
		}
		flatList.clear();

		// for 4BHK

		query = new Query();
		flatList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("flatType").is("RK")), Flat.class);

		if(flatList.size()!=0)
		{
			try
			{
				flatmarketprice.setFlatType(flatList.get(0).getFlatType());

				flatmarketprice.setFlatArea(flatList.get(0).getFlatArea());

				flatmarketprice.setFlatAreawithLoadingInFt(flatList.get(0).getFlatAreawithLoadingInFt());

				flatmarketprice.setFlatCostwithotfloorise(flatList.get(0).getFlatCostwithotfloorise());

				flatmarketprice.setFloorRise(flatList.get(0).getFloorRise());

				flatmarketprice.setInfrastructureCharges(infrastructureCharges);

				aggreementAmount=(flatList.get(0).getFlatbasicCost()+infrastructureCharges);
				flatmarketprice.setAggreementAmount(aggreementAmount);

				stampDuty=((aggreementAmount)/100)*6;
				flatmarketprice.setStampDuty(stampDuty);

				if((aggreementAmount)>3000000)
				{
					registeration=30000.0;  
				}
				else
				{
					registeration=((aggreementAmount)/100)*1;
				}
				flatmarketprice.setRegisteration(registeration);

				flatmarketprice.setHandlingCharge(handlingCharges);

				gst=(aggreementAmount/100)*taxPercentage;
				flatmarketprice.setCgst((gst/2));

				flatmarketprice.setSgst((gst/2));

				total=aggreementAmount+gst+stampDuty+registeration+handlingCharges;
				flatmarketprice.setTotalCost(total);

				flatmarketprice.setMaintenanceCharges(maintenanceCharges);
				netAmount=(long) (total+maintenanceCharges);
				flatmarketprice.setNetAmount(netAmount);
				flat.add(flatmarketprice);


				for(int i=0;i<flatList.size();i++)
				{
					flag=0; 
					flatmarketprice = new FlatMarketPrice();

					for(int j=0;j<flat.size();j++)
					{
						if(Double.compare(flatList.get(i).getFlatArea(), flat.get(j).getFlatArea())==0)
						{
							flag=1;
							break;
						}
					}
					if(flag==0)
					{

						flatmarketprice.setFlatType(flatList.get(i).getFlatType());

						flatmarketprice.setFlatArea(flatList.get(i).getFlatArea());

						flatmarketprice.setFlatAreawithLoadingInFt(flatList.get(i).getFlatAreawithLoadingInFt());

						flatmarketprice.setFlatCostwithotfloorise(flatList.get(i).getFlatCostwithotfloorise());

						flatmarketprice.setFloorRise(flatList.get(i).getFloorRise());

						flatmarketprice.setInfrastructureCharges(infrastructureCharges);

						aggreementAmount=(flatList.get(i).getFlatbasicCost()+infrastructureCharges);
						flatmarketprice.setAggreementAmount(aggreementAmount);

						stampDuty=((aggreementAmount)/100)*6;
						flatmarketprice.setStampDuty(stampDuty);

						if((aggreementAmount)>3000000)
						{
							registeration=30000.0;  
						}
						else
						{
							registeration=((aggreementAmount)/100)*1;
						}
						flatmarketprice.setRegisteration(registeration);

						flatmarketprice.setHandlingCharge(handlingCharges);

						gst=(aggreementAmount/100)*taxPercentage;
						flatmarketprice.setCgst((gst/2));

						flatmarketprice.setSgst((gst/2));

						total=aggreementAmount+gst+stampDuty+registeration+handlingCharges;
						flatmarketprice.setTotalCost(total);

						flatmarketprice.setMaintenanceCharges(maintenanceCharges);
						netAmount=(long) (total+maintenanceCharges);
						flatmarketprice.setNetAmount(netAmount);
						flat.add(flatmarketprice);

					}
				}
			}
			catch(Exception e)
			{
			}
		}
		flatList.clear();

		try
		{
			int index=0;
			JasperPrint print;

			HashMap jmap = new HashMap();
			Collection c = new ArrayList();

			for(int i=0;i<flat.size();i++)
			{
				jmap = new HashMap();

				jmap.put("flatType",""+flat.get(i).getFlatType());
				jmap.put("flatArea",""+flat.get(i).getFlatArea());
				jmap.put("sqftArea",""+flat.get(i).getFlatAreawithLoadingInFt());
				jmap.put("basicRate",""+flat.get(i).getFlatCostwithotfloorise());
				jmap.put("infraCharge",""+flat.get(i).getInfrastructureCharges());
				jmap.put("aggAmt",""+flat.get(i).getAggreementAmount());
				jmap.put("stampDuty",""+flat.get(i).getStampDuty());
				jmap.put("regiCharge",""+flat.get(i).getRegisteration());
				jmap.put("handlingCharge",""+flat.get(i).getHandlingCharge());
				jmap.put("cGST",""+flat.get(i).getCgst());
				jmap.put("sGST",""+flat.get(i).getSgst());
				jmap.put("totalFlatCost",""+flat.get(i).getTotalCost());

				c.add(jmap);
				jmap = null;
			}

			String realPath =CommanController.GetLogoImagePath();
			//String realPath ="//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img";

			JRDataSource dataSource = new JRMapCollectionDataSource(c);
			Map<String, Object> parameterMap = new HashMap<String, Object>();

			Date date = new Date();  
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
			String strDate= formatter.format(date);

			//Booking Receipt Generation Code
			/*1*/ parameterMap.put("projectName", ""+projectDetails.getProjectName());
			/*2*/ parameterMap.put("buildingName",""+projectBuildingDetails.getBuildingName());
			/*3*/ parameterMap.put("wingName",""+wingList.get(0).getWingName());
			/*4*/ parameterMap.put("realPath",realPath);

			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/FlatMarketValueReport.jasper");

			print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(print, baos);

			byte[] contents = baos.toByteArray();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			String filename = "FlatMarketValueReport.pdf";

			JasperExportManager.exportReportToPdfStream(print, baos);

			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
			ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
			res.setHeader("Content-Disposition", "inline; filename=" + filename );
			return resp;
		}
		catch(Exception e)
		{
			return null;
		}

	}


	@ResponseBody
	@RequestMapping(value="/PrintFlatWiseMarketPrice", method=RequestMethod.GET)
	public ResponseEntity<byte[]> PrintFlatWiseMarketPrice(@RequestParam("projectId") String projectId, @RequestParam("buildingId") String buildingId, @RequestParam("wingId") String wingId, 
			@RequestParam("flatNumber") String flatNumber,
			@RequestParam("editRate") String editRate,
			@RequestParam("editflatArea") String editflatArea,
			@RequestParam("editflatbasicCost") String editflatbasicCost,
			@RequestParam("editInfrastructure") String editInfrastructure,
			@RequestParam("editaggValue") String editaggValue,
			@RequestParam("editStampDuty") String editStampDuty,
			@RequestParam("editRegistration") String editRegistration,
			@RequestParam("editLegalCharges") String editLegalCharges,
			@RequestParam("editTotalGst") String editTotalGst,
			@RequestParam("edittotalAmount") String edittotalAmount,
			@RequestParam("tdsAmount") String tdsAmount,
			ModelMap model, HttpServletResponse response)
	{


		int index=0;
		JasperPrint print;
		String projectAddress="",sno="";
		Double infrastructreCost = 0.00d;
		try 
		{	
			Query query = new Query();

			HashMap jmap = new HashMap();
			Collection c = new ArrayList();
			// String projectName = bookingList.get(0).getProjectId();

			query = new Query();
			List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)),Project.class);

			query = new Query();
			Company companyDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("companyId").is(projectDetails.get(0).getCompanyId())), Company.class);

			query = new Query();
			Flat flatDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("flatNumber").is(flatNumber)), Flat.class);

			//Company Address
			query =new Query();
			State companyStateDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("stateId").is(companyDetails.getStateId())), State.class);
			query =new Query();
			City companyCityDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("cityId").is(companyDetails.getCityId())), City.class);


			query =new Query();
			ProjectBuilding buildingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(buildingId)), ProjectBuilding.class);

			query =new Query();
			ProjectWing wingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(wingId)), ProjectWing.class);

			query =new Query();
			Floor floorDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("floorId").is(flatDetails.getFloorId())), Floor.class);

			query = new Query();
			List<PaymentScheduler> paymentschedulerList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId)), PaymentScheduler.class);

			//String realPath = "//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img";	 

			String realPath =CommanController.GetLogoImagePath();
			for(int i=0;i<paymentschedulerList.size();i++)
			{
				jmap = new HashMap();
				jmap.put("paymentDecription",""+paymentschedulerList.get(i).getPaymentDecription());
				jmap.put("percentage",""+paymentschedulerList.get(i).getPercentage());

				c.add(jmap);

				jmap = null;
			}

			JRDataSource dataSource = new JRMapCollectionDataSource(c);
			Map<String, Object> parameterMap = new HashMap<String, Object>();

			SimpleDateFormat formatter = new SimpleDateFormat("d/M/yyyy"); 
			Date date = new Date();  
			String todayDate= formatter.format(date);

			String project=""+projectDetails.get(0).getProjectName()+", "+buildingDetails.getBuildingName()+", "+wingDetails.getWingName();
			String flat=""+floorDetails.getFloortypeName()+", "+flatDetails.getFlatNumber();
			parameterMap.put("userName", "");

			parameterMap.put("companyAddress", ""+companyCityDetails.getCityName()+", "+companyStateDetails.getStateName());
			parameterMap.put("companyName", ""+companyDetails.getCompanyName());

			parameterMap.put("date", ""+todayDate);
			parameterMap.put("projectDetails", ""+project);
			parameterMap.put("flatDetails", ""+flat);
			parameterMap.put("flatType", ""+flatDetails.getFlatType());
			parameterMap.put("flatFacing", ""+flatDetails.getFlatfacingName());

			parameterMap.put("carpetArea", ""+flatDetails.getCarpetArea());
			parameterMap.put("openBalconyArea", ""+flatDetails.getTerraceArea());
			parameterMap.put("encloseBalconyArea", ""+flatDetails.getBalconyArea());
			parameterMap.put("dryTerraceArea", ""+flatDetails.getDryterraceArea());
			parameterMap.put("netFlatArea", ""+flatDetails.getFlatAreawithLoadingInM());
			parameterMap.put("flatArea", ""+editflatArea);

			parameterMap.put("rate", ""+editRate);
			parameterMap.put("flatCost", ""+editflatbasicCost);
			parameterMap.put("infrastructure", ""+editInfrastructure);
			parameterMap.put("aggValue", ""+editaggValue);
			parameterMap.put("stampDuty", ""+editStampDuty);
			parameterMap.put("registration", ""+editRegistration);
			parameterMap.put("legalCharges", ""+editLegalCharges);
			parameterMap.put("gstAmount", ""+editTotalGst);
			parameterMap.put("totalAmount", ""+edittotalAmount);


			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/flatMarketPrice.jasper");

			print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(print, baos);

			byte[] contents = baos.toByteArray();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			String filename = "FlatMarketPrice.pdf";

			JasperExportManager.exportReportToPdfStream(print, baos);


			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

			ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
			response.setHeader("Content-Disposition", "inline; filename=" + filename );

			return resp;					    
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			//redirectAttributes.addFlashAttribute("flagemail", 0);
		}

		return null;
	}

}

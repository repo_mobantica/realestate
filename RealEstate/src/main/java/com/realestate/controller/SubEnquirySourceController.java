package com.realestate.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Agent;
import com.realestate.bean.EnquirySource;
import com.realestate.bean.SubEnquirySource;
import com.realestate.repository.EnquirySourceRepository;
import com.realestate.repository.SubEnquirySourceRepository;
@Controller
@RequestMapping("/")
public class SubEnquirySourceController {

	@Autowired
	EnquirySourceRepository enquirysourceRepository;
	@Autowired
	SubEnquirySourceRepository subEnquirysourcerepository;
	@Autowired
	MongoTemplate mongoTemplate;
	String subEnqCode;

	private String SunEnqCode()
	{
		long Count = subEnquirysourcerepository.count();

		if(Count<10)
		{
			subEnqCode = "SES000"+(Count+1);
		}
		else if((Count>=10) && (Count<100))
		{
			subEnqCode = "SES00"+(Count+1);
		}
		else if((Count>=100) && (Count<1000))
		{
			subEnqCode = "SES0"+(Count+1);
		}
		else 
		{
			subEnqCode = "SES"+(Count+1);
		}

		return subEnqCode;
	}


	private List<EnquirySource> findAllEnquirySourceName()
	{
		List<EnquirySource> enquirysourceList;

		enquirysourceList = enquirysourceRepository.findAll(new Sort(Sort.Direction.ASC,"enquirysourceName"));
		return enquirysourceList;
	}

	@RequestMapping("/SubEnquirySourceMaster")
	public String EnquirySourceMaster(ModelMap model)
	{
		try {
			List<EnquirySource> enquirysourceList = enquirysourceRepository.findAll();
			List<SubEnquirySource> subenquirysourceList = subEnquirysourcerepository.findAll();
			Query query = new Query();

			for(int i=0;i<subenquirysourceList.size();i++)
			{
				try
				{
					query = new Query();
					List<EnquirySource> subenquirysourceDetails = mongoTemplate.find(query.addCriteria(Criteria.where("enquirysourceId").is(subenquirysourceList.get(i).getEnquirysourceId())),EnquirySource.class);
					subenquirysourceList.get(i).setEnquirysourceId(subenquirysourceDetails.get(0).getEnquirysourceName());
				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}

			model.addAttribute("subenquirysourceList",subenquirysourceList);
			model.addAttribute("enquirysourceList",enquirysourceList);
			return "SubEnquirySourceMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/AddSubEnquirySource")
	public String AddSubEnquirySource(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			model.addAttribute("enquirysourceList",findAllEnquirySourceName());	
			model.addAttribute("subEnqCode",SunEnqCode());
			return "AddSubEnquirySource";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping(value = "/AddSubEnquirySource", method = RequestMethod.POST)
	public String storeSave(@ModelAttribute SubEnquirySource subenquirysource, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				subenquirysource = new SubEnquirySource(subenquirysource.getSubsourceId(), subenquirysource.getEnquirysourceId(),subenquirysource.getSubenquirysourceName(),subenquirysource.getCreationDate(), subenquirysource.getUpdateDate(), subenquirysource.getUserName());
				subEnquirysourcerepository.save(subenquirysource);
				model.addAttribute("enquirysourceStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("enquirysourceStatus","Fail");
			}
			model.addAttribute("enquirysourceList",findAllEnquirySourceName());	
			model.addAttribute("subEnqCode",SunEnqCode());
			return "AddSubEnquirySource";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping("/EditSubEnquirySource")
	public String EditSubEnquirySource(@RequestParam("subsourceId") String subsourceId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<SubEnquirySource> subenquirysourceDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subsourceId").is(subsourceId)),SubEnquirySource.class);

			query = new Query();
			List<EnquirySource> enquirysourceDetails = mongoTemplate.find(query.addCriteria(Criteria.where("enquirysourceId").is(subenquirysourceDetails.get(0).getEnquirysourceId())),EnquirySource.class);

			model.addAttribute("enquirysourceName", enquirysourceDetails.get(0).getEnquirysourceName());
			model.addAttribute("subenquirysourceDetails", subenquirysourceDetails);
			model.addAttribute("enquirysourceList",findAllEnquirySourceName());	
			return "EditSubEnquirySource";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value = "/EditSubEnquirySource", method = RequestMethod.POST)
	public String EditSubEnquirySourceSave(@ModelAttribute SubEnquirySource subenquirysource, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				subenquirysource = new SubEnquirySource(subenquirysource.getSubsourceId(), subenquirysource.getEnquirysourceId(),subenquirysource.getSubenquirysourceName(),subenquirysource.getCreationDate(), subenquirysource.getUpdateDate(), subenquirysource.getUserName());
				subEnquirysourcerepository.save(subenquirysource);
				model.addAttribute("enquirysourceStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("enquirysourceStatus","Fail");
			}
			List<EnquirySource> enquirysourceList = enquirysourceRepository.findAll();
			List<SubEnquirySource> subenquirysourceList = subEnquirysourcerepository.findAll();

			Query query =new Query();
			for(int i=0;i<subenquirysourceList.size();i++)
			{
				try
				{
					query = new Query();
					List<EnquirySource> subenquirysourceDetails = mongoTemplate.find(query.addCriteria(Criteria.where("enquirysourceId").is(subenquirysourceList.get(i).getEnquirysourceId())),EnquirySource.class);
					subenquirysourceList.get(i).setEnquirysourceId(subenquirysourceDetails.get(0).getEnquirysourceName());
				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}

			model.addAttribute("subenquirysourceList",subenquirysourceList);
			model.addAttribute("enquirysourceList",enquirysourceList);
			return "SubEnquirySourceMaster";

		}catch (Exception e) {
			return "login";
		}
	}


	@ResponseBody
	@RequestMapping("/getSubEnquirySourceList")
	public List<SubEnquirySource> GetSubEnquirySourceList(@RequestParam("enquirysourceId") String enquirysourceId)
	{
		Query query =new Query();
		List<SubEnquirySource> subenquirysourceList = mongoTemplate.find(query.addCriteria(Criteria.where("enquirysourceId").is(enquirysourceId)), SubEnquirySource.class);

		for(int i=0;i<subenquirysourceList.size();i++)
		{
			try
			{
				query = new Query();
				List<EnquirySource> subenquirysourceDetails = mongoTemplate.find(query.addCriteria(Criteria.where("enquirysourceId").is(subenquirysourceList.get(i).getEnquirysourceId())),EnquirySource.class);
				subenquirysourceList.get(i).setEnquirysourceId(subenquirysourceDetails.get(0).getEnquirysourceName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		return subenquirysourceList;
	}


}

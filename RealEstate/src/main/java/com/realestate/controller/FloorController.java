package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Floor;
import com.realestate.bean.Login;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.FloorRepository;
import com.realestate.repository.ProjectRepository;

@Controller
@RequestMapping("/")
public class FloorController 
{
	@Autowired
	FloorRepository floorRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	MongoTemplate mongoTemplate;
	//Code geration for floor code
	String floorCode;
	private String FloorCode()
	{
		long floorCount = floorRepository.count();

		if(floorCount<10)
		{
			floorCode = "FL000"+(floorCount+1);
		}
		else if((floorCount>=10) && (floorCount<100))
		{
			floorCode = "FL00"+(floorCount+1);
		}
		else if((floorCount>=100) && (floorCount<1000))
		{
			floorCode = "FL0"+(floorCount+1);
		}
		else
		{
			floorCode = "FL"+(floorCount+1);
		}
		return floorCode;
	}


	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	//Reqest Mapping For Add Floor
	@RequestMapping("/AddFloor")
	public String addFloor(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			model.addAttribute("floorCode",FloorCode());

			List<Project> projectList = GetUserAssigenedProjectList(req,res); 

			model.addAttribute("projectList",projectList);

			return "AddFloor";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/AddFloor", method=RequestMethod.POST)
	public String floorSave(@ModelAttribute Floor floor, Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				floor = new Floor(floor.getFloorId(), floor.getProjectId(), floor.getBuildingId(),  floor.getWingId(), floor.getFloortypeName(), floor.getFloorRise(),floor.getFloorzoneType(), floor.getNumberofFlats(), floor.getNumberofShops(), floor.getNumberofOpenParking(), floor.getNumberofCloseParking(), floor.getCreationDate(), floor.getUpdateDate(), floor.getUserName());
				floorRepository.save(floor);

				model.addAttribute("floorStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("floorStatus","Fail");
			}

			List<Project> projectList = GetUserAssigenedProjectList(req,res); 

			model.addAttribute("projectList",projectList);

			model.addAttribute("floorCode",FloorCode());

			return "AddFloor";

		}catch (Exception e) {
			return "login";
		}
	}

	// for floor master
	@RequestMapping("/FloorMaster")
	public String floorMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<Floor> floorList = floorRepository.findAll(); 
			List<Project> projectList = GetUserAssigenedProjectList(req,res); 

			Query query = new Query();
			for(int i=0;i<floorList.size();i++)
			{
				try
				{
					query = new Query();
					List<Project>  projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(floorList.get(i).getProjectId())), Project.class);
					floorList.get(i).setProjectId(projectDetails.get(0).getProjectName());
					query = new Query();
					List<ProjectBuilding>  buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(floorList.get(i).getBuildingId())), ProjectBuilding.class);
					floorList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
					query = new Query();
					List<ProjectWing>  wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(floorList.get(i).getWingId())), ProjectWing.class);
					floorList.get(i).setWingId(wingDetails.get(0).getWingName());
				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}

			model.addAttribute("projectList", projectList);
			model.addAttribute("floorList",floorList);
			return "FloorMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping(value="/getProjectWiseFloorList",method=RequestMethod.POST)
	public List<Floor> FloorListByProject(@RequestParam("projectId") String projectId, HttpSession session)
	{
		List<Floor> floorList= new ArrayList<Floor>();

		Query query = new Query();
		floorList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)), Floor.class);

		for(int i=0;i<floorList.size();i++)
		{
			try
			{
				query = new Query();
				List<Project>  projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(floorList.get(i).getProjectId())), Project.class);
				floorList.get(i).setProjectId(projectDetails.get(0).getProjectName());
				query = new Query();
				List<ProjectBuilding>  buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(floorList.get(i).getBuildingId())), ProjectBuilding.class);
				floorList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
				query = new Query();
				List<ProjectWing>  wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(floorList.get(i).getWingId())), ProjectWing.class);
				floorList.get(i).setWingId(wingDetails.get(0).getWingName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		return floorList;
	}
	@ResponseBody
	@RequestMapping(value="/getBuildingWiseFloorList",method=RequestMethod.POST)
	public List<Floor> FloorListByBuilding(@RequestParam("buildingId") String buildingId, @RequestParam("projectId") String projectId,  HttpSession session)
	{
		List<Floor> floorList= new ArrayList<Floor>();

		Query query = new Query();
		floorList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId)), Floor.class);

		for(int i=0;i<floorList.size();i++)
		{
			try
			{
				query = new Query();
				List<Project>  projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(floorList.get(i).getProjectId())), Project.class);
				floorList.get(i).setProjectId(projectDetails.get(0).getProjectName());
				query = new Query();
				List<ProjectBuilding>  buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(floorList.get(i).getBuildingId())), ProjectBuilding.class);
				floorList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
				query = new Query();
				List<ProjectWing>  wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(floorList.get(i).getWingId())), ProjectWing.class);
				floorList.get(i).setWingId(wingDetails.get(0).getWingName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		return floorList;
	}

	@ResponseBody
	@RequestMapping(value="/getAllWingWiseFloorList",method=RequestMethod.POST)
	public List<Floor> FloorListByWingName( @RequestParam("wingId") String wingId, @RequestParam("buildingId") String buildingId, @RequestParam("projectId") String projectId, HttpSession session)
	{
		List<Floor> floorList= new ArrayList<Floor>();

		Query query = new Query();
		floorList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId)), Floor.class);

		for(int i=0;i<floorList.size();i++)
		{
			try
			{
				query = new Query();
				List<Project>  projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(floorList.get(i).getProjectId())), Project.class);
				floorList.get(i).setProjectId(projectDetails.get(0).getProjectName());
				query = new Query();
				List<ProjectBuilding>  buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(floorList.get(i).getBuildingId())), ProjectBuilding.class);
				floorList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
				query = new Query();
				List<ProjectWing>  wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(floorList.get(i).getWingId())), ProjectWing.class);
				floorList.get(i).setWingId(wingDetails.get(0).getWingName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}


		return floorList;
	}

	@ResponseBody
	@RequestMapping(value="/getFloorNameWiseFloorList",method=RequestMethod.POST)
	public List<Floor> FloorListByFloortype(@RequestParam("floorId") String floorId, @RequestParam("wingId") String wingId, @RequestParam("buildingId") String buildingId, @RequestParam("projectId") String projectId, HttpSession session)
	{
		List<Floor> floorList= new ArrayList<Floor>();

		Query query = new Query();
		floorList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("floorId").is(floorId)), Floor.class);

		for(int i=0;i<floorList.size();i++)
		{
			try
			{
				query = new Query();
				List<Project>  projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(floorList.get(i).getProjectId())), Project.class);
				floorList.get(i).setProjectId(projectDetails.get(0).getProjectName());
				query = new Query();
				List<ProjectBuilding>  buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(floorList.get(i).getBuildingId())), ProjectBuilding.class);
				floorList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
				query = new Query();
				List<ProjectWing>  wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(floorList.get(i).getWingId())), ProjectWing.class);
				floorList.get(i).setWingId(wingDetails.get(0).getWingName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}


		return floorList;
	}

	@ResponseBody
	@RequestMapping(value="/getWingList",method=RequestMethod.POST)
	public List<ProjectWing> WingList(@RequestParam("projectId") String projectId,@RequestParam("buildingId") String buildingId, HttpSession session)
	{

		List<ProjectWing> wingList= new ArrayList<ProjectWing>();

		Query query = new Query();
		wingList = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(buildingId).and("projectId").is(projectId)), ProjectWing.class);
		return wingList;
	}

	@ResponseBody
	@RequestMapping(value="/getFloorList",method=RequestMethod.POST)
	public List<ProjectWing> floorList(@RequestParam("projectId") String projectId,@RequestParam("buildingId") String buildingId,@RequestParam("wingId") String wingId, HttpSession session)
	{

		List<ProjectWing> wingList= new ArrayList<ProjectWing>();

		Query query = new Query();
		wingList = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(buildingId).and("projectId").is(projectId).and("wingId").is(wingId)), ProjectWing.class);
		return wingList;
	}

	@ResponseBody
	@RequestMapping("/getuniquefloorList")
	public List<Floor> AllUniqueFloorList(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		List<Floor> floorList= floorRepository.findAll();

		return floorList;
	}

	@RequestMapping("/EditFloor")
	public String EditFloor(@RequestParam("floorId") String floorId, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			Query query = new Query();
			List<Floor> floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("floorId").is(floorId)),Floor.class);
			query = new Query();
			List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(floorDetails.get(0).getProjectId())),Project.class);

			query = new Query();
			List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(floorDetails.get(0).getBuildingId())),ProjectBuilding.class);

			query = new Query();
			List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(floorDetails.get(0).getWingId())),ProjectWing.class);

			List<Project> projectList = GetUserAssigenedProjectList(req,res); 

			model.addAttribute("projectList",projectList);

			model.addAttribute("projectName", projectDetails.get(0).getProjectName());
			model.addAttribute("buildingName", buildingDetails.get(0).getBuildingName());
			model.addAttribute("wingName", wingDetails.get(0).getWingName());

			model.addAttribute("floorDetails", floorDetails);
			return "EditFloor";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/EditFloor", method=RequestMethod.POST)
	public String EditFloorPostMethod(@ModelAttribute Floor floor, Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				floor = new Floor(floor.getFloorId(),floor.getProjectId(), floor.getBuildingId(),  floor.getWingId(), floor.getFloortypeName(), floor.getFloorRise(),floor.getFloorzoneType(), floor.getNumberofFlats(), floor.getNumberofShops(), floor.getNumberofOpenParking(), floor.getNumberofCloseParking(), floor.getCreationDate(), floor.getUpdateDate(), floor.getUserName());
				floorRepository.save(floor);

				model.addAttribute("floorStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("floorStatus","Fail");
			}

			List<Floor> floorList = floorRepository.findAll(); 

			List<Project> projectList = GetUserAssigenedProjectList(req,res);

			model.addAttribute("projectList", projectList);

			model.addAttribute("floorList",floorList);

			return "FloorMaster";

		}catch (Exception e) {
			return "login";
		}
	}

}

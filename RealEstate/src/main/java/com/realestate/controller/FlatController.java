package com.realestate.controller;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Booking;
import com.realestate.bean.Flat;
import com.realestate.bean.Floor;
import com.realestate.bean.Login;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.FlatRepository;
import com.realestate.repository.FloorRepository;
import com.realestate.repository.ProjectRepository;

@Controller
@RequestMapping("/")
public class FlatController {

	@Autowired
	FlatRepository flatRepository;
	@Autowired
	FloorRepository floorRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	String flatCode;
	private String FlatCode()
	{
		long cnt  = flatRepository.count();
		if(cnt<10)
		{
			flatCode = "FL000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			flatCode = "FL00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			flatCode = "FL0"+(cnt+1);
		}
		else
		{
			flatCode = "FL"+(cnt+1);
		}
		return flatCode;
	}


	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping("/AddFlat")
	public String addFlat(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			model.addAttribute("flatCode",FlatCode());

			List<Project> projectList = GetUserAssigenedProjectList(req,res); 

			model.addAttribute("projectList",projectList);

			return "AddFlat";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping(value = "/AddFlat", method = RequestMethod.POST)
	public String flatSave(@ModelAttribute Flat flat, Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				flat = new Flat(flat.getFlatId(), flat.getFlatNumber(), flat.getProjectId(),flat.getBuildingId(),flat.getWingId(), flat.getFloorId(), flat.getFlatfacingName(),flat.getFlatType(), flat.getCarpetArea(), flat.getTerraceArea(),flat.getBalconyArea(), flat.getDryterraceArea(),flat.getFlatArea(), flat.getFlatCostwithotfloorise(),flat.getFloorRise(), flat.getFlatCost(),flat.getFlatminimumCost(),flat.getFlatbasicCost(),flat.getLoading(),flat.getLoadingpercentage(),flat.getFlatAreawithLoadingInM(),flat.getFlatAreawithLoadingInFt(),flat.getFlatstatus(),flat.getCreationDate(), flat.getUpdateDate(), flat.getUserName());
				flatRepository.save(flat);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}

			List<Project> projectList = GetUserAssigenedProjectList(req,res); 

			model.addAttribute("projectList",projectList);

			model.addAttribute("flatCode",FlatCode());

			return "AddFlat";

		}catch (Exception e) {
			return "login";
		}
	}


	@ResponseBody
	@RequestMapping("/SearchFlatByNumber")
	public List<Flat> SearchFlatByNumber(@RequestParam("flatNumber") String flatNumber)
	{
		Query query = new Query();
		List<Flat> flatDetails = mongoTemplate.find(query.addCriteria(Criteria.where("flatNumber").regex("^"+flatNumber+".*","i")),Flat.class);
		return flatDetails;
	}

	// for floor master
	@RequestMapping("/FlatMaster")
	public String flatMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<Flat> flatList = flatRepository.findAll(); 
			Query query = new Query();
			for(int i=0;i<flatList.size();i++)
			{
				try
				{
					query = new Query();
					List<Project>  projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(flatList.get(i).getProjectId())), Project.class);
					flatList.get(i).setProjectId(projectDetails.get(0).getProjectName());

					query = new Query();
					List<ProjectBuilding>  buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(flatList.get(i).getBuildingId())), ProjectBuilding.class); 
					flatList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
					query = new Query();
					List<ProjectWing>  wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(flatList.get(i).getWingId())), ProjectWing.class); 
					flatList.get(i).setWingId(wingDetails.get(0).getWingName());
					query = new Query();
					List<Floor>  floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("floorId").is(flatList.get(i).getFloorId())), Floor.class); 
					flatList.get(i).setFloorId(floorDetails.get(0).getFloortypeName());
				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}

			List<Project> projectList = GetUserAssigenedProjectList(req,res); 

			model.addAttribute("projectList",projectList);

			model.addAttribute("flatList",flatList);
			return "FlatMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping(value="/getProjectWiseFlatList",method=RequestMethod.POST)
	public List<Flat> FlatListByProject(@RequestParam("projectId") String projectId, HttpSession session)
	{
		List<Flat> flatList= new ArrayList<Flat>();

		Query query = new Query();
		flatList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)), Flat.class);
		/*
		 	     if(flatList.size()!=0)
		 		{
		 		  for(int i=0; i<flatList.size();i++)
		 		  {
		 			 if(flatList.get(i).getFlatstatus().equals("Aggreement Completed") || flatList.get(i).getFlatstatus().equals("Booking Completed"))
		 			 {
		 				 query = new Query();
		 				 List<Booking> bookingList = mongoTemplate.find(query.addCriteria(Criteria.where("flatNumber").is(flatList.get(i).getFlatNumber()).and("projectName").is(flatList.get(i).getProjectId()).and("buildingName").is(flatList.get(i).getBuildingId()).and("wingName").is(flatList.get(i).getWingId()).and("floortypeName").is(flatList.get(i).getFloorId())), Booking.class);

		 				 if(bookingList.size()!=0)
		 				 {
		 					 flatList.get(i).setFlatbasicCost(bookingList.get(0).getFlatbasicCost());	 
		 				 }
		 			  }
		 		   }
		 		 }
		 */
		for(int i=0;i<flatList.size();i++)
		{
			try
			{
				query = new Query();
				List<Project>  projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(flatList.get(i).getProjectId())), Project.class);
				flatList.get(i).setProjectId(projectDetails.get(0).getProjectName());

				query = new Query();
				List<ProjectBuilding>  buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(flatList.get(i).getBuildingId())), ProjectBuilding.class); 
				flatList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
				query = new Query();
				List<ProjectWing>  wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(flatList.get(i).getWingId())), ProjectWing.class); 
				flatList.get(i).setWingId(wingDetails.get(0).getWingName());
				query = new Query();
				List<Floor>  floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("floorId").is(flatList.get(i).getFloorId())), Floor.class); 
				flatList.get(i).setFloorId(floorDetails.get(0).getFloortypeName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}


		return flatList;
	}

	@ResponseBody
	@RequestMapping(value="/getBuildingWiseFlatList",method=RequestMethod.POST)
	public List<Flat> FlatListByBuilding(@RequestParam("buildingId") String buildingId, @RequestParam("projectId") String projectId, HttpSession session)
	{
		List<Flat> flatList= new ArrayList<Flat>();

		Query query = new Query();
		flatList = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(buildingId).and("projectId").is(projectId)), Flat.class);
		/*
		   	   if(flatList.size()!=0)
				{
				  for(int i=0; i<flatList.size();i++)
				  {
					 if(flatList.get(i).getFlatstatus().equals("Aggreement Completed") || flatList.get(i).getFlatstatus().equals("Booking Completed"))
					 {
						 query = new Query();
						 List<Booking> bookingList = mongoTemplate.find(query.addCriteria(Criteria.where("flatNumber").is(flatList.get(i).getFlatNumber()).and("projectName").is(flatList.get(i).getProjectName()).and("buildingName").is(flatList.get(i).getBuildingName()).and("wingName").is(flatList.get(i).getWingName()).and("floortypeName").is(flatList.get(i).getFloortypeName())), Booking.class);

						 if(bookingList.size()!=0)
						 {
							 flatList.get(i).setFlatbasicCost(bookingList.get(0).getFlatbasicCost());	 
						 }
					  }
				   }
				 }
		 */

		for(int i=0;i<flatList.size();i++)
		{
			try
			{
				query = new Query();
				List<Project>  projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(flatList.get(i).getProjectId())), Project.class);
				flatList.get(i).setProjectId(projectDetails.get(0).getProjectName());

				query = new Query();
				List<ProjectBuilding>  buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(flatList.get(i).getBuildingId())), ProjectBuilding.class); 
				flatList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
				query = new Query();
				List<ProjectWing>  wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(flatList.get(i).getWingId())), ProjectWing.class); 
				flatList.get(i).setWingId(wingDetails.get(0).getWingName());
				query = new Query();
				List<Floor>  floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("floorId").is(flatList.get(i).getFloorId())), Floor.class); 
				flatList.get(i).setFloorId(floorDetails.get(0).getFloortypeName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		return flatList;
	}


	@ResponseBody
	@RequestMapping(value="/getWingWiseFlatList",method=RequestMethod.POST)
	public List<Flat> FlatListByWing(@RequestParam("wingId") String wingId, @RequestParam("buildingId") String buildingId, @RequestParam("projectId") String projectId, HttpSession session)
	{
		List<Flat> flatList= new ArrayList<Flat>();

		Query query = new Query();
		flatList = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(buildingId).and("projectId").is(projectId).and("wingId").is(wingId)), Flat.class);
		/*
		   	   if(flatList.size()!=0)
				{
				  for(int i=0; i<flatList.size();i++)
				  {
					 if(flatList.get(i).getFlatstatus().equals("Aggreement Completed") || flatList.get(i).getFlatstatus().equals("Booking Completed"))
					 {
						 query = new Query();
						 List<Booking> bookingList = mongoTemplate.find(query.addCriteria(Criteria.where("flatNumber").is(flatList.get(i).getFlatNumber()).and("projectName").is(flatList.get(i).getProjectName()).and("buildingName").is(flatList.get(i).getBuildingName()).and("wingName").is(flatList.get(i).getWingName()).and("floortypeName").is(flatList.get(i).getFloortypeName())), Booking.class);

						 if(bookingList.size()!=0)
						 {
							 flatList.get(i).setFlatbasicCost(bookingList.get(0).getFlatbasicCost());	 
						 }
					  }
				   }
				 }
		 */
		for(int i=0;i<flatList.size();i++)
		{
			try
			{
				query = new Query();
				List<Project>  projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(flatList.get(i).getProjectId())), Project.class);
				flatList.get(i).setProjectId(projectDetails.get(0).getProjectName());

				query = new Query();
				List<ProjectBuilding>  buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(flatList.get(i).getBuildingId())), ProjectBuilding.class); 
				flatList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
				query = new Query();
				List<ProjectWing>  wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(flatList.get(i).getWingId())), ProjectWing.class); 
				flatList.get(i).setWingId(wingDetails.get(0).getWingName());
				query = new Query();
				List<Floor>  floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("floorId").is(flatList.get(i).getFloorId())), Floor.class); 
				flatList.get(i).setFloorId(floorDetails.get(0).getFloortypeName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		return flatList;
	}

	@ResponseBody
	@RequestMapping(value="/getFloorWiseAllFlatList",method=RequestMethod.POST)
	public List<Flat> FlatListByFloor(@RequestParam("floorId") String floorId, @RequestParam("wingId") String wingId, @RequestParam("buildingId") String buildingId, @RequestParam("projectId") String projectId, HttpSession session)
	{
		List<Flat> flatList= new ArrayList<Flat>();

		Query query = new Query();
		flatList = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(buildingId).and("projectId").is(projectId).and("wingId").is(wingId).and("floorId").is(floorId)), Flat.class);
		/*	  
		   	   if(flatList.size()!=0)
				{
				  for(int i=0; i<flatList.size();i++)
				  {
					 if(flatList.get(i).getFlatstatus().equals("Aggreement Completed") || flatList.get(i).getFlatstatus().equals("Booking Completed"))
					 {
						 query = new Query();
						 List<Booking> bookingList = mongoTemplate.find(query.addCriteria(Criteria.where("flatNumber").is(flatList.get(i).getFlatNumber()).and("projectName").is(flatList.get(i).getProjectName()).and("buildingName").is(flatList.get(i).getBuildingName()).and("wingName").is(flatList.get(i).getWingName()).and("floortypeName").is(flatList.get(i).getFloortypeName())), Booking.class);

						 if(bookingList.size()!=0)
						 {
							 flatList.get(i).setFlatbasicCost(bookingList.get(0).getFlatbasicCost());	 
						 }
					  }
				   }
				 }
		 */

		for(int i=0;i<flatList.size();i++)
		{
			try
			{
				query = new Query();
				List<Project>  projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(flatList.get(i).getProjectId())), Project.class);
				flatList.get(i).setProjectId(projectDetails.get(0).getProjectName());

				query = new Query();
				List<ProjectBuilding>  buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(flatList.get(i).getBuildingId())), ProjectBuilding.class); 
				flatList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
				query = new Query();
				List<ProjectWing>  wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(flatList.get(i).getWingId())), ProjectWing.class); 
				flatList.get(i).setWingId(wingDetails.get(0).getWingName());
				query = new Query();
				List<Floor>  floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("floorId").is(flatList.get(i).getFloorId())), Floor.class); 
				flatList.get(i).setFloorId(floorDetails.get(0).getFloortypeName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}
		return flatList;
	}

	@ResponseBody
	@RequestMapping(value="/getFloorTypeWiseFlatList",method=RequestMethod.POST)
	public List<Flat> FlatListByFloortype(@RequestParam("floortypeName") String floortypeName, HttpSession session)
	{
		List<Flat> flatList= new ArrayList<Flat>();

		Query query = new Query();
		flatList = mongoTemplate.find(query.addCriteria(Criteria.where("floortypeName").is(floortypeName)), Flat.class);

		return flatList;
	}

	@ResponseBody
	@RequestMapping(value="/getFlatTypeWiseFlatList",method=RequestMethod.POST)
	public List<Flat> FlatListByFlattype(@RequestParam("flatType") String flatType, HttpSession session)
	{
		List<Flat> flatList= new ArrayList<Flat>();

		Query query = new Query();
		flatList = mongoTemplate.find(query.addCriteria(Criteria.where("flatType").is(flatType)), Flat.class);

		return flatList;
	}

	@ResponseBody
	@RequestMapping(value="/getwingfloorNameList",method=RequestMethod.POST)
	public List<Floor> wingFloorNameList(@RequestParam("wingId") String wingId, @RequestParam("buildingId") String buildingId, @RequestParam("projectId") String projectId, HttpSession session)
	{
		List<Floor> floortypeList= new ArrayList<Floor>();
		// .and("floorzoneType").is("Flats")
		Query query = new Query();
		floortypeList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId)), Floor.class);
		return floortypeList;
	}

	@ResponseBody
	@RequestMapping("/getuniqueflatnumberList")
	public List<Flat> AlluniqueFlatNumberList(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		List<Flat> flatList= flatRepository.findAll();
		return flatList;
	}

	@RequestMapping("/EditFlat")
	public String EditFlat(@RequestParam("flatId") String flatId, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			Query query = new Query();
			List<Flat> flatDetails = mongoTemplate.find(query.addCriteria(Criteria.where("flatId").is(flatId)), Flat.class);
			query = new Query();
			List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(flatDetails.get(0).getProjectId())), Project.class);

			query = new Query();
			List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(flatDetails.get(0).getBuildingId())), ProjectBuilding.class);

			query = new Query();
			List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(flatDetails.get(0).getWingId())), ProjectWing.class);

			query = new Query();
			List<Floor> floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("floorId").is(flatDetails.get(0).getFloorId())), Floor.class);


			List<Project> projectList = GetUserAssigenedProjectList(req,res); 

			model.addAttribute("projectList",projectList);

			model.addAttribute("projectName",projectDetails.get(0).getProjectName());
			model.addAttribute("buildingName",buildingDetails.get(0).getBuildingName());
			model.addAttribute("wingName",wingDetails.get(0).getWingName());
			model.addAttribute("floortypeName",floorDetails.get(0).getFloortypeName());
			model.addAttribute("flatDetails",flatDetails);

			return "EditFlat";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value = "/EditFlat", method = RequestMethod.POST)
	public String flatEdit(@ModelAttribute Flat flat, Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				flat = new Flat(flat.getFlatId(), flat.getFlatNumber(), flat.getProjectId(),flat.getBuildingId(),flat.getWingId(), flat.getFloorId(), flat.getFlatfacingName(),flat.getFlatType(), flat.getCarpetArea(), flat.getTerraceArea(),flat.getBalconyArea(), flat.getDryterraceArea(),flat.getFlatArea(), flat.getFlatCostwithotfloorise(),flat.getFloorRise(), flat.getFlatCost(),flat.getFlatminimumCost(),flat.getFlatbasicCost(),flat.getLoading(),flat.getLoadingpercentage(),flat.getFlatAreawithLoadingInM(),flat.getFlatAreawithLoadingInFt(),flat.getFlatstatus(),flat.getCreationDate(), flat.getUpdateDate(), flat.getUserName());
				flatRepository.save(flat);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}


			List<Flat> flatList = flatRepository.findAll(); 
			Query query = new Query();
			for(int i=0;i<flatList.size();i++)
			{
				try
				{
					query = new Query();
					List<Project>  projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(flatList.get(i).getProjectId())), Project.class);
					flatList.get(i).setProjectId(projectDetails.get(0).getProjectName());

					query = new Query();
					List<ProjectBuilding>  buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(flatList.get(i).getBuildingId())), ProjectBuilding.class); 
					flatList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
					query = new Query();
					List<ProjectWing>  wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(flatList.get(i).getWingId())), ProjectWing.class); 
					flatList.get(i).setWingId(wingDetails.get(0).getWingName());
					query = new Query();
					List<Floor>  floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("floorId").is(flatList.get(i).getFloorId())), Floor.class); 
					flatList.get(i).setFloorId(floorDetails.get(0).getFloortypeName());
				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}

			List<Project> projectList = GetUserAssigenedProjectList(req,res); 

			model.addAttribute("projectList",projectList);

			model.addAttribute("flatList",flatList);
			return "FlatMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping(value="/getFloorRise",method=RequestMethod.POST)
	public List<Floor> getFloorRise(@RequestParam("projectId") String projectId, @RequestParam("buildingId") String buildingId, @RequestParam("wingId") String wingId, @RequestParam("floorId") String floorId, HttpSession session)
	{
		List<Floor> floorList= new ArrayList<Floor>();

		Query query = new Query();
		floorList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("floorId").is(floorId)), Floor.class);
		return floorList;
	}
	@ResponseBody
	@RequestMapping("/getAllFlatNumbers")
	public List<Flat> getAllFlatNumbers(@RequestParam("projectId") String projectId, @RequestParam("buildingId") String buildingId, @RequestParam("wingId") String wingId, @RequestParam("floorId") String floorId)
	{
		Query query = new Query();
		List<Flat> flatList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("floorId").is(floorId)), Flat.class);

		return flatList;
	}

}

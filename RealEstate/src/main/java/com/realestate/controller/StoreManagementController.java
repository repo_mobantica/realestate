package com.realestate.controller;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.City;
import com.realestate.bean.Country;
import com.realestate.bean.Employee;
import com.realestate.bean.Item;
import com.realestate.bean.LocationArea;
import com.realestate.bean.MaterialsPurchased;
import com.realestate.bean.MaterialsPurchasedHistory;
import com.realestate.bean.Project;
import com.realestate.bean.State;
import com.realestate.bean.Stock;
import com.realestate.bean.Store;
import com.realestate.bean.StoreStock;
import com.realestate.bean.StoreStockHistory;
import com.realestate.bean.SubSupplierType;
import com.realestate.bean.Supplier;
import com.realestate.bean.SupplierQuotationHistory;
import com.realestate.configuration.CommanController;
import com.realestate.repository.EmployeeRepository;
import com.realestate.repository.MaterialsPurchasedHistoryRepository;
import com.realestate.repository.MaterialsPurchasedRepository;
import com.realestate.repository.StockRepository;
import com.realestate.repository.StoreStockHistoryRepository;
import com.realestate.repository.StoreStockRepository;
import com.realestate.repository.SupplierRepository;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;

@Controller
@RequestMapping("/")
public class StoreManagementController {

	@Autowired
	EmployeeRepository employeeRepository;
	@Autowired
	MaterialsPurchasedRepository materialsPurchasedRepository;
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	StoreStockHistoryRepository storestockhistoryRepository;
	@Autowired
	StoreStockRepository storestockRepository;
	@Autowired
	StockRepository stockRepository;
	@Autowired
	MaterialsPurchasedHistoryRepository materialspurchasedhistoryRepository;
	@Autowired
	SupplierRepository supplierRepository;

	String historicode;
	String code;

	long count;
	private long PurchesStoreHistoriCode()
	{

		count  = storestockhistoryRepository.count();

		return count;
	}


	private String PurchesStoreCode()
	{
		long cnt  = storestockRepository.count();
		if(cnt<10)
		{
			code = "SC000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			code = "SC00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			code = "SC0"+(cnt+1);
		}
		else
		{
			code = "SC"+(cnt+1);
		}
		return code;
	}

	@RequestMapping("/StoreManagement")
	public String StoreManagement(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {

			List<Employee> employeeList = employeeRepository.findAll();

			List<Supplier> supplierList=supplierRepository.findAll();
			// List<MaterialsPurchased> purchesedList = materialsPurchasedRepository.findAll(); 
			Query query = new Query();
			List<MaterialsPurchased> purchesedList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("Applied")),MaterialsPurchased.class);

			String empName="";
			for(int i=0;i<purchesedList.size();i++)
			{
				for(int j=0;j<employeeList.size();j++)
				{
					if(purchesedList.get(i).getEmployeeId().equals(employeeList.get(j).getEmployeeId()))
					{
						empName=employeeList.get(j).getEmployeefirstName()+" "+employeeList.get(j).getEmployeemiddleName()+" "+employeeList.get(j).getEmployeelastName();
						break;
					}
				}
				purchesedList.get(i).setEmployeeId(empName);

				for(int j=0;j<supplierList.size();j++)
				{
					if(supplierList.get(j).getSupplierId().equalsIgnoreCase(purchesedList.get(i).getSupplierId()))
					{
						purchesedList.get(i).setSupplierfirmName(supplierList.get(j).getSupplierfirmName());
						break;
					}
				}
			}

			query = new Query();
			List<MaterialsPurchased> IncompletedpurchesedList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("InCompleted")),MaterialsPurchased.class);

			empName="";
			for(int i=0;i<IncompletedpurchesedList.size();i++)
			{
				for(int j=0;j<employeeList.size();j++)
				{
					if(IncompletedpurchesedList.get(i).getEmployeeId().equals(employeeList.get(j).getEmployeeId()))
					{
						empName=employeeList.get(j).getEmployeefirstName()+" "+employeeList.get(j).getEmployeemiddleName()+" "+employeeList.get(j).getEmployeelastName();
						break;
					}
				}
				IncompletedpurchesedList.get(i).setEmployeeId(empName);

				for(int j=0;j<supplierList.size();j++)
				{
					if(supplierList.get(j).getSupplierId().equalsIgnoreCase(IncompletedpurchesedList.get(i).getSupplierId()))
					{
						IncompletedpurchesedList.get(i).setSupplierfirmName(supplierList.get(j).getSupplierfirmName());
						break;
					}
				}
			}

			model.addAttribute("supplierList", supplierList);
			model.addAttribute("IncompletedpurchesedList", IncompletedpurchesedList);
			model.addAttribute("purchesedList", purchesedList);

			return "StoreManagement";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/MaterialPurchesedByStore")
	public String MaterialPurchesedByStore(@RequestParam("materialsPurchasedId") String materialsPurchasedId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<MaterialsPurchasedHistory> materialDetails = mongoTemplate.find(query.addCriteria(Criteria.where("materialsPurchasedId").is(materialsPurchasedId)),MaterialsPurchasedHistory.class);

			Query query1 = new Query();
			List<MaterialsPurchased> materialpurchesDetails = mongoTemplate.find(query1.addCriteria(Criteria.where("materialsPurchasedId").is(materialsPurchasedId)),MaterialsPurchased.class);
			for(int i=0;i<materialDetails.size();i++)
			{
				query =new Query();
				Item itemDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("itemId").is(materialDetails.get(i).getItemId())), Item.class);

				query =new Query();
				List<SubSupplierType> subsuppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subsuppliertypeId").is(itemDetails.getSubsuppliertypeId())), SubSupplierType.class);
				materialDetails.get(i).setSubsuppliertypeName(subsuppliertypeDetails.get(0).getSubsupplierType());
			}

			long storeStockHistoriId= PurchesStoreHistoriCode();
			String grnNo="GRN"+storeStockHistoriId;
			model.addAttribute("materialpurchesDetails", materialpurchesDetails);
			model.addAttribute("materialDetails", materialDetails);
			model.addAttribute("storeStockHistoriId", storeStockHistoriId);
			model.addAttribute("grnNo", grnNo);

			return "MaterialPurchesedByStore";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/MaterialPurchesedByStore",method=RequestMethod.POST)
	public String SaveMaterialPurchesedByStore(@ModelAttribute StoreStock storestock, @RequestParam("status") String status, Model model,HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{

				StoreStock storeStock=new StoreStock();
				storeStock.setStoreStockId(PurchesStoreCode());
				storeStock.setStoreId(storestock.getStoreId());
				storeStock.setMaterialsPurchasedId(storestock.getMaterialsPurchasedId());
				storeStock.setGrnNo(storestock.getGrnNo());
				storeStock.setChallanNo(storestock.getChallanNo());
				storeStock.setEwayBillNo(storestock.getEwayBillNo());
				storeStock.setVehicleNumber(storestock.getVehicleNumber());
				storeStock.setLoadedWeight(storestock.getLoadedWeight());
				storeStock.setUnloadedWeight(storestock.getUnloadedWeight());
				storeStock.setTotalMaterialWeight(storestock.getTotalMaterialWeight());

				storeStock.setRemark(storestock.getRemark());
				storeStock.setCreationDate(storestock.getCreationDate());
				storestockRepository.save(storeStock);

				String itemIds=req.getParameter("itemIds");
				String itemSizes=req.getParameter("itemSizes");
				String itemUnits=req.getParameter("itemUnits");
				String itemBrandNames=req.getParameter("itemBrandNames");
				String itemQuantitys=req.getParameter("itemQuantitys");
				String actualQuantitys=req.getParameter("actualQuantitys");
				String noOfPiecess=req.getParameter("noOfPiecess");
				String remarks=req.getParameter("remarks");
				String materialPurchesHistoriIds=req.getParameter("materialPurchesHistoriIds");

				String[] itemIdsArray = itemIds.split("###!###");
				String[] itemSizesArray = itemSizes.split("###!###");
				String[] itemUnitsArray = itemUnits.split("###!###");
				String[] itemBrandNamesArray = itemBrandNames.split("###!###");
				String[] itemQuantitysArray = itemQuantitys.split("###!###");
				String[] actualQuantitysArray = actualQuantitys.split("###!###");
				String[] noOfPiecessArray = noOfPiecess.split("###!###");
				String[] remarksArray = remarks.split("###!###");
				String[] materialPurchesHistoriIdsArray = materialPurchesHistoriIds.split("###!###");


				double presentStock=0.0;
				long presentnoOfPieces=0;
				Date date = new Date();  
				SimpleDateFormat formatter = new SimpleDateFormat("d/M/yyyy");  
				String strDate= formatter.format(date);

				long historiId=PurchesStoreHistoriCode();
				String storeStockHistoriId="";
				long cnt  = stockRepository.count();
				Stock stock1=new Stock();

				StoreStockHistory storeStockHistory = new StoreStockHistory();
				for(int i=0;i<itemIdsArray.length;i++)
				{

					if(historiId<9)
					{
						storeStockHistoriId = "PSC000"+(historiId+i);
					}
					else if((historiId<99) && (historiId>=10))
					{
						storeStockHistoriId = "PSC00"+(historiId+i);
					}
					else if((historiId<999) && (historiId>=100))
					{
						storeStockHistoriId = "PSC0"+(historiId+i);
					}
					else if(historiId>=9999)
					{
						storeStockHistoriId = "PSC"+(historiId+i);
					}

					try
					{
						storeStockHistory = new StoreStockHistory();
						storeStockHistory.setStoreStockHistoriId(storeStockHistoriId);
						storeStockHistory.setMaterialsPurchasedId(storestock.getMaterialsPurchasedId());
						storeStockHistory.setStoreId(storestock.getStoreId());
						storeStockHistory.setItemId(itemIdsArray[i]);
						storeStockHistory.setItemUnit(itemUnitsArray[i]);
						storeStockHistory.setItemSize(itemSizesArray[i]);
						storeStockHistory.setItemBrandName(itemBrandNamesArray[i]);
						storeStockHistory.setItemQuantity(Double.parseDouble(actualQuantitysArray[i]));
						storeStockHistory.setRemark(remarksArray[i]);
						storeStockHistory.setNoOfPieces(Long.parseLong(noOfPiecessArray[i]));
						storeStockHistory.setCreationDate(strDate);

						storestockhistoryRepository.save(storeStockHistory);
					}
					catch(Exception e)
					{

					}
					MaterialsPurchasedHistory materialspurchasedhistory=new MaterialsPurchasedHistory();
					materialspurchasedhistory = mongoTemplate.findOne(Query.query(Criteria.where("materialPurchesHistoriId").is(materialPurchesHistoriIdsArray[i])), MaterialsPurchasedHistory.class);

					try {
						if(Double.parseDouble(actualQuantitysArray[i])==Double.parseDouble(itemQuantitysArray[i]))
						{
							materialspurchasedhistory.setStatus("Done");
							materialspurchasedhistoryRepository.save(materialspurchasedhistory);
						}
						else
						{
							materialspurchasedhistory.setStatus("InCompleted");
							materialspurchasedhistoryRepository.save(materialspurchasedhistory);
						}
					}catch (Exception e) {
						e.printStackTrace();
						// TODO: handle exception
					}
					try
					{
						Query query1 = new Query();
						List<Stock> stockList = mongoTemplate.find(query1.addCriteria(Criteria.where("itemId").is(itemIdsArray[i]).and("storeId").is(storestock.getStoreId())),Stock.class);
						if(stockList.size()!=0)
						{
							presentStock=stockList.get(0).getCurrentStock();
							presentnoOfPieces=stockList.get(0).getNoOfPieces();
							presentStock=presentStock+Double.parseDouble(actualQuantitysArray[i]);
							presentnoOfPieces=(presentnoOfPieces+Long.parseLong(noOfPiecessArray[i]));
							stockList.get(0).setCurrentStock(presentStock);
							stockList.get(0).setNoOfPieces(presentnoOfPieces);
							stockList.get(0).setUpdateDate(strDate);
							stockRepository.save(stockList.get(0));
						}
						else
						{
							try {
								String stockCode="";
								if(cnt<10)
								{
									stockCode = "SK000"+(cnt+i);
								}
								else if((cnt<100) && (cnt>=10))
								{
									stockCode = "SK00"+(cnt+i);
								}
								else if((cnt<1000) && (cnt>=100))
								{
									stockCode = "SK0"+(cnt+i);
								}
								else if(cnt>=1000)
								{
									stockCode = "SK"+(cnt+i);
								}
								stock1=new Stock();
								stock1.setStockId(stockCode);	
								stock1.setStoreId(storestock.getStoreId());
								stock1.setItemId(itemIdsArray[i]);
								//stock1.setBrandName(itemBrandName);
								stock1.setCurrentStock(Double.parseDouble(actualQuantitysArray[i]));
								stock1.setNoOfPieces(Long.parseLong(noOfPiecessArray[i]));
								stock1.setCreationDate(strDate);
								stockRepository.save(stock1);
							}catch (Exception e) {
								e.printStackTrace();
								// TODO: handle exception
							}
						}
					}
					catch (Exception e) {
						e.printStackTrace();
					}

				}


			}
			catch(Exception ee) {
				ee.printStackTrace();

			}
			String status1="";
			if(status.equalsIgnoreCase("InCompleted"))
			{
				status1="InCompleted";
			}
			else
			{
				status1="Completed";
			}
			try {
				MaterialsPurchased materialspurchased;

				materialspurchased = mongoTemplate.findOne(Query.query(Criteria.where("materialsPurchasedId").is(storestock.getMaterialsPurchasedId())), MaterialsPurchased.class);
				materialspurchased.setStatus(status1);
				materialsPurchasedRepository.save(materialspurchased);
			}
			catch (Exception e) {
			}




			List<Supplier> supplierList=supplierRepository.findAll();
			List<Employee> employeeList = employeeRepository.findAll();
			// List<MaterialsPurchased> purchesedList = materialsPurchasedRepository.findAll(); 
			Query query = new Query();
			List<MaterialsPurchased> purchesedList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("Applied")),MaterialsPurchased.class);

			String empName="";
			for(int i=0;i<purchesedList.size();i++)
			{
				for(int j=0;j<employeeList.size();j++)
				{
					if(purchesedList.get(i).getEmployeeId().equals(employeeList.get(j).getEmployeeId()))
					{
						empName=employeeList.get(j).getEmployeefirstName()+" "+employeeList.get(j).getEmployeemiddleName()+" "+employeeList.get(j).getEmployeelastName();
						break;
					}
				}
				purchesedList.get(i).setEmployeeId(empName);
				for(int j=0;j<supplierList.size();j++)
				{
					if(supplierList.get(j).getSupplierId().equalsIgnoreCase(purchesedList.get(i).getSupplierId()))
					{
						purchesedList.get(i).setSupplierfirmName(supplierList.get(j).getSupplierfirmName());
						break;
					}
				}
			}



			query = new Query();
			List<MaterialsPurchased> IncompletedpurchesedList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("InCompleted")),MaterialsPurchased.class);

			empName="";
			for(int i=0;i<IncompletedpurchesedList.size();i++)
			{
				for(int j=0;j<employeeList.size();j++)
				{
					if(IncompletedpurchesedList.get(i).getEmployeeId().equals(employeeList.get(j).getEmployeeId()))
					{
						empName=employeeList.get(j).getEmployeefirstName()+" "+employeeList.get(j).getEmployeemiddleName()+" "+employeeList.get(j).getEmployeelastName();
						break;
					}
				}
				IncompletedpurchesedList.get(i).setEmployeeId(empName);

				for(int j=0;j<supplierList.size();j++)
				{
					if(supplierList.get(j).getSupplierId().equalsIgnoreCase(IncompletedpurchesedList.get(i).getSupplierId()))
					{
						IncompletedpurchesedList.get(i).setSupplierfirmName(supplierList.get(j).getSupplierfirmName());
						break;
					}
				}
			}





			model.addAttribute("supplierList", supplierList);
			model.addAttribute("IncompletedpurchesedList", IncompletedpurchesedList);
			model.addAttribute("purchesedList", purchesedList);

			return "StoreManagement";

		}catch (Exception e) {
			return "login";
		}
	}


	@ResponseBody
	@RequestMapping("/SaveAlllStoreData")
	public List<StoreStockHistory> SaveAlllStoreData(@RequestParam("storeStockHistoriId") String storeStockHistoriId, @RequestParam("storeId") String storeId, @RequestParam("itemId") String itemId, @RequestParam("itemUnit") String itemUnit, @RequestParam("itemSize") String itemSize, @RequestParam("itemBrandName") String itemBrandName, @RequestParam("itemQuantity") double itemQuantity, 
			@RequestParam("actualQuantity") double actualQuantity, @RequestParam("noOfPieces") long noOfPieces, @RequestParam("remark") String remark,
			@RequestParam("materialPurchesHistoriId") String materialPurchesHistoriId, @RequestParam("materialsPurchasedId") String materialsPurchasedId, HttpSession session)
	{
		double presentStock=0.0;
		long presentnoOfPieces=0;
		Date date = new Date();  
		SimpleDateFormat formatter = new SimpleDateFormat("d/M/yyyy");  
		String strDate= formatter.format(date);
		try
		{
			StoreStockHistory storeStockHistory = new StoreStockHistory();
			storeStockHistory.setStoreStockHistoriId(storeStockHistoriId);
			storeStockHistory.setMaterialsPurchasedId(materialsPurchasedId);
			storeStockHistory.setStoreId(storeId);
			storeStockHistory.setItemId(itemId);
			storeStockHistory.setItemUnit(itemUnit);
			storeStockHistory.setItemSize(itemSize);
			storeStockHistory.setItemBrandName(itemBrandName);
			storeStockHistory.setItemQuantity(actualQuantity);
			storeStockHistory.setRemark(remark);
			storeStockHistory.setNoOfPieces(noOfPieces);
			storeStockHistory.setCreationDate(strDate);

			storestockhistoryRepository.save(storeStockHistory);
		}
		catch(Exception e)
		{

		}
		MaterialsPurchasedHistory materialspurchasedhistory=new MaterialsPurchasedHistory();
		materialspurchasedhistory = mongoTemplate.findOne(Query.query(Criteria.where("materialPurchesHistoriId").is(materialPurchesHistoriId)), MaterialsPurchasedHistory.class);

		if(actualQuantity==itemQuantity)
		{
			materialspurchasedhistory.setStatus("Done");
			materialspurchasedhistoryRepository.save(materialspurchasedhistory);
		}
		else
		{
			materialspurchasedhistory.setStatus("InCompleted");
			materialspurchasedhistoryRepository.save(materialspurchasedhistory);
		}
		try
		{
			Query query1 = new Query();
			List<Stock> stockList = mongoTemplate.find(query1.addCriteria(Criteria.where("itemId").is(itemId).and("storeId").is(storeId)),Stock.class);
			if(stockList.size()!=0)
			{
				presentStock=stockList.get(0).getCurrentStock();
				presentnoOfPieces=stockList.get(0).getNoOfPieces();
				presentStock=presentStock+actualQuantity;
				presentnoOfPieces=presentnoOfPieces+noOfPieces;
				stockList.get(0).setCurrentStock(presentStock);
				stockList.get(0).setNoOfPieces(presentnoOfPieces);
				stockList.get(0).setUpdateDate(strDate);
				stockRepository.save(stockList.get(0));
			}
			else
			{
				String stockCode="";
				long cnt  = stockRepository.count();
				if(cnt<10)
				{
					stockCode = "SK000"+(cnt+1);
				}
				else if((cnt<100) && (cnt>=10))
				{
					stockCode = "SK00"+(cnt+1);
				}
				else if((cnt<1000) && (cnt>=100))
				{
					stockCode = "SK0"+(cnt+1);
				}
				else if(cnt>=1000)
				{
					stockCode = "SK"+(cnt+1);
				}
				Stock stock1=new Stock();
				stock1.setStockId(stockCode);	
				stock1.setStoreId(storeId);
				stock1.setItemId(itemId);
				//stock1.setBrandName(itemBrandName);
				stock1.setCurrentStock(actualQuantity);
				stock1.setNoOfPieces(noOfPieces);
				stock1.setCreationDate(strDate);
				stockRepository.save(stock1);
			}
		}
		catch (Exception e) {
		}

		return null;	
	}	

	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value="/StoreWiseStock", method = RequestMethod.POST)
	public ResponseEntity<byte[]> PrintStoreWiseStock(@RequestParam("storeId") String storeId, @RequestParam("suppliertypeId") String suppliertypeId, HttpServletRequest request, HttpServletResponse response, HttpSession session)
	{
		JasperPrint print;

		Query query = new Query();
		List<Stock> storeStock;
		if(suppliertypeId.equalsIgnoreCase("Default"))
		{
			storeStock = mongoTemplate.find(query.addCriteria(Criteria.where("storeId").is(storeId)), Stock.class);
		}
		else
		{
			storeStock = mongoTemplate.find(query.addCriteria(Criteria.where("storeId").is(storeId)), Stock.class);
		}

		query = new Query();
		Store storeDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("storeId").is(storeId)), Store.class);

		if(storeStock.size()!=0)
		{
			try 
			{	
				HashMap jmap = new HashMap();
				Collection c = new ArrayList();

				/*jmap.put("srno", "1"); */


				/*DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/M/yyyy");
								LocalDate localDate = LocalDate.now();
								String todayDate=dtf.format(localDate);*/


				List<Stock> storewisestockList = new ArrayList<Stock>();
				List<Item> itemDetails= new ArrayList<Item>();

				Stock storewisestock=new Stock();

				for(int i=0;i<storeStock.size();i++)
				{
					storewisestock=new Stock();
					query = new Query();
					itemDetails = mongoTemplate.find(query.addCriteria(Criteria.where("itemId").is(storeStock.get(i).getItemId()).and("suppliertypeId").is(suppliertypeId)), Item.class);

					try
					{
						if(itemDetails.size()!=0)
						{
							storewisestock.setItemId(storeStock.get(i).getItemId());
							//storewisestock.setItemBrandName(storestockList.get(i).getBrandName());
							storewisestock.setItemName(itemDetails.get(0).getItemName());
							
							query =new Query();
							List<SubSupplierType> subsuppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subsuppliertypeId").is(itemDetails.get(0).getSubsuppliertypeId())), SubSupplierType.class);

							storewisestock.setSubsuppliertypeId(subsuppliertypeDetails.get(0).getSubsupplierType());
							storewisestock.setItemQuantity(storeStock.get(i).getCurrentStock());
							storewisestock.setNoOfPieces(storeStock.get(i).getNoOfPieces());
							storewisestockList.add(storewisestock);
							itemDetails.clear();
						}
					} 
					catch (Exception e) {
						// TODO: handle exception
					}

				}

				
				
				
				
				for(int i=0;i<storewisestockList.size();i++)
				{
					jmap = new HashMap();

					jmap.put("srno",""+(i+1));

					jmap.put("itemName",""+storewisestockList.get(i).getItemName());
					jmap.put("itemSubType",""+storewisestockList.get(i).getSubsuppliertypeId());
					jmap.put("noOfPcs",""+storewisestockList.get(i).getNoOfPieces());
					jmap.put("noOfItem",""+storewisestockList.get(i).getCurrentStock());

					c.add(jmap);
					jmap = null;
				}
				if(storewisestockList.size()==0)
				{

					jmap = new HashMap();

					jmap.put("srno","");

					jmap.put("itemName","");
					jmap.put("itemSubType","");
					jmap.put("noOfPcs","");
					jmap.put("noOfItem","");

					c.add(jmap);
					jmap = null;
				}

				//String realPath ="//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img"; //context.getRealPath("/resources/dist/img");
				String realPath=CommanController.GetLogoImagePath();

				JRDataSource dataSource = new JRMapCollectionDataSource(c);
				Map<String, Object> parameterMap = new HashMap<String, Object>();

				Date date = new Date();  
				SimpleDateFormat formatter = new SimpleDateFormat("d/M/yyyy");  
				String strDate= formatter.format(date);

				/*1*/ parameterMap.put("printDate", ""+strDate);
				/*2*/ parameterMap.put("storeName", ""+storeDetails.getStoreName());
				/*3*/ parameterMap.put("realPath", realPath);

				InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/StoreWiseStockList.jasper");

				print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				JasperExportManager.exportReportToPdfStream(print, baos);

				byte[] contents = baos.toByteArray();

				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.parseMediaType("application/pdf"));
				String filename = "StoreWiseStockList.pdf";

				JasperExportManager.exportReportToPdfStream(print, baos);

				headers.setContentType(MediaType.parseMediaType("application/pdf"));
				headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
				ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
				response.setHeader("Content-Disposition", "inline; filename=" + filename );
				return resp;	
			}
			catch(Exception e)
			{
				return null;
			}
		}
		else
		{
			try 
			{	
				HashMap jmap = new HashMap();
				Collection c = new ArrayList();

				/*jmap.put("srno", "1"); */


				/*DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/M/yyyy");
								LocalDate localDate = LocalDate.now();
								String todayDate=dtf.format(localDate);*/

				jmap = new HashMap();

				jmap.put("srno","1");
				jmap.put("itemName","N.A.");
				jmap.put("itemSubType","N.A.");
				jmap.put("noOfPcs","N.A.");

				c.add(jmap);
				jmap = null;

				String realPath =CommanController.GetLogoImagePath();
				//String realPath ="//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img"; //context.getRealPath("/resources/dist/img");

				JRDataSource dataSource = new JRMapCollectionDataSource(c);
				Map<String, Object> parameterMap = new HashMap<String, Object>();

				Date date = new Date();  
				SimpleDateFormat formatter = new SimpleDateFormat("d/M/yyyy");  
				String strDate= formatter.format(date);

				//Booking Receipt Generation Code
				/*1*/ parameterMap.put("printDate", ""+strDate);
				/*2*/ parameterMap.put("storeName", ""+storeDetails.getStoreName());
				/*3*/ parameterMap.put("realPath", realPath);

				InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/StoreWiseStockList.jasper");

				print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				JasperExportManager.exportReportToPdfStream(print, baos);

				byte[] contents = baos.toByteArray();

				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.parseMediaType("application/pdf"));
				String filename = "StoreWiseStockList.pdf";

				JasperExportManager.exportReportToPdfStream(print, baos);

				headers.setContentType(MediaType.parseMediaType("application/pdf"));
				headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
				ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
				response.setHeader("Content-Disposition", "inline; filename=" + filename );
				return resp;	
			}
			catch(Exception e)
			{
				return null;
			}
		}
	}


	@RequestMapping("/PendingMaterialPurchesedByStore")
	public String PendingMaterialPurchesedByStore(@RequestParam("materialsPurchasedId") String materialsPurchasedId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<MaterialsPurchasedHistory> materialDetails = mongoTemplate.find(query.addCriteria(Criteria.where("materialsPurchasedId").is(materialsPurchasedId).and("status").is("InCompleted")),MaterialsPurchasedHistory.class);

			query = new Query();
			List<StoreStockHistory> storeStockHistory = mongoTemplate.find(query.addCriteria(Criteria.where("materialsPurchasedId").is(materialsPurchasedId)),StoreStockHistory.class);
			String challanNo="";
			query = new Query();
			List<StoreStock> storeStockList = mongoTemplate.find(query.addCriteria(Criteria.where("materialsPurchasedId").is(materialsPurchasedId)),StoreStock.class);
			for(int i=0;i<storeStockList.size();i++)
			{
				if(i==0)
				{
					challanNo=""+storeStockList.get(i).getChallanNo();
				}
				else
				{
					challanNo=challanNo+" / "+storeStockList.get(i).getChallanNo();
				}
			}
			double previousQuantity;
			double remainingQuantity;
			long previousNoOfPieces=0;
			for(int i=0;i<materialDetails.size();i++)
			{
				previousQuantity=0.0;
				remainingQuantity=0.0;
				previousNoOfPieces=0;
				for(int j=0;j<storeStockHistory.size();j++)
				{
					if(storeStockHistory.get(j).getItemId().equalsIgnoreCase(materialDetails.get(i).getItemId()))
					{
						previousQuantity=previousQuantity+storeStockHistory.get(j).getItemQuantity();
						previousNoOfPieces=previousNoOfPieces+storeStockHistory.get(j).getNoOfPieces();

					}
				}


				remainingQuantity=materialDetails.get(i).getItemQuantity()-previousQuantity;
				materialDetails.get(i).setPreviousQuantity(previousQuantity);
				//materialDetails.get(i).setStoreStockHistoriId(storeStockHistory.get(j).getStoreStockHistoriId());
				materialDetails.get(i).setPreviousNoOfPieces(previousNoOfPieces);
				materialDetails.get(i).setRemainingQuantity(remainingQuantity);
			}

			query = new Query();
			List<MaterialsPurchased> materialpurchesDetails = mongoTemplate.find(query.addCriteria(Criteria.where("materialsPurchasedId").is(materialsPurchasedId)),MaterialsPurchased.class);
			for(int i=0;i<materialDetails.size();i++)
			{
				query =new Query();
				Item itemDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("itemId").is(materialDetails.get(i).getItemId())), Item.class);

				query =new Query();
				SubSupplierType subsuppliertypeDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("subsuppliertypeId").is(itemDetails.getSubsuppliertypeId())), SubSupplierType.class);
				materialDetails.get(i).setSubsuppliertypeName(subsuppliertypeDetails.getSubsupplierType());
			}
			long storeStockHistoriId= PurchesStoreHistoriCode();
			String grnNo="GRN"+storeStockHistoriId;
			model.addAttribute("grnNo", grnNo);
			model.addAttribute("challanNo", challanNo);
			model.addAttribute("materialpurchesDetails", materialpurchesDetails);
			model.addAttribute("materialDetails", materialDetails);
			model.addAttribute("storeStockHistoriId", PurchesStoreHistoriCode());

			return "PendingMaterialPurchesedByStore";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/PendingMaterialPurchesedByStore",method=RequestMethod.POST)
	public String PendingMaterialPurchesedByStore(@ModelAttribute StoreStock storestock, @RequestParam("status") String status, Model model,HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{

				StoreStock storeStock=new StoreStock();
				storeStock.setStoreStockId(PurchesStoreCode());
				storeStock.setStoreId(storestock.getStoreId());
				storeStock.setMaterialsPurchasedId(storestock.getMaterialsPurchasedId());
				storeStock.setGrnNo(storestock.getGrnNo());
				storeStock.setChallanNo(storestock.getChallanNo());
				storeStock.setEwayBillNo(storestock.getEwayBillNo());
				storeStock.setVehicleNumber(storestock.getVehicleNumber());
				storeStock.setLoadedWeight(storestock.getLoadedWeight());
				storeStock.setUnloadedWeight(storestock.getUnloadedWeight());
				storeStock.setTotalMaterialWeight(storestock.getTotalMaterialWeight());

				storeStock.setRemark(storestock.getRemark());
				storeStock.setCreationDate(storestock.getCreationDate());
				storestockRepository.save(storeStock);

				String itemIds=req.getParameter("itemIds");
				String itemSizes=req.getParameter("itemSizes");
				String itemUnits=req.getParameter("itemUnits");
				String itemBrandNames=req.getParameter("itemBrandNames");
				String itemQuantitys=req.getParameter("itemQuantitys");
				String actualQuantitys=req.getParameter("actualQuantitys");
				String noOfPiecess=req.getParameter("noOfPiecess");
				String remarks=req.getParameter("remarks");
				String materialPurchesHistoriIds=req.getParameter("materialPurchesHistoriIds");
				String previousQuantitys=req.getParameter("previousQuantitys");

				String[] itemIdsArray = itemIds.split("###!###");
				String[] itemSizesArray = itemSizes.split("###!###");
				String[] itemUnitsArray = itemUnits.split("###!###");
				String[] itemBrandNamesArray = itemBrandNames.split("###!###");
				String[] itemQuantitysArray = itemQuantitys.split("###!###");
				String[] actualQuantitysArray = actualQuantitys.split("###!###");
				String[] noOfPiecessArray = noOfPiecess.split("###!###");
				String[] remarksArray = remarks.split("###!###");
				String[] materialPurchesHistoriIdsArray = materialPurchesHistoriIds.split("###!###");
				String[] previousQuantitysArray = previousQuantitys.split("###!###");


				double presentStock=0.0;
				long presentnoOfPieces=0;
				Date date = new Date();  
				SimpleDateFormat formatter = new SimpleDateFormat("d/M/yyyy");  
				String strDate= formatter.format(date);

				long historiId=PurchesStoreHistoriCode();
				String storeStockHistoriId="";
				long cnt  = stockRepository.count();
				Stock stock1=new Stock();

				StoreStockHistory storeStockHistory = new StoreStockHistory();
				for(int i=0;i<itemIdsArray.length;i++)
				{

					if(historiId<9)
					{
						storeStockHistoriId = "PSC000"+(historiId+i);
					}
					else if((historiId<99) && (historiId>=10))
					{
						storeStockHistoriId = "PSC00"+(historiId+i);
					}
					else if((historiId<999) && (historiId>=100))
					{
						storeStockHistoriId = "PSC0"+(historiId+i);
					}
					else if(historiId>=9999)
					{
						storeStockHistoriId = "PSC"+(historiId+i);
					}

					try
					{
						storeStockHistory = new StoreStockHistory();
						storeStockHistory.setStoreStockHistoriId(storeStockHistoriId);
						storeStockHistory.setMaterialsPurchasedId(storestock.getMaterialsPurchasedId());
						storeStockHistory.setStoreId(storestock.getStoreId());
						storeStockHistory.setItemId(itemIdsArray[i]);
						storeStockHistory.setItemUnit(itemUnitsArray[i]);
						storeStockHistory.setItemSize(itemSizesArray[i]);
						storeStockHistory.setItemBrandName(itemBrandNamesArray[i]);
						storeStockHistory.setItemQuantity(Double.parseDouble(actualQuantitysArray[i]));
						storeStockHistory.setRemark(remarksArray[i]);
						storeStockHistory.setNoOfPieces(Long.parseLong(noOfPiecessArray[i]));
						storeStockHistory.setCreationDate(strDate);

						storestockhistoryRepository.save(storeStockHistory);
					}
					catch(Exception e)
					{

					}
					MaterialsPurchasedHistory materialspurchasedhistory=new MaterialsPurchasedHistory();
					materialspurchasedhistory = mongoTemplate.findOne(Query.query(Criteria.where("materialPurchesHistoriId").is(materialPurchesHistoriIdsArray[i])), MaterialsPurchasedHistory.class);
					double qty=Double.parseDouble(previousQuantitysArray[i])+Double.parseDouble(actualQuantitysArray[i]);

					try {
						if(Double.parseDouble(itemQuantitysArray[i])<=qty)
						{
							materialspurchasedhistory.setStatus("Done");
							materialspurchasedhistoryRepository.save(materialspurchasedhistory);
						}
						else
						{
							materialspurchasedhistory.setStatus("InCompleted");
							materialspurchasedhistoryRepository.save(materialspurchasedhistory);
						}
					}catch (Exception e) {
						e.printStackTrace();
						// TODO: handle exception
					}
					try
					{
						Query query1 = new Query();
						List<Stock> stockList = mongoTemplate.find(query1.addCriteria(Criteria.where("itemId").is(itemIdsArray[i]).and("storeId").is(storestock.getStoreId())),Stock.class);
						if(stockList.size()!=0)
						{
							presentStock=stockList.get(0).getCurrentStock();
							presentnoOfPieces=stockList.get(0).getNoOfPieces();
							presentStock=presentStock+Double.parseDouble(actualQuantitysArray[i]);
							presentnoOfPieces=(presentnoOfPieces+Long.parseLong(noOfPiecessArray[i]));
							stockList.get(0).setCurrentStock(presentStock);
							stockList.get(0).setNoOfPieces(presentnoOfPieces);
							stockList.get(0).setUpdateDate(strDate);
							stockRepository.save(stockList.get(0));
						}
						else
						{
							try {
								String stockCode="";
								if(cnt<10)
								{
									stockCode = "SK000"+(cnt+i);
								}
								else if((cnt<100) && (cnt>=10))
								{
									stockCode = "SK00"+(cnt+i);
								}
								else if((cnt<1000) && (cnt>=100))
								{
									stockCode = "SK0"+(cnt+i);
								}
								else if(cnt>=1000)
								{
									stockCode = "SK"+(cnt+i);
								}
								stock1=new Stock();
								stock1.setStockId(stockCode);	
								stock1.setStoreId(storestock.getStoreId());
								stock1.setItemId(itemIdsArray[i]);
								//stock1.setBrandName(itemBrandName);
								stock1.setCurrentStock(Double.parseDouble(actualQuantitysArray[i]));
								stock1.setNoOfPieces(Long.parseLong(noOfPiecessArray[i]));
								stock1.setCreationDate(strDate);
								stockRepository.save(stock1);
							}catch (Exception e) {
								e.printStackTrace();
								// TODO: handle exception
							}
						}
					}
					catch (Exception e) {
						e.printStackTrace();
					}

				}


			}
			catch(Exception ee) {
				ee.printStackTrace();

			}
			String status1="";
			if(status.equalsIgnoreCase("InCompleted"))
			{
				status1="InCompleted";
			}
			else
			{
				status1="Completed";
			}
			try {
				MaterialsPurchased materialspurchased;

				materialspurchased = mongoTemplate.findOne(Query.query(Criteria.where("materialsPurchasedId").is(storestock.getMaterialsPurchasedId())), MaterialsPurchased.class);
				materialspurchased.setStatus(status1);
				materialsPurchasedRepository.save(materialspurchased);
			}
			catch (Exception e) {
			}



			List<Supplier> supplierList=supplierRepository.findAll();
			List<Employee> employeeList = employeeRepository.findAll();
			// List<MaterialsPurchased> purchesedList = materialsPurchasedRepository.findAll(); 
			Query query = new Query();
			List<MaterialsPurchased> purchesedList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("Applied")),MaterialsPurchased.class);

			String empName="";
			for(int i=0;i<purchesedList.size();i++)
			{
				for(int j=0;j<employeeList.size();j++)
				{
					if(purchesedList.get(i).getEmployeeId().equals(employeeList.get(j).getEmployeeId()))
					{
						empName=employeeList.get(j).getEmployeefirstName()+" "+employeeList.get(j).getEmployeemiddleName()+" "+employeeList.get(j).getEmployeelastName();
						break;
					}
				}
				purchesedList.get(i).setEmployeeId(empName);
				for(int j=0;j<supplierList.size();j++)
				{
					if(supplierList.get(j).getSupplierId().equalsIgnoreCase(purchesedList.get(i).getSupplierId()))
					{
						purchesedList.get(i).setSupplierfirmName(supplierList.get(j).getSupplierfirmName());
						break;
					}
				}
			}



			query = new Query();
			List<MaterialsPurchased> IncompletedpurchesedList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("InCompleted")),MaterialsPurchased.class);

			empName="";
			for(int i=0;i<IncompletedpurchesedList.size();i++)
			{
				for(int j=0;j<employeeList.size();j++)
				{
					if(IncompletedpurchesedList.get(i).getEmployeeId().equals(employeeList.get(j).getEmployeeId()))
					{
						empName=employeeList.get(j).getEmployeefirstName()+" "+employeeList.get(j).getEmployeemiddleName()+" "+employeeList.get(j).getEmployeelastName();
						break;
					}
				}
				IncompletedpurchesedList.get(i).setEmployeeId(empName);

				for(int j=0;j<supplierList.size();j++)
				{
					if(supplierList.get(j).getSupplierId().equalsIgnoreCase(IncompletedpurchesedList.get(i).getSupplierId()))
					{
						IncompletedpurchesedList.get(i).setSupplierfirmName(supplierList.get(j).getSupplierfirmName());
						break;
					}
				}
			}





			model.addAttribute("supplierList", supplierList);
			model.addAttribute("IncompletedpurchesedList", IncompletedpurchesedList);
			model.addAttribute("purchesedList", purchesedList);

			return "StoreManagement";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping(value="/getStoreAllMaterialList",method=RequestMethod.POST)
	public List<MaterialsPurchased> getStoreAllMaterialList(@RequestParam("supplierId") String supplierId, HttpSession session)
	{
		List<Employee> employeeList = employeeRepository.findAll();

		Query query = new Query();
		List<MaterialsPurchased> purchesedList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("Applied").and("supplierId").is(supplierId)),MaterialsPurchased.class);
		query = new Query();
		Supplier supplierList = mongoTemplate.findOne(query.addCriteria(Criteria.where("supplierId").is(supplierId)),Supplier.class);


		String empName="";
		for(int i=0;i<purchesedList.size();i++)
		{
			for(int j=0;j<employeeList.size();j++)
			{
				if(purchesedList.get(i).getEmployeeId().equals(employeeList.get(j).getEmployeeId()))
				{
					empName=employeeList.get(j).getEmployeefirstName()+" "+employeeList.get(j).getEmployeemiddleName()+" "+employeeList.get(j).getEmployeelastName();
					break;
				}
			}
			purchesedList.get(i).setEmployeeId(empName);

			purchesedList.get(i).setSupplierfirmName(supplierList.getSupplierfirmName());

		}

		return purchesedList;
	}

	@ResponseBody
	@RequestMapping(value="/getStoreInCompletedMaterialList",method=RequestMethod.POST)
	public List<MaterialsPurchased> getStoreInCompletedMaterialList(@RequestParam("supplierId") String supplierId, HttpSession session)
	{

		List<Employee> employeeList = employeeRepository.findAll();

		Query query = new Query();

		List<MaterialsPurchased> IncompletedpurchesedList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("InCompleted")),MaterialsPurchased.class);
		query = new Query();
		Supplier supplierList = mongoTemplate.findOne(query.addCriteria(Criteria.where("supplierId").is(supplierId)),Supplier.class);

		String empName="";
		for(int i=0;i<IncompletedpurchesedList.size();i++)
		{
			for(int j=0;j<employeeList.size();j++)
			{
				if(IncompletedpurchesedList.get(i).getEmployeeId().equals(employeeList.get(j).getEmployeeId()))
				{
					empName=employeeList.get(j).getEmployeefirstName()+" "+employeeList.get(j).getEmployeemiddleName()+" "+employeeList.get(j).getEmployeelastName();
					break;
				}
			}
			IncompletedpurchesedList.get(i).setEmployeeId(empName);


			IncompletedpurchesedList.get(i).setSupplierfirmName(supplierList.getSupplierfirmName());
		}

		return IncompletedpurchesedList;
	}


}

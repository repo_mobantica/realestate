package com.realestate.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.realestate.bean.Aggreement;
import com.realestate.bean.Booking;
import com.realestate.bean.BookingCancelForm;
import com.realestate.bean.CustomerLoanDetails;
import com.realestate.bean.CustomerOtherPaymentDetails;
import com.realestate.bean.CustomerReceiptForm;
import com.realestate.bean.Enquiry;
import com.realestate.bean.ExtraCharges;
import com.realestate.bean.Login;
import com.realestate.bean.MultipleOtherCharges;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.AggreementRepository;
import com.realestate.repository.BookingCancelFormRepository;
import com.realestate.repository.BookingRepository;
import com.realestate.repository.CustomerLoanDetailsRepository;
import com.realestate.repository.CustomerOtherPaymentDetailsRepository;
import com.realestate.repository.CustomerReceiptFormRepository;
import com.realestate.repository.ExtraChargeRepository;
import com.realestate.repository.ProjectRepository;

@Controller
@RequestMapping("/")
public class ExtraChargeController 
{
	@Autowired 
	AggreementRepository aggreementRepository;
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired 
	ExtraChargeRepository extrachargesRepository;
	@Autowired
	CustomerLoanDetailsRepository customerLoandetailsRepository;
	@Autowired
	BookingCancelFormRepository bookingcancelformRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired 
	BookingRepository bookingRepository;
	@Autowired
	CustomerOtherPaymentDetailsRepository customerOtherPaymentDetailsRepository;
	@Autowired
	CustomerReceiptFormRepository customerreceiptformrepository;

	String Code;
	private String extraCode()
	{
		long Count = extrachargesRepository.count();

		if(Count<10)
		{
			Code = "EC000"+(Count+1);
		}
		else if((Count>=10) && (Count<100))
		{
			Code = "EC00"+(Count+1);
		}
		else if((Count>=100) && (Count<1000))
		{
			Code = "EC0"+(Count+1);
		}
		else
		{
			Code = "EC"+(Count+1);
		}

		return Code;
	}

	String payAmountCode;
	private String customerpayAmountCode()
	{
		long Count = customerOtherPaymentDetailsRepository.count();

		if(Count<10)
		{
			payAmountCode = "TP000"+(Count+1);
		}
		else if((Count>=10) && (Count<100))
		{
			payAmountCode = "TP00"+(Count+1);
		}
		else if((Count>=100) && (Count<1000))
		{
			payAmountCode = "TP0"+(Count+1);
		}
		else if(Count>1000)
		{
			payAmountCode = "TP"+(Count+1);
		}

		return payAmountCode;
	}

	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping("AddExtraCharges")
	public String ExtraCharges(@RequestParam("bookingId") String bookingId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<Booking> bookingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)),Booking.class);

			query = new Query();
			ProjectWing wingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(bookingDetails.get(0).getProjectId()).and("buildingId").is(bookingDetails.get(0).getBuildingId()).and("wingId").is(bookingDetails.get(0).getWingId())), ProjectWing.class);

			query = new Query();
			List<MultipleOtherCharges> multipleOtherCharges = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)),MultipleOtherCharges.class);


			model.addAttribute("extraCode", extraCode());
			model.addAttribute("bookingDetails", bookingDetails);
			model.addAttribute("multipleOtherCharges",multipleOtherCharges);
			model.addAttribute("wingDetails", wingDetails);

			return "AddExtraCharges";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/AddExtraCharges", method = RequestMethod.POST)
	public String ExtraChargesSave(Model model, @ModelAttribute ExtraCharges extracharges, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			extracharges = new ExtraCharges(extracharges.getChargesId(),extracharges.getBookingId(),extracharges.getBookingDate(),extracharges.getCustomerName(),extracharges.getAggreementValue1(),extracharges.getMaintenanceCost(),extracharges.getOtherCharges(),extracharges.getCreationDate(),extracharges.getUpdateDate(),extracharges.getUserName());
			extrachargesRepository.save(extracharges);


			List<Enquiry> enquiryList;
			List<Project> projectList = GetUserAssigenedProjectList(req,res); 
			List<Booking> bookingList; 
			List<Aggreement> aggreementList = aggreementRepository.findAll();
			List<ExtraCharges> extrachargesList = extrachargesRepository.findAll(); 
			List<CustomerLoanDetails> loanList = customerLoandetailsRepository.findAll();
			List<BookingCancelForm> cancelList = bookingcancelformRepository.findAll();

			Query query = new Query();
			enquiryList = mongoTemplate.find(query.addCriteria(Criteria.where("enqStatus").is("Booking")), Enquiry.class);

			Query query1 = new Query();
			bookingList = mongoTemplate.find(query1.addCriteria(Criteria.where("bookingstatus").ne("Cancel")), Booking.class);

			model.addAttribute("aggreementList",aggreementList);
			model.addAttribute("cancelList",cancelList);
			model.addAttribute("loanList",loanList);
			model.addAttribute("extrachargesList",extrachargesList);
			model.addAttribute("bookingList", bookingList);
			model.addAttribute("projectList",projectList);
			model.addAttribute("enquiryList", enquiryList);

			return "BookingMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/ExtraChargesList")
	public String ExtraChargesList(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<ExtraCharges> extrachargesList = extrachargesRepository.findAll(); 

			model.addAttribute("extrachargesList",extrachargesList);
			return "ExtraChargesList";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/EditExtraCharges")
	public String EditExtraCharges(@RequestParam("chargesId") String chargesId,ModelMap model)
	{
		try {
			Query query = new  Query();
			List<ExtraCharges> extrachargesList = mongoTemplate.find(query.addCriteria(Criteria.where("chargesId").is(chargesId)),ExtraCharges.class);

			query = new Query();
			List<MultipleOtherCharges> multipleOtherCharges = mongoTemplate.find(query.addCriteria(Criteria.where("chargesId").is(chargesId)), MultipleOtherCharges.class);

			model.addAttribute("extrachargesList", extrachargesList);
			model.addAttribute("multipleOtherCharges", multipleOtherCharges);

			return "EditExtraCharges";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/EditExtraCharges", method=RequestMethod.POST)
	public String EditExtraCha(@ModelAttribute ExtraCharges extracharges, Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				extracharges = new ExtraCharges(extracharges.getChargesId(),extracharges.getBookingId(),extracharges.getBookingDate(),extracharges.getCustomerName(),extracharges.getAggreementValue1(),extracharges.getMaintenanceCost(),extracharges.getOtherCharges(),extracharges.getCreationDate(),extracharges.getUpdateDate(),extracharges.getUserName());
				extrachargesRepository.save(extracharges);
			}
			catch(DuplicateKeyException de)
			{
				//model.addAttribute("bankStatus","Fail");
			}

			int i=0,j=0;
			Query query = new Query();
			List<Project> projectList = GetUserAssigenedProjectList(req,res); 
			List<Booking> bookingList1 = mongoTemplate.find(query.addCriteria(Criteria.where("bookingstatus").ne("Cancel")), Booking.class);


			List<Booking> bookingList=new ArrayList<Booking>();
			Booking booking=new Booking();

			for(i=0;i<projectList.size();i++)
			{

				for( j=0;j<bookingList1.size();j++)
				{
					if(projectList.get(i).getProjectId().equals(bookingList1.get(j).getProjectId()))
					{

						booking=new Booking();

						booking.setBookingId(bookingList1.get(j).getBookingId());
						booking.setEnquiryId(bookingList1.get(j).getEnquiryId());
						booking.setBookingfirstname(bookingList1.get(j).getBookingfirstname());
						booking.setBookingmiddlename(bookingList1.get(j).getBookingmiddlename());
						booking.setBookinglastname(bookingList1.get(j).getBookinglastname());
						booking.setBookingaddress(bookingList1.get(j).getBookingaddress());
						booking.setBookingPincode(bookingList1.get(j).getBookingPincode());

						booking.setBookingEmail(bookingList1.get(j).getBookingEmail());
						booking.setBookingmobileNumber1(bookingList1.get(j).getBookingmobileNumber1());
						booking.setBookingmobileNumber2(bookingList1.get(j).getBookingmobileNumber2());
						booking.setBookingOccupation(bookingList1.get(j).getBookingOccupation());
						booking.setPurposeOfFlat(bookingList1.get(j).getPurposeOfFlat());

						booking.setProjectId(bookingList1.get(j).getProjectId());
						booking.setBuildingId(bookingList1.get(j).getBuildingId());
						booking.setWingId(bookingList1.get(j).getWingId());
						booking.setFloorId(bookingList1.get(j).getFloorId());
						booking.setFlatType(bookingList1.get(j).getFlatType());
						booking.setFlatId(bookingList1.get(j).getFlatId());
						booking.setFlatFacing(bookingList1.get(j).getFlatFacing());
						booking.setFlatareainSqFt(bookingList1.get(j).getFlatareainSqFt());
						booking.setFlatCostwithotfloorise(bookingList1.get(j).getFlatCostwithotfloorise());
						booking.setFloorRise(bookingList1.get(j).getFloorRise());
						booking.setFlatCost(bookingList1.get(j).getFlatCost());
						booking.setFlatbasicCost(bookingList1.get(j).getFlatbasicCost());
						booking.setParkingFloorId(bookingList1.get(j).getParkingFloorId());
						booking.setParkingZoneId(bookingList1.get(j).getParkingZoneId());
						booking.setAgentId(bookingList1.get(j).getAgentId());

						booking.setInfrastructureCharge(bookingList1.get(j).getInfrastructureCharge());
						booking.setAggreementValue1(bookingList1.get(j).getAggreementValue1());
						booking.setHandlingCharges(bookingList1.get(j).getHandlingCharges());
						booking.setStampDuty1(bookingList1.get(j).getStampDuty1());
						booking.setStampDutyPer(bookingList1.get(j).getStampDutyPer());
						booking.setRegistrationPer(bookingList1.get(j).getRegistrationPer());
						booking.setRegistrationCost1(bookingList1.get(j).getRegistrationCost1());
						booking.setGstCost(bookingList1.get(j).getGstCost());
						booking.setGstAmount1(bookingList1.get(j).getGstAmount1());
						booking.setGrandTotal1(bookingList1.get(j).getGrandTotal1());
						booking.setTds(bookingList1.get(j).getTds());
						booking.setBookingstatus(bookingList1.get(j).getBookingstatus());
						booking.setUserName(bookingList1.get(j).getUserName());
						booking.setCreationDate(bookingList1.get(j).getCreationDate());

						bookingList.add(booking);

					}

				}

			}

			List<ExtraCharges> extrachargesList1 = extrachargesRepository.findAll(); 

			List<ExtraCharges> extrachargesList=new ArrayList<ExtraCharges>();
			ExtraCharges extracharges1=new ExtraCharges();

			for(i=0;i<bookingList.size();i++)
			{
				for(j=0;j<extrachargesList1.size();j++)
				{
					if(bookingList.get(i).getBookingId().equals(extrachargesList1.get(j).getBookingId()))
					{
						extracharges1=new ExtraCharges();
						extracharges1.setChargesId(extrachargesList1.get(j).getChargesId());
						extracharges1.setBookingId(extrachargesList1.get(j).getBookingId());
						extracharges1.setBookingDate(extrachargesList1.get(j).getBookingDate());
						extracharges1.setCustomerName(extrachargesList1.get(j).getCustomerName());
						extracharges1.setAggreementValue1(extrachargesList1.get(j).getAggreementValue1());
						extracharges1.setMaintenanceCost(extrachargesList1.get(j).getMaintenanceCost());
						extracharges1.setOtherCharges(extrachargesList1.get(j).getOtherCharges());
						extracharges1.setCreationDate(extrachargesList1.get(j).getCreationDate());
						extracharges1.setUpdateDate(extrachargesList1.get(j).getUpdateDate());
						extracharges1.setUserName(extrachargesList1.get(j).getUserName());

						extrachargesList.add(extracharges1);

					}

				}
			}

			model.addAttribute("extrachargesList",extrachargesList);
			return "AllCustomerExtraChargesList";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/CustomerOtherPaymentsDetails")
	public String CustomerOtherPaymentDetails(@RequestParam("chargesId") String chargesId, Model model)
	{
		try {
			//long totalMaintAmt = 0, totalHandlingAmt = 0, totalExtraCharge = 0;
			long reamaingMaintainance=0, reamaingHandling=0, reamaingExtraCharges=0;

			Query query = new Query();
			List<ExtraCharges> extraChargesDetails = mongoTemplate.find(query.addCriteria(Criteria.where("chargesId").is(chargesId)), ExtraCharges.class); 

			query = new Query();
			Booking bookingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("bookingId").is(extraChargesDetails.get(0).getBookingId())), Booking.class);

			query = new Query();
			List<MultipleOtherCharges> multipleOtherCharges = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingDetails.getBookingId())),MultipleOtherCharges.class);

			query = new Query();
			List<CustomerOtherPaymentDetails> customerPayment = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingDetails.getBookingId())),CustomerOtherPaymentDetails.class);

			if(customerPayment.size()==0)
			{
				CustomerOtherPaymentDetails customerOtherPaymentDetails = new CustomerOtherPaymentDetails();

				customerOtherPaymentDetails.setTotalPaidMaintainanceAmt(0);
				customerOtherPaymentDetails.setTotalPaidHandlingCharges(0);
				customerOtherPaymentDetails.setTotalPaidExtraCharges(0);

				customerPayment.add(customerOtherPaymentDetails);

				if(extraChargesDetails.size()!=0)
				{
					reamaingMaintainance = (long)(extraChargesDetails.get(0).getMaintenanceCost() - customerPayment.get(0).getTotalPaidMaintainanceAmt());
				}

				if(bookingDetails!=null)
				{
					reamaingHandling = (long)(bookingDetails.getHandlingCharges()-customerPayment.get(0).getTotalPaidHandlingCharges());
				}

				long totalExtraCharges = 0;

				if(multipleOtherCharges.size()!=0)
				{
					for(int i=0;i<multipleOtherCharges.size();i++)
					{
						totalExtraCharges = (long) (totalExtraCharges + multipleOtherCharges.get(i).getAmount());
					}
				}

				if(totalExtraCharges != 0)
				{
					reamaingExtraCharges = (long) (totalExtraCharges - customerPayment.get(0).getTotalPaidExtraCharges());
				}

			}
			else
			{
				if(extraChargesDetails.size()!=0)
				{
					reamaingMaintainance = (long)(extraChargesDetails.get(0).getMaintenanceCost() - customerPayment.get(0).getTotalPaidMaintainanceAmt());
				}

				if(bookingDetails!=null)
				{
					reamaingHandling = (long)(bookingDetails.getHandlingCharges()-customerPayment.get(0).getTotalPaidHandlingCharges());
				}

				long totalExtraCharges = 0;

				if(multipleOtherCharges.size()!=0)
				{
					for(int i=0;i<multipleOtherCharges.size();i++)
					{
						totalExtraCharges = (long) (totalExtraCharges + multipleOtherCharges.get(i).getAmount());
					}
				}

				if(totalExtraCharges != 0)
				{
					reamaingExtraCharges = (long) (totalExtraCharges - customerPayment.get(0).getTotalPaidExtraCharges());
				}

			}

			long totalExtraCharges = 0;

			if(multipleOtherCharges.size()!=0)
			{
				for(int i=0;i<multipleOtherCharges.size();i++)
				{
					totalExtraCharges = (long) (totalExtraCharges + multipleOtherCharges.get(i).getAmount());
				}
			}

			model.addAttribute("totalMaintCost",extraChargesDetails.get(0).getMaintenanceCost().longValue());
			model.addAttribute("bookingId", bookingDetails.getBookingId());
			model.addAttribute("customerName", bookingDetails.getBookingfirstname());
			model.addAttribute("bookingDetails", bookingDetails);
			model.addAttribute("totalHandlingCharge", bookingDetails.getHandlingCharges().longValue());
			model.addAttribute("totalExtraCharges", totalExtraCharges);
			model.addAttribute("customerPayment",customerPayment);
			model.addAttribute("reamaingMaintainance", reamaingMaintainance);
			model.addAttribute("reamaingHandling", reamaingHandling);
			model.addAttribute("reamaingExtraCharges", reamaingExtraCharges);

			return "CustomerOtherPaymentsDetails";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/CustomerOtherPaymentsDetails", method=RequestMethod.POST)
	public String AddCustomerOtherPaymentDetails(@RequestParam("bookingId") String bookingId, @RequestParam("maintainanceAmt") double maintainanceAmt, @RequestParam("handlingChargesAmt") double handlingChargesAmt, @RequestParam("extraChargesAmt") double extraChargesAmt, @RequestParam("paymentMode") String paymentMode, @RequestParam("bankName") String bankName, @RequestParam("branchName") String branchName, @RequestParam("chequeNumber") String chequeNumber, @RequestParam("narration") String narration, HttpSession session, Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			//for getting handling charges from booking
			Query query = new Query();
			Booking bookingList = mongoTemplate.findOne(query.addCriteria((Criteria.where("bookingId").is(bookingId))), Booking.class);

			//for getting extra charges
			query = new Query();
			ExtraCharges extraCharges = mongoTemplate.findOne(query.addCriteria(Criteria.where("bookingId").is(bookingId)), ExtraCharges.class);

			/*String projectName=bookingList.getProjectName();
		     String buildingName=bookingList.getBuildingName();
		     String wingName=bookingList.getWingName();*/

			String one = "REC";
			one = one +"/"+bookingId+"/";
			String receiptCode="";

			Date todayDate = new Date();  

			CustomerReceiptForm customerreceiptform = new CustomerReceiptForm();

			Double paymentAmount;
			String paymentType;

			if(maintainanceAmt!=0.0)
			{
				query = new Query();
				long receiptCount = mongoTemplate.count(query.addCriteria(Criteria.where("bookingId").is(bookingId)), CustomerReceiptForm.class);
				if(receiptCount<10)
				{
					receiptCode = one+"0"+(receiptCount+1);
				}
				else 
				{
					receiptCode = one+(receiptCount+1);
				}

				paymentType="Maintainance";
				paymentAmount=maintainanceAmt;
				customerreceiptform.setReceiptId(receiptCode);
				customerreceiptform.setBookingId(bookingId);
				customerreceiptform.setPaymentAmount(paymentAmount);
				customerreceiptform.setPaymentType(paymentType);
				customerreceiptform.setPaymentMode(paymentMode);
				customerreceiptform.setBankName(bankName);
				customerreceiptform.setBranchName(branchName);
				customerreceiptform.setChequeNumber(chequeNumber);
				customerreceiptform.setNarration(narration);
				customerreceiptform.setCreationDate(todayDate);
				customerreceiptformrepository.save(customerreceiptform);
			}

			if(handlingChargesAmt!=0.0)
			{
				query = new Query();
				long receiptCount = mongoTemplate.count(query.addCriteria(Criteria.where("bookingId").is(bookingId)), CustomerReceiptForm.class);
				if(receiptCount<10)
				{
					receiptCode = one+"0"+(receiptCount+1);
				}
				else 
				{
					receiptCode = one+(receiptCount+1);
				}
				customerreceiptform = new CustomerReceiptForm();
				paymentType="Handling Charge";
				paymentAmount=handlingChargesAmt;
				customerreceiptform.setReceiptId(receiptCode);
				customerreceiptform.setBookingId(bookingId);
				customerreceiptform.setPaymentAmount(paymentAmount);
				customerreceiptform.setPaymentType(paymentType);
				customerreceiptform.setPaymentMode(paymentMode);
				customerreceiptform.setBankName(bankName);
				customerreceiptform.setBranchName(branchName);
				customerreceiptform.setChequeNumber(chequeNumber);
				customerreceiptform.setNarration(narration);
				customerreceiptform.setCreationDate(todayDate);
				customerreceiptformrepository.save(customerreceiptform);
			}
			if(extraChargesAmt!=0.0)
			{
				query = new Query();

				long receiptCount = mongoTemplate.count(query.addCriteria(Criteria.where("bookingId").is(bookingId)), CustomerReceiptForm.class);

				if(receiptCount<10)
				{
					receiptCode = one+"0"+(receiptCount+1);
				}
				else 
				{
					receiptCode = one+(receiptCount+1);
				}

				customerreceiptform = new CustomerReceiptForm();

				paymentType="Extra Charge";

				paymentAmount=extraChargesAmt;

				customerreceiptform.setReceiptId(receiptCode);
				customerreceiptform.setBookingId(bookingId);
				customerreceiptform.setPaymentAmount(paymentAmount);
				customerreceiptform.setPaymentType(paymentType);
				customerreceiptform.setPaymentMode(paymentMode);
				customerreceiptform.setBankName(bankName);
				customerreceiptform.setBranchName(branchName);
				customerreceiptform.setChequeNumber(chequeNumber);
				customerreceiptform.setNarration(narration);
				customerreceiptform.setCreationDate(todayDate);
				customerreceiptformrepository.save(customerreceiptform);
			}

			Double totalPaidMaintainanceAmt=0.0;
			Double totalPaidHandlingCharges=0.0;
			Double totalPaidExtraCharges=0.0;

			query = new Query();
			List<CustomerOtherPaymentDetails> customerOtherPaymentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)),CustomerOtherPaymentDetails.class);

			if(customerOtherPaymentDetails.size()!=0)
			{
				totalPaidMaintainanceAmt = customerOtherPaymentDetails.get(0).getTotalPaidMaintainanceAmt() + maintainanceAmt;
				totalPaidHandlingCharges = customerOtherPaymentDetails.get(0).getTotalPaidHandlingCharges() + handlingChargesAmt;
				totalPaidExtraCharges    = customerOtherPaymentDetails.get(0).getTotalPaidExtraCharges() + extraChargesAmt;

				long totalExtraCharges = 0;

				query = new Query();
				List<MultipleOtherCharges> multipleOtherCharges = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)), MultipleOtherCharges.class);

				for(int i=0;i<multipleOtherCharges.size();i++)
				{
					totalExtraCharges = (long) (totalExtraCharges + multipleOtherCharges.get(i).getAmount());
				}

				String amountStatus;

				if(extraCharges.getMaintenanceCost()==totalPaidMaintainanceAmt && bookingList.getHandlingCharges()==totalPaidHandlingCharges && totalExtraCharges==totalPaidExtraCharges)
				{
					amountStatus="Completed";
				}
				else
				{
					amountStatus="Pending";
				}
				try
				{
					customerOtherPaymentDetails.get(0).setTotalPaidMaintainanceAmt(totalPaidMaintainanceAmt.longValue());
					customerOtherPaymentDetails.get(0).setTotalPaidHandlingCharges(totalPaidHandlingCharges.longValue());
					customerOtherPaymentDetails.get(0).setTotalPaidExtraCharges(totalPaidExtraCharges.longValue());
					customerOtherPaymentDetails.get(0).setAmountStatus(amountStatus);
					customerOtherPaymentDetails.get(0).setCreationDate(todayDate);
					customerOtherPaymentDetails.get(0).setUpdateDate(todayDate);
					customerOtherPaymentDetailsRepository.save(customerOtherPaymentDetails);
				}
				catch(Exception ee)
				{

				}
			}
			else
			{
				long totalExtraCharges = 0;

				query = new Query();
				List<MultipleOtherCharges> multipleOtherCharges = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)), MultipleOtherCharges.class);

				for(int i=0;i<multipleOtherCharges.size();i++)
				{
					totalExtraCharges = (long) (totalExtraCharges + multipleOtherCharges.get(i).getAmount());
				}

				String amountStatus;
				if(extraCharges.getMaintenanceCost()==maintainanceAmt && bookingList.getHandlingCharges()==handlingChargesAmt && totalExtraCharges==extraChargesAmt)
				{
					amountStatus="Completed";
				}
				else
				{
					amountStatus="Pending";
				}

				CustomerOtherPaymentDetails customerpaymentdetails1 = new CustomerOtherPaymentDetails();
				customerpaymentdetails1.setPaymentId(customerpayAmountCode());
				customerpaymentdetails1.setBookingId(bookingId);
				customerpaymentdetails1.setTotalPaidMaintainanceAmt((long)maintainanceAmt);;
				customerpaymentdetails1.setTotalPaidHandlingCharges((long)handlingChargesAmt);;
				customerpaymentdetails1.setTotalPaidExtraCharges((long)extraChargesAmt);;
				customerpaymentdetails1.setAmountStatus(amountStatus);
				customerpaymentdetails1.setCreationDate(todayDate);
				customerpaymentdetails1.setUpdateDate(todayDate);

				customerOtherPaymentDetailsRepository.save(customerpaymentdetails1);
			}




			query = new Query();
			List<Project> projectList = GetUserAssigenedProjectList(req,res); 
			List<Booking> bookingList1 = mongoTemplate.find(query.addCriteria(Criteria.where("bookingstatus").ne("Cancel")), Booking.class);

			List<Booking> bookingList2=new ArrayList<Booking>();
			Booking booking=new Booking();

			for(int i=0;i<projectList.size();i++)
			{

				for(int j=0;j<bookingList1.size();j++)
				{
					if(projectList.get(i).getProjectId().equals(bookingList1.get(j).getProjectId()))
					{
						booking=new Booking();
						booking.setBookingId(bookingList1.get(j).getBookingId());
						booking.setEnquiryId(bookingList1.get(j).getEnquiryId());
						booking.setBookingfirstname(bookingList1.get(j).getBookingfirstname());
						booking.setBookingmiddlename(bookingList1.get(j).getBookingmiddlename());
						booking.setBookinglastname(bookingList1.get(j).getBookinglastname());
						booking.setBookingaddress(bookingList1.get(j).getBookingaddress());
						booking.setBookingPincode(bookingList1.get(j).getBookingPincode());

						booking.setBookingEmail(bookingList1.get(j).getBookingEmail());
						booking.setBookingmobileNumber1(bookingList1.get(j).getBookingmobileNumber1());
						booking.setBookingmobileNumber2(bookingList1.get(j).getBookingmobileNumber2());
						booking.setBookingOccupation(bookingList1.get(j).getBookingOccupation());
						booking.setPurposeOfFlat(bookingList1.get(j).getPurposeOfFlat());

						booking.setProjectId(bookingList1.get(j).getProjectId());
						booking.setBuildingId(bookingList1.get(j).getBuildingId());
						booking.setWingId(bookingList1.get(j).getWingId());
						booking.setFloorId(bookingList1.get(j).getFloorId());
						booking.setFlatType(bookingList1.get(j).getFlatType());
						booking.setFlatId(bookingList1.get(j).getFlatId());
						booking.setFlatFacing(bookingList1.get(j).getFlatFacing());
						booking.setFlatareainSqFt(bookingList1.get(j).getFlatareainSqFt());
						booking.setFlatCostwithotfloorise(bookingList1.get(j).getFlatCostwithotfloorise());
						booking.setFloorRise(bookingList1.get(j).getFloorRise());
						booking.setFlatCost(bookingList1.get(j).getFlatCost());
						booking.setFlatbasicCost(bookingList1.get(j).getFlatbasicCost());
						booking.setParkingFloorId(bookingList1.get(j).getParkingFloorId());
						booking.setParkingZoneId(bookingList1.get(j).getParkingZoneId());
						booking.setAgentId(bookingList1.get(j).getAgentId());

						booking.setInfrastructureCharge(bookingList1.get(j).getInfrastructureCharge());
						booking.setAggreementValue1(bookingList1.get(j).getAggreementValue1());
						booking.setHandlingCharges(bookingList1.get(j).getHandlingCharges());
						booking.setStampDuty1(bookingList1.get(j).getStampDuty1());
						booking.setStampDutyPer(bookingList1.get(j).getStampDutyPer());
						booking.setRegistrationPer(bookingList1.get(j).getRegistrationPer());
						booking.setRegistrationCost1(bookingList1.get(j).getRegistrationCost1());
						booking.setGstCost(bookingList1.get(j).getGstCost());
						booking.setGstAmount1(bookingList1.get(j).getGstAmount1());
						booking.setGrandTotal1(bookingList1.get(j).getGrandTotal1());
						booking.setTds(bookingList1.get(j).getTds());
						booking.setBookingstatus(bookingList1.get(j).getBookingstatus());
						booking.setUserName(bookingList1.get(j).getUserName());
						booking.setCreationDate(bookingList1.get(j).getCreationDate());

						bookingList2.add(booking);

					}

				}

			}

			List<ExtraCharges> extrachargesList1 = extrachargesRepository.findAll(); 

			List<ExtraCharges> extrachargesList=new ArrayList<ExtraCharges>();
			ExtraCharges extracharges1=new ExtraCharges();

			for(int i=0;i<bookingList2.size();i++)
			{
				for(int j=0;j<extrachargesList1.size();j++)
				{
					if(bookingList2.get(i).getBookingId().equals(extrachargesList1.get(j).getBookingId()))
					{
						extracharges1=new ExtraCharges();
						extracharges1.setChargesId(extrachargesList1.get(j).getChargesId());
						extracharges1.setBookingId(extrachargesList1.get(j).getBookingId());
						extracharges1.setBookingDate(extrachargesList1.get(j).getBookingDate());
						extracharges1.setCustomerName(extrachargesList1.get(j).getCustomerName());
						extracharges1.setAggreementValue1(extrachargesList1.get(j).getAggreementValue1());
						extracharges1.setMaintenanceCost(extrachargesList1.get(j).getMaintenanceCost());
						extracharges1.setOtherCharges(extrachargesList1.get(j).getOtherCharges());
						extracharges1.setCreationDate(extrachargesList1.get(j).getCreationDate());
						extracharges1.setUpdateDate(extrachargesList1.get(j).getUpdateDate());
						extracharges1.setUserName(extrachargesList1.get(j).getUserName());

						extrachargesList.add(extracharges1);

					}

				}
			}

			model.addAttribute("extrachargesList",extrachargesList);

			return "AllCustomerExtraChargesList";

		}catch (Exception e) {
			return "login";
		}
	}

}

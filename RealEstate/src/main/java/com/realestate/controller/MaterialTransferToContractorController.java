package com.realestate.controller;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Brand;
import com.realestate.bean.City;
import com.realestate.bean.Company;
import com.realestate.bean.Contractor;
import com.realestate.bean.ContractorEmployees;
import com.realestate.bean.Country;
import com.realestate.bean.Employee;
import com.realestate.bean.Item;
import com.realestate.bean.LocationArea;
import com.realestate.bean.Login;
import com.realestate.bean.MaterialTransferToContractor;
import com.realestate.bean.MaterialTransferToContractorHistory;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.State;
import com.realestate.bean.Stock;
import com.realestate.bean.Store;
import com.realestate.bean.SubSupplierType;
import com.realestate.bean.SupplierType;
import com.realestate.bean.UserAssignedProject;
import com.realestate.configuration.CommanController;
import com.realestate.repository.ContractorRepository;
import com.realestate.repository.MaterialTransferToContractorHistoryRepository;
import com.realestate.repository.MaterialTransferToContractorRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.StockRepository;
import com.realestate.repository.StoreRepository;
import com.realestate.repository.SupplierTypeRepository;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;



@Controller
@RequestMapping("/")
public class MaterialTransferToContractorController {

	@Autowired
	ContractorRepository contractorRepository;
	@Autowired
	StoreRepository storeRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	SupplierTypeRepository suppliertypeRepository;
	@Autowired
	MaterialTransferToContractorHistoryRepository materialTransfertocontractorhistoryRepository;
	@Autowired
	MaterialTransferToContractorRepository materialtransferTocontractorRepository;
	@Autowired
	StockRepository stockRepository;
	@Autowired
	MongoTemplate mongoTemplate;


	String transferCode;
	private String MaterialTransferCode()
	{
		long cnt  = materialtransferTocontractorRepository.count();
		if(cnt<10)
		{
			transferCode = "MTC0000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			transferCode = "MTC000"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			transferCode = "MTC00"+(cnt+1);
		}
		else if((cnt<10000) && (cnt>=1000))
		{
			transferCode = "MTC0"+(cnt+1);
		}
		else
		{
			transferCode = "MTC"+(cnt+1);
		}
		return transferCode;
	}

	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping("/MaterialTransferToContractorMaster")
	public String MaterialTransferToContractorMaster(Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			Query query = new Query();
			List<Project> projectList = GetUserAssigenedProjectList(req,res);
			List<MaterialTransferToContractor> materialList = materialtransferTocontractorRepository.findAll();

			MaterialTransferToContractor materialtransfertocontractor=new MaterialTransferToContractor();

			List<MaterialTransferToContractor> transferList = new ArrayList<MaterialTransferToContractor>();


			for(int i=0;i<materialList.size();i++)
			{
				List<Contractor> contractorList= new ArrayList<Contractor>();
				List<ContractorEmployees> contractoremployeeList= new ArrayList<ContractorEmployees>();

				try 
				{ 
					materialtransfertocontractor=new MaterialTransferToContractor();

					query = new Query();
					contractorList = mongoTemplate.find(query.addCriteria(Criteria.where("contractorId").is(materialList.get(i).getContractorId())), Contractor.class);

					query = new Query();
					contractoremployeeList = mongoTemplate.find(query.addCriteria(Criteria.where("contractorEmployeeId").is(materialList.get(i).getContractorEmployeeId())), ContractorEmployees.class);

					if(contractorList.size()!=0 && contractoremployeeList.size()!=0)
					{
						materialtransfertocontractor.setMaterialTransferId(materialList.get(i).getMaterialTransferId());
						materialtransfertocontractor.setContractorfirmName(contractorList.get(0).getContractorfirmName());
						materialtransfertocontractor.setEmployeeName(contractoremployeeList.get(0).getEmployeeName());
						materialtransfertocontractor.setProjectId(materialList.get(i).getProjectId());
						materialtransfertocontractor.setBuildingId(materialList.get(i).getBuildingId());
						materialtransfertocontractor.setWingId(materialList.get(i).getWingId());
						materialtransfertocontractor.setCreationDate(materialList.get(i).getCreationDate());

						query = new Query();
						Project project=mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(materialList.get(i).getProjectId())), Project.class);
						materialtransfertocontractor.setProjectId(project.getProjectName());

						query = new Query();
						ProjectBuilding projectBuilding=mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(materialList.get(i).getBuildingId())),ProjectBuilding.class);
						materialtransfertocontractor.setBuildingId(projectBuilding.getBuildingName());
						query = new Query();
						ProjectWing projectWing=mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(materialList.get(i).getWingId())), ProjectWing.class);
						materialtransfertocontractor.setWingId(projectWing.getWingName());

						transferList.add(materialtransfertocontractor);
					}
				}
				catch (Exception e) {
				}	  
			}

			model.addAttribute("materialList", transferList);
			model.addAttribute("projectList", projectList);
			return "MaterialTransferToContractorMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/AddMaterialTransferToContractor")
	public String AddMaterialTransferToContractor(Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<Store> storeList = storeRepository.findAll();
			List<Project> projectList = GetUserAssigenedProjectList(req,res);
			List<Contractor> contractorList = contractorRepository.findAll();  
			List<SupplierType> suppliertypeList = suppliertypeRepository.findAll();

			model.addAttribute("transferCode",MaterialTransferCode());

			model.addAttribute("suppliertypeList",suppliertypeList);
			model.addAttribute("contractorList", contractorList);
			model.addAttribute("projectList", projectList);
			model.addAttribute("storeList", storeList);

			return "AddMaterialTransferToContractor";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value = "/AddMaterialTransferToContractor", method = RequestMethod.POST)
	public String SaveMaterialTransferToContractor(@ModelAttribute MaterialTransferToContractor materialtransfercontractor, Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				materialtransfercontractor = new MaterialTransferToContractor(materialtransfercontractor.getMaterialTransferId(), materialtransfercontractor.getStoreId(), 
						materialtransfercontractor.getContractorId(), materialtransfercontractor.getContractorEmployeeId(), 
						materialtransfercontractor.getProjectId(), materialtransfercontractor.getBuildingId(), materialtransfercontractor.getWingId(), 
						materialtransfercontractor.getCreationDate(), materialtransfercontractor.getUpdateDate(), materialtransfercontractor.getUserName());
				materialtransferTocontractorRepository.save(materialtransfercontractor);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}

			Query query = new Query();
			List<Project> projectList = GetUserAssigenedProjectList(req,res);
			List<MaterialTransferToContractor> materialList = materialtransferTocontractorRepository.findAll();

			MaterialTransferToContractor materialtransfertocontractor=new MaterialTransferToContractor();

			List<MaterialTransferToContractor> transferList = new ArrayList<MaterialTransferToContractor>();


			for(int i=0;i<materialList.size();i++)
			{
				List<Contractor> contractorList= new ArrayList<Contractor>();
				List<ContractorEmployees> contractoremployeeList= new ArrayList<ContractorEmployees>();

				try 
				{ 
					materialtransfertocontractor=new MaterialTransferToContractor();

					query = new Query();
					contractorList = mongoTemplate.find(query.addCriteria(Criteria.where("contractorId").is(materialList.get(i).getContractorId())), Contractor.class);

					query = new Query();
					contractoremployeeList = mongoTemplate.find(query.addCriteria(Criteria.where("contractorEmployeeId").is(materialList.get(i).getContractorEmployeeId())), ContractorEmployees.class);

					if(contractorList.size()!=0 && contractoremployeeList.size()!=0)
					{
						materialtransfertocontractor.setMaterialTransferId(materialList.get(i).getMaterialTransferId());
						materialtransfertocontractor.setContractorfirmName(contractorList.get(0).getContractorfirmName());
						materialtransfertocontractor.setEmployeeName(contractoremployeeList.get(0).getEmployeeName());
						materialtransfertocontractor.setProjectId(materialList.get(i).getProjectId());
						materialtransfertocontractor.setBuildingId(materialList.get(i).getBuildingId());
						materialtransfertocontractor.setWingId(materialList.get(i).getWingId());
						materialtransfertocontractor.setCreationDate(materialList.get(i).getCreationDate());

						query = new Query();
						Project project=mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(materialList.get(i).getProjectId())), Project.class);
						materialtransfertocontractor.setProjectId(project.getProjectName());

						query = new Query();
						ProjectBuilding projectBuilding=mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(materialList.get(i).getBuildingId())),ProjectBuilding.class);
						materialtransfertocontractor.setBuildingId(projectBuilding.getBuildingName());
						query = new Query();
						ProjectWing projectWing=mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(materialList.get(i).getWingId())), ProjectWing.class);
						materialtransfertocontractor.setWingId(projectWing.getWingName());

						transferList.add(materialtransfertocontractor);
					}
				}
				catch (Exception e) {
				}	  
			}

			model.addAttribute("materialList", transferList);
			model.addAttribute("projectList", projectList);
			return "MaterialTransferToContractorMaster";

		}catch (Exception e) {
			return "login";
		}
	}



	@RequestMapping("/EditMaterialTransferToContractor")
	public String EditMaterialTransferToContractor(@RequestParam("materialTransferId") String materialTransferId, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<Store> storeList = storeRepository.findAll();
			List<Project> projectList = GetUserAssigenedProjectList(req,res);
			List<Contractor> contractorList = contractorRepository.findAll();
			List<SupplierType> suppliertypeList = suppliertypeRepository.findAll();

			Query query3 = new Query();
			List<MaterialTransferToContractorHistory> contractorMaterialList = mongoTemplate.find(query3.addCriteria(Criteria.where("materialTransferId").is(materialTransferId)), MaterialTransferToContractorHistory.class); 

			Query query = new Query();
			List<MaterialTransferToContractor> materialList = mongoTemplate.find(query.addCriteria(Criteria.where("materialTransferId").is(materialTransferId)),MaterialTransferToContractor.class);

			Query query1 = new Query();
			List<Contractor> contractorDetails = mongoTemplate.find(query1.addCriteria(Criteria.where("contractorId").is(materialList.get(0).getContractorId())),Contractor.class);

			Query query2 = new Query();
			List<ContractorEmployees> contractorEmployeeDetails = mongoTemplate.find(query2.addCriteria(Criteria.where("contractorEmployeeId").is(materialList.get(0).getContractorEmployeeId())),ContractorEmployees.class);


			MaterialTransferToContractor materialtransfertocontractor=new MaterialTransferToContractor();

			List<MaterialTransferToContractor> transferList = new ArrayList<MaterialTransferToContractor>();


			materialtransfertocontractor.setMaterialTransferId(materialList.get(0).getMaterialTransferId());
			materialtransfertocontractor.setStoreId(materialList.get(0).getStoreId());
			materialtransfertocontractor.setContractorId(materialList.get(0).getContractorId());
			materialtransfertocontractor.setContractorfirmName(contractorDetails.get(0).getContractorfirmName());
			materialtransfertocontractor.setContractorEmployeeId(materialList.get(0).getContractorEmployeeId());
			materialtransfertocontractor.setEmployeeName(contractorEmployeeDetails.get(0).getEmployeeName());
			materialtransfertocontractor.setProjectId(materialList.get(0).getProjectId());
			materialtransfertocontractor.setBuildingId(materialList.get(0).getBuildingId());
			materialtransfertocontractor.setWingId(materialList.get(0).getWingId());
			materialtransfertocontractor.setCreationDate(materialList.get(0).getCreationDate());

			transferList.add(materialtransfertocontractor);



			List<MaterialTransferToContractorHistory> materialList1 = new ArrayList<MaterialTransferToContractorHistory>();

			List<Item> itemDetails= new ArrayList<Item>();


			MaterialTransferToContractorHistory transferList1=new MaterialTransferToContractorHistory();

			for(int i=0;i<contractorMaterialList.size();i++)
			{
				transferList1=new MaterialTransferToContractorHistory();
				query = new Query();
				itemDetails = mongoTemplate.find(query.addCriteria(Criteria.where("itemId").is(contractorMaterialList.get(i).getItemId())), Item.class);
				query =new Query();
				List<SubSupplierType> subsuppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subsuppliertypeId").is(itemDetails.get(0).getSubsuppliertypeId())), SubSupplierType.class);

				if(itemDetails.size()!=0)
				{
					transferList1.setMaterialTransferHistoryId(contractorMaterialList.get(i).getMaterialTransferHistoryId());
					transferList1.setMaterialTransferId(contractorMaterialList.get(i).getMaterialTransferId());
					transferList1.setItemId(contractorMaterialList.get(i).getItemId());
					transferList1.setItemName(itemDetails.get(0).getItemName());
					transferList1.setSubsuppliertypeId(subsuppliertypeDetails.get(0).getSubsupplierType());
					transferList1.setTransferPieces(contractorMaterialList.get(i).getTransferPieces());
					transferList1.setTransferStock(contractorMaterialList.get(i).getTransferStock());
					materialList1.add(transferList1);
				}

			}


			query = new Query();
			Project project=mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(materialList.get(0).getProjectId())), Project.class);

			query = new Query();
			ProjectBuilding projectBuilding=mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(materialList.get(0).getBuildingId())),ProjectBuilding.class);
			query = new Query();
			ProjectWing projectWing=mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(materialList.get(0).getWingId())), ProjectWing.class);


			model.addAttribute("projectName",project.getProjectName()); 
			model.addAttribute("buildingName",projectBuilding.getBuildingName()); 
			model.addAttribute("wingName",projectWing.getWingName()); 

			model.addAttribute("suppliertypeList",suppliertypeList); 
			model.addAttribute("contractorMaterialList", materialList1);
			model.addAttribute("transferList", transferList);
			model.addAttribute("contractorList", contractorList);
			model.addAttribute("projectList", projectList);
			model.addAttribute("storeList", storeList);
			return "EditMaterialTransferToContractor";

		}catch (Exception e) {
			return "login";
		}
	}



	@RequestMapping(value="/EditMaterialTransferToContractor",method=RequestMethod.POST)
	public String EditMaterialTransferToContractor(@ModelAttribute MaterialTransferToContractor materialtransfercontractor, Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				materialtransfercontractor = new MaterialTransferToContractor(materialtransfercontractor.getMaterialTransferId(), materialtransfercontractor.getStoreId(), 
						materialtransfercontractor.getContractorId(), materialtransfercontractor.getContractorEmployeeId(), 
						materialtransfercontractor.getProjectId(), materialtransfercontractor.getBuildingId(), materialtransfercontractor.getWingId(), 
						materialtransfercontractor.getCreationDate(), materialtransfercontractor.getUpdateDate(), materialtransfercontractor.getUserName());
				materialtransferTocontractorRepository.save(materialtransfercontractor);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}


			Query query = new Query();
			List<Project> projectList = GetUserAssigenedProjectList(req,res);
			List<MaterialTransferToContractor> materialList = materialtransferTocontractorRepository.findAll();

			MaterialTransferToContractor materialtransfertocontractor=new MaterialTransferToContractor();

			List<MaterialTransferToContractor> transferList = new ArrayList<MaterialTransferToContractor>();


			for(int i=0;i<materialList.size();i++)
			{
				List<Contractor> contractorList= new ArrayList<Contractor>();
				List<ContractorEmployees> contractoremployeeList= new ArrayList<ContractorEmployees>();

				try 
				{ 
					materialtransfertocontractor=new MaterialTransferToContractor();

					query = new Query();
					contractorList = mongoTemplate.find(query.addCriteria(Criteria.where("contractorId").is(materialList.get(i).getContractorId())), Contractor.class);

					query = new Query();
					contractoremployeeList = mongoTemplate.find(query.addCriteria(Criteria.where("contractorEmployeeId").is(materialList.get(i).getContractorEmployeeId())), ContractorEmployees.class);

					if(contractorList.size()!=0 && contractoremployeeList.size()!=0)
					{
						materialtransfertocontractor.setMaterialTransferId(materialList.get(i).getMaterialTransferId());
						materialtransfertocontractor.setContractorfirmName(contractorList.get(0).getContractorfirmName());
						materialtransfertocontractor.setEmployeeName(contractoremployeeList.get(0).getEmployeeName());
						materialtransfertocontractor.setProjectId(materialList.get(i).getProjectId());
						materialtransfertocontractor.setBuildingId(materialList.get(i).getBuildingId());
						materialtransfertocontractor.setWingId(materialList.get(i).getWingId());
						materialtransfertocontractor.setCreationDate(materialList.get(i).getCreationDate());

						query = new Query();
						Project project=mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(materialList.get(i).getProjectId())), Project.class);
						materialtransfertocontractor.setProjectId(project.getProjectName());

						query = new Query();
						ProjectBuilding projectBuilding=mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(materialList.get(i).getBuildingId())),ProjectBuilding.class);
						materialtransfertocontractor.setBuildingId(projectBuilding.getBuildingName());
						query = new Query();
						ProjectWing projectWing=mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(materialList.get(i).getWingId())), ProjectWing.class);
						materialtransfertocontractor.setWingId(projectWing.getWingName());

						transferList.add(materialtransfertocontractor);
					}
				}
				catch (Exception e) {
				}	  
			}

			model.addAttribute("materialList", transferList);
			model.addAttribute("projectList", projectList);

			return "MaterialTransferToContractorMaster";

		}catch (Exception e) {
			return "login";
		}
	}



	@ResponseBody
	@RequestMapping(value="/getContratorEmployeeDetails",method=RequestMethod.POST)
	public List<ContractorEmployees> getContratorEmployeeDetails(@RequestParam("contractorId") String contractorId, HttpSession session)
	{
		List<ContractorEmployees> contractorEmpList= new ArrayList<ContractorEmployees>();

		Query query = new Query();
		contractorEmpList = mongoTemplate.find(query.addCriteria(Criteria.where("contractorId").is(contractorId)), ContractorEmployees.class);
		return contractorEmpList;
	}

	@ResponseBody
	@RequestMapping(value="/GetItemBrandList",method=RequestMethod.POST)
	public List<Brand> GetItemBrandList(@RequestParam("mainitemcategoryName") String mainitemcategoryName, @RequestParam("mainsubitemcategoryName") String mainsubitemcategoryName,HttpSession session)
	{
		List<Brand> brandList= new ArrayList<Brand>();

		Query query = new Query();
		brandList = mongoTemplate.find(query.addCriteria(Criteria.where("mainitemcategoryName").is(mainitemcategoryName).and("mainsubitemcategoryName").is(mainsubitemcategoryName)), Brand.class);
		return brandList;
	}

	@ResponseBody
	@RequestMapping(value="/GetStoreWiseItemQunatity",method=RequestMethod.POST)
	public List<Stock> GetStoreWiseItemQunatity(@RequestParam("storeId") String storeId, @RequestParam("itemId") String itemId,HttpSession session)
	{
		List<Stock> stockitemList= new ArrayList<Stock>();

		Query query = new Query();
		stockitemList = mongoTemplate.find(query.addCriteria(Criteria.where("storeId").is(storeId).and("itemId").is(itemId)), Stock.class);
		return stockitemList;
	}

	@ResponseBody
	@RequestMapping(value="/AddTransferQuantity",method=RequestMethod.POST)
	public List<MaterialTransferToContractorHistory> AddTransferQuantity(@RequestParam("storeId") String storeId, @RequestParam("materialTransferId") String materialTransferId, @RequestParam("itemId") String itemId, @RequestParam("transferPieces") Double transferPieces,  @RequestParam("transferStock") Double transferStock, HttpSession session)
	{


		Query query = new Query();
		try {
			long transferHistoryCode = 0;
			List<MaterialTransferToContractorHistory> transferHistoryList = materialTransfertocontractorhistoryRepository.findAll();

			for(int i=0;i<transferHistoryList.size();i++)
			{
				transferHistoryCode=transferHistoryList.get(i).getMaterialTransferHistoryId()+1;
			}

			MaterialTransferToContractorHistory materialtransferrhistory=new MaterialTransferToContractorHistory();
			materialtransferrhistory.setMaterialTransferHistoryId(transferHistoryCode);
			materialtransferrhistory.setMaterialTransferId(materialTransferId);
			materialtransferrhistory.setItemId(itemId);
			materialtransferrhistory.setTransferPieces(transferPieces);
			materialtransferrhistory.setTransferStock(transferStock);
			materialTransfertocontractorhistoryRepository.save(materialtransferrhistory);
		}
		catch (Exception e) {
		}

		try
		{   	
			Stock storeStock=new Stock();
			storeStock = mongoTemplate.findOne(Query.query(Criteria.where("storeId").is(storeId).and("itemId").is(itemId)), Stock.class);

			Double noOfPieces1=(double) storeStock.getNoOfPieces();
			double currentStock=storeStock.getCurrentStock();
			long noOfPieces=(long) (noOfPieces1-transferPieces);
			storeStock.setNoOfPieces(noOfPieces);
			storeStock.setCurrentStock(currentStock-transferStock);
			stockRepository.save(storeStock);
		}
		catch(Exception ee)
		{
			ee.printStackTrace();
		}


		try {	
			query = new Query();
			List<MaterialTransferToContractorHistory> materialtransferList = mongoTemplate.find(query.addCriteria(Criteria.where("materialTransferId").is(materialTransferId)), MaterialTransferToContractorHistory.class);

			List<MaterialTransferToContractorHistory> materialList = new ArrayList<MaterialTransferToContractorHistory>();

			Item itemDetails= new Item();


			MaterialTransferToContractorHistory transferList=new MaterialTransferToContractorHistory();

			for(int i=0;i<materialtransferList.size();i++)
			{
				transferList=new MaterialTransferToContractorHistory();
				query = new Query();
				itemDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("itemId").is(materialtransferList.get(i).getItemId())), Item.class);

				query =new Query();
				List<SubSupplierType> subsuppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subsuppliertypeId").is(itemDetails.getSubsuppliertypeId())), SubSupplierType.class);

				if(itemDetails!=null)
				{
					transferList.setMaterialTransferHistoryId(materialtransferList.get(i).getMaterialTransferHistoryId());
					transferList.setMaterialTransferId(materialtransferList.get(i).getMaterialTransferId());
					transferList.setItemId(materialtransferList.get(i).getItemId());
					transferList.setItemName(itemDetails.getItemName());

					transferList.setSubsuppliertypeId(subsuppliertypeDetails.get(0).getSubsupplierType());
					transferList.setTransferPieces(materialtransferList.get(i).getTransferPieces());
					transferList.setTransferStock(materialtransferList.get(i).getTransferStock());
					materialList.add(transferList);
				}

			}

			return materialList;
		}
		catch (Exception e) {
			return null;
		}
	}


	@ResponseBody
	@RequestMapping("/DeleteSelectedContractorMaterial")
	public List<MaterialTransferToContractorHistory> DeleteSelectedContractorMaterial(@RequestParam("storeId") String storeId, @RequestParam("materialTransferHistoryId") String materialTransferHistoryId, @RequestParam("materialTransferId") String materialTransferId, HttpSession session)
	{
		Query query = new Query();
		try
		{
			try {
				MaterialTransferToContractorHistory itemDetails1=new MaterialTransferToContractorHistory();
				itemDetails1 = mongoTemplate.findOne(Query.query(Criteria.where("materialTransferHistoryId").is(Integer.parseInt(materialTransferHistoryId))), MaterialTransferToContractorHistory.class);

				String itemId=itemDetails1.getItemId();
				Double deletedItemPieces=itemDetails1.getTransferPieces();
				Double deletedItemStock=itemDetails1.getTransferStock();
				Query query1 = new Query();
				mongoTemplate.remove(query1.addCriteria(Criteria.where("materialTransferHistoryId").is(Integer.parseInt(materialTransferHistoryId))), MaterialTransferToContractorHistory.class);


				Stock storeStock=new Stock();
				storeStock = mongoTemplate.findOne(Query.query(Criteria.where("storeId").is(storeId).and("itemId").is(itemId)), Stock.class);

				Double currentStock1=(double) storeStock.getNoOfPieces();
				long currentStock=(long) (currentStock1+deletedItemPieces);
				storeStock.setNoOfPieces(currentStock);
				storeStock.setCurrentStock(storeStock.getCurrentStock()+deletedItemStock);
				stockRepository.save(storeStock);
			}
			catch (Exception e) {
			}

			query = new Query();
			List<MaterialTransferToContractorHistory> materialtransferList = mongoTemplate.find(query.addCriteria(Criteria.where("materialTransferId").is(materialTransferId)), MaterialTransferToContractorHistory.class);

			List<MaterialTransferToContractorHistory> materialList = new ArrayList<MaterialTransferToContractorHistory>();

			List<Item> itemDetails= new ArrayList<Item>();


			MaterialTransferToContractorHistory transferList=new MaterialTransferToContractorHistory();

			for(int i=0;i<materialtransferList.size();i++)
			{
				transferList=new MaterialTransferToContractorHistory();
				query = new Query();
				itemDetails = mongoTemplate.find(query.addCriteria(Criteria.where("itemId").is(materialtransferList.get(i).getItemId())), Item.class);

				query =new Query();
				List<SubSupplierType> subsuppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subsuppliertypeId").is(itemDetails.get(0).getSubsuppliertypeId())), SubSupplierType.class);

				if(itemDetails.size()!=0)
				{
					transferList.setMaterialTransferHistoryId(materialtransferList.get(i).getMaterialTransferHistoryId());
					transferList.setMaterialTransferId(materialtransferList.get(i).getMaterialTransferId());
					transferList.setItemId(materialtransferList.get(i).getItemId());
					transferList.setItemName(itemDetails.get(0).getItemName());

					transferList.setSubsuppliertypeId(subsuppliertypeDetails.get(0).getSubsupplierType());
					transferList.setTransferPieces(materialtransferList.get(i).getTransferPieces());
					transferList.setTransferStock(materialtransferList.get(i).getTransferStock());
					materialList.add(transferList);
				}

			}

			return materialList;

		}
		catch(Exception e)
		{
			return null;
		}

	} 

	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value="/PrintMaterialTransferToContractor")
	public ResponseEntity<byte[]> PrintMaterialTransferToContractor(@RequestParam("materialTransferId") String materialTransferId, HttpServletRequest request, HttpServletResponse response, HttpSession session)
	{
		JasperPrint print;

		Query query = new Query();
		List<MaterialTransferToContractorHistory> materialTransferdHistory = mongoTemplate.find(query.addCriteria(Criteria.where("materialTransferId").is(materialTransferId)), MaterialTransferToContractorHistory.class);

		query = new Query();
		List<MaterialTransferToContractor> materialTransferToContractorList = mongoTemplate.find(query.addCriteria(Criteria.where("materialTransferId").is(materialTransferId)), MaterialTransferToContractor.class);

		query = new Query();
		List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(materialTransferToContractorList.get(0).getProjectId())), Project.class);

		query = new Query();
		Company companyDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("companyId").is(projectDetails.get(0).getCompanyId())), Company.class);

		query = new Query();
		List<Contractor> contractorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("contractorId").is(materialTransferToContractorList.get(0).getContractorId())), Contractor.class);

		query = new Query();
		List<Store> storeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("storeId").is(materialTransferToContractorList.get(0).getStoreId())), Store.class);

		// company Address
		query =new Query();
		State companyStateDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("stateId").is(companyDetails.getStateId())), State.class);
		query =new Query();
		City companyCityDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("cityId").is(companyDetails.getCityId())), City.class);
		query =new Query();
		LocationArea companyLocationareaDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("locationareaId").is(companyDetails.getLocationareaId())), LocationArea.class);

		//contractor address
		query =new Query();
		State contractorStateDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("stateId").is(contractorDetails.get(0).getStateId())), State.class);
		query =new Query();
		City contractorCityDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("cityId").is(contractorDetails.get(0).getCityId())), City.class);
		query =new Query();
		LocationArea contractorLocationareaDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("locationareaId").is(contractorDetails.get(0).getLocationareaId())), LocationArea.class);

		//project Address
		query =new Query();
		State projectStateDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("stateId").is(projectDetails.get(0).getStateId())), State.class);
		query =new Query();
		City projectCityDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("cityId").is(projectDetails.get(0).getCityId())), City.class);
		query =new Query();
		LocationArea projectLocationareaDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("locationareaId").is(projectDetails.get(0).getLocationareaId())), LocationArea.class);


		if(materialTransferdHistory.size()!=0)
		{
			try 
			{	
				HashMap jmap = new HashMap();
				Collection c = new ArrayList();

				/*jmap.put("srno", "1"); */

				/*DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/M/yyyy");
						LocalDate localDate = LocalDate.now();
						String todayDate=dtf.format(localDate);*/

				for(int i=0;i<materialTransferdHistory.size();i++)
				{
					jmap = new HashMap();

					jmap.put("srno",""+(i+1));

					query = new Query();

					Item itemDetails = new Item();
					itemDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("itemId").is(materialTransferdHistory.get(i).getItemId())), Item.class);

					query =new Query();
					SubSupplierType subsuppliertypeDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("subsuppliertypeId").is(itemDetails.getSubsuppliertypeId())), SubSupplierType.class);

					jmap.put("itemName",""+itemDetails.getItemName());
					jmap.put("noOfPcs",""+materialTransferdHistory.get(i).getTransferPieces());
					jmap.put("noOfItem",""+materialTransferdHistory.get(i).getTransferStock());
					jmap.put("itemSubType",""+subsuppliertypeDetails.getSubsupplierType());

					c.add(jmap);
					jmap = null;
				}

				//String realPath ="//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img"; //context.getRealPath("/resources/dist/img");
				String realPath =CommanController.GetLogoImagePath();
				JRDataSource dataSource = new JRMapCollectionDataSource(c);
				Map<String, Object> parameterMap = new HashMap<String, Object>();

				Date date = new Date();  
				SimpleDateFormat formatter = new SimpleDateFormat("d/M/yyyy");  
				String strDate= formatter.format(date);

				//Booking Receipt Generation Code
				/*1*/  parameterMap.put("companyNameAndAddress", ""+companyDetails.getCompanyName()+", "+companyLocationareaDetails.getLocationareaName()+", "+companyCityDetails.getCityName()+", "+companyStateDetails.getStateName()+"-"+companyDetails.getCompanyPincode());
				/*2*/  parameterMap.put("realPath", realPath);
				/*3*/  parameterMap.put("printDate", ""+strDate);
				/*4*/  parameterMap.put("contractorNameAndAddress", ""+contractorDetails.get(0).getContractorfirmName()+", "+contractorLocationareaDetails.getLocationareaName()+", "+contractorCityDetails.getCityName()+", "+contractorStateDetails.getStateName()+"-"+contractorDetails.get(0).getContractorPincode());
				/*5*/  parameterMap.put("transferNo", ""+materialTransferToContractorList.get(0).getMaterialTransferId());
				/*6*/  parameterMap.put("transferDate", ""+materialTransferToContractorList.get(0).getCreationDate());
				/*7*/  parameterMap.put("siteName", ""+projectDetails.get(0).getProjectName());
				/*8*/  parameterMap.put("companyName", ""+companyDetails.getCompanyName());
				/*9*/  parameterMap.put("siteFullAddress", ""+projectDetails.get(0).getProjectAddress()+", "+projectLocationareaDetails.getLocationareaName()+", "+projectCityDetails.getCityName()+", "+projectStateDetails.getStateName()+"-"+projectDetails.get(0).getAreaPincode());

				session = request.getSession(true);
				List<Employee> userName = (List<Employee>) session.getAttribute("userName");

				/*10*/ parameterMap.put("userName", ""+userName.get(0).getEmployeefirstName()+" "+userName.get(0).getEmployeelastName());
				/*11*/ parameterMap.put("storeName", ""+storeDetails.get(0).getStoreName());
				/*12*/ parameterMap.put("storeNamePassage", ""+storeDetails.get(0).getStoreName());

				InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/MaterialTransferToContractorByStore.jasper");

				print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				JasperExportManager.exportReportToPdfStream(print, baos);

				byte[] contents = baos.toByteArray();

				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.parseMediaType("application/pdf"));
				String filename = "MaterialTransferToContractorByStore.pdf";

				JasperExportManager.exportReportToPdfStream(print, baos);

				headers.setContentType(MediaType.parseMediaType("application/pdf"));
				headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
				ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
				response.setHeader("Content-Disposition", "inline; filename=" + filename );
				return resp;	
			}
			catch(Exception e)
			{
				e.printStackTrace();
				return null;
			}
		}
		else
		{
			try 
			{	
				HashMap jmap = new HashMap();
				Collection c = new ArrayList();

				jmap = new HashMap();

				jmap.put("srno","1");
				jmap.put("itemName","N.A.");
				jmap.put("noOfPcs","N.A.");
				jmap.put("itemSubType","N.A.");

				c.add(jmap);
				jmap = null;

				//String realPath ="//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img"; //context.getRealPath("/resources/dist/img");
				String realPath =CommanController.GetLogoImagePath();
				JRDataSource dataSource = new JRMapCollectionDataSource(c);
				Map<String, Object> parameterMap = new HashMap<String, Object>();

				Date date = new Date();  
				SimpleDateFormat formatter = new SimpleDateFormat("d/M/yyyy");  
				String strDate= formatter.format(date);

				//Booking Receipt Generation Code
				/*1*/  parameterMap.put("companyNameAndAddress", ""+companyDetails.getCompanyName()+", "+companyLocationareaDetails.getLocationareaName()+", "+companyCityDetails.getCityName()+", "+companyStateDetails.getStateName()+"-"+companyDetails.getCompanyPincode());
				/*2*/  parameterMap.put("realPath", realPath);
				/*3*/  parameterMap.put("printDate", ""+strDate);
				/*4*/  parameterMap.put("contractorNameAndAddress", ""+contractorDetails.get(0).getContractorfirmName()+", "+contractorLocationareaDetails.getLocationareaName()+", "+contractorCityDetails.getCityName()+", "+contractorStateDetails.getStateName()+"-"+contractorDetails.get(0).getContractorPincode());
				/*5*/  parameterMap.put("transferNo", ""+materialTransferToContractorList.get(0).getMaterialTransferId());
				/*6*/  parameterMap.put("transferDate", ""+materialTransferToContractorList.get(0).getCreationDate());
				/*7*/  parameterMap.put("siteName", ""+projectDetails.get(0).getLocationareaId());
				/*8*/  parameterMap.put("companyName", ""+companyDetails.getCompanyName());
				/*9*/  parameterMap.put("siteFullAddress", ""+projectDetails.get(0).getProjectAddress()+", "+projectLocationareaDetails.getLocationareaName()+", "+projectCityDetails.getCityName()+", "+projectStateDetails.getStateName()+"-"+projectDetails.get(0).getAreaPincode());

				session = request.getSession(true);
				List<Employee> userName = (List<Employee>) session.getAttribute("userName");

				/*10*/ parameterMap.put("userName", ""+userName.get(0).getEmployeefirstName()+" "+userName.get(0).getEmployeelastName());
				/*11*/ parameterMap.put("storeName", ""+storeDetails.get(0).getStoreName());
				/*12*/ parameterMap.put("storeNamePassage", ""+storeDetails.get(0).getStoreName());

				InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/MaterialTransferToContractorByStore.jasper");

				print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				JasperExportManager.exportReportToPdfStream(print, baos);

				byte[] contents = baos.toByteArray();

				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.parseMediaType("application/pdf"));
				String filename = "MaterialTransferToContractorByStore.pdf";

				JasperExportManager.exportReportToPdfStream(print, baos);

				headers.setContentType(MediaType.parseMediaType("application/pdf"));
				headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
				ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
				response.setHeader("Content-Disposition", "inline; filename=" + filename );
				return resp;		
			}
			catch(Exception e)
			{
				e.printStackTrace();
				return null;
			}
		}
	}

}

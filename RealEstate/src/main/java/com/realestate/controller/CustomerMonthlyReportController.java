package com.realestate.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.realestate.bean.Aggreement;
import com.realestate.bean.Booking;
import com.realestate.bean.CustomerReceiptForm;
import com.realestate.bean.Enquiry;
import com.realestate.bean.Flat;
import com.realestate.bean.Login;
import com.realestate.bean.MonthlyCollectionReport;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.AggreementRepository;
import com.realestate.repository.BookingCancelFormRepository;
import com.realestate.repository.BookingRepository;
import com.realestate.repository.CustomerLoanDetailsRepository;
import com.realestate.repository.CustomerPaymentDetailsRepository;
import com.realestate.repository.CustomerReceiptFormRepository;
import com.realestate.repository.EnquiryRepository;
import com.realestate.repository.ExtraChargeRepository;
import com.realestate.repository.FlatRepository;
import com.realestate.repository.FloorRepository;
import com.realestate.repository.GeneratePaymentSchedulerRepository;
import com.realestate.repository.PaymentSchedulerRepository;
import com.realestate.repository.ProjectBuildingRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.ProjectWingRepository;
import com.realestate.response.DailyReportResponse;
import com.realestate.services.DailySaleReportExcelView;
import com.realestate.services.MonthlySaleReportExcelView;

@Controller
@RequestMapping("/")
public class CustomerMonthlyReportController {

	@Autowired 
	AggreementRepository aggreementRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	ProjectBuildingRepository projectbuildingRepository;
	@Autowired
	ProjectWingRepository wingRepository;

	@Autowired 
	BookingRepository bookingRepository;
	@Autowired
	FlatRepository flatRepository;
	@Autowired
	PaymentSchedulerRepository paymentSchedulerRepository;
	@Autowired
	EnquiryRepository enquiryRepository;
	@Autowired
	GeneratePaymentSchedulerRepository generatepaymentschedulerRepository;
	@Autowired 
	ExtraChargeRepository extrachargesRepository;
	@Autowired
	CustomerLoanDetailsRepository customerLoandetailsRepository;
	@Autowired
	BookingCancelFormRepository bookingcancelformRepository;
	@Autowired
	FloorRepository floorRepository;
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	CustomerPaymentDetailsRepository customerpaymentdetailsrepository;
	@Autowired
	CustomerReceiptFormRepository customerreceiptformrepository;


	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	// get CustomerReceiptForm list

	public Double GetTotalMonthlyCollectiont(Date startDate, Date endDate, HttpServletRequest req, HttpServletResponse res)
	{

		List<Project> projectList = GetUserAssigenedProjectList(req,res); 
		List<Booking> allBookingList = bookingRepository.findAll(); 
		Query query=new Query();
		List<CustomerReceiptForm> todayCustomerReceiptList1 = mongoTemplate.find(query.addCriteria(Criteria.where("creationDate").gte(startDate).lt(endDate)), CustomerReceiptForm.class);

		List<CustomerReceiptForm> todayCustomerReceiptList=new ArrayList<CustomerReceiptForm>();
		CustomerReceiptForm customerreceiptForm=new CustomerReceiptForm();

		double todayAllCustomerPayment=0.0;
		// count today total collection
		for(int i=0;i<todayCustomerReceiptList1.size();i++)
		{

			for(int j=0;j<allBookingList.size();j++)
			{

				if(todayCustomerReceiptList1.get(i).getBookingId().equals(allBookingList.get(j).getBookingId()))
				{

					for(int k=0;k<projectList.size();k++)
					{
						if(allBookingList.get(j).getProjectId().equals(projectList.get(k).getProjectId()))
						{
							customerreceiptForm=new CustomerReceiptForm();

							customerreceiptForm.setReceiptId(todayCustomerReceiptList1.get(i).getReceiptId());
							customerreceiptForm.setBookingId(todayCustomerReceiptList1.get(i).getBookingId());
							customerreceiptForm.setPaymentAmount(todayCustomerReceiptList1.get(i).getPaymentAmount());
							customerreceiptForm.setPaymentType(todayCustomerReceiptList1.get(i).getPaymentType());
							customerreceiptForm.setPaymentMode(todayCustomerReceiptList1.get(i).getPaymentMode());
							customerreceiptForm.setBankName(todayCustomerReceiptList1.get(i).getBankName());
							customerreceiptForm.setBranchName(todayCustomerReceiptList1.get(i).getBranchName());
							customerreceiptForm.setChequeNumber(todayCustomerReceiptList1.get(i).getChequeNumber());
							customerreceiptForm.setNarration(todayCustomerReceiptList1.get(i).getNarration());

							customerreceiptForm.setCreationDate(todayCustomerReceiptList1.get(i).getCreationDate());
							customerreceiptForm.setUserName(todayCustomerReceiptList1.get(i).getUserName());
							customerreceiptForm.setStatus(todayCustomerReceiptList1.get(i).getStatus());

							todayCustomerReceiptList.add(customerreceiptForm);
							todayAllCustomerPayment=todayAllCustomerPayment+todayCustomerReceiptList1.get(i).getPaymentAmount();
							break;
						}

					}
				}
			}
		}

		return todayAllCustomerPayment;

	}

	//get Booking list
	public List<Booking> GetBookingList(Date startDate, Date endDate, HttpServletRequest req, HttpServletResponse res)
	{
		try
		{

			List<Project> projectList = GetUserAssigenedProjectList(req,res); 

			Query query = new Query();
			List<Booking> bookingList1 = mongoTemplate.find(query.addCriteria(Criteria.where("creationDate").gte(startDate).lt(endDate)), Booking.class);

			List<Booking> bookingList=new ArrayList<Booking>();
			Booking booking=new Booking();

			for(int i=0;i<projectList.size();i++)
			{

				for(int j=0;j<bookingList1.size();j++)
				{
					if(projectList.get(i).getProjectId().equals(bookingList1.get(j).getProjectId()))
					{
						booking=new Booking();

						booking.setBookingId(bookingList1.get(j).getBookingId());

						booking.setEnquiryId(bookingList1.get(j).getEnquiryId());
						booking.setBookingfirstname(bookingList1.get(j).getBookingfirstname());
						booking.setBookingmiddlename(bookingList1.get(j).getBookingmiddlename());
						booking.setBookinglastname(bookingList1.get(j).getBookinglastname());
						booking.setBookingaddress(bookingList1.get(j).getBookingaddress());
						booking.setBookingPincode(bookingList1.get(j).getBookingPincode());

						booking.setBookingEmail(bookingList1.get(j).getBookingEmail());
						booking.setBookingmobileNumber1(bookingList1.get(j).getBookingmobileNumber1());
						booking.setBookingmobileNumber2(bookingList1.get(j).getBookingmobileNumber2());
						booking.setBookingOccupation(bookingList1.get(j).getBookingOccupation());
						booking.setPurposeOfFlat(bookingList1.get(j).getPurposeOfFlat());

						booking.setProjectId(bookingList1.get(j).getProjectId());
						booking.setBuildingId(bookingList1.get(j).getBuildingId());
						booking.setWingId(bookingList1.get(j).getWingId());
						booking.setFloorId(bookingList1.get(j).getFloorId());
						booking.setFlatType(bookingList1.get(j).getFlatType());
						booking.setFlatId(bookingList1.get(j).getFlatId());
						booking.setFlatFacing(bookingList1.get(j).getFlatFacing());
						booking.setFlatareainSqFt(bookingList1.get(j).getFlatareainSqFt());
						booking.setFlatCostwithotfloorise(bookingList1.get(j).getFlatCostwithotfloorise());
						booking.setFloorRise(bookingList1.get(j).getFloorRise());
						booking.setFlatCost(bookingList1.get(j).getFlatCost());
						booking.setFlatbasicCost(bookingList1.get(j).getFlatbasicCost());
						booking.setParkingFloorId(bookingList1.get(j).getParkingFloorId());
						booking.setParkingZoneId(bookingList1.get(j).getParkingZoneId());
						booking.setAgentId(bookingList1.get(j).getAgentId());

						booking.setInfrastructureCharge(bookingList1.get(j).getInfrastructureCharge());
						booking.setAggreementValue1(bookingList1.get(j).getAggreementValue1());
						booking.setHandlingCharges(bookingList1.get(j).getHandlingCharges());
						booking.setStampDuty1(bookingList1.get(j).getStampDuty1());
						booking.setStampDutyPer(bookingList1.get(j).getStampDutyPer());
						booking.setRegistrationPer(bookingList1.get(j).getRegistrationPer());
						booking.setRegistrationCost1(bookingList1.get(j).getRegistrationCost1());
						booking.setGstCost(bookingList1.get(j).getGstCost());
						booking.setGstAmount1(bookingList1.get(j).getGstAmount1());
						booking.setGrandTotal1(bookingList1.get(j).getGrandTotal1());
						booking.setTds(bookingList1.get(j).getTds());
						booking.setBookingstatus(bookingList1.get(j).getBookingstatus());
						booking.setUserName(bookingList1.get(j).getUserName());
						booking.setCreationDate(bookingList1.get(j).getCreationDate());

						try {
							query = new Query();
							Project project=mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(bookingList1.get(j).getProjectId())), Project.class);
							booking.setProjectId(project.getProjectName());

							query = new Query();
							ProjectBuilding projectBuilding=mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(bookingList1.get(j).getBuildingId())),ProjectBuilding.class);
							booking.setBuildingId(projectBuilding.getBuildingName());
							query = new Query();
							ProjectWing projectWing=mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(bookingList1.get(j).getWingId())), ProjectWing.class);
							booking.setWingId(projectWing.getWingName());

							query = new Query();
							Flat flat=mongoTemplate.findOne(query.addCriteria(Criteria.where("flatId").is(bookingList1.get(j).getFlatId())), Flat.class);
							booking.setFlatNumber(flat.getFlatNumber());
						}catch (Exception e) {
							// TODO: handle exception
						}
						bookingList.add(booking);

					}

				}

			}

			return bookingList;
		}
		catch(Exception e)
		{
			return null;
		}

	}


	//get all Monthly aggreement count
	public int GetAggreementCount(Date startDate, Date endDate, HttpServletRequest req, HttpServletResponse res)
	{

		List<Project> projectList = GetUserAssigenedProjectList(req,res); 
		List<Booking> allBookingList = bookingRepository.findAll(); 

		int todayTotalFlatAgreement=0;

		Query  query = new Query();
		List<Aggreement> aggreementList = mongoTemplate.find(query.addCriteria(Criteria.where("creationDate").gte(startDate).lt(endDate)), Aggreement.class);


		// count todayTotalFlatAgreement
		for(int i=0;i<aggreementList.size();i++)
		{

			for(int j=0;j<allBookingList.size();j++)
			{
				if(aggreementList.get(i).getBookingId().equals(allBookingList.get(j).getBookingId()))
				{

					for(int k=0;k<projectList.size();k++)
					{
						if(allBookingList.get(j).getProjectId().equals(projectList.get(k).getProjectId()))
						{
							todayTotalFlatAgreement=todayTotalFlatAgreement+1;
							break;
						}

					}
				}

			}

		}
		return todayTotalFlatAgreement;

	}


	//get Enquiry list
	public List<Enquiry> GetEnquiryList(Date startDate, Date endDate, HttpServletRequest req, HttpServletResponse res)
	{
		try
		{

			Query query = new Query();
			List<Enquiry> enquiryList = mongoTemplate.find(query.addCriteria(Criteria.where("creationDate").gte(startDate).lt(endDate)), Enquiry.class);

			return enquiryList;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}

	}


	@RequestMapping("/CustomerMonthlyReport")
	public String CustomerMonthlyReport(ModelMap model, HttpServletRequest req, HttpServletResponse res) throws ParseException 
	{	
		try {
			Calendar now = Calendar.getInstance();

			String currentStartDate1="1/"+(now.get(Calendar.MONTH) + 1)+"/"+now.get(Calendar.YEAR);

			Calendar calendar = Calendar.getInstance();
			int lastDate = calendar.getActualMaximum(Calendar.DATE);
			calendar.set(Calendar.DATE, lastDate);

			Date currentMonthStartDate=new SimpleDateFormat("d/M/yyyy").parse(currentStartDate1);
			Date currentMonthEndDate=calendar.getTime();

			List<Booking> bookingList=  GetBookingList(currentMonthStartDate, currentMonthEndDate, req,res);
			List<Enquiry> enquiryList=  GetEnquiryList(currentMonthStartDate, currentMonthEndDate, req,res);
			int TotalMonthlyFlatAgreement=  GetAggreementCount(currentMonthStartDate, currentMonthEndDate, req,res);
			Double todayAllCustomerPayment=  GetTotalMonthlyCollectiont(currentMonthStartDate, currentMonthEndDate, req,res);

			long totalFlats=0;
			long totalFlatBooking=0;
			long totalFlatAgreement=0;

			List<Project> projectList = GetUserAssigenedProjectList(req,res); 
			List<Flat> allFlatList = flatRepository.findAll(); 

			for(int i=0;i<projectList.size();i++)
			{

				for(int j=0;j<allFlatList.size();j++)
				{
					if(projectList.get(i).getProjectId().equals(allFlatList.get(j).getProjectId()))
					{

						totalFlats=totalFlats+1;

						if(allFlatList.get(i).getFlatstatus().equals("Booking Completed"))
						{
							totalFlatBooking=totalFlatBooking+1;
						}

						else if(allFlatList.get(i).getFlatstatus().equals("Aggreement Completed"))
						{
							totalFlatAgreement=totalFlatAgreement+1;
						}
					}
				}
			}

			model.addAttribute("totalFlats",totalFlats);
			model.addAttribute("totalFlatBooking",totalFlatBooking);
			model.addAttribute("totalFlatAgreement",totalFlatAgreement);

			model.addAttribute("totalMonthlyCustomerPayment",todayAllCustomerPayment);
			model.addAttribute("TotalMonthlyFlatAgreement",TotalMonthlyFlatAgreement);
			model.addAttribute("totaMonthlyBooking",bookingList.size());
			model.addAttribute("totalMonthlyEnquiry",enquiryList.size());

			model.addAttribute("enquiryList", enquiryList);	  
			model.addAttribute("bookingList", bookingList);
			return "CustomerMonthlyReport";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/searchEnquiryReportDateWise")
	public List<Enquiry> searchEnquiryReportDateWise(@RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate, HttpServletRequest req, HttpServletResponse res) throws ParseException
	{
		Date currentMonthStartDate=new SimpleDateFormat("d/M/yyyy").parse(startDate);
		Date currentMonthEndDate=new SimpleDateFormat("d/M/yyyy").parse(endDate);

		List<Enquiry> enquiryList=  GetEnquiryList(currentMonthStartDate, currentMonthEndDate, req,res);
		return enquiryList;
	}

	@ResponseBody
	@RequestMapping("/searchBookingReportDateWise")
	public List<Booking> searchBookingReportDateWise(@RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate, ModelMap model, HttpServletRequest req, HttpServletResponse res) throws ParseException
	{
		Date currentMonthStartDate=new SimpleDateFormat("d/M/yyyy").parse(startDate);
		Date currentMonthEndDate=new SimpleDateFormat("d/M/yyyy").parse(endDate);

		List<Booking> bookingList=  GetBookingList(currentMonthStartDate, currentMonthEndDate, req,res);

		return bookingList;
	}

	@ResponseBody
	@RequestMapping("/searchCollectionReportDateWise")
	public List<MonthlyCollectionReport> searchCollectionReportDateWise(@RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate, ModelMap model, HttpServletRequest req, HttpServletResponse res) throws ParseException
	{
		Date currentMonthStartDate=new SimpleDateFormat("d/M/yyyy").parse(startDate);
		Date currentMonthEndDate=new SimpleDateFormat("d/M/yyyy").parse(endDate);

		List<Booking> bookingList=  GetBookingList(currentMonthStartDate, currentMonthEndDate, req,res);
		List<Enquiry> enquiryList=  GetEnquiryList(currentMonthStartDate, currentMonthEndDate, req,res);
		int TotalMonthlyFlatAgreement=  GetAggreementCount(currentMonthStartDate, currentMonthEndDate, req,res);
		Double todayAllCustomerPayment=  GetTotalMonthlyCollectiont(currentMonthStartDate, currentMonthEndDate, req,res);

		long totalFlats=0;
		long totalFlatBooking=0;
		long totalFlatAgreement=0;

		List<Project> projectList = GetUserAssigenedProjectList(req,res); 
		List<Flat> allFlatList = flatRepository.findAll(); 

		for(int i=0;i<projectList.size();i++)
		{

			for(int j=0;j<allFlatList.size();j++)
			{
				if(projectList.get(i).getProjectId().equals(allFlatList.get(j).getProjectId()))
				{
					totalFlats=totalFlats+1;
					if(allFlatList.get(i).getFlatstatus().equals("Booking Completed"))
					{
						totalFlatBooking=totalFlatBooking+1;
					}
					else if(allFlatList.get(i).getFlatstatus().equals("Aggreement Completed"))
					{
						totalFlatAgreement=totalFlatAgreement+1;
					}
				}
			}
		}


		List<MonthlyCollectionReport>  monthlycollectionReport=new ArrayList<MonthlyCollectionReport>();
		MonthlyCollectionReport MonthlyCollectionReport=new MonthlyCollectionReport();

		MonthlyCollectionReport.setTotalBooking(totalFlatBooking);
		MonthlyCollectionReport.setTotalEnquiry(enquiryList.size());
		MonthlyCollectionReport.setTotalCollection(todayAllCustomerPayment);
		MonthlyCollectionReport.setTodayTotalFlatBooking(bookingList.size());
		MonthlyCollectionReport.setTodayTotalFlatAgreement(TotalMonthlyFlatAgreement);
		MonthlyCollectionReport.setTotalFlatAgreement(totalFlatAgreement);
		MonthlyCollectionReport.setTotalFlatBooking(bookingList.size());
		MonthlyCollectionReport.setTotalFlats(totalFlats);

		monthlycollectionReport.add(MonthlyCollectionReport);

		return monthlycollectionReport;
	} 

	@RequestMapping(value="/ExportMonthlyReport", method=RequestMethod.POST)
	public ModelAndView generateExcel(@RequestParam("startDate") String startDate1, @RequestParam("endDate") String endDate1, HttpServletRequest request, HttpServletResponse response) throws Exception 
	{

		//String startDate1,endDate1;
		Date startDate,endDate;
		if(!startDate1.equalsIgnoreCase("") && !endDate1.equalsIgnoreCase(""))
		{

			startDate=new SimpleDateFormat("d/M/yyyy").parse(startDate1);
			endDate=new SimpleDateFormat("d/M/yyyy").parse(endDate1);

		}
		else
		{

			Calendar now = Calendar.getInstance();

			String currentStartDate1="1/"+(now.get(Calendar.MONTH) + 1)+"/"+now.get(Calendar.YEAR);

			Calendar calendar = Calendar.getInstance();
			int lastDate = calendar.getActualMaximum(Calendar.DATE);
			calendar.set(Calendar.DATE, lastDate);

			startDate=new SimpleDateFormat("d/M/yyyy").parse(currentStartDate1);
			endDate=calendar.getTime();

		}

		List<DailyReportResponse> list=new ArrayList<DailyReportResponse>();
		DailyReportResponse dailyreportResponse=new DailyReportResponse();
		Query query = new Query();
		List<Booking> bookingList = mongoTemplate.find(query.addCriteria(Criteria.where("creationDate").gte(startDate).lt(endDate)), Booking.class);

		query = new Query();
		List<Enquiry> enquiryList = mongoTemplate.find(query.addCriteria(Criteria.where("creationDate").gte(startDate).lt(endDate)), Enquiry.class);

		dailyreportResponse.setEnquiryList(enquiryList);
		query = new Query();
		for(int i=0; i<bookingList.size();i++)
		{
			try {
				query = new Query();
				Project project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(bookingList.get(i).getProjectId())), Project.class);
				bookingList.get(i).setProjectName(project.getProjectName());
				query = new Query();
				Flat flat = mongoTemplate.findOne(query.addCriteria(Criteria.where("flatId").is(bookingList.get(i).getFlatId())), Flat.class);
				bookingList.get(i).setFlatNumber(flat.getFlatNumber());
				bookingList.get(i).setArea(""+flat.getFlatAreawithLoadingInFt());
			}catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}
		}

		dailyreportResponse.setBookingList(bookingList);

		query=new Query();
		List<CustomerReceiptForm> todayCustomerReceiptList1 = mongoTemplate.find(query.addCriteria(Criteria.where("creationDate").gte(startDate).lt(endDate)), CustomerReceiptForm.class);
		double totalPayment=0;
		for(int i=0;i<todayCustomerReceiptList1.size();i++)
		{
			try {
				query = new Query();
				Booking booking = mongoTemplate.findOne(query.addCriteria(Criteria.where("bookingId").is(todayCustomerReceiptList1.get(i).getBookingId())), Booking.class);
				todayCustomerReceiptList1.get(i).setCustomerName(booking.getBookingfirstname());
				totalPayment=totalPayment+todayCustomerReceiptList1.get(i).getPaymentAmount();
				query = new Query();
				Flat flat = mongoTemplate.findOne(query.addCriteria(Criteria.where("flatId").is(booking.getFlatId())), Flat.class);

				query = new Query();
				ProjectBuilding projectBuilding=mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(booking.getBuildingId())),ProjectBuilding.class);
				query = new Query();
				ProjectWing projectWing=mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(booking.getWingId())), ProjectWing.class);
				todayCustomerReceiptList1.get(i).setFlatNumber(""+projectBuilding.getBuildingName()+"-"+projectWing.getWingName()+"-"+flat.getFlatNumber());
			}catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}
		}

		dailyreportResponse.setPaymentList(todayCustomerReceiptList1);
		dailyreportResponse.setTotalPayment(totalPayment);
		list.add(dailyreportResponse);
		ModelAndView modelAndView = new ModelAndView(new MonthlySaleReportExcelView(), "list" ,list);

		return modelAndView;
	}
}

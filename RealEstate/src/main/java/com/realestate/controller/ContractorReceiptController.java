package com.realestate.controller;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.City;
import com.realestate.bean.Company;
import com.realestate.bean.Contractor;
import com.realestate.bean.ContractorType;
import com.realestate.bean.ContractorWorkListHistory;
import com.realestate.bean.ContractorWorkOrder;
import com.realestate.bean.Country;
import com.realestate.bean.Employee;
import com.realestate.bean.LocationArea;
import com.realestate.bean.NumberToWordConversion;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.State;
import com.realestate.bean.SubContractorType;
import com.realestate.bean.Tax;
import com.realestate.configuration.CommanController;
import com.realestate.repository.ContractorRepository;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;

@Controller
@RequestMapping("/")
public class ContractorReceiptController 
{
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	ContractorRepository contractorRepository;


	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ResponseBody
	@RequestMapping(value="/PrintContractorWorkAgreement")
	public ResponseEntity<byte[]> PrintContractorWorkAgreement(@RequestParam("workOrderId") String workOrderId, ModelMap model, HttpServletResponse response)
	{
		try 
		{
			JasperPrint print;
			Query query = new Query();
			HashMap jmap = new HashMap();
			Collection c = new ArrayList();

			query = new Query(); 
			List<ContractorWorkOrder> contractorworkorderDetails = mongoTemplate.find(query.addCriteria(Criteria.where("workOrderId").is(workOrderId)), ContractorWorkOrder.class);

			query = new Query();
			List<Contractor> contractorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("contractorId").is(contractorworkorderDetails.get(0).getContractorId())), Contractor.class);

			query = new Query();
			List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(contractorworkorderDetails.get(0).getProjectId())), Project.class);

			query = new Query();
			List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(contractorworkorderDetails.get(0).getBuildingId())), ProjectBuilding.class);

			query = new Query();
			List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(contractorworkorderDetails.get(0).getWingId())), ProjectWing.class);

			query = new Query();
			List<Company> companyDetails = mongoTemplate.find(query.addCriteria(Criteria.where("companyId").is(projectDetails.get(0).getCompanyId())), Company.class);


			query =new Query();
			State companyStateDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("stateId").is(companyDetails.get(0).getStateId())), State.class);
			query =new Query();
			City companyCityDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("cityId").is(companyDetails.get(0).getCityId())), City.class);
			query =new Query();
			LocationArea companyLocationareaDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("locationareaId").is(companyDetails.get(0).getLocationareaId())), LocationArea.class);

			query =new Query();
			State contractorStateDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("stateId").is(contractorDetails.get(0).getStateId())), State.class);
			query =new Query();
			City contractorCityDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("cityId").is(contractorDetails.get(0).getCityId())), City.class);
			query =new Query();
			LocationArea contractorLocationareaDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("locationareaId").is(contractorDetails.get(0).getLocationareaId())), LocationArea.class);

			query =new Query();
			State projectStateDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("stateId").is(projectDetails.get(0).getStateId())), State.class);
			query =new Query();
			City projectCityDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("cityId").is(projectDetails.get(0).getCityId())), City.class);
			query =new Query();
			LocationArea projectLocationareaDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("locationareaId").is(projectDetails.get(0).getLocationareaId())), LocationArea.class);


			query = new Query();
			List<ContractorWorkListHistory> contractorworkhistoryList = mongoTemplate.find(query.addCriteria(Criteria.where("workId").is(contractorworkorderDetails.get(0).getWorkId())), ContractorWorkListHistory.class);

			if(contractorworkhistoryList.size()!=0)
			{
				for(int i=0;i<contractorworkhistoryList.size();i++)
				{
					List<SubContractorType> subcontractortypeDetails = new ArrayList<SubContractorType>();

					jmap = new HashMap();
					jmap.put("srno",""+(i+1));

					query = new Query();
					subcontractortypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subcontractortypeId").is(contractorworkhistoryList.get(i).getSubcontractortypeId())), SubContractorType.class);
					contractorworkhistoryList.get(i).setSubcontractortypeId(subcontractortypeDetails.get(0).getSubcontractorType());

					jmap.put("subContractorType",""+contractorworkhistoryList.get(i).getSubcontractortypeId());
					jmap.put("workType",""+contractorworkhistoryList.get(i).getWorkType());
					jmap.put("type", ""+contractorworkhistoryList.get(i).getType());
					jmap.put("unit",""+contractorworkhistoryList.get(i).getUnit());
					jmap.put("rate",""+contractorworkhistoryList.get(i).getWorkRate());
					jmap.put("totalAmt",""+contractorworkhistoryList.get(i).getGrandTotal());

					c.add(jmap);
					jmap = null;
				}
			}
			else
			{
				jmap = new HashMap();
				jmap.put("srno","1");
				jmap.put("subContractorType","N.A.");
				jmap.put("workType","N.A.");
				jmap.put("type", "N.A.");
				jmap.put("unit","N.A.");
				jmap.put("rate","N.A.");
				jmap.put("totalAmt","N.A.");

				c.add(jmap);
				jmap = null;
			}

			JRDataSource dataSource = new JRMapCollectionDataSource(c);
			Map<String, Object> parameterMap = new HashMap<String, Object>();

			//String realPath = "//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img";

			/*1*/  parameterMap.put("companyNameAndAddress", ""+companyDetails.get(0).getCompanyName()+" "+companyDetails.get(0).getCompanyType()+" LTD, "+companyDetails.get(0).getCompanyAddress()+", "+companyCityDetails.getCityName()+", "+companyStateDetails.getStateName()+" - "+companyDetails.get(0).getCompanyPincode());
			/*2*/  parameterMap.put("contractorNameAndAddress", ""+contractorDetails.get(0).getContractorfirmName()+" "+contractorDetails.get(0).getContractorfirmType()+" LTD, "+contractorDetails.get(0).getContractorfirmAddress()+", "+contractorCityDetails.getCityName()+", "+contractorStateDetails.getStateName()+" - "+contractorDetails.get(0).getContractorPincode());
			/*4*/  parameterMap.put("projectName", ""+projectDetails.get(0).getProjectName());
			/*5*/  parameterMap.put("projectAddress", ""+projectDetails.get(0).getProjectAddress()+", "+projectDetails.get(0).getCityId()+" - "+projectDetails.get(0).getAreaPincode());
			/*6*/  parameterMap.put("buildingName", "Building : "+buildingDetails.get(0).getBuildingName()+", Wing : "+wingDetails.get(0).getWingName());
			NumberToWordConversion numTOword = new NumberToWordConversion();
			/*7*/  parameterMap.put("contractorAmount", ""+contractorworkorderDetails.get(0).getTotalAmount().longValue()+"/- ("+numTOword.convertToIndianCurrency(""+contractorworkorderDetails.get(0).getTotalAmount().longValue())+")");
			/*8*/  parameterMap.put("retensionAmount", ""+contractorworkorderDetails.get(0).getRetentionAmount()+"/- ("+contractorworkorderDetails.get(0).getRetentionPer()+"%)");
			/*9*/  parameterMap.put("passageFirst", ""+projectDetails.get(0).getProjectName()+", "+projectDetails.get(0).getProjectAddress()+", "+projectCityDetails.getCityName()+" - "+projectDetails.get(0).getAreaPincode());
			/*10*/ parameterMap.put("subTotalAmt", ""+contractorworkorderDetails.get(0).getTotalAmount());

			List<Tax> taxList=new ArrayList<Tax>();
			query = new Query();
			taxList = mongoTemplate.find(query.addCriteria(Criteria.where("taxType").is("CONTRACTOR")), Tax.class);

			/*11*/ parameterMap.put("gstAmt", ""+(contractorworkorderDetails.get(0).getTotalAmount()*taxList.get(0).getTaxPercentage())/100);

			Double gstAmt = (contractorworkorderDetails.get(0).getTotalAmount()*taxList.get(0).getTaxPercentage())/100;
			Double tdsPercentage = contractorDetails.get(0).getTdspercentage();
			Double tdsAmt = (contractorworkorderDetails.get(0).getTotalAmount()*tdsPercentage)/100;

			/*12*/ parameterMap.put("tdsAmt", ""+(contractorworkorderDetails.get(0).getTotalAmount()*tdsPercentage)/100);
			/*13*/ parameterMap.put("grandAmt", ""+((contractorworkorderDetails.get(0).getTotalAmount()+gstAmt)-tdsAmt));
			/*14*/ parameterMap.put("gstPercentage", ""+taxList.get(0).getTaxPercentage()+" %");

			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/ContractorAgreement.jasper");

			print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(print, baos);

			byte[] contents = baos.toByteArray();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			String filename = "ContractorAgreement.pdf";

			JasperExportManager.exportReportToPdfStream(print, baos);


			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

			ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
			response.setHeader("Content-Disposition", "inline; filename=" + filename );

			return resp;					    
		}
		catch(Exception ex)
		{
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value="/PrintContractorBillReceipt")
	public ResponseEntity<byte[]> PrintContractorBillReceipt(@RequestParam("workOrderId") String workOrderId, ModelMap model, HttpServletResponse response, HttpServletRequest request)
	{
		try 
		{	
			JasperPrint print;
			Query query = new Query();
			HashMap jmap = new HashMap();
			Collection c = new ArrayList();

			query = new Query(); 
			List<ContractorWorkOrder> contractorworkorderDetails = mongoTemplate.find(query.addCriteria(Criteria.where("workOrderId").is(workOrderId)), ContractorWorkOrder.class);

			query = new Query();
			List<Contractor> contractorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("contractorId").is(contractorworkorderDetails.get(0).getContractorId())), Contractor.class);

			query = new Query();
			List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(contractorworkorderDetails.get(0).getProjectId())), Project.class);

			query = new Query();
			List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(contractorworkorderDetails.get(0).getBuildingId())), ProjectBuilding.class);

			query = new Query();
			List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(contractorworkorderDetails.get(0).getWingId())), ProjectWing.class);

			query = new Query();
			List<Company> companyDetails = mongoTemplate.find(query.addCriteria(Criteria.where("companyId").is(projectDetails.get(0).getCompanyId())), Company.class);

			query = new Query();
			List<ContractorType> contractorType = mongoTemplate.find(query.addCriteria(Criteria.where("contractortypeId").is(contractorDetails.get(0).getContractortypeId())), ContractorType.class);

			query = new Query();
			List<ContractorWorkListHistory> contractorworkhistoryList = mongoTemplate.find(query.addCriteria(Criteria.where("workId").is(contractorworkorderDetails.get(0).getWorkId())), ContractorWorkListHistory.class);


			query =new Query();
			State companyStateDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("stateId").is(companyDetails.get(0).getStateId())), State.class);
			query =new Query();
			City companyCityDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("cityId").is(companyDetails.get(0).getCityId())), City.class);
			/*	query =new Query();
				LocationArea companyLocationareaDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("locationareaId").is(companyDetails.get(0).getLocationareaId())), LocationArea.class);
			 */
			query =new Query();
			State contractorStateDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("stateId").is(contractorDetails.get(0).getStateId())), State.class);
			query =new Query();
			City contractorCityDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("cityId").is(contractorDetails.get(0).getCityId())), City.class);

			query =new Query();
			State projectStateDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("stateId").is(projectDetails.get(0).getStateId())), State.class);
			query =new Query();
			City projectCityDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("cityId").is(projectDetails.get(0).getCityId())), City.class);



			if(contractorworkhistoryList.size()!=0)
			{
				for(int i=0;i<contractorworkhistoryList.size();i++)
				{
					List<SubContractorType> subcontractortypeDetails = new ArrayList<SubContractorType>();

					jmap = new HashMap();
					jmap.put("srno",""+(i+1));

					query = new Query();
					subcontractortypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subcontractortypeId").is(contractorworkhistoryList.get(i).getSubcontractortypeId())), SubContractorType.class);
					contractorworkhistoryList.get(i).setSubcontractortypeId(subcontractortypeDetails.get(0).getSubcontractorType());

					jmap.put("subContractorType",""+contractorworkhistoryList.get(i).getSubcontractortypeId());
					jmap.put("workType",""+contractorworkhistoryList.get(i).getWorkType());
					jmap.put("type", ""+contractorworkhistoryList.get(i).getType());
					jmap.put("unit",""+contractorworkhistoryList.get(i).getUnit());
					jmap.put("rate",""+contractorworkhistoryList.get(i).getWorkRate());
					jmap.put("totalAmt",""+contractorworkhistoryList.get(i).getGrandTotal());

					c.add(jmap);
					jmap = null;
				}
			}
			else
			{
				jmap = new HashMap();
				jmap.put("srno","1");
				jmap.put("subContractorType","N.A.");
				jmap.put("workType","N.A.");
				jmap.put("type", "N.A.");
				jmap.put("unit","N.A.");
				jmap.put("rate","N.A.");
				jmap.put("totalAmt","N.A.");

				c.add(jmap);
				jmap = null;
			}

			JRDataSource dataSource = new JRMapCollectionDataSource(c);
			Map<String, Object> parameterMap = new HashMap<String, Object>();

			//String realPath = "//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img";
			String realPath =CommanController.GetLogoImagePath();
			Date date = new Date();  
			SimpleDateFormat formatter = new SimpleDateFormat("d/M/yyyy");  
			String strDate= formatter.format(date);


			/*1*/   parameterMap.put("companyNameAndAddress", ""+companyDetails.get(0).getCompanyName()+" "+companyDetails.get(0).getCompanyType()+" LTD, "+companyDetails.get(0).getCompanyAddress()+", "+companyCityDetails.getCityName()+", "+companyStateDetails.getStateName()+" - "+companyDetails.get(0).getCompanyPincode());
			/*2*/   parameterMap.put("realPath", ""+realPath);
			/*3*/   parameterMap.put("contractorNameAndAddress", ""+contractorDetails.get(0).getContractorfirmName()+" "+contractorDetails.get(0).getContractorfirmType()+" LTD, "+contractorDetails.get(0).getContractorfirmAddress()+", "+contractorCityDetails.getCityName()+", "+contractorStateDetails.getStateName()+" - "+contractorDetails.get(0).getContractorPincode());
			/*4*/   parameterMap.put("printDate", ""+strDate);
			/*5*/   parameterMap.put("workOrderNo", ""+contractorworkorderDetails.get(0).getWorkOrderId());
			/*6*/   parameterMap.put("workOrderDate", ""+contractorworkorderDetails.get(0).getCreationDate());
			/*7*/   parameterMap.put("siteName", ""+projectDetails.get(0).getProjectAddress());
			/*8*/   parameterMap.put("companyName", ""+companyDetails.get(0).getCompanyName()+" "+companyDetails.get(0).getCompanyType()+" LTD");
			/*9*/   parameterMap.put("siteFullAddress", ""+projectDetails.get(0).getProjectAddress()+", "+projectCityDetails.getCityName()+", "+projectStateDetails.getStateName()+" - "+projectDetails.get(0).getAreaPincode());

			HttpSession session = request.getSession(true);
			List<Employee> userName = (List<Employee>) session.getAttribute("userName");

			/*10*/  parameterMap.put("userName", ""+userName.get(0).getEmployeefirstName()+" "+userName.get(0).getEmployeelastName());
			/*11*/  parameterMap.put("projectName", ""+projectDetails.get(0).getProjectName());
			/*12*/  parameterMap.put("buildingNo", ""+buildingDetails.get(0).getBuildingName());
			/*13*/  parameterMap.put("wingName", ""+wingDetails.get(0).getWingName());
			/*14*/  parameterMap.put("retensionAmt", ""+contractorworkorderDetails.get(0).getRetentionAmount().longValue()+"/- ("+contractorworkorderDetails.get(0).getRetentionPer()+"%)");
			/*15*/  parameterMap.put("totalAmt", ""+contractorworkorderDetails.get(0).getTotalAmount().longValue()+"/-");

			List<Tax> taxList=new ArrayList<Tax>();
			query = new Query();
			taxList = mongoTemplate.find(query.addCriteria(Criteria.where("taxType").is("CONTRACTOR")), Tax.class);

			Double gstAmt = (contractorworkorderDetails.get(0).getTotalAmount()*taxList.get(0).getTaxPercentage())/100;

			Double tdsPercentage = contractorDetails.get(0).getTdspercentage();
			Double tdsAmt = (contractorworkorderDetails.get(0).getTotalAmount()*tdsPercentage)/100;

			/*16*/  parameterMap.put("tdsAmt", ""+tdsAmt.longValue()+" /-");
			/*17*/  parameterMap.put("gstAmt", ""+(long)(contractorworkorderDetails.get(0).getTotalAmount()*taxList.get(0).getTaxPercentage())/100+" /-");
			/*18*/  parameterMap.put("grandAmt", ""+(long)((contractorworkorderDetails.get(0).getTotalAmount()+gstAmt)-tdsAmt)+" /-");
			/*19*/  parameterMap.put("gstPercentage", ""+taxList.get(0).getTaxPercentage()+" %");
			/*20*/  parameterMap.put("workType", ""+contractorType.get(0).getContractorType());

			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/ContractorBillReceipt.jasper");

			print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(print, baos);

			byte[] contents = baos.toByteArray();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			String filename = "Contractor Bill Receipt.pdf";

			JasperExportManager.exportReportToPdfStream(print, baos);


			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

			ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
			response.setHeader("Content-Disposition", "inline; filename=" + filename );

			return resp;					    
		}
		catch(Exception ex)
		{
			return null;
		}
	}

}

package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Booking;
import com.realestate.bean.CustomerLoanDetails;
import com.realestate.bean.Flat;
import com.realestate.bean.Floor;
import com.realestate.bean.PaymentScheduler;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.UserAssignedProject;
import com.realestate.bean.GeneratePaymentScheduler;
import com.realestate.bean.Login;
import com.realestate.repository.GeneratePaymentSchedulerRepository;
import com.realestate.repository.PaymentSchedulerRepository;
import com.realestate.repository.ProjectRepository;

@Controller
@RequestMapping("/")
public class PaymentSchedulerController
{

	@Autowired
	PaymentSchedulerRepository paymentSchedulerRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	GeneratePaymentSchedulerRepository generatePaymentSchedulerRepository;

	String paymentschedulerId, installmentNumber,customerSchedulerId;

	public String SchedulerIdGenerate()
	{
		long schedulerCount = paymentSchedulerRepository.count();

		if(schedulerCount<10)
		{
			paymentschedulerId = "PS0"+(schedulerCount+1);
		}
		else if((schedulerCount>=10) && (schedulerCount<100))
		{
			paymentschedulerId = "PS"+(schedulerCount+1);
		}

		return paymentschedulerId;
	}

	public String InstallmentNumber()
	{
		long installmentCount = paymentSchedulerRepository.count();

		if(installmentCount == 0)
		{
			installmentNumber = (installmentCount+1)+"st Installment";
		}
		else if(installmentCount == 1)
		{
			installmentNumber = (installmentCount+1)+"nd Installment";
		}
		else if(installmentCount == 2)
		{
			installmentNumber = (installmentCount+1)+"rd Installment";
		}
		else if(installmentCount>2)
		{
			installmentNumber = (installmentCount+1)+"th Installment";
		}

		return installmentNumber;
	}


	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping("/PaymentSchedulerMaster")
	public String PaymentSchedulerMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<PaymentScheduler> paymentschedulerList =  paymentSchedulerRepository.findAll();
			List<Project> projectList = GetUserAssigenedProjectList(req,res);

			Query query = new Query();
			for(int i=0;i<paymentschedulerList.size();i++)
			{
				try
				{
					query = new Query();
					List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(paymentschedulerList.get(i).getProjectId())), Project.class);
					paymentschedulerList.get(i).setProjectId(projectDetails.get(0).getProjectName());
					query = new Query();
					List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(paymentschedulerList.get(i).getBuildingId())), ProjectBuilding.class);
					paymentschedulerList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
					query = new Query();
					List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(paymentschedulerList.get(i).getWingId())), ProjectWing.class);
					paymentschedulerList.get(i).setWingId(wingDetails.get(0).getWingName());
				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}

			model.addAttribute("paymentschedulerList", paymentschedulerList);
			model.addAttribute("projectList",projectList);
			return "PaymentSchedulerMaster";

		}catch (Exception e) {
			return "login";
		}
	}
	@RequestMapping("/AddPaymentScheduler")
	public String PaymentScheduler(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{

				List<Project> projectList = GetUserAssigenedProjectList(req,res); 

				model.addAttribute("projectList",projectList);

				model.addAttribute("schedulerId",SchedulerIdGenerate());

			}
			catch(Exception e)
			{
			}
			return "AddPaymentScheduler";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/AddPaymentScheduler",method=RequestMethod.POST)
	public String PaymentSchedulerSave(@ModelAttribute PaymentScheduler paymentScheduler, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				paymentScheduler = new PaymentScheduler(paymentScheduler.getPaymentschedulerId(),paymentScheduler.getProjectId(),paymentScheduler.getBuildingId(),paymentScheduler.getWingId(), paymentScheduler.getInstallmentNumber(), paymentScheduler.getPaymentDecription(),paymentScheduler.getPercentage(),paymentScheduler.getDueDate(),paymentScheduler.getPaidDate(),paymentScheduler.getSlabStatus(),
						paymentScheduler.getArchitectureSign(),paymentScheduler.getEngineerSign(),paymentScheduler.getMeSign(),
						paymentScheduler.getArchitectureSignDate(),paymentScheduler.getEngineerSignDate(),paymentScheduler.getMeSignDate(),
						paymentScheduler.getCreationDate(),paymentScheduler.getUpdateDate(),paymentScheduler.getUserName());
				paymentSchedulerRepository.save(paymentScheduler);

				model.addAttribute("schedulerStatus","Success");
			}
			catch(Exception e)
			{
				model.addAttribute("schedulerStatus","Fail");
			}

			List<Project> projectList = GetUserAssigenedProjectList(req,res); 

			model.addAttribute("projectList",projectList);

			model.addAttribute("schedulerId",SchedulerIdGenerate());
			return "AddPaymentScheduler";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/EditPaymentScheduler")
	public String EditPaymentScheduler(@RequestParam("paymentschedulerId") String paymentschedulerId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<PaymentScheduler> paymentschedulerDetails = mongoTemplate.find(query.addCriteria(Criteria.where("paymentschedulerId").is(paymentschedulerId)), PaymentScheduler.class);

			query = new Query();
			List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(paymentschedulerDetails.get(0).getProjectId())), Project.class);

			query = new Query();
			List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(paymentschedulerDetails.get(0).getBuildingId())), ProjectBuilding.class);

			query = new Query();
			List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(paymentschedulerDetails.get(0).getWingId())), ProjectWing.class);

			model.addAttribute("projectName",projectDetails.get(0).getProjectName());
			model.addAttribute("buildingName",buildingDetails.get(0).getBuildingName());
			model.addAttribute("wingName",wingDetails.get(0).getWingName());
			model.addAttribute("paymentschedulerDetails",paymentschedulerDetails);

			return "EditPaymentScheduler";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/EditPaymentScheduler", method=RequestMethod.POST)
	public String EditPaymentScheduler(@ModelAttribute PaymentScheduler paymentScheduler,ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				paymentScheduler = new PaymentScheduler(paymentScheduler.getPaymentschedulerId(),paymentScheduler.getProjectId(),paymentScheduler.getBuildingId(),paymentScheduler.getWingId(), paymentScheduler.getInstallmentNumber(), paymentScheduler.getPaymentDecription(),paymentScheduler.getPercentage(),paymentScheduler.getDueDate(),paymentScheduler.getPaidDate(),paymentScheduler.getSlabStatus(),
						paymentScheduler.getArchitectureSign(),paymentScheduler.getEngineerSign(),paymentScheduler.getMeSign(),
						paymentScheduler.getArchitectureSignDate(),paymentScheduler.getEngineerSignDate(),paymentScheduler.getMeSignDate(),
						paymentScheduler.getCreationDate(),paymentScheduler.getUpdateDate(),paymentScheduler.getUserName());
				paymentSchedulerRepository.save(paymentScheduler);
				model.addAttribute("schedulerStatus","Success");
			}
			catch(Exception e)
			{
				model.addAttribute("schedulerStatus","Fail");
			}

			List<PaymentScheduler> paymentschedulerList =  paymentSchedulerRepository.findAll();
			List<Project> projectList = GetUserAssigenedProjectList(req,res);
			Query query = new Query();
			for(int i=0;i<paymentschedulerList.size();i++)
			{
				try
				{
					query = new Query();
					List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(paymentschedulerList.get(i).getProjectId())), Project.class);
					paymentschedulerList.get(i).setProjectId(projectDetails.get(0).getProjectName());
					query = new Query();
					List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(paymentschedulerList.get(i).getBuildingId())), ProjectBuilding.class);
					paymentschedulerList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
					query = new Query();
					List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(paymentschedulerList.get(i).getWingId())), ProjectWing.class);
					paymentschedulerList.get(i).setWingId(wingDetails.get(0).getWingName());
				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}

			model.addAttribute("paymentschedulerList", paymentschedulerList);
			model.addAttribute("projectList",projectList);
			return "PaymentSchedulerMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/GeneratePaymentScheduler") //@RequestParam("bookingId") String bookingId,
	public String GeneratePaymentScheduler(@RequestParam("bookingId") String bookingId, ModelMap model)
	{


		try
		{
			List<GeneratePaymentScheduler> paymentscheduleList=new ArrayList<GeneratePaymentScheduler>(); 
			Query query = new Query();
			paymentscheduleList = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)), GeneratePaymentScheduler.class);
			model.addAttribute("paymentscheduleList", paymentscheduleList);
			List<Booking> bookingDetails= new ArrayList<Booking>();
			List<CustomerLoanDetails> customerLoanList= new ArrayList<CustomerLoanDetails>();

			Query query2 = new Query();
			bookingDetails = mongoTemplate.find(query2.addCriteria(Criteria.where("bookingId").is(bookingId)), Booking.class);

			query =new Query();
			List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(bookingDetails.get(0).getProjectId())), Project.class);

			query =new Query();
			List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(bookingDetails.get(0).getBuildingId())), ProjectBuilding.class);

			query =new Query();
			List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(bookingDetails.get(0).getWingId())), ProjectWing.class);

			query =new Query();
			List<Floor> floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("floorId").is(bookingDetails.get(0).getFloorId())), Floor.class);

			query =new Query();
			List<Flat> flatDetails = mongoTemplate.find(query.addCriteria(Criteria.where("flatId").is(bookingDetails.get(0).getFlatId())), Flat.class);

			model.addAttribute("projectName",projectDetails.get(0).getProjectName());
			model.addAttribute("buildingName",buildingDetails.get(0).getBuildingName());
			model.addAttribute("wingName",wingDetails.get(0).getWingName());
			model.addAttribute("floortypeName",floorDetails.get(0).getFloortypeName());
			model.addAttribute("flatNumber",flatDetails.get(0).getFlatNumber());

			Query query1 = new Query();
			customerLoanList = mongoTemplate.find(query1.addCriteria(Criteria.where("bookingId").is(bookingId)), CustomerLoanDetails.class);
			model.addAttribute("customerLoanList", customerLoanList);
			model.addAttribute("bookingList", bookingDetails);
			model.addAttribute("paymentscheduleList", paymentscheduleList);

		}
		catch(Exception e)
		{

		}

		return "GeneratePaymentScheduler";
	}

	@ResponseBody
	@RequestMapping("/checkTotalPercentage")
	public double returnPaymentSchedule()
	{
		double count=0;
		List<PaymentScheduler> paymentScheduleList = paymentSchedulerRepository.findAll();

		for(int i=0;i<paymentScheduleList.size();i++)
		{
			count = count + paymentScheduleList.get(i).getPercentage();
		}

		return count;
	}

	@ResponseBody
	@RequestMapping("/getPaymentSchedule")
	public List<PaymentScheduler> getPaymentSchedule(@RequestParam("projectId") String projectId, HttpSession session)
	{

		List<PaymentScheduler> paymentScheduleList= new ArrayList<PaymentScheduler>();

		Query query = new Query();
		paymentScheduleList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)), PaymentScheduler.class);
		for(int i=0;i<paymentScheduleList.size();i++)
		{
			try
			{
				query = new Query();
				List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(paymentScheduleList.get(i).getProjectId())), Project.class);
				paymentScheduleList.get(i).setProjectId(projectDetails.get(0).getProjectName());
				query = new Query();
				List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(paymentScheduleList.get(i).getBuildingId())), ProjectBuilding.class);
				paymentScheduleList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
				query = new Query();
				List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(paymentScheduleList.get(i).getWingId())), ProjectWing.class);
				paymentScheduleList.get(i).setWingId(wingDetails.get(0).getWingName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}
		return paymentScheduleList;
	}

	@ResponseBody
	@RequestMapping(value="/getInstallmentNumbers",method=RequestMethod.POST)
	public List<PaymentScheduler> getInstallmentNumbers(@RequestParam("wingId") String wingId, @RequestParam("buildingId") String buildingId, @RequestParam("projectId") String projectId, HttpSession session)
	{
		List<PaymentScheduler> paymentScheduleList= new ArrayList<PaymentScheduler>();

		Query query = new Query();
		paymentScheduleList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId)), PaymentScheduler.class);
		for(int i=0;i<paymentScheduleList.size();i++)
		{
			try
			{
				query = new Query();
				List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(paymentScheduleList.get(i).getProjectId())), Project.class);
				paymentScheduleList.get(i).setProjectId(projectDetails.get(0).getProjectName());
				query = new Query();
				List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(paymentScheduleList.get(i).getBuildingId())), ProjectBuilding.class);
				paymentScheduleList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
				query = new Query();
				List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(paymentScheduleList.get(i).getWingId())), ProjectWing.class);
				paymentScheduleList.get(i).setWingId(wingDetails.get(0).getWingName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}
		return paymentScheduleList;
	} 
	@ResponseBody
	@RequestMapping("/getprojectName")
	public List<Project> getprojectName()
	{
		List<Project> projectList;

		projectList = projectRepository.findAll(new Sort(Sort.Direction.ASC,"projectName"));
		return projectList;
	}

	@ResponseBody
	@RequestMapping("/getWingWisePaymentSchedule")
	public List<PaymentScheduler> getWingWisePaymentSchedule(@RequestParam("projectId") String projectId, @RequestParam("buildingId") String buildingId, @RequestParam("wingId") String wingId)
	{
		Query query = new Query();
		List<PaymentScheduler> paymentschedulerList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId)), PaymentScheduler.class);

		for(int i=0;i<paymentschedulerList.size();i++)
		{
			try
			{
				query = new Query();
				List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(paymentschedulerList.get(i).getProjectId())), Project.class);
				paymentschedulerList.get(i).setProjectId(projectDetails.get(0).getProjectName());
				query = new Query();
				List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(paymentschedulerList.get(i).getBuildingId())), ProjectBuilding.class);
				paymentschedulerList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
				query = new Query();
				List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(paymentschedulerList.get(i).getWingId())), ProjectWing.class);
				paymentschedulerList.get(i).setWingId(wingDetails.get(0).getWingName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		return paymentschedulerList;
	}

}

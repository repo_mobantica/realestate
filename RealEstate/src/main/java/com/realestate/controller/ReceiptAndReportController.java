package com.realestate.controller;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.MongoTypeMapper;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.AddMoreCheckListDetials;
import com.realestate.bean.Aggreement;
import com.realestate.bean.Booking;
import com.realestate.bean.City;
import com.realestate.bean.Company;
import com.realestate.bean.CompanyBanks;
import com.realestate.bean.Country;
import com.realestate.bean.CustomerPaymentDetails;
import com.realestate.bean.CustomerReceiptForm;
import com.realestate.bean.Enquiry;
import com.realestate.bean.EnquirySource;
import com.realestate.bean.ExtraCharges;
import com.realestate.bean.Flat;
import com.realestate.bean.Floor;
import com.realestate.bean.GeneratePaymentScheduler;
import com.realestate.bean.LocationArea;
import com.realestate.bean.Login;
import com.realestate.bean.NumberToWordConversion;
import com.realestate.bean.PaymentScheduler;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.Receipt;
import com.realestate.bean.State;
import com.realestate.bean.UserAssignedProject;
import com.realestate.configuration.CommanController;
import com.realestate.repository.AggreementRepository;
import com.realestate.repository.BookingRepository;
import com.realestate.repository.CityRepository;
import com.realestate.repository.CompanyRepository;
import com.realestate.repository.CountryRepository;
import com.realestate.repository.CustomerPaymentDetailsRepository;
import com.realestate.repository.CustomerReceiptFormRepository;
import com.realestate.repository.EnquirySourceRepository;
import com.realestate.repository.ExtraChargeRepository;
import com.realestate.repository.FlatRepository;
import com.realestate.repository.LocationAreaRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.ReceiptRepository;
import com.realestate.repository.StateRepository;
import com.realestate.bean.CustomerFlatCheckList;
import com.realestate.bean.DemandPaymentDetailsReport;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;

@Controller
@RequestMapping("/")
public class ReceiptAndReportController
{
	@Autowired
	LocationAreaRepository locationAreaRepository;
	@Autowired
	CityRepository cityRepository;
	@Autowired
	StateRepository stateRepository;
	@Autowired 
	AggreementRepository aggreementRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	EnquirySourceRepository enquirySourceRepository;
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	CompanyRepository companyRepository;
	@Autowired
	ExtraChargeRepository extraChargeRepository;
	@Autowired
	BookingRepository bookingRepository;
	@Autowired
	ReceiptRepository receiptRepository;
	@Autowired
	FlatRepository flatRepository;
	@Autowired
	CustomerReceiptFormRepository customerReceiptFormRepository;
	@Autowired
	CustomerPaymentDetailsRepository customerPaymentDetailsRepository;
	@Autowired
	ServletContext context; 
	@Autowired
	CountryRepository countryRepository;

	public long receiptNumberGeneration()
	{
		long receiptNum = receiptRepository.count()+1;

		return receiptNum;
	}


	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@ResponseBody
	@RequestMapping(value="FlatBookingLetter")
	public ResponseEntity<byte[]> BookingLetter(@RequestParam("bookingId") String bookingId, ModelMap model, HttpServletResponse response )
	{
		int index=0;
		JasperPrint print;
		String projectAddress="",sno="";
		Double infrastructreCost = 0.00d;
		try 
		{	
			Query query = new Query();

			Collection c = new ArrayList();
			HashMap jmap = new HashMap();
			jmap.put("index", ""+(index++));
			c.add(jmap);

			jmap = null;

			String bookingLetterSub ="", perticularsDetail1="", customerDetails1="", customerDetails2="",flatNo="";
			query = new Query();
			List<Booking> bookingList = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)),Booking.class);
			query = new Query();
			List<CustomerPaymentDetails> customerPayment = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)),CustomerPaymentDetails.class);

			Double totalPaidAmount=0.0;
			for(int i=0;i<customerPayment.size();i++)
			{
				totalPaidAmount=totalPaidAmount+customerPayment.get(i).getTotalpayAggreement()+customerPayment.get(i).getTotalpayStampDuty()+customerPayment.get(i).getTotalpayRegistration()+customerPayment.get(i).getTotalpaygstAmount();
			}

			// String projectName = bookingList.get(0).getProjectId();

			query = new Query();
			List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(bookingList.get(0).getProjectId())),Project.class);

			query = new Query();
			Company companyDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("companyId").is(projectDetails.get(0).getCompanyId())), Company.class);

			query = new Query();
			Flat flatDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("flatId").is(bookingList.get(0).getFlatId())), Flat.class);
			/*
			// booking Address
			query =new Query();
			Country bookingCountryDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("countryId").is(bookingList.get(0).getCountryId())), Country.class);
			query =new Query();
			State bookingStateDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("stateId").is(bookingList.get(0).getStateId())), State.class);
			query =new Query();
			City bookingCityDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("cityId").is(bookingList.get(0).getCityId())), City.class);
			query =new Query();
			LocationArea bookingLocationareaDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("locationareaId").is(bookingList.get(0).getLocationareaId())), LocationArea.class);
			 */
			//project Address
			query =new Query();
			State projectStateDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("stateId").is(projectDetails.get(0).getStateId())), State.class);
			query =new Query();
			City projectCityDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("cityId").is(projectDetails.get(0).getCityId())), City.class);



			projectAddress =  projectDetails.get(0).getProjectAddress()+", "+projectCityDetails.getCityName()+", "+projectStateDetails.getStateName();
			sno = projectDetails.get(0).getGatORserveyNumber();

			/*for(int i=0;i<extraChargesList.size();i++)
				 {
					 if(bookingList.get(0).getBookingId().equals(extraChargesList.get(i).getBookingId()))
					 {
					    infrastructreCost = extraChargesList.get(i).getParkingCharges();
					    break;
					 }
					 else
					 {
						 infrastructreCost =0.00d;
					 }
				 }*/


			query =new Query();
			List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(bookingList.get(0).getBuildingId())), ProjectBuilding.class);

			query =new Query();
			List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(bookingList.get(0).getWingId())), ProjectWing.class);

			query =new Query();
			List<Floor> floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("floorId").is(bookingList.get(0).getFloorId())), Floor.class);


			bookingLetterSub = bookingLetterSub + "Flat No. "+flatDetails.getFlatNumber()+", "+wingDetails.get(0).getWingName()+", of proposed building known as "+projectDetails.get(0).getProjectName()+", Building "+buildingDetails.get(0).getBuildingName()+" of the said Project at Gat/Servey No.: "+sno+", at Village "+projectAddress+"...";

			perticularsDetail1 = perticularsDetail1 +"Flat No. "+flatDetails.getFlatNumber()+", "+wingDetails.get(0).getWingName()+", "+buildingDetails.get(0).getBuildingName()+" adm carpet area "+flatDetails.getFlatArea()+" i.e. on "+floorDetails.get(0).getFloortypeName()+", in scheme known as "+projectDetails.get(0).getProjectName();

			customerDetails1 = customerDetails1 + "I "+bookingList.get(0).getBookingfirstname()+" "+" & occupation "+bookingList.get(0).getBookingOccupation()+", residing at "+bookingList.get(0).getBookingaddress()+", Contact No. "+bookingList.get(0).getBookingmobileNumber1()+".";

			NumberToWordConversion run = new NumberToWordConversion();
			String totalinWords = run.convertToIndianCurrency(totalPaidAmount.toString());

			query = new Query();
			List<CompanyBanks> CompanyBanksList = mongoTemplate.find(query.addCriteria(Criteria.where("companyId").is(companyDetails.getCompanyId())), CompanyBanks.class); 
			SimpleDateFormat formatter = new SimpleDateFormat("d/M/yyyy"); 
			if(CompanyBanksList.size()!=0)
			{
				customerDetails2 = customerDetails2 + "I am/We are depositing here with the amount of Rs."+totalPaidAmount.longValue()+"/- ("+totalinWords+") A/C No."+CompanyBanksList.get(0).getCompanyBankacno()+", in "+CompanyBanksList.get(0).getCompanyBankname()+", Dated on : "+formatter.format(bookingList.get(0).getCreationDate())+" with this application as an application money.(pl note amount should not be more than 10% of apartment cost).";	 
			}
			else
			{
				customerDetails2 = customerDetails2 + "I am/We are depositing here with the amount of Rs."+totalPaidAmount.longValue()+"/- ("+totalinWords+") A/C No. ************, in *************, Dated on : "+formatter.format(bookingList.get(0).getCreationDate())+" with this application as an application money.(pl note amount should not be more than 10% of apartment cost).";
			}

			String realPath =CommanController.GetLogoImagePath();
			//String realPath = "//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img";	 

			JRDataSource dataSource = new JRMapCollectionDataSource(c);
			Map<String, Object> parameterMap = new HashMap();

			Date date = new Date();  
			String strDate= formatter.format(date);


			/*1*/ parameterMap.put("Date", ""+strDate);

			/*2*/ parameterMap.put("customerName", ""+bookingList.get(0).getBookingfirstname().toUpperCase()+" ");

			/*3*/ parameterMap.put("customerAddress",""+bookingList.get(0).getBookingaddress()+", "+bookingList.get(0).getBookingPincode());

			//+bookingLocationareaDetails.getLocationareaName()+", "+bookingCityDetails.getCityName()+", "+bookingStateDetails.getStateName()+"-"+bookingList.get(0).getBookingPincode()
			/*4*/ parameterMap.put("mobileNumberAndMailId",""+bookingList.get(0).getBookingmobileNumber1()+", "+bookingList.get(0).getBookingEmail());

			/*5*/ parameterMap.put("emailId",""+bookingList.get(0).getBookingEmail());

			/*6*/ parameterMap.put("companyName",""+companyDetails.getCompanyName());

			/*7*/ parameterMap.put("bookingLetterSub", ""+bookingLetterSub);

			/*9*/ parameterMap.put("perticularsDetail1", ""+perticularsDetail1);

			parameterMap.put("agreementCost", ""+bookingList.get(0).getAggreementValue1().longValue());

			parameterMap.put("customerDetails1", ""+customerDetails1);

			parameterMap.put("customerDetails2", ""+customerDetails2);

			parameterMap.put("flatNo", "However, in the event you reject my offer or I fail to get the said agreement executed and registered within the the stipulated period then you are entitled to sell/ allot the said flat to any third person without taking any prior consent from me.In such event you will be entitled to refund the said amount without interest after deducting there from costs incurred by you, issuing a cheque of the said amount in my name. After the stipulated peroid of 30 days I will not be entitled to make any claim in the aforesaid Flat No."+flatDetails.getFlatNumber()+", "+wingDetails.get(0).getWingName()+", "+buildingDetails.get(0).getBuildingName()+", "+projectDetails.get(0).getProjectName()+".");

			parameterMap.put("companyParagraph", "We  understand  that  the Allotment/Agreement and/or  booking  of  the "+flatDetails.getFlatNumber()+" shall only be  by  the execution of agreement  and  passing  of  the  receipt  of  the  application money  does not amount of the  Allotment Agreement  of  the  said  Flat.The  entire  discretion to reject  this application without any reasonable cause remains with "+companyDetails.getCompanyName().toUpperCase()+".");

			parameterMap.put("flatCost", ""+bookingList.get(0).getFlatbasicCost().longValue());

			parameterMap.put("infrastructureCost",""+bookingList.get(0).getInfrastructureCharge().longValue());

			parameterMap.put("stampDuty", ""+bookingList.get(0).getStampDuty1().longValue());

			parameterMap.put("registration",""+bookingList.get(0).getRegistrationCost1().longValue());

			parameterMap.put("gst", ""+bookingList.get(0).getGstAmount1().longValue());

			parameterMap.put("total", ""+bookingList.get(0).getGrandTotal1().longValue());

			parameterMap.put("realPath",""+realPath);

			parameterMap.put("enqNo",""+bookingList.get(0).getEnquiryId());

			parameterMap.put("bkgNo",""+bookingList.get(0).getBookingId());

			parameterMap.put("buildingNo",""+buildingDetails.get(0).getBuildingName().toUpperCase());

			parameterMap.put("wingName",""+wingDetails.get(0).getWingName().toUpperCase());

			parameterMap.put("flatNum",""+flatDetails.getFlatNumber());


			/* if(bookingList.get(0).getBookingDob().isEmpty())
			 {
				 21 parameterMap.put("customerAge","-");
			 }
			 else
			 {
				 String customerAge = bookingList.get(0).getBookingDob();
				 if(customerAge != null)
				 {
					 Date formatter2 = new SimpleDateFormat("dd/MM/yyyy").parse(customerAge);

					 //getting year from birth date
					 SimpleDateFormat formatNowYear = new SimpleDateFormat("yyyy");
					 String CustYear = formatNowYear.format(formatter2);
					 int birthyear = Integer.parseInt(CustYear);

					 //getting month from birth date
					 SimpleDateFormat formatNowYear3 = new SimpleDateFormat("yyyy");
					 String CustMonth = formatNowYear.format(formatter2);
					 int birthmonth = Integer.parseInt(CustMonth);

					 //Age Calculation
					 SimpleDateFormat formatNowYear1 = new SimpleDateFormat("yyyy");
					 java.util.Date nowDate = new java.util.Date();
					 int currentYear = Integer.parseInt(formatNowYear1.format(nowDate));


					 SimpleDateFormat formatNowYear2 = new SimpleDateFormat("M");
					 java.util.Date nowDate1 = new java.util.Date();
					 int currentMonth = Integer.parseInt(formatNowYear1.format(nowDate1));
					 //int currentmonth = currentYear - birthyear;

					 int Age=0;

					 if(birthmonth<currentMonth)
					 {
						 Age = (currentYear - birthyear)-1;
					 }
					 else if(birthmonth>=currentMonth)
					 {
						 Age = currentYear - birthyear;
					 }

					 21 parameterMap.put("customerAge",""+Age);
				 }
			 }*/


			String projectname = projectDetails.get(0).getProjectName();
			String buildingname = buildingDetails.get(0).getBuildingName();
			String wingname = wingDetails.get(0).getWingName();

			//List<PaymentScheduler> paymentScheduleDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectName").is(projectname).and("buildingName").is(buildingname).and("wingName").is(wingname)),PaymentScheduler.class);

			/*for(int i=0;i<paymentScheduleDetails.size();i++)
			 {
				 /*31 parameterMap.put("paymentScheduleDesc", paymentScheduleDetails.get);
			 }*/

			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/FlatBookingLetter.jasper");


			print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(print, baos);

			byte[] contents = baos.toByteArray();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			String filename = "FlatBookingLetter.pdf";

			JasperExportManager.exportReportToPdfStream(print, baos);


			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

			ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
			response.setHeader("Content-Disposition", "inline; filename=" + filename );

			return resp;					    
		}
		catch(Exception ex)
		{
			//redirectAttributes.addFlashAttribute("flagemail", 0);
		}

		return null;
	}


	@ResponseBody
	@RequestMapping(value="BookingReceipt")
	public ResponseEntity<byte[]> BookingReceipt(@RequestParam("bookingId") String bookingId, ModelMap model, HttpServletResponse response, HttpServletRequest req )
	{
		int index=0;
		JasperPrint print;
		String projectAddress="", companyName="", companyAddress="", companyLocationCityStatePin="",companyPhoneNo="",tanNo="", gstNo="";
		try 
		{	
			Query query = new Query();

			Collection c = new ArrayList();
			HashMap jmap = new HashMap();
			jmap.put("index", ""+(index++));
			c.add(jmap);

			jmap = null;


			List<Booking> bookingList = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)),Booking.class);

			query =new Query();
			List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(bookingList.get(0).getProjectId())), Project.class);

			query =new Query();
			List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(bookingList.get(0).getBuildingId())), ProjectBuilding.class);

			query =new Query();
			List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(bookingList.get(0).getWingId())), ProjectWing.class);

			query =new Query();
			List<Floor> floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("floorId").is(bookingList.get(0).getFloorId())), Floor.class);

			query =new Query();
			List<Flat> flatDetails = mongoTemplate.find(query.addCriteria(Criteria.where("flatId").is(bookingList.get(0).getFlatId())), Flat.class);

			query =new Query();
			List<Company> companyDetails = mongoTemplate.find(query.addCriteria(Criteria.where("companyId").is(projectDetails.get(0).getCompanyId())), Company.class);


			// company Address
			query =new Query();
			State companyStateDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("stateId").is(companyDetails.get(0).getStateId())), State.class);
			query =new Query();
			City companyCityDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("cityId").is(companyDetails.get(0).getCityId())), City.class);
			query =new Query();
			LocationArea companyLocationareaDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("locationareaId").is(companyDetails.get(0).getLocationareaId())), LocationArea.class);

			//project Address
			query =new Query();
			State projectStateDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("stateId").is(projectDetails.get(0).getStateId())), State.class);
			query =new Query();
			City projectCityDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("cityId").is(projectDetails.get(0).getCityId())), City.class);

			projectAddress =  projectDetails.get(0).getProjectAddress()+", "+projectCityDetails.getCityName()+", "+projectStateDetails.getStateName();

			companyAddress = companyDetails.get(0).getCompanyAddress();
			companyLocationCityStatePin = companyLocationareaDetails.getLocationareaName()+","+companyCityDetails.getCityName()+","+companyStateDetails.getStateName()+"-"+companyDetails.get(0).getCompanyPincode();
			companyPhoneNo = companyDetails.get(0).getCompanyPhoneno();
			tanNo ="remained";
			gstNo = companyDetails.get(0).getCompanyGstno();


			JRDataSource dataSource = new JRMapCollectionDataSource(c);
			Map<String, Object> parameterMap = new HashMap();

			Date date = new Date();  
			SimpleDateFormat formatter = new SimpleDateFormat("d/M/yyyy");  
			String strDate= formatter.format(date);

			//code to store receipt details in database
			long receiptNo = receiptNumberGeneration();
			try
			{
				Receipt receipt = new Receipt(receiptNo,"Booking Receipt",bookingList.get(0).getBookingId(),"Printed");
				receiptRepository.save(receipt);
			}
			catch(DuplicateKeyException de)
			{
				de.printStackTrace();
			} 
			
			String realPath =CommanController.GetLogoImagePath();
			//String realPath = "//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img"; //context.getRealPath("/resources/dist/img");

			//Booking Receipt Generation Code
			/*1*/ parameterMap.put("ReceiptNo", ""+receiptNo);
			/*2*/ parameterMap.put("receiptDate", ""+strDate);
			/*3*/ parameterMap.put("bookingId", ""+bookingList.get(0).getBookingId());
			/*4*/ parameterMap.put("companyName", ""+companyName.toUpperCase());
			/*5*/ parameterMap.put("companyAddress", ""+companyAddress);
			/*6*/ parameterMap.put("Location_City_State_pin", ""+companyLocationCityStatePin);
			/*7*/ parameterMap.put("companyPhoneNo",""+companyPhoneNo);
			/*8*/ parameterMap.put("customerName", ""+bookingList.get(0).getBookingfirstname().toUpperCase());


			//Formating for booking amount in double
			Double d = new Double(0.00d);
			double dd = d.doubleValue();
			DecimalFormat formatter1 = new DecimalFormat("##0.00##");

			String bookingAmount = formatter1.format(dd); 


			/*9*/parameterMap.put("paymentDetails", " Payment Through:customization , Ref. No.:customization, Rs. "+bookingAmount+"/- Date:"+formatter.format(formatter.format(bookingList.get(0).getCreationDate()))+", Bank Name: customization, IFSC:customization");

			//code to convert amount in words
			NumberToWordConversion run = new NumberToWordConversion();
			String totalinWords = run.convertToIndianCurrency(bookingAmount);

			/*10*/parameterMap.put("amountInWords",""+totalinWords);
			/*11*/ parameterMap.put("projectName", ""+projectDetails.get(0).getProjectName().toUpperCase());
			/*12*/ parameterMap.put("buildingName", ""+buildingDetails.get(0).getBuildingName().toUpperCase());
			/*13*/ parameterMap.put("wingName", ""+wingDetails.get(0).getWingName().toUpperCase());
			/*14*/ parameterMap.put("floorNo", ""+floorDetails.get(0).getFloortypeName());
			/*15*/ parameterMap.put("flatNumber",""+flatDetails.get(0).getFlatNumber());
			/*16*/ parameterMap.put("flatArea",""+bookingList.get(0).getFlatareainSqFt());
			/*17*/ parameterMap.put("paymentDetail1","Cheque No:Customization, Ref.No., Dated:"+formatter.format(bookingList.get(0).getCreationDate())+", Bank:, IFSC:");
			/*18*/ parameterMap.put("bookingAmount",""+bookingAmount);
			/*19*/ parameterMap.put("realPath",realPath);

			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/BookingReceipt.jasper");


			print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(print, baos);

			byte[] contents = baos.toByteArray();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			String filename = "FlatBookingReceipt.pdf";

			JasperExportManager.exportReportToPdfStream(print, baos);


			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

			ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
			response.setHeader("Content-Disposition", "inline; filename=" + filename );

			return resp;					    
		}
		catch(Exception ex)
		{
			return null;
		}

	}


	@ResponseBody
	@RequestMapping(value="CustomerPaymentSchedule")
	public ResponseEntity<byte[]> CustomerPaymentSchedule(@RequestParam("bookingId") String bookingId, ModelMap model, HttpServletResponse response)
	{
		int index=0;
		JasperPrint print;
		String customerName="";
		try 
		{	
			Query query = new Query();
			HashMap jmap = new HashMap();
			Collection c = new ArrayList();

			List<GeneratePaymentScheduler> CustomerPaymentScheduleDetail = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)),GeneratePaymentScheduler.class);
			List<Booking> bookingList = bookingRepository.findAll();

			for(int i=0;i<bookingList.size();i++)
			{
				if(bookingList.get(i).getBookingId().equals(CustomerPaymentScheduleDetail.get(0).getBookingId()))
				{
					customerName =  bookingList.get(i).getBookingfirstname().toUpperCase();
					break;
				}
			}

			Query query1 = new Query();
			List<GeneratePaymentScheduler> paymentScheduleDetails = mongoTemplate.find(query1.addCriteria(Criteria.where("bookingId").is(bookingId)), GeneratePaymentScheduler.class);

			for(int i=0;i<paymentScheduleDetails.size();i++)
			{
				jmap = new HashMap();
				jmap.put("srno",""+(i+1));
				//jmap.put("srno","1");
				jmap.put("installmentNo",""+paymentScheduleDetails.get(i).getInstallmentNumber());
				jmap.put("paymentDescription", ""+paymentScheduleDetails.get(i).getPaymentDecription());
				jmap.put("per",""+paymentScheduleDetails.get(i).getPercentage());

				c.add(jmap);

				jmap = null;
			}

			JRDataSource dataSource = new JRMapCollectionDataSource(c);
			Map<String, Object> parameterMap = new HashMap<String, Object>();

			Date date = new Date();  
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
			String strDate= formatter.format(date);


			//Booking Receipt Generation Code
			/*1*/ parameterMap.put("bookingId", ""+CustomerPaymentScheduleDetail.get(0).getBookingId());
			/*2*/ parameterMap.put("customerName", ""+customerName);
			/*3*/ parameterMap.put("receiptDate", ""+strDate);

			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/CustomerPaymentScheduleReceipt.jasper");

			print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(print, baos);

			byte[] contents = baos.toByteArray();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			String filename = "PaymentSchedule.pdf";

			JasperExportManager.exportReportToPdfStream(print, baos);


			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

			ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
			response.setHeader("Content-Disposition", "inline; filename=" + filename );

			return resp;					    
		}
		catch(Exception ex)
		{
			return null;
		}

	}

	//Code for maintenance receipt
	@ResponseBody
	@RequestMapping(value="/OtherPaymentReceipt")
	public ResponseEntity<byte[]> OtherPaymentReceipt(@RequestParam("receiptId") String receiptId, ModelMap model, HttpServletResponse response)
	{
		int index=0;
		JasperPrint print;
		String customerName="", bankName_Address_ChequeNo_Date="",flatNumber_floorNum_projectName="",gatORserveyNum="",projectAddress="",companyName="";
		try 
		{	
			Query query = new Query();
			Query query1 = new Query();
			Collection c = new ArrayList();
			HashMap jmap = new HashMap();
			jmap.put("index", ""+(index++));
			c.add(jmap);

			jmap = null;
			List<CustomerReceiptForm> paymentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("receiptId").is(receiptId)),CustomerReceiptForm.class);

			List<Booking> bookingList = mongoTemplate.find(query1.addCriteria(Criteria.where("bookingId").is(paymentDetails.get(0).getBookingId())), Booking.class);

			Date date = new Date();  
			SimpleDateFormat formatter = new SimpleDateFormat("d/M/yyyy");  
			String strDate= formatter.format(date);

			query =new Query();
			List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(bookingList.get(0).getProjectId())), Project.class);

			query =new Query();
			List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(bookingList.get(0).getBuildingId())), ProjectBuilding.class);

			query =new Query();
			List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(bookingList.get(0).getWingId())), ProjectWing.class);

			query =new Query();
			List<Floor> floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("floorId").is(bookingList.get(0).getFloorId())), Floor.class);

			query =new Query();
			List<Flat> flatDetails = mongoTemplate.find(query.addCriteria(Criteria.where("flatId").is(bookingList.get(0).getFlatId())), Flat.class);

			query =new Query();
			List<Company> companyDetails = mongoTemplate.find(query.addCriteria(Criteria.where("companyId").is(projectDetails.get(0).getCompanyId())), Company.class);

			customerName =  " Mr/Mrs. "+bookingList.get(0).getBookingfirstname().toUpperCase();

			if(paymentDetails.get(0).getPaymentMode().equals("Cash"))
			{
				bankName_Address_ChequeNo_Date = "Payment Mode    :    Cash  "+", Dated  :  "+formatter.format(paymentDetails.get(0).getCreationDate());
			}
			else
			{
				bankName_Address_ChequeNo_Date = "Bank : "+paymentDetails.get(0).getBankName()+",   Cheque No : "+paymentDetails.get(0).getChequeNumber()+", Dated : "+formatter.format(paymentDetails.get(0).getCreationDate());	 
			}

			flatNumber_floorNum_projectName = "Flat no.:"+flatDetails.get(0).getFlatNumber()+", on "+floorDetails.get(0).getFloortypeName()+", Wing: \""+wingDetails.get(0).getWingName()+", Bldg : "+buildingDetails.get(0).getBuildingName()+", In "+projectDetails.get(0).getProjectName()+"\"";

			gatORserveyNum = projectDetails.get(0).getGatORserveyNumber().toString();

			query =new Query();
			LocationArea locationArea=mongoTemplate.findOne(query.addCriteria(Criteria.where("locationareaId").is(projectDetails.get(0).getLocationareaId())), LocationArea.class);

			query =new Query();
			City city=mongoTemplate.findOne(query.addCriteria(Criteria.where("cityId").is(projectDetails.get(0).getCityId())), City.class);

			projectAddress = locationArea.getLocationareaName()+", "+city.getCityName()+"-"+projectDetails.get(0).getAreaPincode().toString();
			companyName = companyDetails.get(0).getCompanyName().toString();
			String realPath =CommanController.GetLogoImagePath();
			//String realPath ="//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img"; //context.getRealPath("/resources/dist/img");
			//String realPath="D:/Data1";
			JRDataSource dataSource = new JRMapCollectionDataSource(c);
			Map<String, Object> parameterMap = new HashMap();

			//code to store receipt details in database

			NumberToWordConversion run = new NumberToWordConversion();
			String AmountinWords = run.convertToIndianCurrency(paymentDetails.get(0).getPaymentAmount().toString());

			String typeWiseReceiptPaymentData = "";

			/*8*/ parameterMap.put("applicantName_1_2", ""+customerName);
			/*9*/ parameterMap.put("companyName", ""+companyName);
			/*10*/ parameterMap.put("receiptDate", ""+strDate);
			/*11*/ parameterMap.put("receiptNumber",""+paymentDetails.get(0).getReceiptId());
			/*12*/ parameterMap.put("realPath",realPath);


			typeWiseReceiptPaymentData = "Received with thanks a sum of "+paymentDetails.get(0).getPaymentAmount().longValue()+"/- ("+AmountinWords+") wide By";
			try
			{
				if(paymentDetails.get(0).getBankName().equals(""))
				{
					typeWiseReceiptPaymentData = typeWiseReceiptPaymentData + " Cash ";
				}
				else
				{
					typeWiseReceiptPaymentData = typeWiseReceiptPaymentData + " Bank Name : "+paymentDetails.get(0).getBankName()+", ";
					try {
						typeWiseReceiptPaymentData=typeWiseReceiptPaymentData + " Branch : "+paymentDetails.get(0).getBranchName()+", ";
					}
					catch (Exception e) {
						// TODO: handle exception
					}
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			try
			{
				if(paymentDetails.get(0).getPaymentMode().equals("Cash"))
				{

				}	
				if(paymentDetails.get(0).getPaymentMode().equals("Cheque"))
				{
					typeWiseReceiptPaymentData = typeWiseReceiptPaymentData + " Cheq. No: "+paymentDetails.get(0).getChequeNumber();
				}
				else if(paymentDetails.get(0).getPaymentMode().equals("DD"))
				{
					typeWiseReceiptPaymentData = typeWiseReceiptPaymentData + " DD No: "+paymentDetails.get(0).getChequeNumber();
				}
				else if(paymentDetails.get(0).getPaymentMode().equals("RTGS"))
				{
					typeWiseReceiptPaymentData = typeWiseReceiptPaymentData + " RTGS No: "+paymentDetails.get(0).getChequeNumber();
				}
				else if(paymentDetails.get(0).getPaymentMode().equals("NEFT"))
				{
					typeWiseReceiptPaymentData = typeWiseReceiptPaymentData + " NEFT No: "+paymentDetails.get(0).getChequeNumber();
				}
				else if(paymentDetails.get(0).getPaymentMode().equals("IMPS"))
				{
					typeWiseReceiptPaymentData = typeWiseReceiptPaymentData + " IMPS No: "+paymentDetails.get(0).getChequeNumber();
				}
				else if(paymentDetails.get(0).getPaymentMode().equals("Online"))
				{
					typeWiseReceiptPaymentData = typeWiseReceiptPaymentData + " RTGS No: "+paymentDetails.get(0).getChequeNumber();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			typeWiseReceiptPaymentData = typeWiseReceiptPaymentData + " against "+paymentDetails.get(0).getPaymentType()+" for Flat No. "+flatDetails.get(0).getFlatNumber()+" on "+floorDetails.get(0).getFloortypeName()+", Wing : "+wingDetails.get(0).getWingName()+", Bldg : "+buildingDetails.get(0).getBuildingName()+", In "+projectDetails.get(0).getProjectName()+" situated at Gat/Sr No. : "+gatORserveyNum+", Address : "+projectAddress;


			/*13*/ parameterMap.put("typeWiseReceiptPaymentData",""+typeWiseReceiptPaymentData);

			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/TypeWiseReceipt.jasper");

			print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(print, baos);

			byte[] contents = baos.toByteArray();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			String filename = "CustomerReceipt.pdf";

			JasperExportManager.exportReportToPdfStream(print, baos);


			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

			ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
			response.setHeader("Content-Disposition", "inline; filename=" + filename );

			return resp;					    
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			return null;
		}

	}


	//Code generation for demand letter
	@ResponseBody
	@RequestMapping(value="/DemandLetter")
	public ResponseEntity<byte[]> DemandLetter(@RequestParam("bookingId") String bookingId, ModelMap model, HttpServletResponse response)
	{
		int index=0;
		JasperPrint print;

		String customerName="",customerAddress="", bankName_Address_ChequeNo_Date="",flatNumber_floorNum_projectName="",gatORserveyNum="",projectAddress="",companyName="",companyContact="",companyBankIfscCode="",companyBankAcNo_BankName_BranchName="";
		String projectName="",buildingName="",wingName="", companyId="";
		String demandLetterSub="", demandDate="", companyAndBankAcDetails="";
		try 
		{	
			Query query = new Query();
			Query query1 = new Query();
			Query query2 = new Query();
			Query query3 = new Query();
			Collection c = new ArrayList();
			HashMap jmap = new HashMap();
			jmap.put("index",""+(index++));
			c.add(jmap);

			jmap = null;

			List<Booking> BookingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)),Booking.class);


			query =new Query();
			List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(BookingDetails.get(0).getProjectId())), Project.class);

			query =new Query();
			List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(BookingDetails.get(0).getBuildingId())), ProjectBuilding.class);

			query =new Query();
			List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(BookingDetails.get(0).getWingId())), ProjectWing.class);

			query =new Query();
			List<Floor> floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("floorId").is(BookingDetails.get(0).getFloorId())), Floor.class);

			query =new Query();
			List<Flat> flatDetails = mongoTemplate.find(query.addCriteria(Criteria.where("flatId").is(BookingDetails.get(0).getFlatId())), Flat.class);

			query =new Query();
			List<Company> companyDetails = mongoTemplate.find(query.addCriteria(Criteria.where("companyId").is(projectDetails.get(0).getCompanyId())), Company.class);

			projectName = projectDetails.get(0).getProjectName();
			buildingName = buildingDetails.get(0).getBuildingName();
			wingName = wingDetails.get(0).getWingName();

			List<GeneratePaymentScheduler> customerPaymentSchedule = mongoTemplate.find(query2.addCriteria(Criteria.where("bookingId").is(bookingId)),GeneratePaymentScheduler.class);

			List<PaymentScheduler> paymentSchedulerList = mongoTemplate.find(query3.addCriteria(Criteria.where("projectName").is(projectName).and("buildingName").is(buildingName).and("wingName").is(wingName).and("slabStatus").is("Completed")), PaymentScheduler.class);


			// booking Address

			//project Address
			query =new Query();
			City projectCityDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("cityId").is(projectDetails.get(0).getCityId())), City.class);
			query =new Query();
			LocationArea projectLocationareaDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("locationareaId").is(projectDetails.get(0).getLocationareaId())), LocationArea.class);

			Date date = new Date();  
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
			String strDate= formatter.format(date);


			double per=0;
			for(int i=0;i<paymentSchedulerList.size();i++)
			{
				per = per + paymentSchedulerList.get(i).getPercentage();
			}

			customerName =  BookingDetails.get(0).getBookingfirstname()+" "+BookingDetails.get(0).getBookingmiddlename()+" "+BookingDetails.get(0).getBookinglastname();
			customerAddress = BookingDetails.get(0).getBookingaddress()+","+BookingDetails.get(0).getBookingPincode();
			bankName_Address_ChequeNo_Date = ", Dated:"+formatter.format(BookingDetails.get(0).getCreationDate());
			flatNumber_floorNum_projectName = "Flat no.:"+flatDetails.get(0).getFlatNumber()+", on "+floorDetails.get(0).getFloortypeName()+", in \""+projectDetails.get(0).getProjectName()+"\"";

			gatORserveyNum = projectDetails.get(0).getGatORserveyNumber().toString();
			projectAddress = projectLocationareaDetails.getLocationareaName()+", "+projectCityDetails.getCityName()+"-"+projectDetails.get(0).getAreaPincode().toString();

			companyName =companyDetails.get(0).getCompanyName();

			companyContact = companyDetails.get(0).getCompanyPhoneno();
			companyId = companyDetails.get(0).getCompanyId();

			query = new Query();

			List<CompanyBanks> companyBankDetails = mongoTemplate.find(query.addCriteria(Criteria.where("companyId").is(companyId)), CompanyBanks.class);

			if(companyBankDetails.size()!=0)
			{
				companyBankIfscCode = companyBankDetails.get(companyBankDetails.size()-1).getBankifscCode();
				companyBankAcNo_BankName_BranchName = companyBankDetails.get(companyBankDetails.size()-1).getCompanyBankacno()+", "+companyBankDetails.get(companyBankDetails.size()-1).getCompanyBankname()+", "+companyBankDetails.get(companyBankDetails.size()-1).getBranchName();
			}
			else
			{
				companyBankIfscCode = "Not Present";
				companyBankAcNo_BankName_BranchName = "Not Present";
			}

			JRDataSource dataSource = new JRMapCollectionDataSource(c);
			Map<String, Object> parameterMap = new HashMap();


			demandLetterSub = demandLetterSub + "Due Installment towards Flat No.-"+flatDetails.get(0).getFlatNumber()+", in Wing "+wingDetails.get(0).getWingName()+", of Building "+buildingDetails.get(0).getBuildingName()+", in out project named \""+projectDetails.get(0).getProjectName()+"\" situated at Gat/Survey no. "+gatORserveyNum+" at "+projectAddress+".";

			demandDate = demandDate + "This refers to the registered agreement for  sale dated "+formatter.format(BookingDetails.get(0).getCreationDate())+" executed between us for purchase of the above mentioned apartment. As per the payment terms/schedule agreed upon in the said agreement the following amount has become due for the payment.";

			companyAndBankAcDetails = companyAndBankAcDetails + "We request you to pay the above outstanding amount alongwith the taxes as applicable above amount should be paid within 7 days from the date here in to avoid the interest charges as agreed. Cheque/DD to be drawn in favour of "+companyName.toUpperCase()+".  IFSC codes for RTGS- "+companyBankIfscCode+", Bank Account Details- "+companyBankAcNo_BankName_BranchName+".";

			//Booking Receipt Generation Code

			/*1*/  parameterMap.put("date", ""+strDate);
			/*2*/  parameterMap.put("customerName", ""+customerName.toUpperCase());
			/*3*/  parameterMap.put("customerAddress", ""+customerAddress);
			/*4*/  parameterMap.put("customerMobileNo",""+BookingDetails.get(0).getBookingmobileNumber1());
			/*5*/  parameterMap.put("customerEmailId",""+BookingDetails.get(0).getBookingEmail());
			/*6*/  parameterMap.put("flatNumber", ""+flatDetails.get(0).getFlatNumber());
			/*7*/  parameterMap.put("buildingNum", ""+buildingDetails.get(0).getBuildingName().toUpperCase());
			/*8*/  parameterMap.put("wingName", ""+wingDetails.get(0).getWingName().toUpperCase());
			/*9*/  parameterMap.put("projectName", ""+projectDetails.get(0).getProjectName().toUpperCase());
			/*10*/ parameterMap.put("gatNum", ""+gatORserveyNum);
			/*11*/ parameterMap.put("projectAddress", ""+projectAddress);
			/*12*/ parameterMap.put("demandDate", ""+demandDate);
			/*13*/ parameterMap.put("companyName", ""+companyName.toUpperCase());
			/*14*/ parameterMap.put("companyContactNum", ""+companyContact);
			parameterMap.put("demandLetterSub", ""+demandLetterSub);
			parameterMap.put("companyAndBankAcDetails", ""+companyAndBankAcDetails);

			//code to convert agreement value
			/*Double d = new Double(BookingDetails.get(0).getAggreementValue1());
			 double dd = d.doubleValue();
			 DecimalFormat formatter1 = new DecimalFormat("##0.0#");

			 String agreementValue = formatter1.format(dd);*/

			double dueAsPerWork = ((BookingDetails.get(0).getAggreementValue1()*per)/100);
			List<CustomerPaymentDetails> paymentDetails = customerPaymentDetailsRepository.findAll();

			double totalTillDate = 0.0d;
			for(int i=0;i<paymentDetails.size();i++)
			{
				if(BookingDetails.get(0).getBookingId().equals(paymentDetails.get(i).getBookingId()))
				{
					totalTillDate = paymentDetails.get(i).getTotalpayAggreement();
					break;
				}
				else
				{
					totalTillDate = 0.0d;
				}
			}

			double GSTper = BookingDetails.get(0).getGstCost();

			double GSTdue = (dueAsPerWork*GSTper)/100;

			double totalExcludingGST = dueAsPerWork-totalTillDate;

			/*15*/ parameterMap.put("agreementValue", ""+(long)BookingDetails.get(0).getAggreementValue1().doubleValue());
			/*16*/ parameterMap.put("workStagePer", ""+(int)per+"%");
			/*17*/ parameterMap.put("dueAsPerWork", ""+(long)dueAsPerWork);
			/*18*/ parameterMap.put("totalTillDate", ""+(long)totalTillDate);
			/*19*/ parameterMap.put("totalExcludingGST", ""+(long)(dueAsPerWork-totalTillDate));
			/*20*/ parameterMap.put("totalIncludingGST", ""+(long)(totalExcludingGST+GSTdue));
			/*21*/ parameterMap.put("companyBankIfscCode", ""+companyBankIfscCode);
			/*22*/ parameterMap.put("companyBankAcNo_BankName_BranchName", ""+companyBankAcNo_BankName_BranchName);
			/*23*/ parameterMap.put("GSTdue",""+(long)GSTdue);


			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/DemandLetter.jasper");


			print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(print, baos);

			byte[] contents = baos.toByteArray();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			String filename = "DemandLetter.pdf";

			JasperExportManager.exportReportToPdfStream(print, baos);


			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

			ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
			response.setHeader("Content-Disposition", "inline; filename=" + filename );

			return resp;					    
		}
		catch(Exception ex)
		{
			return null;
		}

	}

	@RequestMapping("/WingWiseReport")
	public String WingWiseReport(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<Project> projectList =  GetUserAssigenedProjectList(req,res);

		    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy"); 
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/M/yyyy");
			LocalDate localDate = LocalDate.now();
			String todayDate=dtf.format(localDate);

			Query query = new Query();

			List<Booking> todaysBookingList = mongoTemplate.find(query.addCriteria(Criteria.where("creationDate").is(todayDate)), Booking.class);

			//List<CustomerReceiptForm> paymentDetails = customerReceiptFormRepository.findAll();

			if(todaysBookingList.size()!=0)
			{
				for(int i=0;i<todaysBookingList.size();i++)
				{
					//Formating for booking amount in double
					Double d = new Double(0.00d);
					double dd = d.doubleValue();
					DecimalFormat formatter1 = new DecimalFormat("##0.0##");

					//Formating for booking amount in double
					d = new Double(todaysBookingList.get(i).getGrandTotal1());
					dd = d.doubleValue();
					formatter1 = new DecimalFormat("##0.0##");

					String totalCost = formatter1.format(dd);
					todaysBookingList.get(i).setGrandTotal1(Double.parseDouble(totalCost));
					todaysBookingList.get(i).setStringDate(formatter.format(todaysBookingList.get(i).getCreationDate()));
					//Setting up values of extra charges to infrastructure charges..
					/*if(todaysBookingList.get(i).getBookingId().equals(paymentDetails.get(i).getBookingId()))
					{
						//todaysBookingList.get(i).setInfrastructureCharge((paymentDetails.get(i).get));
						todaysBookingList.get(i).setInfrastructureCharge(0.00);
					}
					else
					{
						todaysBookingList.get(i).setInfrastructureCharge(0.00);
					}*/
				}
			}

			model.addAttribute("projectList",projectList );
			model.addAttribute("todaysBookingList", todaysBookingList);

			return "WingWiseReport";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/EnquiryReport")
	public String EnquiryReport(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<EnquirySource> enquirySourceList =  enquirySourceRepository.findAll();

			Date todayDate=new Date();
			//   String todayDateInString = new SimpleDateFormat("d/M/yyyy").format(new Date());

			final Calendar calendar = Calendar.getInstance();
			calendar.setTime(todayDate);
			calendar.add(Calendar.DAY_OF_YEAR, -1);

			Date date= calendar.getTime();

			Query query = new Query();
			List<Enquiry> todaysEnquiryList = mongoTemplate.find(query.addCriteria(Criteria.where("creationDate").gte(date).lt(todayDate)), Enquiry.class);

			model.addAttribute("enquirySourceList",enquirySourceList);
			model.addAttribute("enquiryList", todaysEnquiryList);

			return "EnquiryReport";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping(value="/PrintEnquiryReport")
	public ResponseEntity<byte[]> PrintEnquiryReport(HttpServletRequest req, ModelMap model, HttpServletResponse response)
	{
		String enquirysourceId = req.getParameter("enquirysourceId");
		String subsourceId = req.getParameter("subsourceId");


		if(!enquirysourceId.equals("Default") && !subsourceId.equals("Default"))
		{
			int index=0;
			JasperPrint print;
			String customerName="";
			try 
			{	
				Query query = new Query();
				HashMap jmap = new HashMap();
				Collection c = new ArrayList();
				/*jmap.put("srno", "1");
				c.add(jmap);
				jmap = null;*/


				List<Enquiry> enquiryList = mongoTemplate.find(query.addCriteria(Criteria.where("enquirysourceId").is(enquirysourceId).and("subsourceId").is(subsourceId)),Enquiry.class);

				/*Query query1 = new Query();
				 	 List<GeneratePaymentScheduler> paymentScheduleDetails = mongoTemplate.find(query1.addCriteria(Criteria.where("bookingId").is(bookingId)), GeneratePaymentScheduler.class);*/

				for(int i=0;i<enquiryList.size();i++)
				{
					jmap = new HashMap();

					jmap.put("srno",""+(i+1));
					jmap.put("enquiryId",""+enquiryList.get(i).getEnquiryId());
					jmap.put("customerName",""+enquiryList.get(i).getEnqfirstName().toUpperCase());
					jmap.put("mobile",""+enquiryList.get(i).getEnqmobileNumber1());
					jmap.put("emailId",""+enquiryList.get(i).getEnqEmail());
					jmap.put("flatType",""+enquiryList.get(i).getFlatType());
					jmap.put("EnqSubSource",""+enquiryList.get(i).getSubsourceId());
					jmap.put("status",""+enquiryList.get(i).getEnqStatus());

					c.add(jmap);
					jmap = null;
				}

				//String realPath ="//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img"; //context.getRealPath("/resources/dist/img");
				String realPath =CommanController.GetLogoImagePath();
				JRDataSource dataSource = new JRMapCollectionDataSource(c);
				Map<String, Object> parameterMap = new HashMap<String, Object>();

				Date date = new Date();  
				SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
				String strDate= formatter.format(date);

				//Booking Receipt Generation Code
				/*1*/ parameterMap.put("reportDate", ""+strDate);
				/*2*/ parameterMap.put("reportName", "Enquiry Source Wise Enquiry Report");
				/*3*/ parameterMap.put("enqSource", ""+enquirysourceId);
				/*4*/ parameterMap.put("subEnqSource", ""+subsourceId);
				/*5*/ parameterMap.put("realPath", realPath);

				InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/EnquiryReport.jasper");

				print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				JasperExportManager.exportReportToPdfStream(print, baos);

				byte[] contents = baos.toByteArray();

				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.parseMediaType("application/pdf"));
				String filename = "WingWiseEnquiryReport.pdf";

				JasperExportManager.exportReportToPdfStream(print, baos);

				headers.setContentType(MediaType.parseMediaType("application/pdf"));
				headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
				ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
				response.setHeader("Content-Disposition", "inline; filename=" + filename );
				return resp;	
			}
			catch(Exception ex)
			{
				return null;
			}
		}
		else if(enquirysourceId.equals("Default") && subsourceId.equals("Default"))
		{
			int index=0;
			JasperPrint print;
			String customerName="";
			try 
			{	
				Query query1 = new Query();
				HashMap jmap = new HashMap();
				Collection c = new ArrayList();
				/*jmap.put("srno", "1"); */


				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/M/yyyy");
				LocalDate localDate = LocalDate.now();
				String todayDate=dtf.format(localDate);

				List<Enquiry> enquiryList = mongoTemplate.find(query1.addCriteria(Criteria.where("creationDate").is(todayDate)), Enquiry.class);

				for(int i=0;i<enquiryList.size();i++)
				{
					jmap = new HashMap();

					jmap.put("srno",""+(i+1));
					jmap.put("enquiryId",""+enquiryList.get(i).getEnquiryId());
					jmap.put("customerName",""+enquiryList.get(i).getEnqfirstName().toUpperCase());
					jmap.put("mobile",""+enquiryList.get(i).getEnqmobileNumber1());
					jmap.put("emailId",""+enquiryList.get(i).getEnqEmail());
					jmap.put("flatType",""+enquiryList.get(i).getFlatType());
					jmap.put("EnqSubSource",""+enquiryList.get(i).getSubsourceId());
					jmap.put("status",""+enquiryList.get(i).getEnqStatus());

					c.add(jmap);
					jmap = null;
				}

				//String realPath ="//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img"; //context.getRealPath("/resources/dist/img");
				String realPath =CommanController.GetLogoImagePath();
				JRDataSource dataSource = new JRMapCollectionDataSource(c);
				Map<String, Object> parameterMap = new HashMap<String, Object>();

				Date date = new Date();  
				SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
				String strDate= formatter.format(date);

				//Booking Receipt Generation Code
				/*1*/ parameterMap.put("reportDate", ""+strDate);
				/*2*/ parameterMap.put("reportName", "Daily Enquiry Report");
				/*3*/ parameterMap.put("projectName", "All");
				/*4*/ parameterMap.put("buildingName", "All");
				/*5*/ parameterMap.put("wingName", "All");
				/*6*/ parameterMap.put("floorName", "All");
				/*7*/ parameterMap.put("realPath", realPath);

				InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/EnquiryReport.jasper");

				print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				JasperExportManager.exportReportToPdfStream(print, baos);

				byte[] contents = baos.toByteArray();

				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.parseMediaType("application/pdf"));
				String filename = "DailyEnquiryReport.pdf";

				JasperExportManager.exportReportToPdfStream(print, baos);

				headers.setContentType(MediaType.parseMediaType("application/pdf"));
				headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
				ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
				response.setHeader("Content-Disposition", "inline; filename=" + filename );
				return resp;	
			}
			catch(Exception e)
			{
				return null;
			}
		}
		else
		{
			return null;
		}
	}

	//wing wise booking report
	@ResponseBody
	@RequestMapping(value="/PrintWingWiseReport")
	public ResponseEntity<byte[]> PrintWingWiseReport(HttpServletRequest req, ModelMap model, HttpServletResponse response)
	{
		String projectId = req.getParameter("projectId");
		String buildingId = req.getParameter("buildingId");
		String wingId = req.getParameter("wingId");
		String floorId = req.getParameter("floorId");


		if(!projectId.equals("Default") && !buildingId.equals("Default") && !wingId.equals("Default") && !floorId.equals("Default"))
		{
			int index=0;
			JasperPrint print;
			String customerName="";
			try 
			{	
				Query query = new Query();
				Query query1;
				HashMap jmap = new HashMap();
				Collection c = new ArrayList();

				List<Booking> bookingList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("floorId").is(floorId)),Booking.class);

				query =new Query();
				List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(bookingList.get(0).getProjectId())), Project.class);

				query =new Query();
				List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(bookingList.get(0).getBuildingId())), ProjectBuilding.class);

				query =new Query();
				List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(bookingList.get(0).getWingId())), ProjectWing.class);

				query =new Query();
				List<Floor> floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("floorId").is(bookingList.get(0).getFloorId())), Floor.class);

				query =new Query();
				List<Flat> flatDetails = mongoTemplate.find(query.addCriteria(Criteria.where("flatId").is(bookingList.get(0).getFlatId())), Flat.class);

				CustomerPaymentDetails paymentDetails = null;
				double reaminingTotalAmount = 0.00d;
				double totalAmount = 0.00d;
				double remainingAmount = 0.00d;

				for(int i=0;i<bookingList.size();i++)
				{
					reaminingTotalAmount = 0.00d;
					query1  = new Query();
					jmap = new HashMap();
					jmap.put("srno",""+(i+1));
					jmap.put("bookingId",""+bookingList.get(i).getBookingId());
					jmap.put("customerName",""+bookingList.get(i).getBookingfirstname().toUpperCase());
					jmap.put("flatType",""+bookingList.get(i).getFlatType());
					jmap.put("flatNo",""+flatDetails.get(i).getFlatNumber());
					jmap.put("flatCost",""+bookingList.get(i).getFlatCost());
					jmap.put("flatBasicCost",""+bookingList.get(i).getFlatbasicCost());
					jmap.put("agreementCost",""+bookingList.get(i).getAggreementValue1());

					paymentDetails = mongoTemplate.findOne(query1.addCriteria(Criteria.where("bookingId").is(bookingList.get(i).getBookingId())), CustomerPaymentDetails.class);

					jmap.put("paidAgreementAmt",""+paymentDetails.getTotalpayAggreement());
					jmap.put("gstAmount",""+bookingList.get(i).getGstAmount1());
					jmap.put("grandTotal",""+bookingList.get(i).getGrandTotal1());

					reaminingTotalAmount = bookingList.get(i).getGrandTotal1()-(paymentDetails.getTotalpayAggreement()+paymentDetails.getTotalpayStampDuty()+paymentDetails.getTotalpayRegistration()+paymentDetails.getTotalpaygstAmount());
					jmap.put("remainingAmount",""+reaminingTotalAmount);

					totalAmount = totalAmount+bookingList.get(i).getGrandTotal1();
					remainingAmount = remainingAmount + reaminingTotalAmount;

					c.add(jmap);
					jmap = null;
				}

				//String realPath ="//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img"; //context.getRealPath("/resources/dist/img");
				String realPath =CommanController.GetLogoImagePath();
				JRDataSource dataSource = new JRMapCollectionDataSource(c);
				Map<String, Object> parameterMap = new HashMap<String, Object>();

				Date date = new Date();  
				SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
				String strDate= formatter.format(date);

				//Booking Receipt Generation Code
				/*1*/ parameterMap.put("reportDate", ""+strDate);
				/*2*/ parameterMap.put("reportName", "Wing Wise Booking Report");
				/*3*/ parameterMap.put("projectName", ""+projectDetails.get(0).getProjectName().toUpperCase());
				/*4*/ parameterMap.put("buildingName", ""+buildingDetails.get(0).getBuildingName().toUpperCase());
				/*5*/ parameterMap.put("wingName", ""+wingDetails.get(0).getWingName().toUpperCase());
				/*6*/ parameterMap.put("floorName", ""+floorDetails.get(0).getFloortypeName());
				/*7*/ parameterMap.put("totalAmount", ""+totalAmount);
				/*8*/ parameterMap.put("reaminingAmount", ""+remainingAmount);
				/*9*/ parameterMap.put("realPath",realPath);

				InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/BookingReport.jasper");

				print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				JasperExportManager.exportReportToPdfStream(print, baos);

				byte[] contents = baos.toByteArray();

				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.parseMediaType("application/pdf"));
				String filename = "WingWiseBookingReport.pdf";

				JasperExportManager.exportReportToPdfStream(print, baos);

				headers.setContentType(MediaType.parseMediaType("application/pdf"));
				headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
				ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
				response.setHeader("Content-Disposition", "inline; filename=" + filename );
				return resp;	
			}
			catch(Exception ex)
			{
				return null;
			}
		}
		else if(projectId.equals("Default") && buildingId.equals("Default") && wingId.equals("Default")  && floorId.equals("Default"))
		{
			int index=0;
			JasperPrint print;
			String customerName="";
			try 
			{	
				Query query = new Query();
				Query query1 = new Query();
				HashMap jmap = new HashMap();
				Collection c = new ArrayList();


				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/M/yyyy");
				LocalDate localDate = LocalDate.now();
				String todayDate=dtf.format(localDate);

				List<Booking> bookingList = mongoTemplate.find(query.addCriteria(Criteria.where("creationDate").is(todayDate)),Booking.class);

				query =new Query();
				List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(bookingList.get(0).getProjectId())), Project.class);

				query =new Query();
				List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(bookingList.get(0).getBuildingId())), ProjectBuilding.class);

				query =new Query();
				List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(bookingList.get(0).getWingId())), ProjectWing.class);

				query =new Query();
				List<Floor> floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("floorId").is(bookingList.get(0).getFloorId())), Floor.class);

				query =new Query();
				List<Flat> flatDetails = mongoTemplate.find(query.addCriteria(Criteria.where("flatId").is(bookingList.get(0).getFlatId())), Flat.class);

				CustomerPaymentDetails paymentDetails = null;
				double reaminingTotalAmount = 0.00d;
				double totalAmount = 0.00d;
				double remainingAmount = 0.00d;


				for(int i=0;i<bookingList.size();i++)
				{
					reaminingTotalAmount = 0.00d;
					query1  = new Query();
					jmap = new HashMap();
					jmap.put("srno",""+(i+1));
					jmap.put("bookingId",""+bookingList.get(i).getBookingId());
					jmap.put("customerName",""+bookingList.get(i).getBookingfirstname().toUpperCase());
					jmap.put("flatType",""+bookingList.get(i).getFlatType());
					jmap.put("flatNo",""+flatDetails.get(i).getFlatNumber());
					jmap.put("flatCost",""+bookingList.get(i).getFlatCost());
					jmap.put("flatBasicCost",""+bookingList.get(i).getFlatbasicCost());
					jmap.put("agreementCost",""+bookingList.get(i).getAggreementValue1());

					paymentDetails = mongoTemplate.findOne(query1.addCriteria(Criteria.where("bookingId").is(bookingList.get(i).getBookingId())), CustomerPaymentDetails.class);

					jmap.put("paidAgreementAmt",""+paymentDetails.getTotalpayAggreement());
					jmap.put("gstAmount",""+bookingList.get(i).getGstAmount1());
					jmap.put("grandTotal",""+bookingList.get(i).getGrandTotal1());

					reaminingTotalAmount = bookingList.get(i).getGrandTotal1()-(paymentDetails.getTotalpayAggreement()+paymentDetails.getTotalpayStampDuty()+paymentDetails.getTotalpayRegistration()+paymentDetails.getTotalpaygstAmount());
					jmap.put("remainingAmount",""+reaminingTotalAmount);

					totalAmount = totalAmount+bookingList.get(i).getGrandTotal1();
					remainingAmount = remainingAmount + reaminingTotalAmount;

					c.add(jmap);
					jmap = null;
				}
				String realPath =CommanController.GetLogoImagePath();
				//String realPath ="//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img"; //context.getRealPath("/resources/dist/img");

				JRDataSource dataSource = new JRMapCollectionDataSource(c);
				Map<String, Object> parameterMap = new HashMap<String, Object>();

				Date date = new Date();  
				SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
				String strDate= formatter.format(date);

				//Booking Receipt Generation Code
				/*1*/ parameterMap.put("reportDate", ""+strDate);
				/*2*/ parameterMap.put("reportName", "Today's Booking Report");
				/*3*/ parameterMap.put("projectName", "All");
				/*4*/ parameterMap.put("buildingName", "All");
				/*5*/ parameterMap.put("wingName", "All");
				/*6*/ parameterMap.put("floorName", "All");
				/*7*/ parameterMap.put("totalAmount", ""+totalAmount);
				/*8*/ parameterMap.put("reaminingAmount", ""+remainingAmount);
				/*9*/ parameterMap.put("realPath",realPath);

				InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/BookingReport.jasper");

				print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				JasperExportManager.exportReportToPdfStream(print, baos);

				byte[] contents = baos.toByteArray();

				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.parseMediaType("application/pdf"));
				String filename = "DailyBookingReport.pdf";

				JasperExportManager.exportReportToPdfStream(print, baos);

				headers.setContentType(MediaType.parseMediaType("application/pdf"));
				headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
				ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
				response.setHeader("Content-Disposition", "inline; filename=" + filename );
				return resp;	
			}
			catch(Exception e)
			{
				return null;
			}
		}
		else
		{
			return null;
		}
	}



	@ResponseBody
	@RequestMapping(value="/FlatWiseReport", method=RequestMethod.POST)
	public ResponseEntity<byte[]> PrintFlatWiseBookingStatusReport(@RequestParam("projectId") String projectId, @RequestParam("buildingId") String buildingId, @RequestParam("wingId") String wingId, @RequestParam("floorId") String floorId, HttpServletRequest req, ModelMap model, HttpServletResponse response)
	{
		long noOfSoldFlats = 0, noOfUnsoldFlats = 0;
		Double totalCostOfSoldFlats = 0.0, totalCostOfUnsoldFlats = 0.0, totalSoldArea = 0.0, totalUnsoldArea = 0.0; 

		Query query = new Query();
		List<Flat> FlatList = new ArrayList<Flat>();
		if(!projectId.equals("Default") && !buildingId.equals("Default") && !wingId.equals("Default") && !floorId.equals("Default"))
		{
			query = new Query();
			FlatList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("floorId").is(floorId)),Flat.class);
		}

		if(!projectId.equals("Default") && !buildingId.equals("Default") && !wingId.equals("Default") && floorId.equals("Default"))
		{
			query = new Query();
			FlatList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId)),Flat.class);
		}

		if(!projectId.equals("Default") && !buildingId.equals("Default") && wingId.equals("Default") && floorId.equals("Default"))
		{
			query = new Query();
			FlatList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId)),Flat.class);
		}

		if(!projectId.equals("Default") && buildingId.equals("Default") && wingId.equals("Default") && floorId.equals("Default"))
		{
			query = new Query();
			FlatList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)),Flat.class);
		}

		if(projectId.equals("Default") && buildingId.equals("Default") && wingId.equals("Default") && floorId.equals("Default"))
		{
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/M/yyyy");
			LocalDate localDate = LocalDate.now();
			String todayDate=dtf.format(localDate);

			query = new Query();
			FlatList = mongoTemplate.find(query.addCriteria(Criteria.where("updateDate").is(todayDate)),Flat.class);
		}

		if(FlatList.size()!=0)
		{
			int index=0;
			JasperPrint print;
			String customerName="";
			try 
			{	
				query = new Query();
				HashMap jmap = new HashMap();
				Collection c = new ArrayList();

				for(int i=0;i<FlatList.size();i++)
				{

					query =new Query();
					List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(FlatList.get(0).getProjectId())), Project.class);

					query =new Query();
					List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(FlatList.get(0).getBuildingId())), ProjectBuilding.class);

					query =new Query();
					List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(FlatList.get(0).getWingId())), ProjectWing.class);

					query =new Query();
					List<Floor> floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("floorId").is(FlatList.get(0).getFloorId())), Floor.class);

					jmap = new HashMap();
					jmap.put("srno",""+(i+1));
					jmap.put("flatType",""+FlatList.get(i).getFlatType());
					jmap.put("flatNo",""+FlatList.get(i).getFlatNumber());

					jmap.put("flatStatus",""+FlatList.get(i).getFlatstatus());
					jmap.put("flat Facing",""+FlatList.get(i).getFlatfacingName());
					jmap.put("flatArea",""+FlatList.get(i).getFlatAreawithLoadingInFt());
					jmap.put("projectName",""+projectDetails.get(0).getProjectName().toUpperCase());
					jmap.put("buildingName",""+buildingDetails.get(0).getBuildingName().toUpperCase());
					jmap.put("wingName",""+wingDetails.get(0).getWingName().toUpperCase());

					if(FlatList.get(i).getFlatstatus().equals("Aggreement Completed") || FlatList.get(i).getFlatstatus().equals("Booking Completed"))
					{
						noOfSoldFlats = noOfSoldFlats + 1;
						totalSoldArea = totalSoldArea + FlatList.get(i).getFlatAreawithLoadingInFt();

						query = new Query();
						List<Booking> bookingList = mongoTemplate.find(query.addCriteria(Criteria.where("flatId").is(FlatList.get(i).getFlatId()).and("projectId").is(FlatList.get(i).getProjectId()).and("buildingId").is(FlatList.get(i).getBuildingId()).and("wingId").is(FlatList.get(i).getWingId()).and("floorId").is(FlatList.get(i).getFloorId())), Booking.class);

						if(bookingList.size()!=0)
						{
							jmap.put("basicCost",""+bookingList.get(0).getFlatbasicCost().longValue());
							totalCostOfSoldFlats = totalCostOfSoldFlats + bookingList.get(0).getFlatbasicCost();
						}

					}
					else
					{

						jmap.put("basicCost",""+FlatList.get(i).getFlatbasicCost().longValue());

						noOfUnsoldFlats = noOfUnsoldFlats + 1;
						totalCostOfUnsoldFlats = totalCostOfUnsoldFlats + FlatList.get(i).getFlatbasicCost();
						totalUnsoldArea = totalUnsoldArea + FlatList.get(i).getFlatAreawithLoadingInFt();
					}

					c.add(jmap);
					jmap = null;
				}
				String realPath =CommanController.GetLogoImagePath();
				//String realPath ="//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img"; //context.getRealPath("/resources/dist/img");

				JRDataSource dataSource = new JRMapCollectionDataSource(c);
				Map<String, Object> parameterMap = new HashMap<String, Object>();

				Date date = new Date();  
				SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
				String strDate= formatter.format(date);

				query =new Query();
				List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)), Project.class);

				query =new Query();
				List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(buildingId)), ProjectBuilding.class);

				query =new Query();
				List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(wingId)), ProjectWing.class);

				query =new Query();
				List<Floor> floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("floorId").is(floorId)), Floor.class);

				//Booking Receipt Generation Code
				/*1*/ parameterMap.put("reportDate", ""+strDate);
				/*2*/ parameterMap.put("reportName", "Flat Booking Status Report");
				if(projectId.equals("Default"))
				{
					/*3*/ parameterMap.put("projectName", "All");   	   
				}
				else
				{
					/*3*/ parameterMap.put("projectName", ""+projectDetails.get(0).getProjectName());   	   
				}

				if(buildingId.equals("Default"))
				{
					/*4*/ parameterMap.put("buildingName", "All");   	   
				}
				else
				{
					/*4*/ parameterMap.put("buildingName", ""+buildingDetails.get(0).getBuildingName());   
				}

				if(wingId.equals("Default"))
				{
					/*5*/ parameterMap.put("wingName", "All"); 	   
				}
				else
				{
					/*5*/ parameterMap.put("wingName", ""+wingDetails.get(0).getWingName());  
				}

				if(floorId.equals("Default"))
				{
					/*6*/ parameterMap.put("floorName", "All"); 	   
				}
				else
				{
					/*6*/ parameterMap.put("floorName", ""+floorDetails.get(0).getFloortypeName());  
				}

				/*7*/ parameterMap.put("realPath", realPath);

				/*8*/ parameterMap.put("noOfSoldFlats", ""+noOfSoldFlats);

				/*9*/ parameterMap.put("noOfUnsoldFlats", ""+noOfUnsoldFlats);

				/*10*/ parameterMap.put("totalFlats", ""+(noOfSoldFlats+noOfUnsoldFlats));

				/*11*/ parameterMap.put("totalCostOfSoldFlats", ""+totalCostOfSoldFlats.longValue());

				/*12*/ parameterMap.put("totalCostOfUnsoldFlats", ""+totalCostOfUnsoldFlats.longValue());

				/*13*/ parameterMap.put("totalCost", ""+(totalCostOfSoldFlats.longValue() + totalCostOfUnsoldFlats.longValue()));

				/*14*/ parameterMap.put("totalSoldArea", ""+totalSoldArea.longValue()+" Sq.Ft.");

				/*15*/ parameterMap.put("totalUnsoldArea", ""+totalUnsoldArea.longValue()+" Sq.Ft.");

				/*16*/ parameterMap.put("totalSaleableArea", ""+(totalSoldArea.longValue()+totalUnsoldArea.longValue())+" Sq.Ft.");

				InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/FlatBookingStatusReport.jasper");

				print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				JasperExportManager.exportReportToPdfStream(print, baos);

				byte[] contents = baos.toByteArray();

				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.parseMediaType("application/pdf"));
				String filename = "FlatBookingStatusReport.pdf";

				JasperExportManager.exportReportToPdfStream(print, baos);

				headers.setContentType(MediaType.parseMediaType("application/pdf"));
				headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
				ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
				response.setHeader("Content-Disposition", "inline; filename=" + filename );
				return resp;	
			}
			catch(Exception ex)
			{
				return null;
			}
		}
		else
		{
			int index=0;
			JasperPrint print;
			String customerName="";

			try 
			{	
				query = new Query();
				HashMap jmap = new HashMap();
				Collection c = new ArrayList();

				//List<Flat> FlatList = mongoTemplate.find(query.addCriteria(Criteria.where("projectName").is(projectName).and("buildingName").is(buildingName).and("wingName").is(wingName).and("floortypeName").is(floortypeName)),Flat.class);


				jmap = new HashMap();
				jmap.put("srno","1");
				jmap.put("flatType","N.A.");
				jmap.put("flatNo","N.A.");
				jmap.put("basicCost","N.A.");
				jmap.put("flatStatus","N.A.");
				jmap.put("flat Facing","N.A.");
				jmap.put("flatArea","N.A.");
				jmap.put("projectName","N.A.");
				jmap.put("buildingName","N.A.");
				jmap.put("wingName","N.A.");

				c.add(jmap);
				jmap = null;

				//String realPath ="//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img"; //context.getRealPath("/resources/dist/img");
				String realPath =CommanController.GetLogoImagePath();
				JRDataSource dataSource = new JRMapCollectionDataSource(c);
				Map<String, Object> parameterMap = new HashMap<String, Object>();

				Date date = new Date();  
				SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
				String strDate= formatter.format(date);

				//Booking Receipt Generation Code
				/*1*/  parameterMap.put("reportDate", ""+strDate);
				/*2*/  parameterMap.put("reportName", "Flat Booking Status Report");
				/*3*/  parameterMap.put("projectName", "N.A.");
				/*4*/  parameterMap.put("buildingName", "N.A.");
				/*5*/  parameterMap.put("wingName", "N.A.");
				/*6*/  parameterMap.put("floorName", "N.A.");
				/*7*/  parameterMap.put("realPath", realPath);
				/*8*/  parameterMap.put("noOfSoldFlats", "N.A.");
				/*9*/  parameterMap.put("noOfUnsoldFlats", "N.A.");
				/*10*/ parameterMap.put("totalFlats", "N.A.");
				/*11*/ parameterMap.put("totalCostOfSoldFlats", "N.A.");
				/*12*/ parameterMap.put("totalCostOfUnsoldFlats", "N.A.");
				/*13*/ parameterMap.put("totalCost", "N.A.");
				/*14*/ parameterMap.put("totalSoldArea", "N.A.");
				/*15*/ parameterMap.put("totalUnsoldArea", "N.A.");
				/*16*/ parameterMap.put("totalSaleableArea", "N.A.");

				InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/FlatBookingStatusReport.jasper");

				print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				JasperExportManager.exportReportToPdfStream(print, baos);

				byte[] contents = baos.toByteArray();

				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.parseMediaType("application/pdf"));
				String filename = "WingWiseFlatBookingStatusReport.pdf";

				JasperExportManager.exportReportToPdfStream(print, baos);

				headers.setContentType(MediaType.parseMediaType("application/pdf"));
				headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
				ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
				response.setHeader("Content-Disposition", "inline; filename=" + filename );
				return resp;	
			}
			catch(Exception ex)
			{
				return null;
			}
		}
	}

	@ResponseBody
	@RequestMapping("/SearchFlatStatus")
	public List<Flat> earchFlatStatus(@RequestParam("projectId") String projectId,@RequestParam("buildingId") String buildingId,@RequestParam("wingId") String wingId,@RequestParam("floorId") String floorId, ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		Query query = new Query();
		List<Flat> FlatBookingStatusList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("floorId").is(floorId)), Flat.class);

		return FlatBookingStatusList;
	}


	//code to print flat report
	@ResponseBody
	@RequestMapping("/FlatWiseStatusReport")
	public ResponseEntity<byte[]> FlatWiseBookingStatusReport(HttpServletRequest req, ModelMap model, HttpServletResponse response)
	{
		String projectId   = req.getParameter("projectId");
		String buildingId  = req.getParameter("buildingId");
		String wingId      = req.getParameter("wingId");
		String floorId = req.getParameter("floorId");


		if(!projectId.equals("Default") && !buildingId.equals("Default") && !wingId.equals("Default") && !floorId.equals("Default"))
		{
			int index=0;
			JasperPrint print;
			String customerName="";
			try 
			{	
				Query query = new Query();
				HashMap jmap = new HashMap();
				Collection c = new ArrayList();

				query =new Query();
				List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)), Project.class);

				query =new Query();
				List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(buildingId)), ProjectBuilding.class);

				query =new Query();
				List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(wingId)), ProjectWing.class);

				query =new Query();
				List<Floor> floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("floorId").is(floorId)), Floor.class);

				List<Flat> FlatList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("floorId").is(floorId)),Flat.class);

				for(int i=0;i<FlatList.size();i++)
				{
					jmap = new HashMap();
					jmap.put("srno",""+(i+1));
					jmap.put("flatType",""+FlatList.get(i).getFlatType());
					jmap.put("flatNo",""+FlatList.get(i).getFlatNumber());
					jmap.put("basicCost",""+FlatList.get(i).getFlatbasicCost());
					jmap.put("flatStatus",""+FlatList.get(i).getFlatstatus());
					jmap.put("flat Facing",""+FlatList.get(i).getFlatfacingName());
					jmap.put("flatArea",""+FlatList.get(i).getFlatArea());
					jmap.put("projectName",""+projectDetails.get(0).getProjectName());
					jmap.put("buildingName",""+buildingDetails.get(0).getBuildingName());
					jmap.put("wingName",""+wingDetails.get(0).getWingName());

					c.add(jmap);
					jmap = null;
				}

				//String realPath ="//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img"; //context.getRealPath("/resources/dist/img");
				String realPath =CommanController.GetLogoImagePath();
				
				JRDataSource dataSource = new JRMapCollectionDataSource(c);
				Map<String, Object> parameterMap = new HashMap<String, Object>();

				Date date = new Date();  
				SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
				String strDate= formatter.format(date);

				//Booking Receipt Generation Code
				/*1*/ parameterMap.put("reportDate", ""+strDate);
				/*2*/ parameterMap.put("reportName", "Flat Booking Status Report");
				/*3*/ parameterMap.put("projectName", ""+projectDetails.get(0).getProjectName());
				/*4*/ parameterMap.put("buildingName", ""+buildingDetails.get(0).getBuildingName());
				/*5*/ parameterMap.put("wingName", ""+wingDetails.get(0).getWingName());
				/*6*/ parameterMap.put("floorName", ""+floorDetails.get(0).getFloortypeName());
				/*7*/ parameterMap.put("realPath", realPath);

				InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/FlatBookingStatusReport.jasper");

				print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				JasperExportManager.exportReportToPdfStream(print, baos);

				byte[] contents = baos.toByteArray();

				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.parseMediaType("application/pdf"));
				String filename = "WingWiseFlatBookingStatusReport.pdf";

				JasperExportManager.exportReportToPdfStream(print, baos);

				headers.setContentType(MediaType.parseMediaType("application/pdf"));
				headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
				ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
				response.setHeader("Content-Disposition", "inline; filename=" + filename );
				return resp;	
			}
			catch(Exception ex)
			{
				return null;
			}
		}
		else if(projectId.equals("Default") && buildingId.equals("Default") && wingId.equals("Default")  && floorId.equals("Default"))
		{
			int index=0;
			JasperPrint print;
			String customerName="";
			try 
			{	
				Query query = new Query();
				Query query1 = new Query();
				HashMap jmap = new HashMap();
				Collection c = new ArrayList();
				/*jmap.put("srno", "1"); */


				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/M/yyyy");
				LocalDate localDate = LocalDate.now();
				String todayDate=dtf.format(localDate);

				List<Flat> FlatList = mongoTemplate.find(query.addCriteria(Criteria.where("creationDate").is(todayDate)),Flat.class);

				for(int i=0;i<FlatList.size();i++)
				{

					query =new Query();
					List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(FlatList.get(i).getProjectId())), Project.class);

					query =new Query();
					List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(FlatList.get(0).getBuildingId())), ProjectBuilding.class);

					query =new Query();
					List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(FlatList.get(i).getWingId())), ProjectWing.class);

					query =new Query();
					List<Floor> floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("floorId").is(FlatList.get(i).getFlatId())), Floor.class);

					jmap = new HashMap();
					jmap.put("srno",""+(i+1));
					jmap.put("flatType",""+FlatList.get(i).getFlatType());
					jmap.put("flatNo",""+FlatList.get(i).getFlatNumber());
					jmap.put("basicCost",""+FlatList.get(i).getFlatbasicCost());
					jmap.put("flatStatus",""+FlatList.get(i).getFlatstatus());
					jmap.put("flat Facing",""+FlatList.get(i).getFlatfacingName());
					jmap.put("flatArea",""+FlatList.get(i).getFlatArea());
					jmap.put("projectName",""+projectDetails.get(0).getProjectName());
					jmap.put("buildingName",""+buildingDetails.get(0).getBuildingName());
					jmap.put("wingName",""+wingDetails.get(0).getWingName());

					c.add(jmap);
					jmap = null;
				}

				//String realPath ="//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img"; //context.getRealPath("/resources/dist/img");
				String realPath =CommanController.GetLogoImagePath();
				
				JRDataSource dataSource = new JRMapCollectionDataSource(c);
				Map<String, Object> parameterMap = new HashMap<String, Object>();

				Date date = new Date();  
				SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
				String strDate= formatter.format(date);

				//Booking Receipt Generation Code
				/*1*/ parameterMap.put("reportDate", ""+strDate);
				/*2*/ parameterMap.put("reportName", "Todays Flat Booking Report");
				/*3*/ parameterMap.put("projectName", "All");
				/*4*/ parameterMap.put("buildingName", "All");
				/*5*/ parameterMap.put("wingName", "All");
				/*6*/ parameterMap.put("floorName", "All");
				/*7*/ parameterMap.put("realPath", realPath);

				InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/FlatBookingStatusReport.jasper");

				print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				JasperExportManager.exportReportToPdfStream(print, baos);

				byte[] contents = baos.toByteArray();

				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.parseMediaType("application/pdf"));
				String filename = "TodaysFlatBookingReport.pdf";

				JasperExportManager.exportReportToPdfStream(print, baos);

				headers.setContentType(MediaType.parseMediaType("application/pdf"));
				headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
				ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
				response.setHeader("Content-Disposition", "inline; filename=" + filename );
				return resp;	
			}
			catch(Exception e)
			{
				return null;
			}
		}
		else
		{
			return null;
		}
	}


	@ResponseBody
	@RequestMapping("/SearchFlatEnquiryReport")
	public List<Enquiry> SearchFlatEnquiryReport(@RequestParam("enquirysourceId") String enquirysourceId, @RequestParam("subsourceId") String subsourceId, ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/M/yyyy");
		LocalDate localDate = LocalDate.now();
		String todayDate=dtf.format(localDate);

		Query query = new Query();
		List<Enquiry> todaysEnquiryList = mongoTemplate.find(query.addCriteria(Criteria.where("enquirysourceId").is(enquirysourceId).and("subsourceId").is(subsourceId)), Enquiry.class);

		return todaysEnquiryList;
	}


	@ResponseBody
	@RequestMapping("/getCatagoryWiseFlatBookingList")
	public List<Booking> getCatagoryWiseFlatBookingList(@RequestParam("projectId") String projectId,@RequestParam("buildingId") String buildingId, @RequestParam("wingId") String wingId, @RequestParam("floorId") String floorId, ModelMap model)
	{
		Query query = new Query();

		List<Booking> bookingList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("floorId").is(floorId)), Booking.class);

		//List<CustomerReceiptForm> paymentDetails = customerReceiptFormRepository.findAll();

	    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy"); 
		for(int i=0;i<bookingList.size();i++)
		{
			//Formating for booking amount in double
			Double d = new Double(0.00d);
			double dd = d.doubleValue();
			DecimalFormat formatter1 = new DecimalFormat("##0.0##");

			//Formating for booking amount in double
			d = new Double(bookingList.get(i).getGrandTotal1());
			dd = d.doubleValue();
			formatter1 = new DecimalFormat("##0.0##");

			String totalCost = formatter1.format(dd);
			bookingList.get(i).setGrandTotal1(Double.parseDouble(totalCost));

			bookingList.get(i).setStringDate(formatter.format(bookingList.get(i).getCreationDate()));
			/*if(bookingList.get(i).getBookingId().equals(paymentDetails.get(i).getBookingId()))
			   {
				  //bookingList.get(i).setInfrastructureCharge((extraChargesList.get(i).getMaintenanceCost()+extraChargesList.get(i).getParkingCharges()+extraChargesList.get(i).getOtherCharges()));
				  bookingList.get(i).setInfrastructureCharge(0.00);
			   }
			   else
			   {
				  bookingList.get(i).setInfrastructureCharge(0.00);
			   }*/
		}

		for(int i=0;i<bookingList.size();i++)
		{

			query = new Query();
			List<Flat> flatDetails = mongoTemplate.find(query.addCriteria(Criteria.where("flatId").is(bookingList.get(i).getFlatId())), Flat.class);
			bookingList.get(i).setFlatId(flatDetails.get(0).getFlatNumber());
		}


		return bookingList;
	}


	@ResponseBody
	@RequestMapping("/getDateWiseFlatBookingList")
	public List<Booking> getDateWiseFlatBookingList(@RequestParam("fromDate") String fromDate,@RequestParam("toDate") String toDate,ModelMap model)
	{
		Query query = new Query();

		List<Booking> bookingListAll = bookingRepository.findAll();
		List<Booking> bookingList = null;

		return bookingListAll;
	}


	@ResponseBody
	@RequestMapping(value="/CustomerFlatCheckListPrint")
	public ResponseEntity<byte[]> CustomerFlatCheckListPrint(@RequestParam("bookingId") String bookingId, HttpServletRequest req, ModelMap model, HttpServletResponse response)
	{
		int index=0;
		JasperPrint print;
		String customerName="", CheckListDetailsPara= "", CheckListDetailsPara1 = "";

		try 
		{	
			Query query;
			HashMap jmap = new HashMap();
			Collection c = new ArrayList();
			/*jmap.put("srno", "1");
					    c.add(jmap);*/

			query = new Query();
			List<CustomerFlatCheckList> checkListDetails = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)),CustomerFlatCheckList.class);

			query = new Query();
			List<AddMoreCheckListDetials> moreCheckListDetails = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)), AddMoreCheckListDetials.class);

			query = new Query();
			List<Aggreement> agreementDetails = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)), Aggreement.class); 

			query = new Query();
			List<Booking> bookingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)), Booking.class);

			query = new Query();
			List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(bookingDetails.get(0).getProjectId())), Project.class);

			query =new Query();
			List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(bookingDetails.get(0).getBuildingId())), ProjectBuilding.class);

			query =new Query();
			List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(bookingDetails.get(0).getWingId())), ProjectWing.class);

			query =new Query();
			List<Floor> floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("floorId").is(bookingDetails.get(0).getFlatId())), Floor.class);

			query = new Query();
			List<Flat> flatDetails = mongoTemplate.find(query.addCriteria(Criteria.where("flatId").is(bookingDetails.get(0).getFlatId())), Flat.class);

			Query query1 = new Query();

			if(moreCheckListDetails.size()!=0)
			{
				for(int i=0;i<moreCheckListDetails.size();i++)
				{
					query1  = new Query();
					jmap = new HashMap();
					jmap.put("srno",""+(i+1));
					jmap.put("numOfKeys",""+moreCheckListDetails.get(i).getNoOfkeySet());
					jmap.put("type",""+moreCheckListDetails.get(i).getDoorType());
					jmap.put("details",""+moreCheckListDetails.get(i).getKeyNo());

					c.add(jmap);
					jmap = null;
				}
			}
			else
			{
				jmap.put("srno", "N.A.");
				jmap.put("type","N.A.");
				jmap.put("details","N.A.");

				c.add(jmap);
				jmap = null;
			}

			//String realPath ="//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img"; //context.getRealPath("/resources/dist/img");

			CheckListDetailsPara = CheckListDetailsPara + "CHECK LIST TO BE SUBMITTED ALONG WITH KEYS OF FLAT NO. "+flatDetails.get(0).getFlatNumber().toUpperCase()+", WING "+wingDetails.get(0).getWingName().toUpperCase()+", BUILDING "+buildingDetails.get(0).getBuildingName().toUpperCase()+", IN "+projectDetails.get(0).getProjectName().toUpperCase();

			try
			{
				CheckListDetailsPara = CheckListDetailsPara + ". PROPERTY TAX NO. "+checkListDetails.get(0).getPropertyTaxNo().toUpperCase();
			}
			catch(Exception e)
			{
				System.out.println("Exception "+e.toString());
			}

			try
			{
				CheckListDetailsPara = CheckListDetailsPara +", ELECTRIC METER NO. "+checkListDetails.get(0).getElectricBillMeterNo().toUpperCase()+".";
			}
			catch(Exception e)
			{
				System.out.println(e.toString()); 
			}

			CheckListDetailsPara1 = CheckListDetailsPara1 +"FLAT NO. "+flatDetails.get(0).getFlatNumber().toUpperCase()+", IN "+wingDetails.get(0).getWingName().toUpperCase()+", "+buildingDetails.get(0).getBuildingName().toUpperCase()+", AT "+projectDetails.get(0).getProjectName()+"  IS PERSONALLY CHECKED BY ME, PERTAINING OF ALL THE POINTS LISTED IN THE CHECKLIST & THAT WORK IS CARRIED OUT FULL SATISFACTION & IS COMPLETE IN ALL RESPECT & NOTHING RESPECT & NOTHING TO REPORT.";


			JRDataSource dataSource = new JRMapCollectionDataSource(c);
			Map<String, Object> parameterMap = new HashMap<String, Object>();

			Date date = new Date();  
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
			String strDate= formatter.format(date);

			//CheckList Data Binding to Jasper
			parameterMap.put("date", ""+strDate);

			parameterMap.put("siteName", ""+projectDetails.get(0).getProjectName()+","+projectDetails.get(0).getProjectAddress());

			parameterMap.put("CheckListDetailsPara", ""+CheckListDetailsPara);

			if(checkListDetails.get(0).getCeilings()!=null)
			{
				parameterMap.put("ceilingsRemark", "Completed");
			}
			else
			{
				parameterMap.put("ceilingsRemark", "N.A.");
			}

			if(checkListDetails.get(0).getTiles() !=null)
			{
				parameterMap.put("tilesRemark", "Completed");
			}
			else
			{
				parameterMap.put("tilesRemark", "N.A.");
			}

			if(checkListDetails.get(0).getDoor() !=null)
			{
				parameterMap.put("doorRemark", "Completed");
			}
			else
			{
				parameterMap.put("doorRemark", "N.A.");
			}

			if(checkListDetails.get(0).getDoor() !=null)
			{
				parameterMap.put("doorRemark", "Completed");
			}
			else
			{
				parameterMap.put("doorRemark", "N.A.");
			}

			if(checkListDetails.get(0).getWindows() !=null)
			{
				parameterMap.put("windowsRemark", "Completed");
			}
			else
			{
				parameterMap.put("windowsRemark", "N.A.");
			}

			if(checkListDetails.get(0).getPlumbingAndsanitory() !=null)
			{
				parameterMap.put("plumblingandsaintoryRemark", "Completed");
			}
			else
			{
				parameterMap.put("plumblingandsaintoryRemark", "N.A.");
			}

			if(checkListDetails.get(0).getElectrification() !=null)
			{
				parameterMap.put("electrificationRemark", "Completed");
			}
			else
			{
				parameterMap.put("electrificationRemark", "N.A.");
			}

			if(checkListDetails.get(0).getPainting() != null)
			{
				parameterMap.put("paintingRemark", "Completed");
			}
			else
			{
				parameterMap.put("paintingRemark", "N.A.");
			}

			parameterMap.put("CheckListDetailsPara1", ""+CheckListDetailsPara1);


			parameterMap.put("customerName",""+agreementDetails.get(0).getFirstApplicantfirstname()+" "+agreementDetails.get(0).getFirstApplicantlastname());

			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/CustomerFlatCheckList.jasper");

			print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(print, baos);

			byte[] contents = baos.toByteArray();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			String filename = "CustomerFlatCheckList.pdf";

			JasperExportManager.exportReportToPdfStream(print, baos);

			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
			ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
			response.setHeader("Content-Disposition", "inline; filename=" + filename );
			return resp;	
		}
		catch(Exception ex)
		{
			return null;
		}
	}


	@RequestMapping("/CustomerPaymentSummary")
	public String CustomerPaymentSummary(Model model)
	{
		try {
			List<Booking> bookingList = bookingRepository.findAll();
			List<Country> countryList = countryRepository.findAll();

			Query query = new Query();
			for(int i=0;i<bookingList.size();i++)
			{
				try
				{
					query = new Query();
					List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(bookingList.get(i).getProjectId())), Project.class);

					query =new Query();
					List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(bookingList.get(i).getBuildingId())), ProjectBuilding.class);

					query =new Query();
					List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(bookingList.get(i).getWingId())), ProjectWing.class);

					query = new Query();
					List<Flat> flatDetails = mongoTemplate.find(query.addCriteria(Criteria.where("flatId").is(bookingList.get(i).getFlatId())), Flat.class);

					bookingList.get(i).setProjectId(projectDetails.get(0).getProjectName());
					bookingList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
					bookingList.get(i).setWingId(wingDetails.get(0).getWingName());
					bookingList.get(i).setFlatId(flatDetails.get(0).getFlatNumber());


				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}

			model.addAttribute("bookingList", bookingList);
			model.addAttribute("countryList", countryList);
			return "CustomerPaymentSummary";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/PrintCustomerSummaryDetails")
	public ResponseEntity<byte[]> PrintCustomerSummaryDetails(@RequestParam("bookingId") String bookingId, HttpServletRequest request, HttpServletResponse response)
	{
		int index=0;
		JasperPrint print;
		String customerName="";

		long netTotalAmount=0, demandAmountTotal=0, paidAmountTotal=0, remainingAmountTotal=0;

		try 
		{	
			Query query = new Query();
			HashMap jmap = new HashMap();
			Collection c = new ArrayList();

			List<Booking> bookingList = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)),Booking.class);

			query = new Query();
			List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(bookingList.get(0).getProjectId())), Project.class);

			query =new Query();
			List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(bookingList.get(0).getBuildingId())), ProjectBuilding.class);

			query =new Query();
			List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(bookingList.get(0).getWingId())), ProjectWing.class);

			query = new Query();
			List<Flat> flatDetails = mongoTemplate.find(query.addCriteria(Criteria.where("flatId").is(bookingList.get(0).getFlatId())), Flat.class);

			query = new Query();
			List<CustomerPaymentDetails> paymentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)), CustomerPaymentDetails.class);

			query = new Query();
			//List<CustomerReceiptForm> customerPaymentHistory = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId).and("status").is("Deposited")), CustomerReceiptForm.class);
			List<CustomerReceiptForm> customerPaymentHistory = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)), CustomerReceiptForm.class);

			query = new Query();
			List<Aggreement> aggreementList = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)), Aggreement.class);

			query = new Query();
			List<ExtraCharges> extraChargesDetails = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)), ExtraCharges.class);


			//project Address
			query =new Query();
			State projectStateDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("stateId").is(projectDetails.get(0).getStateId())), State.class);
			query =new Query();
			City projectCityDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("cityId").is(projectDetails.get(0).getCityId())), City.class);
			query =new Query();
			LocationArea projectLocationareaDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("locationareaId").is(projectDetails.get(0).getLocationareaId())), LocationArea.class);

			if(paymentDetails.size()!=0 && customerPaymentHistory.size()!=0)
			{

				for(int i=0;i<customerPaymentHistory.size();i++)
				{
					jmap = new HashMap();

					try
					{
						if(!customerPaymentHistory.get(i).getBankName().equals(""))
						{
							/*1*/ jmap.put("bankName",""+customerPaymentHistory.get(i).getBankName());
						}
						else
						{
							/*1*/ jmap.put("bankName","CASH");   	   
						}
					}
					catch (Exception e) {								}

					/*	 if(customerPaymentHistory.get(i).getBankBranchName()!=null)
				    			 {
				    		     2 jmap.put("branchName",""+customerPaymentHistory.get(i).getBankBranchName());	 
				    			 }
				    			 else
				    			 {
				    			 2 jmap.put("branchName","N.A.");	 
				    			 }*/
					SimpleDateFormat formatter = new SimpleDateFormat("d/M/yyyy");
					try
					{
						/*3*/ jmap.put("cheqNo",""+customerPaymentHistory.get(i).getChequeNumber());
						/*4*/ jmap.put("chqDate",""+formatter.format(customerPaymentHistory.get(0).getCreationDate()));
						/*5*/ jmap.put("chqAmt",""+customerPaymentHistory.get(i).getPaymentAmount().longValue());
					}
					catch (Exception e) {
						// TODO: handle exception
					}
					try
					{
						if(customerPaymentHistory.get(i).getPaymentType().equals("Aggreement"))
						{
							/*6*/ jmap.put("aggAmt",""+customerPaymentHistory.get(i).getPaymentAmount().longValue()); 
						}
						else
						{
							/*6*/ jmap.put("aggAmt","0");	 
						}

						if(customerPaymentHistory.get(i).getPaymentType().equals("Stamp Duty"))
						{
							/*7*/ jmap.put("stampDuty",""+customerPaymentHistory.get(i).getPaymentAmount().longValue());
						}
						else
						{
							/*7*/ jmap.put("stampDuty","0");
						}

						if(customerPaymentHistory.get(i).getPaymentType().equals("Registration"))
						{
							/*8*/ jmap.put("registAmt",""+customerPaymentHistory.get(i).getPaymentAmount().longValue());
						}
						else
						{
							/*8*/ jmap.put("registAmt","0");
						}

						if(customerPaymentHistory.get(i).getPaymentType().equals("GST Amount"))
						{
							/*9*/ jmap.put("gstAmount",""+customerPaymentHistory.get(i).getPaymentAmount().longValue());
						}
						else
						{
							/*9*/ jmap.put("gstAmount","0");	 
						}

						if(customerPaymentHistory.get(i).getPaymentType().equals("Maintainance"))
						{
							/*10*/jmap.put("maintainanceAmt",""+customerPaymentHistory.get(i).getPaymentAmount().longValue());
						}
						else
						{
							/*10*/jmap.put("maintainanceAmt","0");
						}
					}
					catch (Exception e) {
						// TODO: handle exception
					}
					/*11*/jmap.put("receiptNo",""+customerPaymentHistory.get(i).getReceiptId());

					c.add(jmap);
					jmap = null;
				}

				long maintainanceTotalAmount=0;
				for(int i=0;i<customerPaymentHistory.size();i++)
				{
					if(customerPaymentHistory.get(i).getPaymentType().equals("Maintainance"))
					{
						maintainanceTotalAmount = maintainanceTotalAmount + customerPaymentHistory.get(i).getPaymentAmount().longValue();
					}
					else
					{
						maintainanceTotalAmount= maintainanceTotalAmount + 0;
					}
				}

				//String realPath ="//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img";
				String realPath =CommanController.GetLogoImagePath();
				JRDataSource dataSource = new JRMapCollectionDataSource(c);
				Map<String, Object> parameterMap = new HashMap<String, Object>();

				Date date = new Date();  
				SimpleDateFormat formatter = new SimpleDateFormat("d/M/yyyy");  
				String strDate= formatter.format(date);

				//Booking Receipt Generation Code
				/*1*/ parameterMap.put("projectName", ""+projectDetails.get(0).getProjectName());
				/*2*/ parameterMap.put("buildingName", ""+buildingDetails.get(0).getBuildingName());
				/*3*/ parameterMap.put("wingName", ""+wingDetails.get(0).getWingName());
				/*4*/ parameterMap.put("flatNumber", ""+flatDetails.get(0).getFlatNumber());

				if(aggreementList.size()!=0)
				{
					/*5*/ parameterMap.put("customerName", "Name : Mr/Mrs. "+aggreementList.get(0).getFirstApplicantfirstname().toUpperCase()+" "+aggreementList.get(0).getFirstApplicantmiddlename().toUpperCase()+" "+aggreementList.get(0).getFirstApplicantlastname().toUpperCase()); 	 
				}
				else
				{
					/*5*/ parameterMap.put("customerName", "Name : Mr/Mrs. "+bookingList.get(0).getBookingfirstname().toUpperCase()+" "+bookingList.get(0).getBookingmiddlename().toUpperCase()+" "+bookingList.get(0).getBookinglastname().toUpperCase());
				}

				/*6*/  parameterMap.put("p_chequeAmt", "0");

				/*7*/  parameterMap.put("p_aggAmt", ""+bookingList.get(0).getAggreementValue1().longValue());

				/*8*/  parameterMap.put("p_stamp", ""+bookingList.get(0).getStampDuty1().longValue());

				/*9*/  parameterMap.put("p_regist", ""+bookingList.get(0).getRegistrationCost1().longValue());

				/*10*/ parameterMap.put("p_gstAmt", ""+bookingList.get(0).getGstAmount1().longValue());

				if(extraChargesDetails.size()!=0)
				{
					/*11*/ parameterMap.put("p_maint", ""+extraChargesDetails.get(0).getMaintenanceCost().longValue());
					/*22*/ parameterMap.put("p_maintBalanceAmt", ""+(extraChargesDetails.get(0).getMaintenanceCost().longValue() - maintainanceTotalAmount));

				}
				else
				{
					/*11*/ parameterMap.put("p_maint", "0");
					/*22*/ parameterMap.put("p_maintBalanceAmt", "0");

				}

				/*12*/ parameterMap.put("p_receiptNo", "-");

				/*13*/ parameterMap.put("p_stampTotalAmt", ""+paymentDetails.get(0).getTotalpayStampDuty().longValue());

				/*14*/ parameterMap.put("p_registTotalAmt", ""+paymentDetails.get(0).getTotalpayRegistration().longValue());

				/*15*/ parameterMap.put("p_gstTotalAmt", ""+paymentDetails.get(0).getTotalpaygstAmount().longValue());

				/*16*/ parameterMap.put("p_maintTotalAmt", ""+maintainanceTotalAmount);

				/*17*/ parameterMap.put("p_aggTotalAmt", ""+paymentDetails.get(0).getTotalpayAggreement().longValue());

				/*18*/ parameterMap.put("p_aggBalanceTotalAmt", ""+(bookingList.get(0).getAggreementValue1().longValue() - paymentDetails.get(0).getTotalpayAggreement().longValue()));

				/*19*/ parameterMap.put("p_stampBalanceTotal", ""+(bookingList.get(0).getStampDuty1().longValue() - paymentDetails.get(0).getTotalpayStampDuty().longValue()));

				/*20*/ parameterMap.put("p_registBalanceTotal", ""+(bookingList.get(0).getRegistrationCost1().longValue() - paymentDetails.get(0).getTotalpayRegistration().longValue()));

				/*21*/ parameterMap.put("p_gstBalanceAmt", ""+(bookingList.get(0).getGstAmount1().longValue() - paymentDetails.get(0).getTotalpaygstAmount().longValue()));


				/*23*/ parameterMap.put("realPath", ""+realPath);

				/*24*/ parameterMap.put("bookingId", "Booking Id:"+bookingList.get(0).getBookingId());

				/*25*/ parameterMap.put("customerAddress", "Address: "+bookingList.get(0).getBookingaddress()+","+bookingList.get(0).getBookingPincode());

				InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/CustomerPaymentSummary.jasper");

				print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				JasperExportManager.exportReportToPdfStream(print, baos);

				byte[] contents = baos.toByteArray();

				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.parseMediaType("application/pdf"));
				String filename = "CustomerPaymentSummary.pdf";

				JasperExportManager.exportReportToPdfStream(print, baos);

				headers.setContentType(MediaType.parseMediaType("application/pdf"));
				headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
				ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
				response.setHeader("Content-Disposition", "inline; filename=" + filename );
				return resp;	  
			}
			else
			{
				/*1*/ jmap.put("bankName","N.A.");   	   

				/*2*/ jmap.put("branchName","N.A.");	 

				/*3*/ jmap.put("cheqNo","N.A.");

				/*4*/ jmap.put("chqDate","N.A.");

				/*5*/ jmap.put("chqAmt","N.A.");

				/*6*/ jmap.put("aggAmt","N.A."); 

				/*7*/ jmap.put("stampDuty","N.A.");

				/*8*/ jmap.put("registAmt","N.A.");

				/*9*/ jmap.put("gstAmount","N.A.");	 

				/*10*/jmap.put("maintainanceAmt","N.A.");

				/*11*/jmap.put("receiptNo","N.A.");

				c.add(jmap);
				jmap = null;
			}

			//String realPath ="//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img";
			String realPath =CommanController.GetLogoImagePath();
			
			JRDataSource dataSource = new JRMapCollectionDataSource(c);
			Map<String, Object> parameterMap = new HashMap<String, Object>();

			Date date = new Date();  
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
			String strDate= formatter.format(date);

			//Booking Receipt Generation Code
			/*1*/ parameterMap.put("projectName", "N.A.");

			/*2*/ parameterMap.put("buildingName", "N.A.");

			/*3*/ parameterMap.put("wingName", "N.A.");

			/*4*/ parameterMap.put("flatNumber", "N.A.");

			/*5*/ parameterMap.put("customerName", "(Booking ID : "+bookingList.get(0).getBookingId()+")  Name : Mr/Mrs. "+bookingList.get(0).getBookingfirstname().toUpperCase());

			/*6*/  parameterMap.put("p_chequeAmt", "N.A.");

			/*7*/  parameterMap.put("p_aggAmt", "N.A.");

			/*8*/  parameterMap.put("p_stamp", "N.A.");

			/*9*/  parameterMap.put("p_regist", "N.A.");

			/*10*/ parameterMap.put("p_gstAmt", "N.A.");

			/*11*/ parameterMap.put("p_maint", "N.A.");

			/*12*/ parameterMap.put("p_receiptNo", "N.A.");

			/*13*/ parameterMap.put("p_stampTotalAmt", "N.A.");

			/*14*/ parameterMap.put("p_registTotalAmt", "N.A.");

			/*15*/ parameterMap.put("p_gstTotalAmt", "N.A.");

			/*16*/ parameterMap.put("p_maintTotalAmt", "N.A.");

			/*17*/ parameterMap.put("p_aggTotalAmt", "N.A.");

			/*18*/ parameterMap.put("p_aggBalanceTotalAmt", "N.A.");

			/*19*/ parameterMap.put("p_stampBalanceTotal", "N.A.");

			/*20*/ parameterMap.put("p_registBalanceTotal", "N.A.");

			/*21*/ parameterMap.put("p_gstBalanceAmt", "N.A.");

			/*22*/ parameterMap.put("p_maintBalanceAmt", "N.A.");

			/*23*/ parameterMap.put("realPath", ""+realPath);

			/*24*/ parameterMap.put("bookingId", "N.A.");

			/*25*/ parameterMap.put("customerAddress", "N.A.");

			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/CustomerPaymentSummary.jasper");

			print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(print, baos);

			byte[] contents = baos.toByteArray();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			String filename = "CustomerPaymentSummary.pdf";

			JasperExportManager.exportReportToPdfStream(print, baos);

			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
			ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
			response.setHeader("Content-Disposition", "inline; filename=" + filename );
			return resp; 
		}
		catch(Exception e)
		{
			return null;
		}
	}

	@RequestMapping("/PaymentReceiptReport")
	public String PaymentReceiptReport(Model model)
	{
		try {
			Date todayDate = new Date();
			final Calendar calendar = Calendar.getInstance();
			calendar.setTime(todayDate);
			calendar.add(Calendar.DAY_OF_YEAR, -1);
			Date date= calendar.getTime();

			Query query = new Query();
			List<CustomerReceiptForm> customerReceiptList = mongoTemplate.find(query.addCriteria(Criteria.where("creationDate").gte(date).lt(todayDate)), CustomerReceiptForm.class);

			query = new Query();
			List<Booking> bookingList = bookingRepository.findAll();

			for(int i=0; i< customerReceiptList.size();i++)
			{
				if(customerReceiptList.get(i).getChequeNumber().equals(""))
				{
					customerReceiptList.get(i).setChequeNumber("CASH");
				}
				for(int j=0;j<bookingList.size();j++)
				{
					if(customerReceiptList.get(i).getBookingId().equals(bookingList.get(j).getBookingId()))
					{
						customerReceiptList.get(i).setCustomerName(bookingList.get(j).getBookingfirstname()+" "+bookingList.get(j).getBookinglastname());
					}
				}
			}

			model.addAttribute("receiptList", customerReceiptList);
			return "PaymentReceiptReport";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/SearchDateWiseReceipts")
	public List<CustomerReceiptForm> SearchDateWiseReceipts(@RequestParam("receiptDate") String receiptDate, HttpServletRequest req, HttpServletResponse res) throws ParseException
	{

		Date currentDate=new SimpleDateFormat("d/M/yyyy").parse(receiptDate);

		final Calendar calendar = Calendar.getInstance();
		calendar.setTime(currentDate);
		calendar.add(Calendar.DAY_OF_YEAR, -1);
		Date date= calendar.getTime();

		Query query = new Query();

		SimpleDateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");

		String creationDate="";
		String todayDate=creationDate= createDateFormat.format(currentDate);
		query = new Query();

		List<CustomerReceiptForm> customerReceiptList1 = customerReceiptFormRepository.findAll();

		List<CustomerReceiptForm> customerReceiptList=new ArrayList<CustomerReceiptForm>();
		CustomerReceiptForm customerreceiptform=new CustomerReceiptForm();

		for(int i=0;i<customerReceiptList1.size();i++)
		{
			creationDate= createDateFormat.format(customerReceiptList1.get(i).getCreationDate());

			if(todayDate.equals(creationDate))
			{
				customerreceiptform=new CustomerReceiptForm();
				customerreceiptform.setReceiptId(customerReceiptList1.get(i).getReceiptId());
				customerreceiptform.setBookingId(customerReceiptList1.get(i).getBookingId());
				customerreceiptform.setPaymentAmount(customerReceiptList1.get(i).getPaymentAmount());
				customerreceiptform.setPaymentMode(customerReceiptList1.get(i).getPaymentType());
				customerreceiptform.setPaymentType(customerReceiptList1.get(i).getPaymentType());
				customerreceiptform.setBankName(customerReceiptList1.get(i).getBankName());
				customerreceiptform.setBranchName(customerReceiptList1.get(i).getBranchName());
				customerreceiptform.setChequeNumber(customerReceiptList1.get(i).getChequeNumber());
				customerreceiptform.setNarration(customerReceiptList1.get(i).getNarration());
				customerreceiptform.setCreationDate(customerReceiptList1.get(i).getCreationDate());
				customerreceiptform.setUserName(customerReceiptList1.get(i).getUserName());
				customerreceiptform.setStatus(customerReceiptList1.get(i).getStatus());

				customerReceiptList.add(customerreceiptform);
			}

		}

		query = new Query();
		List<Booking> bookingList = bookingRepository.findAll();

		for(int i=0; i< customerReceiptList.size();i++)
		{
			if(customerReceiptList.get(i).getChequeNumber().equals(""))
			{
				customerReceiptList.get(i).setChequeNumber("CASH");
			}
			for(int j=0;j<bookingList.size();j++)
			{
				if(customerReceiptList.get(i).getBookingId().equals(bookingList.get(j).getBookingId()))
				{
					customerReceiptList.get(i).setCustomerName(bookingList.get(j).getBookingfirstname()+" "+bookingList.get(j).getBookinglastname());
				}
			}
		}


		return customerReceiptList;
	}

	@ResponseBody
	@RequestMapping(value="/PrintPaymentReceiptReport", method=RequestMethod.POST)
	public ResponseEntity<byte[]> PrintPaymentReceiptReport(HttpServletRequest req, ModelMap model, HttpServletResponse response) throws ParseException
	{
		String receiptDate = req.getParameter("receiptDate");

		SimpleDateFormat formatter1=new SimpleDateFormat("d/M/yyyy");

		/*	DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/M/yyyy");
					LocalDate localDate = LocalDate.now();
					String todayDate=dtf.format(localDate);*/
		Date todayDate=new Date();
		Date receiptDate2;
		if(receiptDate.equals(""))
		{
			receiptDate2 = todayDate;
		}
		else
		{
			receiptDate2=formatter1.parse(receiptDate); 
		}

		SimpleDateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");
		String receiptDate1= createDateFormat.format(receiptDate2);

		Query query = new Query();

		String creationDate="";
		query = new Query();

		//List<CustomerReceiptForm> customerReceiptList1 = mongoTemplate.find(query.addCriteria(Criteria.where("creationDate").is(receiptDate)), CustomerReceiptForm.class);

		//List<CustomerReceiptForm> customerReceiptList = mongoTemplate.find(query.addCriteria(Criteria.where("creationDate").is(receiptDate).and("status").is("Deposited")), CustomerReceiptForm.class);
		// List<CustomerReceiptForm> customerReceiptList = mongoTemplate.find(query.addCriteria(Criteria.where("creationDate").gte(receiptDate).lte(receiptDate)), CustomerReceiptForm.class);

		List<CustomerReceiptForm> customerReceiptList1 = customerReceiptFormRepository.findAll();

		List<CustomerReceiptForm> customerReceiptList=new ArrayList<CustomerReceiptForm>();
		CustomerReceiptForm customerreceiptform=new CustomerReceiptForm();

		for(int i=0;i<customerReceiptList1.size();i++)
		{
			creationDate= createDateFormat.format(customerReceiptList1.get(i).getCreationDate());

			if(receiptDate1.equals(creationDate))
			{
				customerreceiptform=new CustomerReceiptForm();
				customerreceiptform.setReceiptId(customerReceiptList1.get(i).getReceiptId());
				customerreceiptform.setBookingId(customerReceiptList1.get(i).getBookingId());
				customerreceiptform.setPaymentAmount(customerReceiptList1.get(i).getPaymentAmount());
				customerreceiptform.setPaymentMode(customerReceiptList1.get(i).getPaymentType());
				customerreceiptform.setPaymentType(customerReceiptList1.get(i).getPaymentType());
				customerreceiptform.setBankName(customerReceiptList1.get(i).getBankName());
				customerreceiptform.setBranchName(customerReceiptList1.get(i).getBranchName());
				customerreceiptform.setChequeNumber(customerReceiptList1.get(i).getChequeNumber());
				customerreceiptform.setNarration(customerReceiptList1.get(i).getNarration());
				customerreceiptform.setCreationDate(customerReceiptList1.get(i).getCreationDate());
				customerreceiptform.setUserName(customerReceiptList1.get(i).getUserName());
				customerreceiptform.setStatus(customerReceiptList1.get(i).getStatus());

				customerReceiptList.add(customerreceiptform);
			}

		}

		Booking bookingDetails = new Booking();

		if(customerReceiptList.size()!=0)
		{
			int index=0;
			JasperPrint print;
			String customerName="";
			try 
			{	
				query = new Query();
				Query query1;
				HashMap jmap = new HashMap();
				Collection c = new ArrayList();

				double totalAmount = 0.0;
				for(int i=0;i<customerReceiptList.size();i++)
				{
					bookingDetails = new Booking();
					query1  = new Query();
					jmap = new HashMap();

					jmap.put("bookingId",""+customerReceiptList.get(i).getBookingId());
					jmap.put("receiptId",""+customerReceiptList.get(i).getReceiptId());

					bookingDetails = mongoTemplate.findOne(query1.addCriteria(Criteria.where("bookingId").is(customerReceiptList.get(i).getBookingId())), Booking.class);

					jmap.put("customerName",""+bookingDetails.getBookingfirstname().toUpperCase());
					jmap.put("paymentAmount",""+customerReceiptList.get(i).getPaymentAmount().longValue());

					totalAmount = totalAmount + customerReceiptList.get(i).getPaymentAmount();

					if(customerReceiptList.get(i).getChequeNumber().equals(""))
					{
						jmap.put("chequeNo","CASH");
					}
					else
					{
						jmap.put("chequeNo",""+customerReceiptList.get(i).getChequeNumber());
					}

					jmap.put("paymentType",""+customerReceiptList.get(i).getPaymentType());

					c.add(jmap);
					jmap = null;
				}

				//String realPath ="//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img";
				String realPath =CommanController.GetLogoImagePath();
				
				JRDataSource dataSource = new JRMapCollectionDataSource(c);
				Map<String, Object> parameterMap = new HashMap<String, Object>();

				Date date = new Date();  
				SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
				String strDate= formatter.format(date);

				//Booking Receipt Generation Code
				/*1*/ parameterMap.put("date", ""+todayDate);
				/*2*/ parameterMap.put("realPath",realPath);
				/*3*/ parameterMap.put("amountTotal",""+(long)totalAmount);

				InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/PaymentReceiptReport.jasper");

				print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				JasperExportManager.exportReportToPdfStream(print, baos);

				byte[] contents = baos.toByteArray();

				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.parseMediaType("application/pdf"));
				String filename = "PaymentReceiptReport.pdf";

				JasperExportManager.exportReportToPdfStream(print, baos);

				headers.setContentType(MediaType.parseMediaType("application/pdf"));
				headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
				ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
				response.setHeader("Content-Disposition", "inline; filename=" + filename );
				return resp;	
			}
			catch(Exception ex)
			{
				return null;
			}
		}
		else
		{
			int index=0;
			JasperPrint print;
			String customerName="";
			try 
			{	
				query = new Query();
				Query query1;
				HashMap jmap = new HashMap();
				Collection c = new ArrayList();

				query1  = new Query();
				jmap = new HashMap();

				jmap.put("bookingId","N.A.");
				jmap.put("receiptId","N.A.");
				jmap.put("customerName","N.A.");
				jmap.put("paymentAmount","N.A.");
				jmap.put("chequeNo","N.A.");
				jmap.put("paymentType","N.A.");

				c.add(jmap);
				jmap = null;

				//String realPath ="//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img";
				String realPath =CommanController.GetLogoImagePath();
				
				JRDataSource dataSource = new JRMapCollectionDataSource(c);
				Map<String, Object> parameterMap = new HashMap<String, Object>();

				Date date = new Date();  
				SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
				String strDate= formatter.format(date);

				//Booking Receipt Generation Code
				/*1*/ parameterMap.put("date", ""+todayDate);
				/*2*/ parameterMap.put("realPath",realPath);
				/*3*/ parameterMap.put("amountTotal","0");

				InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/PaymentReceiptReport.jasper");

				print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				JasperExportManager.exportReportToPdfStream(print, baos);

				byte[] contents = baos.toByteArray();

				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.parseMediaType("application/pdf"));
				String filename = "PaymentReceiptReport.pdf";

				JasperExportManager.exportReportToPdfStream(print, baos);

				headers.setContentType(MediaType.parseMediaType("application/pdf"));
				headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
				ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
				response.setHeader("Content-Disposition", "inline; filename=" + filename );
				return resp;	
			}
			catch(Exception ex)
			{
				return null;
			}
		}

	}
}
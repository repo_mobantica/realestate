package com.realestate.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Consultancy;
import com.realestate.bean.ConsultancyEmployees;
import com.realestate.bean.Country;
import com.realestate.bean.Department;
import com.realestate.bean.Designation;
import com.realestate.bean.Employee;
import com.realestate.repository.ConsultancyEmployeesRepository;
import com.realestate.repository.ConsultancyRepository;
import com.realestate.repository.CountryRepository;
import com.realestate.repository.DepartmentRepository;
import com.realestate.repository.DesignationRepository;
import com.realestate.repository.ProjectRepository;

@Controller
@RequestMapping("/")
public class ConsultancyController {


	@Autowired
	DesignationRepository designationRepository;
	@Autowired
	ConsultancyEmployeesRepository consultancyemployeesRepository;
	@Autowired
	CountryRepository countryRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	ConsultancyRepository consultancyRepository;	
	@Autowired
	DepartmentRepository departmentRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	private long Generate_ConsultancyEmployeeId()
	{
		long consultancyCode=0;
		List<ConsultancyEmployees> consultancyemployeesList= consultancyemployeesRepository.findAll();
		if(consultancyemployeesList.size()!=0)
		{
			consultancyCode=consultancyemployeesList.get(consultancyemployeesList.size()-1).getConsultancyEmployeeId();
		}
		consultancyCode=consultancyCode+1;

		return consultancyCode;
	}

	String consultancyCode;
	private String ConsultancyCode()
	{
		long cnt  = consultancyRepository.count();
		if(cnt<10)
		{
			consultancyCode = "CS000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			consultancyCode = "CS00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			consultancyCode = "CS0"+(cnt+1);
		}
		else
		{
			consultancyCode = "CS"+(cnt+1);
		}
		return consultancyCode;
	}


	//Code For Retrieving Country Names
	private List<Country> findAllCountryId()
	{
		List<Country> countryList;

		countryList = countryRepository.findAll(new Sort(Sort.Direction.ASC,"countryName"));
		return countryList;
	}

	//code for retriving all designations
	private List<Department> getAllDepartment()
	{
		List<Department> departmentList;

		departmentList = departmentRepository.findAll(new Sort(Sort.Direction.ASC,"departmentName"));
		return departmentList;
	}

	@RequestMapping("/ConsultancyMaster")
	public String ConsultancyMaster(Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<Consultancy> consultancyList = consultancyRepository.findAll(); 

			model.addAttribute("consultancyList",consultancyList);
			return "ConsultancyMaster";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping("/AddConsultancy")
	public String AddConsultancy(Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<Department> departmentList = departmentRepository.findAll();
			List<Designation> designationList = designationRepository.findAll();


			model.addAttribute("designationList", designationList);
			model.addAttribute("departmentList", departmentList);
			model.addAttribute("consultancyCode",ConsultancyCode());
			model.addAttribute("countryList",findAllCountryId());
			return "AddConsultancy";

		}catch (Exception e) {
			return "login";
		}
	}



	@RequestMapping(value = "/AddConsultancy", method = RequestMethod.POST)
	public String Saveconsultancy(@ModelAttribute Consultancy consultancy, Model model)
	{
		try {
			try
			{
				consultancy = new Consultancy(consultancy.getConsultancyId(), consultancy.getConsultancyfirmName(), consultancy.getFirmType(), consultancy.getFirmpanNumber(), consultancy.getFirmgstNumber(), 
						consultancy.getCheckPrintingName(), 
						consultancy.getConsultancyAddress(), consultancy.getCountryId(), consultancy.getStateId(), consultancy.getCityId(),consultancy.getLocationareaId(), consultancy.getAreaPincode(),
						consultancy.getDepartmentId(), consultancy.getDesignationId(), consultancy.getEmployeeId(), 
						consultancy.getConsultancyBankName(), consultancy.getBranchName(), consultancy.getBankifscCode(), consultancy.getConsultancyBankacno(),
						consultancy.getStatus(), consultancy.getCreationDate(), consultancy.getUpdateDate(), consultancy.getUserName());
				consultancyRepository.save(consultancy);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}

			List<Consultancy> consultancyList = consultancyRepository.findAll(); 

			model.addAttribute("consultancyList",consultancyList);

			return "ConsultancyMaster";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping("/EditConsultancy")
	public String EditConsultancy(@RequestParam("consultancyId") String consultancyId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<Consultancy> consultancyDetails = mongoTemplate.find(query.addCriteria(Criteria.where("consultancyId").is(consultancyId)),Consultancy.class);

			query = new Query();
			List<ConsultancyEmployees> consultancyemployeeList = mongoTemplate.find(query.addCriteria(Criteria.where("consultancyId").is(consultancyId)), ConsultancyEmployees.class);
			for(int i=0;i<consultancyemployeeList.size();i++)
			{
				query = new Query();
				List<Designation> DesignationDetails = mongoTemplate.find(query.addCriteria(Criteria.where("designationId").is(consultancyemployeeList.get(i).getDesignationId())), Designation.class);
				consultancyemployeeList.get(i).setDesignationId(DesignationDetails.get(0).getDesignationName());
			}


			List<Department> departmentList = departmentRepository.findAll();
			List<Designation> designationList = designationRepository.findAll();

			model.addAttribute("consultancyemployeeList", consultancyemployeeList);
			model.addAttribute("designationList", designationList);
			model.addAttribute("departmentList", departmentList);
			model.addAttribute("consultancyDetails", consultancyDetails);
			model.addAttribute("countryList",findAllCountryId());

			return "EditConsultancy";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping(value = "/EditConsultancy", method = RequestMethod.POST)
	public String EditConsultancy(@ModelAttribute Consultancy consultancy, Model model)
	{
		try {
			try
			{
				consultancy = new Consultancy(consultancy.getConsultancyId(), consultancy.getConsultancyfirmName(), consultancy.getFirmType(), consultancy.getFirmpanNumber(), consultancy.getFirmgstNumber(), 
						consultancy.getCheckPrintingName(), 
						consultancy.getConsultancyAddress(), consultancy.getCountryId(), consultancy.getStateId(), consultancy.getCityId(),consultancy.getLocationareaId(), consultancy.getAreaPincode(),
						consultancy.getDepartmentId(), consultancy.getDesignationId(), consultancy.getEmployeeId(), 
						consultancy.getConsultancyBankName(), consultancy.getBranchName(), consultancy.getBankifscCode(), consultancy.getConsultancyBankacno(),
						consultancy.getStatus(), consultancy.getCreationDate(), consultancy.getUpdateDate(), consultancy.getUserName());
				consultancyRepository.save(consultancy);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}

			List<Consultancy> consultancyList = consultancyRepository.findAll(); 

			model.addAttribute("consultancyList",consultancyList);


			return "ConsultancyMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/AddConsultancyEmployee")
	public List<ConsultancyEmployees> AddConsultancyEmployee(@RequestParam("consultancyId") String consultancyId, @RequestParam("designationId") String designationId, @RequestParam("employeeName") String employeeName, @RequestParam("employeeEmailId") String employeeEmailId, @RequestParam("employeeMobileNo") String employeeMobileNo, HttpSession session)
	{
		try
		{
			ConsultancyEmployees consultancyEmployees = new ConsultancyEmployees();
			consultancyEmployees.setConsultancyEmployeeId(Generate_ConsultancyEmployeeId());
			consultancyEmployees.setConsultancyId(consultancyId);
			consultancyEmployees.setDesignationId(designationId);
			consultancyEmployees.setEmployeeName(employeeName);
			consultancyEmployees.setEmployeeEmailId(employeeEmailId);
			consultancyEmployees.setEmployeeMobileNo(employeeMobileNo);

			consultancyemployeesRepository.save(consultancyEmployees);

			Query query = new Query();
			List<ConsultancyEmployees> consultancyemployeeList = mongoTemplate.find(query.addCriteria(Criteria.where("consultancyId").is(consultancyId)), ConsultancyEmployees.class);
			for(int i=0;i<consultancyemployeeList.size();i++)
			{
				query = new Query();
				List<Designation> DesignationDetails = mongoTemplate.find(query.addCriteria(Criteria.where("designationId").is(consultancyemployeeList.get(i).getDesignationId())), Designation.class);
				consultancyemployeeList.get(i).setDesignationId(DesignationDetails.get(0).getDesignationName());
			}
			return consultancyemployeeList;
		}
		catch(Exception e)
		{
			return null;
		}

	}	 

	@ResponseBody
	@RequestMapping("/DeleteConsultancyEmployee")
	public List<ConsultancyEmployees> DeleteConsultancyEmployee(@RequestParam("consultancyId") String consultancyId, @RequestParam("consultancyEmployeeId") String consultancyEmployeeId, HttpSession session)
	{
		try
		{
			Query query = new Query();
			mongoTemplate.remove(query.addCriteria(Criteria.where("consultancyEmployeeId").is(Integer.parseInt(consultancyEmployeeId)).and("consultancyId").is(consultancyId)), ConsultancyEmployees.class);

			query = new Query();
			List<ConsultancyEmployees> consultancyemployeeList = mongoTemplate.find(query.addCriteria(Criteria.where("consultancyId").is(consultancyId)), ConsultancyEmployees.class);
			for(int i=0;i<consultancyemployeeList.size();i++)
			{
				query = new Query();
				List<Designation> DesignationDetails = mongoTemplate.find(query.addCriteria(Criteria.where("designationId").is(consultancyemployeeList.get(i).getDesignationId())), Designation.class);
				consultancyemployeeList.get(i).setDesignationId(DesignationDetails.get(0).getDesignationName());
			}
			return consultancyemployeeList;
		}
		catch(Exception e)
		{
			return null;
		}

	} 

}

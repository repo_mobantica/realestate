package com.realestate.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.realestate.bean.Agent;
import com.realestate.bean.AgentEmployees;
import com.realestate.bean.Bank;
import com.realestate.bean.City;
import com.realestate.bean.Country;
import com.realestate.bean.Designation;
import com.realestate.bean.LocationArea;
import com.realestate.bean.State;
import com.realestate.bean.SupplierEmployees;
import com.realestate.repository.AgentEmployeeRepository;
import com.realestate.repository.AgentRepository;
import com.realestate.repository.BankRepository;
import com.realestate.repository.CityRepository;
import com.realestate.repository.CountryRepository;
import com.realestate.repository.DesignationRepository;
import com.realestate.repository.LocationAreaRepository;
import com.realestate.repository.StateRepository;
import com.realestate.repository.SupplierEmployeeRepository;
import com.realestate.services.AgentDetailsExcelView;
@Controller
@RequestMapping("/")
public class AgentController {

	@Autowired
	LocationAreaRepository locationAreaRepository;
	@Autowired
	CityRepository cityRepository;
	@Autowired
	StateRepository stateRepository;
	@Autowired
	AgentEmployeeRepository agentEmployeesRepository;
	@Autowired
	CountryRepository countryRepository;
	@Autowired
	BankRepository bankRepository;
	@Autowired
	DesignationRepository designationRepository;
	@Autowired
	AgentRepository agentRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	String agentCode;
	private String AgentCode()
	{
		long cnt  = agentRepository.count();
		if(cnt<10)
		{
			agentCode = "AG000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			agentCode = "AG00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			agentCode = "AG0"+(cnt+1);
		}
		else if(cnt>=1000)
		{
			agentCode = "AG"+(cnt+1);
		}
		return agentCode;
	}
	//Code For Retrieving Country Names
	private List<Country> findAllCountryName()
	{
		List<Country> countryList;

		countryList = countryRepository.findAll(new Sort(Sort.Direction.ASC,"countryId"));
		return countryList;
	}

	private List<Designation> getAllDesignation()
	{
		List<Designation> designationList = designationRepository.findAll(new Sort(Sort.Direction.ASC,"designationName"));

		return designationList;
	}

	//code for getting all bank names
	private List<Bank> getAllBankName()
	{
		List<Bank> bankList = bankRepository.findAll(new Sort(Sort.Direction.ASC,"bankName"));
		return bankList;
	}

	@RequestMapping("/AddAgent")
	public String AddAgent(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			model.addAttribute("designationList", getAllDesignation());
			model.addAttribute("bankList",getAllBankName());
			model.addAttribute("countryList",findAllCountryName());
			model.addAttribute("agentCode",AgentCode());

			return "AddAgent";
		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping("/AgentMaster")
	public String AgentMaster(Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<Agent> agentList = agentRepository.findAll(); 
			model.addAttribute("agentList",agentList);

			return "AgentMaster";
		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value = "/AddAgent", method = RequestMethod.POST)
	public String SaveAgent(@ModelAttribute Agent agent, Model model)
	{
		try {
			try
			{
				agent = new Agent(agent.getAgentId(), agent.getAgentfirmName(), agent.getAgentfirmType(), agent.getFirmpanNumber(), agent.getFirmgstNumber(), 
						agent.getBrokeragePercentage(), agent.getAgentreraNumber(), agent.getCheckPrintingName(), agent.getAgentpaymentTerms(),
						agent.getAgentfirmAddress(), agent.getAgentPincode(), 
						agent.getBankName(),agent.getBranchName(), agent.getBankifscCode(), agent.getFirmbankacNumber(), agent.getCommissionBased(),
						agent.getCreationDate(), agent.getUpdateDate(), agent.getUserName());
				agentRepository.save(agent);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}

			model.addAttribute("designationList", getAllDesignation());
			model.addAttribute("bankList",getAllBankName());
			model.addAttribute("countryList",findAllCountryName());
			model.addAttribute("agentCode",AgentCode());

			return "AddAgent";
		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/EditAgent")
	public String EditAgent(@RequestParam("agentId") String agentId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<Agent> agentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("agentId").is(agentId)),Agent.class);
			List<Designation> designationList = designationRepository.findAll();
			query = new Query();
			List<AgentEmployees> agentEmployeesList = mongoTemplate.find(query.addCriteria(Criteria.where("agentId").is(agentId)), AgentEmployees.class); 

			model.addAttribute("agentDetails", agentDetails);
			model.addAttribute("bankList",getAllBankName());
			model.addAttribute("designationList",designationList);
			model.addAttribute("agentEmployeesList", agentEmployeesList);
			return "EditAgent";
		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping(value="/EditAgent",method=RequestMethod.POST)
	public String EditAgentPostMethod(@ModelAttribute Agent agent, Model model)
	{
		try {
			try
			{
				agent = new Agent(agent.getAgentId(), agent.getAgentfirmName(), agent.getAgentfirmType(), agent.getFirmpanNumber(), agent.getFirmgstNumber(), 
						agent.getBrokeragePercentage(), agent.getAgentreraNumber(), agent.getCheckPrintingName(), agent.getAgentpaymentTerms(),
						agent.getAgentfirmAddress(), agent.getAgentPincode(), 
						agent.getBankName(),agent.getBranchName(), agent.getBankifscCode(), agent.getFirmbankacNumber(), agent.getCommissionBased(),
						agent.getCreationDate(), agent.getUpdateDate(), agent.getUserName());
				agentRepository.save(agent);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}

			List<Agent> agentList = agentRepository.findAll(); 

			model.addAttribute("agentList",agentList);

			return "AgentMaster";
		}catch (Exception e) {
			return "login";
		}
	}


	@ResponseBody
	@RequestMapping("/getCountryWiseAgentList")
	public List<Agent> CountryWiseSupplierList(@RequestParam("countryId") String countryId)
	{
		Query query =new Query();
		List<Agent> agentList = mongoTemplate.find(query.addCriteria(Criteria.where("countryId").is(countryId)), Agent.class);

		return agentList;
	}

	@ResponseBody
	@RequestMapping("/getStateWiseAgentList")
	public List<Agent> StateWiseAgentList(@RequestParam("countryId") String countryId , @RequestParam("stateId") String stateId)
	{
		Query query =new Query();
		List<Agent> agentList = mongoTemplate.find(query.addCriteria(Criteria.where("countryId").is(countryId).and("stateId").is(stateId)), Agent.class);

		return agentList;
	}

	@ResponseBody
	@RequestMapping("/getCityWiseAgentList")
	public List<Agent> CityWiseAgentList(@RequestParam("countryId") String countryId , @RequestParam("stateId") String stateId , @RequestParam("cityId") String cityId)
	{
		Query query =new Query();
		List<Agent> agentList = mongoTemplate.find(query.addCriteria(Criteria.where("countryId").is(countryId).and("stateId").is(stateId).and("cityId").is(cityId)), Agent.class);

		return agentList;
	}

	@ResponseBody
	@RequestMapping("/getLocationAreaWiseAgentList")
	public List<Agent> LocationAreaWiseAgentList(@RequestParam("countryId") String countryId , @RequestParam("stateId") String stateId , @RequestParam("cityId") String cityId , @RequestParam("locationareaId") String locationareaId)
	{
		Query query =new Query();
		List<Agent> agentList = mongoTemplate.find(query.addCriteria(Criteria.where("countryId").is(countryId).and("stateId").is(stateId).and("cityId").is(cityId).and("locationareaId").is(locationareaId)), Agent.class);

		return agentList;			 
	}

	public int Generate_AgentEmployeeId()
	{
		int agentEmployeeId = 0;

		List<AgentEmployees> agentEmployeeList = agentEmployeesRepository.findAll();

		int size = agentEmployeeList.size();

		if(size!=0)
		{
			agentEmployeeId = agentEmployeeList.get(size-1).getAgentEmployeeId();
		}

		return agentEmployeeId+1;
	}

	@ResponseBody
	@RequestMapping("/AddAgentEmployee")
	public List<AgentEmployees> AddAgentEmployee(@RequestParam("agentId") String agentId, @RequestParam("employeeName") String employeeName, @RequestParam("employeeDesignation") String employeeDesignation, @RequestParam("employeeEmail") String employeeEmail, @RequestParam("employeeMobileno") String employeeMobileno, HttpSession session)
	{
		try
		{
			AgentEmployees agentEmployees = new AgentEmployees();
			agentEmployees.setAgentEmployeeId(Generate_AgentEmployeeId());
			agentEmployees.setAgentId(agentId);
			agentEmployees.setEmployeeName(employeeName);
			agentEmployees.setEmployeeDesignation(employeeDesignation);
			agentEmployees.setEmployeeEmail(employeeEmail);
			agentEmployees.setEmployeeMobileno(employeeMobileno);

			agentEmployeesRepository.save(agentEmployees);

			Query query = new Query();
			List<AgentEmployees> agentEmployeesList = mongoTemplate.find(query.addCriteria(Criteria.where("agentId").is(agentId)), AgentEmployees.class);
			return agentEmployeesList;
		}
		catch(Exception e)
		{
			return null;
		}

	}	 

	@ResponseBody
	@RequestMapping("/DeleteAgentEmployee")
	public List<AgentEmployees> DeleteAgentEmployee(@RequestParam("agentId") String agentId, @RequestParam("agentEmployeeId") String agentEmployeeId, HttpSession session)
	{
		try
		{
			Query query = new Query();
			mongoTemplate.remove(query.addCriteria(Criteria.where("agentEmployeeId").is(Integer.parseInt(agentEmployeeId)).and("agentId").is(agentId)), AgentEmployees.class);

			query = new Query();
			List<AgentEmployees> agentEmployeeList = mongoTemplate.find(query.addCriteria(Criteria.where("agentId").is(agentId)), AgentEmployees.class);

			return agentEmployeeList;
		}
		catch(Exception e)
		{
			return null;
		}

	} 


	@RequestMapping(value="/ExportAgentList", method=RequestMethod.GET)
	public ModelAndView generateExcel(HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
		List<Agent> agentDetails = agentRepository.findAll();

		ModelAndView modelAndView = new ModelAndView(new AgentDetailsExcelView(), "agentDetails" , agentDetails);

		return modelAndView;
	}	 
}

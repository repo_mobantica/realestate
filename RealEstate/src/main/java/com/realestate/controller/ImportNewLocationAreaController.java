package com.realestate.controller;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.realestate.bean.City;
import com.realestate.bean.Country;
import com.realestate.bean.LocationArea;
import com.realestate.bean.State;
import com.realestate.repository.CityRepository;
import com.realestate.repository.CountryRepository;
import com.realestate.repository.LocationAreaRepository;
import com.realestate.repository.StateRepository;

@Controller
@RequestMapping("/")
public class ImportNewLocationAreaController {

	@Autowired
	ServletContext context;

	@Autowired
	LocationAreaRepository locationAreaRepository;
	@Autowired
	CityRepository cityRepository;
	@Autowired
	StateRepository stateRepository;
	@Autowired
	CountryRepository countryRepository;
	@Autowired
	MongoTemplate mongoTemplate;
	LocationArea locationarea = new LocationArea();
	HSSFWorkbook workbook;
	XSSFWorkbook workbook1;

	HSSFSheet worksheet;
	XSSFSheet worksheet1;

	String locationAreaCode;
	private String locationAreaCode()
	{
		long locationAreaCount  = locationAreaRepository.count();

		if(locationAreaCount<10)
		{
			locationAreaCode = "LA000"+(locationAreaCount+1);
		}
		else if((locationAreaCount>=10) && (locationAreaCount<100))
		{
			locationAreaCode = "LA00"+(locationAreaCount+1);
		}
		else if((locationAreaCount>=100) && (locationAreaCount<1000))
		{
			locationAreaCode = "LA0"+(locationAreaCount+1);
		}
		else
		{
			locationAreaCode = "LA"+(locationAreaCount+1);
		}

		return locationAreaCode;
	}

	@RequestMapping(value="/ImportNewLocationArea")
	public String ImportNewLocationArea(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		return "ImportNewLocationArea";
	}


	@SuppressWarnings({ "deprecation"})
	@RequestMapping(value = "/ImportNewLocationArea", method = RequestMethod.POST)
	public String uploadLocationAreaDetails(@RequestParam("locationarea_excel") MultipartFile locationarea_excel, Model model, HttpServletRequest request, RedirectAttributes redirectAttributes, HttpSession session) 
	{

		String user = (String)request.getSession().getAttribute("user");
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/M/yyyy");
		LocalDate localDate = LocalDate.now();
		String todayDate=dtf.format(localDate);

		List<LocationArea> locationareaList= locationAreaRepository.findAll();
		List<City> cityList = cityRepository.findAll();
		List<State> stateList= stateRepository.findAll();
		List<Country> countryList= countryRepository.findAll();

		String countryId="",stateId="",cityId="";
		int flag1=0,flag2=0;
		if (!locationarea_excel.isEmpty() || locationarea_excel != null )
		{
			try 
			{
				if(locationarea_excel.getOriginalFilename().endsWith("xls") || locationarea_excel.getOriginalFilename().endsWith("xlsx") || locationarea_excel.getOriginalFilename().endsWith("csv"))
				{
					if(locationarea_excel.getOriginalFilename().endsWith("xlsx"))
					{
						InputStream stream = locationarea_excel.getInputStream();
						XSSFWorkbook workbook = new XSSFWorkbook(stream);

						XSSFSheet sheet = workbook.getSheet("Sheet1");  /// this will read 1st workbook of ExcelSheet

						int firstRow = sheet.getFirstRowNum();

						XSSFRow firstrow = sheet.getRow(firstRow);

						@SuppressWarnings("unused")
						int lastColumnCount = firstrow.getLastCellNum();

						@SuppressWarnings("unused")
						Iterator<Row> rowIterator = sheet.iterator();   
						int last_no=sheet.getLastRowNum();

						LocationArea locationarea = new LocationArea();
						for(int i=0;i<last_no;i++)
						{
							countryId="";
							stateId="";
							cityId="";
							flag1=0;
							flag2=0;
							XSSFRow row = sheet.getRow(i+1);

							// Skip read heading 
							if (row.getRowNum() == 0) 
							{
								continue;
							}

							for(int j=0;j<countryList.size();j++)
							{
								if(countryList.get(j).getCountryName().equalsIgnoreCase(row.getCell(4).getStringCellValue()))
								{
									for(int k=0;k<stateList.size();k++)
									{
										if(stateList.get(k).getStateName().equalsIgnoreCase(row.getCell(3).getStringCellValue()))
										{
											for(int l=0;l<cityList.size();l++)
											{
												if(cityList.get(l).getCityName().equalsIgnoreCase(row.getCell(2).getStringCellValue()))
												{
													countryId=countryList.get(j).getCountryId();
													stateId=stateList.get(k).getStateId();
													cityId=cityList.get(l).getCityId();
													flag1=1;
													break;
												}
											}
										}
									}
								}
							}
							if(flag1==1)
							{
								for(int j=0;j<locationareaList.size();j++)
								{
									if(locationareaList.get(j).getCountryId().equalsIgnoreCase(countryId) && locationareaList.get(j).getStateId().equalsIgnoreCase(stateId) && locationareaList.get(j).getCityId().equalsIgnoreCase(cityId) && locationareaList.get(j).getLocationareaName().equalsIgnoreCase(row.getCell(0).getStringCellValue()))
									{
										flag2=1;
									}
								}
							}

							if(flag1== 1 && flag2==0)
							{
								locationarea.setLocationareaId(locationAreaCode());

								row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
								locationarea.setLocationareaName(row.getCell(0).getStringCellValue());
								row.getCell(1).setCellType(Cell.CELL_TYPE_STRING);
								locationarea.setPinCode(row.getCell(1).getStringCellValue());

								locationarea.setCityId(cityId);
								locationarea.setStateId(stateId);
								locationarea.setCountryId(countryId);

								locationarea.setCreationDate(todayDate);
								locationarea.setUpdateDate(todayDate);
								locationarea.setUserName(user);
								locationAreaRepository.save(locationarea);
							}

						}
						workbook.close();
					}

				}//if after inner if

			}
			catch(Exception e)
			{

			}
		}

		return "ImportNewLocationArea";
	}

	//private static final String INTERNAL_FILE="Newlocationarea.xlsx";
	@RequestMapping(value = "/DownloadLocationAreaTemplate")
	public String downloadLocationAreaTemplate(Model model, HttpServletResponse response, HttpServletRequest request, RedirectAttributes redirectAttributes, HttpSession session) throws IOException 
	{

		String filename = "NewLocationArea.xlsx";
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		String filepath = "//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"Template/";
		response.setContentType("APPLICATION/OCTET-STREAM");
		response.setHeader("Content-Disposition", "attachment; filename=\""+ filename + "\"");


		FileInputStream fileInputStream = new FileInputStream(filepath+filename);

		int i;
		while ((i = fileInputStream.read()) != -1) {
			out.write(i);
		}
		fileInputStream.close();
		out.close();

		return "ImportNewLocationArea";
	}
}

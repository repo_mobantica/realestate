package com.realestate.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Flat;
import com.realestate.bean.Login;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.FlatRepository;
import com.realestate.repository.ProjectBuildingRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.ProjectWingRepository;

@Controller
@RequestMapping("/")
public class SqFtWiseFlatReportController 
{
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	FlatRepository flatRepository;
	@Autowired
	ProjectBuildingRepository projectBuildingRepository;
	@Autowired
	ProjectWingRepository projectWingRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping("/SqFtWiseFlatReport")
	public String SqFtWiseFlatReport(Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			String projectId="", buildingId="", wingId="";

			List<Project> projectList = GetUserAssigenedProjectList(req,res); 

			Map<String, List<String>> flatList = new HashMap<String, List<String>>();

			List<String> flatDeatils = new ArrayList<String>();

			projectId = projectList.get(0).getProjectId();
			try
			{
				Query query = new Query();
				List<ProjectBuilding> buildingList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)), ProjectBuilding.class);
				buildingId = buildingList.get(0).getBuildingId();
				query = new Query();
				List<ProjectWing> wingList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId)), ProjectWing.class);
				wingId = wingList.get(0).getWingId();
				query = new Query();
				List<Flat> flatList1 = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId)), Flat.class);

				for(int i=0;i<flatList1.size();i++)
				{
					//Double flatArea = flatList1.get(i).getFlatAreawithLoadingInFt();
					flatDeatils = new ArrayList<String>();

					for(int j=0;j<flatList1.size();j++)
					{
						if(flatList1.get(i).getFlatAreawithLoadingInFt().equals(flatList1.get(j).getFlatAreawithLoadingInFt()))
						{
							flatDeatils.add(flatList1.get(j).getFlatNumber());
						}
					}

					flatList.put(""+flatList1.get(i).getFlatAreawithLoadingInFt(), flatDeatils);
				}

				model.addAttribute("project_building_wing","Project : "+projectList.get(0).getProjectName()+", Building : "+buildingList.get(0).getBuildingName()+", Wing : "+wingList.get(0).getWingName());

			}
			catch (Exception e) {
				System.out.println("error = "+e);
			}
			model.addAttribute("projectList", projectList);
			model.addAttribute("flatList", flatList);
			return "SqFtWiseFlatReport";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/getFlatListByProject_Building_Wing")
	public Map<String, List<String>> getFlatListByProject_Building_Wing(@RequestParam("projectId") String projectId, @RequestParam("buildingId") String buildingId, @RequestParam("wingId") String wingId)
	{
		//System.out.println("Project Name = "+projectName+" \nBuilding Name = "+buildingName+" \nWing Name = "+wingName);
		try
		{
			Query query = new Query();
			List<Flat> flatList1 = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId)), Flat.class);

			query = new Query();
			List<Project> projectList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)), Project.class);

			query = new Query();
			List<ProjectBuilding> buildingList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)), ProjectBuilding.class);
			buildingId = buildingList.get(0).getBuildingId();
			query = new Query();
			List<ProjectWing> wingList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId)), ProjectWing.class);

			Map<String, List<String>> flatList = new HashMap<String, List<String>>();

			List<String> flatDeatils = new ArrayList<String>();

			for(int i=0;i<flatList1.size();i++)
			{
				try
				{
					flatDeatils = new ArrayList<String>();

					for(int j=0;j<flatList1.size();j++)
					{
						if(flatList1.get(i).getFlatAreawithLoadingInFt().equals(flatList1.get(j).getFlatAreawithLoadingInFt()))
						{
							flatDeatils.add(flatList1.get(j).getFlatNumber());
						}
					}

					flatList.put(""+flatList1.get(i).getFlatAreawithLoadingInFt(), flatDeatils);
				}
				catch (Exception e) {
					System.out.println(" "+e);
				}

				flatList1.get(i).setProjectId(projectList.get(0).getProjectName());
				flatList1.get(i).setBuildingId(buildingList.get(0).getBuildingName());
				flatList1.get(i).setWingId(wingList.get(0).getWingName());

			}
			return flatList;
		}
		catch(Exception e)
		{
			System.out.println("Exception = "+e.toString());
			return null;
		}
	}

}

package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Department;
import com.realestate.bean.Designation;
import com.realestate.repository.DepartmentRepository;
import com.realestate.repository.DesignationRepository;


@Controller
@RequestMapping("/")
public class DesignationController 
{
	@Autowired
	DesignationRepository designationRepository;
	@Autowired
	DepartmentRepository departmentRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	//code for designation code geration
	String designationCode;
	private String designationCode()
	{
		long designationCount = designationRepository.count();

		if(designationCount<10)
		{
			designationCode = "DE000"+(designationCount+1);
		}
		else if((designationCount>=10) && (designationCount<100))
		{
			designationCode = "DE00"+(designationCount+1);
		}
		else if((designationCount>=100) && (designationCount<1000))
		{
			designationCode = "DE0"+(designationCount+1);
		}
		else
		{
			designationCode = "DE"+(designationCount+1);
		}

		return designationCode;
	}


	@ResponseBody
	@RequestMapping("/searchDepartmentWiseDesignation")
	public List<Designation> searchDepartmentWiseDesignation(@RequestParam("departmentId") String departmentId)
	{
		Query query = new Query();
		List<Designation> designationList = mongoTemplate.find(query.addCriteria(Criteria.where("departmentId").is(departmentId)), Designation.class);

		for(int i=0;i<designationList.size();i++)
		{
			try {
				query = new Query();
				List<Department> departmentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("departmentId").is(designationList.get(i).getDepartmentId())),Department.class);
				designationList.get(i).setDepartmentId(departmentDetails.get(0).getDepartmentName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}


		return designationList;
	}

	@RequestMapping("/DesignationMaster")
	public String DesignationMaster(ModelMap model)
	{
		try {
			List<Designation> designationList = designationRepository.findAll();
			List<Department> departmentList = departmentRepository.findAll();
			Query query = new Query();
			for(int i=0;i<designationList.size();i++)
			{
				try {
					query = new Query();
					List<Department> departmentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("departmentId").is(designationList.get(i).getDepartmentId())),Department.class);
					designationList.get(i).setDepartmentId(departmentDetails.get(0).getDepartmentName());
				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}

			model.addAttribute("departmentList",departmentList);
			model.addAttribute("designationList", designationList);

			return "DesignationMaster";

		}catch (Exception e) {
			return "login";
		}

	}

	//Request Mapping For Add Designation
	@RequestMapping("/AddDesignation")
	public String addDesignation(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<Designation> designationList = designationRepository.findAll();
			List<Department> departmentList =departmentRepository.findAll();

			model.addAttribute("designationCode",designationCode());
			model.addAttribute("designationList",designationList);
			model.addAttribute("departmentList", departmentList);

			return "AddDesignation";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/AddDesignation", method=RequestMethod.POST)
	public String designationSave(@ModelAttribute Designation designation, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				designation = new Designation(designation.getDesignationId(), designation.getDepartmentId(),designation.getDesignationName(),designation.getCreationDate(),designation.getUpdateDate(),designation.getUserName());
				designationRepository.save(designation);
				model.addAttribute("designationStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("designationStatus","Fail");
			}

			List<Designation> designationList = designationRepository.findAll();
			List<Department> departmentList =departmentRepository.findAll();

			model.addAttribute("designationCode",designationCode());
			model.addAttribute("designationList", designationList);
			model.addAttribute("departmentList", departmentList);

			return "AddDesignation";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/getdesignationList")
	public List<Designation> AllStateList(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		List<Designation> designationList= designationRepository.findAll();

		return designationList;
	}

	@RequestMapping("/EditDesignation")
	public String EditDesignation(@RequestParam("designationId") String designationId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<Designation> designationDetails = mongoTemplate.find(query.addCriteria(Criteria.where("designationId").is(designationId)),Designation.class);

			query = new Query();
			List<Department> departmentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("departmentId").is(designationDetails.get(0).getDepartmentId())),Department.class);



			List<Department> departmentList =departmentRepository.findAll();

			model.addAttribute("departmentName",departmentDetails.get(0).getDepartmentName());
			model.addAttribute("designationDetails",designationDetails);
			model.addAttribute("departmentList", departmentList);
			return "EditDesignation";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/EditDesignation", method=RequestMethod.POST)
	public String EditDesignationPostMethod(@ModelAttribute Designation designation, ModelMap model)
	{
		try {
			try
			{
				designation = new Designation(designation.getDesignationId(), designation.getDepartmentId(),designation.getDesignationName(),designation.getCreationDate(),designation.getUpdateDate(),designation.getUserName());
				designationRepository.save(designation);
				model.addAttribute("designationStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("designationStatus","Fail");
			}

			List<Designation> designationList = designationRepository.findAll();
			List<Department> departmentList =departmentRepository.findAll();

			model.addAttribute("designationList",designationList);
			model.addAttribute("departmentList", departmentList);

			return "DesignationMaster";

		}catch (Exception e) {
			return "login";
		}
	}
}

package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.realestate.bean.Designation;
import com.realestate.bean.Employee;
import com.realestate.bean.Login;
import com.realestate.bean.Project;
import com.realestate.bean.UserModel;
import com.realestate.bean.UserProjectList;
import com.realestate.configuration.CommanController;
import com.realestate.repository.LoginRepository;

@Controller
@RequestMapping("/")
public class LoginController
{
	@Autowired
	LoginRepository loginRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	String userId = "";

	public String UserId()
	{
		long count = loginRepository.count();

		if(count<10)
		{
			userId = "U000"+(count+1);	 
		}
		else if(count>=10 && count<100)
		{
			userId = "U00"+(count+1);
		}
		else if(count>=100 && count<1000)
		{
			userId = "U0"+(count+1);
		}
		else
		{
			userId = "U"+(count+1);
		}

		return userId;
	}


	//Request Mapping For Login
	@RequestMapping(method = RequestMethod.GET)
	public String loginRedirect(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		return "login";
	}

	@RequestMapping(value="/login",method = RequestMethod.GET)
	public String LoginReturnByLogout(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		return "login";
	}

	@RequestMapping(value="/login", method=RequestMethod.POST) 
	public String checkLogin(@ModelAttribute Login login, Model model, HttpServletRequest req,HttpServletResponse res)
	{
		String userName = req.getParameter("userName");
		String passWord = req.getParameter("passWord");

		Query query = new Query();

		try
		{
			query = new Query();
			List<Login> loginUser = mongoTemplate.find(query.addCriteria(Criteria.where("userName").is(userName).and("passWord").is(passWord).and("status").is("Active")), Login.class);

			if(loginUser.size()==1)
			{
				HttpSession session = req.getSession(true);
				session.setAttribute("user",loginUser.get(0).getUserId());

				query = new Query();
				List<Employee> employeeData = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginUser.get(0).getEmployeeId())), Employee.class);

				query = new Query();
				List<UserModel> userMenuAccessList = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginUser.get(0).getEmployeeId())), UserModel.class);

				if(userMenuAccessList.size()==0 && loginUser.get(0).getUserName().equals("Admin"))
				{
					UserModel user = new UserModel();

					userMenuAccessList = new ArrayList<UserModel>();

					user.setPreSaleaManagment("OK");

					user.setSalesManagment("OK");
					user.setFlatStatusReport("OK");

					user.setEnquiryReport("OK");
					user.setBookingReport("OK");

					user.setCustomerPaymentReport("OK");
					user.setDemandPaymentReport("OK");
					user.setParkingReport("OK");
					user.setAgentReport("OK");
					user.setCustomerReport("OK");

					user.setAddTask("OK");
					user.setHrANDpayRollManagement("OK");

					user.setConsultancyMaster("OK");
					user.setArchitecturalSection("OK");
					user.setContractorMagagement("OK");
					user.setProjectEngineering("OK");
					user.setJrEngineering("OK");
					user.setQualityEngineering("OK");
					user.setSrSupervisor("OK");
					user.setJrSupervisor("OK");
					user.setMaterialRequisitionList("OK");
					user.setStoreManagement("OK");
					user.setStoreStocks("OK");
					user.setMaterialTransferToContractor("OK");
					user.setCustomerPaymentStatus("OK");
					user.setSupplierPaymentBill("OK");
					user.setAgentPayment("OK");
					user.setProjectDevelopmentWork("OK");
					user.setArchitectsCertificate("OK");
					user.setProjectSpecificationMaster("OK");
					user.setUserManagement("OK");
					user.setCompanyDocuments("OK");
					user.setReraDocuments("OK");
					user.setLicensingDepartment("OK");
					/*
			     user.setStatus(userMenuAccessList.get(0).getStatus());
			     user.setCreationDate(userMenuAccessList.get(0).getCreationDate());
			     user.setUpdateDate(userMenuAccessList.get(0).getUpdateDate());
			     user.setUserName(userMenuAccessList.get(0).getUserName());
					 */
					userMenuAccessList.add(user);

					session.setAttribute("userMenuAccessList", userMenuAccessList);
				}
				else
				{
					session.setAttribute("userMenuAccessList", userMenuAccessList);
				}


				query = new Query();
				List<UserProjectList> userLoginProjectList = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginUser.get(0).getEmployeeId())), UserProjectList.class);


				Project project = new Project();

				List<Project> projectList=new ArrayList<Project>();


				for(int i=0;i<userLoginProjectList.size();i++)
				{
					project = new Project();

					project.setProjectId(userLoginProjectList.get(i).getProjectId());
					projectList.add(project);
				}

				try
				{
					query = new Query();
					List<Designation> designationDetails = mongoTemplate.find(query.addCriteria(Criteria.where("designationId").is(employeeData.get(0).getDesignationId())), Designation.class);

					session.setAttribute("designationName", designationDetails.get(0).getDesignationName());
				}
				catch (Exception e) {
					// TODO: handle exception
				}
				try {
					String path = CommanController.GetProfileImagePath() + employeeData.get(0).getEmployeeId() + ".png";

					session.setAttribute("profile_img", path);	
				}
				catch (Exception e) {
					// TODO: handle exception
				}

				session.setAttribute("projectList", projectList);
				session.setAttribute("userName", employeeData);

				return "redirect:home";
			}
			else
			{
				model.addAttribute("loginStatus", "Fail");

				return "login";
			}

		}
		catch(Exception e)
		{
			System.out.println("error = "+e);
			return "login";
		}
	}


	@RequestMapping("/logout")
	public String logout(HttpServletRequest req,HttpServletResponse res)
	{
		HttpSession session = req.getSession(false);

		session.removeAttribute("user");
		session.removeAttribute("userMenuAccessList");

		return "redirect:login";
	}

}
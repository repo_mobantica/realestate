package com.realestate.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Currency;
import com.realestate.bean.Tax;
import com.realestate.repository.CurrencyRepository;
import com.realestate.repository.TaxRepository;

@Controller
@RequestMapping("/")
public class TaxController
{
	@Autowired
	TaxRepository taxRepository;
	@Autowired
	CurrencyRepository currencyRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	String taxCode;
	private String TaxCode()
	{
		long cnt  = taxRepository.count();
		if(cnt<10)
		{
			taxCode = "TX000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			taxCode = "TX00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			taxCode = "TX0"+(cnt+1);
		}
		else
		{
			taxCode = "TX"+(cnt+1);
		}
		return taxCode;
	}

	//code to reterive all currency name
	private List<Currency> findAllCurrencyName()
	{
		List<Currency> currencyList;
		currencyList = currencyRepository.findAll(new Sort(Sort.Direction.ASC,"currencyName"));
		return currencyList;
	}

	@RequestMapping("/TaxMaster")
	public String TaxMaster(Model model)
	{
		try {
			List<Tax> taxDetails = taxRepository.findAll();

			model.addAttribute("taxDetails",taxDetails);
			return "TaxMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/searchTaxListByName")
	public List<Tax> getTaxListByName(@RequestParam("taxName") String taxName)
	{
		List<Tax> taxDetails;

		Query query = new Query();
		taxDetails = mongoTemplate.find(query.addCriteria(Criteria.where("taxName").regex("^"+taxName+".*","i")), Tax.class);

		return taxDetails;
	}

	@RequestMapping("/AddTax")
	public String addTax(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			model.addAttribute("taxCode",TaxCode());
			model.addAttribute("currencyList", findAllCurrencyName());

			return "AddTax";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value = "/AddTax", method = RequestMethod.POST)
	public String taxSave(@ModelAttribute Tax tax, Model model)
	{
		try {
			try
			{
				tax = new Tax(tax.getTaxId(),tax.getTaxType(), tax.getTaxName(),
						tax.getCgstPercentage(),tax.getSgstPercentage(),tax.getTaxPercentage(),
						tax.getCurrencyName(),tax.getWefDate(),
						tax.getCreationDate(), tax.getUpdateDate(), tax.getUserName());
				taxRepository.save(tax);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}

			model.addAttribute("taxCode",TaxCode());
			model.addAttribute("currencyList", findAllCurrencyName());
			return "AddTax";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/EditTax")
	public String EditTax(@RequestParam("taxId") String taxId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<Tax> taxDetails = mongoTemplate.find(query.addCriteria(Criteria.where("taxId").is(taxId)), Tax.class);

			model.addAttribute("taxDetails", taxDetails);
			model.addAttribute("currencyList", findAllCurrencyName());
			return "EditTax";

		}catch (Exception e) {
			return "login";
		}
	}
	@RequestMapping(value="/EditTax", method = RequestMethod.POST)
	public String EditTaxPostMethod(@ModelAttribute Tax tax, Model model)
	{
		try {
			try
			{
				tax = new Tax(tax.getTaxId(),tax.getTaxType(), tax.getTaxName(),
						tax.getCgstPercentage(),tax.getSgstPercentage(),tax.getTaxPercentage(),
						tax.getCurrencyName(),tax.getWefDate(),
						tax.getCreationDate(), tax.getUpdateDate(), tax.getUserName());
				taxRepository.save(tax);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}

			List<Tax> taxDetails = taxRepository.findAll();

			model.addAttribute("taxDetails",taxDetails);
			return "TaxMaster";

		}catch (Exception e) {
			return "login";
		}
	}
}

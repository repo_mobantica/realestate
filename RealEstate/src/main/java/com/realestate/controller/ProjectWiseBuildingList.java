package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.ProjectBuilding;
import com.realestate.repository.ProjectBuildingRepository;

@Controller
@RequestMapping("/")
public class ProjectWiseBuildingList 
{
	@Autowired
	ProjectBuildingRepository projectBuildingRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	@ResponseBody
	@RequestMapping(value="/getBuildingList",method=RequestMethod.POST)
	public List<ProjectBuilding> BuildingList(@RequestParam("projectId") String projectId, HttpSession session)
	{
		List<ProjectBuilding> projectbuildingList= new ArrayList<ProjectBuilding>();

		Query query = new Query();
		projectbuildingList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)), ProjectBuilding.class);

		return projectbuildingList;
	}
}

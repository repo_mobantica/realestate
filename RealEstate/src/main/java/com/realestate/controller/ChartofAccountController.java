package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.ChartofAccount;
import com.realestate.repository.ChartofAccountRepository;

@Controller
@RequestMapping("/")
public class ChartofAccountController
{
	@Autowired
	ChartofAccountRepository chartofaccountRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	String chartofaccountCode;
	private String ChartofAccountCode()
	{
		long cnt  = chartofaccountRepository.count();
		if(cnt<10)
		{
			chartofaccountCode = "COA000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			chartofaccountCode = "COA00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			chartofaccountCode = "COA0"+(cnt+1);
		}
		else
		{
			chartofaccountCode = "COA"+(cnt+1);
		}
		return chartofaccountCode;
	}

	@RequestMapping("/AddChartofAccount")
	public String addChartofAccount(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<ChartofAccount> charofaccountList = chartofaccountRepository.findAll(); 

			model.addAttribute("chartofaccountCode",ChartofAccountCode());
			model.addAttribute("charofaccountList",charofaccountList);
			return "AddChartofAccount";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping(value = "/AddChartofAccount", method = RequestMethod.POST)
	public String chartofaccountSave(@ModelAttribute ChartofAccount chartofaccount, Model model)
	{
		try {
			try
			{
				chartofaccount = new ChartofAccount(chartofaccount.getChartaccountId(), chartofaccount.getChartaccountNumber(), chartofaccount.getChartaccountName(),chartofaccount.getCreationDate(), chartofaccount.getUpdateDate(), chartofaccount.getUserName());
				chartofaccountRepository.save(chartofaccount);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}
			List<ChartofAccount> charofaccountList = chartofaccountRepository.findAll();

			model.addAttribute("chartofaccountCode",ChartofAccountCode());
			model.addAttribute("charofaccountList",charofaccountList);
			return "AddChartofAccount";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping(value="/getChartAccountNumber",method=RequestMethod.POST)
	public List<ChartofAccount> getChartAccountNumber(@RequestParam("chartaccountId") String chartaccountId, HttpSession session)
	{
		List<ChartofAccount> chartaccountNumberList= new ArrayList<ChartofAccount>();

		Query query = new Query();
		chartaccountNumberList = mongoTemplate.find(query.addCriteria(Criteria.where("chartaccountId").is(chartaccountId)), ChartofAccount.class);

		return chartaccountNumberList;
	}
	@RequestMapping("/ChartofAccountMaster")
	public String BudgetMaster(Model model)
	{
		try {
			List<ChartofAccount> charofaccountList = chartofaccountRepository.findAll();

			model.addAttribute("charofaccountList",charofaccountList);
			return "ChartofAccountMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/getSearchCOAList")
	public List<ChartofAccount> getCOAListByname(@RequestParam("chartaccountName") String chartaccountName)
	{
		List<ChartofAccount> charofaccountList;

		Query query = new Query();
		charofaccountList = mongoTemplate.find(query.addCriteria(Criteria.where("chartaccountName").regex("^"+chartaccountName+".*","i")), ChartofAccount.class);

		return charofaccountList;
	}

	@ResponseBody
	@RequestMapping("/getCOAList")
	public List<ChartofAccount> AllCOAList(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		List<ChartofAccount> charofaccountList= chartofaccountRepository.findAll();

		return charofaccountList;
	}

	@RequestMapping("/EditChartofAccount")
	public String EditChartodAccount(@RequestParam("chartaccountId") String chartaccountId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<ChartofAccount> chartofaccountDetails = mongoTemplate.find(query.addCriteria(Criteria.where("chartaccountId").is(chartaccountId)),ChartofAccount.class);

			model.addAttribute("chartofaccountDetails", chartofaccountDetails);
			return "EditChartofAccount";

		}catch (Exception e) {
			return "login";
		}
	}
	@RequestMapping(value="/EditChartofAccount",method=RequestMethod.POST)
	public String EditChartOfAccountPostMethod(@ModelAttribute ChartofAccount chartofaccount, Model model)
	{
		try {
			try
			{
				chartofaccount = new ChartofAccount(chartofaccount.getChartaccountId(), chartofaccount.getChartaccountNumber(), chartofaccount.getChartaccountName(),chartofaccount.getCreationDate(), chartofaccount.getUpdateDate(), chartofaccount.getUserName());
				chartofaccountRepository.save(chartofaccount);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}

			List<ChartofAccount> charofaccountList = chartofaccountRepository.findAll();
			model.addAttribute("charofaccountList",charofaccountList);

			return "ChartofAccountMaster";

		}catch (Exception e) {
			return "login";
		}
	}
}

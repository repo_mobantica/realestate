package com.realestate.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.realestate.bean.Store;
import com.realestate.bean.StoreStock;
import com.realestate.bean.StoreStockHistory;
import com.realestate.bean.SubSupplierType;
import com.realestate.bean.Supplier;
import com.realestate.bean.SupplierBillPayment;
import com.realestate.bean.SupplierBillPaymentHistory;
import com.realestate.repository.StoreRepository;
import com.realestate.repository.SupplierRepository;
import com.realestate.repository.SupplierBillPaymentHistoryRepository;
import com.realestate.repository.SupplierBillPaymentRepository;
import com.realestate.repository.BankTransactionHistoryDetailsRepository;
import com.realestate.repository.ChartofAccountRepository;
import com.realestate.repository.CompanyAccountPaymentDetailsRepository;
import com.realestate.repository.MaterialsPurchasedRepository;
import com.realestate.bean.BankTransactionHistoryDetails;
import com.realestate.bean.ChartofAccount;
import com.realestate.bean.Company;
import com.realestate.bean.CompanyAccountPaymentDetails;
import com.realestate.bean.CompanyBanks;
import com.realestate.bean.Item;
import com.realestate.bean.MaterialPurchaseRequisition;
import com.realestate.bean.MaterialsPurchased;
import com.realestate.bean.MaterialsPurchasedHistory;
import com.realestate.bean.Project;

@Controller
@RequestMapping("/")
public class SupplierPaymentBillController {

	@Autowired
	StoreRepository storeRepository;
	@Autowired
	SupplierRepository supplierRepository;
	@Autowired
	SupplierBillPaymentHistoryRepository supplierbillpaymenthistoryRepository;
	@Autowired
	SupplierBillPaymentRepository supplierbillpaymentRepository;
	@Autowired
	CompanyAccountPaymentDetailsRepository companyaccountpaymentdetailsRepository;
	@Autowired
	MaterialsPurchasedRepository materialspurchasedRepository;
	@Autowired
	ChartofAccountRepository chartofaccountRepository;
	@Autowired
	CompanyAccountPaymentDetailsRepository companyAccountPaymentDetailsRepository;
	@Autowired
	BankTransactionHistoryDetailsRepository banktransactionhistorydetailsRepository;
	@Autowired
	MongoTemplate mongoTemplate;


	String supplierBillPaymentHistoriId;
	private String SupplierBillPaymentHistoriId()
	{
		long cnt  = supplierbillpaymenthistoryRepository.count();
		if(cnt<10)
		{
			supplierBillPaymentHistoriId = "SBPH000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			supplierBillPaymentHistoriId = "SBPH00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			supplierBillPaymentHistoriId = "SBPH0"+(cnt+1);
		}
		else
		{
			supplierBillPaymentHistoriId = "SBPH"+(cnt+1);
		}
		return supplierBillPaymentHistoriId;
	}

	String supplierBillPaymentId;
	private String SupplierBillPaymentId()
	{
		long cnt  = supplierbillpaymentRepository.count();
		if(cnt<10)
		{
			supplierBillPaymentId = "SBP000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			supplierBillPaymentId = "SBP00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			supplierBillPaymentId = "SBP0"+(cnt+1);
		}
		else
		{
			supplierBillPaymentId = "SBP"+(cnt+1);
		}
		return supplierBillPaymentId;
	}

	String paymentId="";
	public String GeneratePaymentId()
	{

		long cnt = companyaccountpaymentdetailsRepository.count();

		if(cnt<10)
		{
			paymentId = "P000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			paymentId = "P00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			paymentId = "P0"+(cnt+1);
		}
		else
		{
			paymentId = "P"+(cnt+1);
		}

		return paymentId;
	}


	@RequestMapping("/SupplierPaymentBill")
	public String SupplierPaymentBill(Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<Store> storeList = storeRepository.findAll();
			List<Supplier> supplierList = supplierRepository.findAll();

			Query query = new Query();
			List<MaterialsPurchased> purchesedList = mongoTemplate.find(query.addCriteria(Criteria.where("paymentStatus").ne("Clear")),MaterialsPurchased.class);
	
			String storeName="";
			String supplierfirmName="";
			int count=1;
			for(int i=purchesedList.size()-1;i>=0;i--)
			{
				purchesedList.get(i).setNumber(count);
				count++;
				for(int j=0;j<storeList.size();j++)
				{
					if(purchesedList.get(i).getStoreId().equals(storeList.get(j).getStoreId()))
					{
						storeName=storeList.get(j).getStoreName();
						break;
					}
				}
				purchesedList.get(i).setStoreId(storeName);

				for(int j=0;j<supplierList.size();j++)
				{
					if(purchesedList.get(i).getSupplierId().equals(supplierList.get(j).getSupplierId()))
					{
						supplierfirmName=supplierList.get(j).getSupplierfirmName();
						break;
					}
				}
				purchesedList.get(i).setSupplierId(supplierfirmName);
			}

			// list for clear payment status
			query = new Query();
			List<MaterialsPurchased> purchesedClearPaymentList = mongoTemplate.find(query.addCriteria(Criteria.where("paymentStatus").is("Clear")),MaterialsPurchased.class);

			for(int i=0;i<purchesedClearPaymentList.size();i++)
			{
				for(int j=0;j<storeList.size();j++)
				{
					if(purchesedClearPaymentList.get(i).getStoreId().equals(storeList.get(j).getStoreId()))
					{
						storeName=storeList.get(j).getStoreName();
						break;
					}
				}
				purchesedClearPaymentList.get(i).setStoreId(storeName);

				for(int j=0;j<supplierList.size();j++)
				{
					if(purchesedClearPaymentList.get(i).getSupplierId().equals(supplierList.get(j).getSupplierId()))
					{
						supplierfirmName=supplierList.get(j).getSupplierfirmName();
						break;
					}
				}
				purchesedClearPaymentList.get(i).setSupplierId(supplierfirmName);
			}

			// Collections.sort(purchesedList, new MaterialsPurchased());
			model.addAttribute("purchesedList", purchesedList);	 
			model.addAttribute("purchesedClearPaymentList", purchesedClearPaymentList);

			return "SupplierPaymentBill";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping("/AddSupplierBillPayment")
	public String AddContractorWorkPayment(@RequestParam("materialsPurchasedId") String materialsPurchasedId, ModelMap model)
	{
		try {
			List<ChartofAccount> chartofaccountList = chartofaccountRepository.findAll();
			List<Store> storeList = storeRepository.findAll();
			List<Supplier> supplierList = supplierRepository.findAll();
			String challanNo="";
			try {
				Query query = new Query();
				List<MaterialsPurchased> purchesedList = mongoTemplate.find(query.addCriteria(Criteria.where("materialsPurchasedId").is(materialsPurchasedId)),MaterialsPurchased.class);

				query = new Query();
				List<StoreStock> storeStockList = mongoTemplate.find(query.addCriteria(Criteria.where("materialsPurchasedId").is(materialsPurchasedId)),StoreStock.class);
				for(int i=0;i<storeStockList.size();i++)
				{
					if(i==0)
					{
						challanNo=""+storeStockList.get(i).getChallanNo();
					}
					else
					{
						challanNo=challanNo+" / "+storeStockList.get(i).getChallanNo();
					}
				}
				query = new Query();
				List<SupplierBillPayment> SupplierBillPaymentList = mongoTemplate.find(query.addCriteria(Criteria.where("materialsPurchasedId").is(materialsPurchasedId)),SupplierBillPayment.class);
				Double supplierPaidAmount=0.0;
				if(SupplierBillPaymentList.size()!=0)
				{
					supplierPaidAmount=SupplierBillPaymentList.get(0).getTotalAmount();
				}


				String storeName="";
				String supplierfirmName="";
				for(int j=0;j<storeList.size();j++)
				{
					if(purchesedList.get(0).getStoreId().equals(storeList.get(j).getStoreId()))
					{
						storeName=storeList.get(j).getStoreName();
						break;
					}
				}
				purchesedList.get(0).setStoreName(storeName);

				for(int j=0;j<storeList.size();j++)
				{
					if(purchesedList.get(0).getSupplierId().equals(supplierList.get(j).getSupplierId()))
					{
						supplierfirmName=supplierList.get(j).getSupplierfirmName();
						break;
					}
				}
				purchesedList.get(0).setSupplierfirmName(supplierfirmName);

				query = new Query();
				List<MaterialsPurchasedHistory> purchesedMaterialList = mongoTemplate.find(query.addCriteria(Criteria.where("materialsPurchasedId").is(materialsPurchasedId)),MaterialsPurchasedHistory.class);

				query = new Query();
				List<StoreStockHistory> storeStockHistory = mongoTemplate.find(query.addCriteria(Criteria.where("materialsPurchasedId").is(materialsPurchasedId)),StoreStockHistory.class);

				double previousQuantity;
				double remainingQuantity;
				long previousNoOfPieces=0;
				double total;
				double discountPer;
				double discountAmount;
				double gstPer;
				double gstAmount;
				double grandTotal;
				for(int i=0;i<purchesedMaterialList.size();i++)
				{
					try
					{
						query = new Query();
						Item itemDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("itemId").is(purchesedMaterialList.get(i).getItemId())), Item.class);
						
						query = new Query();
						SubSupplierType subsuppliertypeDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("subsuppliertypeId").is(itemDetails.getSubsuppliertypeId())), SubSupplierType.class);
						purchesedMaterialList.get(i).setSubsuppliertypeName(subsuppliertypeDetails.getSubsupplierType());
					}
					catch (Exception e) {
						// TODO: handle exception
					}
					
					try {
						previousQuantity=0.0;
						remainingQuantity=0.0;
						previousNoOfPieces=0;
						total=0.0;
						discountAmount=0.0;
						gstAmount=0.0;
						for(int j=0;j<storeStockHistory.size();j++)
						{
							if(storeStockHistory.get(j).getItemId().equalsIgnoreCase(purchesedMaterialList.get(i).getItemId()))
							{
								previousQuantity=previousQuantity+storeStockHistory.get(j).getItemQuantity();
								previousNoOfPieces=previousNoOfPieces+storeStockHistory.get(j).getNoOfPieces();

							}
						}


						remainingQuantity=purchesedMaterialList.get(i).getItemQuantity()-previousQuantity;
						purchesedMaterialList.get(i).setPreviousQuantity(previousQuantity);
						purchesedMaterialList.get(i).setPreviousNoOfPieces(previousNoOfPieces);
						purchesedMaterialList.get(i).setRemainingQuantity(remainingQuantity);

						purchesedMaterialList.get(i).setItemQuantity(previousQuantity);
						total=previousQuantity*purchesedMaterialList.get(i).getRate();
						discountAmount=(total/100)*purchesedMaterialList.get(i).getDiscountPer();
						gstAmount=((total-discountAmount)/100)*purchesedMaterialList.get(i).getGstPer();
						grandTotal=total-discountAmount+gstAmount;

						purchesedMaterialList.get(i).setTotal(total);
						purchesedMaterialList.get(i).setDiscountAmount(discountAmount);
						purchesedMaterialList.get(i).setGstAmount(gstAmount);
						purchesedMaterialList.get(i).setGrandTotal(grandTotal);
					}catch (Exception e) {
						// TODO: handle exception
					}	
					
				}


				query = new Query();
				List<Supplier> supplier = mongoTemplate.find(query.addCriteria(Criteria.where("supplierId").is(purchesedList.get(0).getSupplierId())),Supplier.class);

				query = new Query();
				List<MaterialPurchaseRequisition> materialpurchaseRequisitionList = mongoTemplate.find(query.addCriteria(Criteria.where("requisitionId").is(purchesedList.get(0).getRequisitionId())),MaterialPurchaseRequisition.class);

				query = new Query();
				List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(materialpurchaseRequisitionList.get(0).getProjectId())), Project.class);

				query = new Query();
				List<Company> companyDetails = mongoTemplate.find(query.addCriteria(Criteria.where("companyId").is(projectDetails.get(0).getCompanyId())), Company.class);
				try
				{
					query = new Query();
					List<CompanyBanks> companyBankDetails = mongoTemplate.find(query.addCriteria(Criteria.where("companyId").is(companyDetails.get(0).getCompanyId())), CompanyBanks.class);
					model.addAttribute("companyBankDetails", companyBankDetails);
				}
				catch (Exception e) {
					// TODO: handle exception
				}
				model.addAttribute("chartofaccountList", chartofaccountList); 
				model.addAttribute("supplierPaidAmount", supplierPaidAmount.longValue());
				model.addAttribute("firmpanNumber", supplier.get(0).getFirmpanNumber());
				model.addAttribute("firmgstNumber", supplier.get(0).getFirmgstNumber()); 


				model.addAttribute("challanNo", challanNo);
				model.addAttribute("supplierBillPaymentHistoriId", SupplierBillPaymentHistoriId());
				model.addAttribute("purchesedMaterialList", purchesedMaterialList);
				model.addAttribute("purchesedList", purchesedList);
			}
			catch (Exception e) {
				// TODO: handle exception
			}
			return "AddSupplierBillPayment";

		}catch (Exception e) {
			return "login";
		}
	}



	@RequestMapping(value = "/AddSupplierBillPayment", method = RequestMethod.POST)
	public String AddSupplierBillPayment(@ModelAttribute SupplierBillPaymentHistory supplierbillpaymenthistory, HttpServletRequest req, Model model)
	{
		try {
			try
			{
				supplierbillpaymenthistory = new SupplierBillPaymentHistory(supplierbillpaymenthistory.getSupplierBillPaymentHistoriId(), supplierbillpaymenthistory.getMaterialsPurchasedId(), 
						supplierbillpaymenthistory.getSupplierId(), supplierbillpaymenthistory.getStoreId(),
						supplierbillpaymenthistory.getCompanyBankId(),supplierbillpaymenthistory.getAmount(),supplierbillpaymenthistory.getPaymentMode(),
						supplierbillpaymenthistory.getRefNumber(), supplierbillpaymenthistory.getNarration(), supplierbillpaymenthistory.getChartaccountId(), supplierbillpaymenthistory.getSubchartaccountId(), 
						supplierbillpaymenthistory.getCreationDate(), supplierbillpaymenthistory.getUpdateDate(), supplierbillpaymenthistory.getUserName());

				supplierbillpaymenthistoryRepository.save(supplierbillpaymenthistory);
			}
			catch(DuplicateKeyException de)
			{
			}
			try
			{
				String userName = (String)req.getSession().getAttribute("user");
				BankTransactionHistoryDetails banktransactionhistorydetails=new BankTransactionHistoryDetails();

				banktransactionhistorydetails.setCompanyBankId(supplierbillpaymenthistory.getCompanyBankId());
				banktransactionhistorydetails.setAmount(supplierbillpaymenthistory.getAmount());
				banktransactionhistorydetails.setPaymentType("Supplier");
				banktransactionhistorydetails.setPaymentId(supplierbillpaymenthistory.getSupplierId());
				banktransactionhistorydetails.setCreditOrDebiteType("Debited");
				banktransactionhistorydetails.setCreationDate(new Date());
				banktransactionhistorydetails.setUserName(userName);
				banktransactionhistorydetailsRepository.save(banktransactionhistorydetails);
			}
			catch (Exception e) {
				// TODO: handle exception
			}


			Query query = new Query();
			List<SupplierBillPayment> supplierbillpaymentList = mongoTemplate.find(query.addCriteria(Criteria.where("materialsPurchasedId").is(supplierbillpaymenthistory.getMaterialsPurchasedId())),SupplierBillPayment.class);

			SupplierBillPayment supplierbillpayment=new SupplierBillPayment();
			Double totalAmount=0.0;
			try
			{
				if(supplierbillpaymentList.size()==0)
				{
					supplierbillpayment.setSupplierBillPaymentId(SupplierBillPaymentId());
					supplierbillpayment.setMaterialsPurchasedId(supplierbillpaymenthistory.getMaterialsPurchasedId());
					supplierbillpayment.setSupplierId(supplierbillpaymenthistory.getSupplierId());
					supplierbillpayment.setStoreId(supplierbillpaymenthistory.getStoreId());
					supplierbillpayment.setTotalAmount(supplierbillpaymenthistory.getAmount());
					supplierbillpaymentRepository.save(supplierbillpayment);
				}
				else
				{

					supplierbillpayment = mongoTemplate.findOne(Query.query(Criteria.where("materialsPurchasedId").is(supplierbillpaymenthistory.getMaterialsPurchasedId())), SupplierBillPayment.class);
					totalAmount=supplierbillpayment.getTotalAmount()+supplierbillpaymenthistory.getAmount();
					supplierbillpayment.setTotalAmount(totalAmount);
					supplierbillpaymentRepository.save(supplierbillpayment);
				}
			}
			catch (Exception e) {
			}

			query = new Query();
			List<CompanyAccountPaymentDetails> companyBankAccount = mongoTemplate.find(query.addCriteria(Criteria.where("companyBankId").is(supplierbillpaymenthistory.getCompanyBankId())),CompanyAccountPaymentDetails.class);
			CompanyAccountPaymentDetails companyaccountpaymentDetails=new CompanyAccountPaymentDetails();
			try {
				if(companyBankAccount.size()==0)
				{

					companyaccountpaymentDetails.setPaymentId(GeneratePaymentId());
					companyaccountpaymentDetails.setCompanyBankId(supplierbillpaymenthistory.getCompanyBankId());
					companyaccountpaymentDetails.setCurrentBalance(supplierbillpaymenthistory.getAmount().longValue());

					companyaccountpaymentdetailsRepository.save(companyaccountpaymentDetails);
				}
				else
				{
					long currentBalance;

					companyaccountpaymentDetails = mongoTemplate.findOne(Query.query(Criteria.where("companyBankId").is(supplierbillpaymenthistory.getCompanyBankId())), CompanyAccountPaymentDetails.class);
					currentBalance=companyaccountpaymentDetails.getCurrentBalance()-supplierbillpaymenthistory.getAmount().longValue();
					companyaccountpaymentDetails.setCurrentBalance(currentBalance);

					companyaccountpaymentdetailsRepository.save(companyaccountpaymentDetails);
				}
			}
			catch (Exception e) {
			}

			query = new Query();
			List<MaterialsPurchased> purchesedList = mongoTemplate.find(query.addCriteria(Criteria.where("materialsPurchasedId").is(supplierbillpaymenthistory.getMaterialsPurchasedId())),MaterialsPurchased.class);
			if(purchesedList.get(0).getTotalAmount()<=totalAmount)
			{
				MaterialsPurchased materialspurchased=new MaterialsPurchased();
				materialspurchased = mongoTemplate.findOne(Query.query(Criteria.where("materialsPurchasedId").is(supplierbillpaymenthistory.getMaterialsPurchasedId())), MaterialsPurchased.class);
				materialspurchased.setPaymentStatus("Clear");
				materialspurchasedRepository.save(materialspurchased);

			}


			//back SupplierPaymentBill page
			List<Store> storeList = storeRepository.findAll();
			List<Supplier> supplierList = supplierRepository.findAll();

			query = new Query();
			List<MaterialsPurchased> purchesedList1 = mongoTemplate.find(query.addCriteria(Criteria.where("paymentStatus").ne("Clear")),MaterialsPurchased.class);

			String storeName="";
			String supplierfirmName="";
			for(int i=0;i<purchesedList1.size();i++)
			{
				for(int j=0;j<storeList.size();j++)
				{
					if(purchesedList1.get(i).getStoreId().equals(storeList.get(j).getStoreId()))
					{
						storeName=storeList.get(j).getStoreName();
						break;
					}
				}
				purchesedList1.get(i).setStoreId(storeName);

				for(int j=0;j<storeList.size();j++)
				{
					if(purchesedList1.get(i).getSupplierId().equals(supplierList.get(j).getSupplierId()))
					{
						supplierfirmName=supplierList.get(j).getSupplierfirmName();
						break;
					}
				}
				purchesedList1.get(i).setSupplierId(supplierfirmName);
			}

			// list for clear payment status
			query = new Query();
			List<MaterialsPurchased> purchesedClearPaymentList = mongoTemplate.find(query.addCriteria(Criteria.where("paymentStatus").is("Clear")),MaterialsPurchased.class);

			for(int i=0;i<purchesedClearPaymentList.size();i++)
			{
				for(int j=0;j<storeList.size();j++)
				{
					if(purchesedClearPaymentList.get(i).getStoreId().equals(storeList.get(j).getStoreId()))
					{
						storeName=storeList.get(j).getStoreName();
						break;
					}
				}
				purchesedClearPaymentList.get(i).setStoreId(storeName);

				for(int j=0;j<storeList.size();j++)
				{
					if(purchesedClearPaymentList.get(i).getSupplierId().equals(supplierList.get(j).getSupplierId()))
					{
						supplierfirmName=supplierList.get(j).getSupplierfirmName();
						break;
					}
				}
				purchesedClearPaymentList.get(i).setSupplierId(supplierfirmName);
			}

			model.addAttribute("purchesedList", purchesedList1);	 
			model.addAttribute("purchesedClearPaymentList", purchesedClearPaymentList);

			return "SupplierPaymentBill";

		}catch (Exception e) {
			return "login";
		}
	}  


	@RequestMapping("/MaterialPurchaseOrderList")
	public String MaterialPurchaseOrderList(Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<Store> storeList = storeRepository.findAll();
			List<Supplier> supplierList = supplierRepository.findAll();

			Query query = new Query();
			List<MaterialsPurchased> purchesedList = mongoTemplate.find(query.addCriteria(Criteria.where("paymentStatus").ne("Clear")),MaterialsPurchased.class);
	
			String storeName="";
			String supplierfirmName="";
			int count=1;
			for(int i=purchesedList.size()-1;i>=0;i--)
			{
				purchesedList.get(i).setNumber(count);
				count++;
				for(int j=0;j<storeList.size();j++)
				{
					if(purchesedList.get(i).getStoreId().equals(storeList.get(j).getStoreId()))
					{
						storeName=storeList.get(j).getStoreName();
						break;
					}
				}
				purchesedList.get(i).setStoreId(storeName);

				for(int j=0;j<supplierList.size();j++)
				{
					if(purchesedList.get(i).getSupplierId().equals(supplierList.get(j).getSupplierId()))
					{
						supplierfirmName=supplierList.get(j).getSupplierfirmName();
						break;
					}
				}
				purchesedList.get(i).setSupplierId(supplierfirmName);
			}

			model.addAttribute("purchesedList", purchesedList);	 

			return "MaterialPurchaseOrderList";

		}catch (Exception e) {
			return "login";
		}
	}

}

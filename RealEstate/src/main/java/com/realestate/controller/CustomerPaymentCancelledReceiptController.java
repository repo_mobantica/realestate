package com.realestate.controller;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Booking;
import com.realestate.bean.Company;
import com.realestate.bean.CustomerPaymentStatusHistory;
import com.realestate.bean.CustomerReceiptForm;
import com.realestate.bean.Flat;
import com.realestate.bean.Floor;
import com.realestate.bean.Login;
import com.realestate.bean.NumberToWordConversion;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.UserAssignedProject;
import com.realestate.configuration.CommanController;
import com.realestate.repository.ProjectRepository;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;

@Controller
@RequestMapping("/")
public class CustomerPaymentCancelledReceiptController
{
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	ProjectRepository projectRepository;

	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@ResponseBody
	@RequestMapping("/OtherCancelledPaymentReceipt")
	public ResponseEntity<byte[]> OtherCancelledPaymentReceipt(@RequestParam("receiptId") String receiptId, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		int index=0;
		JasperPrint print;
		String customerName="", bankName_Address_ChequeNo_Date="",flatNumber_floorNum_projectName="",gatORserveyNum="",projectAddress="",companyName="";
		try 
		{	
			Query query = new Query();
			Query query1 = new Query();
			Collection c = new ArrayList();
			HashMap jmap = new HashMap();
			jmap.put("index", ""+(index++));
			c.add(jmap);

			jmap = null;
			List<CustomerReceiptForm> paymentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("receiptId").is(receiptId)),CustomerReceiptForm.class);

			CustomerPaymentStatusHistory customerPaymentStatusHistory = new CustomerPaymentStatusHistory();
			query= new Query();
			customerPaymentStatusHistory = mongoTemplate.findOne(query.addCriteria(Criteria.where("receiptId").is(receiptId)), CustomerPaymentStatusHistory.class);

			String bkId = paymentDetails.get(0).getBookingId();

			List<Booking> bookingList = mongoTemplate.find(query1.addCriteria(Criteria.where("bookingId").is(bkId)), Booking.class);


			query = new Query();
			List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(bookingList.get(0).getProjectId())), Project.class);

			query =new Query();
			List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(bookingList.get(0).getBuildingId())), ProjectBuilding.class);

			query =new Query();
			List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(bookingList.get(0).getWingId())), ProjectWing.class);

			query =new Query();
			List<Floor> floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("floorId").is(bookingList.get(0).getFloorId())), Floor.class);

			query = new Query();
			List<Flat> flatDetails = mongoTemplate.find(query.addCriteria(Criteria.where("flatId").is(bookingList.get(0).getFlatId())), Flat.class);

			query = new Query();
			List<Company> companyDetails = mongoTemplate.find(query.addCriteria(Criteria.where("companyId").is(projectDetails.get(0).getCompanyId())), Company.class);


			List<Project> projectList = GetUserAssigenedProjectList(req,res);


			customerName =  " Mr/Mrs. "+bookingList.get(0).getBookingfirstname()+" "+bookingList.get(0).getBookingmiddlename()+" "+bookingList.get(0).getBookinglastname();
			/*		 	
		 	 if(paymentDetails.get(0).getPaymentMode().equals("Cash"))
		 	 {
		 		bankName_Address_ChequeNo_Date = "Payment Mode    :    Cash  "+", Dated  :  "+paymentDetails.get(0).getCreationDate();
		 	 }
		 	 else
		 	 {
		 		bankName_Address_ChequeNo_Date = "Bank : "+paymentDetails.get(0).getBankName()+",   Cheque No : "+paymentDetails.get(0).getChequeNumber()+", Dated : "+paymentDetails.get(0).getCreationDate();	 
		 	 }
			 */

			flatNumber_floorNum_projectName = "Flat no.:"+flatDetails.get(0).getFlatNumber()+", on "+floorDetails.get(0).getFloortypeName()+", Wing: \""+wingDetails.get(0).getWingName()+", Bldg : "+buildingDetails.get(0).getBuildingName()+", In "+projectDetails.get(0).getProjectName()+"\"";

			gatORserveyNum = projectDetails.get(0).getGatORserveyNumber().toString();
			projectAddress = projectList.get(0).getLocationareaId()+", "+projectList.get(0).getCityId()+"-"+projectList.get(0).getAreaPincode().toString();

			companyName = companyDetails.get(0).getCompanyName();


			//String realPath ="//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img"; //context.getRealPath("/resources/dist/img");
			String realPath =CommanController.GetLogoImagePath();
			JRDataSource dataSource = new JRMapCollectionDataSource(c);
			Map<String, Object> parameterMap = new HashMap();

			Date date = new Date();  
			SimpleDateFormat formatter = new SimpleDateFormat("d/M/yyyy");  
			String strDate= formatter.format(date);

			//code to store receipt details in database

			NumberToWordConversion run = new NumberToWordConversion();
			String AmountinWords = run.convertToIndianCurrency(paymentDetails.get(0).getPaymentAmount().toString());

			String cancellationReceiptPaymentData = "";

			/*8*/ parameterMap.put("applicantName_1_2", ""+customerName);
			/*9*/ parameterMap.put("companyName", ""+companyName);
			/*10*/ parameterMap.put("receiptDate", ""+strDate);
			/*11*/ parameterMap.put("receiptNumber",""+paymentDetails.get(0).getReceiptId());
			/*12*/ parameterMap.put("realPath",realPath);


			cancellationReceiptPaymentData = "Sorry, your payment sum of "+paymentDetails.get(0).getPaymentAmount().longValue()+"/- ("+AmountinWords+") has been cancelled due to \""+customerPaymentStatusHistory.getRemark()+"\" wide By";

			try
			{
				if(paymentDetails.get(0).getBankName().equals(""))
				{
					cancellationReceiptPaymentData = cancellationReceiptPaymentData + " Cash ";
				}
				else
				{
					cancellationReceiptPaymentData = cancellationReceiptPaymentData + " Bank Name : "+paymentDetails.get(0).getBankName()+", ";
					try 
					{
						cancellationReceiptPaymentData = cancellationReceiptPaymentData + " Branch : "+paymentDetails.get(0).getBranchName()+", ";
					}
					catch (Exception e) 
					{
						// TODO: handle exception
					}
				}
			}
			catch (Exception e)
			{

			}
			try
			{
				if(paymentDetails.get(0).getPaymentMode().equals("Cash"))
				{

				}	
				if(paymentDetails.get(0).getPaymentMode().equals("Cheque"))
				{
					cancellationReceiptPaymentData = cancellationReceiptPaymentData + " Cheq. No: "+paymentDetails.get(0).getChequeNumber();
				}
				else if(paymentDetails.get(0).getPaymentMode().equals("DD"))
				{
					cancellationReceiptPaymentData = cancellationReceiptPaymentData + " DD No: "+paymentDetails.get(0).getChequeNumber();
				}
				else if(paymentDetails.get(0).getPaymentMode().equals("RTGS"))
				{
					cancellationReceiptPaymentData = cancellationReceiptPaymentData + " RTGS No: "+paymentDetails.get(0).getChequeNumber();
				}
				else if(paymentDetails.get(0).getPaymentMode().equals("NEFT"))
				{
					cancellationReceiptPaymentData = cancellationReceiptPaymentData + " NEFT No: "+paymentDetails.get(0).getChequeNumber();
				}
				else if(paymentDetails.get(0).getPaymentMode().equals("IMPS"))
				{
					cancellationReceiptPaymentData = cancellationReceiptPaymentData + " IMPS No: "+paymentDetails.get(0).getChequeNumber();
				}
				else if(paymentDetails.get(0).getPaymentMode().equals("Online"))
				{
					cancellationReceiptPaymentData = cancellationReceiptPaymentData + " RTGS No: "+paymentDetails.get(0).getChequeNumber();
				}
			}
			catch (Exception e) 
			{

			}

			cancellationReceiptPaymentData = cancellationReceiptPaymentData + " against "+paymentDetails.get(0).getPaymentType()+" for Flat No. "+flatDetails.get(0).getFlatNumber()+" on "+floorDetails.get(0).getFloortypeName()+", Wing : "+wingDetails.get(0).getWingName()+", Bldg : "+buildingDetails.get(0).getBuildingName()+", In "+projectDetails.get(0).getProjectName()+" situated at Gat/Sr No. : "+gatORserveyNum+", Address : "+projectAddress;

			/*13*/ parameterMap.put("cancellationReceiptPaymentData",""+cancellationReceiptPaymentData);
			///*14*/ parameterMap.put("watermark", new WaterMarkRenderer(true));


			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/CancellationReceipt.jasper");

			print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(print, baos);

			byte[] contents = baos.toByteArray();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			String filename = "Payment Cancelled Receipt.pdf";

			JasperExportManager.exportReportToPdfStream(print, baos);


			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

			ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
			res.setHeader("Content-Disposition", "inline; filename=" + filename );

			return resp;					    
		}
		catch(Exception ex)
		{
			return null;
		}

	}
}

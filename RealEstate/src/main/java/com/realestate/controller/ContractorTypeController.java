package com.realestate.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.ContractorType;
import com.realestate.repository.ContractorTypeRepository;

@Controller
@RequestMapping("/")
public class ContractorTypeController {
	@Autowired
	ContractorTypeRepository contractortypeRepository;
	@Autowired
	MongoTemplate mongoTemplate;


	String contractortypeCode;
	private String ContractorTypeCode()
	{
		long cnt  = contractortypeRepository.count();
		if(cnt<10)
		{
			contractortypeCode = "CT000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			contractortypeCode = "CT00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			contractortypeCode = "CT0"+(cnt+1);
		}
		else 
		{
			contractortypeCode = "CT"+(cnt+1);
		}
		return contractortypeCode;
	}


	@RequestMapping("/AddContractorType")
	public String AddContractorType(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			model.addAttribute("contractortypeCode",ContractorTypeCode());

			return "AddContractorType";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping(value = "/AddContractorType", method = RequestMethod.POST)
	public String contractortypeSave(@ModelAttribute ContractorType contractortype, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				contractortype = new ContractorType(contractortype.getContractortypeId(), contractortype.getContractorType(),contractortype.getCreationDate(), contractortype.getUpdateDate(), contractortype.getUserName());
				contractortypeRepository.save(contractortype);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}

			model.addAttribute("contractortypeCode",ContractorTypeCode());
			return "AddContractorType";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping("/ContractorTypeMaster")
	public String contractorTypeMaster(ModelMap model)
	{
		try {
			List<ContractorType> contractortypeList = contractortypeRepository.findAll();
			model.addAttribute("contractortypeList",contractortypeList);

			return "ContractorTypeMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/SearchcontractorTypeListByName")
	public List<ContractorType> SearchItemUnitListByName(@RequestParam("contractorType") String contractorType)
	{
		Query query = new Query();

		List<ContractorType> contractortypeList = mongoTemplate.find(query.addCriteria(Criteria.where("contractorType").regex("^"+contractorType+".*","i")),ContractorType.class);
		return contractortypeList;
	}

	@ResponseBody
	@RequestMapping("/getcontractorTypeList")
	public List<ContractorType> ContractorTypeList(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		List<ContractorType> contractortypeList= contractortypeRepository.findAll();

		return contractortypeList;
	}

	@RequestMapping("/EditContractorType")
	public String EditContractorType(@RequestParam("contractortypeId") String contractortypeId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<ContractorType> contractortypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("contractortypeId").is(contractortypeId)),ContractorType.class);

			model.addAttribute("contractortypeDetails",contractortypeDetails);
			return "EditContractorType";

		}catch (Exception e) {
			return "login";
		}
	}
	@RequestMapping(value="/EditContractorType", method=RequestMethod.POST)
	public String EditContractorTypePostMethod(@ModelAttribute ContractorType contractortype, ModelMap model)
	{
		try {
			try
			{
				contractortype = new ContractorType(contractortype.getContractortypeId(), contractortype.getContractorType(),contractortype.getCreationDate(), contractortype.getUpdateDate(), contractortype.getUserName());
				contractortypeRepository.save(contractortype);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}

			List<ContractorType> contractortypeList = contractortypeRepository.findAll();
			model.addAttribute("contractortypeList",contractortypeList);

			return "ContractorTypeMaster";

		}catch (Exception e) {
			return "login";
		}
	}
}

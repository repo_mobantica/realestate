package com.realestate.controller;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Aggreement;
import com.realestate.bean.Booking;
import com.realestate.bean.CustomerPaymentDetails;
import com.realestate.bean.CustomerPaymentReminderLetterHistory;
import com.realestate.bean.DemandPaymentDetailsReport;
import com.realestate.bean.Employee;
import com.realestate.bean.Flat;
import com.realestate.bean.Login;
import com.realestate.bean.PaymentScheduler;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.UserAssignedProject;
import com.realestate.configuration.CommanController;
import com.realestate.repository.AggreementRepository;
import com.realestate.repository.BookingRepository;
import com.realestate.repository.CityRepository;
import com.realestate.repository.CompanyRepository;
import com.realestate.repository.CountryRepository;
import com.realestate.repository.CustomerPaymentDetailsRepository;
import com.realestate.repository.CustomerReceiptFormRepository;
import com.realestate.repository.EnquirySourceRepository;
import com.realestate.repository.ExtraChargeRepository;
import com.realestate.repository.FlatRepository;
import com.realestate.repository.LocationAreaRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.ReceiptRepository;
import com.realestate.repository.StateRepository;
import com.realestate.repository.CustomerPaymentReminderLetterHistoryRepository;
import com.realestate.services.EmailService;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;

@Controller
@RequestMapping("/")
public class DemandPaymentDetailsReportController extends EmailService {

	@Autowired
	CustomerPaymentReminderLetterHistoryRepository customerpaymentreminderletterhistoryRepository;
	@Autowired
	LocationAreaRepository locationAreaRepository;
	@Autowired
	CityRepository cityRepository;
	@Autowired
	StateRepository stateRepository;
	@Autowired 
	AggreementRepository aggreementRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	EnquirySourceRepository enquirySourceRepository;
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	CompanyRepository companyRepository;
	@Autowired
	ExtraChargeRepository extraChargeRepository;
	@Autowired
	BookingRepository bookingRepository;
	@Autowired
	ReceiptRepository receiptRepository;
	@Autowired
	FlatRepository flatRepository;
	@Autowired
	CustomerReceiptFormRepository customerReceiptFormRepository;
	@Autowired
	CustomerPaymentDetailsRepository customerPaymentDetailsRepository;
	@Autowired
	ServletContext context; 
	@Autowired
	CountryRepository countryRepository;

	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping("/DemandPaymentDetailsReport")
	public String DemandPaymentDetailsReport(Model model, HttpServletRequest req, HttpServletResponse res)
	{
		List<Project> projectList = GetUserAssigenedProjectList(req,res);

		model.addAttribute("projectList", projectList);
		return "DemandPaymentDetailsReport";
	}

	//search Button
	@ResponseBody
	@RequestMapping("/GetDemandPaymentDetailsList")
	public List<DemandPaymentDetailsReport> GetDemandPaymentDetailsList(@RequestParam("projectId") String projectId,@RequestParam("buildingId") String buildingId, @RequestParam("wingId") String wingId)
	{
		List<DemandPaymentDetailsReport> demandPaymentDetailsReport = new ArrayList<DemandPaymentDetailsReport>();
		demandPaymentDetailsReport.clear();
		String customerName="";
		try 
		{	

			Query query = new Query();

			List<Booking> bookingList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId)),Booking.class);
			List<CustomerPaymentDetails> paymentDetails = customerPaymentDetailsRepository.findAll();
			query = new Query();
			List<PaymentScheduler> paymentSchedulerList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("slabStatus").is("Completed")), PaymentScheduler.class);

			double per=0;
			for(int i=0;i<paymentSchedulerList.size();i++)
			{
				per = per + paymentSchedulerList.get(i).getPercentage();
			}

			List<Aggreement> aggreementList1 = aggreementRepository.findAll();

			List<Aggreement> aggreementList = new ArrayList<Aggreement>();

			try
			{
				for(int i=0; i<bookingList.size();i++)
				{
					Aggreement 	a1=new Aggreement();
					query = new Query();
					a1 = mongoTemplate.findOne(query.addCriteria(Criteria.where("bookingId").is(bookingList.get(i).getBookingId())), Aggreement.class);
					if(a1 !=null)
					{
						aggreementList.add(a1);
					}
				}
			}
			catch(Exception wq)
			{
				System.out.println("EXC -"+wq);
			}

			Booking bookingList1;
			for(int i=0;i<aggreementList.size();i++)
			{


				DemandPaymentDetailsReport a = new DemandPaymentDetailsReport();
				query = new Query();  
				bookingList1 = new Booking();
				bookingList1 = mongoTemplate.findOne(query.addCriteria(Criteria.where("bookingId").is(aggreementList.get(i).getBookingId())),Booking.class);

				query = new Query();
				List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(bookingList1.getProjectId())), Project.class);

				query =new Query();
				List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(bookingList1.getBuildingId())), ProjectBuilding.class);

				query =new Query();
				List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(bookingList1.getWingId())), ProjectWing.class);

				query = new Query();
				List<Flat> flatDetails = mongoTemplate.find(query.addCriteria(Criteria.where("flatId").is(bookingList1.getFlatId())), Flat.class);


				a.setBookingId(""+bookingList1.getBookingId());

				a.setCustomerName(""+aggreementList.get(i).getFirstApplicantfirstname()+" "+aggreementList.get(i).getFirstApplicantlastname());
				//a.setCustomerName("gh");
				a.setWingId(""+buildingDetails.get(0).getBuildingName()+"-"+wingDetails.get(0).getWingName());

				a.setFlatId(""+flatDetails.get(0).getFlatNumber());

				a.setAgreementAmount(""+bookingList1.getAggreementValue1().longValue());

				double dueAsPerWork = ((bookingList1.getAggreementValue1()*per)/100);

				a.setDemandAmount(""+(long)dueAsPerWork);

				double totalTillDate = 0.0d;

				for(int j=0;j<paymentDetails.size();j++)
				{
					if(bookingList1.getBookingId().equals(paymentDetails.get(j).getBookingId()))
					{
						totalTillDate = paymentDetails.get(j).getTotalpayAggreement().longValue();
						break;
					}
					else
					{
						totalTillDate = 0.0d;
					}
				}

				a.setPaidTillDate(""+(long)totalTillDate);

				a.setRemainingDemandAmount(""+(long)(dueAsPerWork-totalTillDate));

				a.setRemainingAmount(""+(long)(bookingList1.getAggreementValue1()-totalTillDate));

				demandPaymentDetailsReport.add(a);
				//String realPath ="//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img"; //context.getRealPath("/resources/dist/img");
			}
			return demandPaymentDetailsReport;
		}
		catch(Exception ex)
		{
			return null;
		}
	}

	//print Button Action
	@ResponseBody
	@RequestMapping(value="/DemandPaymentDetailsReport", method=RequestMethod.POST)
	public ResponseEntity<byte[]> GetDemandPaymentDetailsList(HttpServletRequest request, HttpServletResponse response)
	{
		String projectId  = request.getParameter("projectId");
		String buildingId = request.getParameter("buildingId");
		String wingId     = request.getParameter("wingId");
		//String floortypeName = req.getParameter("floortypeName");*/

		if(!projectId.equals("Default") && !buildingId.equals("Default") && !wingId.equals("Default"))
		{
			int index=0;
			JasperPrint print;
			String customerName="";

			long netTotalAmount=0, demandAmountTotal=0, paidAmountTotal=0, totalRemainingDemandAmt=0 ,remainingAmountTotal=0;

			try 
			{	
				Query query = new Query();
				HashMap jmap = new HashMap();
				Collection c = new ArrayList();

				List<Booking> bookingList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId)),Booking.class);

				query = new Query();
				List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(bookingList.get(0).getProjectId())), Project.class);

				query =new Query();
				List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(bookingList.get(0).getBuildingId())), ProjectBuilding.class);

				query =new Query();
				List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(bookingList.get(0).getWingId())), ProjectWing.class);

				query = new Query();
				List<Flat> flatDetails = mongoTemplate.find(query.addCriteria(Criteria.where("flatId").is(bookingList.get(0).getFlatId())), Flat.class);


				List<CustomerPaymentDetails> paymentDetails = customerPaymentDetailsRepository.findAll();

				query = new Query();
				List<PaymentScheduler> paymentSchedulerList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("slabStatus").is("Completed")), PaymentScheduler.class);
				double per=0;
				for(int i=0;i<paymentSchedulerList.size();i++)
				{
					per = per + paymentSchedulerList.get(i).getPercentage();

				}


				List<Aggreement> aggreementList1 = aggreementRepository.findAll();

				List<Aggreement> aggreementList = new ArrayList<Aggreement>();
				try
				{
					for(int i=0; i<bookingList.size();i++)
					{
						Aggreement 	a1=new Aggreement();
						query = new Query();
						a1 = mongoTemplate.findOne(query.addCriteria(Criteria.where("bookingId").is(bookingList.get(i).getBookingId())), Aggreement.class);
						if(a1 !=null)
						{
							aggreementList.add(a1);
						}
					}
				}
				catch(Exception wq)
				{
					System.out.println("EXC -"+wq);
				}


				for(int i=0;i<aggreementList.size();i++)
				{
					List<Booking> bookingList1=new ArrayList<Booking>();
					bookingList1.clear();

					query = new Query();  
					bookingList1 = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(aggreementList.get(i).getBookingId())),Booking.class);

					query = new Query();
					List<Project> projectDetails1 = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(bookingList1.get(0).getProjectId())), Project.class);

					query =new Query();
					List<ProjectBuilding> buildingDetails1 = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(bookingList1.get(0).getBuildingId())), ProjectBuilding.class);

					query =new Query();
					List<ProjectWing> wingDetails1 = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(bookingList1.get(0).getWingId())), ProjectWing.class);

					query = new Query();
					List<Flat> flatDetails1 = mongoTemplate.find(query.addCriteria(Criteria.where("flatId").is(bookingList1.get(0).getFlatId())), Flat.class);


					jmap = new HashMap();
					/*1*/jmap.put("srno",""+(i+1));
					/*2*/jmap.put("bookingId",""+bookingList1.get(0).getBookingId());


					/*3*/jmap.put("customerName",""+aggreementList.get(i).getFirstApplicantfirstname()+" "+aggreementList.get(i).getFirstApplicantlastname());

					/*4*/jmap.put("buildingNameAndWingName",""+buildingDetails1.get(0).getBuildingName()+"-"+wingDetails1.get(0).getWingName());
					/*5*/jmap.put("flatNo",""+flatDetails1.get(0).getFlatNumber());

					/*6*/jmap.put("aggAmt",""+bookingList1.get(0).getGrandTotal1().longValue());
					netTotalAmount = netTotalAmount + bookingList1.get(0).getGrandTotal1().longValue(); 	  

					double dueAsPerWork = ((bookingList1.get(0).getAggreementValue1()*per)/100);
					/*7*/jmap.put("demandAmount",""+(long)dueAsPerWork);
					demandAmountTotal = demandAmountTotal + (long)dueAsPerWork;  

					double totalTillDate = 0.0d;
					for(int j=0;j<paymentDetails.size();j++)
					{
						if(bookingList1.get(0).getBookingId().equals(paymentDetails.get(j).getBookingId()))
						{
							totalTillDate = paymentDetails.get(j).getTotalpayAggreement().longValue();

							break;
						}
						else
						{
							totalTillDate = 0.0d;
						}
					}

					/*8*/jmap.put("paidAmount",""+(long)totalTillDate);
					paidAmountTotal = paidAmountTotal + (long)totalTillDate;

					/*9*/jmap.put("remainingDemandAmt",""+(long)(dueAsPerWork-totalTillDate));

					totalRemainingDemandAmt = totalRemainingDemandAmt + (long)(dueAsPerWork-totalTillDate);

					/*10*/jmap.put("remainingAmount",""+(long)(bookingList1.get(0).getAggreementValue1()-totalTillDate));
					remainingAmountTotal = remainingAmountTotal + (long)(bookingList1.get(0).getAggreementValue1()-totalTillDate);

					c.add(jmap);
					jmap = null;
				}

				//String realPath ="//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img"; //context.getRealPath("/resources/dist/img");
				String realPath =CommanController.GetLogoImagePath();
				
				JRDataSource dataSource = new JRMapCollectionDataSource(c);
				Map<String, Object> parameterMap = new HashMap<String, Object>();

				Date date = new Date();  
				SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
				String strDate= formatter.format(date);

				//Booking Receipt Generation Code
				/*1*/ parameterMap.put("printDate", ""+strDate);
				/*2*/ parameterMap.put("projectName", ""+projectDetails.get(0).getProjectName());
				/*3*/ parameterMap.put("buildingName", ""+buildingDetails.get(0).getBuildingName());
				/*4*/ parameterMap.put("wingName", ""+wingDetails.get(0).getWingName());
				/*5*/ parameterMap.put("netAmountTotal", ""+netTotalAmount);
				/*6*/ parameterMap.put("demandAmountTotal", ""+demandAmountTotal);
				/*7*/ parameterMap.put("paidAmountTotal", ""+paidAmountTotal);
				/*8*/ parameterMap.put("remainingAmountTotal", ""+remainingAmountTotal);
				/*9*/ parameterMap.put("realPath", ""+realPath);		 
				/*10*/ parameterMap.put("totalRemainingDemandAmt",""+totalRemainingDemandAmt);

				InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/DemandPaymentDetailsReport.jasper");

				print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				JasperExportManager.exportReportToPdfStream(print, baos);

				byte[] contents = baos.toByteArray();

				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.parseMediaType("application/pdf"));
				String filename = "WingWiseFlatBookingStatusReport.pdf";

				JasperExportManager.exportReportToPdfStream(print, baos);

				headers.setContentType(MediaType.parseMediaType("application/pdf"));
				headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
				ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
				response.setHeader("Content-Disposition", "inline; filename=" + filename );
				return resp;	
			}
			catch(Exception ex)
			{
				return null;
			}
		}
		/*else if(!projectName.equals("Default") && !buildingName.equals("Default") && wingName.equals("Default"))
   	{
   		int index=0;
   		JasperPrint print;
   		String customerName="";
   		try 
   		{	
   			Query query = new Query();
   			HashMap jmap = new HashMap();
   			Collection c = new ArrayList();

   	   	    List<Booking> bookingList = mongoTemplate.find(query.addCriteria(Criteria.where("projectName").is(projectName).and("buildingName").is(buildingName)),Booking.class);

   	   	    List<CustomerPaymentDetails> paymentDetails = customerPaymentDetailsRepository.findAll();//-----------

   	   	    query = new Query();
   	   	    List<PaymentScheduler> paymentSchedulerList = mongoTemplate.find(query.addCriteria(Criteria.where("projectName").is(projectName).and("buildingName").is(buildingName).and("slabStatus").is("Completed")), PaymentScheduler.class);

	    	   	double per=0;
				for(int i=0;i<paymentSchedulerList.size();i++)
				{
				   per = per + paymentSchedulerList.get(i).getPercentage();
				}

   	   	    List<Aggreement> aggreementList = new ArrayList<Aggreement>();

   	   	    for(int i=0; i<bookingList.size();i++)
   	   	    {
   	   	      query = new Query();
   			  Aggreement a = mongoTemplate.findOne(query.addCriteria(Criteria.where("bookingId").is(bookingList.get(i).getBookingId())), Aggreement.class);
   			  aggreementList.add(a);
   	   	    }


   			for(int i=0;i<bookingList.size();i++)
   			{
   			      jmap = new HashMap();
   			 1jmap.put("srno",""+(i+1));
   			 2jmap.put("bookingId",""+bookingList.get(i).getBookingId());

   				  if(bookingList.get(i).getBookingId().equals(aggreementList.get(i).getBookingId()))
   				  {
   		     3  jmap.put("customerName",""+aggreementList.get(i).getFirstApplicantfirstname()+" "+aggreementList.get(i).getFirstApplicantlastname());
   				  }
   				  else
   				  {
   			   	    jmap.put("customerName","N.A.");
   				  }

   			4 jmap.put("buildingNameAndWingName",""+bookingList.get(i).getBuildingName()+bookingList.get(i).getWingName());
   			5 jmap.put("flatNo",""+bookingList.get(i).getFlatNumber());
   			6 jmap.put("netAmount",""+bookingList.get(i).getGrandTotal1());

   				  double dueAsPerWork = ((bookingList.get(i).getAggreementValue1()*per)/100);
   			7 jmap.put("demandAmount",""+dueAsPerWork);

	    			  double totalTillDate = 0.0d;
					  for(int j=0;j<paymentDetails.size();j++)
					  {
						 if(bookingList.get(i).getBookingId().equals(paymentDetails.get(j).getBookingId()))
						 {
							 totalTillDate = paymentDetails.get(j).getTotalpayAggreement();
							 break;
						 }
						 else
						 {
							 totalTillDate = 0.0d;
						 }
					   }

   			8 jmap.put("paidAmount",""+totalTillDate);


   			9 jmap.put("remainingAmount",""+(bookingList.get(i).getAggreementValue1()-totalTillDate));

   				  c.add(jmap);
   				  jmap = null;
   			 }

   		  	 String realPath ="//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img"; //context.getRealPath("/resources/dist/img");

   			 JRDataSource dataSource = new JRMapCollectionDataSource(c);
   			 Map<String, Object> parameterMap = new HashMap<String, Object>();

   			 Date date = new Date();  
   			 SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
   			 String strDate= formatter.format(date);

   			 //Booking Receipt Generation Code
   			 1 parameterMap.put("printDate", ""+strDate);
   			 2 parameterMap.put("projectName", ""+projectName);
   			 3 parameterMap.put("buildingName", ""+buildingName);
   			 4 parameterMap.put("wingName", ""+wingName);
   			 5 parameterMap.put("netAmountTotal", "Remained");
   			 6 parameterMap.put("demandAmountTotal", "Remained");
   			 7 parameterMap.put("paidAmountTotal", "Remained");
   			 8 parameterMap.put("remainingAmountTotal", "Remained");

   			 InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/DemandPaymentDetailsReport.jasper");

   			 print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

   			 ByteArrayOutputStream baos = new ByteArrayOutputStream();
   			 JasperExportManager.exportReportToPdfStream(print, baos);

   			 byte[] contents = baos.toByteArray();

   			 HttpHeaders headers = new HttpHeaders();
   			 headers.setContentType(MediaType.parseMediaType("application/pdf"));
   			 String filename = "WingWiseFlatBookingStatusReport.pdf";

   			 JasperExportManager.exportReportToPdfStream(print, baos);

   			 headers.setContentType(MediaType.parseMediaType("application/pdf"));
   			 headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
   			 ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
   			 response.setHeader("Content-Disposition", "inline; filename=" + filename );
   			 return resp;	
   	}
   	catch(Exception ex)
   	{
   		 return null;
   	}
     }*/
		else
		{
			return null;
		}
	}


	//Send Mail
	@ResponseBody
	@RequestMapping("/SendMailForDemandPayment")
	public List<DemandPaymentDetailsReport> SendMailForDemandPayment(@RequestParam("projectId") String projectId,@RequestParam("buildingId") String buildingId, @RequestParam("wingId") String wingId)
	{
		List<DemandPaymentDetailsReport> demandPaymentDetailsReport = new ArrayList<DemandPaymentDetailsReport>();
		demandPaymentDetailsReport.clear();
		String customerName="";
		try 
		{	

			Query query = new Query();

			List<Booking> bookingList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId)),Booking.class);
			List<CustomerPaymentDetails> paymentDetails = customerPaymentDetailsRepository.findAll();
			query = new Query();
			List<PaymentScheduler> paymentSchedulerList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("slabStatus").is("Completed")), PaymentScheduler.class);

			double per=0;
			for(int i=0;i<paymentSchedulerList.size();i++)
			{
				per = per + paymentSchedulerList.get(i).getPercentage();
			}

			List<Aggreement> aggreementList1 = aggreementRepository.findAll();

			List<Aggreement> aggreementList = new ArrayList<Aggreement>();

			try
			{
				for(int i=0; i<bookingList.size();i++)
				{
					Aggreement 	a1=new Aggreement();
					query = new Query();
					a1 = mongoTemplate.findOne(query.addCriteria(Criteria.where("bookingId").is(bookingList.get(i).getBookingId())), Aggreement.class);
					if(a1 !=null)
					{
						aggreementList.add(a1);
					}
				}
			}
			catch(Exception wq)
			{
				System.out.println("EXC -"+wq);
			}

			Booking bookingList1;
			for(int i=0;i<aggreementList.size();i++)
			{


				DemandPaymentDetailsReport a = new DemandPaymentDetailsReport();
				query = new Query();  
				bookingList1 = new Booking();
				bookingList1 = mongoTemplate.findOne(query.addCriteria(Criteria.where("bookingId").is(aggreementList.get(i).getBookingId())),Booking.class);

				query = new Query();
				List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(bookingList1.getProjectId())), Project.class);

				query =new Query();
				List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(bookingList1.getBuildingId())), ProjectBuilding.class);

				query =new Query();
				List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(bookingList1.getWingId())), ProjectWing.class);

				query = new Query();
				List<Flat> flatDetails = mongoTemplate.find(query.addCriteria(Criteria.where("flatId").is(bookingList1.getFlatId())), Flat.class);


				a.setBookingId(""+bookingList1.getBookingId());

				a.setCustomerName(""+aggreementList.get(i).getFirstApplicantfirstname()+" "+aggreementList.get(i).getFirstApplicantlastname());
				//a.setCustomerName("gh");
				a.setWingId(""+buildingDetails.get(0).getBuildingName()+"-"+wingDetails.get(0).getWingName());

				a.setFlatId(""+flatDetails.get(0).getFlatNumber());

				a.setAgreementAmount(""+bookingList1.getAggreementValue1().longValue());

				double dueAsPerWork = ((bookingList1.getAggreementValue1()*per)/100);

				a.setDemandAmount(""+(long)dueAsPerWork);

				double totalTillDate = 0.0d;

				for(int j=0;j<paymentDetails.size();j++)
				{
					if(bookingList1.getBookingId().equals(paymentDetails.get(j).getBookingId()))
					{
						totalTillDate = paymentDetails.get(j).getTotalpayAggreement().longValue();
						break;
					}
					else
					{
						//totalTillDate = 0.0d;
					}
				}

				a.setPaidTillDate(""+(long)totalTillDate);

				a.setRemainingDemandAmount(""+(long)(dueAsPerWork-totalTillDate));

				a.setRemainingAmount(""+(long)(bookingList1.getAggreementValue1()-totalTillDate));

				demandPaymentDetailsReport.add(a);
				//String realPath ="//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img"; //context.getRealPath("/resources/dist/img");
			}

			String bookingId="";
			String customerMailId;
			String flatDetails;

			String bookingCustomerName;
			String agreementAmount;
			String demandAmount;
			String paidTillDate;
			String remainingDemandAmount;
			String remainingAmount;

			int reminderCount=1;
			Date todayDate=new Date();

			SimpleDateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");

			String todayDate1=createDateFormat.format(todayDate);
			String creationDate="";
			for(int i=0;i<demandPaymentDetailsReport.size();i++)
			{
				//remainingDemandAmount
				if(Integer.parseInt(demandPaymentDetailsReport.get(i).getRemainingDemandAmount())>0)
				{
					bookingId="";
					customerMailId="";
					flatDetails="";
					bookingCustomerName="";
					agreementAmount="";
					demandAmount="";
					paidTillDate="";
					remainingDemandAmount="";
					remainingAmount="";
					reminderCount=1;
					creationDate="";

					for(int j=0;j<bookingList.size();j++)
					{
						if(demandPaymentDetailsReport.get(i).getBookingId().equalsIgnoreCase(bookingList.get(j).getBookingId()))
						{
							bookingId=bookingList.get(j).getBookingId();
							customerMailId=bookingList.get(j).getBookingEmail();
							bookingCustomerName=bookingList.get(j).getBookingfirstname()+" "+bookingList.get(j).getBookinglastname();

							agreementAmount=demandPaymentDetailsReport.get(i).getAgreementAmount();
							demandAmount=demandPaymentDetailsReport.get(i).getDemandAmount();
							paidTillDate=demandPaymentDetailsReport.get(i).getPaidTillDate();
							remainingDemandAmount=demandPaymentDetailsReport.get(i).getRemainingDemandAmount();
							remainingAmount=demandPaymentDetailsReport.get(j).getRemainingAmount();

							break;
						}
					}


					query = new Query();
					List<CustomerPaymentReminderLetterHistory> customerpaymentreminderletterHistoryList = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId).and("paymentschedulerId").is(paymentSchedulerList.get(paymentSchedulerList.size()-1).getPaymentschedulerId())), CustomerPaymentReminderLetterHistory.class);

					CustomerPaymentReminderLetterHistory customerpaymentreminderletterHistory=new CustomerPaymentReminderLetterHistory();
					if(customerpaymentreminderletterHistoryList.size()==0)
					{
						customerpaymentreminderletterHistory=new CustomerPaymentReminderLetterHistory();
						customerpaymentreminderletterHistory.setBookingId(bookingId);
						customerpaymentreminderletterHistory.setPaymentschedulerId(paymentSchedulerList.get(paymentSchedulerList.size()-1).getPaymentschedulerId());
						customerpaymentreminderletterHistory.setReminderCount(1);
						customerpaymentreminderletterHistory.setStatus("Remaining");
						customerpaymentreminderletterHistory.setCreationDate(new Date());
						customerpaymentreminderletterHistory.setUpdateDate(new Date());
						customerpaymentreminderletterhistoryRepository.save(customerpaymentreminderletterHistory);

					}
					else
					{

						creationDate=createDateFormat.format(customerpaymentreminderletterHistoryList.get(0).getUpdateDate());

						reminderCount=1+customerpaymentreminderletterHistoryList.get(0).getReminderCount();

						if(!creationDate.equals(todayDate1))
						{
							customerpaymentreminderletterHistoryList.get(0).setReminderCount(reminderCount);
							customerpaymentreminderletterHistoryList.get(0).setUpdateDate(new Date());
							customerpaymentreminderletterhistoryRepository.save(customerpaymentreminderletterHistoryList.get(0));
						}

					}

					if(!customerMailId.equals(""))
					{

						try
						{
							String Msgbody1 = "Dear,  "+bookingCustomerName;
							String Msgbody2 = "\n The next payment Damand is as below";
							String Msgbody3 = "\n Total Flat Agreement Amount : "+agreementAmount;
							String Msgbody4 =  "\n Total Demand Amount : "+demandAmount;
							String Msgbody5 =  "\n Total Paid Amount : "+paidTillDate;
							String Msgbody6 =  "\n Total remaining Demand Amount : "+remainingDemandAmount;

							String emilId = customerMailId;

							sendLoginCredentialsMail(emilId,"Demand Letter", Msgbody1+"\n\t"+Msgbody2+"\n"+Msgbody3+"\n"+Msgbody4+"\n"+Msgbody5+"\n"+Msgbody6);
						}
						catch(Exception e)
						{
							System.out.println("Mail Sending Exception = "+e.toString());
						}
					}

				}


			}


			return null;
		}
		catch(Exception ex)
		{
			return null;
		}
	}


}

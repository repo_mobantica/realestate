package com.realestate.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.realestate.bean.Bank;
import com.realestate.bean.City;
import com.realestate.bean.Country;
import com.realestate.bean.LocationArea;
import com.realestate.bean.State;

@Controller
@RequestMapping("/")
public class BankNocController {

	//Request Mapping For  Bank Master
	@RequestMapping("/BanckNocList")
	public String BankMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		/*
    	List<Bank> bankList = bankRepository.findAll(); 


    	model.addAttribute("countryList", findAllCountryName());
    	model.addAttribute("bankList",bankList);
		 */

		return "BanckNocList";
	}

}

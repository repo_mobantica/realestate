package com.realestate.controller;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Booking;
import com.realestate.bean.Flat;
import com.realestate.bean.Floor;
import com.realestate.bean.Login;
import com.realestate.bean.ParkingZone;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.UserAssignedProject;
import com.realestate.configuration.CommanController;
import com.realestate.repository.BookingRepository;
import com.realestate.repository.ProjectRepository;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;

@Controller
@RequestMapping("/")
public class WingWiseParkingReportController 
{
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	BookingRepository bookingRepository;
	@Autowired
	ProjectRepository projectRepository;

	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping("/WingWiseParkingReport")
	private String WingWiseParkingReport(Model model, HttpServletRequest req,HttpServletResponse res)
	{
		List<Project> projectList = GetUserAssigenedProjectList(req,res);

		Query query = new Query();
		List<Booking> parkingList = bookingRepository.findAll(); 

		for(int i=0;i<parkingList.size();i++)
		{
			try
			{
				Flat flatDetails = new Flat();
				ParkingZone parkingZoneDetails = new ParkingZone();
				Floor floorDetails = new Floor();
				ProjectWing wingDetails = new ProjectWing();
				ProjectBuilding buildingDetails = new ProjectBuilding();
				Project projectDetails = new Project();

				query = new Query();
				flatDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("flatId").is(parkingList.get(i).getFlatId())), Flat.class);

				query = new Query();
				parkingZoneDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("parkingZoneId").is(parkingList.get(i).getParkingZoneId())), ParkingZone.class);

				query = new Query();
				floorDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("floorId").is(parkingList.get(i).getFloorId())), Floor.class);

				query = new Query();
				wingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(parkingList.get(i).getWingId())), ProjectWing.class);

				query = new Query();
				buildingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(parkingList.get(i).getBuildingId())), ProjectBuilding.class);

				query = new Query();
				projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(parkingList.get(i).getProjectId())), Project.class);

				if(flatDetails != null)
				{
					parkingList.get(i).setFlatId(""+flatDetails.getFlatNumber());	
				}
				else
				{
					parkingList.get(i).setFlatId("N.A.");
				}

				if(floorDetails != null)
				{
					parkingList.get(i).setParkingFloorId(""+floorDetails.getFloortypeName());	
				}
				else
				{
					parkingList.get(i).setParkingFloorId("N.A.");
				}

				if(parkingZoneDetails != null)
				{
					parkingList.get(i).setParkingZoneId(parkingZoneDetails.getParkingNumber().toString());	
				}
				else
				{
					parkingList.get(i).setParkingZoneId("N.A.");
				}

				if(wingDetails != null)
				{
					parkingList.get(i).setWingId(""+wingDetails.getWingName());	
				}
				else
				{
					parkingList.get(i).setWingId("N.A.");
				}

				if(buildingDetails != null)
				{
					parkingList.get(i).setBuildingId(""+buildingDetails.getBuildingName());	
				}
				else
				{
					parkingList.get(i).setBuildingId("N.A.");	
				}

				if(projectDetails != null)
				{
					parkingList.get(i).setProjectId(""+projectDetails.getProjectName());	
				}
				else
				{
					parkingList.get(i).setProjectId("N.A.");	
				}

			}
			catch(Exception e)
			{
				System.out.println("Exception = "+e.toString());
			}
		}

		model.addAttribute("parkingList", parkingList);
		model.addAttribute("projectList", projectList);

		return "WingWiseParkingReport";	
	}

	@ResponseBody
	@RequestMapping("/ParkingReportByWing")
	public List<Booking> ParkingReportByWing(@RequestParam("projectId") String projectId, @RequestParam("buildingId") String buildingId, @RequestParam("wingId") String wingId)
	{
		Query query = new Query();

		query = new Query();
		ProjectWing wingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(wingId)), ProjectWing.class);

		query = new Query();
		ProjectBuilding buildingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(buildingId)), ProjectBuilding.class);

		query = new Query();
		Project projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectId)), Project.class);

		List<Booking> parkingList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId)), Booking.class);

		for(int i=0;i<parkingList.size();i++)
		{
			try
			{
				Flat flatDetails = new Flat();
				ParkingZone parkingZoneDetails = new ParkingZone();
				Floor floorDetails = new Floor();

				query = new Query();
				flatDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("flatId").is(parkingList.get(i).getFlatId())), Flat.class);

				query = new Query();
				parkingZoneDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("parkingZoneId").is(parkingList.get(i).getParkingZoneId())), ParkingZone.class);

				query = new Query();
				floorDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("floorId").is(parkingList.get(i).getParkingFloorId())), Floor.class);

				if(flatDetails != null)
				{
					parkingList.get(i).setFlatId(""+flatDetails.getFlatNumber());	
				}
				else
				{
					parkingList.get(i).setFlatId("N.A.");
				}

				if(floorDetails != null)
				{
					parkingList.get(i).setParkingFloorId(""+floorDetails.getFloortypeName());	
				}
				else
				{
					parkingList.get(i).setParkingFloorId("N.A.");
				}

				if(parkingZoneDetails != null)
				{
					parkingList.get(i).setParkingZoneId(parkingZoneDetails.getParkingNumber().toString());	
				}
				else
				{
					parkingList.get(i).setParkingZoneId("N.A.");
				}

				if(wingDetails != null)
				{
					parkingList.get(i).setWingId(""+wingDetails.getWingName());	
				}
				else
				{
					parkingList.get(i).setWingId("N.A.");
				}

				if(buildingDetails != null)
				{
					parkingList.get(i).setBuildingId(""+buildingDetails.getBuildingName());	
				}
				else
				{
					parkingList.get(i).setBuildingId("N.A.");	
				}

				if(projectDetails != null)
				{
					parkingList.get(i).setProjectId(""+projectDetails.getProjectName());	
				}
				else
				{
					parkingList.get(i).setProjectId("N.A.");	
				}
			}
			catch(Exception e)
			{
				System.out.println("Exception = "+e.toString());
			}
		}

		return parkingList;
	}

	@SuppressWarnings({ "unchecked", "unused", "rawtypes" })
	@ResponseBody
	@RequestMapping("/PrintWingWiseParkingReport")
	public ResponseEntity<byte[]> PrintWingWiseParkingReport(@RequestParam("projectId") String projectId, @RequestParam("buildingId") String buildingId, @RequestParam("wingId") String wingId, HttpServletRequest req, ModelMap model, HttpServletResponse response)
	{
		try 
		{	
			JasperPrint print;
			Query query = new Query();
			HashMap jmap = new HashMap();
			Collection c = new ArrayList();

			query = new Query();
			ProjectWing wingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(wingId)), ProjectWing.class);

			query = new Query();
			ProjectBuilding buildingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(buildingId)), ProjectBuilding.class);

			query = new Query();
			Project projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectId)), Project.class);

			List<Booking> parkingList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId)), Booking.class);

			if (parkingList.size() != 0) 
			{
				for (int i = 0; i < parkingList.size(); i++) 
				{
					Flat flatDetails = new Flat();
					ParkingZone parkingZoneDetails = new ParkingZone();
					Floor floorDetails = new Floor();

					query = new Query();
					jmap = new HashMap();

					query = new Query();
					flatDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("flatId").is(parkingList.get(i).getFlatId())), Flat.class);

					query = new Query();
					floorDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("floorId").is(parkingList.get(i).getParkingFloorId())), Floor.class);

					query = new Query();
					parkingZoneDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("parkingZoneId").is(parkingList.get(i).getParkingZoneId())), ParkingZone.class); 

					jmap.put("srno", "" + (i + 1));
					jmap.put("bookingId", "" +parkingList.get(i).getBookingId());
					jmap.put("custName", ""+parkingList.get(i).getBookingfirstname()+" "+parkingList.get(i).getBookingmiddlename()+" "+parkingList.get(i).getBookinglastname());

					if(flatDetails != null)
					{
						jmap.put("flatNumber", ""+flatDetails.getFlatNumber());
					}
					else
					{
						jmap.put("flatNumber", "N.A.");
					}

					if(floorDetails!=null)
					{	
						jmap.put("parkingFloor", ""+floorDetails.getFloortypeName());
					}
					else
					{
						jmap.put("parkingFloor", "N.A.");
					}

					if(parkingZoneDetails!=null)
					{
						jmap.put("parkingNumber", ""+parkingZoneDetails.getParkingNumber());
					}
					else
					{
						jmap.put("parkingNumber", "N.A.");	
					}

					c.add(jmap);
					jmap = null;
				}
			} 
			else 
			{
				jmap = new HashMap();
				jmap.put("srno", "N.A.");
				jmap.put("bookingId", "N.A.");
				jmap.put("custName", "N.A.");
				jmap.put("flatNumber", "N.A.");
				jmap.put("parkingFloor", "N.A.");
				jmap.put("parkingNumber", "N.A.");

				c.add(jmap);
				jmap = null;
			}

			String realPath =CommanController.GetLogoImagePath();
			// String realPath ="//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img";
			//context.getRealPath("/resources/dist/img");

			JRDataSource dataSource = new JRMapCollectionDataSource(c);
			Map<String, Object> parameterMap = new HashMap<String, Object>();

			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			String strDate = formatter.format(date);

			/* 1 */ parameterMap.put("printDate", "" + strDate);
			/* 2 */ parameterMap.put("projectName", ""+projectDetails.getProjectName());
			/* 3 */ parameterMap.put("buildingName", ""+buildingDetails.getBuildingName());
			/* 4 */ parameterMap.put("wingName", ""+wingDetails.getWingName());

			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/WingWiseParkingAllotemjentReport.jasper");

			print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(print, baos);

			byte[] contents = baos.toByteArray();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			String filename = "Wing Wise Parking Allotment Report.pdf";

			JasperExportManager.exportReportToPdfStream(print, baos);

			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
			ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
			response.setHeader("Content-Disposition", "inline; filename=" + filename);
			return resp;
		} 
		catch (Exception ex) 
		{
			return null;
		}
	}
}

package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Bank;
import com.realestate.bean.City;
import com.realestate.bean.Country;
import com.realestate.bean.Designation;
import com.realestate.bean.LocationArea;
import com.realestate.bean.State;
import com.realestate.bean.SubSupplierType;
import com.realestate.bean.Supplier;
import com.realestate.bean.SupplierEmployees;
import com.realestate.bean.SupplierType;
import com.realestate.repository.BankRepository;
import com.realestate.repository.CityRepository;
import com.realestate.repository.CountryRepository;
import com.realestate.repository.DesignationRepository;
import com.realestate.repository.LocationAreaRepository;
import com.realestate.repository.StateRepository;
import com.realestate.repository.SupplierEmployeeRepository;
import com.realestate.repository.SupplierRepository;
import com.realestate.repository.SupplierTypeRepository;

@Controller
@RequestMapping("/")
public class SupplierController
{

	@Autowired
	SupplierRepository supplierRepository;
	@Autowired
	SupplierEmployeeRepository supplierEmployeesRepository;
	@Autowired
	LocationAreaRepository locationAreaRepository;
	@Autowired
	CityRepository cityRepository;
	@Autowired
	StateRepository stateRepository;
	@Autowired
	CountryRepository countryRepository;
	@Autowired
	SupplierTypeRepository suppliertypeRepository;
	@Autowired
	BankRepository bankRepository;
	@Autowired
	DesignationRepository designationRepository;


	/*@Autowired
	BankRepository bankRepository;
	 */
	@Autowired
	MongoTemplate mongoTemplate;

	String supplierCode;
	private String SupplierCode()
	{
		long cnt  = supplierRepository.count();
		if(cnt<10)
		{
			supplierCode = "SP000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			supplierCode = "SP00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			supplierCode = "SP0"+(cnt+1);
		}
		else 
		{
			supplierCode = "SP"+(cnt+1);
		}
		return supplierCode;
	}

	//Code For Retrieving Country Names
	private List<Country> findAllCountryName()
	{
		List<Country> countryList;

		countryList = countryRepository.findAll(new Sort(Sort.Direction.ASC,"countryId"));
		return countryList;
	}

	//Code For Retrieving Supplier Type List
	private List<SupplierType> findAllSupplierType()
	{
		List<SupplierType> suppliertypeList;

		suppliertypeList = suppliertypeRepository.findAll(new Sort(Sort.Direction.ASC,"supplierType"));
		return suppliertypeList;
	}

	//code for getting all bank names
	private List<Bank> getAllBankName()
	{
		List<Bank> bankList= new ArrayList<Bank>();
		Query query = new Query();
		bankList = bankRepository.findAll(new Sort(Sort.Direction.ASC,"bankName"));
		return bankList;
	}

	private List<Designation> getAllDesignation()
	{
		List<Designation> designationList = designationRepository.findAll(new Sort(Sort.Direction.ASC,"designationName"));

		return designationList;
	}

	@RequestMapping("/SupplierMaster")
	public String SupplierMaster(Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<Supplier> supplierList = supplierRepository.findAll(); 

			int j=0,i=0;
			Query query = new Query();
			for(i=0;i<supplierList.size();i++)
			{
				try
				{
					query = new Query();
					List<SupplierType> suppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("suppliertypeId").is(supplierList.get(i).getSuppliertypeId())), SupplierType.class); 
					supplierList.get(i).setSuppliertypeId(suppliertypeDetails.get(0).getSupplierType());
					query = new Query();
					List<SubSupplierType> subsuppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subsuppliertypeId").is(supplierList.get(i).getSubsuppliertypeId())), SubSupplierType.class); 
					supplierList.get(i).setSubsuppliertypeId(subsuppliertypeDetails.get(0).getSubsupplierType());

				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}

			model.addAttribute("countryList",findAllCountryName());
			model.addAttribute("supplierList",supplierList);

			return "SupplierMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/getCountryWiseSupplierList")//used
	public List<Supplier> CountryWiseSupplierList(@RequestParam("countryId") String countryId)
	{
		Query query =new Query();
		List<Supplier> supplierList = mongoTemplate.find(query.addCriteria(Criteria.where("countryId").is(countryId)), Supplier.class);

		int j=0,i=0;
		query = new Query();
		for(i=0;i<supplierList.size();i++)
		{
			try
			{
				query = new Query();
				List<SupplierType> suppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("suppliertypeId").is(supplierList.get(i).getSuppliertypeId())), SupplierType.class); 
				supplierList.get(i).setSuppliertypeId(suppliertypeDetails.get(0).getSupplierType());
				query = new Query();
				List<SubSupplierType> subsuppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subsuppliertypeId").is(supplierList.get(i).getSubsuppliertypeId())), SubSupplierType.class); 
				supplierList.get(i).setSubsuppliertypeId(subsuppliertypeDetails.get(0).getSubsupplierType());

			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		return supplierList;
	}

	@ResponseBody   //used
	@RequestMapping("/getStateWiseSupplierList")
	public List<Supplier> StateWiseSupplierList(@RequestParam("countryId") String countryId , @RequestParam("stateId") String stateId)
	{
		Query query =new Query();
		List<Supplier> supplierList = mongoTemplate.find(query.addCriteria(Criteria.where("countryId").is(countryId).and("stateId").is(stateId)), Supplier.class);

		int j=0,i=0;
		query = new Query();
		for(i=0;i<supplierList.size();i++)
		{
			try
			{
				query = new Query();
				List<SupplierType> suppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("suppliertypeId").is(supplierList.get(i).getSuppliertypeId())), SupplierType.class); 
				supplierList.get(i).setSuppliertypeId(suppliertypeDetails.get(0).getSupplierType());
				query = new Query();
				List<SubSupplierType> subsuppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subsuppliertypeId").is(supplierList.get(i).getSubsuppliertypeId())), SubSupplierType.class); 
				supplierList.get(i).setSubsuppliertypeId(subsuppliertypeDetails.get(0).getSubsupplierType());

			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		return supplierList;
	}

	@ResponseBody   //used
	@RequestMapping("/getCityWiseSupplierList")
	public List<Supplier> CityWiseSupplierList(@RequestParam("countryId") String countryId , @RequestParam("stateId") String stateId , @RequestParam("cityId") String cityId)
	{
		Query query =new Query();
		List<Supplier> supplierList = mongoTemplate.find(query.addCriteria(Criteria.where("countryId").is(countryId).and("stateId").is(stateId).and("cityId").is(cityId)), Supplier.class);

		List<LocationArea> locationareaList= locationAreaRepository.findAll();
		List<City> cityList = cityRepository.findAll();
		List<State> stateList= stateRepository.findAll();
		List<Country> countryList= countryRepository.findAll();
		int j=0,i=0;
		query = new Query();
		for(i=0;i<supplierList.size();i++)
		{
			try
			{
				query = new Query();
				List<SupplierType> suppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("suppliertypeId").is(supplierList.get(i).getSuppliertypeId())), SupplierType.class); 
				supplierList.get(i).setSuppliertypeId(suppliertypeDetails.get(0).getSupplierType());
				query = new Query();
				List<SubSupplierType> subsuppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subsuppliertypeId").is(supplierList.get(i).getSubsuppliertypeId())), SubSupplierType.class); 
				supplierList.get(i).setSubsuppliertypeId(subsuppliertypeDetails.get(0).getSubsupplierType());

			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}


		return supplierList;
	}

	@ResponseBody //used
	@RequestMapping("/getLocationAreaWiseSupplierList")
	public List<Supplier> LocationAreaWiseSupplierList(@RequestParam("countryId") String countryId , @RequestParam("stateId") String stateId , @RequestParam("cityId") String cityId , @RequestParam("locationareaId") String locationareaId)
	{
		Query query =new Query();
		List<Supplier> supplierList = mongoTemplate.find(query.addCriteria(Criteria.where("countryId").is(countryId).and("stateId").is(stateId).and("cityId").is(cityId).and("locationareaId").is(locationareaId)), Supplier.class);

		List<LocationArea> locationareaList= locationAreaRepository.findAll();
		List<City> cityList = cityRepository.findAll();
		List<State> stateList= stateRepository.findAll();
		List<Country> countryList= countryRepository.findAll();
		int j=0,i=0;
		query = new Query();
		for(i=0;i<supplierList.size();i++)
		{
			try
			{
				query = new Query();
				List<SupplierType> suppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("suppliertypeId").is(supplierList.get(i).getSuppliertypeId())), SupplierType.class); 
				supplierList.get(i).setSuppliertypeId(suppliertypeDetails.get(0).getSupplierType());
				query = new Query();
				List<SubSupplierType> subsuppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subsuppliertypeId").is(supplierList.get(i).getSubsuppliertypeId())), SubSupplierType.class); 
				supplierList.get(i).setSubsuppliertypeId(subsuppliertypeDetails.get(0).getSubsupplierType());

			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}


		return supplierList;			 
	}

	@ResponseBody
	@RequestMapping("/searchNameWiseSupplierList")
	public List<Supplier> NameWiseSearch(@RequestParam("supplierName") String supplierName)
	{
		List<Supplier> supplierList;

		Query query =new Query();
		//supplierList = mongoTemplate.find(query.addCriteria(Criteria.where("supplierfirstName").regex("^"+supplierName+".*","i").orOperator(Criteria.where("suppliermiddleName").regex("^"+supplierName+".*","i").orOperator(Criteria.where("supplierlastName").regex("^"+supplierName+".*","i")))), Supplier.class);
		supplierList = mongoTemplate.find(query.addCriteria(Criteria.where("supplierfirstName").regex("^"+supplierName+".*","i")), Supplier.class);
		return supplierList;
	}

	@ResponseBody
	@RequestMapping("/searchSupplierTypeWiseSupplierList")
	public List<Supplier> SearchSupplierTypeWiseSupplierList(@RequestParam("supplierType") String supplierType)
	{
		List<Supplier> supplierList;

		Query query =new Query();
		//supplierList = mongoTemplate.find(query.addCriteria(Criteria.where("supplierfirstName").regex("^"+supplierName+".*","i").orOperator(Criteria.where("suppliermiddleName").regex("^"+supplierName+".*","i").orOperator(Criteria.where("supplierlastName").regex("^"+supplierName+".*","i")))), Supplier.class);
		supplierList = mongoTemplate.find(query.addCriteria(Criteria.where("supplierType").regex("^"+supplierType+".*","i")), Supplier.class);
		return supplierList;
	}

	@RequestMapping("/AddSupplier")
	public String addSupplier(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			Query query = new Query();

			model.addAttribute("suppliertypeList",findAllSupplierType());
			model.addAttribute("bankList",getAllBankName());
			model.addAttribute("supplierCode",SupplierCode());
			model.addAttribute("designationList", getAllDesignation());

			String supplierId = SupplierCode();

			List<SupplierEmployees> supplierEmployeeList = mongoTemplate.find(query.addCriteria(Criteria.where("supplierId").is(supplierId)), SupplierEmployees.class);

			if(supplierEmployeeList.size()!=0)
			{
				query = new Query();
				mongoTemplate.remove(query.addCriteria(Criteria.where("supplierId").is(supplierId)), SupplierEmployees.class);	
			}

			return "AddSupplier";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value = "/AddSupplier", method = RequestMethod.POST)
	public String supplierSave(@ModelAttribute Supplier supplier, Model model)
	{
		try {
			try
			{
				supplier = new Supplier(supplier.getSupplierId(), supplier.getSupplierfirmName(), supplier.getSupplierfirmType(), supplier.getSupplierfirmAddress(), supplier.getSupplierPincode(), supplier.getFirmpanNumber(), supplier.getFirmgstNumber(), supplier.getBankName(), supplier.getBranchName(), supplier.getBankifscCode(), supplier.getFirmbankacNumber(), supplier.getSuppliertypeId(), supplier.getSubsuppliertypeId(), supplier.getSupplierpaymentTerms(), supplier.getCheckPrintingName(),supplier.getSupplierOfficeNo(), supplier.getCreationDate(), supplier.getUpdateDate(), supplier.getUserName());
				supplierRepository.save(supplier);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}

			model.addAttribute("suppliertypeList",findAllSupplierType());
			model.addAttribute("bankList",getAllBankName());
			model.addAttribute("supplierCode",SupplierCode());
			model.addAttribute("designationList", getAllDesignation());

			return "AddSupplier";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/getsupplieraadharnumberList")
	public List<Supplier> AadharnumberList(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		List<Supplier> supplieraadharnumberList= supplierRepository.findAll();

		return supplieraadharnumberList;
	}

	@RequestMapping("/EditSupplier")
	public String EditSupplier(@RequestParam("supplierId") String supplierId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<Supplier> supplierDetails = mongoTemplate.find(query.addCriteria(Criteria.where("supplierId").is(supplierId)),Supplier.class);
			List<Designation> designationList = designationRepository.findAll();
			query = new Query();
			List<SupplierEmployees> supplierEmployeesList = mongoTemplate.find(query.addCriteria(Criteria.where("supplierId").is(supplierId)), SupplierEmployees.class); 
			query = new Query();
			List<SupplierType> suppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("suppliertypeId").is(supplierDetails.get(0).getSuppliertypeId())), SupplierType.class); 
			query = new Query();
			List<SubSupplierType> subsuppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subsuppliertypeId").is(supplierDetails.get(0).getSubsuppliertypeId())), SubSupplierType.class); 



			model.addAttribute("supplierType", suppliertypeDetails.get(0).getSupplierType());
			model.addAttribute("subsupplierType", subsuppliertypeDetails.get(0).getSubsupplierType());

			model.addAttribute("supplierDetails", supplierDetails);
			model.addAttribute("suppliertypeList",findAllSupplierType());
			model.addAttribute("bankList",getAllBankName());
			model.addAttribute("designationList",designationList);
			model.addAttribute("supplierEmployeesList", supplierEmployeesList);

			return "EditSupplier";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/EditSupplier",method=RequestMethod.POST)
	public String EditSupplierPostMethod(@ModelAttribute Supplier supplier, Model model)
	{
		try {
			try
			{
				supplier = new Supplier(supplier.getSupplierId(), supplier.getSupplierfirmName(), supplier.getSupplierfirmType(), supplier.getSupplierfirmAddress(), supplier.getSupplierPincode(), supplier.getFirmpanNumber(), supplier.getFirmgstNumber(), supplier.getBankName(), supplier.getBranchName(), supplier.getBankifscCode(), supplier.getFirmbankacNumber(), supplier.getSuppliertypeId(), supplier.getSubsuppliertypeId(),  supplier.getSupplierpaymentTerms(), supplier.getCheckPrintingName(), supplier.getSupplierOfficeNo(), supplier.getCreationDate(), supplier.getUpdateDate(), supplier.getUserName());
				supplierRepository.save(supplier);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}

			List<Supplier> supplierList = supplierRepository.findAll(); 

			List<LocationArea> locationareaList= locationAreaRepository.findAll();
			List<City> cityList = cityRepository.findAll();
			List<State> stateList= stateRepository.findAll();
			List<Country> countryList= countryRepository.findAll();
			int j=0,i=0;
			Query query = new Query();
			for(i=0;i<supplierList.size();i++)
			{
				try
				{
					query = new Query();
					List<SupplierType> suppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("suppliertypeId").is(supplierList.get(i).getSuppliertypeId())), SupplierType.class); 
					supplierList.get(i).setSuppliertypeId(suppliertypeDetails.get(0).getSupplierType());
					query = new Query();
					List<SubSupplierType> subsuppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subsuppliertypeId").is(supplierList.get(i).getSubsuppliertypeId())), SubSupplierType.class); 
					supplierList.get(i).setSubsuppliertypeId(subsuppliertypeDetails.get(0).getSubsupplierType());


				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}

			model.addAttribute("countryList",findAllCountryName());
			model.addAttribute("supplierList",supplierList);

			return "SupplierMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	public int Generate_SupplierEmployeeId()
	{
		int supplierEmployeeId = 0;

		List<SupplierEmployees> supplierEmployeeList = supplierEmployeesRepository.findAll();

		int size = supplierEmployeeList.size();

		if(size!=0)
		{
			supplierEmployeeId = supplierEmployeeList.get(size-1).getSupplierEmployeeId();
		}

		return supplierEmployeeId+1;
	}

	@ResponseBody
	@RequestMapping("/AddSupplierEmployee")
	public List<SupplierEmployees> AddSupplierEmployee(@RequestParam("supplierId") String supplierId, @RequestParam("employeeName") String employeeName, @RequestParam("employeeDesignation") String employeeDesignation, @RequestParam("employeeEmail") String employeeEmail, @RequestParam("employeeMobileno") String employeeMobileno, HttpSession session)
	{
		try
		{
			SupplierEmployees supplierEmployees = new SupplierEmployees();
			supplierEmployees.setSupplierEmployeeId(Generate_SupplierEmployeeId());
			supplierEmployees.setSupplierId(supplierId);
			supplierEmployees.setEmployeeName(employeeName);
			supplierEmployees.setEmployeeDesignation(employeeDesignation);
			supplierEmployees.setEmployeeEmail(employeeEmail);
			supplierEmployees.setEmployeeMobileno(employeeMobileno);

			supplierEmployeesRepository.save(supplierEmployees);

			Query query = new Query();
			List<SupplierEmployees> supplierEmployeesList = mongoTemplate.find(query.addCriteria(Criteria.where("supplierId").is(supplierId)), SupplierEmployees.class);

			return supplierEmployeesList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@ResponseBody
	@RequestMapping("/DeleteSupplierEmployee")
	public List<SupplierEmployees> DeleteSupplierEmployee(@RequestParam("supplierId") String supplierId, @RequestParam("supplierEmployeeId") String supplierEmployeeId, HttpSession session)
	{
		try
		{
			Query query = new Query();
			mongoTemplate.remove(query.addCriteria(Criteria.where("supplierEmployeeId").is(Integer.parseInt(supplierEmployeeId)).and("supplierId").is(supplierId)), SupplierEmployees.class);

			query = new Query();
			List<SupplierEmployees> supplierEmployeeList = mongoTemplate.find(query.addCriteria(Criteria.where("supplierId").is(supplierId)), SupplierEmployees.class);

			return supplierEmployeeList;
		}
		catch(Exception e)
		{
			return null;
		}

	}
}

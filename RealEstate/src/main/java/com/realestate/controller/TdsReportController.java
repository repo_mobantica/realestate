package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Booking;
import com.realestate.bean.CustomerReceiptForm;
import com.realestate.bean.Flat;
import com.realestate.bean.Floor;
import com.realestate.bean.Login;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.BookingRepository;

@Controller
@RequestMapping("/")
public class TdsReportController {

	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	BookingRepository bookingRepository;

	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping("/TdsReportMaster")
	public String TdsReportMaster(Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			Query query = new Query();

			List<Booking> bookingList = mongoTemplate.find(query.addCriteria(Criteria.where("tds").ne(0)), Booking.class);

			for(int i=0;i<bookingList.size();i++)
			{
				try {
					query = new Query();
					Project projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(bookingList.get(i).getProjectId())), Project.class);
					query = new Query();
					ProjectBuilding buildingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(bookingList.get(i).getBuildingId())), ProjectBuilding.class);
					query = new Query();
					ProjectWing wingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(bookingList.get(i).getWingId())), ProjectWing.class);
					query = new Query();
					Flat flatDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("flatId").is(bookingList.get(i).getFlatId())), Flat.class);

					bookingList.get(i).setProjectName(projectDetails.getProjectName());
					bookingList.get(i).setFlatNumber(buildingDetails.getBuildingName()+"-"+wingDetails.getWingName()+"-"+flatDetails.getFlatNumber());
				}catch (Exception e) {
					e.printStackTrace();
					// TODO: handle exception
				}
			}
			List<Project> projectList = GetUserAssigenedProjectList(req,res); 

			model.addAttribute("projectList",projectList);

			model.addAttribute("bookingList", bookingList);
			return "TdsReportMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping(value="/getTdsReport",method=RequestMethod.POST)
	public List<Booking> getTdsReport(@RequestParam("wingId") String wingId, @RequestParam("buildingId") String buildingId, @RequestParam("projectId") String projectId, HttpSession session)
	{

		Query query = new Query();

		System.out.println("wingId = "+wingId);
		System.out.println("buildingId = "+buildingId);
		System.out.println("projectId = "+projectId);

		List<Booking> bookingList =new ArrayList<Booking>();

		if(projectId.equalsIgnoreCase("Default"))
		{

			bookingList= mongoTemplate.find(query.addCriteria(Criteria.where("tds").ne(0)), Booking.class);	

		}
		else
		{
			if(buildingId.equalsIgnoreCase("Default"))
			{
				bookingList= mongoTemplate.find(query.addCriteria(Criteria.where("tds").ne(0).and("projectId").is(projectId)), Booking.class);	

			}
			else
			{
				if(wingId.equalsIgnoreCase("Default"))
				{
					bookingList= mongoTemplate.find(query.addCriteria(Criteria.where("tds").ne(0).and("projectId").is(projectId).and("buildingId").is(buildingId)), Booking.class);	
				}
				else
				{
					bookingList= mongoTemplate.find(query.addCriteria(Criteria.where("tds").ne(0).and("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId)), Booking.class);	
				}
			}
		}
		for(int i=0;i<bookingList.size();i++)
		{
			try {
				query = new Query();
				Project projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(bookingList.get(i).getProjectId())), Project.class);
				query = new Query();
				ProjectBuilding buildingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(bookingList.get(i).getBuildingId())), ProjectBuilding.class);
				query = new Query();
				ProjectWing wingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(bookingList.get(i).getWingId())), ProjectWing.class);
				query = new Query();
				Flat flatDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("flatId").is(bookingList.get(i).getFlatId())), Flat.class);

				bookingList.get(i).setProjectName(projectDetails.getProjectName());
				bookingList.get(i).setFlatNumber(buildingDetails.getBuildingName()+"-"+wingDetails.getWingName()+"-"+flatDetails.getFlatNumber());
			}catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}
		}

		return bookingList;
	}

}

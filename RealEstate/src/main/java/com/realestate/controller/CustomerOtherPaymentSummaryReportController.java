package com.realestate.controller;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.realestate.bean.Aggreement;
import com.realestate.bean.Booking;
import com.realestate.bean.City;
import com.realestate.bean.Country;
import com.realestate.bean.CustomerOtherPaymentDetails;
import com.realestate.bean.CustomerReceiptForm;
import com.realestate.bean.ExtraCharges;
import com.realestate.bean.Flat;
import com.realestate.bean.LocationArea;
import com.realestate.bean.MultipleOtherCharges;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.State;
import com.realestate.configuration.CommanController;
import com.realestate.repository.BookingRepository;
import com.realestate.repository.CountryRepository;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;

@Controller
@RequestMapping("/")
public class CustomerOtherPaymentSummaryReportController 
{
	@Autowired
	BookingRepository bookingRepository;
	@Autowired
	CountryRepository countryRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	@RequestMapping("/CustomerOtherPaymentSummary")
	public String CustomerOtherPaymentSummary(Model model)
	{
		List<Booking> bookingList = bookingRepository.findAll();
		List<Country> countryList = countryRepository.findAll();

		Query query = new Query();
		for(int i=0;i<bookingList.size();i++)
		{
			try
			{
				query = new Query();
				List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(bookingList.get(i).getProjectId())), Project.class);

				query =new Query();
				List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(bookingList.get(i).getBuildingId())), ProjectBuilding.class);

				query =new Query();
				List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(bookingList.get(i).getWingId())), ProjectWing.class);

				query = new Query();
				List<Flat> flatDetails = mongoTemplate.find(query.addCriteria(Criteria.where("flatId").is(bookingList.get(i).getFlatId())), Flat.class);

				bookingList.get(i).setProjectId(projectDetails.get(0).getProjectName());
				bookingList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
				bookingList.get(i).setWingId(wingDetails.get(0).getWingName());
				bookingList.get(i).setFlatId(flatDetails.get(0).getFlatNumber());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		model.addAttribute("bookingList", bookingList);
		model.addAttribute("countryList", countryList);

		return "CustomerOtherPaymentSummary";
	}

	@RequestMapping(value="/PrintCustomerOtherPaymentSummaryDetails")
	public ResponseEntity<byte[]> PrintCustomerSummaryDetails(@RequestParam("bookingId") String bookingId, HttpServletRequest request, HttpServletResponse response)
	{
		JasperPrint print;

		try 
		{	
			Query query = new Query();
			HashMap jmap = new HashMap();
			Collection c = new ArrayList();

			List<Booking> bookingList = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)),Booking.class);

			query = new Query();
			List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(bookingList.get(0).getProjectId())), Project.class);

			query =new Query();
			List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(bookingList.get(0).getBuildingId())), ProjectBuilding.class);

			query =new Query();
			List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(bookingList.get(0).getWingId())), ProjectWing.class);

			query = new Query();
			List<Flat> flatDetails = mongoTemplate.find(query.addCriteria(Criteria.where("flatId").is(bookingList.get(0).getFlatId())), Flat.class);

			query = new Query();
			List<CustomerOtherPaymentDetails> paymentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)), CustomerOtherPaymentDetails.class);

			query = new Query();
			//List<CustomerReceiptForm> customerPaymentHistory = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId).and("status").is("Deposited")), CustomerReceiptForm.class);
			List<CustomerReceiptForm> customerPaymentHistory = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)), CustomerReceiptForm.class);

			query = new Query();
			List<Aggreement> aggreementList = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)), Aggreement.class);

			query = new Query();
			List<ExtraCharges> extraChargesDetails = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)), ExtraCharges.class);

			query = new Query();
			List<MultipleOtherCharges> otherCharges = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)), MultipleOtherCharges.class);


			long maintainanceTotalAmount=0;

			if(paymentDetails.size()!=0 && customerPaymentHistory.size()!=0)
			{

				for(int i=0;i<customerPaymentHistory.size();i++)
				{

					jmap = new HashMap();

					try
					{
						if(customerPaymentHistory.get(i).getPaymentType().equals("Maintainance"))
						{
							if(!customerPaymentHistory.get(i).getBankName().equals(""))
							{
								/*1*/ jmap.put("bankName",""+customerPaymentHistory.get(i).getBankName());
							}
							else
							{
								/*1*/ jmap.put("bankName","CASH");   	   
							}

							if(customerPaymentHistory.get(i).getBranchName()!=null)
							{
								/*2*/ jmap.put("branchName",""+customerPaymentHistory.get(i).getBranchName());	 
							}
							else
							{
								/*2*/ jmap.put("branchName","N.A.");	 
							}

							/*3*/ jmap.put("cheqNo",""+customerPaymentHistory.get(i).getChequeNumber());
							/*4*/ jmap.put("chqDate",""+customerPaymentHistory.get(i).getCreationDate());
							/*5*/ jmap.put("chqAmt",""+customerPaymentHistory.get(i).getPaymentAmount().longValue());
							/*6*/jmap.put("maintAmt",""+customerPaymentHistory.get(i).getPaymentAmount().longValue());
							/*7*/jmap.put("receiptNo",""+customerPaymentHistory.get(i).getReceiptId());
						}
						/*else
			    			 {

				    			 1 jmap.put("bankName","N.A.");   	   
				    			 2 jmap.put("branchName","N.A.");	 
				    			 3 jmap.put("cheqNo","N.A.");
				    			 4 jmap.put("chqDate","--");
				    			 5 jmap.put("chqAmt","0");
			    			     6jmap.put("maintAmt","0");
			    			     7jmap.put("receiptNo","N.A.");
			    			 }*/

						if(customerPaymentHistory.get(i).getPaymentType().equals("Handling Charge"))
						{
							if(!customerPaymentHistory.get(i).getBankName().equals(""))
							{
								/*1*/ jmap.put("bankName",""+customerPaymentHistory.get(i).getBankName());
							}
							else
							{
								/*1*/ jmap.put("bankName","CASH");   	   
							}

							if(customerPaymentHistory.get(i).getBranchName()!=null)
							{
								/*2*/ jmap.put("branchName",""+customerPaymentHistory.get(i).getBranchName());	 
							}
							else
							{
								/*2*/ jmap.put("branchName","N.A.");	 
							}

							/*3*/ jmap.put("cheqNo",""+customerPaymentHistory.get(i).getChequeNumber());
							/*4*/ jmap.put("chqDate",""+customerPaymentHistory.get(i).getCreationDate());
							/*5*/ jmap.put("chqAmt",""+customerPaymentHistory.get(i).getPaymentAmount().longValue());
							/*6*/ jmap.put("handlingAmt",""+customerPaymentHistory.get(i).getPaymentAmount().longValue());
							/*7*/ jmap.put("receiptNo",""+customerPaymentHistory.get(i).getReceiptId());

						}
						/*else
			    			 {
			    				 1 jmap.put("bankName","N.A.");
				    			 2 jmap.put("branchName","N.A.");	 
			    				 3 jmap.put("cheqNo","--");
				    			 4 jmap.put("chqDate","--");
				    			 5 jmap.put("chqAmt","--");
			    			     6 jmap.put("handlingAmt","0");
 		    			         7jmap.put("receiptNo","N.A.");
			    			 }*/

						if(customerPaymentHistory.get(i).getPaymentType().equals("Extra Charge"))
						{
							if(!customerPaymentHistory.get(i).getBankName().equals(""))
							{
								/*1*/ jmap.put("bankName",""+customerPaymentHistory.get(i).getBankName());
							}
							else
							{
								/*1*/ jmap.put("bankName","CASH");   	   
							}

							if(customerPaymentHistory.get(i).getBranchName()!=null)
							{
								/*2*/ jmap.put("branchName",""+customerPaymentHistory.get(i).getBranchName());	 
							}
							else
							{
								/*2*/ jmap.put("branchName","N.A.");	 
							}

							/*3*/ jmap.put("cheqNo",""+customerPaymentHistory.get(i).getChequeNumber());
							/*4*/ jmap.put("chqDate",""+customerPaymentHistory.get(i).getCreationDate());
							/*5*/ jmap.put("chqAmt",""+customerPaymentHistory.get(i).getPaymentAmount().longValue());
							/*6*/ jmap.put("extraChrgAmount",""+customerPaymentHistory.get(i).getPaymentAmount().longValue());
							/*7*/ jmap.put("receiptNo",""+customerPaymentHistory.get(i).getReceiptId());
						}
						/*else
			    			 {

				    			 1 jmap.put("bankName","N.A.");   	   
				    			 2 jmap.put("branchName","N.A.");	 
			    				 3 jmap.put("cheqNo","--");
				    			 4 jmap.put("chqDate","--");
				    			 5 jmap.put("chqAmt","--");
			    			     6 jmap.put("extraChrgAmount","0");
			    			     7 jmap.put("receiptNo","N.A.");
			    			 }*/

					}
					catch (Exception e)
					{
						// TODO: handle exception
					}

					if(jmap.size()!=0)
					{
						c.add(jmap);
						jmap = null;	
					}
					else
					{
						jmap=null;
					}
				}

				for(int i=0;i<customerPaymentHistory.size();i++)
				{
					if(customerPaymentHistory.get(i).getPaymentType().equals("Maintainance"))
					{
						maintainanceTotalAmount = maintainanceTotalAmount + customerPaymentHistory.get(i).getPaymentAmount().longValue();
					}
					else
					{
						maintainanceTotalAmount= maintainanceTotalAmount + 0;
					}
				}

				//String realPath ="//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img";
				String realPath =CommanController.GetLogoImagePath();
				JRDataSource dataSource = new JRMapCollectionDataSource(c);
				Map<String, Object> parameterMap = new HashMap<String, Object>();

				Date date = new Date();  
				SimpleDateFormat formatter = new SimpleDateFormat("d/M/yyyy");  
				String strDate= formatter.format(date);

				/*1*/ parameterMap.put("projectName", ""+projectDetails.get(0).getProjectName());
				/*2*/ parameterMap.put("buildingName", ""+buildingDetails.get(0).getBuildingName());
				/*3*/ parameterMap.put("wingName", ""+wingDetails.get(0).getWingName());
				/*4*/ parameterMap.put("flatNumber", ""+flatDetails.get(0).getFlatNumber());

				if(aggreementList.size()!=0)
				{
					/*5*/ parameterMap.put("customerName", "Name : Mr/Mrs. "+aggreementList.get(0).getFirstApplicantfirstname()+" "+aggreementList.get(0).getFirstApplicantmiddlename()+" "+aggreementList.get(0).getFirstApplicantlastname()); 	 
				}
				else
				{
					/*5*/ parameterMap.put("customerName", "Name : Mr/Mrs. "+bookingList.get(0).getBookingfirstname()+" "+bookingList.get(0).getBookingmiddlename()+" "+bookingList.get(0).getBookinglastname());
				}

				/*6*/  parameterMap.put("p_chequeAmt", "0");

				/*7*/  parameterMap.put("p_handling", ""+bookingList.get(0).getHandlingCharges().longValue());

				Double otherChagresTotal=0.0;
				if(otherCharges.size()!=0)
				{
					for(int i=0; i<otherCharges.size();i++)
					{
						otherChagresTotal+=otherCharges.get(i).getAmount();
					}
					/*8*/  parameterMap.put("p_extraChrgAmt", ""+otherChagresTotal.longValue());
				}
				else
				{
					/*8*/  parameterMap.put("p_extraChrgAmt", "0");
				}

				/*9*/  parameterMap.put("p_regist", ""+bookingList.get(0).getRegistrationCost1().longValue());

				/*10*/ parameterMap.put("p_gstAmt", ""+bookingList.get(0).getGstAmount1().longValue());

				if(extraChargesDetails.size()!=0)
				{
					/*11*/ parameterMap.put("p_maintainance", ""+extraChargesDetails.get(0).getMaintenanceCost().longValue());
					/*22*/ parameterMap.put("p_maintBalanceAmt", ""+(extraChargesDetails.get(0).getMaintenanceCost().longValue() - maintainanceTotalAmount));

				}
				else
				{
					/*11*/ parameterMap.put("p_maintainance", "0");
					/*22*/ parameterMap.put("p_maintBalanceAmt", "0");

				}

				/*12*/ parameterMap.put("p_receiptNo", "-");

				/*13*/ parameterMap.put("p_maintTotalAmt", ""+(Long)paymentDetails.get(0).getTotalPaidMaintainanceAmt());

				/*14*/ parameterMap.put("p_handlingTotalAmt", ""+(Long)paymentDetails.get(0).getTotalPaidHandlingCharges());

				/*15*/ parameterMap.put("p_extraChrgTotalAmt", ""+(Long)paymentDetails.get(0).getTotalPaidExtraCharges());

				/*16*/ parameterMap.put("p_maintTotalAmt", ""+maintainanceTotalAmount);

				/*18*/ parameterMap.put("p_handlingBalanceTotal", ""+(bookingList.get(0).getHandlingCharges().longValue() - (Long)paymentDetails.get(0).getTotalPaidHandlingCharges()));

				/*19*/ parameterMap.put("p_extraChrgBalanceAmt", ""+(otherChagresTotal.longValue() - (Long)paymentDetails.get(0).getTotalPaidExtraCharges()));

				/*23*/ parameterMap.put("realPath", ""+realPath);

				/*24*/ parameterMap.put("bookingId", "Booking Id:"+bookingList.get(0).getBookingId());

				/*25*/ parameterMap.put("customerAddress", "Address: "+bookingList.get(0).getBookingaddress()+"-"+bookingList.get(0).getBookingPincode());

				InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/CustomerOtherPaymentSummaryReport.jasper");

				print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				JasperExportManager.exportReportToPdfStream(print, baos);

				byte[] contents = baos.toByteArray();

				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.parseMediaType("application/pdf"));
				String filename = "CustomerOtherPaymentSummaryReport.pdf";

				JasperExportManager.exportReportToPdfStream(print, baos);

				headers.setContentType(MediaType.parseMediaType("application/pdf"));
				headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
				ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
				response.setHeader("Content-Disposition", "inline; filename=" + filename );
				return resp;	  
			}
			else
			{
				/*1*/ jmap.put("bankName","N.A.");   	   

				/*2*/ jmap.put("branchName","N.A.");	 

				/*3*/ jmap.put("cheqNo","N.A.");

				/*4*/ jmap.put("chqDate","N.A.");

				/*5*/ jmap.put("chqAmt","N.A.");

				/*6*/ jmap.put("maintAmt","N.A."); 

				/*7*/ jmap.put("handlingAmt","N.A.");

				/*8*/ jmap.put("extraChrgAmount","N.A.");

				/*9*/jmap.put("receiptNo","N.A.");

				c.add(jmap);
				jmap = null;
			}

			//String realPath ="//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img";
			String realPath =CommanController.GetLogoImagePath();
			JRDataSource dataSource = new JRMapCollectionDataSource(c);
			Map<String, Object> parameterMap = new HashMap<String, Object>();

			Date date = new Date();  
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
			String strDate= formatter.format(date);


			/*1*/ parameterMap.put("projectName", ""+projectDetails.get(0).getProjectName());
			/*2*/ parameterMap.put("buildingName", ""+buildingDetails.get(0).getBuildingName());
			/*3*/ parameterMap.put("wingName", ""+wingDetails.get(0).getWingName());
			/*4*/ parameterMap.put("flatNumber", ""+flatDetails.get(0).getFlatNumber());
			/*5*/ parameterMap.put("customerName", "Name : Mr/Mrs. "+bookingList.get(0).getBookingfirstname()+" "+bookingList.get(0).getBookingmiddlename()+" "+bookingList.get(0).getBookinglastname());
			/*6*/  parameterMap.put("p_chequeAmt", "0");
			/*7*/  parameterMap.put("p_handling", ""+bookingList.get(0).getHandlingCharges().longValue());
			/*8*/  parameterMap.put("p_extraChrgAmt", "0");
			/*9*/  parameterMap.put("p_regist", ""+bookingList.get(0).getRegistrationCost1().longValue());
			/*10*/ parameterMap.put("p_gstAmt", ""+bookingList.get(0).getGstAmount1().longValue());

			query = new Query();
			List<ProjectWing> wingList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(bookingList.get(0).getProjectId()).and("buildingId").is(bookingList.get(0).getBuildingId()).and("wingId").is(bookingList.get(0).getWingId())), ProjectWing.class);

			/*11*/ parameterMap.put("p_maintainance", ""+wingList.get(0).getMaintenanceCharges());
			/*12*/ parameterMap.put("p_maintBalanceAmt", ""+wingList.get(0).getMaintenanceCharges());
			/*13*/ parameterMap.put("p_receiptNo", "-");
			/*14*/ parameterMap.put("p_maintTotalAmt", ""+wingList.get(0).getMaintenanceCharges());
			/*15*/ parameterMap.put("p_handlingTotalAmt", ""+bookingList.get(0).getHandlingCharges().longValue());
			/*16*/ parameterMap.put("p_extraChrgTotalAmt", "0");
			/*17*/ parameterMap.put("p_maintTotalAmt", ""+wingList.get(0).getMaintenanceCharges());
			/*18*/ parameterMap.put("p_handlingBalanceTotal", ""+bookingList.get(0).getHandlingCharges().longValue());
			/*19*/ parameterMap.put("p_extraChrgBalanceAmt", "0");
			/*20*/ parameterMap.put("realPath", ""+realPath);
			/*21*/ parameterMap.put("bookingId", "Booking Id: "+bookingList.get(0).getBookingId());
			/*22*/ parameterMap.put("customerAddress", "Address: "+bookingList.get(0).getBookingaddress()+"-"+bookingList.get(0).getBookingPincode());

			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/CustomerOtherPaymentSummaryReport.jasper");

			print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(print, baos);

			byte[] contents = baos.toByteArray();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			String filename = "CustomerOtherPaymentSummaryReport.pdf";

			JasperExportManager.exportReportToPdfStream(print, baos);

			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
			ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
			response.setHeader("Content-Disposition", "inline; filename=" + filename );
			return resp; 
		}
		catch(Exception e)
		{
			return null;
		}
	}
}

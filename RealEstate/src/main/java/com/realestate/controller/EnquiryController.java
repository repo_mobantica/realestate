package com.realestate.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Bank;
import com.realestate.bean.Budget;
import com.realestate.bean.Country;
import com.realestate.bean.Enquiry;
import com.realestate.bean.EnquiryFollowUp;
import com.realestate.bean.EnquirySource;
import com.realestate.bean.Flat;
import com.realestate.bean.Login;
import com.realestate.bean.Occupation;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.SubEnquirySource;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.BankRepository;
import com.realestate.repository.BudgetRepository;
import com.realestate.repository.CountryRepository;
import com.realestate.repository.EnquiryFollowUpRepository;
import com.realestate.repository.EnquiryRepository;
import com.realestate.repository.EnquirySourceRepository;
import com.realestate.repository.FlatRepository;
import com.realestate.repository.OccupationRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.SubEnquirySourceRepository;
import com.realestate.services.SmsServices;

@Controller
@RequestMapping("/")
public class EnquiryController
{

	@Autowired
	SubEnquirySourceRepository subEnquirysourcerepository;
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	EnquiryRepository enquiryRepository;
	@Autowired
	CountryRepository countryRepository;
	@Autowired
	BankRepository bankRepository;
	@Autowired
	EnquirySourceRepository enquirySourceRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	FlatRepository flatRepository;
	@Autowired 
	OccupationRepository occupationRepository;
	@Autowired
	BudgetRepository budgetRepository;
	@Autowired
	EnquiryFollowUpRepository enquiryFollowUpRepository;


	int year = Calendar.getInstance().get(Calendar.YEAR);
	int month=Calendar.getInstance().get(Calendar.MONTH)+1;
	int y1,y2;

	//Code generation for enquiry

	String enquiryCode;
	private String EnquiryCode()
	{
		long enquiryCount = enquiryRepository.count();
		if(month>3) 
		{
			y1=year;
			y2=year+1;
			if(enquiryCount<10)
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/000"+(enquiryCount+1);
			}
			else if((enquiryCount>=10) && (enquiryCount<100))
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/00"+(enquiryCount+1);
			}
			else if((enquiryCount>=100) && (enquiryCount<1000))
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/0"+(enquiryCount+1);
			}
			else
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/"+(enquiryCount+1);
			}
		}
		else
		{
			y1=year-1;
			y2=year;
			if(enquiryCount<10)
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/000"+(enquiryCount+1);
			}
			else if((enquiryCount>=10) && (enquiryCount<100))
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/00"+(enquiryCount+1);
			}
			else if((enquiryCount>=100) && (enquiryCount<1000))
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/0"+(enquiryCount+1);
			}
			else 
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/"+(enquiryCount+1);
			}
		}
		return enquiryCode;
	}


	//Code for budget limit generation 
	private List<Budget> getBudgetList()
	{
		List<Budget> budgetList;

		budgetList = budgetRepository.findAll(new Sort(Sort.Direction.ASC,"budgetCost"));
		return budgetList;
	}

	//code for getting all country name
	private List<Country> findAllCountryName()
	{
		List<Country> countryList;
		countryList = countryRepository.findAll(new Sort(Sort.Direction.ASC,"countryName"));
		return countryList;
	}

	//code for getting all bank names
	private List<Bank> getAllBankName()
	{
		List<Bank> bankList= new ArrayList<Bank>();
		Query query = new Query();
		bankList = bankRepository.findAll(new Sort(Sort.Direction.ASC,"bankName"));
		return bankList;
	}

	//code getting all enquiry sources
	private List<EnquirySource> getAllEnquirySourceName()
	{
		List<EnquirySource> enquirySourceList;

		enquirySourceList = enquirySourceRepository.findAll(new Sort(Sort.Direction.ASC,"enquirysourceName"));
		return enquirySourceList;
	}

	//code for retriving all occupation retriving

	private List<Occupation> getAllOccupationName()
	{
		List<Occupation> occupationList;
		occupationList = occupationRepository.findAll(new Sort(Sort.Direction.ASC,"occupationName"));
		return occupationList;
	}


	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	//Request mapping for getting building names by project
	@ResponseBody
	@RequestMapping(value="/getBuildingList1",method=RequestMethod.POST)
	public List<ProjectBuilding> BuildingList(@RequestParam("projectName") String projectName, HttpSession session)
	{
		List<ProjectBuilding> buildingList= new ArrayList<ProjectBuilding>();

		Query query = new Query();
		buildingList = mongoTemplate.find(query.addCriteria(Criteria.where("projectName").is(projectName)), ProjectBuilding.class);

		return buildingList;
	}


	//----------------------------------Request Mapping For Add Enquiry ----------------------
	@RequestMapping(value="/AddEnquiry")
	public String AddEnquiry(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<Enquiry> enquiryList = enquiryRepository.findAll(); 

			List<Project> projectList = GetUserAssigenedProjectList(req,res); 

			model.addAttribute("projectList",projectList);
			model.addAttribute("enquiryCode",EnquiryCode());
			model.addAttribute("countryList", findAllCountryName());
			model.addAttribute("bankList",getAllBankName());
			model.addAttribute("enquirySourceList", getAllEnquirySourceName());
			model.addAttribute("occupationList",getAllOccupationName());
			model.addAttribute("budgetList",getBudgetList());
			model.addAttribute("enquiryList",enquiryList);
			return "AddEnquiry";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/AddEnquiry", method=RequestMethod.POST)
	public String SaveEnquiry(@ModelAttribute EnquiryFollowUp enquiryFollowUp, @ModelAttribute Enquiry enquiry, Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{

				//for follow table
				EnquiryFollowUp enquiryFollowUp1=new EnquiryFollowUp();
				enquiryFollowUp1.setEnquiryId(enquiryFollowUp.getEnquiryId());
				enquiryFollowUp1.setFollowupDate(enquiryFollowUp.getFollowupDate());

				enquiryFollowUp1.setEnqRemark(enquiryFollowUp.getEnqRemark());
				enquiryFollowUp1.setEnqStatus(enquiryFollowUp.getEnqStatus());
				enquiryFollowUp1.setFollowupTakenBy( enquiryFollowUp.getFollowupTakenBy());
				enquiryFollowUp1.setCreationDate(new Date());
				enquiryFollowUp1.setUserName(enquiryFollowUp.getUserName());
				enquiryFollowUpRepository.save(enquiryFollowUp1);

				//for enquiry table
				Enquiry enquiry1=new Enquiry();
				enquiry1.setEnquiryId(enquiry.getEnquiryId());
				enquiry1.setEnqfirstName(enquiry.getEnqfirstName());
				enquiry1.setEnqmiddleName(enquiry.getEnqmiddleName());
				enquiry1.setEnqlastName(enquiry.getEnqlastName());
				enquiry1.setEnqEmail(enquiry.getEnqEmail());
				enquiry1.setEnqmobileNumber1(enquiry.getEnqmobileNumber1());
				enquiry1.setEnqmobileNumber2(enquiry.getEnqmobileNumber2());
				enquiry1.setEnqOccupation(enquiry.getEnqOccupation());
				enquiry1.setProjectName(enquiry.getProjectName());
				enquiry1.setFlatType(enquiry.getFlatType());
				enquiry1.setFlatRemark(enquiry.getFlatRemark());
				enquiry1.setFlatBudget(enquiry.getFlatBudget());
				enquiry1.setEnquirysourceId(enquiry.getEnquirysourceId());
				enquiry1.setSubsourceId(enquiry.getSubsourceId());
				enquiry1.setFollowupDate(enquiry.getFollowupDate());
				enquiry1.setEnqStatus(enquiry.getEnqStatus());
				enquiry1.setEnqRemark(enquiry.getEnqRemark());
				enquiry1.setCreationDate(new Date());
				enquiry1.setUpdateDate(new Date());
				enquiry1.setUserName(enquiry.getUserName());
				enquiryRepository.save(enquiry1);

				model.addAttribute("enquirydbStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("enquirydbStatus","Fail");
			}
			List<Enquiry> enquiryList = enquiryRepository.findAll(); 

			List<Project> projectList = GetUserAssigenedProjectList(req,res); 

			//Send sms to customer
			try {
				String sms="";
				sms="Dear "+enquiry.getEnqfirstName()+", Thanks for visiting us and enquiring a "+enquiry.getProjectName()+" project of Sonigara Group for "+enquiry.getFlatType()+" flat.\r\n" + 
						"Sales Ex Contact No:-09527023000"+".\r\n" +
						"Pls do visit again.";
				SmsServices.sendSMS(enquiry.getEnqmobileNumber1(),sms);
			}catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}

			model.addAttribute("projectList",projectList);
			model.addAttribute("enquiryCode",EnquiryCode());
			model.addAttribute("countryList", findAllCountryName());
			model.addAttribute("bankList",getAllBankName());
			model.addAttribute("enquirySourceList", getAllEnquirySourceName());
			model.addAttribute("occupationList",getAllOccupationName());
			model.addAttribute("budgetList",getBudgetList());
			model.addAttribute("enquiryList",enquiryList);
			return "AddEnquiry";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/EnquiryMaster")
	public String enquiryMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			Date todayDate = new Date();
			final Calendar calendar = Calendar.getInstance();
			calendar.setTime(todayDate);
			calendar.add(Calendar.DAY_OF_YEAR, -1);
			Date date= calendar.getTime();

			List<Project> projectList = GetUserAssigenedProjectList(req,res); 

			List<SubEnquirySource> subenquirysourceList = subEnquirysourcerepository.findAll();
			Query query = new Query();
			List<Enquiry> enquiryList = mongoTemplate.find(query.addCriteria(Criteria.where("creationDate").gte(date).lt(todayDate)), Enquiry.class);

			for(int i=0;i<enquiryList.size();i++)
			{
				try
				{
					for(int j=0;j<subenquirysourceList.size();j++)
					{
						if(enquiryList.get(i).getSubsourceId().equals(subenquirysourceList.get(j).getSubsourceId()))
						{
							enquiryList.get(i).setSubsourceId(subenquirysourceList.get(j).getSubenquirysourceName());
							break;
						}
					}
				}catch (Exception e) {
					// TODO: handle exception
				}
			}

			model.addAttribute("countryList", findAllCountryName());
			model.addAttribute("projectList",projectList);
			model.addAttribute("enquiryList", enquiryList);

			return "EnquiryMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping(value="/getenquiryList",method=RequestMethod.POST)
	public List<Enquiry> enquiryList(@RequestParam("flatType") String flatType, HttpSession session)
	{
		Date todayDate = new Date();
		final Calendar calendar = Calendar.getInstance();
		calendar.setTime(todayDate);
		calendar.add(Calendar.DAY_OF_YEAR, -1);
		Date date= calendar.getTime();

		Query query = new Query();
		List<Enquiry> enquiryList = mongoTemplate.find(query.addCriteria(Criteria.where("flatType").is(flatType).and("creationDate").gte(date).lt(todayDate)), Enquiry.class);

		return enquiryList;
	}

	@RequestMapping("/AllEnquiryViews")
	public String allenquiryviewMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<Enquiry> enquiryList=enquiryRepository.findAll();
			List<Project> projectList = GetUserAssigenedProjectList(req,res); 

			model.addAttribute("countryList", findAllCountryName());
			model.addAttribute("projectList",projectList);
			model.addAttribute("enquiryList", enquiryList);

			return "AllEnquiryViews";

		}catch (Exception e) {
			return "login";
		}
	}
	@ResponseBody
	@RequestMapping(value="/getallenquirybyprojectList",method=RequestMethod.POST)
	public List<Enquiry> allenquirybyprojectList(@RequestParam("projectName") String projectName, HttpSession session)
	{
		List<Enquiry> enquiryList= new ArrayList<Enquiry>();

		Query query = new Query();
		enquiryList = mongoTemplate.find(query.addCriteria(Criteria.where("projectName").is(projectName)), Enquiry.class);

		return enquiryList;
	}
	@ResponseBody
	@RequestMapping(value="/getallenquirybyselectedDateList",method=RequestMethod.POST)
	public List<Enquiry> allenquirybyselecteddateList(@RequestParam("datepicker") String date, HttpSession session) throws ParseException
	{
		Query query = new Query();

		SimpleDateFormat formatter1=new SimpleDateFormat("dd/MM/yyyy"); 
		Date todayDate=formatter1.parse(date);  

		final Calendar calendar = Calendar.getInstance();
		calendar.setTime(todayDate);
		calendar.add(Calendar.DAY_OF_YEAR, +1);
		Date date1= calendar.getTime();

		query = new Query();
		List<Enquiry> enquiryList = mongoTemplate.find(query.addCriteria(Criteria.where("creationDate").gte(todayDate).lt(date1)), Enquiry.class);
		return enquiryList;
	}


	@ResponseBody
	@RequestMapping("/getenquirymobilenumberList")
	public List<Enquiry> EnquirymobilenumberList(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		List<Enquiry> enquirymobilenumberList= enquiryRepository.findAll();

		return enquirymobilenumberList;
	}

	@ResponseBody
	@RequestMapping(value="/getSubEnquiryList",method=RequestMethod.POST)
	public List<SubEnquirySource> getSubEnquiryList(@RequestParam("enquirysourceId") String enquirysourceId, HttpSession session)
	{
		List<SubEnquirySource> subenquiryList= new ArrayList<SubEnquirySource>();

		Query query = new Query();
		subenquiryList = mongoTemplate.find(query.addCriteria(Criteria.where("enquirysourceId").is(enquirysourceId)), SubEnquirySource.class);
		return subenquiryList;
	}
}

package com.realestate.controller;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.City;
import com.realestate.bean.Company;
import com.realestate.bean.Country;
import com.realestate.bean.LocationArea;
import com.realestate.bean.Login;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectDetails;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.State;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.ProjectRepository;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;

@Controller
@RequestMapping("/")
public class ArchitectCertificateController
{
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	MongoTemplate mongoTemplate;


	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping("/ArchitectCertificate")
	public String ArchitectCertificate(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		List<Project> projectList = GetUserAssigenedProjectList(req,res);

		model.addAttribute("projectList",projectList);
		return "ArchitectCertificate";
	}

	@ResponseBody
	@RequestMapping("/PrintArchitectCertificate")
	public ResponseEntity<byte[]> PrintArchitectCertificate(@RequestParam("wingId") String wingId, HttpServletRequest req, ModelMap model, HttpServletResponse response)
	{
		try 
		{	
			JasperPrint print;
			Query query = new Query();
			HashMap jmap = new HashMap();
			Collection c = new ArrayList();

			query = new Query();
			List<ProjectWing> wingList = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(wingId)), ProjectWing.class);

			List<Project> projectDetails = null;
			List<ProjectBuilding> buildingDetails = null;
			Company companyDetails = new Company();

			for(int i=0;i<wingList.size();i++)
			{
				try
				{
					query = new Query();
					projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(wingList.get(i).getProjectId())), Project.class);
					wingList.get(i).setProjectId(projectDetails.get(0).getProjectName());

					query = new Query();
					companyDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("companyId").is(projectDetails.get(0).getCompanyId())), Company.class);

					query = new Query();
					buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(wingList.get(i).getBuildingId())), ProjectBuilding.class);
					wingList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
				}
				catch (Exception e)
				{
					// TODO: handle exception
				}
			}

			query =new Query();
			State companyStateDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("stateId").is(companyDetails.getStateId())), State.class);
			query =new Query();
			City companyCityDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("cityId").is(companyDetails.getCityId())), City.class);
			query =new Query();
			LocationArea companyLocationareaDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("locationareaId").is(companyDetails.getLocationareaId())), LocationArea.class);


			query =new Query();
			State projectStateDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("stateId").is(projectDetails.get(0).getStateId())), State.class);
			query =new Query();
			City projectCityDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("cityId").is(projectDetails.get(0).getCityId())), City.class);
			query =new Query();
			LocationArea projectLocationareaDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("locationareaId").is(projectDetails.get(0).getLocationareaId())), LocationArea.class);

			jmap = new HashMap();
			jmap.put("srno", "1");
			c.add(jmap);
			jmap = null;

			JRDataSource dataSource = new JRMapCollectionDataSource(c);
			Map<String, Object> parameterMap = new HashMap<String, Object>();

			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			String strDate = formatter.format(date);

			/* 1 */  parameterMap.put("printDate", "" + strDate);
			/* 2 */  parameterMap.put("companyName", ""+companyDetails.getCompanyName()+" "+companyDetails.getCompanyType()+"");
			query = new Query();
			ProjectDetails projectDetails2 = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectDetails.get(0).getProjectId())), ProjectDetails.class);
			/* 3 */  parameterMap.put("companyAddress", ""+companyDetails.getCompanyAddress()+", "+companyLocationareaDetails.getLocationareaName()+", "+companyCityDetails.getCityName()+", "+companyStateDetails.getStateName()+" - "+companyDetails.getCompanyPincode());
			/* 4 */  parameterMap.put("subject", "Certificate of Completion of Construction Work of Project :- "+projectDetails.get(0).getProjectName()+", Building :- "+buildingDetails.get(0).getBuildingName()+" & Wing :- "+wingList.get(0).getWingName()+" Of The Building Of The Project ["+projectDetails.get(0).getReraNumber()+"] Situated On The Plot Bearing C.N. No./CTS No./Survey No./Got No./Final Plot No."+projectDetails.get(0).getGatORserveyNumber()+", Demarcated Byits Boundaries "+projectDetails2.getLocationInNorth()+" To The North, "+projectDetails2.getLocationInSouth()+" To The South, "+projectDetails2.getLocationInEast()+" To The East, "+projectDetails2.getLocationInWest()+" To The West. Located At :- "+projectDetails.get(0).getProjectAddress()+", "+projectLocationareaDetails.getLocationareaName()+", Of Division "+projectCityDetails.getCityName()+", "+projectStateDetails.getStateName()+"-"+projectDetails.get(0).getAreaPincode());
			/* 5 */  parameterMap.put("passage1", "I/We [Name Of Architect] Have Undertaken Assigenment As Architect/Licensed Surveyor Of Certifying Completion Of Construction Work Of Project :- "+projectDetails.get(0).getProjectName()+", Building :- "+buildingDetails.get(0).getBuildingName()+", Wing :- "+wingList.get(0).getWingName()+" Of The Building Situated On The Plot Bearing C.N. No./CTS No./Survey No./Got No./Final Plot No."+projectDetails.get(0).getGatORserveyNumber()+", Located At "+projectDetails.get(0).getProjectAddress()+", "+projectLocationareaDetails.getLocationareaName()+", Of Division "+projectCityDetails.getCityName()+", "+projectStateDetails.getStateName()+"-"+projectDetails.get(0).getAreaPincode()+" Ad-measuring "+projectDetails.get(0).getProjectlandArea()+" Sq.Mtr. Are Being Developed By M/S. "+companyDetails.getCompanyName()+" "+companyDetails.getCompanyType()+" Ltd.");
			/* 6 */  parameterMap.put("architectName", "Remaining");
			/* 7 */  parameterMap.put("structuralConsultant", "Remaining");
			/* 8 */  parameterMap.put("MEP_consultant", "Remaining");
			/* 9 */  parameterMap.put("siteSupervisor", "Remaining");
			/* 10 */ parameterMap.put("passage2", "Based On Completion Based On Completion Certificate Received From Structural Engineer And Site Supervisor : And To The Best Of My/Our Knowledge/We Hereby Certify That Project :- "+projectDetails.get(0).getProjectName()+", Building :- "+buildingDetails.get(0).getBuildingName()+" & Wing :- "+wingList.get(0).getWingName()+" Of The Building Has Been Completed In All Aspects And Is Fit For Occupancy For Which It Has Been Erected/Re-erected/Conslructed And Enlarged. The Project :- "+projectDetails.get(0).getProjectName()+", Building :- "+buildingDetails.get(0).getBuildingName()+" & Wing :- "+wingList.get(0).getWingName()+" Of The Buildingis Granted By Occupancy Certificate/Completion Certificate Bearing Number Port Completion Certificate No. __________, Dated :__/__/____ By ____________________________________.");

			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/ArchitectCertificate.jasper");

			print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(print, baos);

			byte[] contents = baos.toByteArray();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			String filename = "Architect Certificate Report.pdf";

			JasperExportManager.exportReportToPdfStream(print, baos);

			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
			ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
			response.setHeader("Content-Disposition", "inline; filename=" + filename);
			return resp;
		} 
		catch (Exception ex) 
		{
			return null;
		}
	}

}

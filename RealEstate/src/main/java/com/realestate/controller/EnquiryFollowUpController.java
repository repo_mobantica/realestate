package com.realestate.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.realestate.bean.Enquiry;
import com.realestate.bean.EnquiryFollowUp;
import com.realestate.repository.EnquiryFollowUpRepository;
import com.realestate.repository.EnquiryRepository;

@RequestMapping("/")
@Controller
public class EnquiryFollowUpController
{
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	EnquiryFollowUpRepository enquiryFollowUpRepository;
	@Autowired
	EnquiryRepository enquiryRepository;

	private List<Enquiry> getTodayEnquiryList()
	{
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/M/yyyy");
		LocalDate localDate = LocalDate.now();
		String todayDate=dtf.format(localDate);

		List<Enquiry> todayenquiryList;
		//todayenquiryList = enquiryRepository.findAll();

		Query query = new Query();
		todayenquiryList = mongoTemplate.find(query.addCriteria(Criteria.where("followupDate").is(todayDate).and("enqStatus").ne("Booking")), Enquiry.class);

		return todayenquiryList;
	}

	@RequestMapping("/TodayEnquiryFollowUp")
	public String addTodayEnquiryFollowUp(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			int year = Calendar.getInstance().get(Calendar.YEAR);
			int month=Calendar.getInstance().get(Calendar.MONTH)+1;
			int day=Calendar.getInstance().get(Calendar.DATE)-1;
			int yday=day-1;
			String date1=String.valueOf(day)+"/"+String.valueOf(month)+"/"+String.valueOf(year);
			String date2=String.valueOf(yday)+"/"+String.valueOf(month)+"/"+String.valueOf(year);

			List<Enquiry> pendingenquiryList;
			Query query = new Query();
			pendingenquiryList = mongoTemplate.find(query.addCriteria(Criteria.where("followupDate").is(date1).and("enqStatus").ne("Booking")), Enquiry.class);
			List<Enquiry> pendingenquiryList1;
			Query query1 = new Query();
			pendingenquiryList1 = mongoTemplate.find(query1.addCriteria(Criteria.where("followupDate").is(date2).and("enqStatus").ne("Booking")), Enquiry.class);

			List<Enquiry> allenquiryList;
			Query query2 = new Query();
			allenquiryList = mongoTemplate.find(query2.addCriteria(Criteria.where("enqStatus").ne("Booking")), Enquiry.class);


			model.addAttribute("allenquiryList", allenquiryList);
			model.addAttribute("pendingenquiryList", pendingenquiryList);
			model.addAttribute("pendingenquiryList1", pendingenquiryList1);
			model.addAttribute("todayenquiryList", getTodayEnquiryList());
			return "TodayEnquiryFollowUp";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/EnquiryFollowUp")
	public String getEnquiryFollowUp(Model model, @RequestParam("enquiryId") String enquiryId)
	{
		try {
			try
			{
				List<Enquiry> enquiryDetails;
				List<EnquiryFollowUp> followupList ; 
				Query query = new Query();
				followupList = mongoTemplate.find(query.addCriteria(Criteria.where("enquiryId").is(enquiryId)), EnquiryFollowUp.class);

				Query query1 = new Query();
				enquiryDetails = mongoTemplate.find(query1.addCriteria(Criteria.where("enquiryId").is(enquiryId)), Enquiry.class);

				model.addAttribute("enquiryDetails", enquiryDetails);
				model.addAttribute("followupList",followupList);
				return "EnquiryFollowUp";

			}
			catch(NumberFormatException nfe)
			{	
				return "TodayEnquiryFollowUp";
			}

		}catch (Exception e) {
			return "login";
		}
	}
	@RequestMapping(value="/EnquiryFollowUp", method=RequestMethod.POST)
	public String addEnquiryFollowUp(@ModelAttribute EnquiryFollowUp enquiryFollowUp, Model model)
	{
		try {
			try
			{
				enquiryFollowUp = new EnquiryFollowUp(enquiryFollowUp.getEnquiryId(), enquiryFollowUp.getFollowupDate(), enquiryFollowUp.getEnqRemark(), enquiryFollowUp.getEnqStatus(), enquiryFollowUp.getFollowupTakenBy(), new Date(), enquiryFollowUp.getUserName());
				enquiryFollowUpRepository.insert(enquiryFollowUp);

				Enquiry enquiry = mongoTemplate.findOne(Query.query(Criteria.where("enquiryId").is(enquiryFollowUp.getEnquiryId())), Enquiry.class);
				enquiry.setFollowupDate(enquiryFollowUp.getFollowupDate());
				enquiry.setEnqStatus(enquiryFollowUp.getEnqStatus());

				enquiryRepository.save(enquiry);

				model.addAttribute("enquiryfollowupdbStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("enquiryfollowupdbStatus","Fail");
			}


			int year = Calendar.getInstance().get(Calendar.YEAR);
			int month=Calendar.getInstance().get(Calendar.MONTH)+1;
			int day=Calendar.getInstance().get(Calendar.DATE)-1;
			int yday=day-1;
			String date1=String.valueOf(day)+"/"+String.valueOf(month)+"/"+String.valueOf(year);
			String date2=String.valueOf(yday)+"/"+String.valueOf(month)+"/"+String.valueOf(year);

			List<Enquiry> pendingenquiryList;
			Query query = new Query();
			pendingenquiryList = mongoTemplate.find(query.addCriteria(Criteria.where("followupDate").is(date1).and("enqStatus").ne("Booking")), Enquiry.class);
			List<Enquiry> pendingenquiryList1;
			Query query1 = new Query();
			pendingenquiryList1 = mongoTemplate.find(query1.addCriteria(Criteria.where("followupDate").is(date2).and("enqStatus").ne("Booking")), Enquiry.class);

			List<Enquiry> allenquiryList;
			Query query2 = new Query();
			allenquiryList = mongoTemplate.find(query2.addCriteria(Criteria.where("enqStatus").ne("Booking")), Enquiry.class);


			model.addAttribute("allenquiryList", allenquiryList);
			model.addAttribute("pendingenquiryList", pendingenquiryList);
			model.addAttribute("pendingenquiryList1", pendingenquiryList1);
			model.addAttribute("todayenquiryList", getTodayEnquiryList());

			return "TodayEnquiryFollowUp";

		}catch (Exception e) {
			return "login";
		}
	}

}

package com.realestate.controller;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.realestate.bean.Login;
import com.realestate.bean.PaymentScheduler;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.PaymentSchedulerRepository;
import com.realestate.repository.ProjectBuildingRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.ProjectWingRepository;

@Controller
@RequestMapping("/")
public class ImportNewPaymentSchedulerController {

	@Autowired
	PaymentSchedulerRepository paymentSchedulerRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	ProjectBuildingRepository projectbuildingRepository;
	@Autowired
	ProjectWingRepository projectwingRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	String paymentschedulerId, installmentNumber,customerSchedulerId;
	public String SchedulerIdGenerate()
	{
		long schedulerCount = paymentSchedulerRepository.count();

		if(schedulerCount<10)
		{
			paymentschedulerId = "PS0"+(schedulerCount+1);
		}
		else if((schedulerCount>=10) && (schedulerCount<100))
		{
			paymentschedulerId = "PS"+(schedulerCount+1);
		}

		return paymentschedulerId;
	}

	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping(value="/ImportNewPaymentScheduler")
	public String ImportNewPaymentScheduler(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		return "ImportNewPaymentScheduler";
	}


	@SuppressWarnings({ "deprecation"})
	@RequestMapping(value = "/ImportNewPaymentScheduler", method = RequestMethod.POST)
	public String uploadFlatDetails(@RequestParam("flat_excel") MultipartFile flat_excel, Model model, HttpServletRequest req, HttpServletResponse res, RedirectAttributes redirectAttributes, HttpSession session) 
	{
		String user = (String)req.getSession().getAttribute("user");

		List<Project> projectList = GetUserAssigenedProjectList(req,res);
		List<ProjectBuilding> buildingList = projectbuildingRepository.findAll();
		List<ProjectWing> wingList = projectwingRepository.findAll();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/M/yyyy");
		LocalDate localDate = LocalDate.now();
		String todayDate=dtf.format(localDate);
		String projectId="",buildingId="",wingId="";

		if (!flat_excel.isEmpty() || flat_excel != null )
		{
			try 
			{
				if(flat_excel.getOriginalFilename().endsWith("xls") || flat_excel.getOriginalFilename().endsWith("xlsx") || flat_excel.getOriginalFilename().endsWith("csv"))
				{
					if(flat_excel.getOriginalFilename().endsWith("xlsx"))
					{
						InputStream stream = flat_excel.getInputStream();
						XSSFWorkbook workbook = new XSSFWorkbook(stream);

						XSSFSheet sheet = workbook.getSheet("Sheet1");  /// this will read 1st workbook of ExcelSheet

						int firstRow = sheet.getFirstRowNum();

						XSSFRow firstrow = sheet.getRow(firstRow);

						@SuppressWarnings("unused")
						int lastColumnCount = firstrow.getLastCellNum();

						@SuppressWarnings("unused")
						Iterator<Row> rowIterator = sheet.iterator();   
						int last_no=sheet.getLastRowNum();

						PaymentScheduler paymentscheduler = new PaymentScheduler();

						for(int i=0;i<last_no;i++)
						{

							projectId="";
							buildingId="";
							wingId="";
							XSSFRow row = sheet.getRow(i+1);

							// Skip read heading 
							if (row.getRowNum() == 0) 
							{
								continue;
							}

							for(int j=0; j<projectList.size();j++)
							{
								if((projectList.get(j).getProjectName().trim()).equalsIgnoreCase((row.getCell(0).getStringCellValue().trim())))
								{
									projectId=projectList.get(j).getProjectId();
									break;
								}
							}

							for(int j=0; j<buildingList.size();j++)
							{
								if((buildingList.get(j).getBuildingName().trim()).equalsIgnoreCase((row.getCell(1).getStringCellValue().trim())))
								{
									buildingId=buildingList.get(j).getBuildingId();
									break;
								}
							}

							for(int j=0; j<wingList.size();j++)
							{
								if((wingList.get(j).getWingName().trim()).equalsIgnoreCase((row.getCell(2).getStringCellValue().trim())))
								{
									wingId=wingList.get(j).getWingId();
									break;
								}
							}


							if(!projectId.equals("") && !buildingId.equals("") && !wingId.equals(""))
							{

								paymentscheduler.setPaymentschedulerId(SchedulerIdGenerate());

								row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
								paymentscheduler.setProjectId(projectId);

								paymentscheduler.setBuildingId(buildingId);

								paymentscheduler.setWingId(wingId);

								paymentscheduler.setInstallmentNumber(row.getCell(3).getStringCellValue());

								paymentscheduler.setPaymentDecription(row.getCell(4).getStringCellValue());

								row.getCell(5).setCellType(Cell.CELL_TYPE_NUMERIC);
								paymentscheduler.setPercentage(row.getCell(5).getNumericCellValue());

								paymentscheduler.setDueDate(row.getCell(6).getStringCellValue());

								paymentscheduler.setPaidDate(row.getCell(7).getStringCellValue());

								paymentscheduler.setSlabStatus("Incompleted");
								paymentscheduler.setArchitectureSign("Incompleted");
								paymentscheduler.setEngineerSign("Incompleted");
								paymentscheduler.setMeSign("Incompleted");
								paymentscheduler.setCreationDate(todayDate);
								paymentscheduler.setUpdateDate(todayDate);
								paymentscheduler.setUserName(user);

								paymentSchedulerRepository.save(paymentscheduler);
							}
						}
						workbook.close();
					}

				}//if after inner if

			}
			catch(Exception e)
			{

			}
		}

		List<PaymentScheduler> paymentschedulerList =  paymentSchedulerRepository.findAll();
		model.addAttribute("paymentschedulerList", paymentschedulerList);
		return "redirect:PaymentSchedulerMaster";
	}

	@RequestMapping(value = "/DownloadPaymentTemplate")
	public String DownloadPaymentTemplate(Model model, HttpServletResponse response, RedirectAttributes redirectAttributes, HttpSession session) throws IOException 
	{


		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String filename = "Paymentschedule.xlsx";
		String filepath ="//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"Template/";
		response.setContentType("APPLICATION/OCTET-STREAM");
		response.setHeader("Content-Disposition", "attachment; filename=\""
				+ filename + "\"");


		FileInputStream fileInputStream = new FileInputStream(filepath + filename);

		int i;
		while ((i = fileInputStream.read()) != -1) {
			out.write(i);
		}
		fileInputStream.close();
		out.close();


		return "ImportNewPaymentScheduler";
	}
}

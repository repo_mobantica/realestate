package com.realestate.controller;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Employee;
import com.realestate.bean.Item;
import com.realestate.bean.MaterialPurchaseRequisition;
import com.realestate.bean.MaterialPurchaseRequisitionHistory;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.SubSupplierType;
import com.realestate.bean.SupplierType;
import com.realestate.configuration.CommanController;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;

@Controller
@RequestMapping("/")
public class MatrialPurchaseReportAndReceiptController 
{
	@Autowired
	MongoTemplate mongoTemplate;

	@ResponseBody
	@RequestMapping(value="/PrintMaterialPurchaseListReceipt")
	public ResponseEntity<byte[]> PrintMaterialPurchaseListReceipt(@RequestParam("requisitionId") String requisitionId, HttpServletRequest req, ModelMap model, HttpServletResponse response)
	{
		try
		{
			Query query = new Query();
			List<MaterialPurchaseRequisition> materialRequsitionDetails = mongoTemplate.find(query.addCriteria(Criteria.where("requisitionId").is(requisitionId)), MaterialPurchaseRequisition.class);

			query = new Query();
			List<MaterialPurchaseRequisitionHistory> materialRequisitioHistory = mongoTemplate.find(query.addCriteria(Criteria.where("requisitionId").is(requisitionId)), MaterialPurchaseRequisitionHistory.class);

			query = new Query();
			List<Employee> employeeList = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(materialRequsitionDetails.get(0).getEmployeeId())), Employee.class);

			int index=0;
			JasperPrint print;

			HashMap jmap = new HashMap();
			Collection c = new ArrayList();

			for(int i=0;i<materialRequisitioHistory.size();i++)
			{
				Item itemDetails = new Item();

				query = new Query();
				itemDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("itemId").is(materialRequisitioHistory.get(i).getItemId())), Item.class);

				query =new Query();
				List<SupplierType> suppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("suppliertypeId").is(itemDetails.getSuppliertypeId())), SupplierType.class);

				query =new Query();
				List<SubSupplierType> subsuppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subsuppliertypeId").is(itemDetails.getSubsuppliertypeId())), SubSupplierType.class);

				jmap = new HashMap();

				jmap.put("srno",""+(i+1));
				jmap.put("itemName",""+itemDetails.getItemName());
				jmap.put("unit",""+materialRequisitioHistory.get(i).getItemunitName());
				jmap.put("size",""+materialRequisitioHistory.get(i).getItemSize());
				jmap.put("qty",""+materialRequisitioHistory.get(i).getItemQuantity());
				jmap.put("itemType",""+suppliertypeDetails.get(0).getSupplierType());
				jmap.put("itemSubType",""+subsuppliertypeDetails.get(0).getSubsupplierType());

				c.add(jmap);
				jmap = null;
			}


			//String realPath ="//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img";
			String realPath =CommanController.GetLogoImagePath();
			JRDataSource dataSource = new JRMapCollectionDataSource(c);
			Map<String, Object> parameterMap = new HashMap<String, Object>();

			Date date = new Date();  
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
			String strDate= formatter.format(date);
			query =new Query();
			List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(materialRequsitionDetails.get(0).getProjectId())), Project.class);

			query =new Query();
			List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(materialRequsitionDetails.get(0).getBuildingId())), ProjectBuilding.class);
			query =new Query();
			List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(materialRequsitionDetails.get(0).getWingId())), ProjectWing.class);

			//Booking Receipt Generation Code
			/*1*/ parameterMap.put("requisitionApplyDate", ""+materialRequsitionDetails.get(0).getCreationDate());
			/*2*/ parameterMap.put("requisitionDate",""+materialRequsitionDetails.get(0).getRequiredDate());
			/*3*/ parameterMap.put("projectName",""+projectDetails.get(0).getProjectName());
			/*4*/ parameterMap.put("buildingName", ""+buildingDetails.get(0).getBuildingName());
			/*5*/ parameterMap.put("wingName",""+wingDetails.get(0).getWingName());
			/*6*/ parameterMap.put("employeeName",""+employeeList.get(0).getEmployeefirstName()+" "+employeeList.get(0).getEmployeemiddleName()+" "+employeeList.get(0).getEmployeelastName());
			/*8*/ parameterMap.put("realPath",realPath);

			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/MaterialPurchaseListReceipt.jasper");

			print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(print, baos);

			byte[] contents = baos.toByteArray();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			String filename = "MaterialPurchaseListReceipt.pdf";

			JasperExportManager.exportReportToPdfStream(print, baos);

			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
			ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
			response.setHeader("Content-Disposition", "inline; filename=" + filename );
			return resp;
		}
		catch(Exception e)
		{
			System.out.println("Jasper Exception = "+e.toString());
			return null;
		}

	}
}

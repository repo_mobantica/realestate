package com.realestate.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Country;
import com.realestate.bean.State;
import com.realestate.repository.CountryRepository;
import com.realestate.repository.StateRepository;

@Controller
@RequestMapping("/")
public class StateController
{
	@Autowired
	StateRepository stateRepository;
	@Autowired
	CountryRepository countryRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	//Code For Auto generation for state id
	String stateCode;

	private String StateCode()
	{
		long stateCount  = stateRepository.count();

		if(stateCount<10)
		{
			stateCode = "ST000"+(stateCount+1);
		}
		else if((stateCount>=10) && (stateCount<100))
		{
			stateCode = "ST00"+(stateCount+1);
		}
		else if((stateCount>=100) && (stateCount<1000))
		{
			stateCode = "ST0"+(stateCount+1);
		}
		else
		{
			stateCode = "ST"+(stateCount+1);
		}

		return stateCode;
	}


	//Code For Retrieving Country Names
	private List<Country> findAllCountryName()
	{
		List<Country> countryList;

		countryList = countryRepository.findAll(new Sort(Sort.Direction.ASC,"countryName"));
		return countryList;
	}


	@RequestMapping("/StateMaster")
	public String StateMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<State> stateList= stateRepository.findAll();
			List<Country> countryList= countryRepository.findAll();
			for(int i=0;i<stateList.size();i++)
			{
				try {
					for(int j=0;j<countryList.size();j++)
					{
						if(stateList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
						{
							stateList.get(i).setCountryId(countryList.get(j).getCountryName());
							break;
						}
					}
				}catch (Exception e) {
					// TODO: handle exception
				}
			}

			model.addAttribute("countryList",findAllCountryName());
			model.addAttribute("stateList", stateList);

			return "StateMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/AddState")
	public String addState(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			model.addAttribute("countryList",findAllCountryName());
			model.addAttribute("stateCode",StateCode());
			return "AddState";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value = "/AddState", method = RequestMethod.POST)
	public String stateSave(@ModelAttribute State state, Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				state = new State(state.getStateId(), state.getStateName(), state.getCountryId(), state.getCreationDate(), state.getUpdateDate(), state.getUserName());
				stateRepository.save(state);
				model.addAttribute("stateStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("stateStatus","Fail");
			}


			model.addAttribute("countryList",findAllCountryName());
			model.addAttribute("stateCode",StateCode());

			return "AddState";

		}catch (Exception e) {
			return "login";
		}
	}


	// to search all state in state master


	@ResponseBody
	@RequestMapping(value="/getSearchStateList",method=RequestMethod.POST)
	public List<State> StateList(@RequestParam("stateName") String stateName, HttpSession session)
	{
		Query query = new Query();
		List<State> stateList = mongoTemplate.find(query.addCriteria(Criteria.where("stateName").regex("^"+stateName+".*","i")), State.class);

		return stateList;
	}

	@ResponseBody
	@RequestMapping("/getallStateList")
	public List<State> AllStateList(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		List<State> stateList= stateRepository.findAll();

		List<Country> countryList= countryRepository.findAll();
		for(int i=0;i<stateList.size();i++)
		{
			try {
				for(int j=0;j<countryList.size();j++)
				{
					if(stateList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
					{
						stateList.get(i).setCountryId(countryList.get(j).getCountryName());
						break;
					}
				}
			}catch (Exception e) {
				// TODO: handle exception
			}
		}

		return stateList;
	}

	@RequestMapping("/EditState")
	public String EditState(@RequestParam("stateId") String stateId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<State> stateDetails = mongoTemplate.find(query.addCriteria(Criteria.where("stateId").is(stateId)),State.class);
			query = new Query();
			Country countryDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("countryId").is(stateDetails.get(0).getCountryId())),Country.class);

			model.addAttribute("countryList",findAllCountryName());
			model.addAttribute("countryName", countryDetails.getCountryName());
			model.addAttribute("stateDetails", stateDetails);
			return "EditState";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/EditState", method=RequestMethod.POST)
	public String EditStatePostMethod(@ModelAttribute State state, Model model)
	{
		try {
			try
			{
				state = new State(state.getStateId(), state.getStateName(), state.getCountryId(), state.getCreationDate(), state.getUpdateDate(), state.getUserName());
				stateRepository.save(state);
				model.addAttribute("stateStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("stateStatus","Fail");
			}


			List<State> stateList= stateRepository.findAll();
			List<Country> countryList= countryRepository.findAll();
			for(int i=0;i<stateList.size();i++)
			{
				try {
					for(int j=0;j<countryList.size();j++)
					{
						if(stateList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
						{
							stateList.get(i).setCountryId(countryList.get(j).getCountryName());
							break;
						}
					}
				}catch (Exception e) {
					// TODO: handle exception
				}
			}

			model.addAttribute("countryList",findAllCountryName());
			model.addAttribute("stateList", stateList);

			return "StateMaster";

		}catch (Exception e) {
			return "login";
		}
	}
}

package com.realestate.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Occupation;
import com.realestate.repository.OccupationRepository;

@Controller
@RequestMapping("/")
public class OccupationController 
{
	@Autowired
	OccupationRepository occupationRepository;

	@Autowired
	MongoTemplate mongoTemplate;
	String occupationCode;
	private String OccupationCode()
	{
		long cnt  = occupationRepository.count();
		if(cnt<10)
		{
			occupationCode = "OC000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			occupationCode = "OC00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			occupationCode = "OC0"+(cnt+1);
		}
		else
		{
			occupationCode = "OC"+(cnt+1);
		}
		return occupationCode;
	}

	@RequestMapping("/AddOccupation")
	public String addOccupation(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<Occupation> occupationList = occupationRepository.findAll();

			model.addAttribute("occupationCode",OccupationCode());
			model.addAttribute("occupationList", occupationList);
			return "AddOccupation";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value = "/AddOccupation", method = RequestMethod.POST)
	public String occupationSave(@ModelAttribute Occupation occupation, Model model)
	{
		try {
			try
			{
				occupation = new Occupation(occupation.getOccupationId(), occupation.getOccupationName(),occupation.getCreationDate(), occupation.getUpdateDate(), occupation.getUserName());
				occupationRepository.save(occupation);
				model.addAttribute("occupationStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("occupationStatus","Fail");
			}
			//List<Occupation> occupationList = occupationRepository.findAll();

			model.addAttribute("occupationCode",OccupationCode());
			//	model.addAttribute("occupationList", occupationList);
			return "AddOccupation";

		}catch (Exception e) {
			return "login";
		}
	}
	@RequestMapping("/OccupationMaster")
	public String supplierTypeMaster(ModelMap model)
	{
		try {
			List<Occupation> occupationList = occupationRepository.findAll();
			model.addAttribute("occupationList",occupationList);

			return "OccupationMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/SearchoccupationListByName")
	public List<Occupation> SearchItemUnitListByName(@RequestParam("occupationName") String occupationName)
	{
		Query query = new Query();

		List<Occupation> occupationList = mongoTemplate.find(query.addCriteria(Criteria.where("occupationName").regex("^"+occupationName+".*","i")),Occupation.class);
		return occupationList;
	}

	@ResponseBody
	@RequestMapping("/getoccupationList")
	public List<Occupation> occupationList(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		List<Occupation> occupationList= occupationRepository.findAll();

		return occupationList;
	}

	@RequestMapping("/EditOccupation")
	public String EditOccupation(@RequestParam("occupationId") String occupationId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<Occupation> occupationDetails = mongoTemplate.find(query.addCriteria(Criteria.where("occupationId").is(occupationId)), Occupation.class);

			model.addAttribute("occupationDetails", occupationDetails);
			return "EditOccupation";

		}catch (Exception e) {
			return "login";
		}
	}
	@RequestMapping(value="/EditOccupation", method=RequestMethod.POST)
	public String EditOccupationPostMethod(@ModelAttribute Occupation occupation, Model model)
	{
		try {
			try
			{
				occupation = new Occupation(occupation.getOccupationId(), occupation.getOccupationName(),occupation.getCreationDate(), occupation.getUpdateDate(), occupation.getUserName());
				occupationRepository.save(occupation);
				model.addAttribute("occupationStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("occupationStatus","Fail");
			}

			List<Occupation> occupationList = occupationRepository.findAll();
			model.addAttribute("occupationList",occupationList);

			return "OccupationMaster";

		}catch (Exception e) {
			return "login";
		}
	}
}

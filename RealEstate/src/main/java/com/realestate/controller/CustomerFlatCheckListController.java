package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Booking;
import com.realestate.bean.BookingCancelForm;
import com.realestate.bean.Contractor;
import com.realestate.bean.ContractorEmployees;
import com.realestate.bean.AddMoreCheckListDetials;
import com.realestate.bean.Aggreement;
import com.realestate.bean.CustomerFlatCheckList;
import com.realestate.bean.CustomerLoanDetails;
import com.realestate.bean.Designation;
import com.realestate.bean.Enquiry;
import com.realestate.bean.ExtraCharges;
import com.realestate.bean.Flat;
import com.realestate.bean.Floor;
import com.realestate.bean.Login;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.AddMoreCheckListDetialsRepository;
import com.realestate.repository.AgentRepository;
import com.realestate.repository.AggreementRepository;
import com.realestate.repository.BookingCancelFormRepository;
import com.realestate.repository.CustomerFlatCheckListRepository;
import com.realestate.repository.CustomerLoanDetailsRepository;
import com.realestate.repository.EnquiryRepository;
import com.realestate.repository.ExtraChargeRepository;
import com.realestate.repository.GeneratePaymentSchedulerRepository;
import com.realestate.repository.ProjectRepository;

@Controller
@RequestMapping("/")
public class CustomerFlatCheckListController
{

	@Autowired 
	AggreementRepository aggreementRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	AddMoreCheckListDetialsRepository addmorechecklistdetialsrepository;
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	CustomerFlatCheckListRepository customerflatchecklistRepository;
	@Autowired
	AgentRepository agentRepository;
	@Autowired
	EnquiryRepository enquiryRepository;
	@Autowired
	GeneratePaymentSchedulerRepository generatepaymentschedulerRepository;
	@Autowired 
	ExtraChargeRepository extrachargesRepository;
	@Autowired
	CustomerLoanDetailsRepository customerLoandetailsRepository;
	@Autowired
	BookingCancelFormRepository bookingcancelformRepository;
	String checklistCode;
	private String CheckListCode()
	{
		long cnt  = customerflatchecklistRepository.count();
		if(cnt<10)
		{
			checklistCode = "CL000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			checklistCode = "CL00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			checklistCode = "CL0"+(cnt+1);
		}
		else 
		{
			checklistCode = "CL"+(cnt+1);
		}
		return checklistCode;
	}

	String morechecklistCode;
	private String MoreCheckListCode()
	{
		long cnt  = addmorechecklistdetialsrepository.count();
		if(cnt<10)
		{
			morechecklistCode = "MCL000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			morechecklistCode = "MCL00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			morechecklistCode = "MCL0"+(cnt+1);
		}
		else if(cnt>=1000)
		{
			morechecklistCode = "MCL"+(cnt+1);
		}
		return morechecklistCode;
	}

	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping("/CustomerFlatCheckList")
	public String CustomerFlatCheckList(@RequestParam("bookingId") String bookingId, ModelMap model)
	{
try {
		Query query = new Query();
		Query query1 = new Query();
		List<Booking> bookingDetails; 
		bookingDetails = mongoTemplate.find(query1.addCriteria(Criteria.where("bookingId").is(bookingId)), Booking.class);
		List<Aggreement> aggreementDetails; 
		aggreementDetails = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)), Aggreement.class);

		query =new Query();
		List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(bookingDetails.get(0).getProjectId())), Project.class);

		query =new Query();
		List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(bookingDetails.get(0).getBuildingId())), ProjectBuilding.class);

		query =new Query();
		List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(bookingDetails.get(0).getWingId())), ProjectWing.class);

		query =new Query();
		List<Floor> floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("floorId").is(bookingDetails.get(0).getFloorId())), Floor.class);

		query =new Query();
		List<Flat> flatDetails = mongoTemplate.find(query.addCriteria(Criteria.where("flatId").is(bookingDetails.get(0).getFlatId())), Flat.class);

		model.addAttribute("projectName",projectDetails.get(0).getProjectName());
		model.addAttribute("buildingName",buildingDetails.get(0).getBuildingName());
		model.addAttribute("wingName",wingDetails.get(0).getWingName());
		model.addAttribute("floortypeName",floorDetails.get(0).getFloortypeName());
		model.addAttribute("flatNumber",flatDetails.get(0).getFlatNumber());

		model.addAttribute("aggreementDetails",aggreementDetails);
		model.addAttribute("bookingDetails",bookingDetails); 
		model.addAttribute("checklistCode",CheckListCode());

		String checkListId = CheckListCode();
		Query query2 = new Query();
		Query query3 = new Query();
		List<AddMoreCheckListDetials> moreList = mongoTemplate.find(query2.addCriteria(Criteria.where("checkListId").is(checkListId)), AddMoreCheckListDetials.class);

		if(moreList.size()!=0)
		{
			mongoTemplate.remove(query3.addCriteria(Criteria.where("checkListId").is(checkListId)), AddMoreCheckListDetials.class);	
		}

		return "CustomerFlatCheckList";
		
		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value = "/CustomerFlatCheckList", method = RequestMethod.POST)
	public String SaveCustomerFlatCheckList(@ModelAttribute CustomerFlatCheckList customerflatchecklist, Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
	
		try
		{
			customerflatchecklist = new CustomerFlatCheckList(customerflatchecklist.getCheckListId(), customerflatchecklist.getBookingId(), 
					customerflatchecklist.getAggreementId(), customerflatchecklist.getCeilings(), customerflatchecklist.getTiles(), 
					customerflatchecklist.getDoor(), customerflatchecklist.getWindows(), customerflatchecklist.getPlumbingAndsanitory(), customerflatchecklist.getElectrification(),
					customerflatchecklist.getPainting(), customerflatchecklist.getPropertyTaxNo(), customerflatchecklist.getElectricBillMeterNo(), 
					customerflatchecklist.getCreationDate(), customerflatchecklist.getUpdateDate(), customerflatchecklist.getUserName());
			customerflatchecklistRepository.save(customerflatchecklist);
			model.addAttribute("Status","Success");
		}
		catch(DuplicateKeyException de)
		{
			model.addAttribute("Status","Fail");
		}

		List<Enquiry> enquiryList;
		List<Project> projectList = GetUserAssigenedProjectList(req,res); 
		List<Booking> bookingList; 
		List<Aggreement> aggreementList = aggreementRepository.findAll();
		List<ExtraCharges> extrachargesList = extrachargesRepository.findAll(); 
		List<CustomerLoanDetails> loanList = customerLoandetailsRepository.findAll();
		List<BookingCancelForm> cancelList = bookingcancelformRepository.findAll();
		List<CustomerFlatCheckList> CustomercheckList = customerflatchecklistRepository.findAll();
		Query query = new Query();
		enquiryList = mongoTemplate.find(query.addCriteria(Criteria.where("enqStatus").is("Booking")), Enquiry.class);
		Query query1 = new Query();
		bookingList = mongoTemplate.find(query1.addCriteria(Criteria.where("bookingstatus").ne("Cancel")), Booking.class);

		model.addAttribute("CustomercheckList",CustomercheckList);
		model.addAttribute("aggreementList",aggreementList);
		model.addAttribute("cancelList",cancelList);
		model.addAttribute("loanList",loanList);
		model.addAttribute("extrachargesList",extrachargesList);
		model.addAttribute("bookingList", bookingList);
		model.addAttribute("projectList",projectList);
		model.addAttribute("enquiryList", enquiryList);
		return "BookingMaster";
		
		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/AddMoreCheckListDetails")
	public List<AddMoreCheckListDetials> AddMoreCheckListDetails(@RequestParam("checkListId") String checkListId, @RequestParam("bookingId") String bookingId, @RequestParam("aggreementId") String aggreementId, @RequestParam("doorType") String doorType, @RequestParam("noOfkeySet") String noOfkeySet, @RequestParam("keyNo") String keyNo, HttpSession session)
	{
		try
		{
			AddMoreCheckListDetials morecheckListdetials = new AddMoreCheckListDetials();
			morecheckListdetials.setTypeId(MoreCheckListCode());
			morecheckListdetials.setCheckListId(checkListId);
			morecheckListdetials.setBookingId(bookingId);
			morecheckListdetials.setAggreementId(aggreementId);
			morecheckListdetials.setDoorType(doorType);
			morecheckListdetials.setNoOfkeySet(noOfkeySet);
			morecheckListdetials.setKeyNo(keyNo);

			addmorechecklistdetialsrepository.save(morecheckListdetials);

			Query query = new Query();
			List<AddMoreCheckListDetials> morecheckListdetialsList = mongoTemplate.find(query.addCriteria(Criteria.where("checkListId").is(checkListId)), AddMoreCheckListDetials.class);

			return morecheckListdetialsList;
		}
		catch(Exception e)
		{

			return null;
		}

	}

	@ResponseBody
	@RequestMapping("/DeleteMoreCheckListDetails")
	public List<AddMoreCheckListDetials> DeleteMoreCheckListDetails(@RequestParam("checkListId") String checkListId, @RequestParam("typeId") String typeId, HttpSession session)
	{
		try
		{
			Query query = new Query();
			mongoTemplate.remove(query.addCriteria(Criteria.where("checkListId").is(checkListId).and("typeId").is(typeId)), AddMoreCheckListDetials.class);

			query = new Query();
			List<AddMoreCheckListDetials> morecheckListdetialsList = mongoTemplate.find(query.addCriteria(Criteria.where("checkListId").is(checkListId)), AddMoreCheckListDetials.class);
			return morecheckListdetialsList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping("/EditCustomerFlatCheckList")
	public String EditCustomerFlatCheckList(@RequestParam("bookingId") String bookingId, ModelMap model)
	{
		Query query = new Query();
		List<CustomerFlatCheckList> customerflatchecklistDetails =  mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)),CustomerFlatCheckList.class);
		query = new Query();
		List<AddMoreCheckListDetials> morecheckListdetialsList = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)), AddMoreCheckListDetials.class);
		List<Booking> bookingDetails; 
		query = new Query();
		bookingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)), Booking.class);

		query =new Query();
		List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(bookingDetails.get(0).getProjectId())), Project.class);

		query =new Query();
		List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(bookingDetails.get(0).getBuildingId())), ProjectBuilding.class);

		query =new Query();
		List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(bookingDetails.get(0).getWingId())), ProjectWing.class);

		query =new Query();
		List<Floor> floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("floorId").is(bookingDetails.get(0).getFloorId())), Floor.class);

		query =new Query();
		List<Flat> flatDetails = mongoTemplate.find(query.addCriteria(Criteria.where("flatId").is(bookingDetails.get(0).getFlatId())), Flat.class);

		List<Aggreement> aggreementDetails;
		query = new Query();
		aggreementDetails = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)), Aggreement.class);


		model.addAttribute("projectName",projectDetails.get(0).getProjectName());
		model.addAttribute("buildingName",buildingDetails.get(0).getBuildingName());
		model.addAttribute("wingName",wingDetails.get(0).getWingName());
		model.addAttribute("floortypeName",floorDetails.get(0).getFloortypeName());
		model.addAttribute("flatNumber",flatDetails.get(0).getFlatNumber());

		model.addAttribute("aggreementDetails",aggreementDetails);
		model.addAttribute("bookingDetails",bookingDetails);
		model.addAttribute("customerflatchecklistDetails", customerflatchecklistDetails);
		model.addAttribute("morecheckListdetialsList", morecheckListdetialsList);
		return "EditCustomerFlatCheckList";
	}	


	@RequestMapping(value="/EditCustomerFlatCheckList", method=RequestMethod.POST)
	public String SaveEditCustomerFlatCheckList(@ModelAttribute CustomerFlatCheckList customerflatchecklist, Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try
		{
			customerflatchecklist = new CustomerFlatCheckList(customerflatchecklist.getCheckListId(), customerflatchecklist.getBookingId(), 
					customerflatchecklist.getAggreementId(), customerflatchecklist.getCeilings(), customerflatchecklist.getTiles(), 
					customerflatchecklist.getDoor(), customerflatchecklist.getWindows(), customerflatchecklist.getPlumbingAndsanitory(), customerflatchecklist.getElectrification(),
					customerflatchecklist.getPainting(), customerflatchecklist.getPropertyTaxNo(), customerflatchecklist.getElectricBillMeterNo(), 
					customerflatchecklist.getCreationDate(), customerflatchecklist.getUpdateDate(), customerflatchecklist.getUserName());
			customerflatchecklistRepository.save(customerflatchecklist);
			model.addAttribute("Status","Success");
		}
		catch(DuplicateKeyException de)
		{
			model.addAttribute("Status","Fail");
		}
		List<Enquiry> enquiryList;
		List<Project> projectList = GetUserAssigenedProjectList(req,res); 
		List<Booking> bookingList; 
		List<Aggreement> aggreementList = aggreementRepository.findAll();
		List<ExtraCharges> extrachargesList = extrachargesRepository.findAll(); 
		List<CustomerLoanDetails> loanList = customerLoandetailsRepository.findAll();
		List<BookingCancelForm> cancelList = bookingcancelformRepository.findAll();
		List<CustomerFlatCheckList> CustomercheckList = customerflatchecklistRepository.findAll();
		Query query = new Query();
		enquiryList = mongoTemplate.find(query.addCriteria(Criteria.where("enqStatus").is("Booking")), Enquiry.class);
		Query query1 = new Query();
		bookingList = mongoTemplate.find(query1.addCriteria(Criteria.where("bookingstatus").ne("Cancel")), Booking.class);

		model.addAttribute("CustomercheckList",CustomercheckList);
		model.addAttribute("aggreementList",aggreementList);
		model.addAttribute("cancelList",cancelList);
		model.addAttribute("loanList",loanList);
		model.addAttribute("extrachargesList",extrachargesList);
		model.addAttribute("bookingList", bookingList);
		model.addAttribute("projectList",projectList);
		model.addAttribute("enquiryList", enquiryList);
		return "BookingMaster";
	}
}

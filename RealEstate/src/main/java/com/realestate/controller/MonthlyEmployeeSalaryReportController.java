package com.realestate.controller;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.City;
import com.realestate.bean.Company;
import com.realestate.bean.Department;
import com.realestate.bean.Designation;
import com.realestate.bean.Employee;
import com.realestate.bean.EmployeeSalaryDetails;
import com.realestate.bean.LocationArea;
import com.realestate.bean.Login;
import com.realestate.bean.MonthlyEmployeeSalaryReport;
import com.realestate.bean.NumberToWordConversion;
import com.realestate.repository.BankRepository;
import com.realestate.repository.CompanyRepository;
import com.realestate.repository.CountryRepository;
import com.realestate.repository.DepartmentRepository;
import com.realestate.repository.EmployeeRepository;
import com.realestate.repository.EmployeeSalaryDetailsRepository;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;

@Controller
@RequestMapping("/")
public class MonthlyEmployeeSalaryReportController {

	@Autowired
	EmployeeRepository employeeRepository;
	@Autowired
	CountryRepository countryRepository;
	@Autowired
	BankRepository bankRepository;
	@Autowired
	DepartmentRepository departmentRepository;
	@Autowired
	CompanyRepository companyRepository;
	@Autowired
	EmployeeSalaryDetailsRepository employeesalarydetailsRepository;
	@Autowired
	MongoTemplate mongoTemplate;


	@RequestMapping("/MonthlySalaryReport")
	public String MonthlySalaryReport(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			return "MonthlySalaryReport";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping(value = "/MonthlySalaryReport", method = RequestMethod.POST)
	public ResponseEntity<byte[]> PrintEmployeeSalarySlip(@RequestParam("month") String month, @RequestParam("year") String year, ModelMap model, HttpServletRequest req, HttpServletResponse res, HttpServletResponse response)
	{

		int index=0;
		JasperPrint print;
		try 
		{	

			Query query = new Query();
			String userId = (String)req.getSession().getAttribute("user");

			query = new Query();
			List<Login> loginDetails = mongoTemplate.find(query.addCriteria(Criteria.where("userId").is(userId)),Login.class);

			query = new Query();
			List<Employee> userDetails = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.get(0).getEmployeeId())),Employee.class);

			Collection c = new ArrayList();
			HashMap jmap = new HashMap();

			SimpleDateFormat createDateFormat = new SimpleDateFormat("d/M/yyyy");
			List<Company> companyDetails= companyRepository.findAll();
			try {
				query =new Query();
				City companyCityDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("cityId").is(companyDetails.get(0).getCityId())), City.class);
				companyDetails.get(0).setCityId(companyCityDetails.getCityName());
				query =new Query();
				LocationArea companyLocationareaDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("locationareaId").is(companyDetails.get(0).getLocationareaId())), LocationArea.class);
				companyDetails.get(0).setLocationareaId(companyLocationareaDetails.getLocationareaName());
			}catch (Exception e) {			}

			jmap = null;

			Date date = new Date();  
			String todayDate= createDateFormat.format(date);

			Calendar cal = Calendar.getInstance();
			int newYear = Integer.parseInt(year);
			int newMonth = Integer.parseInt(month);
			int totaDayInMonth = cal.get(Calendar.DAY_OF_MONTH);	


			query = new Query();
			List<Employee> employeeList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("Active")),Employee.class);

			MonthlyEmployeeSalaryReport monthlyemployeesalaryReport=new MonthlyEmployeeSalaryReport();
			List<MonthlyEmployeeSalaryReport> monthlyemployeesalaryReportList=new ArrayList<MonthlyEmployeeSalaryReport>();

			query = new Query();
			List<EmployeeSalaryDetails> employeesalaryDetails = mongoTemplate.find(query.addCriteria(Criteria.where("month").is(newMonth).and("year").is(newYear)),EmployeeSalaryDetails.class);

			int flag=0;
			for(int i=0;i<employeeList.size();i++)
			{
				flag=0;
				monthlyemployeesalaryReport=new MonthlyEmployeeSalaryReport();
				monthlyemployeesalaryReport.setEmployeeId(employeeList.get(i).getEmployeeId());
				monthlyemployeesalaryReport.setEmployeeName(employeeList.get(i).getEmployeefirstName()+" "+employeeList.get(i).getEmployeelastName());
				monthlyemployeesalaryReport.setBasicPay(employeeList.get(i).getEmployeeBasicpay());
				monthlyemployeesalaryReport.setHra(employeeList.get(i).getEmployeeHra());

				for(int j=0;j<employeesalaryDetails.size();j++)
				{
					if(employeeList.get(i).getEmployeeId().equals(employeesalaryDetails.get(j).getEmployeeId()))
					{

						monthlyemployeesalaryReport.setLeaveDeduction(employeesalaryDetails.get(j).getLeaveDeduction());
						monthlyemployeesalaryReport.setPfAmount(employeesalaryDetails.get(j).getProvidentFund());
						monthlyemployeesalaryReport.setEsi(employeesalaryDetails.get(j).getEsiAmount());
						monthlyemployeesalaryReport.setLoanAmount(employeesalaryDetails.get(j).getLoanAmount());
						monthlyemployeesalaryReport.setPrefesionTax(employeesalaryDetails.get(j).getProfessionalTax());
						monthlyemployeesalaryReport.setTdsAmount(employeesalaryDetails.get(j).getTds());
						flag=1;
						break;
					}

				}
				if(flag==1)
				{
					monthlyemployeesalaryReport.setSalarySatus("Paid");
				}
				else
				{
					monthlyemployeesalaryReport.setSalarySatus("Remaining");
				}
				monthlyemployeesalaryReportList.add(monthlyemployeesalaryReport);

			}


			String month1="";

			if(newMonth==1)
			{
				month1="January";
			}
			else if(newMonth==2)
			{
				month1="February";
			}
			else if(newMonth==3)
			{
				month1="March";
			}
			else if(newMonth==4)
			{
				month1="April";
			}
			else if(newMonth==5)
			{
				month1="May";
			}
			else if(newMonth==6)
			{
				month1="June";
			}
			else if(newMonth==7)
			{
				month1="July";
			}
			else if(newMonth==8)
			{
				month1="August";
			}
			else if(newMonth==9)
			{
				month1="September";
			}
			else if(newMonth==10)
			{
				month1="October";
			}
			else if(newMonth==11)
			{
				month1="November";
			}
			else if(newMonth==12)
			{
				month1="December";
			}

			for(int i=0;i<monthlyemployeesalaryReportList.size();i++)
			{
				jmap = new HashMap();

				jmap.put("srno",""+(i+1));
				jmap.put("employeeId",""+monthlyemployeesalaryReportList.get(i).getEmployeeId());

				jmap.put("employeeName",""+monthlyemployeesalaryReportList.get(i).getEmployeeName());
				jmap.put("presentDay",""+monthlyemployeesalaryReportList.get(i).getPresentDay());
				jmap.put("basicPay",""+monthlyemployeesalaryReportList.get(i).getBasicPay());
				jmap.put("hra",""+monthlyemployeesalaryReportList.get(i).getHra());
				jmap.put("leaveDeduction",""+monthlyemployeesalaryReportList.get(i).getLeaveDeduction());
				jmap.put("pfAmount",""+monthlyemployeesalaryReportList.get(i).getPfAmount());
				jmap.put("esi",""+monthlyemployeesalaryReportList.get(i).getEsi());
				jmap.put("loanAmount",""+monthlyemployeesalaryReportList.get(i).getLoanAmount());
				jmap.put("prefesionTax",""+monthlyemployeesalaryReportList.get(i).getPrefesionTax());
				jmap.put("tdsAmount",""+monthlyemployeesalaryReportList.get(i).getTdsAmount());
				jmap.put("salarySatus",""+monthlyemployeesalaryReportList.get(i).getSalarySatus());

				c.add(jmap);
				jmap = null;
			}

			JRDataSource dataSource = new JRMapCollectionDataSource(c);
			Map<String, Object> parameterMap = new HashMap();

			String currentMonth=month1+" "+year+" Month Salary Report";

			parameterMap.put("date", ""+todayDate);

			parameterMap.put("currentMonth",""+currentMonth);

			parameterMap.put("companyName", ""+companyDetails.get(0).getCompanyName());

			parameterMap.put("companyAddress", ""+companyDetails.get(0).getCompanyAddress()+", "+companyDetails.get(0).getLocationareaId()+", "+companyDetails.get(0).getCityId());

			parameterMap.put("userName", ""+userDetails.get(0).getEmployeefirstName()+" "+userDetails.get(0).getEmployeelastName());


			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/MonthlySalaryReport.jasper");

			print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(print, baos);

			byte[] contents = baos.toByteArray();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			String filename = "MonthlySalaryReport.pdf";

			JasperExportManager.exportReportToPdfStream(print, baos);

			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
			ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
			response.setHeader("Content-Disposition", "inline; filename=" + filename );
			return resp;				    
		}
		catch(Exception ex)
		{
			return null;
		}

	}

}

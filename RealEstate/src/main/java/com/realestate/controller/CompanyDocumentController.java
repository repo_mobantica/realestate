package com.realestate.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.realestate.bean.Company;
import com.realestate.repository.CompanyDocumentsRepository;
import com.realestate.repository.CompanyRepository;
import com.realestate.bean.CompanyDocuments;
import com.realestate.configuration.CommanController;

@Controller
@RequestMapping("/")
public class CompanyDocumentController
{
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	CompanyRepository companyRepository;
	@Autowired
	CompanyDocumentsRepository companydocumentsRepository;

	@RequestMapping("/CompanyDocumentMaster")
	public String CompanyDocumentMaster(Model model)
	{
		List<Company> companyList = companyRepository.findAll();

		model.addAttribute("companyList", companyList);
		return "CompanyDocumentMaster";
	}

	@RequestMapping("/AddCompanyDocuments")
	public String CompanyDocuments(@RequestParam("companyId") String companyId, Model model)
	{
		try {
		Query query = new Query();
		Company companyDetails =  mongoTemplate.findOne(query.addCriteria(Criteria.where("companyId").is(companyId)), Company.class);

		query = new Query();
		List<CompanyDocuments> documentList = mongoTemplate.find(query.addCriteria(Criteria.where("companyId").is(companyId)), CompanyDocuments.class);

		model.addAttribute("companyDetails", companyDetails);
		model.addAttribute("companyId", companyId);
		model.addAttribute("documentList", documentList);

		return "AddCompanyDocuments";
		
		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/checkDocumentFormat")
	public String getFileExtension(@RequestParam("fileName") String fileName)
	{
		try
		{
			int dotIndex = fileName.lastIndexOf('.');
			String extension =  (dotIndex == -1) ? "" : fileName.substring(dotIndex + 1);
			if(extension.equalsIgnoreCase("pdf") || extension.equalsIgnoreCase("jpg") || extension.equalsIgnoreCase("jpeg") || extension.equalsIgnoreCase("png"))
			{
				return "OK";
			}
			else
			{
				return "NOT OK";
			}
		}
		catch(Exception e)
		{
			e.toString();

			return "NOT OK";
		}
	}

	public String knowFileExtension(@RequestParam("fileName") String fileName)
	{
		try
		{
			String fileName1 = new File(fileName).getName();
			int dotIndex = fileName1.lastIndexOf('.');
			String extension =  (dotIndex == -1) ? "" : fileName.substring(dotIndex + 1);

			return extension;
		}
		catch(Exception e)
		{
			e.toString();

			return null;
		}
	}


	public boolean UploadFileToPath(MultipartFile file, String path)
	{
		try
		{
			if (!file.isEmpty())
			{

				try {

					byte[] bytes = file.getBytes();
					BufferedOutputStream stream =new BufferedOutputStream(new FileOutputStream(new File(path)));
					stream.write(bytes);
					stream.flush();
					stream.close();

				}catch (Exception e) {
					System.out.println("Eooror = "+e);
					// TODO: handle exception
				}

			}
		}
		catch(Exception e)
		{

		}

		File f = new File(path);

		if (f.exists() && !f.isDirectory()) 
		{
			return true;
		} 
		else 
		{
			return false;
		}
	}

	@RequestMapping(value="/uploadCompanyDocument",method=RequestMethod.POST)
	public String uploadCompanyDocument(@ModelAttribute CompanyDocuments documentDetails,@RequestParam("document") MultipartFile file, Model model)
	{
		try {
		String extension="."+knowFileExtension(file.getOriginalFilename());
		try
		{
			String path = CommanController.GetCompanyDocumentPath() + documentDetails.getCompanyId()+"_"+documentDetails.getDocumentNumber()+"."+knowFileExtension(file.getOriginalFilename());
			//String path="D:/Data1/" + documentDetails.getCompanyId()+"_"+documentDetails.getDocumentNumber()+"."+knowFileExtension(file.getOriginalFilename());
			UploadFileToPath(file, path);
		}
		catch(Exception e)
		{
			System.out.println("1.File upload exception = "+e.toString());
		}

		try
		{
			documentDetails = new CompanyDocuments(documentDetails.getCompanyId()+"_"+documentDetails.getDocumentNumber(), documentDetails.getCompanyId(), documentDetails.getDocumentNumber(), extension , documentDetails.getCreationDate(), documentDetails.getUpdateDate());
			companydocumentsRepository.save(documentDetails);
			model.addAttribute("documentStatus", "Success");
		}
		catch(Exception e)
		{

		}

		Query query = new Query();
		List<CompanyDocuments> documentList = mongoTemplate.find(query.addCriteria(Criteria.where("companyId").is(documentDetails.getCompanyId())), CompanyDocuments.class);

		model.addAttribute("documentList", documentList);
		model.addAttribute("companyId", documentDetails.getCompanyId());
		return "AddCompanyDocuments";
		
		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/ViewCompanyDocument")
	public String ViewCompanyDocument(@RequestParam("documentId") String documentId, Model model)
	{
		try {
		Query query = new Query();
		CompanyDocuments documentDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("documentId").is(documentId)), CompanyDocuments.class);

		String fileAddress = CommanController.GetCompanyDocumentPath()+documentId+documentDetails.getExtension();
		//String fileAddress ="D:/Data1/" +documentId+documentDetails.getExtension();
		if(documentDetails.getExtension().equals(".jpg") || documentDetails.getExtension().equals(".jpeg") || documentDetails.getExtension().equals(".png"))
		{
			model.addAttribute("fileAddress", fileAddress);
			return "DisplayImage";	
		}	
		else
		{
			model.addAttribute("fileAddress", fileAddress);
			return "DisplayPDF";
		}
		
		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("GetDocument")
	public void GetImageForEdit(@RequestParam("fileAddress") String fileAddress,HttpServletResponse response)
	{

		try
		{
			if(fileAddress !=null && !fileAddress.equals(""))
			{

				File file=new File(fileAddress);

				if(file.exists())
				{
					FileInputStream input=new FileInputStream(file);
					byte[] outputFile=IOUtils.toByteArray(input);
					response.getOutputStream().write(outputFile);	
				}
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	@ResponseBody
	@RequestMapping("/DeleteCompanyDocument")
	public List<CompanyDocuments> DeleteCompanyDocument(@RequestParam("documentId") String documentId, @RequestParam("companyId") String companyId , Model model)
	{
		Query query = new Query();

		try
		{
			CompanyDocuments documentDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("documentId").is(documentId)), CompanyDocuments.class);

			String fileAddress = CommanController.GetCompanyDocumentPath()+documentId+documentDetails.getExtension();

			File file = new File(fileAddress);

			file.delete();
		}
		catch(Exception e)
		{
			System.out.println("File Delete Exception = "+e.toString());
		}

		try
		{
			query = new Query();
			mongoTemplate.remove(query.addCriteria(Criteria.where("documentId").is(documentId)), CompanyDocuments.class);
		}
		catch(Exception e)
		{
			System.out.println("Record delete exception = "+e.toString());
		}

		query = new Query();
		List<CompanyDocuments> documentList = mongoTemplate.find(query.addCriteria(Criteria.where("companyId").is(companyId)), CompanyDocuments.class);

		return documentList;
	}
}

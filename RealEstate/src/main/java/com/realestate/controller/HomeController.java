package com.realestate.controller;

import java.text.DateFormat;  
import java.text.SimpleDateFormat;  
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Employee;
import com.realestate.bean.Login;
import com.realestate.services.EmailService;

import org.springframework.ui.ModelMap;
import com.realestate.bean.Booking;
import com.realestate.bean.BookingCancelForm;
import com.realestate.bean.City;
import com.realestate.bean.Country;
import com.realestate.bean.Enquiry;
import com.realestate.bean.Flat;
import com.realestate.bean.Floor;
import com.realestate.bean.LocationArea;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.State;
import com.realestate.bean.Task;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.BookingRepository;
import com.realestate.repository.BudgetRepository;
import com.realestate.repository.CityRepository;
import com.realestate.repository.CountryRepository;
import com.realestate.repository.EmployeeRepository;
import com.realestate.repository.EnquiryRepository;
import com.realestate.repository.LocationAreaRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.StateRepository;
import com.realestate.repository.TaskRepository;
import com.realestate.response.HomeGraphDetails;
import com.realestate.response.ProjectGraghDetails;

@Controller
@RequestMapping("/")
public class HomeController extends EmailService
{
	@Autowired
	LocationAreaRepository locationAreaRepository;
	@Autowired
	CityRepository cityRepository;
	@Autowired
	StateRepository stateRepository;
	@Autowired
	CountryRepository countryRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	EnquiryRepository enquiryRepository;
	@Autowired
	EmployeeRepository employeeRepository;
	@Autowired
	TaskRepository taskRepository;
	@Autowired
	BookingRepository bookingRepository;
	@Autowired
	MongoTemplate mongoTemplate;


	@Autowired
	private JavaMailSender mailSender;

	private long TotalEnquiry()
	{
		long totalenquiry=0;
		Date todayDate = new Date();

		final Calendar calendar = Calendar.getInstance();
		calendar.setTime(todayDate);
		calendar.add(Calendar.DAY_OF_YEAR, -1);
		Date date= calendar.getTime();

		Query query = new Query();
		totalenquiry = mongoTemplate.count(query.addCriteria(Criteria.where("creationDate").gte(date).lt(todayDate)), Enquiry.class);
		return totalenquiry;
	}
	private long TotalBooking(HttpServletRequest req, HttpServletResponse res)
	{
		long totalbooking=0;
		Date todayDate = new Date();

		try{

			final Calendar calendar = Calendar.getInstance();
			calendar.setTime(todayDate);
			calendar.add(Calendar.DAY_OF_YEAR, -1);
			Date date= calendar.getTime(); 

			List<Project> projectList = GetUserAssigenedProjectList(req,res); 

			Query query = new Query();

			for(int i=0;i<projectList.size();i++)
			{
				query = new Query();	
				totalbooking = totalbooking + mongoTemplate.count(query.addCriteria(Criteria.where("projectId").is(projectList.get(i).getProjectId()).and("creationDate").gte(date).lt(todayDate)), Booking.class);
			}	

		}catch (Exception e) {
			// TODO: handle exception
		}

		return totalbooking;
	}

	private long TotalSiteVisited()
	{
		long totalsitevisited=0;
		DateFormat dateFormat = new SimpleDateFormat("d/M/yyyy");
		Date date = new Date();
		String todayDate =dateFormat.format(date);
		//System.out.println(dateFormat.format(date));
		Query query = new Query();
		totalsitevisited = mongoTemplate.count(query.addCriteria(Criteria.where("creationDate").is(todayDate).and("enqStatus").is("Site Visited")), Enquiry.class);
		return totalsitevisited;
	}

	private long TotalCancel()
	{
		long totalCancel=0;
		DateFormat dateFormat = new SimpleDateFormat("d/M/yyyy");
		Date date = new Date();
		String todayDate =dateFormat.format(date);
		//System.out.println(dateFormat.format(date));
		Query query = new Query();
		totalCancel = mongoTemplate.count(query.addCriteria(Criteria.where("creationDate").is(todayDate)), BookingCancelForm.class);
		return totalCancel;
	}

	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);

			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}

			List<LocationArea> locationareaList= locationAreaRepository.findAll();
			List<City> cityList = cityRepository.findAll();
			List<State> stateList= stateRepository.findAll();
			List<Country> countryList= countryRepository.findAll();

			int i,j;
			for(i=0;i<projectList.size();i++)
			{
				try 
				{
					for(j=0;j<countryList.size();j++)
					{
						if(projectList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
						{
							projectList.get(i).setCountryId(countryList.get(j).getCountryName());
							break;
						}
					}

					for(j=0;j<stateList.size();j++)
					{
						if(projectList.get(i).getStateId().equals(stateList.get(j).getStateId()))
						{
							projectList.get(i).setStateId(stateList.get(j).getStateName());
							break;
						}
					}

					for(j=0;j<cityList.size();j++)
					{
						if(projectList.get(i).getCityId().equals(cityList.get(j).getCityId()))
						{
							projectList.get(i).setCityId(cityList.get(j).getCityName());
							break;
						}
					}

					for(j=0;j<locationareaList.size();j++)
					{
						if(projectList.get(i).getLocationareaId().equals(locationareaList.get(j).getLocationareaId()))
						{
							projectList.get(i).setLocationareaId(locationareaList.get(j).getLocationareaName());
							break;
						}
					}

				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}

			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	//Request Mapping For Home
	@RequestMapping("/home")
	public String homeRedirect(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<Project> projectList = GetUserAssigenedProjectList(req,res); 

			List<Employee> employeeList = employeeRepository.findAll();
			Query query = new Query();
			List<Task> taskDetails = mongoTemplate.find(query.addCriteria(Criteria.where("taskStatus").ne("Complete")), Task.class);

			try
			{
				for(int i=0; i<taskDetails.size();i++)
				{
					query = new Query();
					if(!taskDetails.get(i).getProjectId().equals("ALL"))
					{
						query = new Query();
						Project projectDetails = new Project();
						projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(taskDetails.get(i).getProjectId())), Project.class);
						taskDetails.get(i).setProjectName(projectDetails.getProjectName());
					}
					else
					{
						taskDetails.get(i).setProjectName("ALL");	
					}

					if(!taskDetails.get(i).getBuildingId().equals("ALL"))
					{
						query = new Query();
						ProjectBuilding projectBuilding = new ProjectBuilding();
						projectBuilding = mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(taskDetails.get(i).getBuildingId())), ProjectBuilding.class);
						taskDetails.get(i).setBuildingName(projectBuilding.getBuildingName());
					}
					else
					{
						taskDetails.get(i).setBuildingName("ALL");
					}

					if(!taskDetails.get(i).getWingId().equals("ALL"))
					{
						query = new Query();
						ProjectWing projectWing = new ProjectWing();
						projectWing = mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(taskDetails.get(i).getWingId())), ProjectWing.class);
						taskDetails.get(i).setWingName(projectWing.getWingName());
					}
					else
					{
						taskDetails.get(i).setWingName("ALL");
					}

					if(!taskDetails.get(i).getFloorId().equals("ALL"))
					{
						query = new Query();
						Floor floorDetails = new Floor();
						floorDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("floorId").is(taskDetails.get(i).getFloorId())), Floor.class);
						taskDetails.get(i).setFloorName(floorDetails.getFloortypeName());
					}
					else
					{
						taskDetails.get(i).setFloorName("ALL");
					}

					if(!taskDetails.get(i).getFlatId().equals("ALL"))
					{
						query = new Query();
						Flat flatDetails = new Flat();
						flatDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("flatId").is(taskDetails.get(i).getFlatId())), Flat.class);
						taskDetails.get(i).setFlatNumber(flatDetails.getFlatNumber());
					}
					else
					{
						taskDetails.get(i).setFlatNumber("ALL");
					}

					for(int j=0;j<employeeList.size();j++)
					{
						if(taskDetails.get(i).getTaskassignFrom().equals(employeeList.get(j).getEmployeeId()))
						{
							taskDetails.get(i).setAssignFrom(employeeList.get(j).getEmployeefirstName()+" "+employeeList.get(j).getEmployeemiddleName()+" "+employeeList.get(j).getEmployeelastName());
						}

						if(taskDetails.get(i).getTaskassignTo().equals(employeeList.get(j).getEmployeeId()))
						{
							taskDetails.get(i).setAssignTo(employeeList.get(j).getEmployeefirstName()+" "+employeeList.get(j).getEmployeemiddleName()+" "+employeeList.get(j).getEmployeelastName());
						}
					}

				}
			}
			catch(Exception e)
			{
				System.out.println("Excepion = "+e.toString());
			}

			model.addAttribute("taskDetails", taskDetails);
			model.addAttribute("projectList",projectList);
			model.addAttribute("totalenquiry",TotalEnquiry());
			model.addAttribute("totalbooking",TotalBooking(req,res));
			model.addAttribute("totalsitevisited",TotalSiteVisited());
			model.addAttribute("totalCancel",TotalCancel());
			return "home";

		}catch (Exception e) {
			return "login";
		}
	}


	@ResponseBody
	@RequestMapping("/HomeMailSending")
	public void HomeMailSending(@RequestParam("emailto") String emailto , @RequestParam("mailSubject") String mailSubject , @RequestParam("mailBody") String mailBody)
	{

		try
		{

			String Msgbody = "/"+mailBody+"/";

			sendLoginCredentialsMail(emailto, mailSubject, Msgbody);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println("Mail Sending Exception = "+e.toString());
		}

	}


	//Request mapping for getting building names by project
	@ResponseBody
	@RequestMapping(value="/getGraghDetails",method=RequestMethod.POST)
	public List<HomeGraphDetails> getGraghDetails(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{

		Calendar calendar = Calendar.getInstance();
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

		int bookingSaleInSunday=0;
		int bookingSaleInMonday=0;
		int bookingSaleInTuesday=0;
		int bookingSaleInWednesday=0;
		int bookingSaleInThursday=0;
		int bookingSaleInFriday=0;
		int bookingSaleInSaturday=0;

		Date date;

		List<Project> projectList = GetUserAssigenedProjectList(req,res); 

		List<HomeGraphDetails> homegraphDetailsList=new ArrayList<HomeGraphDetails>();

		HomeGraphDetails homegraphDetails=new HomeGraphDetails();

		for(int i=0;i<projectList.size();i++)
		{

			bookingSaleInSunday=0;
			bookingSaleInMonday=0;
			bookingSaleInTuesday=0;
			bookingSaleInWednesday=0;
			bookingSaleInThursday=0;
			bookingSaleInFriday=0;
			bookingSaleInSaturday=0;

			if(dayOfWeek==1)
			{

				bookingSaleInSunday=GetBookingCount(new Date(),projectList.get(i).getProjectId());

			}
			else if(dayOfWeek==2)
			{

				calendar = Calendar.getInstance();
				calendar.setTime(new Date());
				calendar.add(Calendar.DAY_OF_YEAR, -1);
				date= calendar.getTime();

				bookingSaleInSunday=GetBookingCount(date,projectList.get(i).getProjectId());
				bookingSaleInMonday=GetBookingCount(new Date(),projectList.get(i).getProjectId());

			}
			else if(dayOfWeek==3)
			{

				calendar = Calendar.getInstance();
				calendar.setTime(new Date());
				calendar.add(Calendar.DAY_OF_YEAR, -2);
				date= calendar.getTime();
				bookingSaleInSunday=GetBookingCount(date,projectList.get(i).getProjectId());


				calendar = Calendar.getInstance();
				calendar.setTime(new Date());
				calendar.add(Calendar.DAY_OF_YEAR, -1);
				date= calendar.getTime();
				bookingSaleInMonday=GetBookingCount(date,projectList.get(i).getProjectId());

				bookingSaleInTuesday=GetBookingCount(new Date(),projectList.get(i).getProjectId());

			}
			else if(dayOfWeek==4)
			{

				calendar = Calendar.getInstance();
				calendar.setTime(new Date());
				calendar.add(Calendar.DAY_OF_YEAR, -3);
				date= calendar.getTime();
				bookingSaleInSunday=GetBookingCount(date,projectList.get(i).getProjectId());

				calendar = Calendar.getInstance();
				calendar.setTime(new Date());
				calendar.add(Calendar.DAY_OF_YEAR, -2);
				date= calendar.getTime();
				bookingSaleInMonday=GetBookingCount(date,projectList.get(i).getProjectId());

				calendar = Calendar.getInstance();
				calendar.setTime(new Date());
				calendar.add(Calendar.DAY_OF_YEAR, -1);
				date= calendar.getTime();
				bookingSaleInTuesday=GetBookingCount(date,projectList.get(i).getProjectId());

				bookingSaleInWednesday=GetBookingCount(new Date(),projectList.get(i).getProjectId());

			}
			else if(dayOfWeek==5)
			{
				calendar = Calendar.getInstance();
				calendar.setTime(new Date());
				calendar.add(Calendar.DAY_OF_YEAR, -4);
				date= calendar.getTime();
				bookingSaleInSunday=GetBookingCount(date,projectList.get(i).getProjectId());

				calendar = Calendar.getInstance();
				calendar.setTime(new Date());
				calendar.add(Calendar.DAY_OF_YEAR, -3);
				date= calendar.getTime();
				bookingSaleInMonday=GetBookingCount(date,projectList.get(i).getProjectId());

				calendar = Calendar.getInstance();
				calendar.setTime(new Date());
				calendar.add(Calendar.DAY_OF_YEAR, -2);
				date= calendar.getTime();
				bookingSaleInTuesday=GetBookingCount(date,projectList.get(i).getProjectId());

				calendar = Calendar.getInstance();
				calendar.setTime(new Date());
				calendar.add(Calendar.DAY_OF_YEAR, -1);
				date= calendar.getTime();
				bookingSaleInWednesday=GetBookingCount(date,projectList.get(i).getProjectId());

				bookingSaleInThursday=GetBookingCount(new Date(),projectList.get(i).getProjectId());

			}
			else if(dayOfWeek==6)
			{
				calendar = Calendar.getInstance();
				calendar.setTime(new Date());
				calendar.add(Calendar.DAY_OF_YEAR, -5);
				date= calendar.getTime();
				bookingSaleInSunday=GetBookingCount(date,projectList.get(i).getProjectId());

				calendar = Calendar.getInstance();
				calendar.setTime(new Date());
				calendar.add(Calendar.DAY_OF_YEAR, -4);
				date= calendar.getTime();
				bookingSaleInMonday=GetBookingCount(date,projectList.get(i).getProjectId());

				calendar = Calendar.getInstance();
				calendar.setTime(new Date());
				calendar.add(Calendar.DAY_OF_YEAR, -3);
				date= calendar.getTime();
				bookingSaleInTuesday=GetBookingCount(date,projectList.get(i).getProjectId());

				calendar = Calendar.getInstance();
				calendar.setTime(new Date());
				calendar.add(Calendar.DAY_OF_YEAR, -2);
				date= calendar.getTime();
				bookingSaleInWednesday=GetBookingCount(date,projectList.get(i).getProjectId());

				calendar = Calendar.getInstance();
				calendar.setTime(new Date());
				calendar.add(Calendar.DAY_OF_YEAR, -1);
				date= calendar.getTime();
				bookingSaleInThursday=GetBookingCount(date,projectList.get(i).getProjectId());

				bookingSaleInFriday=GetBookingCount(new Date(),projectList.get(i).getProjectId());

			}
			else if(dayOfWeek==7)
			{

				calendar = Calendar.getInstance();
				calendar.setTime(new Date());
				calendar.add(Calendar.DAY_OF_YEAR, -6);
				date= calendar.getTime();
				bookingSaleInSunday=GetBookingCount(date,projectList.get(i).getProjectId());

				calendar = Calendar.getInstance();
				calendar.setTime(new Date());
				calendar.add(Calendar.DAY_OF_YEAR, -5);
				date= calendar.getTime();
				bookingSaleInMonday=GetBookingCount(date,projectList.get(i).getProjectId());

				calendar = Calendar.getInstance();
				calendar.setTime(new Date());
				calendar.add(Calendar.DAY_OF_YEAR, -4);
				date= calendar.getTime();
				bookingSaleInTuesday=GetBookingCount(date,projectList.get(i).getProjectId());

				calendar = Calendar.getInstance();
				calendar.setTime(new Date());
				calendar.add(Calendar.DAY_OF_YEAR, -3);
				date= calendar.getTime();
				bookingSaleInWednesday=GetBookingCount(date,projectList.get(i).getProjectId());

				calendar = Calendar.getInstance();
				calendar.setTime(new Date());
				calendar.add(Calendar.DAY_OF_YEAR, -2);
				date= calendar.getTime();
				bookingSaleInThursday=GetBookingCount(date,projectList.get(i).getProjectId());

				calendar = Calendar.getInstance();
				calendar.setTime(new Date());
				calendar.add(Calendar.DAY_OF_YEAR, -1);
				date= calendar.getTime();
				bookingSaleInFriday=GetBookingCount(date,projectList.get(i).getProjectId());

				bookingSaleInSaturday=GetBookingCount(new Date(),projectList.get(i).getProjectId());
			}


			homegraphDetails=new HomeGraphDetails();

			homegraphDetails.setProjectName(projectList.get(i).getProjectName());
			homegraphDetails.setBookingSaleInSunday(bookingSaleInSunday);
			homegraphDetails.setBookingSaleInMonday(bookingSaleInMonday);
			homegraphDetails.setBookingSaleInTuesday(bookingSaleInTuesday);
			homegraphDetails.setBookingSaleInWednesday(bookingSaleInWednesday);
			homegraphDetails.setBookingSaleInThursday(bookingSaleInThursday);
			homegraphDetails.setBookingSaleInFriday(bookingSaleInFriday);
			homegraphDetails.setBookingSaleInSaturday(bookingSaleInSaturday);

			homegraphDetailsList.add(homegraphDetails);

		}

		return homegraphDetailsList;
	}   


	public int GetBookingCount(Date currentDate, String projectId)
	{
		int totalbooking=0;

		Query query = new Query();
		final Calendar calendar = Calendar.getInstance();
		calendar.setTime(currentDate);
		calendar.add(Calendar.DAY_OF_YEAR, -1);
		Date date= calendar.getTime(); 

		totalbooking = (int) mongoTemplate.count(query.addCriteria(Criteria.where("projectId").is(projectId).and("creationDate").gte(date).lt(currentDate)), Booking.class);

		return totalbooking;
	}

	//Request mapping for getting building names by project
	@ResponseBody
	@RequestMapping(value="/getProjectGraghDetails",method=RequestMethod.POST)
	public List<ProjectGraghDetails> getProjectGraghDetails(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{

		int totalFlat=0;
		int totalFlatBooking=0;
		int totalFlatAgreement=0;
		int totalFlatRemaining=0;

		List<Project> projectList = GetUserAssigenedProjectList(req,res); 

		Query query = new Query();

		for(int i=0;i<projectList.size();i++)
		{

			query = new Query();
			totalFlat = totalFlat+ (int) mongoTemplate.count(query.addCriteria(Criteria.where("projectId").is(projectList.get(i).getProjectId())), Flat.class);

			query = new Query();
			totalFlatBooking = totalFlatBooking+ (int) mongoTemplate.count(query.addCriteria(Criteria.where("projectId").is(projectList.get(i).getProjectId()).and("flatstatus").is("Booking Completed")), Flat.class);

			query = new Query();
			totalFlatAgreement = totalFlatAgreement+ (int) mongoTemplate.count(query.addCriteria(Criteria.where("projectId").is(projectList.get(i).getProjectId()).and("flatstatus").is("Aggreement Completed")), Flat.class);

		}

		List<ProjectGraghDetails> projectgraphDetailsList=new ArrayList<ProjectGraghDetails>();

		ProjectGraghDetails projectgraphDetails=new ProjectGraghDetails();

		totalFlatRemaining=totalFlat-(totalFlatBooking+totalFlatAgreement);
		projectgraphDetails.setTotalFlat(totalFlatRemaining);
		projectgraphDetails.setTotalFlatBooking(totalFlatBooking);
		projectgraphDetails.setTotalFlatAgreement(totalFlatAgreement);
		projectgraphDetails.setTotalFlatRemaining(totalFlatRemaining);

		projectgraphDetailsList.add(projectgraphDetails);

		return projectgraphDetailsList;
	}   


}

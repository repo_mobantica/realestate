package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Login;
import com.realestate.bean.PaymentScheduler;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.GeneratePaymentSchedulerRepository;
import com.realestate.repository.PaymentSchedulerRepository;
import com.realestate.repository.ProjectRepository;


@Controller
@RequestMapping("/")
public class PaymentSchedulerForProjectEnggMasterController {


	@Autowired
	PaymentSchedulerRepository paymentSchedulerRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	GeneratePaymentSchedulerRepository generatePaymentSchedulerRepository;


	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping("/PaymentSchedulerForProjectEnggMaster")
	public String PaymentSchedulerMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		List<PaymentScheduler> paymentschedulerList =  paymentSchedulerRepository.findAll();
		List<Project> projectList = GetUserAssigenedProjectList(req,res);
		Query query = new Query();
		for(int i=0;i<paymentschedulerList.size();i++)
		{
			try
			{
				query = new Query();
				List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(paymentschedulerList.get(i).getProjectId())), Project.class);
				paymentschedulerList.get(i).setProjectId(projectDetails.get(0).getProjectName());
				query = new Query();
				List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(paymentschedulerList.get(i).getBuildingId())), ProjectBuilding.class);
				paymentschedulerList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
				query = new Query();
				List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(paymentschedulerList.get(i).getWingId())), ProjectWing.class);
				paymentschedulerList.get(i).setWingId(wingDetails.get(0).getWingName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}
		model.addAttribute("paymentschedulerList", paymentschedulerList);
		model.addAttribute("projectList",projectList);
		return "PaymentSchedulerForProjectEnggMaster";
	}



	@RequestMapping("/ViewPaymentScheduler")
	public String ViewPaymentScheduler(@RequestParam("paymentschedulerId") String paymentschedulerId, ModelMap model)
	{	
		Query query = new Query();
		List<PaymentScheduler> paymentschedulerDetails = mongoTemplate.find(query.addCriteria(Criteria.where("paymentschedulerId").is(paymentschedulerId)), PaymentScheduler.class);

		query = new Query();
		List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(paymentschedulerDetails.get(0).getProjectId())), Project.class);

		query = new Query();
		List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(paymentschedulerDetails.get(0).getBuildingId())), ProjectBuilding.class);

		query = new Query();
		List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(paymentschedulerDetails.get(0).getWingId())), ProjectWing.class);

		model.addAttribute("projectName",projectDetails.get(0).getProjectName());
		model.addAttribute("buildingName",buildingDetails.get(0).getBuildingName());
		model.addAttribute("wingName",wingDetails.get(0).getWingName());
		model.addAttribute("paymentschedulerDetails",paymentschedulerDetails);

		return "ViewPaymentScheduler";
	}	


	@RequestMapping(value="/ViewPaymentScheduler", method=RequestMethod.POST)
	public String ViewPaymentScheduler(@ModelAttribute PaymentScheduler paymentScheduler,ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try
		{
			paymentScheduler = new PaymentScheduler(paymentScheduler.getPaymentschedulerId(),paymentScheduler.getProjectId(),paymentScheduler.getBuildingId(),paymentScheduler.getWingId(), paymentScheduler.getInstallmentNumber(), paymentScheduler.getPaymentDecription(),paymentScheduler.getPercentage(),paymentScheduler.getDueDate(),paymentScheduler.getPaidDate(),paymentScheduler.getSlabStatus(),
					paymentScheduler.getArchitectureSign(),paymentScheduler.getEngineerSign(),paymentScheduler.getMeSign(),
					paymentScheduler.getArchitectureSignDate(),paymentScheduler.getEngineerSignDate(),paymentScheduler.getMeSignDate(),
					paymentScheduler.getCreationDate(),paymentScheduler.getUpdateDate(),paymentScheduler.getUserName());
			paymentSchedulerRepository.save(paymentScheduler);
			model.addAttribute("schedulerStatus","Success");
		}
		catch(Exception e)
		{
			model.addAttribute("schedulerStatus","Fail");
		}

		List<PaymentScheduler> paymentschedulerList =  paymentSchedulerRepository.findAll();
		List<Project> projectList = GetUserAssigenedProjectList(req,res);
		Query query = new Query();
		for(int i=0;i<paymentschedulerList.size();i++)
		{
			try
			{
				query = new Query();
				List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(paymentschedulerList.get(i).getProjectId())), Project.class);
				paymentschedulerList.get(i).setProjectId(projectDetails.get(0).getProjectName());
				query = new Query();
				List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(paymentschedulerList.get(i).getBuildingId())), ProjectBuilding.class);
				paymentschedulerList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
				query = new Query();
				List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(paymentschedulerList.get(i).getWingId())), ProjectWing.class);
				paymentschedulerList.get(i).setWingId(wingDetails.get(0).getWingName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		model.addAttribute("paymentschedulerList", paymentschedulerList);
		model.addAttribute("projectList",projectList);
		return "PaymentSchedulerForProjectEnggMaster";
	}

	@ResponseBody
	@RequestMapping("/getProjectWisePaymentSchedule")
	public List<PaymentScheduler> getProjectWisePaymentSchedule(@RequestParam("projectId") String projectId)
	{
		Query query = new Query();
		List<PaymentScheduler> paymentschedulerList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)), PaymentScheduler.class);

		for(int i=0;i<paymentschedulerList.size();i++)
		{
			try
			{
				query = new Query();
				List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(paymentschedulerList.get(i).getProjectId())), Project.class);
				paymentschedulerList.get(i).setProjectId(projectDetails.get(0).getProjectName());
				query = new Query();
				List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(paymentschedulerList.get(i).getBuildingId())), ProjectBuilding.class);
				paymentschedulerList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
				query = new Query();
				List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(paymentschedulerList.get(i).getWingId())), ProjectWing.class);
				paymentschedulerList.get(i).setWingId(wingDetails.get(0).getWingName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		return paymentschedulerList;
	}

	@ResponseBody
	@RequestMapping("/getBuildingWisePaymentSchedule")
	public List<PaymentScheduler> getProjectWisePaymentSchedule(@RequestParam("projectId") String projectId, @RequestParam("buildingId") String buildingId)
	{
		Query query = new Query();
		List<PaymentScheduler> paymentschedulerList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId)), PaymentScheduler.class);

		for(int i=0;i<paymentschedulerList.size();i++)
		{
			try
			{
				query = new Query();
				List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(paymentschedulerList.get(i).getProjectId())), Project.class);
				paymentschedulerList.get(i).setProjectId(projectDetails.get(0).getProjectName());
				query = new Query();
				List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(paymentschedulerList.get(i).getBuildingId())), ProjectBuilding.class);
				paymentschedulerList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
				query = new Query();
				List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(paymentschedulerList.get(i).getWingId())), ProjectWing.class);
				paymentschedulerList.get(i).setWingId(wingDetails.get(0).getWingName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		return paymentschedulerList;
	}

}

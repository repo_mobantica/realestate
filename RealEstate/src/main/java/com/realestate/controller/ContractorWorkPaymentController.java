package com.realestate.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.realestate.bean.ContractorPaidPayment;
import com.realestate.bean.ContractorPaidPaymentHistory;
import com.realestate.bean.ContractorPaymentSchedule;
import com.realestate.bean.ContractorType;
import com.realestate.bean.ContractorWorkListHistory;
import com.realestate.bean.ContractorWorkOrder;
import com.realestate.bean.Login;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.SubContractorType;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.ContractorPaymentScheduleRepository;
import com.realestate.repository.ContractorRepository;
import com.realestate.repository.ContractorTypeRepository;
import com.realestate.repository.ContractorWorkOrderRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.ContractorPaidPaymentHistoryRepository;
import com.realestate.repository.ContractorPaidPaymentRepository;
import com.realestate.repository.CompanyAccountPaymentDetailsRepository;

@Controller
@RequestMapping("/")
public class ContractorWorkPaymentController {

	@Autowired
	ContractorRepository contractorRepository;
	@Autowired
	ContractorPaymentScheduleRepository contractorpaymentscheduleRepository;
	@Autowired
	ContractorTypeRepository contractortypeRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	ContractorWorkOrderRepository contractorworkorderRepository;
	@Autowired
	ContractorPaidPaymentHistoryRepository contractorpaidpaymenthistoryRepository;
	@Autowired
	ContractorPaidPaymentRepository contractorpaidpaymentRepository;
	@Autowired
	CompanyAccountPaymentDetailsRepository companyaccountpaymentdetailsRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	String paymentCode;

	private String PaymentCode() {
		long paymentCount = contractorpaidpaymentRepository.count();

		if (paymentCount < 10) {
			paymentCode = "CPP000" + (paymentCount + 1);
		} else if ((paymentCount >= 10) && (paymentCount < 100)) {
			paymentCode = "CPP00" + (paymentCount + 1);
		} else if ((paymentCount >= 100) && (paymentCount < 1000)) {
			paymentCode = "CPP0" + (paymentCount + 1);
		} else{
			paymentCode = "CPP" + (paymentCount + 1);
		}

		return paymentCode;
	}

	String paymenthistoryCode;

	private String PaymentHistoryCode() {
		long historyCount = contractorpaidpaymenthistoryRepository.count();

		if (historyCount < 10) {
			paymenthistoryCode = "CPPH000" + (historyCount + 1);
		} else if ((historyCount >= 10) && (historyCount < 100)) {
			paymenthistoryCode = "CPPH00" + (historyCount + 1);
		} else if ((historyCount >= 100) && (historyCount < 1000)) {
			paymenthistoryCode = "CPPH0" + (historyCount + 1);
		} else{
			paymenthistoryCode = "CPPH" + (historyCount + 1);
		}

		return paymenthistoryCode;
	}

	String paymentId = "";

	public String GenerateAccountPaymentId() {

		long cnt = companyaccountpaymentdetailsRepository.count();

		if (cnt < 10) {
			paymentId = "P000" + (cnt + 1);
		} else if ((cnt < 100) && (cnt >= 10)) {
			paymentId = "P00" + (cnt + 1);
		} else if ((cnt < 1000) && (cnt >= 100)) {
			paymentId = "P0" + (cnt + 1);
		} else {
			paymentId = "P" + (cnt + 1);
		}

		return paymentId;
	}

	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req, HttpServletResponse res) {
		try {
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String) req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);

			query = new Query();
			projectList1 = mongoTemplate.find(
					query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())),
					UserAssignedProject.class);

			Project project = new Project();
			for (int i = 0; i < projectList1.size(); i++) {
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(
						query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())),
						Project.class);

				if (project != null) {
					projectList.add(project);
				}
			}
			return projectList;
		} catch (Exception e) {
			return null;
		}

	}

	@RequestMapping("/AddContractorWorkPayment")
	public String AddContractorWorkPayment(@RequestParam("workOrderId") String workOrderId, ModelMap model) {
		try {
			Query query = new Query();
			List<ContractorWorkOrder> contractorworkorderDetails = mongoTemplate
					.find(query.addCriteria(Criteria.where("workOrderId").is(workOrderId)), ContractorWorkOrder.class);

			try {

				query = new Query();
				List<Project> projectDetails = mongoTemplate.find(
						query.addCriteria(Criteria.where("projectId").is(contractorworkorderDetails.get(0).getProjectId())),
						Project.class);
				query = new Query();
				List<ProjectBuilding> buildingDetails = mongoTemplate.find(
						query.addCriteria(
								Criteria.where("buildingId").is(contractorworkorderDetails.get(0).getBuildingId())),
						ProjectBuilding.class);
				query = new Query();
				List<ProjectWing> wingDetails = mongoTemplate.find(
						query.addCriteria(Criteria.where("wingId").is(contractorworkorderDetails.get(0).getWingId())),
						ProjectWing.class);

				query = new Query();
				List<ContractorType> contractorDetails1 = mongoTemplate.find(query.addCriteria(
						Criteria.where("contractortypeId").is(contractorworkorderDetails.get(0).getContractortypeId())),
						ContractorType.class);

				query = new Query();
				List<ContractorWorkListHistory> contractorworkhistoryList = mongoTemplate.find(
						query.addCriteria(Criteria.where("workId").is(contractorworkorderDetails.get(0).getWorkId())),
						ContractorWorkListHistory.class);

				for (int i = 0; i < contractorworkhistoryList.size(); i++) {
					query = new Query();
					List<SubContractorType> subcontractortypeDetails = mongoTemplate.find(
							query.addCriteria(Criteria.where("subcontractortypeId")
									.is(contractorworkhistoryList.get(i).getSubcontractortypeId())),
							SubContractorType.class);
					contractorworkhistoryList.get(i)
					.setSubcontractortypeId(subcontractortypeDetails.get(0).getSubcontractorType());
				}

				query = new Query();

				long totalPaidAmount = 0;
				query = new Query();

				List<ContractorPaidPayment> ContractorPaidpaymentList = mongoTemplate.find(
						query.addCriteria(
								Criteria.where("workOrderId").is(contractorworkorderDetails.get(0).getWorkOrderId())
								.and("contractorId").is(contractorworkorderDetails.get(0).getContractorId())),
						ContractorPaidPayment.class);
				if (ContractorPaidpaymentList.size() != 0) {
					totalPaidAmount = ContractorPaidpaymentList.get(0).getPaidAmount();
				}

				query = new Query();
				List<ContractorPaymentSchedule> contractorpaymentscheduleList = mongoTemplate.find(
						query.addCriteria(Criteria.where("workOrderId").is(workOrderId)), ContractorPaymentSchedule.class);
				List<ContractorPaymentSchedule> contractorpaymentscheduleList1 = new ArrayList<ContractorPaymentSchedule>();

				ContractorPaymentSchedule contractorpaymentschedule = new ContractorPaymentSchedule();
				try {
					long paidPaymentAmount = 0;
					for (int i = 0; i < contractorpaymentscheduleList.size(); i++) {
						contractorpaymentschedule = new ContractorPaymentSchedule();
						contractorpaymentschedule.setContractorrschedulerId(
								contractorpaymentscheduleList.get(i).getContractorrschedulerId());
						contractorpaymentschedule.setWorkOrderId(contractorpaymentscheduleList.get(i).getWorkOrderId());
						contractorpaymentschedule.setContractorId(contractorpaymentscheduleList.get(i).getContractorId());
						contractorpaymentschedule
						.setPaymentDecription(contractorpaymentscheduleList.get(i).getPaymentDecription());
						contractorpaymentschedule.setPaymentAmount(contractorpaymentscheduleList.get(i).getPaymentAmount());
						contractorpaymentschedule.setGstAmount(contractorpaymentscheduleList.get(i).getGstAmount());
						contractorpaymentschedule
						.setPaymentTotalAmount(contractorpaymentscheduleList.get(i).getPaymentTotalAmount());
						contractorpaymentschedule.setTdsAmount(contractorpaymentscheduleList.get(i).getTdsAmount());
						contractorpaymentschedule.setGrandAmount(contractorpaymentscheduleList.get(i).getGrandAmount());
						contractorpaymentschedule.setStatus(contractorpaymentscheduleList.get(i).getStatus());
						paidPaymentAmount = 0;
						query = new Query();
						List<ContractorPaidPaymentHistory> contractorpaymentList = mongoTemplate.find(
								query.addCriteria(Criteria.where("workOrderId").is(workOrderId).and("contractorId")
										.is(contractorpaymentscheduleList.get(i).getContractorId()).and("paymentDecription")
										.is(contractorpaymentscheduleList.get(i).getPaymentDecription())),
								ContractorPaidPaymentHistory.class);
						if (contractorpaymentList.size() != 0) {
							for (int j = 0; j < contractorpaymentList.size(); j++) {
								paidPaymentAmount = paidPaymentAmount + contractorpaymentList.get(j).getPaymentAmount();
							}
						}
						contractorpaymentschedule.setPaidPaymentAmount(paidPaymentAmount);
						contractorpaymentscheduleList1.add(contractorpaymentschedule);
					}
				} catch (Exception e) {
				}

				model.addAttribute("contractorworkhistoryList", contractorworkhistoryList);
				model.addAttribute("projectName", projectDetails.get(0).getProjectName());
				model.addAttribute("buildingName", buildingDetails.get(0).getBuildingName());
				model.addAttribute("wingName", wingDetails.get(0).getWingName());

				model.addAttribute("contractorType", contractorDetails1.get(0).getContractorType());

				model.addAttribute("totalPaidAmount", totalPaidAmount);
				model.addAttribute("contractorpaymentscheduleList1", contractorpaymentscheduleList1);
				model.addAttribute("contractorworkorderDetails", contractorworkorderDetails);
			} catch (Exception e) {
				System.out.println("error==  " + e);
				// TODO: handle exception
			}
			return "AddContractorWorkPayment";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value = "/AddContractorWorkPayment", method = RequestMethod.POST)
	public String AddContractorWorkPayment(@ModelAttribute ContractorPaidPayment contractorpaidpayment1,
			@ModelAttribute ContractorPaidPaymentHistory contractorpaidpaymenthistory1, Model model,
			HttpServletRequest req, HttpServletResponse res) throws ParseException {
		try {
			Date todateDate = new Date();
			Query query = new Query();
			try {
				ContractorPaidPaymentHistory contractorpaidpaymenthistory = new ContractorPaidPaymentHistory();
				contractorpaidpaymenthistory.setContractorPaidPaymentHistoriId(PaymentHistoryCode());
				contractorpaidpaymenthistory.setWorkOrderId(contractorpaidpaymenthistory1.getWorkOrderId());
				contractorpaidpaymenthistory.setContractorId(contractorpaidpaymenthistory1.getContractorId());
				contractorpaidpaymenthistory.setBillNumber(contractorpaidpaymenthistory1.getBillNumber());
				contractorpaidpaymenthistory.setBillDate(contractorpaidpaymenthistory1.getBillDate());
				contractorpaidpaymenthistory.setPaymentAmount(contractorpaidpaymenthistory1.getPaymentAmount());
				contractorpaidpaymenthistory.setPaymentDecription(contractorpaidpaymenthistory1.getPaymentDecription());
				contractorpaidpaymenthistory.setNarration(contractorpaidpaymenthistory1.getNarration());
				contractorpaidpaymenthistory.setPaymentStatus("Unclear");
				contractorpaidpaymenthistory.setCreationDate(todateDate);
				contractorpaidpaymenthistoryRepository.save(contractorpaidpaymenthistory);

			} catch (DuplicateKeyException de) {
			}

			try {
				query = new Query();
				List<ContractorPaidPayment> contractorpaymentscheduleList = mongoTemplate.find(
						query.addCriteria(Criteria.where("workOrderId").is(contractorpaidpaymenthistory1.getWorkOrderId())
								.and("contractorId").is(contractorpaidpaymenthistory1.getContractorId())),
						ContractorPaidPayment.class);
				ContractorPaidPayment contractorpaidpayment = new ContractorPaidPayment();
				if (contractorpaymentscheduleList.size() == 0) {
					contractorpaidpayment.setContractorPaidPaymentId(PaymentCode());
					contractorpaidpayment.setWorkOrderId(contractorpaidpaymenthistory1.getWorkOrderId());
					contractorpaidpayment.setContractorId(contractorpaidpaymenthistory1.getContractorId());
					contractorpaidpayment.setPaidAmount(contractorpaidpaymenthistory1.getPaymentAmount());
					contractorpaidpaymentRepository.save(contractorpaidpayment);
				} else {
					long amount = 0;
					contractorpaidpayment = mongoTemplate.findOne(
							Query.query(Criteria.where("workOrderId").is(contractorpaidpaymenthistory1.getWorkOrderId())
									.and("contractorId").is(contractorpaidpaymenthistory1.getContractorId())),
							ContractorPaidPayment.class);
					amount = contractorpaidpayment.getPaidAmount() + contractorpaidpaymenthistory1.getPaymentAmount();

					contractorpaidpayment.setPaidAmount(amount);
					contractorpaidpaymentRepository.save(contractorpaidpayment);
				}

			} catch (Exception e) {
			}

			// to back in ContractorPaymentBill;

			List<Project> projectList = GetUserAssigenedProjectList(req, res);

			query = new Query();
			List<ContractorWorkOrder> contractorworkorder = mongoTemplate
					.find(query.addCriteria(Criteria.where("paymentStatus").ne("Clear")), ContractorWorkOrder.class);

			List<ContractorWorkOrder> contractorworkorderDetails = new ArrayList<ContractorWorkOrder>();

			long totalPaidAmount = 0;
			query = new Query();

			ContractorWorkOrder contractorwork = new ContractorWorkOrder();
			Double workAmount = 0.0;
			for (int i = 0; i < contractorworkorder.size(); i++) {
				try {

					contractorwork = new ContractorWorkOrder();
					workAmount = 0.0;
					query = new Query();
					List<ContractorPaymentSchedule> contractorpaymentSchedule = mongoTemplate.find(
							query.addCriteria(Criteria.where("workOrderId").is(contractorworkorder.get(i).getWorkOrderId())
									.and("status").is("Completed")),
							ContractorPaymentSchedule.class);
					for (int j = 0; j < contractorpaymentSchedule.size(); j++) {
						workAmount = workAmount + contractorpaymentSchedule.get(j).getGrandAmount();
					}

					totalPaidAmount = 0;
					query = new Query();
					List<ContractorPaidPayment> ContractorPaidpaymentList = mongoTemplate.find(
							query.addCriteria(Criteria.where("workOrderId").is(contractorworkorder.get(i).getWorkOrderId())
									.and("contractorId").is(contractorworkorder.get(i).getContractorId())),
							ContractorPaidPayment.class);

					if (ContractorPaidpaymentList.size() != 0) {
						totalPaidAmount = ContractorPaidpaymentList.get(0).getPaidAmount();
					}
					contractorwork.setWorkOrderId(contractorworkorder.get(i).getWorkOrderId());
					contractorwork.setContractorId(contractorworkorder.get(i).getContractorId());
					contractorwork.setContractorfirmName(contractorworkorder.get(i).getContractorfirmName());
					try {
						query = new Query();
						List<Project> projectDetails = mongoTemplate.find(
								query.addCriteria(
										Criteria.where("projectId").is(contractorworkorder.get(i).getProjectId())),
								Project.class);
						query = new Query();
						List<ProjectBuilding> buildingDetails = mongoTemplate.find(
								query.addCriteria(
										Criteria.where("buildingId").is(contractorworkorder.get(i).getBuildingId())),
								ProjectBuilding.class);
						query = new Query();
						List<ProjectWing> wingDetails = mongoTemplate.find(
								query.addCriteria(Criteria.where("wingId").is(contractorworkorder.get(i).getWingId())),
								ProjectWing.class);

						contractorwork.setProjectId(projectDetails.get(0).getProjectName());
						contractorwork.setBuildingId(buildingDetails.get(0).getBuildingName());
						contractorwork.setWingId(wingDetails.get(0).getWingName());
					} catch (Exception e) {
						// TODO: handle exception
					}
					contractorwork.setTotalAmount(contractorworkorder.get(i).getTotalAmount());
					contractorwork.setWorkAmount(workAmount);

					contractorwork.setTotalPaidAmount(totalPaidAmount);
					contractorworkorderDetails.add(contractorwork);

				} catch (Exception e) {
					// TODO: handle exception
				}
			}

			model.addAttribute("projectList", projectList);
			model.addAttribute("contractorworkorderDetails", contractorworkorderDetails);
			return "ContractorPaymentBill";

		}catch (Exception e) {
			return "login";
		}
	}

}

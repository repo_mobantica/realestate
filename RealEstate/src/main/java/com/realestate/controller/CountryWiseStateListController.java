package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Country;
import com.realestate.bean.State;
import com.realestate.repository.CountryRepository;
import com.realestate.repository.StateRepository;


@Controller
@RequestMapping("/")
public class CountryWiseStateListController 
{
	@Autowired
	StateRepository stateRepository;
	@Autowired
	CountryRepository countryRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	@ResponseBody
	@RequestMapping(value="/getStateList",method=RequestMethod.POST)
	public List<State> StateList(@RequestParam("countryId") String countryId, HttpSession session)
	{
		List<State> stateList= new ArrayList<State>();

		Query query = new Query();
		stateList = mongoTemplate.find(query.addCriteria(Criteria.where("countryId").is(countryId)), State.class);

		List<Country> countryList= countryRepository.findAll();
		for(int i=0;i<stateList.size();i++)
		{
			try {
				for(int j=0;j<countryList.size();j++)
				{
					if(stateList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
					{
						stateList.get(i).setCountryId(countryList.get(j).getCountryName());
						break;
					}
				}
			}catch (Exception e) {
				// TODO: handle exception
			}
		}

		return stateList;
	}
}

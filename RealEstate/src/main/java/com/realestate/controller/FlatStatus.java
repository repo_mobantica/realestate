package com.realestate.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Agent;
import com.realestate.bean.AgentEmployees;
import com.realestate.bean.Booking;
import com.realestate.bean.City;
import com.realestate.bean.Country;
import com.realestate.bean.Designation;
import com.realestate.bean.Flat;
import com.realestate.bean.Floor;
import com.realestate.bean.LocationArea;
import com.realestate.bean.Login;
import com.realestate.bean.PaymentScheduler;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.State;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.FlatRepository;
import com.realestate.repository.FloorRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.ProjectWingRepository;

@Controller
@RequestMapping("/")
public class FlatStatus {

	@Autowired
	FlatRepository flatRepository;
	@Autowired
	FloorRepository floorRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	ProjectWingRepository projectwingrepository;
	@Autowired
	MongoTemplate mongoTemplate;


	private String getLastProjectName(HttpServletRequest req, HttpServletResponse res)
	{
		String projectId="";
		List<Project> projectList;
		projectList = GetUserAssigenedProjectList(req,res);
		for(int i=0;i<projectList.size();i++)
		{
			projectId=projectList.get(i).getProjectId();
		}
		return projectId;
	}

	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);

			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{

				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}

			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping("/FlatStatus")
	public String Flatstatus(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			try {
				Query query = new Query();
				String projectId=getLastProjectName(req, res);

				query = new Query();
				Project projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectId)), Project.class);

				query = new Query();
				List<ProjectWing> wingList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)), ProjectWing.class);

				query = new Query();
				List<Floor> floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(wingList.get(0).getWingId()).and("floorzoneType").is("Flats")), Floor.class);

				query = new Query();
				List<Flat> flatList  = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(wingList.get(0).getWingId())), Flat.class);

				query = new Query();
				List<Booking> bookingDetails  = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(wingList.get(0).getWingId())), Booking.class);

				int j=0;
				String bookingName="";
				for(int i=0;i<flatList.size();i++)
				{

					if(flatList.get(i).getFlatstatus().equals("Booking Completed"))
					{

						for(j=0;j<bookingDetails.size();j++)
						{
							if(flatList.get(i).getFlatId().equalsIgnoreCase(bookingDetails.get(j).getFlatId()))
							{
								bookingName="Booking by "+bookingDetails.get(j).getBookingfirstname()+" "+bookingDetails.get(j).getBookinglastname();
								flatList.get(i).setBookingName(bookingName);
								break;
							}
						}
					}
					else if(flatList.get(i).getFlatstatus().equals("Aggreement Completed"))
					{
						for(j=0;j<bookingDetails.size();j++)
						{
							if(flatList.get(i).getFlatId().equalsIgnoreCase(bookingDetails.get(j).getFlatId()))
							{
								bookingName="Agreement by "+bookingDetails.get(j).getBookingfirstname()+" "+bookingDetails.get(j).getBookinglastname();
								flatList.get(i).setBookingName(bookingName);
								break;
							}
						}
					}
					else
					{
						bookingName="Remaninig";
						flatList.get(i).setBookingName(bookingName);
					}
				}

				query = new Query();
				List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(flatList.get(0).getBuildingId())), ProjectBuilding.class);

				model.addAttribute("flatList",flatList);
				model.addAttribute("floorDetails",floorDetails);

				model.addAttribute("wingName",wingList.get(0).getWingName());
				model.addAttribute("buildingName",buildingDetails.get(0).getBuildingName());
				model.addAttribute("projectName",projectDetails.getProjectName());
			}catch (Exception e) {
				System.out.println("Exception = = "+e);
				// TODO: handle exception
			}
			List<Project> projectList = GetUserAssigenedProjectList(req,res); 
			model.addAttribute("projectList",projectList);
			return "FlatStatus";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping(value="/FlatStatus",method=RequestMethod.POST)
	public String FlatStatus(@RequestParam("projectId") String projectId, @RequestParam("buildingId") String buildingId, @RequestParam("wingId") String wingId, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			Query query = new Query();

			ProjectWing wingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(wingId)), ProjectWing.class);

			query = new Query();
			List<Floor> floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(wingId).and("floorzoneType").is("Flats")), Floor.class);

			query = new Query();
			List<Flat> flatList  = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(wingId)), Flat.class);

			query = new Query();
			ProjectBuilding buildingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(buildingId)), ProjectBuilding.class);

			query = new Query();
			Project projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectId)), Project.class);

			query = new Query();
			List<Booking> bookingDetails  = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(wingId)), Booking.class);


			int j=0;
			String bookingName="";
			for(int i=0;i<flatList.size();i++)
			{
				if(flatList.get(i).getFlatstatus().equals("Booking Completed"))
				{
					for(j=0;j<bookingDetails.size();j++)
					{
						if(flatList.get(i).getFlatId().equalsIgnoreCase(bookingDetails.get(j).getFlatId()))
						{
							bookingName="Booking by "+bookingDetails.get(j).getBookingfirstname();
							flatList.get(i).setBookingName(bookingName);
							break;
						}
					}
				}
				else if(flatList.get(i).getFlatstatus().equals("Aggreement Completed"))
				{
					for(j=0;j<bookingDetails.size();j++)
					{
						if(flatList.get(i).getFlatId().equalsIgnoreCase(bookingDetails.get(j).getFlatId()))
						{
							bookingName="Agreement by "+bookingDetails.get(j).getBookingfirstname();
							flatList.get(i).setBookingName(bookingName);
							break;
						}
					}
				}
				else
				{
					bookingName="Remaninig";
					flatList.get(i).setBookingName(bookingName);
				}
			}

			model.addAttribute("flatList",flatList);
			model.addAttribute("floorDetails",floorDetails);

			model.addAttribute("wingName",wingDetails.getWingName());
			model.addAttribute("buildingName",buildingDetails.getBuildingName());
			model.addAttribute("projectName",projectDetails.getProjectName());
		}catch (Exception e) {
			// TODO: handle exception
		}

		List<Project> projectList = GetUserAssigenedProjectList(req,res);
		model.addAttribute("projectList",projectList);

		return "FlatStatus";
	}



	@ResponseBody
	@RequestMapping("/getWingWiseflatStatus")
	public List<Flat> getWingWiseflatStatus(@RequestParam("projectId") String projectId, @RequestParam("buildingId") String buildingId, @RequestParam("wingId") String wingId )
	{
		Query query =new Query();
		List<Flat> flatList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId)), Flat.class);
		return flatList;			 
	}

	@ResponseBody
	@RequestMapping("/getProjectWiseflatStatus")
	public List<Flat> getProjectWiseflatStatus(@RequestParam("projectId") String projectId )
	{
		Query query =new Query();
		List<Flat> flatList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)), Flat.class);
		return flatList;			 
	}

	//code for Flat Wise Report
	@RequestMapping("/FlatWiseReport")
	public String FlatWiseReport(ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/M/yyyy");
			LocalDate localDate = LocalDate.now();
			String todayDate=dtf.format(localDate);

			List<Flat> flatList = flatRepository.findAll();
			Query query = new Query();
			for(int i=0;i<flatList.size();i++)
			{
				try
				{
					query = new Query();
					List<Project>  projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(flatList.get(i).getProjectId())), Project.class);
					flatList.get(i).setProjectId(projectDetails.get(0).getProjectName());

					query = new Query();
					List<ProjectBuilding>  buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(flatList.get(i).getBuildingId())), ProjectBuilding.class); 
					flatList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
					query = new Query();
					List<ProjectWing>  wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(flatList.get(i).getWingId())), ProjectWing.class); 
					flatList.get(i).setWingId(wingDetails.get(0).getWingName());
					query = new Query();
					List<Floor>  floorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("floorId").is(flatList.get(i).getFloorId())), Floor.class); 
					flatList.get(i).setFloorId(floorDetails.get(0).getFloortypeName());
				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}

			List<Project> projectList = GetUserAssigenedProjectList(req,res); 

			model.addAttribute("projectList",projectList);

			model.addAttribute("flatList",flatList);
			return "FlatWiseReport";

		}catch (Exception e) {
			return "login";
		}
	}

}

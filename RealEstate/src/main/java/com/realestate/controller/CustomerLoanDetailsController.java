package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import org.springframework.data.mongodb.core.query.Query;
import com.realestate.bean.Booking;
import com.realestate.bean.BookingCancelForm;
import com.realestate.bean.Aggreement;
import com.realestate.bean.Bank;
import com.realestate.repository.AggreementRepository;
import com.realestate.repository.BankRepository;
import com.realestate.repository.BookingCancelFormRepository;
import com.realestate.repository.BookingRepository;
import com.realestate.repository.CustomerLoanDetailsRepository;
import com.realestate.repository.ExtraChargeRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.bean.CustomerLoanDetails;
import com.realestate.bean.Enquiry;
import com.realestate.bean.ExtraCharges;
import com.realestate.bean.Login;
import com.realestate.bean.Project;
import com.realestate.bean.UserAssignedProject;
@Controller
@RequestMapping("/")
public class CustomerLoanDetailsController
{
	@Autowired 
	AggreementRepository aggreementRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired 
	BookingRepository bookingRepository;
	@Autowired 
	ExtraChargeRepository extrachargesRepository;
	@Autowired
	BankRepository bankRepository;
	@Autowired
	CustomerLoanDetailsRepository customerLoandetailsRepository;
	@Autowired
	BookingCancelFormRepository bookingcancelformRepository;
	@Autowired
	MongoTemplate mongoTemplate;


	String loanCode;
	private String LoanCode()
	{
		long loanCount = customerLoandetailsRepository.count();

		if(loanCount<10)
		{
			loanCode = "LN000"+(loanCount+1);
		}
		else if((loanCount>=10) && (loanCount<100))
		{
			loanCode = "LN00"+(loanCount+1);
		}
		else if((loanCount>=100) && (loanCount<1000))
		{
			loanCode = "LN0"+(loanCount+1);
		}
		else
		{
			loanCode = "LN"+(loanCount+1);
		}

		return loanCode;
	}

	private List<Bank> getHousingBankName()
	{
		List<Bank> bankList= new ArrayList<Bank>();
		Query query = new Query();
		bankList = bankRepository.findAll();
		return bankList;
	}

	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping("/CustomerLoanDetails")
	public String Loan(@RequestParam("bookingId") String bookingId, ModelMap model) 
	{
		try {
			Query query = new Query();
			List<Booking> bookingList = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)),Booking.class);

			model.addAttribute("loanCode", LoanCode());
			model.addAttribute("bookingList", bookingList);

			return "CustomerLoanDetails";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/CustomerLoanDetails", method=RequestMethod.POST)
	public String addloan(@ModelAttribute CustomerLoanDetails loan, Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				loan = new CustomerLoanDetails(loan.getCustomerloanId(),loan.getBookingId(),loan.getCustomerName(),
						loan.getTotalAmount(),loan.getLoanAmount(),loan.getRemainingAmount(),
						loan.getBankName(),loan.getFileNumber(),loan.getBankPersonName(),loan.getBankPersonMobNo(),loan.getBankPersonEmailId(),
						loan.getAgainstAggreement(), loan.getAgainstStampDuty(), loan.getAgainstRegistration(), loan.getAgainstGST());
				customerLoandetailsRepository.save(loan);
				// model.addAttribute("bankStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				//model.addAttribute("bankStatus","Fail");
			}

			List<Enquiry> enquiryList;
			List<Project> projectList = GetUserAssigenedProjectList(req,res); 
			List<Booking> bookingList; 
			List<Aggreement> aggreementList = aggreementRepository.findAll();
			List<ExtraCharges> extrachargesList = extrachargesRepository.findAll(); 
			List<CustomerLoanDetails> loanList = customerLoandetailsRepository.findAll();
			List<BookingCancelForm> cancelList = bookingcancelformRepository.findAll();

			Query query = new Query();
			enquiryList = mongoTemplate.find(query.addCriteria(Criteria.where("enqStatus").is("Booking")), Enquiry.class);

			Query query1 = new Query();
			bookingList = mongoTemplate.find(query1.addCriteria(Criteria.where("bookingstatus").ne("Cancel")), Booking.class);

			model.addAttribute("aggreementList",aggreementList);
			model.addAttribute("cancelList",cancelList);
			model.addAttribute("loanList",loanList);
			model.addAttribute("extrachargesList",extrachargesList);
			model.addAttribute("bookingList", bookingList);
			model.addAttribute("projectList",projectList);
			model.addAttribute("enquiryList", enquiryList);
			return "BookingMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/CustomerLoanList")
	public String loanDetails(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<CustomerLoanDetails> loanList = customerLoandetailsRepository.findAll(); 
			model.addAttribute("loanList",loanList);
			return "CustomerLoanList";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/EditCustomerLoanDetails")
	public String EditCustomerLoan(@RequestParam("bookingId") String bookingId,ModelMap model)
	{
		try {
			Query query = new  Query();
			List<CustomerLoanDetails> loanList = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)),CustomerLoanDetails.class);

			query = new Query();
			List<Booking> bookingList = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)), Booking.class);

			model.addAttribute("loanList", loanList);
			model.addAttribute("bookingList", bookingList);
			return "EditCustomerLoanDetails";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping(value="/EditCustomerLoanDetails", method = RequestMethod.POST)
	public String editloan(Model model, @ModelAttribute CustomerLoanDetails loan, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{ 
				loan = new CustomerLoanDetails(loan.getCustomerloanId(),loan.getBookingId(),loan.getCustomerName(),
						loan.getTotalAmount(),loan.getLoanAmount(),loan.getRemainingAmount(),
						loan.getBankName(),loan.getFileNumber(),loan.getBankPersonName(),loan.getBankPersonMobNo(),loan.getBankPersonEmailId(),
						loan.getAgainstAggreement(), loan.getAgainstStampDuty(), loan.getAgainstRegistration(), loan.getAgainstGST());
				customerLoandetailsRepository.save(loan);
				// model.addAttribute("bankStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				//model.addAttribute("bankStatus","Fail");
			}



			Query query = new Query();
			List<Project> projectList = GetUserAssigenedProjectList(req,res); 
			List<Booking> bookingList1 = mongoTemplate.find(query.addCriteria(Criteria.where("bookingstatus").ne("Cancel")), Booking.class);


			List<Booking> bookingList=new ArrayList<Booking>();
			Booking booking=new Booking();

			for(int i=0;i<projectList.size();i++)
			{

				for(int j=0;j<bookingList1.size();j++)
				{
					if(projectList.get(i).getProjectId().equals(bookingList1.get(j).getProjectId()))
					{

						booking=new Booking();

						booking.setBookingId(bookingList1.get(j).getBookingId());
						booking.setEnquiryId(bookingList1.get(j).getEnquiryId());
						booking.setBookingfirstname(bookingList1.get(j).getBookingfirstname());
						booking.setBookingmiddlename(bookingList1.get(j).getBookingmiddlename());
						booking.setBookinglastname(bookingList1.get(j).getBookinglastname());
						booking.setBookingaddress(bookingList1.get(j).getBookingaddress());
						booking.setBookingPincode(bookingList1.get(j).getBookingPincode());

						booking.setBookingEmail(bookingList1.get(j).getBookingEmail());
						booking.setBookingmobileNumber1(bookingList1.get(j).getBookingmobileNumber1());
						booking.setBookingmobileNumber2(bookingList1.get(j).getBookingmobileNumber2());
						booking.setBookingOccupation(bookingList1.get(j).getBookingOccupation());
						booking.setPurposeOfFlat(bookingList1.get(j).getPurposeOfFlat());

						booking.setProjectId(bookingList1.get(j).getProjectId());
						booking.setBuildingId(bookingList1.get(j).getBuildingId());
						booking.setWingId(bookingList1.get(j).getWingId());
						booking.setFloorId(bookingList1.get(j).getFloorId());
						booking.setFlatType(bookingList1.get(j).getFlatType());
						booking.setFlatId(bookingList1.get(j).getFlatId());
						booking.setFlatFacing(bookingList1.get(j).getFlatFacing());
						booking.setFlatareainSqFt(bookingList1.get(j).getFlatareainSqFt());
						booking.setFlatCostwithotfloorise(bookingList1.get(j).getFlatCostwithotfloorise());
						booking.setFloorRise(bookingList1.get(j).getFloorRise());
						booking.setFlatCost(bookingList1.get(j).getFlatCost());
						booking.setFlatbasicCost(bookingList1.get(j).getFlatbasicCost());
						booking.setParkingFloorId(bookingList1.get(j).getParkingFloorId());
						booking.setParkingZoneId(bookingList1.get(j).getParkingZoneId());
						booking.setAgentId(bookingList1.get(j).getAgentId());

						booking.setInfrastructureCharge(bookingList1.get(j).getInfrastructureCharge());
						booking.setAggreementValue1(bookingList1.get(j).getAggreementValue1());
						booking.setHandlingCharges(bookingList1.get(j).getHandlingCharges());
						booking.setStampDuty1(bookingList1.get(j).getStampDuty1());
						booking.setStampDutyPer(bookingList1.get(j).getStampDutyPer());
						booking.setRegistrationPer(bookingList1.get(j).getRegistrationPer());
						booking.setRegistrationCost1(bookingList1.get(j).getRegistrationCost1());
						booking.setGstCost(bookingList1.get(j).getGstCost());
						booking.setGstAmount1(bookingList1.get(j).getGstAmount1());
						booking.setGrandTotal1(bookingList1.get(j).getGrandTotal1());
						booking.setTds(bookingList1.get(j).getTds());
						booking.setBookingstatus(bookingList1.get(j).getBookingstatus());
						booking.setUserName(bookingList1.get(j).getUserName());
						booking.setCreationDate(bookingList1.get(j).getCreationDate());

						bookingList.add(booking);

					}

				}

			}


			List<CustomerLoanDetails> loanList1 = customerLoandetailsRepository.findAll();


			List<CustomerLoanDetails> loanList=new ArrayList<CustomerLoanDetails>();
			CustomerLoanDetails customerloanDetails=new CustomerLoanDetails();

			for(int i=0;i<bookingList.size();i++)
			{
				for(int j=0;j<loanList1.size();j++)
				{
					if(bookingList.get(i).getBookingId().equals(loanList1.get(j).getBookingId()))
					{
						customerloanDetails=new CustomerLoanDetails();
						customerloanDetails.setCustomerloanId(loanList1.get(j).getCustomerloanId());
						customerloanDetails.setBookingId(loanList1.get(j).getBookingId());
						customerloanDetails.setCustomerName(loanList1.get(j).getCustomerName());
						customerloanDetails.setTotalAmount(loanList1.get(j).getTotalAmount());
						customerloanDetails.setLoanAmount(loanList1.get(j).getLoanAmount());
						customerloanDetails.setRemainingAmount(loanList1.get(j).getRemainingAmount());
						customerloanDetails.setBankName(loanList1.get(j).getBankName());
						customerloanDetails.setFileNumber(loanList1.get(j).getFileNumber());
						customerloanDetails.setBankPersonName(loanList1.get(j).getBankPersonName());
						customerloanDetails.setBankPersonEmailId(loanList1.get(j).getBankPersonEmailId());
						customerloanDetails.setBankPersonMobNo(loanList1.get(j).getBankPersonMobNo());
						customerloanDetails.setAgainstAggreement(loanList1.get(j).getAgainstAggreement());
						customerloanDetails.setAgainstGST(loanList1.get(j).getAgainstGST());
						customerloanDetails.setAgainstRegistration(loanList1.get(j).getAgainstRegistration());
						customerloanDetails.setAgainstStampDuty(loanList1.get(j).getAgainstStampDuty());
						loanList.add(customerloanDetails);
					}

				}

			}

			model.addAttribute("loanList",loanList);
			return "AllCustomerLoanList";

		}catch (Exception e) {
			return "login";
		}
	}

}

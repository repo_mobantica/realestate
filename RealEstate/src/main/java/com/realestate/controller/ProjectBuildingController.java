package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Company;
import com.realestate.bean.Login;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.CompanyRepository;
import com.realestate.repository.ProjectBuildingRepository;
import com.realestate.repository.ProjectRepository;

@Controller
@RequestMapping("/")
public class ProjectBuildingController
{
	@Autowired
	ProjectBuildingRepository projectBuildingRepository;
	@Autowired
	CompanyRepository companyRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	//Code for generating project code
	String buildingCode;
	private String BuildingCode()
	{
		long buildingCount = projectBuildingRepository.count();

		if(buildingCount<10)
		{
			buildingCode = "BD000"+(buildingCount+1);
		}
		else if((buildingCount>=10) && (buildingCount<100))
		{
			buildingCode = "BD00"+(buildingCount+1);
		}
		else if((buildingCount>=100) && (buildingCount<1000))
		{
			buildingCode = "BD0"+(buildingCount+1);
		}
		else
		{
			buildingCode = "BD"+(buildingCount+1);
		}

		return buildingCode;
	}

	//code for retrieving all companies
	private List<Company> getAllCompanies()
	{
		List<Company> companyList;

		companyList = companyRepository.findAll(new Sort(Sort.Direction.ASC,"companyName"));
		return companyList;
	}


	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}


	//Request Mapping For Add Project Unit
	@RequestMapping("/AddProjectBuilding")
	public String addPrijectUnit(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<Project> projectList = GetUserAssigenedProjectList(req,res); 
			model.addAttribute("projectList",projectList);
			model.addAttribute("buildingCode",BuildingCode());
			model.addAttribute("companyList",getAllCompanies());


			return "AddProjectBuilding";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/AddProjectBuilding",method=RequestMethod.POST)
	public String projectunitSave(@ModelAttribute ProjectBuilding projectBuilding, Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				projectBuilding = new ProjectBuilding(projectBuilding.getBuildingId(),projectBuilding.getProjectId(),projectBuilding.getBuildingName().toUpperCase(),projectBuilding.getNumberofWing(),projectBuilding.getBuildingStatus(),
						projectBuilding.getCreationDate(),projectBuilding.getUpdateDate(),projectBuilding.getUserName());
				projectBuildingRepository.save(projectBuilding);
				model.addAttribute("projectbuildingdbStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("projectbuildingdbStatus","Fail");
			}

			List<Project> projectList = GetUserAssigenedProjectList(req,res); 

			model.addAttribute("projectList",projectList);
			model.addAttribute("buildingCode",BuildingCode());
			model.addAttribute("companyList",getAllCompanies());

			return "AddProjectBuilding";

		}catch (Exception e) {
			return "login";
		}
	}

	//Request Mapping For Add Project Unit
	@RequestMapping("/ProjectBuildingMaster")
	public String PrijectBuidingMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<ProjectBuilding> buildingList = projectBuildingRepository.findAll();

			List<Project> projectDetails=new ArrayList<Project>();


			for(int i=0;i<buildingList.size();i++)
			{
				try
				{
					Query query = new Query();
					projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(buildingList.get(i).getProjectId())), Project.class);
					buildingList.get(i).setProjectId(projectDetails.get(0).getProjectName());


				}
				catch (Exception e) {
					System.out.println("Error= "+e);
				}
			}

			List<Project> projectList = GetUserAssigenedProjectList(req,res); 
			model.addAttribute("projectList",projectList);
			model.addAttribute("buildingList",buildingList);
			return "ProjectBuildingMaster";

		}catch (Exception e) {
			return "login";
		}
	}


	// to get all city list by country
	@ResponseBody
	@RequestMapping(value="/getbuildingList",method=RequestMethod.POST)
	public List<ProjectBuilding> projectBuildingList(@RequestParam("projectId") String projectId, HttpSession session)
	{
		List<ProjectBuilding> buildingList= new ArrayList<ProjectBuilding>();

		List<Project> projectDetails=new ArrayList<Project>();


		for(int i=0;i<buildingList.size();i++)
		{
			try
			{
				Query query = new Query();
				projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(buildingList.get(i).getProjectId())), Project.class);
				buildingList.get(i).setProjectId(projectDetails.get(0).getProjectName());

			}
			catch (Exception e) {
				System.out.println("Error= "+e);
			}
		}
		Query query = new Query();
		buildingList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)), ProjectBuilding.class);
		return buildingList;
	}

	@RequestMapping("/EditProjectBuilding")
	public String EditProjectBuilding(@RequestParam("buildingId") String buildingId, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			Query query = new Query();
			List<ProjectBuilding> projectbuildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(buildingId)),ProjectBuilding.class);
			query = new Query();
			List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectbuildingDetails.get(0).getProjectId())), Project.class);

			model.addAttribute("projectName", projectDetails.get(0).getProjectName());
			model.addAttribute("projectbuildingDetails", projectbuildingDetails);
			model.addAttribute("companyList",getAllCompanies());


			List<Project> projectList = GetUserAssigenedProjectList(req,res); 
			model.addAttribute("projectList",projectList);
			return "EditProjectBuilding";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/EditProjectBuilding",method=RequestMethod.POST)
	public String EditProjectBuildingPostMethod(@ModelAttribute ProjectBuilding projectBuilding, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				projectBuilding = new ProjectBuilding(projectBuilding.getBuildingId(),projectBuilding.getProjectId(),projectBuilding.getBuildingName().toUpperCase(),projectBuilding.getNumberofWing(),projectBuilding.getBuildingStatus(),
						projectBuilding.getCreationDate(),projectBuilding.getUpdateDate(),projectBuilding.getUserName());
				projectBuildingRepository.save(projectBuilding);

				model.addAttribute("projectbuildingdbStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("projectbuildingdbStatus","Fail");
			}


			List<ProjectBuilding> buildingList = projectBuildingRepository.findAll();


			List<Project> projectList = GetUserAssigenedProjectList(req,res); 
			model.addAttribute("projectList",projectList);

			model.addAttribute("buildingList",buildingList);

			return "ProjectBuildingMaster";

		}catch (Exception e) {
			return "login";
		}
	}

}

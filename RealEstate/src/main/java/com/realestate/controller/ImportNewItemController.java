package com.realestate.controller;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.realestate.bean.Item;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.SubSupplierType;
import com.realestate.bean.SupplierType;
import com.realestate.repository.ItemRepository;
import com.realestate.repository.SubSupplierTypeRepository;
import com.realestate.repository.SupplierTypeRepository;

@Controller
@RequestMapping("/")
public class ImportNewItemController {

	@Autowired
	ItemRepository itemRepository;

	@Autowired
	SubSupplierTypeRepository subsuppliertypeRepository;
	@Autowired
	SupplierTypeRepository suppliertypeRepository;
	@Autowired
	ServletContext context;
	@Autowired
	MongoTemplate mongoTemplate;
	Item item = new Item();
	HSSFWorkbook workbook;
	XSSFWorkbook workbook1;

	HSSFSheet worksheet;
	XSSFSheet worksheet1; 
	String itemcode;
	private String ItemCode()
	{
		long cnt  = itemRepository.count();
		if(cnt<10)
		{
			itemcode = "IT000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			itemcode = "IT00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			itemcode = "IT0"+(cnt+1);
		}
		else
		{
			itemcode = "IT"+(cnt+1);
		}
		return itemcode;
	}


	@RequestMapping(value="/ImportNewItem")
	public String importNewItem(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		return "ImportNewItem";
	}


	@SuppressWarnings({ "deprecation"})
	@RequestMapping(value = "/ImportNewItem", method = RequestMethod.POST)
	public String uploadItemDetails(@RequestParam("item_excel") MultipartFile item_excel, Model model, HttpServletRequest request, RedirectAttributes redirectAttributes, HttpSession session) 
	{
		String user = (String)request.getSession().getAttribute("user");

		List<SupplierType> suppliertypeList = suppliertypeRepository.findAll();
		List<SubSupplierType> subsuppliertypeList = subsuppliertypeRepository.findAll();

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/M/yyyy");
		LocalDate localDate = LocalDate.now();
		String todayDate=dtf.format(localDate);
		String supplierTypeId="";
		String subSupplierTypeId="";

		if (!item_excel.isEmpty() || item_excel != null )
		{
			try 
			{
				if(item_excel.getOriginalFilename().endsWith("xls") || item_excel.getOriginalFilename().endsWith("xlsx") || item_excel.getOriginalFilename().endsWith("csv"))
				{
					if(item_excel.getOriginalFilename().endsWith("xlsx"))
					{
						InputStream stream = item_excel.getInputStream();
						XSSFWorkbook workbook = new XSSFWorkbook(stream);

						XSSFSheet sheet = workbook.getSheet("Sheet1");  /// this will read 1st workbook of ExcelSheet

						int firstRow = sheet.getFirstRowNum();

						XSSFRow firstrow = sheet.getRow(firstRow);

						@SuppressWarnings("unused")
						int lastColumnCount = firstrow.getLastCellNum();

						@SuppressWarnings("unused")
						Iterator<Row> rowIterator = sheet.iterator();   
						int last_no=sheet.getLastRowNum();

						Item item = new Item();

						for(int i=0;i<last_no;i++)
						{
							supplierTypeId="";
							subSupplierTypeId="";

							try {
								XSSFRow row = sheet.getRow(i+1);

								if (row.getRowNum() == 0) 
								{
									continue;
								}

								row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);

								for(int j=0; j<suppliertypeList.size();j++)
								{
									if((suppliertypeList.get(j).getSupplierType().trim()).equalsIgnoreCase((row.getCell(0).getStringCellValue().trim())))
									{
										supplierTypeId=suppliertypeList.get(j).getSuppliertypeId();
										break;
									}
								}


								row.getCell(1).setCellType(Cell.CELL_TYPE_STRING);

								for(int j=0; j<subsuppliertypeList.size();j++)
								{
									if((subsuppliertypeList.get(j).getSubsupplierType().trim()).equalsIgnoreCase((row.getCell(1).getStringCellValue().trim())))
									{
										subSupplierTypeId=subsuppliertypeList.get(j).getSubsuppliertypeId();
										break;
									}
								}

								if(!supplierTypeId.equals("") && !subSupplierTypeId.equals(""))
								{

									item.setItemId(ItemCode());

									item.setSuppliertypeId(supplierTypeId);

									item.setSubsuppliertypeId(subSupplierTypeId);

									row.getCell(2).setCellType(Cell.CELL_TYPE_STRING);

									item.setItemName(row.getCell(2).getStringCellValue());

									row.getCell(3).setCellType(Cell.CELL_TYPE_NUMERIC);
									item.setIntemInitialPrice(row.getCell(3).getNumericCellValue());

									row.getCell(4).setCellType(Cell.CELL_TYPE_NUMERIC);
									item.setItemInitialDiscount(row.getCell(4).getNumericCellValue());

									// item.setItemSize(row.getCell(6).getStringCellValue());

									row.getCell(5).setCellType(Cell.CELL_TYPE_NUMERIC);
									item.setMinimumStock(row.getCell(5).getNumericCellValue());

									row.getCell(6).setCellType(Cell.CELL_TYPE_NUMERIC);
									item.setCurrentStock(row.getCell(6).getNumericCellValue());

									row.getCell(7).setCellType(Cell.CELL_TYPE_STRING);
									item.setHsnCode(row.getCell(7).getStringCellValue());

									row.getCell(8).setCellType(Cell.CELL_TYPE_NUMERIC);
									item.setGstPer(row.getCell(8).getNumericCellValue());

									item.setItemStatus("Active");
									item.setCreationDate(todayDate);
									item.setUpdateDate(todayDate);
									item.setUserName(user);
									itemRepository.save(item); 

								}

							}
							catch(Exception ee) {}
						}
						workbook.close();
					}

				}//if after inner if

			}
			catch(Exception e)
			{

			}
		}
		/*	
		List<Item> itemDetails;
		itemDetails = itemRepository.findAll();

		model.addAttribute("itemDetails", itemDetails);
		 */	
		return "ImportNewItem";
	}

	//private static final String INTERNAL_FILE="Newitem.xlsx";
	@RequestMapping(value = "/DownloadItemTemplate")
	public String downloadItemTemplate(Model model, HttpServletResponse response, HttpServletRequest request, RedirectAttributes redirectAttributes, HttpSession session) throws IOException 
	{
		try
		{
			String filename = "NewItem.xlsx";
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();

			String filepath = "//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"Template/";
			response.setContentType("APPLICATION/OCTET-STREAM");
			response.setHeader("Content-Disposition", "attachment; filename=\""+ filename + "\"");


			FileInputStream fileInputStream = new FileInputStream(filepath+filename);

			int i;
			while ((i = fileInputStream.read()) != -1) {
				out.write(i);
			}
			fileInputStream.close();
			out.close();
		}
		catch(Exception ee) {
		}
		return "ImportNewItem";
	}

}

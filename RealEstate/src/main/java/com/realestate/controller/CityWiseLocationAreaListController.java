package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.LocationArea;

@Controller
@RequestMapping("/")
public class CityWiseLocationAreaListController {

	@Autowired
	MongoTemplate mongoTemplate;

	@ResponseBody
	@RequestMapping(value="/getLocationAreaList",method=RequestMethod.POST)
	public List<LocationArea> LocationAreaList(@RequestParam("cityId") String cityId, @RequestParam("stateId") String stateId, @RequestParam("countryId") String countryId, HttpSession session)
	{
		List<LocationArea> chartaccountNumber= new ArrayList<LocationArea>();

		Query query = new Query();
		chartaccountNumber = mongoTemplate.find(query.addCriteria(Criteria.where("cityId").is(cityId).and("stateId").is(stateId).and("countryId").is(countryId)), LocationArea.class);

		return chartaccountNumber;
	}
}

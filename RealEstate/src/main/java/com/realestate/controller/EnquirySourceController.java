package com.realestate.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.realestate.bean.EnquirySource;
import com.realestate.repository.EnquirySourceRepository;

@Controller
@RequestMapping("/")
public class EnquirySourceController 
{
	@Autowired
	EnquirySourceRepository enquirysourceRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	String enquirysourceCode;

	private String EnquirysourceCode()
	{
		long cnt  = enquirysourceRepository.count();
		if(cnt<10)
		{
			enquirysourceCode = "ES000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			enquirysourceCode = "ES00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			enquirysourceCode = "ES0"+(cnt+1);
		}
		else
		{
			enquirysourceCode = "ES"+(cnt+1);
		}
		return enquirysourceCode;
	}


	@RequestMapping("/AddEnquirySource")
	public String addStore(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<EnquirySource> enquirysourceList = enquirysourceRepository.findAll();

			model.addAttribute("enquirysourceCode",EnquirysourceCode());
			model.addAttribute("enquirysourceList",enquirysourceList);
			return "AddEnquirySource";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value = "/AddEnquirySource", method = RequestMethod.POST)
	public String storeSave(@ModelAttribute EnquirySource enquirysource, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				enquirysource = new EnquirySource(enquirysource.getEnquirysourceId(), enquirysource.getEnquirysourceName(),enquirysource.getCreationDate(), enquirysource.getUpdateDate(), enquirysource.getUserName());
				enquirysourceRepository.save(enquirysource);
				model.addAttribute("enquirysourceStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("enquirysourceStatus","Fail");
			}
			List<EnquirySource> enquirysourceList = enquirysourceRepository.findAll();

			model.addAttribute("enquirysourceCode",EnquirysourceCode());
			model.addAttribute("enquirysourceList",enquirysourceList);
			return "AddEnquirySource";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/EnquirySourceMaster")
	public String EnquirySourceMaster(ModelMap model)
	{
		try {
			List<EnquirySource> enquirysourceList = enquirysourceRepository.findAll();

			model.addAttribute("enquirysourceList",enquirysourceList);
			return "EnquirySourceMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/EditEnquirySource")
	public String EditEnquirySource(@RequestParam("enquirysourceId") String enquirysourceId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<EnquirySource> enquirysourceDetails = mongoTemplate.find(query.addCriteria(Criteria.where("enquirysourceId").is(enquirysourceId)),EnquirySource.class);

			model.addAttribute("enquirysourceDetails", enquirysourceDetails);
			return "EditEnquirySource";

		}catch (Exception e) {
			return "login";
		}
	}
	@RequestMapping(value="/EditEnquirySource",method=RequestMethod.POST)
	public String EditEqnSource(@ModelAttribute EnquirySource enquirysource, ModelMap model)
	{
		try {
			try
			{
				enquirysource = new EnquirySource(enquirysource.getEnquirysourceId(), enquirysource.getEnquirysourceName(),enquirysource.getCreationDate(), enquirysource.getUpdateDate(), enquirysource.getUserName());
				enquirysourceRepository.save(enquirysource);
				model.addAttribute("enquirysourceStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("enquirysourceStatus","Fail");
			}

			List<EnquirySource> enquirysourceList = enquirysourceRepository.findAll();

			model.addAttribute("enquirysourceList",enquirysourceList);			
			return "EnquirySourceMaster";

		}catch (Exception e) {
			return "login";
		}
	}

}

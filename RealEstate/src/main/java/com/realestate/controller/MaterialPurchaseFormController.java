package com.realestate.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Brand;
import com.realestate.bean.Employee;
import com.realestate.bean.Item;
import com.realestate.bean.Login;
import com.realestate.bean.MaterialPurchaseRequisition;
import com.realestate.bean.MaterialPurchaseRequisitionHistory;
import com.realestate.bean.MaterialsPurchased;
import com.realestate.bean.MaterialsPurchasedHistory;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.Store;
import com.realestate.bean.SubSupplierType;
import com.realestate.bean.Supplier;
import com.realestate.bean.SupplierQuotation;
import com.realestate.bean.SupplierQuotationHistory;
import com.realestate.bean.SupplierType;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.EmployeeRepository;
import com.realestate.repository.MaterialPurchaseRequisitionHistoryRepository;
import com.realestate.repository.MaterialPurchaseRequisitionRepository;
import com.realestate.repository.MaterialsPurchasedHistoryRepository;
import com.realestate.repository.MaterialsPurchasedRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.StoreRepository;
import com.realestate.repository.SupplierRepository;

@Controller
@RequestMapping("/")
public class MaterialPurchaseFormController
{
	@Autowired
	EmployeeRepository employeeRepository;
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	MaterialsPurchasedHistoryRepository materialsPurchasedHistoryRepository;
	@Autowired
	MaterialsPurchasedRepository materialsPurchasedRepository;
	@Autowired
	MaterialPurchaseRequisitionRepository materialpurchaserequisitionRepository;
	@Autowired
	MaterialPurchaseRequisitionHistoryRepository materialpurchaserequisitionhistoryRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	SupplierRepository supplierRepository;
	@Autowired
	StoreRepository storeRepository;
	String purchasedId= null;
	public String GenerateMaterialPurchasedId(String projectName, String buildingName, String wingName)
	{
		long count = 0;
		SimpleDateFormat formatter = new SimpleDateFormat("d/M/yyyy");  
		Date date = new Date();  
		String creationDate=""+formatter.format(date);

		Query query = new Query();
		List<MaterialsPurchased> materialspPurchasedList = mongoTemplate.find(query.addCriteria(Criteria.where("creationDate").is(creationDate)), MaterialsPurchased.class);

		count=materialspPurchasedList.size()+1;
		String projectName1[]=null, buildingName1[]=null, wingName1[]=null;
		projectName1  = projectName.split(" ");
		buildingName1 = buildingName.split(" ");
		wingName1 	  = wingName.split(" ");

		purchasedId="PO/";
		if(projectName1.length==1)
		{
			purchasedId = purchasedId + projectName1[0].charAt(0) + projectName1[0].charAt(1);
		}
		else
		{
			purchasedId = purchasedId + projectName1[0].charAt(0) + projectName1[1].charAt(0);
		}
		/*
		purchasedId = purchasedId +"/";

		if(buildingName1.length==1)
		{
			if(buildingName1[0].length()==1)
			{
				purchasedId = purchasedId + buildingName1[0].charAt(0);
			}
			else
			{
				purchasedId = purchasedId + buildingName1[0].charAt(0) + buildingName1[0].charAt(1);
			}
		}
		else
		{
			purchasedId = purchasedId + buildingName1[0].charAt(0) + buildingName1[1].charAt(0);
		}
		 */
		if(count<10)
		{
			purchasedId = purchasedId+ "/"+creationDate+"/00"+count; 	
		}
		else if(count<100)
		{
			purchasedId = purchasedId+ "/"+creationDate+"/0"+count; 
		}
		else
		{
			purchasedId = purchasedId+ "/"+creationDate+"/"+count; 
		}

		return purchasedId;
	}
	long count ;
	private long MaterialsPurchasedHistriesId()
	{
		String materialsPurchasedHistriesId="";
		count = materialsPurchasedHistoryRepository.count();

		return count;
	}

	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);

			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);

			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping("/MaterialPurchasedList")
	public String MaterialPurchasedList(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<Employee> employeeList = employeeRepository.findAll();
			// List<MaterialsPurchased> purchesedList = materialsPurchasedRepository.findAll(); 
			Query query = new Query();
			List<MaterialsPurchased> purchesedList = mongoTemplate.find(query.addCriteria(Criteria.where("status").ne("Completed")),MaterialsPurchased.class);

			String empName="";
			for(int i=0;i<purchesedList.size();i++)
			{
				for(int j=0;j<employeeList.size();j++)
				{
					if(purchesedList.get(i).getEmployeeId().equals(employeeList.get(j).getEmployeeId()))
					{
						empName=employeeList.get(j).getEmployeefirstName()+" "+employeeList.get(j).getEmployeemiddleName()+" "+employeeList.get(j).getEmployeelastName();
						break;
					}
				}
				purchesedList.get(i).setEmployeeId(empName);
			}


			model.addAttribute("purchesedList", purchesedList);

			return "MaterialPurchasedList";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/getMaterialDetails")
	public HashMap<String, Object> getMaterialDetails(@RequestParam("requisitionHistoryId") String requisitionHistoryId, @RequestParam("itemId") String itemId)
	{
		HashMap<String, Object> materialDetails= new HashMap<String, Object>();
		Query query = new Query();
		List<MaterialPurchaseRequisitionHistory> materialpurchaserequisitionhistory = mongoTemplate.find(query.addCriteria(Criteria.where("requisitionHistoryId").is(requisitionHistoryId)),MaterialPurchaseRequisitionHistory.class);

		materialDetails.put("requisitionHistoryId", materialpurchaserequisitionhistory.get(0).getRequisitionHistoryId());
		materialDetails.put("requisitionId", materialpurchaserequisitionhistory.get(0).getRequisitionId());
		materialDetails.put("itemId", materialpurchaserequisitionhistory.get(0).getItemId());
		materialDetails.put("itemName", materialpurchaserequisitionhistory.get(0).getItemName());
		materialDetails.put("itemunitName", materialpurchaserequisitionhistory.get(0).getItemunitName());
		materialDetails.put("itemSize", materialpurchaserequisitionhistory.get(0).getItemSize());
		materialDetails.put("itemQuantity", materialpurchaserequisitionhistory.get(0).getItemQuantity());

		query = new Query();
		List<Item> itemDetails = mongoTemplate.find(query.addCriteria(Criteria.where("itemId").is(itemId)),Item.class);

		materialDetails.put("intemInitialPrice", itemDetails.get(0).getIntemInitialPrice());
		materialDetails.put("itemInitialDiscount", itemDetails.get(0).getItemInitialDiscount());
		materialDetails.put("gstPer", itemDetails.get(0).getGstPer());


		//	materialDetails.put("itemMainCategoryName", itemDetails.get(0).getItemMainCategoryName());
		//materialDetails.put("itemCategoryName", itemDetails.get(0).getItemCategoryName());
		return materialDetails;
	}

	@RequestMapping("/MaterialPurchaseForm")
	public String MaterialPurchaseForm(@RequestParam("requisitionId") String requisitionId, ModelMap model)
	{

		try {
			Query query = new Query();

			query = new Query();

			Query query1 = new Query();

			List<MaterialPurchaseRequisition> materialpurchaserequisition = mongoTemplate.find(query1.addCriteria(Criteria.where("requisitionId").is(requisitionId)),MaterialPurchaseRequisition.class);

			query = new Query();
			List<MaterialPurchaseRequisitionHistory> materialpurchaserequisitionhistory = mongoTemplate.find(query.addCriteria(Criteria.where("requisitionId").is(requisitionId)),MaterialPurchaseRequisitionHistory.class);

			List<MaterialPurchaseRequisitionHistory> materialpurchaserequisitionhistory1 = new ArrayList<MaterialPurchaseRequisitionHistory>();

			MaterialPurchaseRequisitionHistory material = new MaterialPurchaseRequisitionHistory();
			try
			{

				for(int i=0;i<materialpurchaserequisitionhistory.size();i++)
				{
					material = new MaterialPurchaseRequisitionHistory();
					query = new Query();
					List<Item> itemDetails = mongoTemplate.find(query.addCriteria(Criteria.where("itemId").is(materialpurchaserequisitionhistory.get(i).getItemId())),Item.class);
					material.setRequisitionHistoryId(materialpurchaserequisitionhistory.get(i).getRequisitionHistoryId());
					material.setRequisitionId(materialpurchaserequisitionhistory.get(i).getRequisitionId()); 
					material.setItemId(materialpurchaserequisitionhistory.get(i).getItemId());
					material.setItemName(itemDetails.get(0).getItemName());
					try
					{
						query =new Query();
						List<SupplierType> suppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("suppliertypeId").is(itemDetails.get(0).getSuppliertypeId())), SupplierType.class);
						material.setSuppliertypeId(suppliertypeDetails.get(0).getSupplierType());	

						query =new Query();
						List<SubSupplierType> subsuppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subsuppliertypeId").is(itemDetails.get(0).getSubsuppliertypeId())), SubSupplierType.class);
						material.setSubsuppliertypeId(subsuppliertypeDetails.get(0).getSubsupplierType());
					}
					catch (Exception e) {
						// TODO: handle exception
					}

					material.setGstPer(itemDetails.get(0).getGstPer());
					material.setItemInitialDiscount(itemDetails.get(0).getItemInitialDiscount());
					material.setItemInitialPrice(itemDetails.get(0).getIntemInitialPrice());
					material.setItemQuantity(materialpurchaserequisitionhistory.get(i).getItemQuantity());
					material.setItemSize(materialpurchaserequisitionhistory.get(i).getItemSize());
					material.setItemunitName(materialpurchaserequisitionhistory.get(i).getItemunitName());
					materialpurchaserequisitionhistory1.add(material);

				}
			}catch (Exception e) {
				System.out.println("error= = "+e);
				// TODO: handle exception
			}

			String empId=materialpurchaserequisition.get(0).getEmployeeId();
			String empName="";

			query = new Query();
			List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(materialpurchaserequisition.get(0).getProjectId())),Project.class);
			query =new Query();
			List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(materialpurchaserequisition.get(0).getBuildingId())), ProjectBuilding.class);

			query =new Query();
			List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(materialpurchaserequisition.get(0).getWingId())), ProjectWing.class);

			query =new Query();
			List<Employee> employeeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(materialpurchaserequisition.get(0).getEmployeeId())), Employee.class);
			empName=employeeDetails.get(0).getEmployeefirstName()+" "+employeeDetails.get(0).getEmployeemiddleName()+" "+employeeDetails.get(0).getEmployeelastName();

			materialpurchaserequisition.get(0).setProjectId(projectDetails.get(0).getProjectName());
			materialpurchaserequisition.get(0).setBuildingId(buildingDetails.get(0).getBuildingName());
			materialpurchaserequisition.get(0).setWingId(wingDetails.get(0).getWingName());
			materialpurchaserequisition.get(0).setEmployeeId(empName);

			List<Supplier> supplierList = supplierRepository.findAll();
			List<Store> storeList = storeRepository.findAll();
			model.addAttribute("employeeId", empId);
			model.addAttribute("supplierList", supplierList);
			model.addAttribute("storeList", storeList);
			model.addAttribute("materialsPurchasedId", GenerateMaterialPurchasedId(projectDetails.get(0).getProjectName(),buildingDetails.get(0).getBuildingName(),wingDetails.get(0).getWingName()));
			model.addAttribute("materialpurchaserequisition",materialpurchaserequisition);

			model.addAttribute("materialsPurchasedHistriesId",MaterialsPurchasedHistriesId());
			model.addAttribute("materialpurchaserequisitionhistory", materialpurchaserequisitionhistory1);

			return "MaterialPurchaseForm";

		}catch (Exception e) {
			return "login";
		}
	}



	@RequestMapping(value = "/MaterialPurchaseForm", method = RequestMethod.POST)
	public String SaveMaterialPurchaseForm(@ModelAttribute MaterialsPurchased materialspurchased, Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{

				Double totalPrice=0.0;
				Double totalDiscount=0.0;
				Double totalGstAmount=0.0;
				Double totalAmount=0.0;

				String itemIds=req.getParameter("itemIds");
				String itemNames=req.getParameter("itemNames");
				String itemSizes=req.getParameter("itemSizes");
				String itemUnits=req.getParameter("itemUnits");
				String itemBrandNames=req.getParameter("itemBrandNames");
				String itemQuantitys=req.getParameter("quantitys");
				String rates=req.getParameter("rates");
				String totals=req.getParameter("totals");
				String discountPers=req.getParameter("discountPers");
				String discountAmounts=req.getParameter("discountAmounts");
				String gstPers=req.getParameter("gstPers");

				String gstAmounts=req.getParameter("gstAmounts");
				String grandTotals=req.getParameter("grandTotals");

				String[] itemIdsArray = itemIds.split("###!###");
				String[] itemNamesArray = itemNames.split("###!###");
				String[] itemSizesArray = itemSizes.split("###!###");
				String[] itemUnitsArray = itemUnits.split("###!###");
				String[] itemBrandNamesArray = itemBrandNames.split("###!###");
				String[] itemQuantitysArray = itemQuantitys.split("###!###");
				String[] ratesArray = rates.split("###!###");
				String[] totalsArray = totals.split("###!###");
				String[] discountPersArray = discountPers.split("###!###");
				String[] discountAmountsArray = discountAmounts.split("###!###");
				String[] gstPersArray = gstPers.split("###!###");

				String[] gstAmountsArray = gstAmounts.split("###!###");
				String[] grandTotalsArray = grandTotals.split("###!###");

				long count=MaterialsPurchasedHistriesId();
				String materialsPurchasedHistriesId="";
				MaterialsPurchasedHistory materialpurches = new MaterialsPurchasedHistory();
				for(int i=0;i<itemIdsArray.length;i++)
				{
					materialsPurchasedHistriesId="";
					count=count+i;
					if(count<10)
					{
						materialsPurchasedHistriesId = "MPD000"+(count);
					}
					else if((count<100) && (count>=10))
					{
						materialsPurchasedHistriesId = "MPD00"+(count);
					}
					else if((count<1000) && (count>=100))
					{
						materialsPurchasedHistriesId = "MPD0"+(count);
					}
					else if(count>=1000)
					{
						materialsPurchasedHistriesId = "MPD"+(count);
					}
					materialpurches = new MaterialsPurchasedHistory();
					materialpurches.setMaterialPurchesHistoriId(materialsPurchasedHistriesId);
					materialpurches.setMaterialsPurchasedId(materialspurchased.getMaterialsPurchasedId());
					materialpurches.setRequisitionId(materialspurchased.getRequisitionId());
					materialpurches.setItemId(itemIdsArray[i]);
					materialpurches.setItemName(itemNamesArray[i]);
					materialpurches.setItenSize(itemSizesArray[i]);
					materialpurches.setItemUnit(itemUnitsArray[i]);
					materialpurches.setItemBrandName(itemBrandNamesArray[i]);
					materialpurches.setItemQuantity(Double.valueOf(itemQuantitysArray[i]));

					materialpurches.setRate(Double.valueOf(ratesArray[i]));
					materialpurches.setTotal(Double.valueOf(totalsArray[i]));
					materialpurches.setDiscountPer(Double.valueOf(discountPersArray[i]));
					materialpurches.setDiscountAmount(Double.valueOf(discountAmountsArray[i]));
					materialpurches.setGstPer(Double.valueOf(gstPersArray[i]));
					materialpurches.setGstAmount(Double.valueOf(gstAmountsArray[i]));
					materialpurches.setGrandTotal(Double.valueOf(grandTotalsArray[i]));

					materialsPurchasedHistoryRepository.save(materialpurches);


					totalPrice=totalPrice+Double.valueOf(totalsArray[i]);
					totalDiscount=totalDiscount+Double.valueOf(discountAmountsArray[i]);
					totalGstAmount=totalGstAmount+Double.valueOf(gstAmountsArray[i]);
					totalAmount=totalAmount+Double.valueOf(grandTotalsArray[i]);


				}
				double totalchargeWithtransport=totalAmount+materialspurchased.getTransportCharges();
				materialspurchased = new MaterialsPurchased(materialspurchased.getMaterialsPurchasedId(), materialspurchased.getRequisitionId(),
						materialspurchased.getSupplierId(), materialspurchased.getStoreId(),materialspurchased.getEmployeeId(),
						totalPrice, totalDiscount, totalGstAmount, totalAmount, materialspurchased.getTransportCharges(),totalchargeWithtransport,
						materialspurchased.getDelivery(),materialspurchased.getPaymentterms(),materialspurchased.getAfterdelivery(),
						materialspurchased.getStatus(),materialspurchased.getPaymentStatus(), materialspurchased.getCreationDate(), materialspurchased.getUpdateDate(), materialspurchased.getUserName());
				materialsPurchasedRepository.save(materialspurchased);

				model.addAttribute("Status","Success");

			}
			catch(DuplicateKeyException de)
			{
				de.printStackTrace();
				model.addAttribute("Status","Fail");
			}

			try {
				MaterialPurchaseRequisition materialpurchaserequisition;
				materialpurchaserequisition = mongoTemplate.findOne(Query.query(Criteria.where("requisitionId").is(materialspurchased.getRequisitionId())), MaterialPurchaseRequisition.class);
				materialpurchaserequisition.setStatus("Completed");
				materialpurchaserequisitionRepository.save(materialpurchaserequisition);
			}
			catch(Exception ee)
			{
			}


			List<Employee> employeeList = employeeRepository.findAll();
			List<Project> projectList = GetUserAssigenedProjectList(req,res);
			String empName="";
			Query query1 = new Query();
			List<MaterialPurchaseRequisition> allItemList = mongoTemplate.find(query1.addCriteria(Criteria.where("status").is("Approved")), MaterialPurchaseRequisition.class);

			for(int i=0;i<allItemList.size();i++)
			{
				for(int j=0;j<employeeList.size();j++)
				{
					if(allItemList.get(i).getEmployeeId().equals(employeeList.get(j).getEmployeeId()))
					{
						empName=employeeList.get(j).getEmployeefirstName()+" "+employeeList.get(j).getEmployeemiddleName()+" "+employeeList.get(j).getEmployeelastName();
						break;
					}
				}
				allItemList.get(i).setEmployeeId(empName);
			}

			model.addAttribute("employeeList", employeeList);
			model.addAttribute("projectList", projectList);
			model.addAttribute("allItemList",allItemList);

			return "MaterialPurchaseRequisitionList";

		}catch (Exception e) {
			e.printStackTrace();
			return "login";
		}
	}

	/*	  
		 @ResponseBody
		 @RequestMapping("/getItemBrandName")
		 public List<Brand> getItemBrandName(@RequestParam("itemId") String itemId )
		 {
			 Query query =new Query();
			 Query query1 =new Query();
			 List<Item> itemList = mongoTemplate.find(query.addCriteria(Criteria.where("itemId").is(itemId)), Item.class);
			 String mainitemcategoryName=itemList.get(0).getItemMainCategoryName();
			 String mainsubitemcategoryName=itemList.get(0).getItemCategoryName();

			 List<Brand> brandList = mongoTemplate.find(query1.addCriteria(Criteria.where("mainitemcategoryName").is(mainitemcategoryName).and("mainsubitemcategoryName").is(mainsubitemcategoryName)), Brand.class);
			 return brandList;			 
		 }
	 */
	@ResponseBody
	@RequestMapping("/AddItemRequisitionPurches")
	public List<MaterialsPurchasedHistory> AddItemRequisitionPurches(@RequestParam("materialsPurchasedHistriesId") String materialsPurchasedHistriesId, @RequestParam("materialsPurchasedId") String materialsPurchasedId, @RequestParam("requisitionId") String requisitionId, @RequestParam("itemId") String itemId, @RequestParam("itenSize") String itenSize, 
			@RequestParam("itemUnit") String itemUnit, @RequestParam("subsuppliertypeId") String subsuppliertypeId, @RequestParam("itemName") String itemName, @RequestParam("itemBrandName") String itemBrandName, @RequestParam("itemQuantity") double itemQuantity, @RequestParam("rate") double rate, @RequestParam("total") double total,  
			@RequestParam("discountPer") double discountPer,
			@RequestParam("discountAmount") double discountAmount,
			@RequestParam("gstPer") double gstPer, @RequestParam("gstAmount") double gstAmount,  @RequestParam("grandTotal") double grandTotal, HttpSession session)
	{
		/*DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/M/yyyy");
				LocalDate localDate = LocalDate.now();
				String todayDate=dtf.format(localDate);*/
		try
		{
			MaterialsPurchasedHistory materialpurches = new MaterialsPurchasedHistory();

			materialpurches.setMaterialPurchesHistoriId(materialsPurchasedHistriesId);
			materialpurches.setMaterialsPurchasedId(materialsPurchasedId);
			materialpurches.setRequisitionId(requisitionId);
			materialpurches.setItemId(itemId);
			materialpurches.setItemName(itemName);
			materialpurches.setItenSize(itenSize);
			materialpurches.setItemUnit(itemUnit);
			materialpurches.setItemBrandName(itemBrandName);
			materialpurches.setItemQuantity(itemQuantity);

			materialpurches.setRate(rate);
			materialpurches.setTotal(total);
			materialpurches.setDiscountPer(discountPer);
			materialpurches.setDiscountAmount(discountAmount);
			materialpurches.setGstPer(gstPer);
			materialpurches.setGstAmount(gstAmount);
			materialpurches.setGrandTotal(grandTotal);

			materialsPurchasedHistoryRepository.save(materialpurches);

		}
		catch(Exception e)
		{

		}
		return null;
	}

	@ResponseBody
	@RequestMapping("/DeletePurchesedItem")
	public List<MaterialsPurchasedHistory> DeleteAgentEmployee(@RequestParam("materialPurchesHistoriId") String materialPurchesHistoriId, @RequestParam("requisitionId") String requisitionId, HttpSession session)
	{
		try
		{
			Query query = new Query();
			mongoTemplate.remove(query.addCriteria(Criteria.where("materialPurchesHistoriId").is(materialPurchesHistoriId)), MaterialsPurchasedHistory.class);
			Query query1 = new Query();
			List<MaterialsPurchasedHistory> materialspurchasedhistory = mongoTemplate.find(query1.addCriteria(Criteria.where("requisitionId").is(requisitionId)),MaterialsPurchasedHistory.class);
			return materialspurchasedhistory;
		}
		catch(Exception e)
		{
			return null;
		}

	} 		


	/*  
	 @ResponseBody
	 @RequestMapping("/getSupplierQuotation")
	 public List<SupplierQuotationHistory> getSupplierQuotation(@RequestParam("requisitionId") String requisitionId, @RequestParam("supplierId") String supplierId)
	 {
		 Query query =new Query();

		 List<SupplierQuotation> supplierquotationList1 = mongoTemplate.find(query.addCriteria(Criteria.where("requisitionId").is(requisitionId).and("supplierId").is(supplierId)), SupplierQuotation.class);
		 String quotationId=supplierquotationList1.get(supplierquotationList1.size()-1).getQuotationId();

		 query = new Query();
		  List<SupplierQuotationHistory> supplierquotationHistoriesList = mongoTemplate.find(query.addCriteria(Criteria.where("quotationId").is(quotationId)), SupplierQuotationHistory.class);

		  List<SupplierQuotationHistory> supplierquotationHistoriesList1 = new ArrayList<SupplierQuotationHistory>();

		  SupplierQuotationHistory material = new SupplierQuotationHistory();

		  for(int i=0;i<supplierquotationHistoriesList.size();i++)
		  {
			  material = new SupplierQuotationHistory();
			  query = new Query();
			  List<Item> itemDetails = mongoTemplate.find(query.addCriteria(Criteria.where("itemId").is(supplierquotationHistoriesList.get(i).getItemId())),Item.class);

			  material.setQuotationHistoriId(supplierquotationHistoriesList.get(i).getQuotationHistoriId());
			  material.setQuotationId(supplierquotationHistoriesList.get(i).getQuotationId());
			  material.setItemId(itemDetails.get(0).getItemId());

			  material.setItemName(itemDetails.get(0).getItemName());
			  material.setItemMainCategoryName(itemDetails.get(0).getItemMainCategoryName());
			  material.setItemCategoryName(itemDetails.get(0).getItemCategoryName());
			  material.setItemUnit(supplierquotationHistoriesList.get(i).getItemUnit());
			  material.setItemSize(supplierquotationHistoriesList.get(i).getItemUnit());
			  material.setItemBrandName(supplierquotationHistoriesList.get(i).getItemBrandName());
			  material.setQuantity(supplierquotationHistoriesList.get(i).getQuantity());
			  material.setRate(supplierquotationHistoriesList.get(i).getRate());
			  material.setTotal(supplierquotationHistoriesList.get(i).getTotal());
			  material.setDiscountPer(supplierquotationHistoriesList.get(i).getDiscountPer());
			  material.setDiscountAmount(supplierquotationHistoriesList.get(i).getDiscountAmount());
			  material.setGstPer(supplierquotationHistoriesList.get(i).getGstPer());
			  material.setGstAmount(supplierquotationHistoriesList.get(i).getGstAmount());
			  material.setGrandTotal(supplierquotationHistoriesList.get(i).getGrandTotal());

			  supplierquotationHistoriesList1.add(material);
		  }


		 return supplierquotationHistoriesList1;			 
	 }
	 */

}
package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Country;
import com.realestate.bean.Currency;
import com.realestate.repository.CountryRepository;
import com.realestate.repository.CurrencyRepository;

@Controller
@RequestMapping("/")
public class CurrencyController 
{
	@Autowired
	CurrencyRepository currencyRepository;
	@Autowired
	CountryRepository countryRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	String currencycode;

	private String currencyCode()
	{
		long cnt  = currencyRepository.count();
		if(cnt<10)
		{
			currencycode = "CU000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			currencycode = "CU00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			currencycode = "CU0"+(cnt+1);
		}
		else
		{
			currencycode = "CU"+(cnt+1);
		}
		return currencycode;
	}


	//code to reterive all country name
	private List<Country> findAllCountryName()
	{
		List<Country> countryList;
		countryList = countryRepository.findAll(new Sort(Sort.Direction.ASC,"countryName"));

		return countryList;
	}


	@ResponseBody
	@RequestMapping("/searchNameWiseCurrency")
	public List<Currency> SearchNameWiseCurrency(@RequestParam("currencyName") String currencyName)
	{
		Query query = new Query();
		List<Currency> currencyList = mongoTemplate.find(query.addCriteria(Criteria.where("currencyName").regex("^"+currencyName+".*","i")), Currency.class);

		return currencyList;
	}

	//mapping for Currency Master
	@RequestMapping("/CurrencyMaster")
	public String CurrencyMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<Currency> currencyList = currencyRepository.findAll();

			List<Country> countryList= countryRepository.findAll();
			int i,j;
			for(i=0;i<currencyList.size();i++)
			{
				try 
				{
					for(j=0;j<countryList.size();j++)
					{
						if(currencyList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
						{
							currencyList.get(i).setCountryId(countryList.get(j).getCountryName());
							break;
						}
					}
				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}

			model.addAttribute("countryList", countryList);
			model.addAttribute("currencyList", currencyList);

			return "CurrencyMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	//to Retive All Currencies in Currency Master
	@ResponseBody
	@RequestMapping(value="/getCurrencyList",method=RequestMethod.POST)
	public List<Currency> CurrencyList(@RequestParam("countryId") String countryId, HttpSession session)
	{
		List<Currency> currencyList= new ArrayList<Currency>();

		Query query = new Query();
		currencyList = mongoTemplate.find(query.addCriteria(Criteria.where("countryId").is(countryId)), Currency.class);

		List<Country> countryList= countryRepository.findAll();
		int i,j;
		for(i=0;i<currencyList.size();i++)
		{
			try 
			{
				for(j=0;j<countryList.size();j++)
				{
					if(currencyList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
					{
						currencyList.get(i).setCountryId(countryList.get(j).getCountryName());
						break;
					}
				}
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		return currencyList;
	}
	//mapping for AddCurrency
	@RequestMapping("/AddCurrency")
	public String addCurrency(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<Currency> currencyList;
			currencyList = currencyRepository.findAll();

			model.addAttribute("currencycode",currencyCode());
			model.addAttribute("countryList", findAllCountryName());
			model.addAttribute("currencyList", currencyList);

			return "AddCurrency";

		}catch (Exception e) {
			return "login";
		}
	}

	//For save New Currency
	@RequestMapping(value = "/AddCurrency", method = RequestMethod.POST)
	public String currencySave(@ModelAttribute Currency currency, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				currency = new Currency(currency.getCurrencyId(),currency.getCountryId(), currency.getCurrencyName(),currency.getCreationDate(), currency.getUpdateDate(), currency.getUserName());
				currencyRepository.save(currency);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}
			List<Currency> currencyList;
			currencyList = currencyRepository.findAll();

			model.addAttribute("currencycode",currencyCode());
			model.addAttribute("countryList", findAllCountryName());
			model.addAttribute("currencyList", currencyList);

			return "AddCurrency";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/getcurrencyList")
	public List<Currency> AllStateList(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		List<Currency> currencyList= currencyRepository.findAll();

		List<Country> countryList= countryRepository.findAll();
		int i,j;
		for(i=0;i<currencyList.size();i++)
		{
			try 
			{
				for(j=0;j<countryList.size();j++)
				{
					if(currencyList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
					{
						currencyList.get(i).setCountryId(countryList.get(j).getCountryName());
						break;
					}
				}
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		return currencyList;
	}

	@RequestMapping("/EditCurrency")
	public String EditCurrency(@RequestParam("currencyId") String currencyId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<Currency> currencyDetails = mongoTemplate.find(query.addCriteria(Criteria.where("currencyId").is(currencyId)), Currency.class);
			query = new Query();
			List<Country> countryDetails = mongoTemplate.find(query.addCriteria(Criteria.where("countryId").is(currencyDetails.get(0).getCountryId())), Country.class);

			model.addAttribute("countryName", countryDetails.get(0).getCountryName());
			model.addAttribute("countryList", findAllCountryName());
			model.addAttribute("currencyDetails",currencyDetails);
			return "EditCurrency";

		}catch (Exception e) {
			return "login";
		}
	}
	@RequestMapping(value="/EditCurrency", method=RequestMethod.POST)
	public String EditCurrencyPostMethod(@ModelAttribute Currency currency, ModelMap model)
	{
		try {
			try
			{
				currency = new Currency(currency.getCurrencyId(),currency.getCountryId(), currency.getCurrencyName(),currency.getCreationDate(), currency.getUpdateDate(), currency.getUserName());
				currencyRepository.save(currency);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}

			List<Currency> currencyList;
			currencyList = currencyRepository.findAll();

			List<Country> countryList= countryRepository.findAll();
			int i,j;
			for(i=0;i<currencyList.size();i++)
			{
				try 
				{
					for(j=0;j<countryList.size();j++)
					{
						if(currencyList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
						{
							currencyList.get(i).setCountryId(countryList.get(j).getCountryName());
							break;
						}
					}
				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}

			model.addAttribute("countryList", findAllCountryName());
			model.addAttribute("currencyList", currencyList);

			return "CurrencyMaster";

		}catch (Exception e) {
			return "login";
		}
	}
}

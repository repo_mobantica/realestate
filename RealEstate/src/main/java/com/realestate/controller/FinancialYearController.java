package com.realestate.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.realestate.bean.FinancialYear;
import com.realestate.repository.FinancialYearRepository;

@Controller
@RequestMapping("/")
public class FinancialYearController 
{
	@Autowired
	FinancialYearRepository financialyearRepository;

	String financialyearCode;
	private String FinancialyearCode()
	{
		long cnt  = financialyearRepository.count();
		if(cnt<10)
		{
			financialyearCode = "FY000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			financialyearCode = "FY00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			financialyearCode = "FY0"+(cnt+1);
		}
		else
		{
			financialyearCode = "FY"+(cnt+1);
		}
		return financialyearCode;
	}


	@RequestMapping("/AddFinancialYear")
	public String addFinancialYear(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<FinancialYear> financialyearList= financialyearRepository.findAll();

			model.addAttribute("financialyearCode",FinancialyearCode());
			model.addAttribute("financialyearList", financialyearList);
			return "AddFinancialYear";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping(value = "/AddFinancialYear", method = RequestMethod.POST)
	public String financialyearSave(@ModelAttribute FinancialYear financialyear, Model model)
	{
		try {
			try
			{
				financialyear = new FinancialYear(financialyear.getFinancialyearId(), financialyear.getFinancialstartDate(), financialyear.getFinancialendDate(),financialyear.getCreationDate(), financialyear.getUpdateDate(), financialyear.getUserName());
				financialyearRepository.save(financialyear);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}

			List<FinancialYear> financialyearList= financialyearRepository.findAll();

			model.addAttribute("financialyearCode",FinancialyearCode());
			model.addAttribute("financialyearList", financialyearList);
			return "AddFinancialYear";

		}catch (Exception e) {
			return "login";
		}
	}
}

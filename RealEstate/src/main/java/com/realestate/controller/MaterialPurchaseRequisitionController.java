package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.MaterialPurchaseRequisition;
import com.realestate.bean.MaterialPurchaseRequisitionHistory;
import com.realestate.bean.Employee;
import com.realestate.bean.Item;
import com.realestate.bean.ItemUnit;
import com.realestate.bean.Login;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.SupplierType;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.EmployeeRepository;
import com.realestate.repository.ItemRepository;
import com.realestate.repository.ItemUnitRepository;
import com.realestate.repository.MaterialPurchaseRequisitionRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.SupplierTypeRepository;
import com.realestate.services.SmsServices;
import com.realestate.repository.MaterialPurchaseRequisitionHistoryRepository;

@Controller
@RequestMapping("/")
public class MaterialPurchaseRequisitionController {
	@Autowired
	EmployeeRepository employeeRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	ItemRepository itemRepository;
	@Autowired
	ItemUnitRepository itemunitRepository;
	@Autowired
	MaterialPurchaseRequisitionRepository materialpurchaserequisitionRepository;
	@Autowired
	MaterialPurchaseRequisitionHistoryRepository materialpurchaserequisitionhistoryRepository;
	@Autowired
	SupplierTypeRepository suppliertypeRepository;
	@Autowired
	MongoTemplate mongoTemplate;


	String requisitionCode;
	private String RequisitionCode()
	{
		long mcount = materialpurchaserequisitionRepository.count();

		if(mcount<10)
		{
			requisitionCode = "MPR000"+(mcount+1);
		}
		else if((mcount>=10) && (mcount<100))
		{
			requisitionCode = "MPR00"+(mcount+1);
		}
		else if((mcount>=100) && (mcount<1000))
		{
			requisitionCode = "MPR0"+(mcount+1);
		}
		else
		{
			requisitionCode = "MPR"+(mcount+1);
		}

		return requisitionCode;
	}

	String requisitionHistoryCode;
	private String RequisitionHistoryCode()
	{
		long mcount = materialpurchaserequisitionhistoryRepository.count();

		if(mcount<10)
		{
			requisitionHistoryCode = "MPR000"+(mcount+1);
		}
		else if((mcount>=10) && (mcount<100))
		{
			requisitionHistoryCode = "MPR00"+(mcount+1);
		}
		else if((mcount>=100) && (mcount<1000))
		{
			requisitionHistoryCode = "MPR0"+(mcount+1);
		}
		else if(mcount>1000)
		{
			requisitionHistoryCode = "MPR"+(mcount+1);
		}

		return requisitionHistoryCode;
	}

	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping("/MaterialPurchaseRequisitionMaster")
	public String MaterialPurchaseRequisitionMaster(Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<Employee> employeeList = employeeRepository.findAll();
			List<Project> projectList = GetUserAssigenedProjectList(req,res);
			String empName="";
			Query query1 = new Query();
			List<MaterialPurchaseRequisition> allItemList = mongoTemplate.find(query1.addCriteria(Criteria.where("status").is("Applied")), MaterialPurchaseRequisition.class);

			Query query = new Query();
			for(int i=0;i<allItemList.size();i++)
			{
				try
				{
					query = new Query();
					List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(allItemList.get(i).getProjectId())),Project.class);
					query =new Query();
					List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(allItemList.get(i).getBuildingId())), ProjectBuilding.class);

					query =new Query();
					List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(allItemList.get(i).getWingId())), ProjectWing.class);
					query =new Query();
					List<Employee> employeeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(allItemList.get(i).getEmployeeId())), Employee.class);
					empName=employeeDetails.get(0).getEmployeefirstName()+" "+employeeDetails.get(0).getEmployeemiddleName()+" "+employeeDetails.get(0).getEmployeelastName();

					allItemList.get(i).setProjectId(projectDetails.get(0).getProjectName());
					allItemList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
					allItemList.get(i).setWingId(wingDetails.get(0).getWingName());
					allItemList.get(i).setEmployeeId(empName);
				} 
				catch (Exception e) {
					// TODO: handle exception
				}

			}

			model.addAttribute("employeeList", employeeList);
			model.addAttribute("projectList", projectList);
			model.addAttribute("allItemList",allItemList);
			return "MaterialPurchaseRequisitionMaster";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping("/EditMaterialPurchaseRequisition")
	public String EditMaterialPurchaseRequisition(@RequestParam("requisitionId") String requisitionId, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			Query query = new Query();
			List<MaterialPurchaseRequisition> materialpurchaserequisition = mongoTemplate.find(query.addCriteria(Criteria.where("requisitionId").is(requisitionId)),MaterialPurchaseRequisition.class);

			List<Employee> employeeList = employeeRepository.findAll(); 
			List<Project> projectList = GetUserAssigenedProjectList(req,res);
			List<ItemUnit> itemUnitList = itemunitRepository.findAll();
			List<SupplierType> suppliertypeList = suppliertypeRepository.findAll();

			query = new Query();
			Project projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(materialpurchaserequisition.get(0).getProjectId())),Project.class);
			query =new Query();
			ProjectBuilding buildingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(materialpurchaserequisition.get(0).getBuildingId())), ProjectBuilding.class);

			query =new Query();
			ProjectWing wingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(materialpurchaserequisition.get(0).getWingId())), ProjectWing.class);

			query =new Query();
			Employee employeeDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("employeeId").is(materialpurchaserequisition.get(0).getEmployeeId())), Employee.class);


			String empName=employeeDetails.getEmployeefirstName()+" "+employeeDetails.getEmployeemiddleName()+" "+employeeDetails.getEmployeelastName();

			materialpurchaserequisition.get(0).setEmployeeName(empName);

			query = new Query();
			List<MaterialPurchaseRequisitionHistory> materialpurchaserequisitionhistory = mongoTemplate.find(query.addCriteria(Criteria.where("requisitionId").is(requisitionId)),MaterialPurchaseRequisitionHistory.class);

			for(int i=0; i<materialpurchaserequisitionhistory.size();i++)
			{
				query = new Query();
				List<Item> itemDetails = mongoTemplate.find(query.addCriteria(Criteria.where("itemId").is(materialpurchaserequisitionhistory.get(i).getItemId())),Item.class);
				materialpurchaserequisitionhistory.get(i).setItemId(itemDetails.get(0).getItemName());

			}
			model.addAttribute("projectName",projectDetails.getProjectName()); 
			model.addAttribute("buildingName", buildingDetails.getBuildingName());
			model.addAttribute("wingName", wingDetails.getWingName());

			model.addAttribute("suppliertypeList",suppliertypeList); 
			model.addAttribute("itemUnitList", itemUnitList);
			model.addAttribute("employeeList", employeeList);
			model.addAttribute("projectList", projectList);
			model.addAttribute("materialpurchaserequisition",materialpurchaserequisition);
			model.addAttribute("materialpurchaserequisitionhistory", materialpurchaserequisitionhistory);
			return "EditMaterialPurchaseRequisition";

		}catch (Exception e) {
			return "login";
		}
	}



	@RequestMapping(value="/EditMaterialPurchaseRequisition",method=RequestMethod.POST)
	public String SaveEditMaterialPurchaseRequisition(@ModelAttribute MaterialPurchaseRequisition materialpurchaserequisition, Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				materialpurchaserequisition = new MaterialPurchaseRequisition(materialpurchaserequisition.getRequisitionId(),materialpurchaserequisition.getEmployeeId(),materialpurchaserequisition.getProjectId(),materialpurchaserequisition.getBuildingId(),materialpurchaserequisition.getWingId(),materialpurchaserequisition.getRequiredDate(),materialpurchaserequisition.getTotalNoItem(),materialpurchaserequisition.getTotalQuantity(),materialpurchaserequisition.getStatus(),materialpurchaserequisition.getCreationDate(),materialpurchaserequisition.getUpdateDate(),materialpurchaserequisition.getUserName());
				materialpurchaserequisitionRepository.save(materialpurchaserequisition);
				model.addAttribute("materialpurchaserequisitionStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("materialpurchaserequisitionStatus","Fail");
			}

			List<Employee> employeeList = employeeRepository.findAll();
			List<Project> projectList = GetUserAssigenedProjectList(req,res);
			String empName="";
			Query query1 = new Query();
			List<MaterialPurchaseRequisition> allItemList = mongoTemplate.find(query1.addCriteria(Criteria.where("status").is("Applied")), MaterialPurchaseRequisition.class);

			Query query = new Query();
			for(int i=0;i<allItemList.size();i++)
			{
				try
				{
					query = new Query();
					List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(allItemList.get(i).getProjectId())),Project.class);
					query =new Query();
					List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(allItemList.get(i).getBuildingId())), ProjectBuilding.class);

					query =new Query();
					List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(allItemList.get(i).getWingId())), ProjectWing.class);
					query =new Query();
					List<Employee> employeeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(allItemList.get(i).getEmployeeId())), Employee.class);
					empName=employeeDetails.get(0).getEmployeefirstName()+" "+employeeDetails.get(0).getEmployeemiddleName()+" "+employeeDetails.get(0).getEmployeelastName();

					allItemList.get(i).setProjectId(projectDetails.get(0).getProjectName());
					allItemList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
					allItemList.get(i).setWingId(wingDetails.get(0).getWingName());
					allItemList.get(i).setEmployeeId(empName);
				} 
				catch (Exception e) {
					// TODO: handle exception
				}

			}
			model.addAttribute("employeeList", employeeList);
			model.addAttribute("projectList", projectList);
			model.addAttribute("allItemList",allItemList);
			return "MaterialPurchaseRequisitionMaster";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping("/MaterialPurchaseRequisition")
	public String MaterialPurchaseRequisition(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			String code=RequisitionCode();
			Query query = new Query();
			Query query1 = new Query();
			List<MaterialPurchaseRequisitionHistory> materialList = mongoTemplate.find(query.addCriteria(Criteria.where("requisitionId").is(code)), MaterialPurchaseRequisitionHistory.class);

			if(materialList.size()!=0)
			{
				mongoTemplate.remove(query1.addCriteria(Criteria.where("requisitionId").is(code)), MaterialPurchaseRequisitionHistory.class);	
			}

			List<Employee> employeeList = employeeRepository.findAll();
			List<Project> projectList = GetUserAssigenedProjectList(req,res);
			// List<Item> itemList = itemRepository.findAll();
			List<ItemUnit> itemUnitList = itemunitRepository.findAll();
			List<SupplierType> suppliertypeList = suppliertypeRepository.findAll();
			model.addAttribute("suppliertypeList",suppliertypeList); 
			model.addAttribute("requisitionCode", RequisitionCode());
			model.addAttribute("itemUnitList", itemUnitList);
			model.addAttribute("employeeList", employeeList);
			// model.addAttribute("itemList", itemList);
			model.addAttribute("projectList", projectList);


			return "MaterialPurchaseRequisition";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/AddItemRequisition")
	public List<MaterialPurchaseRequisitionHistory> AddItemRequisition(@RequestParam("requisitionId") String requisitionId, @RequestParam("itemId") String itemId, @RequestParam("itemunitName") String itemunitName, @RequestParam("itemSize") String itemSize, @RequestParam("itemQuantity") double itemQuantity, HttpSession session)
	{
		try
		{
			MaterialPurchaseRequisitionHistory materialrequisition = new MaterialPurchaseRequisitionHistory();
			//materialrequisition.setRequisitionHistoryId(RequisitionHistoryCode());
			materialrequisition.setRequisitionId(requisitionId);
			materialrequisition.setItemId(itemId);
			materialrequisition.setItemunitName(itemunitName);
			materialrequisition.setItemSize(itemSize);
			materialrequisition.setItemQuantity(itemQuantity);

			materialpurchaserequisitionhistoryRepository.save(materialrequisition);
			Query query = new Query();
			List<MaterialPurchaseRequisitionHistory> materialpurchaserequisitionList = mongoTemplate.find(query.addCriteria(Criteria.where("requisitionId").is(requisitionId)), MaterialPurchaseRequisitionHistory.class);

			for(int i=0; i<materialpurchaserequisitionList.size();i++)
			{
				query = new Query();
				List<Item> itemDetails = mongoTemplate.find(query.addCriteria(Criteria.where("itemId").is(materialpurchaserequisitionList.get(i).getItemId())),Item.class);
				materialpurchaserequisitionList.get(i).setItemId(itemDetails.get(0).getItemName());

			}
			return materialpurchaserequisitionList;
		}
		catch(Exception e)
		{

			return null;
		}

	}

	@RequestMapping(value="/MaterialPurchaseRequisition", method=RequestMethod.POST)
	public String materialpurchaserequisitionSave(@ModelAttribute MaterialPurchaseRequisition materialpurchaserequisition, Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				materialpurchaserequisition = new MaterialPurchaseRequisition(materialpurchaserequisition.getRequisitionId(),materialpurchaserequisition.getEmployeeId(),materialpurchaserequisition.getProjectId(),materialpurchaserequisition.getBuildingId(),materialpurchaserequisition.getWingId(),materialpurchaserequisition.getRequiredDate(), materialpurchaserequisition.getTotalNoItem(),materialpurchaserequisition.getTotalQuantity(),materialpurchaserequisition.getStatus(),materialpurchaserequisition.getCreationDate(),materialpurchaserequisition.getUpdateDate(),materialpurchaserequisition.getUserName());
				materialpurchaserequisitionRepository.save(materialpurchaserequisition);
				model.addAttribute("materialpurchaserequisitionStatus","Success");

				//Send sms to customer
				try {

					Query	query1 = new Query();
					Employee employee = mongoTemplate.findOne(query1.addCriteria(Criteria.where("employeeId").is(materialpurchaserequisition.getEmployeeId())), Employee.class);

					String sms="";
					sms="Dear Sagar, "+employee.getEmployeefirstName()+" "+employee.getEmployeelastName()+" is add new Requisition.\r\n" + 
							"Pls do check.";
					SmsServices.sendSMS("9527098100",sms);
				}catch (Exception e) {
					e.printStackTrace();
					// TODO: handle exception
				}

			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("materialpurchaserequisitionStatus","Fail");
			}

			List<Employee> employeeList = employeeRepository.findAll();
			List<Project> projectList = GetUserAssigenedProjectList(req,res);
			String empName="";
			Query query1 = new Query();
			List<MaterialPurchaseRequisition> allItemList = mongoTemplate.find(query1.addCriteria(Criteria.where("status").is("Applied")), MaterialPurchaseRequisition.class);




			Query query = new Query();
			for(int i=0;i<allItemList.size();i++)
			{
				try
				{
					query = new Query();
					List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(allItemList.get(i).getProjectId())),Project.class);
					query =new Query();
					List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(allItemList.get(i).getBuildingId())), ProjectBuilding.class);

					query =new Query();
					List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(allItemList.get(i).getWingId())), ProjectWing.class);
					query =new Query();
					List<Employee> employeeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(allItemList.get(i).getEmployeeId())), Employee.class);
					empName=employeeDetails.get(0).getEmployeefirstName()+" "+employeeDetails.get(0).getEmployeemiddleName()+" "+employeeDetails.get(0).getEmployeelastName();

					allItemList.get(i).setProjectId(projectDetails.get(0).getProjectName());
					allItemList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
					allItemList.get(i).setWingId(wingDetails.get(0).getWingName());
					allItemList.get(i).setEmployeeId(empName);
				} 
				catch (Exception e) {
					// TODO: handle exception
				}

			}


			model.addAttribute("employeeList", employeeList);
			model.addAttribute("projectList", projectList);
			model.addAttribute("allItemList",allItemList);
			return "MaterialPurchaseRequisitionMaster";

		}catch (Exception e) {
			return "login";
		}
	}


	@ResponseBody
	@RequestMapping("/DeleteSelectedItem")
	public List<MaterialPurchaseRequisitionHistory> DeleteSelectedItem(@RequestParam("requisitionHistoryId") String requisitionHistoryId, @RequestParam("requisitionId") String requisitionId, HttpSession session)
	{
		try
		{
			Query query = new Query();
			mongoTemplate.remove(query.addCriteria(Criteria.where("requisitionId").is((requisitionId)).and("requisitionHistoryId").is(requisitionHistoryId)), MaterialPurchaseRequisitionHistory.class);

			Query query1 = new Query();
			List<MaterialPurchaseRequisitionHistory> materialpurchaserequisitionList = mongoTemplate.find(query1.addCriteria(Criteria.where("requisitionId").is(requisitionId)), MaterialPurchaseRequisitionHistory.class);

			for(int i=0; i<materialpurchaserequisitionList.size();i++)
			{
				query = new Query();
				List<Item> itemDetails = mongoTemplate.find(query.addCriteria(Criteria.where("itemId").is(materialpurchaserequisitionList.get(i).getItemId())),Item.class);
				materialpurchaserequisitionList.get(i).setItemId(itemDetails.get(0).getItemName());

			}
			return materialpurchaserequisitionList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping("/MaterialPurchaseRequisitionList")
	public String MaterialPurchaseRequisitionList(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<Employee> employeeList = employeeRepository.findAll();
			List<Project> projectList = GetUserAssigenedProjectList(req,res);
			String empName="";
			Query query1 = new Query();
			List<MaterialPurchaseRequisition> allItemList = mongoTemplate.find(query1.addCriteria(Criteria.where("status").is("Approved")), MaterialPurchaseRequisition.class);

			Query query = new Query();
			for(int i=0;i<allItemList.size();i++)
			{
				try
				{
					query = new Query();
					List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(allItemList.get(i).getProjectId())),Project.class);
					query =new Query();
					List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(allItemList.get(i).getBuildingId())), ProjectBuilding.class);

					query =new Query();
					List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(allItemList.get(i).getWingId())), ProjectWing.class);
					query =new Query();
					List<Employee> employeeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(allItemList.get(i).getEmployeeId())), Employee.class);
					empName=employeeDetails.get(0).getEmployeefirstName()+" "+employeeDetails.get(0).getEmployeemiddleName()+" "+employeeDetails.get(0).getEmployeelastName();

					allItemList.get(i).setProjectId(projectDetails.get(0).getProjectName());
					allItemList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
					allItemList.get(i).setWingId(wingDetails.get(0).getWingName());
					allItemList.get(i).setEmployeeId(empName);
				} 
				catch (Exception e) {
					// TODO: handle exception
				}

			}
			model.addAttribute("employeeList", employeeList);
			model.addAttribute("projectList", projectList);
			model.addAttribute("allItemList",allItemList);
			return "MaterialPurchaseRequisitionList";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping("/MaterialPurchaseRequisitionApprovalMaster")
	public String MaterialPurchaseRequisitionApprovalMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<Employee> employeeList = employeeRepository.findAll();
			List<Project> projectList = GetUserAssigenedProjectList(req,res);
			String empName="";
			Query query1 = new Query();
			List<MaterialPurchaseRequisition> allItemList = mongoTemplate.find(query1.addCriteria(Criteria.where("status").is("Applied")), MaterialPurchaseRequisition.class);

			Query query = new Query();
			for(int i=0;i<allItemList.size();i++)
			{
				try
				{
					query = new Query();
					List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(allItemList.get(i).getProjectId())),Project.class);
					query =new Query();
					List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(allItemList.get(i).getBuildingId())), ProjectBuilding.class);

					query =new Query();
					List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(allItemList.get(i).getWingId())), ProjectWing.class);
					query =new Query();
					List<Employee> employeeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(allItemList.get(i).getEmployeeId())), Employee.class);
					empName=employeeDetails.get(0).getEmployeefirstName()+" "+employeeDetails.get(0).getEmployeemiddleName()+" "+employeeDetails.get(0).getEmployeelastName();

					allItemList.get(i).setProjectId(projectDetails.get(0).getProjectName());
					allItemList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
					allItemList.get(i).setWingId(wingDetails.get(0).getWingName());
					allItemList.get(i).setEmployeeId(empName);
				} 
				catch (Exception e) {
					// TODO: handle exception
				}

			}
			model.addAttribute("employeeList", employeeList);
			model.addAttribute("projectList", projectList);
			model.addAttribute("allItemList",allItemList);
			return "MaterialPurchaseRequisitionApprovalMaster";

		}catch (Exception e) {
			return "login";
		}
	}
	

	@RequestMapping("/RequisitionApprovalStatus")
	public String RequisitionApprovalStatus(@RequestParam("requisitionId") String requisitionId, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			Query query = new Query();
			List<MaterialPurchaseRequisition> materialpurchaserequisition = mongoTemplate.find(query.addCriteria(Criteria.where("requisitionId").is(requisitionId)),MaterialPurchaseRequisition.class);

			query = new Query();
			Project projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(materialpurchaserequisition.get(0).getProjectId())),Project.class);
			query =new Query();
			ProjectBuilding buildingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(materialpurchaserequisition.get(0).getBuildingId())), ProjectBuilding.class);

			query =new Query();
			ProjectWing wingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(materialpurchaserequisition.get(0).getWingId())), ProjectWing.class);

			query =new Query();
			Employee employeeDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("employeeId").is(materialpurchaserequisition.get(0).getEmployeeId())), Employee.class);


			String empName=employeeDetails.getEmployeefirstName()+" "+employeeDetails.getEmployeemiddleName()+" "+employeeDetails.getEmployeelastName();

			materialpurchaserequisition.get(0).setEmployeeName(empName);

			query = new Query();
			List<MaterialPurchaseRequisitionHistory> materialpurchaserequisitionhistory = mongoTemplate.find(query.addCriteria(Criteria.where("requisitionId").is(requisitionId)),MaterialPurchaseRequisitionHistory.class);

			for(int i=0; i<materialpurchaserequisitionhistory.size();i++)
			{
				query = new Query();
				List<Item> itemDetails = mongoTemplate.find(query.addCriteria(Criteria.where("itemId").is(materialpurchaserequisitionhistory.get(i).getItemId())),Item.class);
				materialpurchaserequisitionhistory.get(i).setItemId(itemDetails.get(0).getItemName());

			}
			model.addAttribute("projectName",projectDetails.getProjectName()); 
			model.addAttribute("buildingName", buildingDetails.getBuildingName());
			model.addAttribute("wingName", wingDetails.getWingName());

			model.addAttribute("materialpurchaserequisition",materialpurchaserequisition);
			model.addAttribute("materialpurchaserequisitionhistory", materialpurchaserequisitionhistory);
			return "RequisitionApprovalStatus";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/RequisitionApprovalStatus",method=RequestMethod.POST)
	public String RequisitionApprovalStatus(@RequestParam("requisitionId") String requisitionId,@RequestParam("status") String status, Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				Query query = new Query();
				MaterialPurchaseRequisition materialPurchaseRequisition = mongoTemplate.findOne(query.addCriteria(Criteria.where("requisitionId").is(requisitionId)),MaterialPurchaseRequisition.class);
				materialPurchaseRequisition.setStatus(status);
				materialpurchaserequisitionRepository.save(materialPurchaseRequisition);
				model.addAttribute("materialpurchaserequisitionStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("materialpurchaserequisitionStatus","Fail");
			}

			
			
			List<Employee> employeeList = employeeRepository.findAll();
			List<Project> projectList = GetUserAssigenedProjectList(req,res);
			String empName="";
			Query query1 = new Query();
			List<MaterialPurchaseRequisition> allItemList = mongoTemplate.find(query1.addCriteria(Criteria.where("status").is("Applied")), MaterialPurchaseRequisition.class);

			Query query = new Query();
			for(int i=0;i<allItemList.size();i++)
			{
				try
				{
					query = new Query();
					List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(allItemList.get(i).getProjectId())),Project.class);
					query =new Query();
					List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(allItemList.get(i).getBuildingId())), ProjectBuilding.class);

					query =new Query();
					List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(allItemList.get(i).getWingId())), ProjectWing.class);
					query =new Query();
					List<Employee> employeeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(allItemList.get(i).getEmployeeId())), Employee.class);
					empName=employeeDetails.get(0).getEmployeefirstName()+" "+employeeDetails.get(0).getEmployeemiddleName()+" "+employeeDetails.get(0).getEmployeelastName();

					allItemList.get(i).setProjectId(projectDetails.get(0).getProjectName());
					allItemList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
					allItemList.get(i).setWingId(wingDetails.get(0).getWingName());
					allItemList.get(i).setEmployeeId(empName);
				} 
				catch (Exception e) {
					// TODO: handle exception
				}

			}
			model.addAttribute("employeeList", employeeList);
			model.addAttribute("projectList", projectList);
			model.addAttribute("allItemList",allItemList);
			return "MaterialPurchaseRequisitionApprovalMaster";

		}catch (Exception e) {
			return "login";
		}
	}

}

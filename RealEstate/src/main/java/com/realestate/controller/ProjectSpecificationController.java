package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Brand;
import com.realestate.bean.Item;
import com.realestate.bean.Login;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectSpecification;
import com.realestate.bean.SubSupplierType;
import com.realestate.bean.SupplierType;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.ProjectSpecificationRepository;
import com.realestate.repository.SubSupplierTypeRepository;
import com.realestate.repository.SupplierTypeRepository;

@Controller
@RequestMapping("/")
public class ProjectSpecificationController
{
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	SupplierTypeRepository supplierTypeRepository;
	@Autowired
	SubSupplierTypeRepository subSupplierTypeRepository;
	@Autowired
	ProjectSpecificationRepository projectspecificationRepository;

	public long ProjectSpecificationId()
	{
		long projectspecificationId=0;
		long cnt = projectspecificationRepository.count();

		if(cnt==0)
		{
			projectspecificationId = 0;
			return projectspecificationId;
		}
		else
		{
			List<ProjectSpecification> projectSpecificationDetails = projectspecificationRepository.findAll();
			projectspecificationId = cnt + (long)projectSpecificationDetails.get(projectSpecificationDetails.size()-1).getProjectspecificationId();
			return projectspecificationId;
		}
	}

	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping("/ProjectSpecificationMaster")
	public String ProjectSpecificationMaster(Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<Project> projectList = GetUserAssigenedProjectList(req,res);

			model.addAttribute("projectList", projectList);
			return "ProjectSpecificationMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/AddProjectSpecification")
	public String AddProjectSpecification(@RequestParam("projectId") String projectId, Model model)
	{
		try {
			Query query = new Query();
			List<Project> projectList = new ArrayList<Project>();
			projectList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)), Project.class);

			query = new Query(); 
			List<ProjectSpecification> projectSpecificationsList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)), ProjectSpecification.class);

			if(projectSpecificationsList.size()!=0)
			{
				for(int i=0;i<projectSpecificationsList.size();i++)
				{
					query = new Query();
					Project project = new Project();
					project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectId)), Project.class);

					projectSpecificationsList.get(i).setProjectName(project.getProjectName());

					query = new Query();
					SupplierType supplierType = new SupplierType();
					supplierType = mongoTemplate.findOne(query.addCriteria(Criteria.where("suppliertypeId").is(projectSpecificationsList.get(i).getSuppliertypeId())), SupplierType.class);
					projectSpecificationsList.get(i).setSupplierType(supplierType.getSupplierType());

					query = new Query();
					SubSupplierType subsupplierType = new SubSupplierType();
					subsupplierType = mongoTemplate.findOne(query.addCriteria(Criteria.where("subsuppliertypeId").is(projectSpecificationsList.get(i).getSubsuppliertypeId())), SubSupplierType.class);
					projectSpecificationsList.get(i).setSubsupplierType(subsupplierType.getSubsupplierType());

					query = new Query();
					Item item = new Item();
					item = mongoTemplate.findOne(query.addCriteria(Criteria.where("itemId").is(projectSpecificationsList.get(i).getItemId())), Item.class);
					projectSpecificationsList.get(i).setItemName(item.getItemName());

					query = new Query();
					Brand brand =new Brand();
					brand = mongoTemplate.findOne(query.addCriteria(Criteria.where("brandId").is(projectSpecificationsList.get(i).getBrand1())), Brand.class);
					projectSpecificationsList.get(i).setBrandName1(brand.getBrandName());

					query = new Query();
					brand =new Brand();
					brand = mongoTemplate.findOne(query.addCriteria(Criteria.where("brandId").is(projectSpecificationsList.get(i).getBrand2())), Brand.class);
					projectSpecificationsList.get(i).setBrandName2(brand.getBrandName());

					query = new Query();
					brand =new Brand();
					brand = mongoTemplate.findOne(query.addCriteria(Criteria.where("brandId").is(projectSpecificationsList.get(i).getBrand3())), Brand.class);
					projectSpecificationsList.get(i).setBrandName3(brand.getBrandName());
				}
			}

			List<SupplierType> supplierTypeList = supplierTypeRepository.findAll();

			model.addAttribute("projectList", projectList);
			model.addAttribute("supplierTypeList", supplierTypeList);
			model.addAttribute("projectSpecificationsList", projectSpecificationsList);

			return "AddProjectSpecification";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/AddMultipleSpecifications")
	public List<ProjectSpecification> AddMultipleSpecifications(@RequestParam("projectId") String projectId, @RequestParam("suppliertypeId") String suppliertypeId, @RequestParam("subsuppliertypeId") String subsuppliertypeId, @RequestParam("itemId") String itemId, @RequestParam("brand1") String brand1, @RequestParam("brand2") String brand2, @RequestParam("brand3") String brand3)
	{
		try
		{
			ProjectSpecification projectSpecification = new ProjectSpecification(ProjectSpecificationId(), projectId, suppliertypeId, subsuppliertypeId, itemId, brand1, brand2, brand3);
			projectspecificationRepository.save(projectSpecification);
		}
		catch(Exception e)
		{

		}

		Query query = new Query(); 
		List<ProjectSpecification> projectSpecificationsList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)), ProjectSpecification.class);

		if(projectSpecificationsList.size()!=0)
		{
			for(int i=0;i<projectSpecificationsList.size();i++)
			{
				query = new Query();
				Project project = new Project();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectId)), Project.class);

				projectSpecificationsList.get(i).setProjectName(project.getProjectName());

				query = new Query();
				SupplierType supplierType = new SupplierType();
				supplierType = mongoTemplate.findOne(query.addCriteria(Criteria.where("suppliertypeId").is(projectSpecificationsList.get(i).getSuppliertypeId())), SupplierType.class);
				projectSpecificationsList.get(i).setSupplierType(supplierType.getSupplierType());

				query = new Query();
				SubSupplierType subsupplierType = new SubSupplierType();
				subsupplierType = mongoTemplate.findOne(query.addCriteria(Criteria.where("subsuppliertypeId").is(projectSpecificationsList.get(i).getSubsuppliertypeId())), SubSupplierType.class);
				projectSpecificationsList.get(i).setSubsupplierType(subsupplierType.getSubsupplierType());

				query = new Query();
				Item item = new Item();
				item = mongoTemplate.findOne(query.addCriteria(Criteria.where("itemId").is(projectSpecificationsList.get(i).getItemId())), Item.class);
				projectSpecificationsList.get(i).setItemName(item.getItemName());

				query = new Query();
				Brand brand =new Brand();
				brand = mongoTemplate.findOne(query.addCriteria(Criteria.where("brandId").is(projectSpecificationsList.get(i).getBrand1())), Brand.class);
				projectSpecificationsList.get(i).setBrandName1(brand.getBrandName());

				query = new Query();
				brand =new Brand();
				brand = mongoTemplate.findOne(query.addCriteria(Criteria.where("brandId").is(projectSpecificationsList.get(i).getBrand2())), Brand.class);
				projectSpecificationsList.get(i).setBrandName2(brand.getBrandName());

				query = new Query();
				brand =new Brand();
				brand = mongoTemplate.findOne(query.addCriteria(Criteria.where("brandId").is(projectSpecificationsList.get(i).getBrand3())), Brand.class);
				projectSpecificationsList.get(i).setBrandName3(brand.getBrandName());
			}
		}

		return projectSpecificationsList;
	}

	@ResponseBody
	@RequestMapping("/DeleteProjectSpecification")
	public List<ProjectSpecification> DeleteProjectSpecification(@RequestParam("projectspecificationId") String projectspecificationId, @RequestParam("projectId") String projectId)
	{

		try
		{
			Query query = new Query();
			ProjectSpecification projectSpecification = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectspecificationId").is(Long.parseLong(projectspecificationId))), ProjectSpecification.class);
			mongoTemplate.remove(projectSpecification);

			query = new Query();
			List<ProjectSpecification> projectSpecificationsList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)), ProjectSpecification.class);

			if(projectSpecificationsList.size()!=0)
			{
				for(int i=0;i<projectSpecificationsList.size();i++)
				{
					query = new Query();
					Project project = new Project();
					project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectId)), Project.class);

					projectSpecificationsList.get(i).setProjectName(project.getProjectName());

					query = new Query();
					SupplierType supplierType = new SupplierType();
					supplierType = mongoTemplate.findOne(query.addCriteria(Criteria.where("suppliertypeId").is(projectSpecificationsList.get(i).getSuppliertypeId())), SupplierType.class);
					projectSpecificationsList.get(i).setSupplierType(supplierType.getSupplierType());

					query = new Query();
					SubSupplierType subsupplierType = new SubSupplierType();
					subsupplierType = mongoTemplate.findOne(query.addCriteria(Criteria.where("subsuppliertypeId").is(projectSpecificationsList.get(i).getSubsuppliertypeId())), SubSupplierType.class);
					projectSpecificationsList.get(i).setSubsupplierType(subsupplierType.getSubsupplierType());

					query = new Query();
					Item item = new Item();
					item = mongoTemplate.findOne(query.addCriteria(Criteria.where("itemId").is(projectSpecificationsList.get(i).getItemId())), Item.class);
					projectSpecificationsList.get(i).setItemName(item.getItemName());

					query = new Query();
					Brand brand =new Brand();
					brand = mongoTemplate.findOne(query.addCriteria(Criteria.where("brandId").is(projectSpecificationsList.get(i).getBrand1())), Brand.class);
					projectSpecificationsList.get(i).setBrandName1(brand.getBrandName());

					query = new Query();
					brand =new Brand();
					brand = mongoTemplate.findOne(query.addCriteria(Criteria.where("brandId").is(projectSpecificationsList.get(i).getBrand2())), Brand.class);
					projectSpecificationsList.get(i).setBrandName2(brand.getBrandName());

					query = new Query();
					brand =new Brand();
					brand = mongoTemplate.findOne(query.addCriteria(Criteria.where("brandId").is(projectSpecificationsList.get(i).getBrand3())), Brand.class);
					projectSpecificationsList.get(i).setBrandName3(brand.getBrandName());
				}
			}

			return projectSpecificationsList;
		}
		catch(Exception e)
		{
			return null;
		}


	}

}

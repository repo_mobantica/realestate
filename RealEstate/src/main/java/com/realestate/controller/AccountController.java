package com.realestate.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.CustomerReceiptForm;
import com.realestate.bean.Project;
import com.realestate.bean.SubChartofAccount;
import com.realestate.bean.BankTransactionHistoryDetails;
import com.realestate.bean.Booking;
import com.realestate.bean.ChartofAccount;
import com.realestate.bean.Company;
import com.realestate.bean.CompanyAccountPaymentDetails;
import com.realestate.bean.CompanyBanks;
import com.realestate.bean.CustomerOtherPaymentDetails;
import com.realestate.bean.CustomerPaymentDetails;
import com.realestate.bean.CustomerPaymentStatusHistory;
import com.realestate.repository.BookingRepository;
import com.realestate.repository.CompanyAccountPaymentDetailsRepository;
import com.realestate.repository.CustomerOtherPaymentDetailsRepository;
import com.realestate.repository.CustomerPaymentDetailsRepository;
import com.realestate.repository.CustomerPaymentStatusHistoryRepository;
import com.realestate.repository.CustomerReceiptFormRepository;
import com.realestate.repository.ChartofAccountRepository;
import com.realestate.repository.BankTransactionHistoryDetailsRepository;

@Controller
@RequestMapping("/")
public class AccountController
{
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	BookingRepository bookingRepository;
	@Autowired
	CustomerReceiptFormRepository customerReceiptFormRepository;
	@Autowired
	CustomerPaymentDetailsRepository customerPaymentDetailsRepository;
	@Autowired
	CustomerOtherPaymentDetailsRepository customerOtherPaymentDetailsRepository;
	@Autowired
	CompanyAccountPaymentDetailsRepository companyAccountPaymentDetailsRepository;
	@Autowired
	CustomerPaymentStatusHistoryRepository customerPaymentStatusHistoryRepository;
	@Autowired
	BankTransactionHistoryDetailsRepository banktransactionhistorydetailsRepository;
	@Autowired
	ChartofAccountRepository chartofaccountRepository;
	//for paymentId Generation
	String  paymentHistoryId="";
	public String GeneratePaymentHistoryId()
	{
		long cnt = customerPaymentStatusHistoryRepository.count();

		if(cnt<10)
		{
			paymentHistoryId = "PHI0000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			paymentHistoryId = "PHI000"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			paymentHistoryId = "PHI00"+(cnt+1);
		}
		else if(cnt>=1000 && cnt<10000)
		{
			paymentHistoryId = "PHI0"+(cnt+1);
		}
		else
		{
			paymentHistoryId = "PHI"+(cnt+1);
		}

		return paymentHistoryId;
	}

	String paymentId="";
	public String GeneratePaymentId()
	{

		long cnt = companyAccountPaymentDetailsRepository.count();

		if(cnt<10)
		{
			paymentId = "P000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			paymentId = "P00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			paymentId = "P0"+(cnt+1);
		}
		else
		{
			paymentId = "P"+(cnt+1);
		}

		return paymentId;
	}

	@RequestMapping("/CustomerPaymentStatusMaster")
	public String CustomerPaymentStatusMaster(Model model)
	{
		try {
			Query query = new Query();
			List<CustomerReceiptForm> customerReceiptList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("Not Deposited")), CustomerReceiptForm.class);

			List<Booking> bookingList = bookingRepository.findAll();

			for(int i=0;i<customerReceiptList.size();i++)
			{
				for(int j=0;j<bookingList.size();j++)
				{
					if(customerReceiptList.get(i).getBookingId().equals(bookingList.get(j).getBookingId()))
					{
						customerReceiptList.get(i).setCustomerName(bookingList.get(j).getBookingfirstname());
					}
				}
			}

			query = new Query();
			List<CustomerReceiptForm> customerclearpaymentList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("Deposited")), CustomerReceiptForm.class);


			for(int i=0;i<customerclearpaymentList.size();i++)
			{
				for(int j=0;j<bookingList.size();j++)
				{
					if(customerclearpaymentList.get(i).getBookingId().equals(bookingList.get(j).getBookingId()))
					{
						customerclearpaymentList.get(i).setCustomerName(bookingList.get(j).getBookingfirstname());
					}
				}
			}


			query = new Query();
			List<CustomerReceiptForm> customercancelpaymentList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("Canceled")), CustomerReceiptForm.class);


			for(int i=0;i<customercancelpaymentList.size();i++)
			{
				for(int j=0;j<bookingList.size();j++)
				{
					if(customercancelpaymentList.get(i).getBookingId().equals(bookingList.get(j).getBookingId()))
					{
						customercancelpaymentList.get(i).setCustomerName(bookingList.get(j).getBookingfirstname());
					}
				}
			}

			model.addAttribute("customercancelpaymentList", customercancelpaymentList);
			model.addAttribute("customerclearpaymentList", customerclearpaymentList);
			model.addAttribute("customerReceiptList", customerReceiptList);
			return "CustomerPaymentStatusMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/UpdatePaymentStatus")
	public String UpdatePaymentStatus(@RequestParam("receiptId") String receiptId, Model model)
	{
		try {

			List<ChartofAccount> chartofaccountList = chartofaccountRepository.findAll();
			Query query = new Query();
			List<CustomerReceiptForm> paymentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("receiptId").is(receiptId)), CustomerReceiptForm.class);

			query = new Query();
			List<Booking> bookingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(paymentDetails.get(0).getBookingId())), Booking.class);

			for(int i=0;i<paymentDetails.size();i++)
			{
				paymentDetails.get(i).setCustomerName(bookingDetails.get(0).getBookingfirstname());
			}

			query = new Query();
			List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(bookingDetails.get(0).getProjectId())), Project.class);

			query = new Query();
			List<Company> companyDetails = mongoTemplate.find(query.addCriteria(Criteria.where("companyId").is(projectDetails.get(0).getCompanyId())), Company.class);
			try
			{
				query = new Query();
				List<CompanyBanks> companyBankDetails = mongoTemplate.find(query.addCriteria(Criteria.where("companyId").is(companyDetails.get(0).getCompanyId())), CompanyBanks.class);
				model.addAttribute("companyBankDetails", companyBankDetails);
			}
			catch (Exception e) {
				// TODO: handle exception
			}
			model.addAttribute("chartofaccountList", chartofaccountList); 
			model.addAttribute("paymentDetails", paymentDetails);
			return "UpdatePaymentStatus";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/UpdatePaymentStatus", method=RequestMethod.POST)
	public String UpdatePaymentStatus_Post(@RequestParam("receiptId") String receiptId, @RequestParam("bookingId") String bookingId, @RequestParam("paymentType") String paymentType, @RequestParam("paymentAmount") Double paymentAmount, @RequestParam("paymentMode") String paymentMode, @RequestParam("bankName") String bankName,@RequestParam("chequeNumber") String refNo, @RequestParam("companyBankId") String companyBankId, @RequestParam("transactionStatus") String transactionStatus, @RequestParam("paymentStatus") String paymentStatus, @RequestParam("remark") String remark, @RequestParam("paymentDate") String paymentDate,  @RequestParam("chartaccountId") String chartaccountId, @RequestParam("subchartaccountId") String subchartaccountId, HttpServletRequest req, Model model)
	{
		//System.out.println(receiptId+"\n"+bookingId+"\n"+paymentType+"\n"+paymentAmount+"\n"+paymentMode+"\n"+paymentMode+"\n"+bankName+"\n"+branchName+"\n"+refNo+"\n"+companyBankId+"\n"+companyBankacno+"\n"+transactionStatus+"\n"+paymentStatus+"\n"+remark);
		try {
			Query query = new Query();

			try
			{
				if(paymentStatus.equals("Cleared"))
				{
					String userName = (String)req.getSession().getAttribute("user");
					BankTransactionHistoryDetails banktransactionhistorydetails=new BankTransactionHistoryDetails();

					banktransactionhistorydetails.setCompanyBankId(companyBankId);
					banktransactionhistorydetails.setAmount(paymentAmount);
					banktransactionhistorydetails.setPaymentType("Booking");
					banktransactionhistorydetails.setPaymentId(bookingId);
					banktransactionhistorydetails.setCreditOrDebiteType("Credited");
					banktransactionhistorydetails.setCreationDate(new Date());
					banktransactionhistorydetails.setUserName(userName);
					banktransactionhistorydetailsRepository.save(banktransactionhistorydetails);
				}
			}
			catch (Exception e) {
				// TODO: handle exception
			}

			if(paymentStatus.equals("Cleared"))
			{
				query = new Query();
				CustomerReceiptForm customerReceiptForm = mongoTemplate.findOne(query.addCriteria(Criteria.where("receiptId").is(receiptId)), CustomerReceiptForm.class);
				customerReceiptForm.setStatus("Deposited");

				customerReceiptFormRepository.save(customerReceiptForm);


				//save and update balance in company bank account
				try
				{
					query = new Query();
					List<CompanyAccountPaymentDetails> companyAccountPaymentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("companyBankId").is(companyBankId)), CompanyAccountPaymentDetails.class);

					if(companyAccountPaymentDetails.size()!=0)
					{
						//companyAccountPaymentDetails.get(0).setPaymentId(GeneratePaymentId());
						//companyAccountPaymentDetails.get(0).setCompanyBankId(companyBankId);

						long currentBalance = companyAccountPaymentDetails.get(0).getCurrentBalance();
						companyAccountPaymentDetails.get(0).setCurrentBalance((currentBalance+paymentAmount.longValue()));

						companyAccountPaymentDetailsRepository.save(companyAccountPaymentDetails);
					}
					else
					{
						CompanyAccountPaymentDetails companyAccountPaymentDetails1 = new CompanyAccountPaymentDetails();

						companyAccountPaymentDetails1.setPaymentId(GeneratePaymentId());
						companyAccountPaymentDetails1.setCompanyBankId(companyBankId);
						companyAccountPaymentDetails1.setCurrentBalance(paymentAmount.longValue());

						companyAccountPaymentDetailsRepository.save(companyAccountPaymentDetails1);
					}
				}
				catch(Exception e)
				{
				}

			}
			else
			{
				CustomerReceiptForm customerReceiptForm = mongoTemplate.findOne(query.addCriteria(Criteria.where("receiptId").is(receiptId)), CustomerReceiptForm.class);
				customerReceiptForm.setStatus("Canceled");

				customerReceiptFormRepository.save(customerReceiptForm);

				CustomerPaymentDetails customerPaymentDetails = new CustomerPaymentDetails();

				CustomerOtherPaymentDetails customerOtherPaymentDetails = new CustomerOtherPaymentDetails();

				if(paymentType.equals("Agreement") || paymentType.equals("Stamp Duty") || paymentType.equals("Registration") || paymentType.equals("GST"))
				{
					query = new Query();
					customerPaymentDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("BookingId").is(bookingId)), CustomerPaymentDetails.class);  
				}

				if(paymentType.equals("Maintainance") || paymentType.equals("Handling Charge") || paymentType.equals("Extra Charge"))
				{
					query = new Query();
					customerOtherPaymentDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("BookingId").is(bookingId)), CustomerOtherPaymentDetails.class);
				}


				// for customerPaymentDetails table update
				if(paymentType.equals("Agreement"))
				{
					long originalAggAmt = customerPaymentDetails.getTotalpayAggreement().longValue();
					long currentAggAmt  = paymentAmount.longValue();
					customerPaymentDetails.setTotalpayAggreement((double)originalAggAmt-currentAggAmt);

					customerPaymentDetailsRepository.save(customerPaymentDetails);
				}

				if(paymentType.equals("Stamp Duty"))
				{
					long originalStmpDutyAmt = customerPaymentDetails.getTotalpayStampDuty().longValue();
					long currentStmpDutyAmt  = paymentAmount.longValue();
					customerPaymentDetails.setTotalpayStampDuty((double)originalStmpDutyAmt-currentStmpDutyAmt);

					customerPaymentDetailsRepository.save(customerPaymentDetails);
				}

				if(paymentType.equals("Registration"))
				{
					long originalRegiAmt = customerPaymentDetails.getTotalpayRegistration().longValue();
					long currentRegiAmt  = paymentAmount.longValue();
					customerPaymentDetails.setTotalpayRegistration((double)originalRegiAmt-currentRegiAmt);

					customerPaymentDetailsRepository.save(customerPaymentDetails); 
				}

				if(paymentType.equals("GST"))
				{
					long originalGSTAmt = customerPaymentDetails.getTotalpaygstAmount().longValue();
					long currentGSTAmt  = paymentAmount.longValue();
					customerPaymentDetails.setTotalpaygstAmount((double)originalGSTAmt-currentGSTAmt);

					customerPaymentDetailsRepository.save(customerPaymentDetails);
				}

				//for CustomerOtherPaymentDetails table update
				if(paymentType.equals("Maintainance"))
				{
					long originalMaintAmt = customerOtherPaymentDetails.getTotalPaidMaintainanceAmt();
					long currentMaintAmt  = paymentAmount.longValue();
					customerOtherPaymentDetails.setTotalPaidMaintainanceAmt(originalMaintAmt-currentMaintAmt);

					customerOtherPaymentDetailsRepository.save(customerOtherPaymentDetails);
				}

				if(paymentType.equals("Handling Charge"))
				{
					long originalHandAmt = customerOtherPaymentDetails.getTotalPaidHandlingCharges();
					long currentHandAmt  = paymentAmount.longValue();
					customerOtherPaymentDetails.setTotalPaidHandlingCharges(originalHandAmt-currentHandAmt);

					customerOtherPaymentDetailsRepository.save(customerOtherPaymentDetails);
				}

				if(paymentType.equals("Extra Charge"))
				{
					long originalExtraAmt = customerOtherPaymentDetails.getTotalPaidExtraCharges();
					long currentExtraAmt  = paymentAmount.longValue();
					customerOtherPaymentDetails.setTotalPaidExtraCharges(originalExtraAmt-currentExtraAmt);

					customerOtherPaymentDetailsRepository.save(customerOtherPaymentDetails);
				}
			}

			//code to save entry in customer payment status history
			try
			{
				CustomerPaymentStatusHistory customerPaymentStatusHistory = new CustomerPaymentStatusHistory();
				customerPaymentStatusHistory.setPaymentHistoryId(GeneratePaymentHistoryId());
				customerPaymentStatusHistory.setReceiptId(receiptId);
				customerPaymentStatusHistory.setBookingId(bookingId);
				customerPaymentStatusHistory.setPaymentAmount(paymentAmount.longValue());
				customerPaymentStatusHistory.setTransactionStatus(transactionStatus);
				customerPaymentStatusHistory.setPaymentStatus(paymentStatus);
				customerPaymentStatusHistory.setPaymentDate(paymentDate);
				customerPaymentStatusHistory.setRemark(remark);
				customerPaymentStatusHistory.setChartaccountId(chartaccountId);
				customerPaymentStatusHistory.setSubchartaccountId(subchartaccountId);
				Date todateDate = new Date();
				customerPaymentStatusHistory.setCreationDate(todateDate);

				customerPaymentStatusHistoryRepository.save(customerPaymentStatusHistory);
			}
			catch(Exception e)
			{
			}


			//back on master page

			query = new Query();
			List<CustomerReceiptForm> customerReceiptList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("Not Deposited")), CustomerReceiptForm.class);

			List<Booking> bookingList = bookingRepository.findAll();

			for(int i=0;i<customerReceiptList.size();i++)
			{
				for(int j=0;j<bookingList.size();j++)
				{
					if(customerReceiptList.get(i).getBookingId().equals(bookingList.get(j).getBookingId()))
					{
						customerReceiptList.get(i).setCustomerName(bookingList.get(j).getBookingfirstname());
					}
				}
			}

			query = new Query();
			List<CustomerReceiptForm> customerclearpaymentList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("Deposited")), CustomerReceiptForm.class);


			for(int i=0;i<customerclearpaymentList.size();i++)
			{
				for(int j=0;j<bookingList.size();j++)
				{
					if(customerclearpaymentList.get(i).getBookingId().equals(bookingList.get(j).getBookingId()))
					{
						customerclearpaymentList.get(i).setCustomerName(bookingList.get(j).getBookingfirstname());
					}
				}
			}


			query = new Query();
			List<CustomerReceiptForm> customercancelpaymentList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("Canceled")), CustomerReceiptForm.class);


			for(int i=0;i<customercancelpaymentList.size();i++)
			{
				for(int j=0;j<bookingList.size();j++)
				{
					if(customercancelpaymentList.get(i).getBookingId().equals(bookingList.get(j).getBookingId()))
					{
						customercancelpaymentList.get(i).setCustomerName(bookingList.get(j).getBookingfirstname());
					}
				}
			}

			model.addAttribute("customercancelpaymentList", customercancelpaymentList);
			model.addAttribute("customerclearpaymentList", customerclearpaymentList);
			model.addAttribute("customerReceiptList", customerReceiptList);
			return "CustomerPaymentStatusMaster";
		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/getCompanyAccountBankName")
	public List<CompanyBanks> getBankAccountId(@RequestParam("companyBankId") String companyBankId, HttpSession session)
	{
		Query query = new Query();
		List<CompanyBanks> companyBankDetails = mongoTemplate.find(query.addCriteria(Criteria.where("companyBankId").is(companyBankId)), CompanyBanks.class);
		return companyBankDetails;
	}



	@ResponseBody
	@RequestMapping("/getSunChartOfAccount")
	public List<SubChartofAccount> getSunChartOfAccount(@RequestParam("chartaccountName") String chartaccountName, HttpSession session)
	{
		Query query = new Query();
		List<SubChartofAccount> subchartAccountList = mongoTemplate.find(query.addCriteria(Criteria.where("chartaccountName").is(chartaccountName)), SubChartofAccount.class);
		return subchartAccountList;
	}

}

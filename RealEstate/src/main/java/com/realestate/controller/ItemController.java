package com.realestate.controller;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Item;
import com.realestate.bean.ItemSubCategory;
import com.realestate.bean.ItemUnit;
import com.realestate.bean.SubSupplierType;
import com.realestate.bean.SupplierType;
import com.realestate.repository.ItemRepository;
import com.realestate.repository.ItemUnitRepository;
import com.realestate.repository.SupplierTypeRepository;

@Controller
@RequestMapping("/")
public class ItemController {

	@Autowired
	ItemRepository itemRepository;
	@Autowired
	SupplierTypeRepository suppliertypeRepository;
	@Autowired
	ItemUnitRepository unitcategoryRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	String itemcode;
	private String ItemCode()
	{
		long cnt  = itemRepository.count();
		if(cnt<10)
		{
			itemcode = "IT000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			itemcode = "IT00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			itemcode = "IT0"+(cnt+1);
		}
		else
		{
			itemcode = "IT"+(cnt+1);
		}
		return itemcode;
	}


	//code to reterive all item item unit
	private List<ItemUnit> findAllItemUnitName()
	{
		List<ItemUnit> itemunitList;
		itemunitList = unitcategoryRepository.findAll(new Sort(Sort.Direction.ASC,"itemunitName"));
		return itemunitList;
	}


	@RequestMapping("/AddItem")
	public String addItem(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<Item> itemList = itemRepository.findAll();
			List<SupplierType> suppliertypeList = suppliertypeRepository.findAll();
			model.addAttribute("suppliertypeList",suppliertypeList);
			model.addAttribute("itemCode",ItemCode());
			model.addAttribute("itemunitList", findAllItemUnitName());
			model.addAttribute("itemList",itemList);

			return "AddItem";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value = "/AddItem", method = RequestMethod.POST)
	public String itemSave(@ModelAttribute Item item, Model model)
	{
		try {
			try
			{
				item = new Item(item.getItemId(),item.getSuppliertypeId(),item.getSubsuppliertypeId(),item.getItemName(), item.getIntemInitialPrice(),item.getItemInitialDiscount(), item.getMinimumStock(), item.getCurrentStock(),item.getHsnCode(),item.getGstPer(),item.getItemStatus(),item.getCreationDate(), item.getUpdateDate(), item.getUserName());
				itemRepository.save(item);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}
			List<Item> itemList = itemRepository.findAll();
			List<SupplierType> suppliertypeList = suppliertypeRepository.findAll();
			model.addAttribute("suppliertypeList",suppliertypeList);
			model.addAttribute("itemCode",ItemCode());
			model.addAttribute("itemunitList", findAllItemUnitName());
			model.addAttribute("itemList",itemList);
			return "AddItem";

		}catch (Exception e) {
			return "login";
		}
	}

	//for Item Master
	@RequestMapping("/ItemMaster")
	public String ItemMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<SupplierType> suppliertypeList = suppliertypeRepository.findAll();
			model.addAttribute("suppliertypeList",suppliertypeList);
			List<Item> itemList = itemRepository.findAll();

			Query query = new Query();
			for(int i=0;i<itemList.size();i++)
			{
				try
				{
					query = new Query();
					List<SupplierType> suppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("suppliertypeId").is(itemList.get(i).getSuppliertypeId())),SupplierType.class);
					itemList.get(i).setSuppliertypeId(suppliertypeDetails.get(0).getSupplierType());
					query = new Query();
					List<SubSupplierType> subsuppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subsuppliertypeId").is(itemList.get(i).getSubsuppliertypeId())),SubSupplierType.class);
					itemList.get(i).setSubsuppliertypeId(subsuppliertypeDetails.get(0).getSubsupplierType());

				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}

			model.addAttribute("itemList",itemList);

			return "ItemMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	//to Retive All Item by category wise
	@ResponseBody
	@RequestMapping(value="/getCategoryWiseItemList",method=RequestMethod.POST)
	public List<Item> CategoryWiseItemList(@RequestParam("suppliertypeId") String suppliertypeId, HttpSession session)
	{

		List<Item> itemList= new ArrayList<Item>();

		Query query = new Query();
		itemList = mongoTemplate.find(query.addCriteria(Criteria.where("suppliertypeId").is(suppliertypeId)), Item.class);

		for(int i=0;i<itemList.size();i++)
		{
			try
			{
				query = new Query();
				List<SupplierType> suppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("suppliertypeId").is(itemList.get(i).getSuppliertypeId())),SupplierType.class);
				itemList.get(i).setSuppliertypeId(suppliertypeDetails.get(0).getSupplierType());
				query = new Query();
				List<SubSupplierType> subsuppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subsuppliertypeId").is(itemList.get(i).getSubsuppliertypeId())),SubSupplierType.class);
				itemList.get(i).setSubsuppliertypeId(subsuppliertypeDetails.get(0).getSubsupplierType());

			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}


		return itemList;
	}

	@ResponseBody
	@RequestMapping("/getallItemList")
	public List<Item> AllItemList(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		List<Item> itemList= itemRepository.findAll();

		return itemList;
	}

	@RequestMapping("/EditItem")
	public String EditItem(@RequestParam("itemId") String itemId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<Item> itemDetails = mongoTemplate.find(query.addCriteria(Criteria.where("itemId").is(itemId)),Item.class);
			List<SupplierType> suppliertypeList = suppliertypeRepository.findAll();

			query = new Query();
			List<SubSupplierType> subsuppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subsuppliertypeId").is(itemDetails.get(0).getSubsuppliertypeId())),SubSupplierType.class);

			query = new Query();
			List<SupplierType> suppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("suppliertypeId").is(itemDetails.get(0).getSuppliertypeId())),SupplierType.class);

			model.addAttribute("supplierType",suppliertypeDetails.get(0).getSupplierType());
			model.addAttribute("subsupplierType",subsuppliertypeDetails.get(0).getSubsupplierType());

			model.addAttribute("suppliertypeList",suppliertypeList);
			model.addAttribute("itemDetails",itemDetails);
			model.addAttribute("itemunitList", findAllItemUnitName());
			return "EditItem";

		}catch (Exception e) {
			return "login";
		}
	}
	@RequestMapping(value="/EditItem",method=RequestMethod.POST)
	public String EditItemPostMethod(@ModelAttribute Item item, Model model)
	{
		try {
			try
			{
				item = new Item(item.getItemId(), item.getSuppliertypeId(),item.getSubsuppliertypeId(), item.getItemName(), item.getIntemInitialPrice(),item.getItemInitialDiscount(),item.getMinimumStock(), item.getCurrentStock(),item.getHsnCode(),item.getGstPer(),item.getItemStatus(),item.getCreationDate(), item.getUpdateDate(), item.getUserName());
				itemRepository.save(item);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}

			List<Item> itemList = itemRepository.findAll();
			List<SupplierType> suppliertypeList = suppliertypeRepository.findAll();
			model.addAttribute("suppliertypeList",suppliertypeList);
			model.addAttribute("itemList",itemList);

			return "ItemMaster";

		}catch (Exception e) {
			return "login";
		}
	}


	@ResponseBody
	@RequestMapping(value="/getSubItemCategoryList",method=RequestMethod.POST)
	public List<ItemSubCategory> getSubItemCategoryList(@RequestParam("itemMainCategoryName") String itemMainCategoryName, @RequestParam("itemCategoryName") String itemCategoryName, HttpSession session)
	{

		List<ItemSubCategory> itemList= new ArrayList<ItemSubCategory>();

		Query query = new Query();
		itemList = mongoTemplate.find(query.addCriteria(Criteria.where("itemMainCategoryName").is(itemMainCategoryName).and("itemCategoryName").is(itemCategoryName)), ItemSubCategory.class);
		return itemList;
	}
	@ResponseBody
	@RequestMapping(value="/getAllItemList",method=RequestMethod.POST)
	public List<Item> getAllItemList(@RequestParam("suppliertypeId") String suppliertypeId, @RequestParam("subsuppliertypeId") String subsuppliertypeId, HttpSession session)
	{
		List<Item> itemList= new ArrayList<Item>();

		Query query = new Query();
		itemList = mongoTemplate.find(query.addCriteria(Criteria.where("suppliertypeId").is(suppliertypeId).and("subsuppliertypeId").is(subsuppliertypeId)), Item.class);
		return itemList;
	}	

	@ResponseBody
	@RequestMapping(value="/getItemDetails",method=RequestMethod.POST)
	public List<Item> getItemDetails(@RequestParam("itemMainCategoryName") String itemMainCategoryName, @RequestParam("itemCategoryName") String itemCategoryName, @RequestParam("itemName") String itemName, HttpSession session)
	{
		List<Item> itemList= new ArrayList<Item>();

		Query query = new Query();
		itemList = mongoTemplate.find(query.addCriteria(Criteria.where("itemMainCategoryName").is(itemMainCategoryName).and("itemCategoryName").is(itemCategoryName).and("itemName").is(itemName)), Item.class);
		return itemList;
	}	
}

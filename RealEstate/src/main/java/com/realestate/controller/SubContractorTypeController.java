package com.realestate.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections4.functors.ComparatorPredicate.Criterion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.repository.SubContractorTypeRepository;
import com.realestate.bean.SubContractorType;
import com.realestate.bean.ContractorEmployees;
import com.realestate.bean.ContractorType;
import com.realestate.repository.ContractorTypeRepository;

@Controller
@RequestMapping("/")
public class SubContractorTypeController {

	@Autowired
	SubContractorTypeRepository subcontractortypeRepository;
	@Autowired
	ContractorTypeRepository contractortypeRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	String subcontractortypeCode;
	private String SubContractorTypeCode()
	{
		long cnt  = subcontractortypeRepository.count();
		if(cnt<10)
		{
			subcontractortypeCode = "SC000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			subcontractortypeCode = "SC00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			subcontractortypeCode = "SC0"+(cnt+1);
		}
		else 
		{
			subcontractortypeCode = "SC"+(cnt+1);
		}
		return subcontractortypeCode;
	}

	@RequestMapping("/SubContractorTypeMaster")
	public String contractorTypeMaster(ModelMap model)
	{
		try {
			Query query = new Query();
			List<SubContractorType> subcontractortypeList = subcontractortypeRepository.findAll();
			List<ContractorType> contractortypeList = contractortypeRepository.findAll();

			for(int i=0;i<subcontractortypeList.size();i++)
			{
				try {
					query = new Query();
					List<ContractorType> contractortypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("contractortypeId").is(subcontractortypeList.get(i).getContractortypeId())),ContractorType.class);
					subcontractortypeList.get(i).setContractortypeId(contractortypeDetails.get(0).getContractorType());
				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}
			model.addAttribute("contractortypeList",contractortypeList);
			model.addAttribute("subcontractortypeList",subcontractortypeList);

			return "SubContractorTypeMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/AddSubContractorType")
	public String addSubContractorType(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<ContractorType> contractortypeList = contractortypeRepository.findAll();
			model.addAttribute("contractortypeList",contractortypeList);
			model.addAttribute("subcontractortypeCode",SubContractorTypeCode());
			return "AddSubContractorType";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping(value = "/AddSubContractorType", method = RequestMethod.POST)
	public String subcontractortypeSave(@ModelAttribute SubContractorType subcontractortype, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				subcontractortype = new SubContractorType(subcontractortype.getSubcontractortypeId(),subcontractortype.getSubcontractorType(), subcontractortype.getContractortypeId(),subcontractortype.getCreationDate(), subcontractortype.getUpdateDate(), subcontractortype.getUserName());
				subcontractortypeRepository.save(subcontractortype);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}

			List<ContractorType> contractortypeList = contractortypeRepository.findAll();
			model.addAttribute("contractortypeList",contractortypeList);
			model.addAttribute("subcontractortypeCode",SubContractorTypeCode());
			return "AddSubContractorType";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/EditSubContractorType")
	public String EditSubContractorType(@RequestParam("subcontractortypeId") String subcontractortypeId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<SubContractorType> subcontractortypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subcontractortypeId").is(subcontractortypeId)),SubContractorType.class);
			List<ContractorType> contractortypeList = contractortypeRepository.findAll();
			query = new Query();
			List<ContractorType> contractortypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("contractortypeId").is(subcontractortypeDetails.get(0).getContractortypeId())),ContractorType.class);

			model.addAttribute("contractorType",contractortypeDetails.get(0).getContractorType());
			model.addAttribute("contractortypeList",contractortypeList);
			model.addAttribute("subcontractortypeDetails",subcontractortypeDetails);
			return "EditSubContractorType";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/EditSubContractorType", method=RequestMethod.POST)
	public String EditContractorTypePostMethod(@ModelAttribute SubContractorType subcontractortype, ModelMap model)
	{
		try {
			try
			{
				subcontractortype = new SubContractorType(subcontractortype.getSubcontractortypeId(),subcontractortype.getSubcontractorType(), subcontractortype.getContractortypeId(),subcontractortype.getCreationDate(), subcontractortype.getUpdateDate(), subcontractortype.getUserName());
				subcontractortypeRepository.save(subcontractortype);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}

			List<SubContractorType> subcontractortypeList = subcontractortypeRepository.findAll();
			List<ContractorType> contractortypeList = contractortypeRepository.findAll();
			Query query = new Query();
			for(int i=0;i<subcontractortypeList.size();i++)
			{
				try {
					query = new Query();
					List<ContractorType> contractortypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("contractortypeId").is(subcontractortypeList.get(i).getContractortypeId())),ContractorType.class);
					subcontractortypeList.get(i).setContractortypeId(contractortypeDetails.get(0).getContractorType());
				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}
			model.addAttribute("contractortypeList",contractortypeList);
			model.addAttribute("subcontractortypeList",subcontractortypeList);

			return "SubContractorTypeMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/getSubContractorTypeList")
	public List<SubContractorType> getSubContractorTypeList(@RequestParam("contractortypeId") String contractortypeId)
	{
		Query query = new Query();
		List<SubContractorType> subcontractortypeList = mongoTemplate.find(query.addCriteria(Criteria.where("contractortypeId").is(contractortypeId)), SubContractorType.class);

		for(int i=0;i<subcontractortypeList.size();i++)
		{
			try
			{
				query = new Query();
				List<ContractorType> contractortypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("contractortypeId").is(subcontractortypeList.get(i).getContractortypeId())),ContractorType.class);
				subcontractortypeList.get(i).setContractortypeId(contractortypeDetails.get(0).getContractorType());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}
		return subcontractortypeList;
	}
}

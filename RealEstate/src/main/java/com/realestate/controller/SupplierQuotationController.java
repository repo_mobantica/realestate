package com.realestate.controller;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Company;
import com.realestate.bean.Flat;
import com.realestate.bean.Item;
import com.realestate.bean.MaterialPurchaseRequisition;
import com.realestate.bean.MaterialPurchaseRequisitionHistory;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.SubSupplierType;
import com.realestate.bean.Supplier;
import com.realestate.bean.SupplierQuotation;
import com.realestate.bean.SupplierQuotationHistory;
import com.realestate.bean.SupplierType;
import com.realestate.repository.ItemRepository;
import com.realestate.repository.SupplierQuotationRepository;
import com.realestate.repository.SupplierRepository;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;

import com.realestate.repository.SupplierQuotationHistoryRepository;

@Controller
@RequestMapping("/")
public class SupplierQuotationController {


	@Autowired
	ItemRepository itemRepository;
	@Autowired
	SupplierRepository supplierRepository;
	@Autowired
	SupplierQuotationRepository supplierquotationRepository;
	@Autowired
	SupplierQuotationHistoryRepository supplierquotationhistoryRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	String quotationCode;

	private String SupplierQuotationCode()
	{
		long cnt  = supplierquotationRepository.count();
		if(cnt<10)
		{
			quotationCode = "SQ000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			quotationCode = "SQ00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			quotationCode = "SQ0"+(cnt+1);
		}
		else 
		{
			quotationCode = "SQ"+(cnt+1);
		}
		return quotationCode;
	}

	long quotationHistoriId;
	private long QuotationHistoriId()
	{

		long cnt=0;
		cnt  = supplierquotationhistoryRepository.count();
		quotationHistoriId=cnt+1;
		return quotationHistoriId;
	}

	@RequestMapping("/AddSupplierQuotation")
	public String AddSupplierQuotation(@RequestParam("requisitionId") String requisitionId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<MaterialPurchaseRequisition> materialList = mongoTemplate.find(query.addCriteria(Criteria.where("requisitionId").is(requisitionId)), MaterialPurchaseRequisition.class);

			query = new Query();
			List<MaterialPurchaseRequisitionHistory> materialpurchaserequisitionhistory = mongoTemplate.find(query.addCriteria(Criteria.where("requisitionId").is(requisitionId)),MaterialPurchaseRequisitionHistory.class);

			List<MaterialPurchaseRequisitionHistory> materialpurchaserequisitionhistory1 = new ArrayList<MaterialPurchaseRequisitionHistory>();

			MaterialPurchaseRequisitionHistory material = new MaterialPurchaseRequisitionHistory();
			try
			{

				for(int i=0;i<materialpurchaserequisitionhistory.size();i++)
				{
					material = new MaterialPurchaseRequisitionHistory();
					query = new Query();
					List<Item> itemDetails = mongoTemplate.find(query.addCriteria(Criteria.where("itemId").is(materialpurchaserequisitionhistory.get(i).getItemId())),Item.class);
					material.setRequisitionHistoryId(materialpurchaserequisitionhistory.get(i).getRequisitionHistoryId());
					material.setRequisitionId(materialpurchaserequisitionhistory.get(i).getRequisitionId()); 
					material.setItemId(materialpurchaserequisitionhistory.get(i).getItemId());
					material.setItemName(itemDetails.get(0).getItemName());
					try
					{
						query =new Query();
						List<SupplierType> suppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("suppliertypeId").is(itemDetails.get(0).getSuppliertypeId())), SupplierType.class);
						material.setSuppliertypeId(suppliertypeDetails.get(0).getSupplierType());	

						query =new Query();
						List<SubSupplierType> subsuppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subsuppliertypeId").is(itemDetails.get(0).getSubsuppliertypeId())), SubSupplierType.class);
						material.setSubsuppliertypeId(subsuppliertypeDetails.get(0).getSubsupplierType());
					}
					catch (Exception e) {
						// TODO: handle exception
					}

					material.setGstPer(itemDetails.get(0).getGstPer());
					material.setItemInitialDiscount(itemDetails.get(0).getItemInitialDiscount());
					material.setItemInitialPrice(itemDetails.get(0).getIntemInitialPrice());
					material.setItemQuantity(materialpurchaserequisitionhistory.get(i).getItemQuantity());
					material.setItemSize(materialpurchaserequisitionhistory.get(i).getItemSize());
					material.setItemunitName(materialpurchaserequisitionhistory.get(i).getItemunitName());
					materialpurchaserequisitionhistory1.add(material);

				}
			}catch (Exception e) {
				System.out.println("error= = "+e);
				// TODO: handle exception
			}


			List<Supplier> supplierList = supplierRepository.findAll();

			model.addAttribute("quotationHistoriId", QuotationHistoriId());
			model.addAttribute("supplierList", supplierList);
			model.addAttribute("materialpurchaserequisitionhistory", materialpurchaserequisitionhistory1);
			model.addAttribute("materialList", materialList);
			model.addAttribute("requisitionId", requisitionId);
			model.addAttribute("quotationId", SupplierQuotationCode());

			return "AddSupplierQuotation";

		}catch (Exception e) {
			return "login";
		}
	}


	@ResponseBody
	@RequestMapping(value="/SaveSupplierQuotation",method=RequestMethod.POST)
	public List<SupplierQuotationHistory> SaveSupplierQuotation(@RequestParam("quotationHistoriId") long quotationHistoriId, @RequestParam("quotationId") String quotationId, @RequestParam("itemId") String itemId, @RequestParam("itemSize") String itemSize, @RequestParam("itemUnit") String itemUnit, @RequestParam("itemBrandName") String itemBrandName, @RequestParam("quantity") Double quantity, @RequestParam("rate") Double rate, @RequestParam("total") Double total, @RequestParam("discountPer") Double discountPer, @RequestParam("discountAmount") Double discountAmount, @RequestParam("gstPer") Double gstPer, @RequestParam("gstAmount") Double gstAmount, @RequestParam("grandTotal") Double grandTotal, HttpSession session)
	{

		SupplierQuotationHistory supplierquotation=new SupplierQuotationHistory();
		supplierquotation.setQuotationHistoriId(quotationHistoriId);
		supplierquotation.setQuotationId(quotationId);
		supplierquotation.setItemId(itemId);
		supplierquotation.setItemSize(itemSize);
		supplierquotation.setItemUnit(itemUnit);
		supplierquotation.setItemBrandName(itemBrandName);
		supplierquotation.setQuantity(quantity);
		supplierquotation.setRate(rate);
		supplierquotation.setTotal(total);
		supplierquotation.setDiscountPer(discountPer);
		supplierquotation.setDiscountAmount(discountAmount);
		supplierquotation.setGstPer(gstPer);
		supplierquotation.setGstAmount(gstAmount);
		supplierquotation.setGrandTotal(grandTotal);
		supplierquotationhistoryRepository.save(supplierquotation);
		return null;
	}



	@RequestMapping(value = "/AddSupplierQuotation", method = RequestMethod.POST)
	public String SaveAddSupplierQuotation(@ModelAttribute SupplierQuotation supplierquotation, Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				long quotationHistoriId=QuotationHistoriId();
				supplierquotation = new SupplierQuotation(supplierquotation.getQuotationId(), supplierquotation.getSupplierId(), supplierquotation.getRequisitionId(), supplierquotation.getTotal(), 
						supplierquotation.getDiscountAmount(), supplierquotation.getGstAmount(), supplierquotation.getGrandTotal(),
						supplierquotation.getCreationDate(), supplierquotation.getUpdateDate(), supplierquotation.getUserName());
				supplierquotationRepository.save(supplierquotation);



				String itemIds=req.getParameter("itemIds");
				String itemSizes=req.getParameter("itemSizes");
				String itemUnits=req.getParameter("itemUnits");
				String itemBrandNames=req.getParameter("itemBrandNames");
				String quantitys=req.getParameter("quantitys");
				String rates=req.getParameter("rates");
				String totals=req.getParameter("totals");
				String discountPers=req.getParameter("discountPers");
				String discountAmounts=req.getParameter("discountAmounts");
				String gstPers=req.getParameter("gstPers");
				String gstAmounts=req.getParameter("gstAmounts");
				String grandTotals=req.getParameter("grandTotals");

				String[] itemIdsArray = itemIds.split("###!###");
				String[] itemSizesArray = itemSizes.split("###!###");
				String[] itemUnitsArray = itemUnits.split("###!###");
				String[] itemBrandNamesArray = itemBrandNames.split("###!###");
				String[] quantitysArray = quantitys.split("###!###");
				String[] ratesArray = rates.split("###!###");
				String[] totalsArray = totals.split("###!###");
				String[] discountPersArray = discountPers.split("###!###");
				String[] discountAmountsArray = discountAmounts.split("###!###");
				String[] gstPersArray = gstPers.split("###!###");
				String[] gstAmountsArray = gstAmounts.split("###!###");
				String[] grandTotalsArray = grandTotals.split("###!###");

				for(int i=0;i<itemIdsArray.length;i++)
				{
					SupplierQuotationHistory supplierQuotationHistory=new SupplierQuotationHistory();
					supplierQuotationHistory.setQuotationHistoriId(quotationHistoriId);
					supplierQuotationHistory.setQuotationId(supplierquotation.getQuotationId());
					supplierQuotationHistory.setItemId(itemIdsArray[i]);
					supplierQuotationHistory.setItemSize(itemSizesArray[i]);
					supplierQuotationHistory.setItemUnit(itemUnitsArray[i]);
					supplierQuotationHistory.setItemBrandName(itemBrandNamesArray[i]);
					supplierQuotationHistory.setQuantity(Double.valueOf(quantitysArray[i]));
					supplierQuotationHistory.setRate(Double.valueOf(ratesArray[i]));
					supplierQuotationHistory.setTotal(Double.valueOf(totalsArray[i]));
					supplierQuotationHistory.setDiscountPer(Double.valueOf(discountPersArray[i]));
					supplierQuotationHistory.setDiscountAmount(Double.valueOf(discountAmountsArray[i]));
					supplierQuotationHistory.setGstPer(Double.valueOf(gstPersArray[i]));
					supplierQuotationHistory.setGstAmount(Double.valueOf(gstAmountsArray[i]));
					supplierQuotationHistory.setGrandTotal(Double.valueOf(grandTotalsArray[i]));
					supplierquotationhistoryRepository.save(supplierQuotationHistory);
					quotationHistoriId=quotationHistoriId+1;

				}



				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			} 

			String requisitionId=supplierquotation.getRequisitionId();


			Query query = new Query();
			List<MaterialPurchaseRequisition> materialList = mongoTemplate.find(query.addCriteria(Criteria.where("requisitionId").is(requisitionId)), MaterialPurchaseRequisition.class);

			query = new Query();
			List<MaterialPurchaseRequisitionHistory> materialpurchaserequisitionhistory = mongoTemplate.find(query.addCriteria(Criteria.where("requisitionId").is(requisitionId)),MaterialPurchaseRequisitionHistory.class);

			List<MaterialPurchaseRequisitionHistory> materialpurchaserequisitionhistory1 = new ArrayList<MaterialPurchaseRequisitionHistory>();

			MaterialPurchaseRequisitionHistory material = new MaterialPurchaseRequisitionHistory();


			for(int i=0;i<materialpurchaserequisitionhistory.size();i++)
			{
				material = new MaterialPurchaseRequisitionHistory();
				query = new Query();
				List<Item> itemDetails = mongoTemplate.find(query.addCriteria(Criteria.where("itemId").is(materialpurchaserequisitionhistory.get(i).getItemId())),Item.class);
				material.setRequisitionHistoryId(materialpurchaserequisitionhistory.get(i).getRequisitionHistoryId());
				material.setRequisitionId(materialpurchaserequisitionhistory.get(i).getRequisitionId()); 
				material.setItemId(materialpurchaserequisitionhistory.get(i).getItemId());

				material.setItemName(itemDetails.get(0).getItemName());
				try {
					query =new Query();
					List<SupplierType> suppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("suppliertypeId").is(itemDetails.get(0).getSuppliertypeId())), SupplierType.class);

					query =new Query();
					List<SubSupplierType> subsuppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subsuppliertypeId").is(itemDetails.get(0).getSubsuppliertypeId())), SubSupplierType.class);

					material.setSuppliertypeId(suppliertypeDetails.get(0).getSupplierType());
					material.setSubsuppliertypeId(subsuppliertypeDetails.get(0).getSubsupplierType());
				}
				catch (Exception e) {
					// TODO: handle exception
				}
				material.setGstPer(itemDetails.get(0).getGstPer());
				material.setItemInitialDiscount(itemDetails.get(0).getItemInitialDiscount());
				material.setItemInitialPrice(itemDetails.get(0).getIntemInitialPrice());
				material.setItemQuantity(materialpurchaserequisitionhistory.get(i).getItemQuantity());
				material.setItemSize(materialpurchaserequisitionhistory.get(i).getItemSize());
				material.setItemunitName(materialpurchaserequisitionhistory.get(i).getItemunitName());
				materialpurchaserequisitionhistory1.add(material);

			}



			List<Supplier> supplierList = supplierRepository.findAll();

			model.addAttribute("quotationHistoriId", QuotationHistoriId());
			model.addAttribute("supplierList", supplierList);
			model.addAttribute("materialpurchaserequisitionhistory", materialpurchaserequisitionhistory1);
			model.addAttribute("materialList", materialList);
			model.addAttribute("requisitionId", requisitionId);
			model.addAttribute("quotationId", SupplierQuotationCode());

			return "AddSupplierQuotation";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping("/AllSupplierQuotationList")
	public String AllSupplierQuotationList(@RequestParam("requisitionId") String requisitionId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<SupplierQuotation> supplierquotationList = mongoTemplate.find(query.addCriteria(Criteria.where("requisitionId").is(requisitionId)), SupplierQuotation.class);
			model.addAttribute("requisitionId", requisitionId);
			model.addAttribute("supplierquotationList", supplierquotationList);
			return "AllSupplierQuotationList";

		}catch (Exception e) {
			return "login";
		}
	}


	@ResponseBody
	@RequestMapping("/PrintSupplierQuotation")
	public ResponseEntity<byte[]> PrintSupplierQuotation(@RequestParam("quotationId") String quotationId, HttpServletRequest req, ModelMap model, HttpServletResponse response)
	{
		try 
		{	
			JasperPrint print;
			Query query = new Query();
			HashMap jmap = new HashMap();
			Collection c = new ArrayList();

			query = new Query();	
			List<SupplierQuotation> supplierQuotationDetails = mongoTemplate.find(query.addCriteria(Criteria.where("quotationId").is(quotationId)), SupplierQuotation.class);

			query = new Query();
			Supplier supplierDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("supplierId").is(supplierQuotationDetails.get(0).getSupplierId())), Supplier.class);

			query = new Query();
			List<SupplierQuotationHistory> supplierQuotationHistoryDetails = mongoTemplate.find(query.addCriteria(Criteria.where("quotationId").is(quotationId)), SupplierQuotationHistory.class);

			Item itemDetails = new Item();
			long subGrandTotal = 0;

			if(supplierQuotationHistoryDetails.size()!=0)
			{
				for(int i=0;i<supplierQuotationHistoryDetails.size();i++)
				{
					itemDetails = new Item();
					query = new Query();
					jmap = new HashMap();
					jmap.put("srno",""+(i+1));

					itemDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("itemId").is(supplierQuotationHistoryDetails.get(i).getItemId())), Item.class);
					supplierQuotationHistoryDetails.get(i).setItemName(itemDetails.getItemName());

					query =new Query();
					List<SupplierType> suppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("suppliertypeId").is(itemDetails.getSuppliertypeId())), SupplierType.class);

					query =new Query();
					List<SubSupplierType> subsuppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subsuppliertypeId").is(itemDetails.getSubsuppliertypeId())), SubSupplierType.class);

					supplierQuotationHistoryDetails.get(i).setSuppliertypeId(suppliertypeDetails.get(0).getSupplierType());
					supplierQuotationHistoryDetails.get(i).setSubsuppliertypeId(subsuppliertypeDetails.get(0).getSubsupplierType());

					jmap.put("itemSubType",""+subsuppliertypeDetails.get(0).getSubsupplierType());
					jmap.put("itemName",""+supplierQuotationHistoryDetails.get(i).getItemName());
					jmap.put("unit",""+supplierQuotationHistoryDetails.get(i).getItemUnit());
					jmap.put("qty",""+supplierQuotationHistoryDetails.get(i).getQuantity());
					jmap.put("size",""+supplierQuotationHistoryDetails.get(i).getItemSize());
					jmap.put("rate",""+supplierQuotationHistoryDetails.get(i).getRate());
					jmap.put("subTotal",""+supplierQuotationHistoryDetails.get(i).getTotal().longValue());
					subGrandTotal = subGrandTotal + supplierQuotationHistoryDetails.get(i).getTotal().longValue();
					jmap.put("discPer",""+supplierQuotationHistoryDetails.get(i).getDiscountPer());
					jmap.put("gstPer",""+supplierQuotationHistoryDetails.get(i).getGstPer());
					jmap.put("total",""+supplierQuotationHistoryDetails.get(i).getGrandTotal().longValue());

					c.add(jmap);
					jmap = null;
				}
			}
			else
			{
				jmap = new HashMap();
				jmap.put("srno","N.A.");
				jmap.put("itemSubType","N.A.");
				jmap.put("itemName","N.A.");
				jmap.put("unit","N.A.");
				jmap.put("qty","N.A.");
				jmap.put("size","N.A.");
				jmap.put("rate","N.A.");
				jmap.put("subTotal","N.A.");
				jmap.put("discPer","N.A.");
				jmap.put("gstPer","N.A.");
				jmap.put("total","N.A.");

				c.add(jmap);
				jmap = null; 
			}	

			//String realPath ="//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img"; //context.getRealPath("/resources/dist/img");

			JRDataSource dataSource = new JRMapCollectionDataSource(c);
			Map<String, Object> parameterMap = new HashMap<String, Object>();

			Date date = new Date();  
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
			String strDate= formatter.format(date);

			/*1*/  parameterMap.put("supplierName", ""+supplierDetails.getSupplierfirmName());
			/*2*/  parameterMap.put("printDate", ""+strDate);
			/*3*/  parameterMap.put("supplierAddress", ""+supplierDetails.getSupplierfirmAddress()+", "+supplierDetails.getSupplierPincode());

			MaterialPurchaseRequisition purchasedFor = new MaterialPurchaseRequisition();
			query = new Query();
			purchasedFor = mongoTemplate.findOne(query.addCriteria(Criteria.where("requisitionId").is(supplierQuotationDetails.get(0).getRequisitionId())), MaterialPurchaseRequisition.class);

			query = new Query();
			Project projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(purchasedFor.getProjectId())), Project.class);
			query = new Query();
			ProjectBuilding buildingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingIdId").is(purchasedFor.getBuildingId())), ProjectBuilding.class);
			query = new Query();
			ProjectWing wingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(purchasedFor.getWingId())), ProjectWing.class);

			query = new Query();
			Company companyDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("companyId").is(projectDetails.getCompanyId())), Company.class);


			/*4*/   parameterMap.put("paragraph1", "A Quotation For Material Rate Requested By "+companyDetails.getCompanyName()+" Is Given Below : ");
			/*5*/   parameterMap.put("siteAddress",""+projectDetails.getProjectName()+", "+projectDetails.getProjectAddress()+", "+projectDetails.getLocationareaId()+", "+projectDetails.getCityId()+", "+projectDetails.getStateId()+"-"+projectDetails.getAreaPincode());
			/*6*/   parameterMap.put("projectName", ""+projectDetails.getProjectName());
			/*7*/   parameterMap.put("buildingName", ""+buildingDetails.getBuildingName());
			/*8*/   parameterMap.put("wingName", ""+wingDetails.getWingName());
			/*9*/   parameterMap.put("lastSubTotal", ""+subGrandTotal);
			/*10*/  parameterMap.put("lastGrandTotal", ""+supplierQuotationDetails.get(0).getGrandTotal().longValue());
			/*11*/  parameterMap.put("totalDiscountAmt", ""+supplierQuotationDetails.get(0).getDiscountAmount().longValue());
			/*12*/  parameterMap.put("totalGSTAmt", ""+supplierQuotationDetails.get(0).getGstAmount().longValue());

			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/SupplierQuotation.jasper");

			print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(print, baos);

			byte[] contents = baos.toByteArray();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			String filename = "Supplier Quotation.pdf";

			JasperExportManager.exportReportToPdfStream(print, baos);

			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
			ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
			response.setHeader("Content-Disposition", "inline; filename=" + filename );
			return resp;	
		}

		catch(Exception ex)
		{
			return null;
		}
	}

}

package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.City;
import com.realestate.bean.Country;
import com.realestate.bean.Designation;
import com.realestate.bean.Employee;
import com.realestate.bean.LocationArea;
import com.realestate.bean.Login;
import com.realestate.bean.State;
import com.realestate.repository.LoginRepository;
import com.realestate.services.EmailService;

@Controller
@RequestMapping("/")
public class ProfileController extends EmailService
{
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	LoginRepository loginRepository;


	@RequestMapping("/profile")
	public String ProfileRediect(Model model,HttpServletRequest req,HttpServletResponse res)
	{
		try {
			HttpSession session = req.getSession(true);
			String userId = session.getAttribute("user").toString();

			Query query = new Query();
			List<Login> loginDetails = mongoTemplate.find(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);

			query = new Query();
			List<Employee> employeeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.get(0).getEmployeeId())), Employee.class);
			try 
			{
				query = new Query();
				List<Designation> designationDetails = mongoTemplate.find(query.addCriteria(Criteria.where("designationId").is(employeeDetails.get(0).getDesignationId())), Designation.class);
				model.addAttribute("designationName",designationDetails.get(0).getDesignationName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
			model.addAttribute("loginDetails",loginDetails);
			model.addAttribute("employeeDetails", employeeDetails);

			return "profile";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/getUserName")
	public List<Login> getUserName(@RequestParam("userName") String userName)
	{
		Query query = new Query();
		List<Login> loginDetails = mongoTemplate.find(query.addCriteria(Criteria.where("userName").is(userName)), Login.class);

		return loginDetails;
	}

	@ResponseBody
	@RequestMapping("/getExistingPassword")
	public List<Login> getExistingPassword(@RequestParam("OldPassword") String OldPassword,HttpServletRequest req,HttpServletResponse res)
	{
		HttpSession session = req.getSession(true);
		String userId = session.getAttribute("user").toString();

		Query query = new Query();
		List<Login> loginDetails = mongoTemplate.find(query.addCriteria(Criteria.where("userId").is(userId).and("passWord").is(OldPassword)), Login.class);

		return loginDetails;
	}

	@ResponseBody
	@RequestMapping("/CheckNewAndOldPassword")
	public List<Login> CheckNewAndOldPassword(@RequestParam("NewPassword") String NewPassword,HttpServletRequest req,HttpServletResponse res)
	{
		HttpSession session = req.getSession(true);
		String userId = session.getAttribute("user").toString();

		Query query = new Query();
		List<Login> loginDetails = mongoTemplate.find(query.addCriteria(Criteria.where("userId").is(userId).and("passWord").is(NewPassword)), Login.class);

		return loginDetails;
	}

	@ResponseBody
	@RequestMapping("/UpdateProfile")
	public List<Login> UpdateProfile(@RequestParam("userName") String userName, @RequestParam("passWord") String passWord, HttpServletRequest req, HttpServletResponse res)
	{
		List<Login> loginUpdate = new ArrayList<Login>();
		HttpSession session = req.getSession(true);
		String userId = session.getAttribute("user").toString();

		Query query = new Query();
		Login loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);

		if(loginDetails!=null)
		{
			loginDetails.setUserName(userName);
			loginDetails.setPassWord(passWord);

			loginRepository.save(loginDetails);

			try
			{
				query = new Query();
				Employee employeeDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), Employee.class);

				String Msgbody1 = "Hi, "+employeeDetails.getEmployeefirstName()+" "+employeeDetails.getEmployeelastName();
				String Msgbody2 = "\n Your account updated successfully. Login to qaerp.genieiot.in/";
				String Msgbody3 = "\n With further updated login Credentials : ";
				String Msgbody4 =  "\n Username : "+userName+"\n Password : "+passWord;

				String emilId = employeeDetails.getEmployeeEmailId();

				sendLoginCredentialsMail(emilId,"Login Credentials", Msgbody1+"\n\t"+Msgbody2+"\n"+Msgbody3+"\n"+Msgbody4+" \n\n\t Please, do not reply to the mail.");
			}
			catch(Exception e)
			{
				System.out.println("Mail Sending Exception = "+e.toString());
			}

			loginUpdate.add(loginDetails);
			return loginUpdate;
		}
		else
		{
			return loginUpdate;
		}
	}

}

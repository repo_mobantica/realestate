package com.realestate.controller;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.realestate.bean.SupplierType;
import com.realestate.repository.SupplierTypeRepository;

@Controller
@RequestMapping("/")
public class ImportNewSupplierTypeController {

	@Autowired
	ServletContext context;

	@Autowired
	SupplierTypeRepository supplierTypeRepository;
	@Autowired
	MongoTemplate mongoTemplate;
	SupplierType supplierType = new SupplierType();
	HSSFWorkbook workbook;
	XSSFWorkbook workbook1;

	HSSFSheet worksheet;
	XSSFSheet worksheet1;

	String suppliertypeCode;
	private String SupplierTypeCode()
	{
		long cnt  = supplierTypeRepository.count();
		if(cnt<10)
		{
			suppliertypeCode = "ST000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			suppliertypeCode = "ST00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			suppliertypeCode = "ST0"+(cnt+1);
		}
		else
		{
			suppliertypeCode = "ST"+(cnt+1);
		}
		return suppliertypeCode;
	}

	@RequestMapping(value="/ImportNewSupplierType")
	public String ImportNewSupplierType(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		return "ImportNewSupplierType";
	}


	@SuppressWarnings({ "deprecation"})
	@RequestMapping(value = "/ImportNewSupplierType", method = RequestMethod.POST)
	public String uploadSupplierTypeDetails(@RequestParam("supplierType_excel") MultipartFile supplierType_excel, Model model, HttpServletRequest request, RedirectAttributes redirectAttributes, HttpSession session) 
	{

		String user = (String)request.getSession().getAttribute("user");
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/M/yyyy");
		LocalDate localDate = LocalDate.now();
		String todayDate=dtf.format(localDate);

		if (!supplierType_excel.isEmpty() || supplierType_excel != null )
		{
			try 
			{
				if(supplierType_excel.getOriginalFilename().endsWith("xls") || supplierType_excel.getOriginalFilename().endsWith("xlsx") || supplierType_excel.getOriginalFilename().endsWith("csv"))
				{
					if(supplierType_excel.getOriginalFilename().endsWith("xlsx"))
					{
						InputStream stream = supplierType_excel.getInputStream();
						XSSFWorkbook workbook = new XSSFWorkbook(stream);

						XSSFSheet sheet = workbook.getSheet("Sheet1");  /// this will read 1st workbook of ExcelSheet

						int firstRow = sheet.getFirstRowNum();

						XSSFRow firstrow = sheet.getRow(firstRow);

						@SuppressWarnings("unused")
						int lastColumnCount = firstrow.getLastCellNum();

						@SuppressWarnings("unused")
						Iterator<Row> rowIterator = sheet.iterator();   
						int last_no=sheet.getLastRowNum();

						SupplierType supplierType = new SupplierType();
						for(int i=0;i<last_no;i++)
						{

							XSSFRow row = sheet.getRow(i+1);

							// Skip read heading 
							if (row.getRowNum() == 0) 
							{
								continue;
							}

							supplierType.setSuppliertypeId(SupplierTypeCode());

							row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
							supplierType.setSupplierType(row.getCell(0).getStringCellValue());

							supplierType.setCreationDate(todayDate);
							supplierType.setUpdateDate(todayDate);
							supplierType.setUserName(user);
							supplierTypeRepository.save(supplierType);
						}
						workbook.close();
					}

				}//if after inner if

			}
			catch(Exception e)
			{

			}
		}

		return "ImportNewSupplierType";
	}

	//private static final String INTERNAL_FILE="NewsupplierType.xlsx";
	@RequestMapping(value = "/DownloadSupplierTypeTemplate")
	public String downloadSupplierTypeTemplate(Model model, HttpServletResponse response, HttpServletRequest request, RedirectAttributes redirectAttributes, HttpSession session) throws IOException 
	{

		String filename = "NewSupplierType.xlsx";
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		String filepath = "//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"Template/";
		response.setContentType("APPLICATION/OCTET-STREAM");
		response.setHeader("Content-Disposition", "attachment; filename=\""+ filename + "\"");


		FileInputStream fileInputStream = new FileInputStream(filepath+filename);

		int i;
		while ((i = fileInputStream.read()) != -1) {
			out.write(i);
		}
		fileInputStream.close();
		out.close();

		return "ImportNewSupplierType";
	}
}

package com.realestate.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.EmployeeLeaveDetails;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.QueryBuilder;
import com.realestate.bean.Employee;
import com.realestate.repository.EmployeeRepository;
import com.realestate.repository.EmployeeLeaveDetailsRepository;


@Controller
@RequestMapping("/")
public class EmployeeLeaveDetailsController {

	@Autowired
	EmployeeLeaveDetailsRepository employeeleavedetailsRepository;
	@Autowired
	EmployeeRepository employeeRepository;
	@Autowired
	MongoTemplate mongoTemplate;


	@RequestMapping("/EmployeeLeaveDetails")
	public String EmployeeLeaveDetails(Model model, HttpServletRequest req, HttpServletResponse res) throws ParseException
	{
		try {
			List<Employee> empolyeeList = employeeRepository.findAll(); 

			DateFormat dateFormat = new SimpleDateFormat("d/M/yyyy");
			Calendar now = Calendar.getInstance();

			String currentStartDate1="1/"+(now.get(Calendar.MONTH) + 1)+"/"+now.get(Calendar.YEAR);

			Calendar calendar = Calendar.getInstance();
			int lastDate = calendar.getActualMaximum(Calendar.DATE);
			calendar.set(Calendar.DATE, lastDate);

			Date currentMonthStartDate=new SimpleDateFormat("d/M/yyyy").parse(currentStartDate1);
			Date currentMonthEndDate=calendar.getTime();
			Query query=new Query();


			List<EmployeeLeaveDetails> employeeLeaveList = mongoTemplate.find(query.addCriteria(Criteria.where("leaveFromDate").gte( currentMonthStartDate).lt(currentMonthEndDate)), EmployeeLeaveDetails.class);

			for(int i=0;i<employeeLeaveList.size();i++)
			{

				for(int j=0;j<empolyeeList.size();j++)
				{
					if(employeeLeaveList.get(i).getEmployeeId().equals(empolyeeList.get(j).getEmployeeId()))
					{
						employeeLeaveList.get(i).setEmployeeId(empolyeeList.get(j).getEmployeefirstName()+" "+empolyeeList.get(j).getEmployeelastName());
						break;
					}
				}

				employeeLeaveList.get(i).setFromDate(dateFormat.format(employeeLeaveList.get(i).getLeaveFromDate()));
				employeeLeaveList.get(i).setToDate(dateFormat.format(employeeLeaveList.get(i).getLeaveToDate()));
				employeeLeaveList.get(i).setCreateDate(dateFormat.format(employeeLeaveList.get(i).getCreationDate()));

			}

			model.addAttribute("empolyeeList",empolyeeList);
			model.addAttribute("employeeLeaveList",employeeLeaveList);

			return "EmployeeLeaveDetails";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping("/AddEmployeeLeave")
	public String AddEmployeeLeave(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<Employee> empolyeeList = employeeRepository.findAll(); 

			model.addAttribute("empolyeeList",empolyeeList);
			//model.addAttribute("agentCode",AgentCode());

			return "AddEmployeeLeave";

		}catch (Exception e) {
			return "login";
		}
	}



	@RequestMapping(value = "/AddEmployeeLeave", method = RequestMethod.POST)
	public String AddEmployeeLeave(@ModelAttribute EmployeeLeaveDetails employeeleavedetails,@RequestParam("leaveFromDate") String leaveFromDate, @RequestParam("leaveToDate") String leaveToDate, Model model) throws ParseException
	{
		try {
			try
			{
				Date todayDate=new Date();

				DateFormat dateFormat = new SimpleDateFormat("d/M/yyyy");
				String todayDateInString= dateFormat.format(todayDate);
				Date leaveFromDate1=new SimpleDateFormat("d/M/yyyy").parse(leaveFromDate);
				Date leaveToDate1=new SimpleDateFormat("d/M/yyyy").parse(leaveToDate);

				long diff = leaveFromDate1.getTime() - leaveToDate1.getTime();
				long totalDays = diff / (24 * 60 * 60 * 1000);
				totalDays=totalDays*(-1)+1;

				String employeeLeaveId=employeeleavedetails.getEmployeeId()+"/"+todayDateInString+"/"+totalDays;

				EmployeeLeaveDetails employeeleavedetails1=new EmployeeLeaveDetails();

				employeeleavedetails1.setEmployeeLeaveId(employeeLeaveId);
				employeeleavedetails1.setEmployeeId(employeeleavedetails.getEmployeeId());
				employeeleavedetails1.setLeaveFromDate(leaveFromDate1);
				employeeleavedetails1.setLeaveToDate(leaveToDate1);
				employeeleavedetails1.setPurposeOfLeave(employeeleavedetails.getPurposeOfLeave());
				employeeleavedetails1.setNoOfDays(totalDays);
				employeeleavedetails1.setLeaveType(employeeleavedetails.getLeaveType());
				employeeleavedetails1.setCreationDate(todayDate);
				employeeleavedetails1.setUpdateDate(todayDate);
				employeeleavedetails1.setUserName(employeeleavedetails.getUserName());

				employeeleavedetailsRepository.save(employeeleavedetails1);

				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}


			List<Employee> empolyeeList = employeeRepository.findAll(); 

			DateFormat dateFormat = new SimpleDateFormat("d/M/yyyy");
			Calendar now = Calendar.getInstance();

			String currentStartDate1="1/"+(now.get(Calendar.MONTH) + 1)+"/"+now.get(Calendar.YEAR);

			Calendar calendar = Calendar.getInstance();
			int lastDate = calendar.getActualMaximum(Calendar.DATE);
			calendar.set(Calendar.DATE, lastDate);

			Date currentMonthStartDate=new SimpleDateFormat("d/M/yyyy").parse(currentStartDate1);
			Date currentMonthEndDate=calendar.getTime();
			Query query=new Query();


			List<EmployeeLeaveDetails> employeeLeaveList = mongoTemplate.find(query.addCriteria(Criteria.where("leaveFromDate").gte( currentMonthStartDate).lt(currentMonthEndDate)), EmployeeLeaveDetails.class);

			for(int i=0;i<employeeLeaveList.size();i++)
			{

				for(int j=0;j<empolyeeList.size();j++)
				{
					if(employeeLeaveList.get(i).getEmployeeId().equals(empolyeeList.get(j).getEmployeeId()))
					{
						employeeLeaveList.get(i).setEmployeeId(empolyeeList.get(j).getEmployeefirstName()+" "+empolyeeList.get(j).getEmployeelastName());
						break;
					}
				}

				employeeLeaveList.get(i).setFromDate(dateFormat.format(employeeLeaveList.get(i).getLeaveFromDate()));
				employeeLeaveList.get(i).setToDate(dateFormat.format(employeeLeaveList.get(i).getLeaveToDate()));
				employeeLeaveList.get(i).setCreateDate(dateFormat.format(employeeLeaveList.get(i).getCreationDate()));

			}

			model.addAttribute("employeeLeaveList",employeeLeaveList);

			return "EmployeeLeaveDetails";

		}catch (Exception e) {
			return "login";
		}
	}



	@ResponseBody
	@RequestMapping(value="/getAllYearllyDetials",method=RequestMethod.POST)
	public List<EmployeeLeaveDetails> getAllYearllyDetials(@RequestParam("employeeId") String employeeId, HttpSession session) throws ParseException
	{
		List<Employee> empolyeeList = employeeRepository.findAll(); 

		DateFormat dateFormat = new SimpleDateFormat("d/M/yyyy");
		Calendar now = Calendar.getInstance();

		String currentYearStartDate1="1/"+(now.get(Calendar.MONTH) + 1)+"/"+now.get(Calendar.YEAR);
		String currentYearEndDate1="31/12/"+now.get(Calendar.YEAR);

		Date currentYearStartDate=new SimpleDateFormat("d/M/yyyy").parse(currentYearStartDate1);
		Date currentYearEndDate=new SimpleDateFormat("d/M/yyyy").parse(currentYearEndDate1);
		Query query=new Query();

		List<EmployeeLeaveDetails> employeeLeaveList = mongoTemplate.find(query.addCriteria(Criteria.where("leaveFromDate").gte( currentYearStartDate).lt(currentYearEndDate).and("employeeId").is(employeeId)), EmployeeLeaveDetails.class);

		for(int i=0;i<employeeLeaveList.size();i++)
		{
			for(int j=0;j<empolyeeList.size();j++)
			{
				if(employeeLeaveList.get(i).getEmployeeId().equals(empolyeeList.get(j).getEmployeeId()))
				{
					employeeLeaveList.get(i).setEmployeeId(empolyeeList.get(j).getEmployeefirstName()+" "+empolyeeList.get(j).getEmployeelastName());
					break;
				}
			}

			employeeLeaveList.get(i).setFromDate(dateFormat.format(employeeLeaveList.get(i).getLeaveFromDate()));
			employeeLeaveList.get(i).setToDate(dateFormat.format(employeeLeaveList.get(i).getLeaveToDate()));
			employeeLeaveList.get(i).setCreateDate(dateFormat.format(employeeLeaveList.get(i).getCreationDate()));

		}

		return employeeLeaveList;
	}


	@ResponseBody
	@RequestMapping(value="/searchDatewiseLeaveDetails",method=RequestMethod.POST)
	public List<EmployeeLeaveDetails> searchDatewiseLeaveDetails(@RequestParam("employeeId") String employeeId, @RequestParam("leaveFromDate") String leaveFromDate, @RequestParam("leaveToDate") String leaveToDate, HttpSession session) throws ParseException
	{
		List<Employee> empolyeeList = employeeRepository.findAll(); 

		DateFormat dateFormat = new SimpleDateFormat("d/M/yyyy");

		Date currentYearStartDate=new SimpleDateFormat("d/M/yyyy").parse(leaveFromDate);
		Date currentYearEndDate=new SimpleDateFormat("d/M/yyyy").parse(leaveToDate);
		Query query=new Query();
		List<EmployeeLeaveDetails> employeeLeaveList;
		if(employeeId.equalsIgnoreCase("Default"))
		{
			employeeLeaveList = mongoTemplate.find(query.addCriteria(Criteria.where("leaveFromDate").gte( currentYearStartDate).lt(currentYearEndDate)), EmployeeLeaveDetails.class);
		}
		else
		{
			employeeLeaveList = mongoTemplate.find(query.addCriteria(Criteria.where("leaveFromDate").gte( currentYearStartDate).lt(currentYearEndDate).and("employeeId").is(employeeId)), EmployeeLeaveDetails.class);

		}
		for(int i=0;i<employeeLeaveList.size();i++)
		{
			for(int j=0;j<empolyeeList.size();j++)
			{
				if(employeeLeaveList.get(i).getEmployeeId().equals(empolyeeList.get(j).getEmployeeId()))
				{
					employeeLeaveList.get(i).setEmployeeId(empolyeeList.get(j).getEmployeefirstName()+" "+empolyeeList.get(j).getEmployeelastName());
					break;
				}
			}

			employeeLeaveList.get(i).setFromDate(dateFormat.format(employeeLeaveList.get(i).getLeaveFromDate()));
			employeeLeaveList.get(i).setToDate(dateFormat.format(employeeLeaveList.get(i).getLeaveToDate()));
			employeeLeaveList.get(i).setCreateDate(dateFormat.format(employeeLeaveList.get(i).getCreationDate()));

		}

		return employeeLeaveList;
	}

}

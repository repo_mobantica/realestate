package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Agent;
import com.realestate.bean.Brand;
import com.realestate.bean.SubSupplierType;
import com.realestate.bean.SupplierType;
import com.realestate.repository.BrandRepository;
import com.realestate.repository.SupplierTypeRepository;

@Controller
@RequestMapping("/")
public class BrandController
{
	@Autowired
	BrandRepository brandRepository;
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	SupplierTypeRepository suppliertypeRepository;

	String brandCode;

	private String brandCode()
	{
		long brandCount  = brandRepository.count();
		if(brandCount<10)
		{
			brandCode = "BN000"+(brandCount+1);
		}
		else if((brandCount>=10) && (brandCount<100))
		{
			brandCode = "BN00"+(brandCount+1);
		}
		else if((brandCount>=100) && (brandCount<1000))
		{
			brandCode = "BN0"+(brandCount+1);
		}
		else
		{
			brandCode = "BN"+(brandCount+1);
		}
		return brandCode;
	}


	@RequestMapping("/BrandMaster")
	public String BrandMaster(ModelMap model)
	{
		try {
			List<Brand> brandList;
			brandList = brandRepository.findAll();
			List<SupplierType> suppliertypeList = suppliertypeRepository.findAll();
			model.addAttribute("suppliertypeList",suppliertypeList);
			model.addAttribute("brandList", brandList);

			return "BrandMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	//Request Mapping For AddBrand
	@RequestMapping("/AddBrand")
	public String addBrand(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<SupplierType> suppliertypeList = suppliertypeRepository.findAll();
			model.addAttribute("suppliertypeList",suppliertypeList);
			model.addAttribute("brandCode",brandCode());

			return "AddBrand";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping(value = "/AddBrand", method = RequestMethod.POST)
	public String brandSave(@ModelAttribute Brand brand, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				brand = new Brand(brand.getBrandId(), brand.getSuppliertypeId(),brand.getSubsuppliertypeId(), brand.getBrandName(), brand.getCreationDate(), brand.getUpdateDate(), brand.getUserName());
				brandRepository.save(brand);

				model.addAttribute("brandStatus", "Success");

			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("brandStatus", "Fail");
			}

			model.addAttribute("brandCode",brandCode());

			List<Brand> brandList;
			brandList = brandRepository.findAll();
			List<SupplierType> suppliertypeList = suppliertypeRepository.findAll();
			model.addAttribute("suppliertypeList",suppliertypeList);
			model.addAttribute("brandList", brandList);

			return "AddBrand";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/EditBrand",method=RequestMethod.GET)
	public String EditBrand(@RequestParam("brandId") String brandId,ModelMap model)
	{
		try {
			Query query = new Query();
			List<Brand> brandDetails = mongoTemplate.find(query.addCriteria(Criteria.where("brandId").is(brandId)), Brand.class);

			query = new Query();
			List<SupplierType> suppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("suppliertypeId").is(brandDetails.get(0).getSuppliertypeId())), SupplierType.class);

			query = new Query();
			List<SubSupplierType> subsuppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subsuppliertypeId").is(brandDetails.get(0).getSubsuppliertypeId())), SubSupplierType.class);


			List<SupplierType> suppliertypeList = suppliertypeRepository.findAll();
			model.addAttribute("supplierType",suppliertypeDetails.get(0).getSupplierType());
			model.addAttribute("subsupplierType",subsuppliertypeDetails.get(0).getSubsupplierType());
			model.addAttribute("suppliertypeList",suppliertypeList);
			model.addAttribute("brandDetails",brandDetails);

			return "EditBrand";

		}catch (Exception e) {
			return "login";
		}
	}
	@RequestMapping(value="/EditBrand",method=RequestMethod.POST)
	public String EditBrandPostMethod(@ModelAttribute Brand brand, ModelMap model)
	{
		try {
			try
			{
				brand = new Brand(brand.getBrandId(), brand.getSuppliertypeId(),brand.getSubsuppliertypeId(), brand.getBrandName(), brand.getCreationDate(), brand.getUpdateDate(), brand.getUserName());
				brandRepository.save(brand);

				model.addAttribute("brandStatus", "Success");

			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("brandStatus", "Fail");
			}

			List<Brand> brandList = brandRepository.findAll();

			model.addAttribute("brandList", brandList);
			return "BrandMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/getItemCategoryWiseBrandList")
	public List<Brand> getItemCategoryWiseBrandList(@RequestParam("suppliertypeId") String suppliertypeId)
	{
		Query query =new Query();
		List<Brand> brandList = mongoTemplate.find(query.addCriteria(Criteria.where("suppliertypeId").is(suppliertypeId)), Brand.class);
		return brandList;			 
	}
	@ResponseBody
	@RequestMapping("/getItemSubCategoryWiseBrandList")
	public List<Brand> getItemSubCategoryWiseBrandList(@RequestParam("suppliertypeId") String suppliertypeId, @RequestParam("subsuppliertypeId") String subsuppliertypeId)
	{
		Query query =new Query();
		List<Brand> brandList = mongoTemplate.find(query.addCriteria(Criteria.where("suppliertypeId").is(suppliertypeId).and("subsuppliertypeId").is(subsuppliertypeId)), Brand.class);
		return brandList;			 
	}

	@ResponseBody
	@RequestMapping("/getAllBrandList")
	public List<Brand> getAllBrandList(@RequestParam("suppliertypeId") String suppliertypeId, @RequestParam("subsuppliertypeId") String subsuppliertypeId)
	{
		Query query = new Query();
		List<Brand> brandList = new ArrayList<Brand>();
		brandList = mongoTemplate.find(query.addCriteria(Criteria.where("suppliertypeId").is(suppliertypeId).and("subsuppliertypeId").is(subsuppliertypeId)), Brand.class);

		return brandList;
	}

}

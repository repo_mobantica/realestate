package com.realestate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.realestate.bean.ContractorWorkList;
import com.realestate.bean.ContractorWorkListHistory;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.SubContractorType;
import com.realestate.bean.Tax;
import com.realestate.bean.Contractor;
import com.realestate.bean.ContractorQuotation;
import com.realestate.bean.ContractorType;
import com.realestate.repository.ContractorRepository;
import com.realestate.repository.ContractorTypeRepository;
import com.realestate.repository.TaxRepository;
import com.realestate.repository.ContractorQuotationRepository;

@Controller
@RequestMapping("/")
public class ContractorQuotationController {

	@Autowired
	ContractorRepository contractorRepository;
	@Autowired
	ContractorQuotationRepository contractorquotationRepository;
	@Autowired
	TaxRepository taxRepository;

	@Autowired
	ContractorTypeRepository contractortypeRepository;
	@Autowired
	MongoTemplate mongoTemplate;


	String quotationCode;

	private String ContractorQuotationCode()
	{
		long cnt  = contractorquotationRepository.count();
		if(cnt<10)
		{
			quotationCode = "CQ000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			quotationCode = "CQ00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			quotationCode = "CQ0"+(cnt+1);
		}
		else
		{
			quotationCode = "CQ"+(cnt+1);
		}
		return quotationCode;
	}

	@RequestMapping("/AddContractorQuotation")
	public String AddContractorQuotation(@RequestParam("workId") String workId, ModelMap model)
	{
		try {
			long totalAmount=0;
			Query query = new Query();
			List<ContractorWorkList> contractorworkList = mongoTemplate.find(query.addCriteria(Criteria.where("workId").is(workId)), ContractorWorkList.class);

			query = new Query();
			List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(contractorworkList.get(0).getProjectId())), Project.class);
			query = new Query();
			List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(contractorworkList.get(0).getBuildingId())), ProjectBuilding.class);
			query = new Query();
			List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(contractorworkList.get(0).getWingId())), ProjectWing.class);

			query = new Query();
			List<ContractorType> contractorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("contractortypeId").is(contractorworkList.get(0).getContractortypeId())), ContractorType.class);

			query = new Query();
			List<ContractorWorkListHistory> contractorworkhistoryList = mongoTemplate.find(query.addCriteria(Criteria.where("workId").is(workId)), ContractorWorkListHistory.class);

			for(int i=0;i<contractorworkhistoryList.size();i++)
			{
				query = new Query();
				List<SubContractorType> subcontractortypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subcontractortypeId").is(contractorworkhistoryList.get(i).getSubcontractortypeId())), SubContractorType.class);
				contractorworkhistoryList.get(i).setSubcontractortypeId(subcontractortypeDetails.get(0).getSubcontractorType());
				totalAmount=(long) (totalAmount+contractorworkhistoryList.get(i).getGrandTotal());
			}

			query= new Query();
			List<Tax> taxList = mongoTemplate.find(query.addCriteria(Criteria.where("taxType").is("CONTRACTOR")), Tax.class);
			List<Contractor> contractorList = contractorRepository.findAll();

			model.addAttribute("totalAmount", totalAmount);
			model.addAttribute("projectName", projectDetails.get(0).getProjectName());
			model.addAttribute("buildingName", buildingDetails.get(0).getBuildingName());
			model.addAttribute("wingName", wingDetails.get(0).getWingName());
			model.addAttribute("contractorType", contractorDetails.get(0).getContractorType());

			model.addAttribute("contractorworkhistoryList", contractorworkhistoryList);
			model.addAttribute("contractorList", contractorList);
			model.addAttribute("contractorworkList", contractorworkList);
			model.addAttribute("taxList", taxList);
			model.addAttribute("quotationId", ContractorQuotationCode());

			return "AddContractorQuotation";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping(value = "/AddContractorQuotation", method = RequestMethod.POST)
	public String AddContractorQuotation(@ModelAttribute ContractorQuotation contractorquotation, Model model)
	{
		try {
			try
			{
				contractorquotation = new ContractorQuotation(contractorquotation.getQuotationId(), contractorquotation.getWorkId(), 
						contractorquotation.getContractorId(), contractorquotation.getTotalAmount(), contractorquotation.getStatus(),
						contractorquotation.getCreationDate(), contractorquotation.getUpdateDate(), contractorquotation.getUserName());
				contractorquotationRepository.save(contractorquotation);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}

			Double totalAmount=0.0;
			Query query = new Query();
			List<ContractorWorkList> contractorworkList = mongoTemplate.find(query.addCriteria(Criteria.where("workId").is(contractorquotation.getWorkId())), ContractorWorkList.class);

			query = new Query();
			List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(contractorworkList.get(0).getProjectId())), Project.class);
			query = new Query();
			List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(contractorworkList.get(0).getBuildingId())), ProjectBuilding.class);
			query = new Query();
			List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(contractorworkList.get(0).getWingId())), ProjectWing.class);

			query = new Query();
			List<ContractorType> contractorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("contractortypeId").is(contractorworkList.get(0).getContractortypeId())), ContractorType.class);

			query = new Query();
			List<ContractorWorkListHistory> contractorworkhistoryList = mongoTemplate.find(query.addCriteria(Criteria.where("workId").is(contractorquotation.getWorkId())), ContractorWorkListHistory.class);

			for(int i=0;i<contractorworkhistoryList.size();i++)
			{
				query = new Query();
				List<SubContractorType> subcontractortypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subcontractortypeId").is(contractorworkhistoryList.get(i).getSubcontractortypeId())), SubContractorType.class);
				contractorworkhistoryList.get(i).setSubcontractortypeId(subcontractortypeDetails.get(0).getSubcontractorType());
				totalAmount=totalAmount+contractorworkhistoryList.get(i).getGrandTotal();
			}

			query= new Query();
			List<Tax> taxList = mongoTemplate.find(query.addCriteria(Criteria.where("taxType").is("CONTRACTOR")), Tax.class);
			List<Contractor> contractorList = contractorRepository.findAll();

			model.addAttribute("totalAmount", totalAmount);
			model.addAttribute("projectName", projectDetails.get(0).getProjectName());
			model.addAttribute("buildingName", buildingDetails.get(0).getBuildingName());
			model.addAttribute("wingName", wingDetails.get(0).getWingName());
			model.addAttribute("contractorType", contractorDetails.get(0).getContractorType());

			model.addAttribute("contractorworkhistoryList", contractorworkhistoryList);
			model.addAttribute("contractorList", contractorList);
			model.addAttribute("contractorworkList", contractorworkList);
			model.addAttribute("taxList", taxList);
			model.addAttribute("quotationId", ContractorQuotationCode());

			return "AddContractorQuotation";

		}catch (Exception e) {
			return "login";
		}
	}



	@RequestMapping("/AllContractorQuotationList")
	public String AllContractorQuotationList(@RequestParam("workId") String workId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<ContractorQuotation> contractorquotationList = mongoTemplate.find(query.addCriteria(Criteria.where("workId").is(workId)), ContractorQuotation.class);
			/*
		  List<ContractorType> contractortypeList= contractortypeRepository.findAll();
	    	for(int i=0;i<contractorquotationList.size();i++)
	    	{
	    		for(int j=0;j<contractortypeList.size();j++)
	    		{
	    			if(contractorquotationList.get(i).getWorkId().equalsIgnoreCase(contractortypeList.get(j).getContractortypeId()))
	    			{
	    				contractorquotationList.get(i).setWorkId(contractortypeList.get(j).getContractorType());
	    				break;
	    			}
	    		}

	    	}
			 */
			model.addAttribute("workId", workId);
			model.addAttribute("contractorquotationList", contractorquotationList);
			return "AllContractorQuotationList";

		}catch (Exception e) {
			return "login";
		}
	}

	/*
	     @ResponseBody
	     @RequestMapping("/PrintContractorQuotation")
	     public ResponseEntity<byte[]> PrintSupplierQuotation(@RequestParam("quotationId") String quotationId, HttpServletRequest req, ModelMap model, HttpServletResponse response)
	 	 {
	 			try 
	 			{	
	 				JasperPrint print;
	 				Query query = new Query();
	 				HashMap jmap = new HashMap();
	 				Collection c = new ArrayList();

	 				query = new Query();	
		 	        List<ContractorQuotation> contractorQuotationDetails = mongoTemplate.find(query.addCriteria(Criteria.where("quotationId").is(quotationId)), ContractorQuotation.class);

		 	        query = new Query();
		 	        Contractor contractorDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("contractorId").is(contractorQuotationDetails.get(0).getContractorId())), Contractor.class);

		 	        query = new Query();
		 	        List<ContractorWorkList> contractorWorkList = mongoTemplate.find(query.addCriteria(Criteria.where("workId").is(contractorQuotationDetails.get(0).getWorkId())), ContractorWorkList.class);

		 	        query = new Query();
		 	        ProjectWing projectWingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(contractorWorkList.get(0).getWingId())), ProjectWing.class);

		 	        query = new Query();
		 	        ProjectBuilding projectBuildingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(contractorWorkList.get(0).getBuildingId())), ProjectBuilding.class);

		 	        query = new Query();
		 	        Project projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(contractorWorkList.get(0).getProjectId())), Project.class);

		 	        query = new Query();
		 	        Company companyDetails =mongoTemplate.findOne(query.addCriteria(Criteria.where("companyId").is(projectDetails.getCompanyId())), Company.class);

		 	        query = new Query();
		 	        ContractorType contractorTypeDetails =mongoTemplate.findOne(query.addCriteria(Criteria.where("contractortypeId").is(contractorWorkList.get(0).getContractortypeId())), ContractorType.class);

		 	        query = new  Query();
		 	        SubContractorType subContractorTypeDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("subcontractortypeId").is(contractorWorkList.get(0).getSubcontractortypeId())), SubContractorType.class);

		 			jmap = new HashMap();
	 				jmap.put("srno","N.A.");
	 				jmap.put("itemSubType","N.A.");
	 				jmap.put("itemName","N.A.");
	 				jmap.put("unit","N.A.");
	 				jmap.put("qty","N.A.");
	 				jmap.put("size","N.A.");
	 				jmap.put("rate","N.A.");
	 				jmap.put("subTotal","N.A.");
	 				jmap.put("discPer","N.A.");
	 				jmap.put("gstPer","N.A.");
	 				jmap.put("total","N.A.");

	 				c.add(jmap);
	 				jmap = null; 

		 			//String realPath ="//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"resources/dist/img"; //context.getRealPath("/resources/dist/img");

		 			JRDataSource dataSource = new JRMapCollectionDataSource(c);
		 			Map<String, Object> parameterMap = new HashMap<String, Object>();

		 			Date date = new Date();  
		 			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
		 			String strDate= formatter.format(date);

		 			1   parameterMap.put("contractorName", ""+contractorDetails.getContractorfirmName());
		 			2   parameterMap.put("printDate", ""+strDate);
		 			3   parameterMap.put("contractorAddress", ""+contractorDetails.getContractorfirmAddress()+", "+contractorDetails.getLocationareaName()+", "+contractorDetails.getCityName()+", "+contractorDetails.getStateName()+"-"+contractorDetails.getContractorPincode());
		 		    4   parameterMap.put("paragraph1", "A Quotation For Work Rate Requested By "+companyDetails.getCompanyName()+" "+companyDetails.getCompanyType()+"Ltd. Is Given Below : ");
		 			5   parameterMap.put("siteAddress",""+projectDetails.getProjectName()+", "+projectDetails.getProjectAddress()+", "+projectDetails.getLocationareaName()+", "+projectDetails.getCityName()+", "+projectDetails.getStateName()+"-"+projectDetails.getAreaPincode());
		 			6   parameterMap.put("projectName", ""+projectDetails.getProjectName());
		 			7   parameterMap.put("buildingName", ""+projectBuildingDetails.getBuildingName());
		 			8   parameterMap.put("wingName", ""+projectWingDetails.getWingName());
		 			9   parameterMap.put("totalBuiltupArea", ""+contractorQuotationDetails.get(0).getTotalBuildupArea());
		 			10  parameterMap.put("workType", ""+contractorTypeDetails.getContractorType());
		 			11  parameterMap.put("subContractorType", ""+subContractorTypeDetails.getSubcontractorType());
		 			12  parameterMap.put("workDetails", ""+contractorWorkList.get(0).getWorkDetails());
		 			13  parameterMap.put("rate", ""+contractorQuotationDetails.get(0).getWorkRate());
		 			14  parameterMap.put("totalAggAmt", ""+contractorQuotationDetails.get(0).getAggTotalAmount());
		 			15  parameterMap.put("gstAmt", ""+contractorQuotationDetails.get(0).getAggGstAmount());
		 			16  parameterMap.put("totalAmt", ""+contractorQuotationDetails.get(0).getTotalAmount());
		 			17  parameterMap.put("tdsAmtAndPer", ""+contractorQuotationDetails.get(0).getAggTdsAmount());
		 			18  parameterMap.put("grandTotal", ""+contractorQuotationDetails.get(0).getAggGrandAmount());

		 				    InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/ContractorQuotation.jasper");

		 					print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

		 					ByteArrayOutputStream baos = new ByteArrayOutputStream();
		 					JasperExportManager.exportReportToPdfStream(print, baos);

		 					byte[] contents = baos.toByteArray();

		 					HttpHeaders headers = new HttpHeaders();
		 					headers.setContentType(MediaType.parseMediaType("application/pdf"));
		 					String filename = "Contractor Quotation.pdf";

		 					JasperExportManager.exportReportToPdfStream(print, baos);

		 					headers.setContentType(MediaType.parseMediaType("application/pdf"));
		 					headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		 					ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
		 					response.setHeader("Content-Disposition", "inline; filename=" + filename );
		 					return resp;	
		 		     }
				     catch(Exception ex)
	 				 {
	 					 return null;
	 				 }
	 	}
	 */
}

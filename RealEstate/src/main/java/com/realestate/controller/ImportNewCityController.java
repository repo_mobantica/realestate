package com.realestate.controller;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.realestate.bean.City;
import com.realestate.bean.Country;
import com.realestate.bean.State;
import com.realestate.repository.CityRepository;
import com.realestate.repository.CountryRepository;
import com.realestate.repository.LocationAreaRepository;
import com.realestate.repository.StateRepository;

@Controller
@RequestMapping("/")
public class ImportNewCityController {

	@Autowired
	ServletContext context;

	@Autowired
	LocationAreaRepository locationAreaRepository;
	@Autowired
	CityRepository cityRepository;
	@Autowired
	StateRepository stateRepository;
	@Autowired
	CountryRepository countryRepository;
	@Autowired
	MongoTemplate mongoTemplate;
	City city = new City();
	HSSFWorkbook workbook;
	XSSFWorkbook workbook1;

	HSSFSheet worksheet;
	XSSFSheet worksheet1;

	String cityCode;

	private String CityCode()
	{
		long cityCount  = cityRepository.count();
		if(cityCount<10)
		{
			cityCode = "CN000"+(cityCount+1);
		}
		else if((cityCount>=10) && (cityCount<100))
		{
			cityCode = "CN00"+(cityCount+1);
		}
		else if((cityCount>=100) && (cityCount<1000))
		{
			cityCode = "CN0"+(cityCount+1);
		}
		else 
		{
			cityCode = "CN"+(cityCount+1);
		}
		return cityCode;
	}

	@RequestMapping(value="/ImportNewCity")
	public String importNewCity(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		return "ImportNewCity";
	}


	@SuppressWarnings({ "deprecation"})
	@RequestMapping(value = "/ImportNewCity", method = RequestMethod.POST)
	public String uploadCityDetails(@RequestParam("city_excel") MultipartFile city_excel, Model model, HttpServletRequest request, RedirectAttributes redirectAttributes, HttpSession session) 
	{

		String user = (String)request.getSession().getAttribute("user");
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/M/yyyy");
		LocalDate localDate = LocalDate.now();
		String todayDate=dtf.format(localDate);

		List<State> stateList= stateRepository.findAll();
		List<Country> countryList= countryRepository.findAll();
		String countryId="",stateId="";

		if (!city_excel.isEmpty() || city_excel != null )
		{
			try 
			{
				if(city_excel.getOriginalFilename().endsWith("xls") || city_excel.getOriginalFilename().endsWith("xlsx") || city_excel.getOriginalFilename().endsWith("csv"))
				{
					if(city_excel.getOriginalFilename().endsWith("xlsx"))
					{
						InputStream stream = city_excel.getInputStream();
						XSSFWorkbook workbook = new XSSFWorkbook(stream);

						XSSFSheet sheet = workbook.getSheet("Sheet1");  /// this will read 1st workbook of ExcelSheet

						int firstRow = sheet.getFirstRowNum();

						XSSFRow firstrow = sheet.getRow(firstRow);

						@SuppressWarnings("unused")
						int lastColumnCount = firstrow.getLastCellNum();

						@SuppressWarnings("unused")
						Iterator<Row> rowIterator = sheet.iterator();   
						int last_no=sheet.getLastRowNum();

						City city = new City();
						for(int i=0;i<last_no;i++)
						{
							countryId="";
							stateId="";
							XSSFRow row = sheet.getRow(i+1);

							// Skip read heading 
							if (row.getRowNum() == 0) 
							{
								continue;
							}

							for(int k=0;k<countryList.size();k++)
							{
								if(countryList.get(k).getCountryName().equalsIgnoreCase(row.getCell(2).getStringCellValue()))
								{
									countryId=countryList.get(k).getCountryId();

									for(int j=0;j<stateList.size();j++)
									{
										if(stateList.get(j).getStateName().equalsIgnoreCase(row.getCell(1).getStringCellValue()))
										{
											stateId=stateList.get(j).getStateId();

											city.setCityId(CityCode());

											row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
											city.setCityName(row.getCell(0).getStringCellValue());

											city.setStateId(stateId);
											city.setCountryId(countryId);

											city.setCreationDate(todayDate);
											city.setUpdateDate(todayDate);
											city.setUserName(user);
											cityRepository.save(city);
											break;
										}
									}
									break;
								}
							}
						}
						workbook.close();
					}

				}//if after inner if

			}
			catch(Exception e)
			{

			}
		}

		return "ImportNewCity";
	}

	//private static final String INTERNAL_FILE="Newcity.xlsx";
	@RequestMapping(value = "/DownloadCityTemplate")
	public String downloadCityTemplate(Model model, HttpServletResponse response, HttpServletRequest request, RedirectAttributes redirectAttributes, HttpSession session) throws IOException 
	{

		String filename = "NewCity.xlsx";
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		String filepath = "//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"Template/";
		response.setContentType("APPLICATION/OCTET-STREAM");
		response.setHeader("Content-Disposition", "attachment; filename=\""+ filename + "\"");


		FileInputStream fileInputStream = new FileInputStream(filepath+filename);

		int i;
		while ((i = fileInputStream.read()) != -1) {
			out.write(i);
		}
		fileInputStream.close();
		out.close();

		return "ImportNewCity";
	}
}

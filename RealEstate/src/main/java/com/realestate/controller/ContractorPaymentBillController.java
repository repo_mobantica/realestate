package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.ContractorPaidPayment;
import com.realestate.bean.ContractorPaymentSchedule;
import com.realestate.bean.ContractorWorkOrder;
import com.realestate.bean.Login;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.ContractorPaymentScheduleRepository;
import com.realestate.repository.ContractorRepository;
import com.realestate.repository.ContractorTypeRepository;
import com.realestate.repository.ContractorWorkOrderRepository;
import com.realestate.repository.ProjectRepository;

@Controller
@RequestMapping("/")
public class ContractorPaymentBillController {

	@Autowired
	ContractorRepository contractorRepository;
	@Autowired
	ContractorPaymentScheduleRepository contractorpaymentscheduleRepository;
	@Autowired
	ContractorTypeRepository contractortypeRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	ContractorWorkOrderRepository contractorworkorderRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping("/ContractorPaymentBill")
	public String ContractorPaymentBill(Model model, HttpServletRequest req, HttpServletResponse res) {

		try {
		List<Project> projectList = GetUserAssigenedProjectList(req, res);

		Query query = new Query();
		List<ContractorWorkOrder> contractorworkorder = mongoTemplate
				.find(query.addCriteria(Criteria.where("paymentStatus").ne("Clear")), ContractorWorkOrder.class);

		List<ContractorWorkOrder> contractorworkorderDetails = new ArrayList<ContractorWorkOrder>();

		long totalPaidAmount = 0;
		query = new Query();

		ContractorWorkOrder contractorwork = new ContractorWorkOrder();
		Double workAmount = 0.0;
		for (int i = 0; i < contractorworkorder.size(); i++) {
			try {

				contractorwork = new ContractorWorkOrder();
				workAmount = 0.0;
				query = new Query();
				List<ContractorPaymentSchedule> contractorpaymentSchedule = mongoTemplate.find(
						query.addCriteria(Criteria.where("workOrderId").is(contractorworkorder.get(i).getWorkOrderId())
								.and("status").is("Completed")),
						ContractorPaymentSchedule.class);
				for (int j = 0; j < contractorpaymentSchedule.size(); j++) {
					workAmount = workAmount + contractorpaymentSchedule.get(j).getGrandAmount();
				}

				totalPaidAmount = 0;
				query = new Query();
				List<ContractorPaidPayment> ContractorPaidpaymentList = mongoTemplate.find(
						query.addCriteria(Criteria.where("workOrderId").is(contractorworkorder.get(i).getWorkOrderId())
								.and("contractorId").is(contractorworkorder.get(i).getContractorId())),
						ContractorPaidPayment.class);

				if (ContractorPaidpaymentList.size() != 0) {
					totalPaidAmount = ContractorPaidpaymentList.get(0).getPaidAmount();
				}
				contractorwork.setWorkOrderId(contractorworkorder.get(i).getWorkOrderId());
				contractorwork.setContractorId(contractorworkorder.get(i).getContractorId());
				contractorwork.setContractorfirmName(contractorworkorder.get(i).getContractorfirmName());
				try {
					query = new Query();
					List<Project> projectDetails = mongoTemplate.find(
							query.addCriteria(
									Criteria.where("projectId").is(contractorworkorder.get(i).getProjectId())),
							Project.class);
					query = new Query();
					List<ProjectBuilding> buildingDetails = mongoTemplate.find(
							query.addCriteria(
									Criteria.where("buildingId").is(contractorworkorder.get(i).getBuildingId())),
							ProjectBuilding.class);
					query = new Query();
					List<ProjectWing> wingDetails = mongoTemplate.find(
							query.addCriteria(Criteria.where("wingId").is(contractorworkorder.get(i).getWingId())),
							ProjectWing.class);

					contractorwork.setProjectId(projectDetails.get(0).getProjectName());
					contractorwork.setBuildingId(buildingDetails.get(0).getBuildingName());
					contractorwork.setWingId(wingDetails.get(0).getWingName());
				} catch (Exception e) {
					// TODO: handle exception
				}
				contractorwork.setTotalAmount(contractorworkorder.get(i).getTotalAmount());
				contractorwork.setWorkAmount(workAmount);

				contractorwork.setTotalPaidAmount(totalPaidAmount);
				contractorworkorderDetails.add(contractorwork);

			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		model.addAttribute("projectList", projectList);
		model.addAttribute("contractorworkorderDetails", contractorworkorderDetails);
		return "ContractorPaymentBill";
		
		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/getProjectwiseContractorPaymentList")
	public List<ContractorWorkOrder> getProjectwiseContractorPaymentList(@RequestParam("projectId") String projectId,
			HttpSession session) {
		try {

			Query query = new Query();
			List<ContractorWorkOrder> contractorworkorder = mongoTemplate.find(
					query.addCriteria(Criteria.where("paymentStatus").ne("Clear").and("projectId").is(projectId)),
					ContractorWorkOrder.class);

			List<ContractorWorkOrder> contractorworkorderDetails = new ArrayList<ContractorWorkOrder>();

			long totalPaidAmount = 0;
			query = new Query();

			ContractorWorkOrder contractorwork = new ContractorWorkOrder();
			Double workAmount = 0.0;
			for (int i = 0; i < contractorworkorder.size(); i++) {
				try {

					contractorwork = new ContractorWorkOrder();
					workAmount = 0.0;
					query = new Query();
					List<ContractorPaymentSchedule> contractorpaymentSchedule = mongoTemplate.find(
							query.addCriteria(Criteria.where("workOrderId")
									.is(contractorworkorder.get(i).getWorkOrderId()).and("status").is("Completed")),
							ContractorPaymentSchedule.class);
					for (int j = 0; j < contractorpaymentSchedule.size(); j++) {
						workAmount = workAmount + contractorpaymentSchedule.get(j).getGrandAmount();
					}

					totalPaidAmount = 0;
					query = new Query();
					List<ContractorPaidPayment> ContractorPaidpaymentList = mongoTemplate.find(
							query.addCriteria(
									Criteria.where("workOrderId").is(contractorworkorder.get(i).getWorkOrderId())
									.and("contractorId").is(contractorworkorder.get(i).getContractorId())),
							ContractorPaidPayment.class);

					if (ContractorPaidpaymentList.size() != 0) {
						totalPaidAmount = ContractorPaidpaymentList.get(0).getPaidAmount();
					}
					contractorwork.setWorkOrderId(contractorworkorder.get(i).getWorkOrderId());
					contractorwork.setContractorId(contractorworkorder.get(i).getContractorId());
					contractorwork.setContractorfirmName(contractorworkorder.get(i).getContractorfirmName());
					try {
						query = new Query();
						List<Project> projectDetails = mongoTemplate.find(
								query.addCriteria(
										Criteria.where("projectId").is(contractorworkorder.get(i).getProjectId())),
								Project.class);
						query = new Query();
						List<ProjectBuilding> buildingDetails = mongoTemplate.find(
								query.addCriteria(
										Criteria.where("buildingId").is(contractorworkorder.get(i).getBuildingId())),
								ProjectBuilding.class);
						query = new Query();
						List<ProjectWing> wingDetails = mongoTemplate.find(
								query.addCriteria(Criteria.where("wingId").is(contractorworkorder.get(i).getWingId())),
								ProjectWing.class);

						contractorwork.setProjectId(projectDetails.get(0).getProjectName());
						contractorwork.setBuildingId(buildingDetails.get(0).getBuildingName());
						contractorwork.setWingId(wingDetails.get(0).getWingName());
					} catch (Exception e) {
						// TODO: handle exception
					}
					contractorwork.setTotalAmount(contractorworkorder.get(i).getTotalAmount());
					contractorwork.setWorkAmount(workAmount);

					contractorwork.setTotalPaidAmount(totalPaidAmount);
					contractorworkorderDetails.add(contractorwork);

				} catch (Exception e) {
					// TODO: handle exception
				}
			}

			return contractorworkorderDetails;
		} catch (Exception e) {
			return null;
		}

	}

	@ResponseBody
	@RequestMapping("/getBuildingwiseWingContractorPaymentList")
	public List<ContractorWorkOrder> getBuildingwiseWingContractorWorkOrderList(
			@RequestParam("buildingId") String buildingId, @RequestParam("projectId") String projectId,
			HttpSession session) {
		try {

			Query query = new Query();
			List<ContractorWorkOrder> contractorworkorder = mongoTemplate
					.find(query.addCriteria(Criteria.where("paymentStatus").ne("Clear").and("projectId").is(projectId)
							.and("buildingId").is(buildingId)), ContractorWorkOrder.class);

			List<ContractorWorkOrder> contractorworkorderDetails = new ArrayList<ContractorWorkOrder>();

			long totalPaidAmount = 0;
			query = new Query();

			ContractorWorkOrder contractorwork = new ContractorWorkOrder();
			Double workAmount = 0.0;
			for (int i = 0; i < contractorworkorder.size(); i++) {
				try {

					contractorwork = new ContractorWorkOrder();
					workAmount = 0.0;
					query = new Query();
					List<ContractorPaymentSchedule> contractorpaymentSchedule = mongoTemplate.find(
							query.addCriteria(Criteria.where("workOrderId")
									.is(contractorworkorder.get(i).getWorkOrderId()).and("status").is("Completed")),
							ContractorPaymentSchedule.class);
					for (int j = 0; j < contractorpaymentSchedule.size(); j++) {
						workAmount = workAmount + contractorpaymentSchedule.get(j).getGrandAmount();
					}

					totalPaidAmount = 0;
					query = new Query();
					List<ContractorPaidPayment> ContractorPaidpaymentList = mongoTemplate.find(
							query.addCriteria(
									Criteria.where("workOrderId").is(contractorworkorder.get(i).getWorkOrderId())
									.and("contractorId").is(contractorworkorder.get(i).getContractorId())),
							ContractorPaidPayment.class);

					if (ContractorPaidpaymentList.size() != 0) {
						totalPaidAmount = ContractorPaidpaymentList.get(0).getPaidAmount();
					}
					contractorwork.setWorkOrderId(contractorworkorder.get(i).getWorkOrderId());
					contractorwork.setContractorId(contractorworkorder.get(i).getContractorId());
					contractorwork.setContractorfirmName(contractorworkorder.get(i).getContractorfirmName());
					try {
						query = new Query();
						List<Project> projectDetails = mongoTemplate.find(
								query.addCriteria(
										Criteria.where("projectId").is(contractorworkorder.get(i).getProjectId())),
								Project.class);
						query = new Query();
						List<ProjectBuilding> buildingDetails = mongoTemplate.find(
								query.addCriteria(
										Criteria.where("buildingId").is(contractorworkorder.get(i).getBuildingId())),
								ProjectBuilding.class);
						query = new Query();
						List<ProjectWing> wingDetails = mongoTemplate.find(
								query.addCriteria(Criteria.where("wingId").is(contractorworkorder.get(i).getWingId())),
								ProjectWing.class);

						contractorwork.setProjectId(projectDetails.get(0).getProjectName());
						contractorwork.setBuildingId(buildingDetails.get(0).getBuildingName());
						contractorwork.setWingId(wingDetails.get(0).getWingName());
					} catch (Exception e) {
						// TODO: handle exception
					}
					contractorwork.setTotalAmount(contractorworkorder.get(i).getTotalAmount());
					contractorwork.setWorkAmount(workAmount);

					contractorwork.setTotalPaidAmount(totalPaidAmount);
					contractorworkorderDetails.add(contractorwork);

				} catch (Exception e) {
					// TODO: handle exception
				}
			}

			return contractorworkorderDetails;
		} catch (Exception e) {
			return null;
		}

	}

	@ResponseBody
	@RequestMapping("/getWingwiseWingContractorPaymentList")
	public List<ContractorWorkOrder> getWingwiseWingContractorWorkOrderList(@RequestParam("wingId") String wingId,
			@RequestParam("buildingId") String buildingId, @RequestParam("projectId") String projectId,
			HttpSession session) {
		try {

			Query query = new Query();
			List<ContractorWorkOrder> contractorworkorder = mongoTemplate
					.find(query.addCriteria(Criteria.where("paymentStatus").ne("Clear").and("projectId").is(projectId)
							.and("buildingId").is(buildingId).and("wingId").is(wingId)), ContractorWorkOrder.class);

			List<ContractorWorkOrder> contractorworkorderDetails = new ArrayList<ContractorWorkOrder>();

			long totalPaidAmount = 0;
			query = new Query();

			ContractorWorkOrder contractorwork = new ContractorWorkOrder();
			Double workAmount = 0.0;
			for (int i = 0; i < contractorworkorder.size(); i++) {
				try {

					contractorwork = new ContractorWorkOrder();
					workAmount = 0.0;
					query = new Query();
					List<ContractorPaymentSchedule> contractorpaymentSchedule = mongoTemplate.find(
							query.addCriteria(Criteria.where("workOrderId")
									.is(contractorworkorder.get(i).getWorkOrderId()).and("status").is("Completed")),
							ContractorPaymentSchedule.class);
					for (int j = 0; j < contractorpaymentSchedule.size(); j++) {
						workAmount = workAmount + contractorpaymentSchedule.get(j).getGrandAmount();
					}

					totalPaidAmount = 0;
					query = new Query();
					List<ContractorPaidPayment> ContractorPaidpaymentList = mongoTemplate.find(
							query.addCriteria(
									Criteria.where("workOrderId").is(contractorworkorder.get(i).getWorkOrderId())
									.and("contractorId").is(contractorworkorder.get(i).getContractorId())),
							ContractorPaidPayment.class);

					if (ContractorPaidpaymentList.size() != 0) {
						totalPaidAmount = ContractorPaidpaymentList.get(0).getPaidAmount();
					}
					contractorwork.setWorkOrderId(contractorworkorder.get(i).getWorkOrderId());
					contractorwork.setContractorId(contractorworkorder.get(i).getContractorId());
					contractorwork.setContractorfirmName(contractorworkorder.get(i).getContractorfirmName());
					try {
						query = new Query();
						List<Project> projectDetails = mongoTemplate.find(
								query.addCriteria(
										Criteria.where("projectId").is(contractorworkorder.get(i).getProjectId())),
								Project.class);
						query = new Query();
						List<ProjectBuilding> buildingDetails = mongoTemplate.find(
								query.addCriteria(
										Criteria.where("buildingId").is(contractorworkorder.get(i).getBuildingId())),
								ProjectBuilding.class);
						query = new Query();
						List<ProjectWing> wingDetails = mongoTemplate.find(
								query.addCriteria(Criteria.where("wingId").is(contractorworkorder.get(i).getWingId())),
								ProjectWing.class);

						contractorwork.setProjectId(projectDetails.get(0).getProjectName());
						contractorwork.setBuildingId(buildingDetails.get(0).getBuildingName());
						contractorwork.setWingId(wingDetails.get(0).getWingName());
					} catch (Exception e) {
						// TODO: handle exception
					}
					contractorwork.setTotalAmount(contractorworkorder.get(i).getTotalAmount());
					contractorwork.setWorkAmount(workAmount);

					contractorwork.setTotalPaidAmount(totalPaidAmount);
					contractorworkorderDetails.add(contractorwork);

				} catch (Exception e) {
					// TODO: handle exception
				}
			}

			return contractorworkorderDetails;
		} catch (Exception e) {
			return null;
		}

	}

}

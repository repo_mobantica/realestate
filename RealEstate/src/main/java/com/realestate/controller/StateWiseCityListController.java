package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.City;
import com.realestate.bean.Country;
import com.realestate.bean.State;
import com.realestate.repository.CityRepository;
import com.realestate.repository.CountryRepository;
import com.realestate.repository.StateRepository;

@Controller
@RequestMapping("/")
public class StateWiseCityListController
{
	@Autowired
	CityRepository cityRepository;
	@Autowired
	StateRepository stateRepository;
	@Autowired
	CountryRepository countryRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	@ResponseBody
	@RequestMapping(value="/getCityList",method=RequestMethod.POST)
	public List<City> CityList(@RequestParam("stateId") String stateId, @RequestParam("countryId") String countryId, HttpSession session)
	{
		List<City> cityList= new ArrayList<City>();

		Query query = new Query();
		cityList = mongoTemplate.find(query.addCriteria(Criteria.where("stateId").is(stateId).and("countryId").is(countryId)), City.class);

		List<State> stateList= stateRepository.findAll();
		List<Country> countryList= countryRepository.findAll();

		int i,j;
		for(i=0;i<cityList.size();i++)
		{
			try
			{
				for(j=0;j<countryList.size();j++)
				{
					if(cityList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
					{
						cityList.get(i).setCountryId(countryList.get(j).getCountryName());
						break;
					}
				}

				for(j=0;j<stateList.size();j++)
				{
					if(cityList.get(i).getStateId().equals(stateList.get(j).getStateId()))
					{
						cityList.get(i).setStateId(stateList.get(j).getStateName());
						break;
					}
				}

			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		return cityList;
	}


}

package com.realestate.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.City;
import com.realestate.bean.Company;
import com.realestate.bean.Country;
import com.realestate.bean.Flat;
import com.realestate.bean.LocationArea;
import com.realestate.bean.Login;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectMarketRate;
import com.realestate.bean.State;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.CityRepository;
import com.realestate.repository.CompanyRepository;
import com.realestate.repository.CountryRepository;
import com.realestate.repository.FlatRepository;
import com.realestate.repository.LocationAreaRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.ProjectMarketRateRepository;
import com.realestate.repository.StateRepository;

@Controller
@RequestMapping("/")
public class ProjectController 
{

	@Autowired
	LocationAreaRepository locationAreaRepository;
	@Autowired
	ProjectMarketRateRepository projectmarketrateRepository;
	@Autowired
	CityRepository cityRepository;
	@Autowired
	StateRepository stateRepository;
	@Autowired
	CountryRepository countryRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	CompanyRepository companyRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	FlatRepository flatRepository;
	//Code for generating project code
	String projectCode;
	private String ProjectCode()
	{
		long projectCount = projectRepository.count();

		if(projectCount<10)
		{
			projectCode = "PR000"+(projectCount+1);
		}
		else if((projectCount>=10) && (projectCount<100))
		{
			projectCode = "PR00"+(projectCount+1);
		}
		else if((projectCount>=100) && (projectCount<1000))
		{
			projectCode = "PR0"+(projectCount+1);
		}
		else
		{
			projectCode = "PR"+(projectCount+1);
		}

		return projectCode;
	}


	//code for getting all country name
	private List<Country> findAllCountryId()
	{
		List<Country> countryList;

		countryList = countryRepository.findAll(new Sort(Sort.Direction.ASC,"countryId"));
		return countryList;
	}

	//code for retrieving all companies
	private List<Company> getAllCompanies()
	{
		List<Company> companyList;

		companyList = companyRepository.findAll(new Sort(Sort.Direction.ASC,"companyName"));
		return companyList;
	}

	//Request Mapping For Add Project
	@RequestMapping("/AddProject")
	public String addProject(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			model.addAttribute("projectCode", ProjectCode());
			model.addAttribute("countryList", findAllCountryId());
			model.addAttribute("companyList", getAllCompanies());
			return "AddProject";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/AddProject", method=RequestMethod.POST)
	public String projectSave(@ModelAttribute Project project, Model model)
	{
		try {
			try
			{
				project = new Project(project.getProjectId(),project.getProjectName().toUpperCase(),project.getProjectType(),project.getProjectAddress(),project.getCountryId(),project.getStateId(),project.getCityId(),project.getLocationareaId(),project.getAreaPincode(),project.getProjectofficePhoneno(),project.getCompanyId(),project.getProjectlandArea(),project.getProjectCost(),project.getNumberofBuildings(),project.getLoading(),project.getReraStatus(),project.getReraNumber(),project.getProjectstartDate(),project.getProjectendDate(),project.getProjectStatus(),project.getGatORserveyNumber(),
						project.getGstPer(),project.getStampdutyPer(),project.getRegistrationPer(),
						project.getCreationDate(),project.getUpdateDate(),project.getUserName());
				projectRepository.save(project);
				model.addAttribute("projectdbStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("projectdbStatus","Fail");
			}

			model.addAttribute("projectCode", ProjectCode());
			model.addAttribute("countryList", findAllCountryId());
			model.addAttribute("companyList", getAllCompanies());
			return "AddProject";

		}catch (Exception e) {
			return "login";
		}
	}

	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	//Request Mapping For  Project Master
	@RequestMapping("/ProjectMaster")
	public String ProjectMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			//List<Project> projectList = GetUserAssigenedProjectList(req,res); 
			List<Project> projectList = projectRepository.findAll();

			List<LocationArea> locationareaList= locationAreaRepository.findAll();
			List<City> cityList = cityRepository.findAll();
			List<State> stateList= stateRepository.findAll();
			List<Country> countryList= countryRepository.findAll();


			int i,j;
			for(i=0;i<projectList.size();i++)
			{
				try 
				{
					for(j=0;j<countryList.size();j++)
					{
						if(projectList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
						{
							projectList.get(i).setCountryId(countryList.get(j).getCountryName());
							break;
						}
					}

					for(j=0;j<stateList.size();j++)
					{
						if(projectList.get(i).getStateId().equals(stateList.get(j).getStateId()))
						{
							projectList.get(i).setStateId(stateList.get(j).getStateName());
							break;
						}
					}

					for(j=0;j<cityList.size();j++)
					{
						if(projectList.get(i).getCityId().equals(cityList.get(j).getCityId()))
						{
							projectList.get(i).setCityId(cityList.get(j).getCityName());
							break;
						}
					}

					for(j=0;j<locationareaList.size();j++)
					{
						if(projectList.get(i).getLocationareaId().equals(locationareaList.get(j).getLocationareaId()))
						{
							projectList.get(i).setLocationareaId(locationareaList.get(j).getLocationareaName());
							break;
						}
					}

				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}

			model.addAttribute("countryList", findAllCountryId());
			model.addAttribute("projectList",projectList);
			return "ProjectMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	//to Retive All Project in Project Master by country name
	@ResponseBody
	@RequestMapping(value="/getCountryProjectList",method=RequestMethod.POST)
	public List<Project> CountryWiseProjectList(@RequestParam("countryId") String countryId, HttpSession session)
	{
		List<Project> projectList= new ArrayList<Project>();

		Query query = new Query();
		projectList = mongoTemplate.find(query.addCriteria(Criteria.where("countryId").is(countryId)), Project.class);

		List<LocationArea> locationareaList= locationAreaRepository.findAll();
		List<City> cityList = cityRepository.findAll();
		List<State> stateList= stateRepository.findAll();
		List<Country> countryList= countryRepository.findAll();


		int i,j;
		for(i=0;i<projectList.size();i++)
		{
			try 
			{
				for(j=0;j<countryList.size();j++)
				{
					if(projectList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
					{
						projectList.get(i).setCountryId(countryList.get(j).getCountryName());
						break;
					}
				}

				for(j=0;j<stateList.size();j++)
				{
					if(projectList.get(i).getStateId().equals(stateList.get(j).getStateId()))
					{
						projectList.get(i).setStateId(stateList.get(j).getStateName());
						break;
					}
				}

				for(j=0;j<cityList.size();j++)
				{
					if(projectList.get(i).getCityId().equals(cityList.get(j).getCityId()))
					{
						projectList.get(i).setCityId(cityList.get(j).getCityName());
						break;
					}
				}

				for(j=0;j<locationareaList.size();j++)
				{
					if(projectList.get(i).getLocationareaId().equals(locationareaList.get(j).getLocationareaId()))
					{
						projectList.get(i).setLocationareaId(locationareaList.get(j).getLocationareaName());
						break;
					}
				}

			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		return projectList;
	}


	//to Retive All Project in Project Master by State name
	@ResponseBody
	@RequestMapping(value="/getStateWiseProjectList",method=RequestMethod.POST)
	public List<Project> ProjectListByState(@RequestParam("stateId") String stateId, @RequestParam("countryId") String countryId, HttpSession session)
	{
		List<Project> projectList= new ArrayList<Project>();

		Query query = new Query();
		projectList = mongoTemplate.find(query.addCriteria(Criteria.where("countryId").is(countryId).and("stateId").is(stateId)),  Project.class);

		List<LocationArea> locationareaList= locationAreaRepository.findAll();
		List<City> cityList = cityRepository.findAll();
		List<State> stateList= stateRepository.findAll();
		List<Country> countryList= countryRepository.findAll();


		int i,j;
		for(i=0;i<projectList.size();i++)
		{
			try 
			{
				for(j=0;j<countryList.size();j++)
				{
					if(projectList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
					{
						projectList.get(i).setCountryId(countryList.get(j).getCountryName());
						break;
					}
				}

				for(j=0;j<stateList.size();j++)
				{
					if(projectList.get(i).getStateId().equals(stateList.get(j).getStateId()))
					{
						projectList.get(i).setStateId(stateList.get(j).getStateName());
						break;
					}
				}

				for(j=0;j<cityList.size();j++)
				{
					if(projectList.get(i).getCityId().equals(cityList.get(j).getCityId()))
					{
						projectList.get(i).setCityId(cityList.get(j).getCityName());
						break;
					}
				}

				for(j=0;j<locationareaList.size();j++)
				{
					if(projectList.get(i).getLocationareaId().equals(locationareaList.get(j).getLocationareaId()))
					{
						projectList.get(i).setLocationareaId(locationareaList.get(j).getLocationareaName());
						break;
					}
				}

			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		return projectList;
	}

	//to Retive All Project in Project Master by city name
	@ResponseBody
	@RequestMapping(value="/getCityWiseProjectList",method=RequestMethod.POST)
	public List<Project> ProjectListByCity(@RequestParam("cityId") String cityId, @RequestParam("stateId") String stateId, @RequestParam("countryId") String countryId, HttpSession session)
	{
		List<Project> projectList= new ArrayList<Project>();

		Query query = new Query();
		projectList = mongoTemplate.find(query.addCriteria(Criteria.where("countryId").is(countryId).and("stateId").is(stateId).and("cityId").is(cityId)), Project.class);

		List<LocationArea> locationareaList= locationAreaRepository.findAll();
		List<City> cityList = cityRepository.findAll();
		List<State> stateList= stateRepository.findAll();
		List<Country> countryList= countryRepository.findAll();


		int i,j;
		for(i=0;i<projectList.size();i++)
		{
			try 
			{
				for(j=0;j<countryList.size();j++)
				{
					if(projectList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
					{
						projectList.get(i).setCountryId(countryList.get(j).getCountryName());
						break;
					}
				}

				for(j=0;j<stateList.size();j++)
				{
					if(projectList.get(i).getStateId().equals(stateList.get(j).getStateId()))
					{
						projectList.get(i).setStateId(stateList.get(j).getStateName());
						break;
					}
				}

				for(j=0;j<cityList.size();j++)
				{
					if(projectList.get(i).getCityId().equals(cityList.get(j).getCityId()))
					{
						projectList.get(i).setCityId(cityList.get(j).getCityName());
						break;
					}
				}

				for(j=0;j<locationareaList.size();j++)
				{
					if(projectList.get(i).getLocationareaId().equals(locationareaList.get(j).getLocationareaId()))
					{
						projectList.get(i).setLocationareaId(locationareaList.get(j).getLocationareaName());
						break;
					}
				}

			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		return projectList;
	}

	//to Retive All Project in Project Master by city name
	@ResponseBody
	@RequestMapping(value="/getProjectList",method=RequestMethod.POST)
	public List<Project> ProjectListByLocation(@RequestParam("locationareaId") String locationareaId, @RequestParam("cityId") String cityId, @RequestParam("stateId") String stateId, @RequestParam("countryId") String countryId, HttpSession session)
	{
		List<Project> projectList= new ArrayList<Project>();

		Query query = new Query();
		projectList = mongoTemplate.find(query.addCriteria(Criteria.where("countryId").is(countryId).and("stateId").is(stateId).and("cityId").is(cityId).and("locationareaId").is(locationareaId)),  Project.class);

		query = new Query();
		List<LocationArea> locationareaList= mongoTemplate.find(query.addCriteria(Criteria.where("locationareaId").is(locationareaId)),  LocationArea.class);

		query = new Query();
		List<City> cityList= mongoTemplate.find(query.addCriteria(Criteria.where("cityId").is(cityId)),  City.class);

		query = new Query();
		List<State> stateList= mongoTemplate.find(query.addCriteria(Criteria.where("stateId").is(stateId)),  State.class);

		query = new Query();
		List<Country> countryList= mongoTemplate.find(query.addCriteria(Criteria.where("countryId").is(countryId)),  Country.class);


		int i,j;
		for(i=0;i<projectList.size();i++)
		{
			try 
			{
				projectList.get(i).setCountryId(countryList.get(0).getCountryName());

				projectList.get(i).setStateId(stateList.get(0).getStateName());

				projectList.get(i).setCityId(cityList.get(0).getCityName());

				projectList.get(i).setLocationareaId(locationareaList.get(0).getLocationareaName());

			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		return projectList;
	}

	@ResponseBody
	@RequestMapping("/SearchProjectNameWiseList")
	public List<Project> SearchProjectNameWiseList(@RequestParam("projectName") String projectName)
	{
		Query query = new Query();

		List<Project> projectList = mongoTemplate.find(query.addCriteria(Criteria.where("projectName").regex("^"+projectName+".*","i")), Project.class);
		return projectList;
	}

	@ResponseBody
	@RequestMapping("/getallprojectList")
	public List<Project> AllprojectList(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		List<Project> projectList= GetUserAssigenedProjectList(req,res);

		return projectList;
	}

	@RequestMapping("/EditProject")
	public String EditProject(@RequestParam("projectId") String projectId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)),Project.class);

			Double projectCost = projectDetails.get(0).getProjectCost() ;

			query = new Query();
			List<Company> companyDetails = mongoTemplate.find(query.addCriteria(Criteria.where("companyId").is(projectDetails.get(0).getCompanyId())),Company.class);


			try {
				query = new Query();
				Country countryDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("countryId").is(projectDetails.get(0).getCountryId())),Country.class);
				query = new Query();
				State stateDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("stateId").is(projectDetails.get(0).getStateId())),State.class);
				query = new Query();
				City cityDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("cityId").is(projectDetails.get(0).getCityId())),City.class);
				query = new Query();
				LocationArea locationareaDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("locationareaId").is(projectDetails.get(0).getLocationareaId())),LocationArea.class);

				model.addAttribute("countryName", countryDetails.getCountryName());
				model.addAttribute("stateName", stateDetails.getStateName());
				model.addAttribute("cityName", cityDetails.getCityName());
				model.addAttribute("locationareaName", locationareaDetails.getLocationareaName());
			}catch (Exception e) {
				// TODO: handle exception
			}

			model.addAttribute("projectCost", BigDecimal.valueOf(projectCost).toPlainString());
			model.addAttribute("companyName",companyDetails.get(0).getCompanyName());
			model.addAttribute("projectDetails",projectDetails);
			model.addAttribute("countryList", findAllCountryId());
			model.addAttribute("companyList", getAllCompanies());
			return "EditProject";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/EditProject", method=RequestMethod.POST)
	public String EditProjectPostMethod(@ModelAttribute Project project, Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				project = new Project(project.getProjectId(),project.getProjectName().toUpperCase(),project.getProjectType(),project.getProjectAddress(),project.getCountryId(),project.getStateId(),project.getCityId(),project.getLocationareaId(),project.getAreaPincode(),project.getProjectofficePhoneno(),project.getCompanyId(),project.getProjectlandArea(),project.getProjectCost(),project.getNumberofBuildings(),project.getLoading(),project.getReraStatus(),project.getReraNumber(),project.getProjectstartDate(),project.getProjectendDate(),project.getProjectStatus(),project.getGatORserveyNumber(),
						project.getGstPer(),project.getStampdutyPer(),project.getRegistrationPer(),
						project.getCreationDate(),project.getUpdateDate(),project.getUserName());
				projectRepository.save(project);
				model.addAttribute("projectdbStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("projectdbStatus","Fail");
			}


			Double dryterraceArea;
			Double balconyArea;
			Double carpetArea,terraceArea,loading,totalArea=0.0;
			Double totalloading,flatAreawithLoadingInM,flatAreawithLoadingInFt,loadingpercentage,floorRise=0.0,totalFlatCosrperSqft,flatBasiccost;
			Double flatCostwithotfloorise;
			if(project.getPriviousLoading()!=project.getLoading())
			{

				Query query = new Query();
				List<Flat> flatList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(project.getProjectId()).and("flatstatus").is("Booking Remaninig")),Flat.class);

				for(int i=0;i<flatList.size();i++)
				{
					dryterraceArea=flatList.get(i).getDryterraceArea();
					balconyArea=flatList.get(i).getBalconyArea();
					carpetArea=flatList.get(i).getCarpetArea();
					terraceArea=flatList.get(i).getTerraceArea();
					loading=Double.valueOf(project.getLoading());
					flatCostwithotfloorise=flatList.get(i).getFlatCostwithotfloorise();
					floorRise=flatList.get(i).getFloorRise();

					totalArea=dryterraceArea+balconyArea+carpetArea+terraceArea;

					totalloading=loading/100;
					flatAreawithLoadingInM=((totalArea)*(loading/100))+totalArea;
					flatAreawithLoadingInFt=(double) Math.round(flatAreawithLoadingInM*10.764);
					loadingpercentage=totalArea*loading/100;

					totalFlatCosrperSqft=flatCostwithotfloorise+floorRise;
					flatBasiccost=(double) Math.round(flatAreawithLoadingInFt*totalFlatCosrperSqft);

					flatList.get(i).setFlatAreawithLoadingInM((double)Math.round(flatAreawithLoadingInM));
					flatList.get(i).setFlatAreawithLoadingInFt((double)Math.round(flatAreawithLoadingInFt));
					flatList.get(i).setLoadingpercentage((double)Math.round(loadingpercentage));
					flatList.get(i).setLoading(loading);
					flatList.get(i).setFlatCost(totalFlatCosrperSqft);
					flatList.get(i).setFlatbasicCost(flatBasiccost);

					flatRepository.save(flatList.get(i));
				}



			}



			//List<Project> projectList = GetUserAssigenedProjectList(req,res); 
			List<Project> projectList = projectRepository.findAll();


			List<LocationArea> locationareaList= locationAreaRepository.findAll();
			List<City> cityList = cityRepository.findAll();
			List<State> stateList= stateRepository.findAll();
			List<Country> countryList= countryRepository.findAll();


			int i,j;
			for(i=0;i<projectList.size();i++)
			{
				try 
				{
					for(j=0;j<countryList.size();j++)
					{
						if(projectList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
						{
							projectList.get(i).setCountryId(countryList.get(j).getCountryName());
							break;
						}
					}

					for(j=0;j<stateList.size();j++)
					{
						if(projectList.get(i).getStateId().equals(stateList.get(j).getStateId()))
						{
							projectList.get(i).setStateId(stateList.get(j).getStateName());
							break;
						}
					}

					for(j=0;j<cityList.size();j++)
					{
						if(projectList.get(i).getCityId().equals(cityList.get(j).getCityId()))
						{
							projectList.get(i).setCityId(cityList.get(j).getCityName());
							break;
						}
					}

					for(j=0;j<locationareaList.size();j++)
					{
						if(projectList.get(i).getLocationareaId().equals(locationareaList.get(j).getLocationareaId()))
						{
							projectList.get(i).setLocationareaId(locationareaList.get(j).getLocationareaName());
							break;
						}
					}

				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}

			model.addAttribute("countryList", findAllCountryId());
			model.addAttribute("projectList",projectList);
			return "ProjectMaster";

		}catch (Exception e) {
			return "login";
		}
	}


	@ResponseBody
	@RequestMapping(value="/getProjectLoading",method=RequestMethod.POST)
	public List<Project> ProjectLoading(@RequestParam("projectId") String projectId,HttpSession session)
	{
		List<Project> projectList= new ArrayList<Project>();

		Query query = new Query();
		projectList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)),  Project.class);

		return projectList;
	}

	@RequestMapping("/AddProjectMarketRate")
	public String AddProjectMarketRate(@RequestParam("projectId") String projectId, ModelMap model)
	{
		try {
			try {
				Query query = new Query();
				Project projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectId)), Project.class);

				query = new Query();
				List<ProjectMarketRate> projectmarketRateList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)), ProjectMarketRate.class);
				model.addAttribute("projectDetails",projectDetails);
				model.addAttribute("projectmarketRateList",projectmarketRateList);
			}catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}
			return "AddProjectMarketRate";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/AddProjectMarketRate", method=RequestMethod.POST)
	public String AddProjectMarketRate(@ModelAttribute ProjectMarketRate projectmarketRate, Model model)
	{
		try {
			try
			{
				ProjectMarketRate projectmarketRate1=new ProjectMarketRate();
				projectmarketRate1.setProjectId(projectmarketRate.getProjectId());
				projectmarketRate1.setYear(projectmarketRate.getYear());
				projectmarketRate1.setSubdivision(projectmarketRate.getSubdivision());
				projectmarketRate1.setOpenGround(projectmarketRate.getOpenGround());
				projectmarketRate1.setResidentialHouse(projectmarketRate.getResidentialHouse());
				projectmarketRate1.setOffice(projectmarketRate.getOffice());
				projectmarketRate1.setShops(projectmarketRate.getShops());
				projectmarketRate1.setIndustrial(projectmarketRate.getIndustrial());
				projectmarketRate1.setUnit(projectmarketRate.getUnit());
				projectmarketRate1.setUserName(projectmarketRate.getUserName());

				projectmarketrateRepository.save(projectmarketRate1);

				model.addAttribute("projectdbStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				de.printStackTrace();
				model.addAttribute("projectdbStatus","Fail");
			}




			//List<Project> projectList = GetUserAssigenedProjectList(req,res); 
			List<Project> projectList = projectRepository.findAll();

			List<LocationArea> locationareaList= locationAreaRepository.findAll();
			List<City> cityList = cityRepository.findAll();
			List<State> stateList= stateRepository.findAll();
			List<Country> countryList= countryRepository.findAll();

			int i,j;
			for(i=0;i<projectList.size();i++)
			{
				try 
				{
					for(j=0;j<countryList.size();j++)
					{
						if(projectList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
						{
							projectList.get(i).setCountryId(countryList.get(j).getCountryName());
							break;
						}
					}

					for(j=0;j<stateList.size();j++)
					{
						if(projectList.get(i).getStateId().equals(stateList.get(j).getStateId()))
						{
							projectList.get(i).setStateId(stateList.get(j).getStateName());
							break;
						}
					}

					for(j=0;j<cityList.size();j++)
					{
						if(projectList.get(i).getCityId().equals(cityList.get(j).getCityId()))
						{
							projectList.get(i).setCityId(cityList.get(j).getCityName());
							break;
						}
					}

					for(j=0;j<locationareaList.size();j++)
					{
						if(projectList.get(i).getLocationareaId().equals(locationareaList.get(j).getLocationareaId()))
						{
							projectList.get(i).setLocationareaId(locationareaList.get(j).getLocationareaName());
							break;
						}
					}

				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}

			model.addAttribute("countryList", findAllCountryId());
			model.addAttribute("projectList",projectList);
			return "ProjectMaster";

		}catch (Exception e) {
			return "login";
		}
	}
}

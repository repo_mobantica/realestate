package com.realestate.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.realestate.bean.Country;
import com.realestate.bean.State;
import com.realestate.repository.StateRepository;
import com.realestate.repository.CountryRepository;

@Controller
@RequestMapping("/")
public class ImportNewStateController {

	@Autowired
	StateRepository stateRepository;
	@Autowired
	CountryRepository countryRepository;

	@Autowired
	ServletContext context;

	State state = new State();
	HSSFWorkbook workbook;
	XSSFWorkbook workbook1;

	HSSFSheet worksheet;
	XSSFSheet worksheet1;

	@Autowired
	MongoTemplate mongoTemplate;

	String stateCode;

	private String StateCode()
	{
		long stateCount  = stateRepository.count();

		if(stateCount<10)
		{
			stateCode = "ST000"+(stateCount+1);
		}
		else if((stateCount>=10) && (stateCount<100))
		{
			stateCode = "ST00"+(stateCount+1);
		}
		else if((stateCount>=100) && (stateCount<1000))
		{
			stateCode = "ST0"+(stateCount+1);
		}
		else
		{
			stateCode = "ST"+(stateCount+1);
		}

		return stateCode;
	}

	@RequestMapping(value="/ImportNewState")
	public String importNewState(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		return "ImportNewState";
	}

	@SuppressWarnings({ "deprecation"})
	@RequestMapping(value = "/ImportNewState", method = RequestMethod.POST)
	public String uploadStateDetails(@RequestParam("state_excel") MultipartFile state_excel, Model model, HttpServletRequest request, RedirectAttributes redirectAttributes, HttpSession session) 
	{

		List<Country> countryList= countryRepository.findAll();
		String employeeId = (String)request.getSession().getAttribute("employeeId");
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/M/yyyy");
		LocalDate localDate = LocalDate.now();
		String todayDate=dtf.format(localDate);

		if (!state_excel.isEmpty() || state_excel != null )
		{
			try 
			{
				if(state_excel.getOriginalFilename().endsWith("xls") || state_excel.getOriginalFilename().endsWith("xlsx") || state_excel.getOriginalFilename().endsWith("csv"))
				{
					if(state_excel.getOriginalFilename().endsWith("xlsx"))
					{
						InputStream stream = state_excel.getInputStream();
						XSSFWorkbook workbook = new XSSFWorkbook(stream);

						XSSFSheet sheet = workbook.getSheet("Sheet1");  /// this will read 1st workbook of ExcelSheet

						int firstRow = sheet.getFirstRowNum();

						XSSFRow firstrow = sheet.getRow(firstRow);

						@SuppressWarnings("unused")
						int lastColumnCount = firstrow.getLastCellNum();

						@SuppressWarnings("unused")
						Iterator<Row> rowIterator = sheet.iterator();   
						int last_no=sheet.getLastRowNum();

						State state = new State();
						for(int i=0;i<last_no;i++)
						{
							try
							{
								XSSFRow row = sheet.getRow(i+1);

								// Skip read heading 
								if (row.getRowNum() == 0) 
								{
									continue;
								}

								for(int k=0;k<countryList.size();k++)
								{
									if(countryList.get(k).getCountryName().equalsIgnoreCase(row.getCell(1).getStringCellValue()))
									{

										state.setStateId(StateCode());

										row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
										state.setStateName(row.getCell(0).getStringCellValue());

										state.setCountryId(countryList.get(k).getCountryId());
										state.setCreationDate(todayDate);
										state.setUpdateDate(todayDate);
										state.setUserName(employeeId);
										stateRepository.save(state);
										break;
									}

								}
							}
							catch (Exception e) {
								// TODO: handle exception
							}      

						}
						workbook.close();
					}

				}//if after inner if

			}
			catch(Exception e)
			{

			}
		}

		return "ImportNewState";
	}


	//private static final String INTERNAL_FILE="Newstate.xlsx";
	@RequestMapping(value = "/DownloadStateTemplate")
	public String downloadStateTemplate(Model model, HttpServletResponse response, HttpServletRequest request, RedirectAttributes redirectAttributes, HttpSession session) throws IOException 
	{

		String filename = "NewState.xlsx";
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		String filepath = "//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"Template/";
		response.setContentType("APPLICATION/OCTET-STREAM");
		response.setHeader("Content-Disposition", "attachment; filename=\""+ filename + "\"");


		FileInputStream fileInputStream = new FileInputStream(filepath+filename);

		int i;
		while ((i = fileInputStream.read()) != -1) {
			out.write(i);
		}
		fileInputStream.close();
		out.close();

		return "ImportNewState";
	}
}

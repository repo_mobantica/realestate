package com.realestate.controller;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.ChartofAccount;
import com.realestate.repository.ChartofAccountRepository;


@Controller
@RequestMapping("/")
public class ChartofAccountWiseSubchartListController 
{
	@Autowired
	ChartofAccountRepository chartofaccountRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	@ResponseBody
	@RequestMapping(value="/getChartNumberList",method=RequestMethod.POST)
	public ChartofAccount SubChartNumberList(@RequestParam("chartaccountNumber") String chartaccountNumber, HttpSession session)
	{
		//List<ChartofAccount> chartaccountList= new ArrayList<ChartofAccount>();
		// chartaccountList = createNumber("age", Integer.class);

		Query query = new Query();
		final ChartofAccount chartaccountList = (ChartofAccount) mongoTemplate.find(query.addCriteria(Criteria.where("chartaccountNumber").is(chartaccountNumber)), ChartofAccount.class);

		return chartaccountList;
	}
}

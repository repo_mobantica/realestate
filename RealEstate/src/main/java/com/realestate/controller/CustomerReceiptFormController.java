package com.realestate.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Aggreement;
import com.realestate.bean.Bank;
import com.realestate.bean.Booking;
import com.realestate.bean.BookingCancelForm;
import com.realestate.bean.CustomerFlatCheckList;
import com.realestate.bean.CustomerLoanDetails;
import com.realestate.bean.CustomerPaymentDetails;
import com.realestate.bean.CustomerReceiptForm;
import com.realestate.bean.Enquiry;
import com.realestate.bean.ExtraCharges;
import com.realestate.bean.Flat;
import com.realestate.bean.GeneratePaymentScheduler;
import com.realestate.bean.Login;
import com.realestate.bean.PaymentScheduler;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.AgentRepository;
import com.realestate.repository.AggreementRepository;
import com.realestate.repository.BankRepository;
import com.realestate.repository.BookingCancelFormRepository;
import com.realestate.repository.BookingRepository;
import com.realestate.repository.BudgetRepository;
import com.realestate.repository.CountryRepository;
import com.realestate.repository.CustomerFlatCheckListRepository;
import com.realestate.repository.CustomerLoanDetailsRepository;
import com.realestate.repository.CustomerPaymentDetailsRepository;
import com.realestate.repository.CustomerReceiptFormRepository;
import com.realestate.repository.DesignationRepository;
import com.realestate.repository.EnquiryRepository;
import com.realestate.repository.EnquirySourceRepository;
import com.realestate.repository.ExtraChargeRepository;
import com.realestate.repository.FlatRepository;
import com.realestate.repository.FloorRepository;
import com.realestate.repository.GeneratePaymentSchedulerRepository;
import com.realestate.repository.OccupationRepository;
import com.realestate.repository.ParkingZoneRepository;
import com.realestate.repository.PaymentSchedulerRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.TaxRepository;
import com.realestate.services.SmsServices;

@Controller
@RequestMapping("/")
public class CustomerReceiptFormController
{
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	CustomerReceiptFormRepository customerreceiptformrepository;
	@Autowired
	CustomerPaymentDetailsRepository customerpaymentdetailsrepository;
	@Autowired
	CustomerLoanDetailsRepository customerLoandetailsRepository;
	@Autowired 
	AggreementRepository aggreementRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	BankRepository bankRepository;
	@Autowired 
	BookingRepository bookingRepository;
	@Autowired
	FlatRepository flatRepository;
	@Autowired
	BudgetRepository budgetRepository;
	@Autowired
	EnquirySourceRepository enquirySourceRepository;
	@Autowired
	EnquiryRepository enquiryRepository;
	@Autowired
	ExtraChargeRepository extrachargesRepository;
	@Autowired
	BookingCancelFormRepository bookingcancelformRepository;
	@Autowired
	AgentRepository agentRepository;
	@Autowired
	CustomerFlatCheckListRepository customerflatchecklistRepository;

	String payAmountCode;
	private String customerpayAmountCode()
	{
		long Count = customerpaymentdetailsrepository.count();

		if(Count<10)
		{
			payAmountCode = "TP000"+(Count+1);
		}
		else if((Count>=10) && (Count<100))
		{
			payAmountCode = "TP00"+(Count+1);
		}
		else if((Count>=100) && (Count<1000))
		{
			payAmountCode = "TP0"+(Count+1);
		}
		else
		{
			payAmountCode = "TP"+(Count+1);
		}

		return payAmountCode;
	}

	private List<Bank> getOtherBankName()
	{
		List<Bank> bankList= new ArrayList<Bank>();
		Query query = new Query();
		bankList = bankRepository.findAll(new Sort(Sort.Direction.ASC,"bankName"));
		return bankList;
	}

	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping("/CustomerReceiptForm")
	public String CustomerreceiptForm(@RequestParam("bookingId") String bookingId, ModelMap model) 
	{

		try {
			try
			{
				Query query = new Query();
				List<Booking> bookingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)),Booking.class);
				query = new Query();
				List<CustomerPaymentDetails> customerPayment = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)),CustomerPaymentDetails.class);

				query = new Query();
				List<GeneratePaymentScheduler> generatepaymentscheduler = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)),GeneratePaymentScheduler.class);


				query = new  Query();
				List<CustomerLoanDetails> loanList = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingId)),CustomerLoanDetails.class);


				//code for getting demand details

				//code to get payment schedule
				query = new Query();
				List<PaymentScheduler> paymentSchedulerList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(bookingDetails.get(0).getProjectId()).and("buildingId").is(bookingDetails.get(0).getBuildingId()).and("wingId").is(bookingDetails.get(0).getWingId()).and("slabStatus").is("Completed")), PaymentScheduler.class);

				double per = 0.0d;

				for(int i=0;i<paymentSchedulerList.size();i++)
				{
					per = per + paymentSchedulerList.get(i).getPercentage();
				}

				double dueAsPerWork = ((bookingDetails.get(0).getAggreementValue1()*per)/100);

				//code to get totalpaid by customer till date
				query = new Query();
				List<CustomerPaymentDetails> paymentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingDetails.get(0).getBookingId())), CustomerPaymentDetails.class);


				double totalpaidTillDate=0.0;
				try
				{
					if(paymentDetails.size()!=0)
					{	
						totalpaidTillDate = paymentDetails.get(0).getTotalpayAggreement();
					}
					else
					{
						totalpaidTillDate = 0;
					}
				}
				catch (Exception e) {
					// TODO: handle exception
				}

				double totalExcludingGST = dueAsPerWork-totalpaidTillDate;

				//code to get customer GST percentage
				double GSTper = bookingDetails.get(0).getGstCost();

				double GSTdue = (dueAsPerWork*GSTper)/100;

				HashMap<String,Object> demandDetails = new HashMap<String, Object>();

				demandDetails.put("agreementValue", ""+(long)bookingDetails.get(0).getAggreementValue1().doubleValue());
				demandDetails.put("completedWorkSteg",(int)per+"%");
				demandDetails.put("dueAsPerWork",(long)dueAsPerWork);
				demandDetails.put("totalTillDate",totalpaidTillDate);
				demandDetails.put("totalExcludingGST",(long)(dueAsPerWork-totalpaidTillDate));
				demandDetails.put("GSTdue",(long)GSTdue);
				demandDetails.put("totalIncludingGST",(long)(totalExcludingGST+GSTdue));
				//end of demand details retrieval code

				//model.addAttribute("receiptCode",ReceiptCode(bookingDetails.get(0).getProjectName(), bookingDetails.get(0).getBuildingName(), bookingDetails.get(0).getBookingId()));
				String loanAmountAgainst="";
				long loanAmount=0;
				try {
					if(loanList.size()!=0)
					{
						loanAmount=loanList.get(0).getLoanAmount().longValue();

						if(loanList.get(0).getAgainstAggreement().equals("AggreementAgainst"))
						{
							loanAmountAgainst=loanAmountAgainst+"Agreement  ";
						}
						if(loanList.get(0).getAgainstRegistration().equals("RegistrationAgainst"))
						{
							loanAmountAgainst=loanAmountAgainst+"Registration  ";
						}
						if(loanList.get(0).getAgainstStampDuty().equals("StampDutyAgainst"))
						{
							loanAmountAgainst=loanAmountAgainst+"Stamp Duty  ";
						}
						if(loanList.get(0).getAgainstGST().equals("GSTAgainst"))
						{
							loanAmountAgainst=loanAmountAgainst+"GST  ";
						}
					}
				}
				catch (Exception e) {
					// TODO: handle exception
				}
				model.addAttribute("loanAmountAgainst", loanAmountAgainst);
				model.addAttribute("loanAmount", loanAmount);
				model.addAttribute("bankList",getOtherBankName());
				model.addAttribute("bookingDetails", bookingDetails);
				model.addAttribute("customerPayment", customerPayment);
				model.addAttribute("generatepaymentscheduler", generatepaymentscheduler);
				model.addAttribute("demandDetails",demandDetails);
			}
			catch (Exception e) {
				// TODO: handle exception
			}

			return "CustomerReceiptForm";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/CustomerReceiptForm", method = RequestMethod.POST)
	public String SavePayment(@RequestParam("bookingId") String bookingId, @RequestParam("aggreementAmount") double aggreementAmount,@RequestParam("stampDutyAmount") double stampDutyAmount, @RequestParam("registrationAmount") double registrationAmount,@RequestParam("gstAmount") double gstAmount, @RequestParam("paymentMode") String paymentMode, @RequestParam("bankName") String bankName, @RequestParam("branchName") String branchName, @RequestParam("chequeNumber") String chequeNumber, @RequestParam("narration") String narration, @RequestParam("date") String date, Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			Query query = new Query();
			String mobileNumber="";
			String sms="";

			try
			{
				query = new Query();
				Booking bookingList;
				bookingList = mongoTemplate.findOne(Query.query(Criteria.where("bookingId").is(bookingId)), Booking.class);
				mobileNumber=bookingList.getBookingmobileNumber1();
				sms="Dear "+bookingList.getBookingfirstname()+", thanks for paid amount under the ";
				/*
			 query =new Query();
			 List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(bookingList.getProjectId())), Project.class);

			 query =new Query();
			 List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(bookingList.getBuildingId())), ProjectBuilding.class);

			 query =new Query();
			 List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(bookingList.getWingId())), ProjectWing.class);

		     String projectName=projectDetails.get(0).getProjectName();
		     String buildingName=buildingDetails.get(0).getBuildingName();
		     String wingName=wingDetails.get(0).getWingName();
				 */
				String one = "REC";
				one = one +"/"+bookingId+"/";
				String receiptCode="";

				Date todayDate = new Date();  

				/*SimpleDateFormat formatter = new SimpleDateFormat("d/M/yyyy");  
			 String strDate= formatter.format(date);
				 */
				CustomerReceiptForm customerreceiptform=new CustomerReceiptForm();
				Double paymentAmount;
				String paymentType;
				if(aggreementAmount!=0.0)
				{
					sms=sms+" Agreement amount "+aggreementAmount +"/- ";
					query = new Query();
					long receiptCount = mongoTemplate.count(query.addCriteria(Criteria.where("bookingId").is(bookingId)), CustomerReceiptForm.class);
					if(receiptCount<10)
					{
						receiptCode = one+"0"+(receiptCount+1);
					}
					else 
					{
						receiptCode = one+(receiptCount+1);
					}

					paymentType="Agreement";
					paymentAmount=aggreementAmount;
					customerreceiptform.setReceiptId(receiptCode);
					customerreceiptform.setBookingId(bookingId);
					customerreceiptform.setPaymentAmount(paymentAmount);
					customerreceiptform.setPaymentType(paymentType);
					customerreceiptform.setPaymentMode(paymentMode);
					customerreceiptform.setBankName(bankName);
					customerreceiptform.setBranchName(branchName);
					customerreceiptform.setChequeNumber(chequeNumber);
					customerreceiptform.setNarration(narration);
					customerreceiptform.setDate(date);
					customerreceiptform.setCreationDate(todayDate);
					customerreceiptform.setStatus("Not Deposited");
					customerreceiptformrepository.save(customerreceiptform);
				}

				if(stampDutyAmount!=0.0)
				{
					sms=sms+" Stamp Duty amount "+stampDutyAmount+"/- ";
					query = new Query();
					long receiptCount = mongoTemplate.count(query.addCriteria(Criteria.where("bookingId").is(bookingId)), CustomerReceiptForm.class);
					if(receiptCount<10)
					{
						receiptCode = one+"0"+(receiptCount+1);
					}
					else 
					{
						receiptCode = one+(receiptCount+1);
					}
					customerreceiptform=new CustomerReceiptForm();
					paymentType="Stamp Duty";
					paymentAmount=stampDutyAmount;
					customerreceiptform.setReceiptId(receiptCode);
					customerreceiptform.setBookingId(bookingId);
					customerreceiptform.setPaymentAmount(paymentAmount);
					customerreceiptform.setPaymentType(paymentType);
					customerreceiptform.setPaymentMode(paymentMode);
					customerreceiptform.setBankName(bankName);
					customerreceiptform.setBranchName(branchName);
					customerreceiptform.setChequeNumber(chequeNumber);
					customerreceiptform.setNarration(narration);
					customerreceiptform.setDate(date);
					customerreceiptform.setCreationDate(todayDate);
					customerreceiptform.setStatus("Not Deposited");
					customerreceiptformrepository.save(customerreceiptform);
				}
				if(registrationAmount!=0.0)
				{
					sms=sms+" Registration amount "+registrationAmount+"/-";
					query = new Query();
					long receiptCount = mongoTemplate.count(query.addCriteria(Criteria.where("bookingId").is(bookingId)), CustomerReceiptForm.class);
					if(receiptCount<10)
					{
						receiptCode = one+"0"+(receiptCount+1);
					}
					else 
					{
						receiptCode = one+(receiptCount+1);
					}
					customerreceiptform=new CustomerReceiptForm();
					paymentType="Registration";
					paymentAmount=registrationAmount;
					customerreceiptform.setReceiptId(receiptCode);
					customerreceiptform.setBookingId(bookingId);
					customerreceiptform.setPaymentAmount(paymentAmount);
					customerreceiptform.setPaymentType(paymentType);
					customerreceiptform.setPaymentMode(paymentMode);
					customerreceiptform.setBankName(bankName);
					customerreceiptform.setBranchName(branchName);
					customerreceiptform.setChequeNumber(chequeNumber);
					customerreceiptform.setNarration(narration);
					customerreceiptform.setDate(date);
					customerreceiptform.setCreationDate(todayDate);
					customerreceiptform.setStatus("Not Deposited");
					customerreceiptformrepository.save(customerreceiptform);
				}
				if(gstAmount!=0.0)
				{
					sms=sms+" GST amount "+gstAmount+"/-";
					query = new Query();
					long receiptCount = mongoTemplate.count(query.addCriteria(Criteria.where("bookingId").is(bookingId)), CustomerReceiptForm.class);
					if(receiptCount<10)
					{
						receiptCode = one+"0"+(receiptCount+1);
					}
					else 
					{
						receiptCode = one+(receiptCount+1);
					}
					customerreceiptform=new CustomerReceiptForm();
					paymentType="GST";
					paymentAmount=gstAmount;
					customerreceiptform.setReceiptId(receiptCode);
					customerreceiptform.setBookingId(bookingId);
					customerreceiptform.setPaymentAmount(paymentAmount);
					customerreceiptform.setPaymentType(paymentType);
					customerreceiptform.setPaymentMode(paymentMode);
					customerreceiptform.setBankName(bankName);
					customerreceiptform.setBranchName(branchName);
					customerreceiptform.setChequeNumber(chequeNumber);
					customerreceiptform.setNarration(narration);
					customerreceiptform.setDate(date);
					customerreceiptform.setCreationDate(todayDate);
					customerreceiptform.setStatus("Not Deposited");
					customerreceiptformrepository.save(customerreceiptform);
				}
				Double totalpayAggreement=0.0;
				Double totalpayStampDuty=0.0;
				Double totalpayRegistration=0.0;
				Double totalpaygstAmount=0.0;
				Query query4 = new Query();
				List<CustomerPaymentDetails> customerpaymentdetails = mongoTemplate.find(query4.addCriteria(Criteria.where("bookingId").is(bookingId)),CustomerPaymentDetails.class);
				if(customerpaymentdetails.size()!=0)
				{
					totalpayAggreement=customerpaymentdetails.get(0).getTotalpayAggreement()+aggreementAmount;
					totalpayStampDuty=customerpaymentdetails.get(0).getTotalpayStampDuty()+stampDutyAmount;
					totalpayRegistration=customerpaymentdetails.get(0).getTotalpayRegistration()+registrationAmount;
					totalpaygstAmount=customerpaymentdetails.get(0).getTotalpaygstAmount()+gstAmount;
					String amountStatus;
					if(bookingList.getAggreementValue1()==totalpayAggreement && bookingList.getStampDuty1()==totalpayStampDuty && bookingList.getRegistrationCost1()==totalpayRegistration && bookingList.getGstAmount1()==totalpaygstAmount)
					{
						amountStatus="Completed";
					}
					else
					{
						amountStatus="Pending";
					}
					try
					{
						customerpaymentdetails.get(0).setTotalpayAggreement(totalpayAggreement);
						customerpaymentdetails.get(0).setTotalpayStampDuty(totalpayStampDuty);
						customerpaymentdetails.get(0).setTotalpayRegistration(totalpayRegistration);
						customerpaymentdetails.get(0).setTotalpaygstAmount(totalpaygstAmount);
						customerpaymentdetails.get(0).setAmountStatus(amountStatus);
						customerpaymentdetails.get(0).setCreationDate(todayDate);
						customerpaymentdetails.get(0).setUpdateDate(todayDate);
						//customerpaymentdetails.getUserName();
						customerpaymentdetailsrepository.save(customerpaymentdetails);
					}
					catch(Exception ee)
					{
					}
				}
				else
				{
					String amountStatus;
					if(bookingList.getAggreementValue1()==aggreementAmount && bookingList.getStampDuty1()==stampDutyAmount && bookingList.getRegistrationCost1()==registrationAmount && bookingList.getGstAmount1()==gstAmount)
					{
						amountStatus="Completed";
					}
					else
					{
						amountStatus="Pending";
					}

					CustomerPaymentDetails customerpaymentdetails1=new CustomerPaymentDetails();
					customerpaymentdetails1.setId(customerpayAmountCode());
					customerpaymentdetails1.setBookingId(bookingId);
					customerpaymentdetails1.setTotalpayAggreement(aggreementAmount);
					customerpaymentdetails1.setTotalpayStampDuty(stampDutyAmount);
					customerpaymentdetails1.setTotalpayRegistration(registrationAmount);
					customerpaymentdetails1.setTotalpaygstAmount(gstAmount);
					customerpaymentdetails1.setAmountStatus(amountStatus);
					customerpaymentdetails1.setCreationDate(todayDate);
					customerpaymentdetails1.setUpdateDate(todayDate);
					//customerpaymentdetails.getUserName();
					customerpaymentdetailsrepository.save(customerpaymentdetails1);
				}
			}
			catch (Exception e) {
				System.out.println("Error = "+e);
				// TODO: handle exception
			}

			try {
				sms=sms+".";
				SmsServices.sendSMS(mobileNumber,sms);
			}catch (Exception e) {
				// TODO: handle exception
			}


			query = new Query();
			List<Project> projectList = GetUserAssigenedProjectList(req,res); 
			List<Booking> bookingList1 = mongoTemplate.find(query.addCriteria(Criteria.where("bookingstatus").ne("Cancel")), Booking.class);


			List<Booking> bookingList=new ArrayList<Booking>();
			Booking booking=new Booking();

			for(int i=0;i<projectList.size();i++)
			{

				for(int j=0;j<bookingList1.size();j++)
				{
					if(projectList.get(i).getProjectId().equals(bookingList1.get(j).getProjectId()))
					{

						booking=new Booking();

						booking.setBookingId(bookingList1.get(j).getBookingId());
						booking.setEnquiryId(bookingList1.get(j).getEnquiryId());
						booking.setBookingfirstname(bookingList1.get(j).getBookingfirstname());
						booking.setBookingmiddlename(bookingList1.get(j).getBookingmiddlename());
						booking.setBookinglastname(bookingList1.get(j).getBookinglastname());
						booking.setBookingaddress(bookingList1.get(j).getBookingaddress());
						booking.setBookingPincode(bookingList1.get(j).getBookingPincode());

						booking.setBookingEmail(bookingList1.get(j).getBookingEmail());
						booking.setBookingmobileNumber1(bookingList1.get(j).getBookingmobileNumber1());
						booking.setBookingmobileNumber2(bookingList1.get(j).getBookingmobileNumber2());
						booking.setBookingOccupation(bookingList1.get(j).getBookingOccupation());
						booking.setPurposeOfFlat(bookingList1.get(j).getPurposeOfFlat());

						booking.setProjectId(bookingList1.get(j).getProjectId());
						booking.setBuildingId(bookingList1.get(j).getBuildingId());
						booking.setWingId(bookingList1.get(j).getWingId());
						booking.setFloorId(bookingList1.get(j).getFloorId());
						booking.setFlatType(bookingList1.get(j).getFlatType());
						booking.setFlatId(bookingList1.get(j).getFlatId());
						booking.setFlatFacing(bookingList1.get(j).getFlatFacing());
						booking.setFlatareainSqFt(bookingList1.get(j).getFlatareainSqFt());
						booking.setFlatCostwithotfloorise(bookingList1.get(j).getFlatCostwithotfloorise());
						booking.setFloorRise(bookingList1.get(j).getFloorRise());
						booking.setFlatCost(bookingList1.get(j).getFlatCost());
						booking.setFlatbasicCost(bookingList1.get(j).getFlatbasicCost());
						booking.setParkingFloorId(bookingList1.get(j).getParkingFloorId());
						booking.setParkingZoneId(bookingList1.get(j).getParkingZoneId());
						booking.setAgentId(bookingList1.get(j).getAgentId());

						booking.setInfrastructureCharge(bookingList1.get(j).getInfrastructureCharge());
						booking.setAggreementValue1(bookingList1.get(j).getAggreementValue1());
						booking.setHandlingCharges(bookingList1.get(j).getHandlingCharges());
						booking.setStampDuty1(bookingList1.get(j).getStampDuty1());
						booking.setStampDutyPer(bookingList1.get(j).getStampDutyPer());
						booking.setRegistrationPer(bookingList1.get(j).getRegistrationPer());
						booking.setRegistrationCost1(bookingList1.get(j).getRegistrationCost1());
						booking.setGstCost(bookingList1.get(j).getGstCost());
						booking.setGstAmount1(bookingList1.get(j).getGstAmount1());
						booking.setGrandTotal1(bookingList1.get(j).getGrandTotal1());
						booking.setTds(bookingList1.get(j).getTds());
						booking.setBookingstatus(bookingList1.get(j).getBookingstatus());
						booking.setUserName(bookingList1.get(j).getUserName());
						booking.setCreationDate(bookingList1.get(j).getCreationDate());

						try {


							query = new Query();
							Project project=mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(bookingList1.get(j).getProjectId())), Project.class);
							booking.setProjectId(project.getProjectName());

							query = new Query();
							ProjectBuilding projectBuilding=mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(bookingList1.get(j).getBuildingId())),ProjectBuilding.class);
							booking.setBuildingId(projectBuilding.getBuildingName());
							query = new Query();
							ProjectWing projectWing=mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(bookingList1.get(j).getWingId())), ProjectWing.class);
							booking.setWingId(projectWing.getWingName());

							query = new Query();
							Flat flat=mongoTemplate.findOne(query.addCriteria(Criteria.where("flatId").is(bookingList1.get(j).getFlatId())), Flat.class);
							booking.setFlatNumber(flat.getFlatNumber());
						}catch (Exception e) {
							// TODO: handle exception
						}
						bookingList.add(booking);

					}

				}

			}

			//Query query = new Query();
			//List<Booking> bookingList = mongoTemplate.find(query.addCriteria(Criteria.where("bookingstatus").ne("Cancel")), Booking.class);

			model.addAttribute("bookingList", bookingList);
			return "AddCustomerPayment";

		}catch (Exception e) {
			return "login";
		}
	}



	@ResponseBody
	@RequestMapping("/SaveAlllPayment")
	public List<CustomerReceiptForm> SaveAlllPayment(@RequestParam("bookingId") String bookingId, @RequestParam("aggreementAmount") double aggreementAmount,@RequestParam("stampDutyAmount") double stampDutyAmount, @RequestParam("registrationAmount") double registrationAmount,@RequestParam("gstAmount") double gstAmount, @RequestParam("paymentMode") String paymentMode, @RequestParam("bankName") String bankName, @RequestParam("branchName") String branchName, @RequestParam("chequeNumber") String chequeNumber, @RequestParam("narration") String narration, HttpSession session)
	{

		Booking bookingList;
		bookingList = mongoTemplate.findOne(Query.query(Criteria.where("bookingId").is(bookingId)), Booking.class);

		Query query =new Query();
		List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(bookingList.getProjectId())), Project.class);

		query =new Query();
		List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(bookingList.getBuildingId())), ProjectBuilding.class);

		query =new Query();
		List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(bookingList.getWingId())), ProjectWing.class);

		String projectName=projectDetails.get(0).getProjectName();
		String buildingName=buildingDetails.get(0).getBuildingName();
		String wingName=wingDetails.get(0).getWingName();
		String one = "REC";
		one = one +"/"+bookingId+"/";
		String receiptCode="";

		Date todayDate = new Date();  

		CustomerReceiptForm customerreceiptform=new CustomerReceiptForm();
		Double paymentAmount;
		String paymentType;

		if(aggreementAmount!=0.0)
		{
			query = new Query();
			long receiptCount = mongoTemplate.count(query.addCriteria(Criteria.where("bookingId").is(bookingId)), CustomerReceiptForm.class);
			if(receiptCount<10)
			{
				receiptCode = one+"0"+(receiptCount+1);
			}
			else 
			{
				receiptCode = one+(receiptCount+1);
			}

			paymentType="Agreement";
			paymentAmount=aggreementAmount;
			customerreceiptform.setReceiptId(receiptCode);
			customerreceiptform.setBookingId(bookingId);
			customerreceiptform.setPaymentAmount(paymentAmount);
			customerreceiptform.setPaymentType(paymentType);
			customerreceiptform.setPaymentMode(paymentMode);
			customerreceiptform.setBankName(bankName);
			customerreceiptform.setBranchName(branchName);
			customerreceiptform.setChequeNumber(chequeNumber);
			customerreceiptform.setNarration(narration);
			customerreceiptform.setCreationDate(todayDate);
			customerreceiptform.setStatus("Not Deposited");
			customerreceiptformrepository.save(customerreceiptform);
		}

		if(stampDutyAmount!=0.0)
		{
			query = new Query();
			long receiptCount = mongoTemplate.count(query.addCriteria(Criteria.where("bookingId").is(bookingId)), CustomerReceiptForm.class);
			if(receiptCount<10)
			{
				receiptCode = one+"0"+(receiptCount+1);
			}
			else 
			{
				receiptCode = one+(receiptCount+1);
			}
			customerreceiptform=new CustomerReceiptForm();
			paymentType="Stamp Duty";
			paymentAmount=stampDutyAmount;
			customerreceiptform.setReceiptId(receiptCode);
			customerreceiptform.setBookingId(bookingId);
			customerreceiptform.setPaymentAmount(paymentAmount);
			customerreceiptform.setPaymentType(paymentType);
			customerreceiptform.setPaymentMode(paymentMode);
			customerreceiptform.setBankName(bankName);
			customerreceiptform.setBranchName(branchName);
			customerreceiptform.setChequeNumber(chequeNumber);
			customerreceiptform.setNarration(narration);
			customerreceiptform.setCreationDate(todayDate);
			customerreceiptform.setStatus("Not Deposited");
			customerreceiptformrepository.save(customerreceiptform);
		}
		if(registrationAmount!=0.0)
		{
			query = new Query();
			long receiptCount = mongoTemplate.count(query.addCriteria(Criteria.where("bookingId").is(bookingId)), CustomerReceiptForm.class);
			if(receiptCount<10)
			{
				receiptCode = one+"0"+(receiptCount+1);
			}
			else 
			{
				receiptCode = one+(receiptCount+1);
			}
			customerreceiptform=new CustomerReceiptForm();
			paymentType="Registration";
			paymentAmount=registrationAmount;
			customerreceiptform.setReceiptId(receiptCode);
			customerreceiptform.setBookingId(bookingId);
			customerreceiptform.setPaymentAmount(paymentAmount);
			customerreceiptform.setPaymentType(paymentType);
			customerreceiptform.setPaymentMode(paymentMode);
			customerreceiptform.setBankName(bankName);
			customerreceiptform.setBranchName(branchName);
			customerreceiptform.setChequeNumber(chequeNumber);
			customerreceiptform.setNarration(narration);
			customerreceiptform.setCreationDate(todayDate);
			customerreceiptform.setStatus("Not Deposited");
			customerreceiptformrepository.save(customerreceiptform);
		}
		if(gstAmount!=0.0)
		{
			query = new Query();
			long receiptCount = mongoTemplate.count(query.addCriteria(Criteria.where("bookingId").is(bookingId)), CustomerReceiptForm.class);
			if(receiptCount<10)
			{
				receiptCode = one+"0"+(receiptCount+1);
			}
			else 
			{
				receiptCode = one+(receiptCount+1);
			}
			customerreceiptform=new CustomerReceiptForm();
			paymentType="GST";
			paymentAmount=gstAmount;
			customerreceiptform.setReceiptId(receiptCode);
			customerreceiptform.setBookingId(bookingId);
			customerreceiptform.setPaymentAmount(paymentAmount);
			customerreceiptform.setPaymentType(paymentType);
			customerreceiptform.setPaymentMode(paymentMode);
			customerreceiptform.setBankName(bankName);
			customerreceiptform.setBranchName(branchName);
			customerreceiptform.setChequeNumber(chequeNumber);
			customerreceiptform.setNarration(narration);
			customerreceiptform.setCreationDate(todayDate);
			customerreceiptform.setStatus("Not Deposited");
			customerreceiptformrepository.save(customerreceiptform);
		}
		Double totalpayAggreement=0.0;
		Double totalpayStampDuty=0.0;
		Double totalpayRegistration=0.0;
		Double totalpaygstAmount=0.0;
		Query query4 = new Query();
		List<CustomerPaymentDetails> customerpaymentdetails = mongoTemplate.find(query4.addCriteria(Criteria.where("bookingId").is(bookingId)),CustomerPaymentDetails.class);
		if(customerpaymentdetails.size()!=0)
		{
			totalpayAggreement=customerpaymentdetails.get(0).getTotalpayAggreement()+aggreementAmount;
			totalpayStampDuty=customerpaymentdetails.get(0).getTotalpayStampDuty()+stampDutyAmount;
			totalpayRegistration=customerpaymentdetails.get(0).getTotalpayRegistration()+registrationAmount;
			totalpaygstAmount=customerpaymentdetails.get(0).getTotalpaygstAmount()+gstAmount;
			String amountStatus;
			if(bookingList.getAggreementValue1()==totalpayAggreement && bookingList.getStampDuty1()==totalpayStampDuty && bookingList.getRegistrationCost1()==totalpayRegistration && bookingList.getGstAmount1()==totalpaygstAmount)
			{
				amountStatus="Completed";
			}
			else
			{
				amountStatus="Pending";
			}
			try
			{
				customerpaymentdetails.get(0).setTotalpayAggreement(totalpayAggreement);
				customerpaymentdetails.get(0).setTotalpayStampDuty(totalpayStampDuty);
				customerpaymentdetails.get(0).setTotalpayRegistration(totalpayRegistration);
				customerpaymentdetails.get(0).setTotalpaygstAmount(totalpaygstAmount);
				customerpaymentdetails.get(0).setAmountStatus(amountStatus);
				customerpaymentdetails.get(0).setCreationDate(todayDate);
				customerpaymentdetails.get(0).setUpdateDate(todayDate);
				//customerpaymentdetails.getUserName();
				customerpaymentdetailsrepository.save(customerpaymentdetails);
			}
			catch(Exception ee)
			{
			}
		}
		else
		{
			String amountStatus;
			if(bookingList.getAggreementValue1()==aggreementAmount && bookingList.getStampDuty1()==stampDutyAmount && bookingList.getRegistrationCost1()==registrationAmount && bookingList.getGstAmount1()==gstAmount)
			{
				amountStatus="Completed";
			}
			else
			{
				amountStatus="Pending";
			}

			CustomerPaymentDetails customerpaymentdetails1=new CustomerPaymentDetails();
			customerpaymentdetails1.setId(customerpayAmountCode());
			customerpaymentdetails1.setBookingId(bookingId);
			customerpaymentdetails1.setTotalpayAggreement(aggreementAmount);
			customerpaymentdetails1.setTotalpayStampDuty(stampDutyAmount);
			customerpaymentdetails1.setTotalpayRegistration(registrationAmount);
			customerpaymentdetails1.setTotalpaygstAmount(gstAmount);
			customerpaymentdetails1.setAmountStatus(amountStatus);
			customerpaymentdetails1.setCreationDate(todayDate);
			customerpaymentdetails1.setUpdateDate(todayDate);
			//customerpaymentdetails.getUserName();
			customerpaymentdetailsrepository.save(customerpaymentdetails1);
		}

		return null;	
	}	
}

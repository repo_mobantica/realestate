package com.realestate.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.SubSupplierType;
import com.realestate.bean.SupplierType;
import com.realestate.repository.SubSupplierTypeRepository;
import com.realestate.repository.SupplierTypeRepository;

@Controller
@RequestMapping("/")
public class SubSupplierTypeController {

	@Autowired
	SubSupplierTypeRepository subsuppliertypeRepository;
	@Autowired
	SupplierTypeRepository suppliertypeRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	String subsuppliertypeCode;
	private String SubSupplierTypeCode()
	{
		long cnt  = subsuppliertypeRepository.count();
		if(cnt<10)
		{
			subsuppliertypeCode = "SS000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			subsuppliertypeCode = "SS00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			subsuppliertypeCode = "SS0"+(cnt+1);
		}
		else 
		{
			subsuppliertypeCode = "SS"+(cnt+1);
		}
		return subsuppliertypeCode;
	}

	@RequestMapping("/SubSupplierTypeMaster")
	public String supplierTypeMaster(ModelMap model)
	{
		try {
			List<SubSupplierType> subsuppliertypeList = subsuppliertypeRepository.findAll();
			List<SupplierType> suppliertypeList = suppliertypeRepository.findAll();
			Query query = new Query();
			for(int i=0;i<subsuppliertypeList.size();i++)
			{
				try
				{
					query = new Query();
					List<SupplierType> supplierDetails = mongoTemplate.find(query.addCriteria(Criteria.where("suppliertypeId").is(subsuppliertypeList.get(i).getSuppliertypeId())),SupplierType.class);
					subsuppliertypeList.get(i).setSuppliertypeId(supplierDetails.get(0).getSupplierType());
				}
				catch (Exception e) {
					// TODO: handle exception
				}

			}

			model.addAttribute("suppliertypeList",suppliertypeList);
			model.addAttribute("subsuppliertypeList",subsuppliertypeList);

			return "SubSupplierTypeMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/AddSubSupplierType")
	public String addSubSupplierType(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<SupplierType> suppliertypeList = suppliertypeRepository.findAll();
			model.addAttribute("suppliertypeList",suppliertypeList);
			model.addAttribute("subsuppliertypeCode",SubSupplierTypeCode());
			return "AddSubSupplierType";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping(value = "/AddSubSupplierType", method = RequestMethod.POST)
	public String subsuppliertypeSave(@ModelAttribute SubSupplierType subsuppliertype, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				subsuppliertype = new SubSupplierType(subsuppliertype.getSubsuppliertypeId(),subsuppliertype.getSubsupplierType(), subsuppliertype.getSuppliertypeId(),subsuppliertype.getCreationDate(), subsuppliertype.getUpdateDate(), subsuppliertype.getUserName());
				subsuppliertypeRepository.save(subsuppliertype);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}

			List<SupplierType> suppliertypeList = suppliertypeRepository.findAll();
			model.addAttribute("suppliertypeList",suppliertypeList);
			model.addAttribute("subsuppliertypeCode",SubSupplierTypeCode());
			return "AddSubSupplierType";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/EditSubSupplierType")
	public String EditSubSupplierType(@RequestParam("subsuppliertypeId") String subsuppliertypeId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<SubSupplierType> subsuppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subsuppliertypeId").is(subsuppliertypeId)),SubSupplierType.class);

			query = new Query();
			List<SupplierType> suppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("suppliertypeId").is(subsuppliertypeDetails.get(0).getSuppliertypeId())),SupplierType.class);

			List<SupplierType> suppliertypeList = suppliertypeRepository.findAll();
			model.addAttribute("supplierType",suppliertypeDetails.get(0).getSupplierType());
			model.addAttribute("suppliertypeList",suppliertypeList);
			model.addAttribute("subsuppliertypeDetails",subsuppliertypeDetails);
			return "EditSubSupplierType";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/EditSubSupplierType", method=RequestMethod.POST)
	public String EditSupplierTypePostMethod(@ModelAttribute SubSupplierType subsuppliertype, ModelMap model)
	{
		try {
			try
			{
				subsuppliertype = new SubSupplierType(subsuppliertype.getSubsuppliertypeId(),subsuppliertype.getSubsupplierType(), subsuppliertype.getSuppliertypeId(),subsuppliertype.getCreationDate(), subsuppliertype.getUpdateDate(), subsuppliertype.getUserName());
				subsuppliertypeRepository.save(subsuppliertype);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}

			List<SubSupplierType> subsuppliertypeList = subsuppliertypeRepository.findAll();
			List<SupplierType> suppliertypeList = suppliertypeRepository.findAll();
			model.addAttribute("suppliertypeList",suppliertypeList);
			model.addAttribute("subsuppliertypeList",subsuppliertypeList);

			return "SubSupplierTypeMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/getSubSupplierTypeList")
	public List<SubSupplierType> getSubSupplierTypeList(@RequestParam("suppliertypeId") String suppliertypeId)
	{
		Query query = new Query();

		List<SubSupplierType> subsuppliertypeList = mongoTemplate.find(query.addCriteria(Criteria.where("suppliertypeId").is(suppliertypeId)),SubSupplierType.class);
		for(int i=0;i<subsuppliertypeList.size();i++)
		{
			try
			{
				query = new Query();
				List<SupplierType> supplierDetails = mongoTemplate.find(query.addCriteria(Criteria.where("suppliertypeId").is(subsuppliertypeList.get(i).getSuppliertypeId())),SupplierType.class);
				subsuppliertypeList.get(i).setSuppliertypeId(supplierDetails.get(0).getSupplierType());
			}
			catch (Exception e) {
				// TODO: handle exception
			}

		}
		return subsuppliertypeList;
	}
}

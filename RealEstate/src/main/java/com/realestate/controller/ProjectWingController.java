package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Login;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.CompanyRepository;
import com.realestate.repository.ProjectBuildingRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.ProjectWingRepository;

@Controller
@RequestMapping("/")
public class ProjectWingController {
	@Autowired
	ProjectBuildingRepository projectBuildingRepository;
	@Autowired
	ProjectWingRepository projectWingRepository;
	@Autowired
	CompanyRepository companyRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	String wingCode;
	private String WingCode()
	{
		long wingCount = projectWingRepository.count();

		if(wingCount<10)
		{
			wingCode = "WG000"+(wingCount+1);
		}
		else if((wingCount>=10) && (wingCount<100))
		{
			wingCode = "WG00"+(wingCount+1);
		}
		else if((wingCount>=100) && (wingCount<1000))
		{
			wingCode = "WG0"+(wingCount+1);
		}
		else 
		{
			wingCode = "WG"+(wingCount+1);
		}

		return wingCode;
	}


	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}
	//Request Mapping For Add Project Wings
	@RequestMapping("/AddProjectWing")
	public String addPrijectWing(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			model.addAttribute("wingCode",WingCode());

			List<Project> projectList = GetUserAssigenedProjectList(req,res); 
			model.addAttribute("projectList",projectList);
			return "AddProjectWing";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/AddProjectWing",method=RequestMethod.POST)
	public String projectWingSave(@ModelAttribute ProjectWing projectWing, Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				projectWing = new ProjectWing(projectWing.getWingId(),projectWing.getWingName().toUpperCase(),projectWing.getBuildingId(),projectWing.getProjectId(),
						projectWing.getInfrastructureCharges(),projectWing.getMaintenanceCharges(),projectWing.getHandlingCharges(),
						projectWing.getNoofBasement(),projectWing.getNoofStilts(),projectWing.getNoofPodiums(),projectWing.getNoofFloors(),projectWing.getTotal(),projectWing.getWingStatus(),projectWing.getCreationDate(),projectWing.getUpdateDate(),projectWing.getUserName());
				projectWingRepository.save(projectWing);
				model.addAttribute("wingStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("wingStatus","Fail");
			}


			List<Project> projectList = GetUserAssigenedProjectList(req,res); 
			model.addAttribute("projectList",projectList);
			model.addAttribute("wingCode",WingCode());

			return "AddProjectWing";

		}catch (Exception e) {
			return "login";
		}
	}
	@ResponseBody
	@RequestMapping("/getuniqueWingList")
	public List<ProjectWing> AllUniqueWingList(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		List<ProjectWing> wingList= projectWingRepository.findAll();

		return wingList;
	}
	//Request Mapping For Add Project Unit
	@RequestMapping("/ProjectWingMaster")
	public String PrijectWingMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<Project> projectList = GetUserAssigenedProjectList(req,res);
			List<ProjectWing> wingList = projectWingRepository.findAll();

			Query query = new Query();

			for(int i=0;i<wingList.size();i++)
			{
				try {
					query = new Query();
					List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(wingList.get(i).getProjectId())), Project.class);
					wingList.get(i).setProjectId(projectDetails.get(0).getProjectName());
					query = new Query();
					List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(wingList.get(i).getBuildingId())), ProjectBuilding.class);
					wingList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}

			model.addAttribute("projectList",projectList);
			model.addAttribute("wingList",wingList);
			return "ProjectWingMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping(value="/getProjectwiseWingList",method=RequestMethod.POST)
	public List<ProjectWing> projectwisewingList(@RequestParam("projectId") String projectId, HttpSession session)
	{

		List<ProjectWing> wingList= new ArrayList<ProjectWing>();

		Query query = new Query();
		wingList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)), ProjectWing.class);

		for(int i=0;i<wingList.size();i++)
		{
			try {
				query = new Query();
				List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(wingList.get(i).getProjectId())), Project.class);
				wingList.get(i).setProjectId(projectDetails.get(0).getProjectName());
				query = new Query();
				List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(wingList.get(i).getBuildingId())), ProjectBuilding.class);
				wingList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		return wingList;
	}
	@ResponseBody
	@RequestMapping(value="/getBuildingwiseWingList",method=RequestMethod.POST)
	public List<ProjectWing> buildingwisewingList(@RequestParam("buildingId") String buildingId, @RequestParam("projectId") String projectId, HttpSession session)
	{

		List<ProjectWing> wingList= new ArrayList<ProjectWing>();

		Query query = new Query();
		wingList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId)), ProjectWing.class);

		for(int i=0;i<wingList.size();i++)
		{
			try {
				query = new Query();
				List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(wingList.get(i).getProjectId())), Project.class);
				wingList.get(i).setProjectId(projectDetails.get(0).getProjectName());
				query = new Query();
				List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(wingList.get(i).getBuildingId())), ProjectBuilding.class);
				wingList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}
		return wingList;
	}

	@ResponseBody
	@RequestMapping("/getSearchwiseWingList")
	public List<ProjectWing> SearchNameWiseWingList(@RequestParam("wingName") String wingName)
	{
		Query query = new Query();
		List<ProjectWing> wingList = mongoTemplate.find(query.addCriteria(Criteria.where("wingName").regex("^"+wingName+".*","i")),ProjectWing.class);

		return wingList;
	}

	@RequestMapping("/EditProjectWing")
	public String EditProjectWing(@RequestParam("wingId") String wingId, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			Query query = new Query();
			List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(wingId)),ProjectWing.class);
			query = new Query();
			List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(wingDetails.get(0).getBuildingId())),ProjectBuilding.class);
			query = new Query();
			List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(wingDetails.get(0).getProjectId())),Project.class);

			List<Project> projectList = GetUserAssigenedProjectList(req,res); 
			model.addAttribute("projectList",projectList);
			model.addAttribute("projectName", projectDetails.get(0).getProjectName());
			model.addAttribute("buildingName", buildingDetails.get(0).getBuildingName());
			model.addAttribute("wingDetails", wingDetails);

			return "EditProjectWing";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/EditProjectWing", method=RequestMethod.POST)
	public String EditProjectWingPostMethod(@ModelAttribute ProjectWing projectWing, Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				projectWing = new ProjectWing(projectWing.getWingId(),projectWing.getWingName().toUpperCase(),projectWing.getBuildingId(),projectWing.getProjectId(),
						projectWing.getInfrastructureCharges(),projectWing.getMaintenanceCharges(),projectWing.getHandlingCharges(),
						projectWing.getNoofBasement(),projectWing.getNoofStilts(),projectWing.getNoofPodiums(),projectWing.getNoofFloors(),projectWing.getTotal(),projectWing.getWingStatus(),projectWing.getCreationDate(),projectWing.getUpdateDate(),projectWing.getUserName());
				projectWingRepository.save(projectWing);

				model.addAttribute("wingStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("wingStatus","Fail");
			}

			List<ProjectWing> wingList = projectWingRepository.findAll();

			List<Project> projectList = GetUserAssigenedProjectList(req,res); 
			model.addAttribute("projectList",projectList);
			model.addAttribute("wingList",wingList);
			return "ProjectWingMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/getWingwiseWingList")
	public List<ProjectWing> getWingwiseWingList(@RequestParam("projectId") String projectId, @RequestParam("buildingId") String buildingId, @RequestParam("wingId") String wingId)
	{

		Query query = new Query();
		List<ProjectWing> wingList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId)), ProjectWing.class);

		for(int i=0;i<wingList.size();i++)
		{
			try {
				query = new Query();
				List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(wingList.get(i).getProjectId())), Project.class);
				wingList.get(i).setProjectId(projectDetails.get(0).getProjectName());
				query = new Query();
				List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(wingList.get(i).getBuildingId())), ProjectBuilding.class);
				wingList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		return wingList;
	}

	@ResponseBody
	@RequestMapping(value="/getprojectwingList",method=RequestMethod.POST)
	public List<ProjectWing> wingList(@RequestParam("buildingId") String buildingId, @RequestParam("projectId") String projectId, HttpSession session)
	{
		List<ProjectWing> projectwingList= new ArrayList<ProjectWing>();

		Query query = new Query();
		projectwingList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId)), ProjectWing.class);

		for(int i=0;i<projectwingList.size();i++)
		{
			try {
				query = new Query();
				List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectwingList.get(i).getProjectId())), Project.class);
				projectwingList.get(i).setProjectId(projectDetails.get(0).getProjectName());
				query = new Query();
				List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(projectwingList.get(i).getBuildingId())), ProjectBuilding.class);
				projectwingList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}
		return projectwingList;
	}
}

package com.realestate.controller;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Agent;
import com.realestate.bean.Booking;
import com.realestate.bean.City;
import com.realestate.bean.Country;
import com.realestate.bean.Flat;
import com.realestate.bean.Floor;
import com.realestate.bean.LocationArea;
import com.realestate.bean.ParkingZone;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.State;
import com.realestate.bean.AgentPayment;
import com.realestate.repository.AgentRepository;
import com.realestate.repository.CityRepository;
import com.realestate.repository.CountryRepository;
import com.realestate.repository.LocationAreaRepository;
import com.realestate.repository.StateRepository;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;

@Controller
@RequestMapping("/")
public class AgentPaymentController
{
	@Autowired
	LocationAreaRepository locationAreaRepository;
	@Autowired
	CityRepository cityRepository;
	@Autowired
	StateRepository stateRepository;
	@Autowired
	CountryRepository countryRepository;
	@Autowired
	AgentRepository agentRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	@RequestMapping("/AgentPayment")
	public String AgentPayment(Model model, HttpServletRequest req, HttpServletResponse res)
	{
		List<Agent> agentList = agentRepository.findAll(); 

		model.addAttribute("agentList",agentList);

		return "AgentPayment";
	}

	@ResponseBody
	@RequestMapping("/getAgentPayment")
	public List<Booking> getAgentPayment(@RequestParam("agentId") String agentId)
	{
		Query query =new Query();
		List<Booking> bookingList = mongoTemplate.find(query.addCriteria(Criteria.where("agentId").is(agentId).and("bookingstatus").ne("Cancel")), Booking.class);

		query =new Query();
		List<Agent> agentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("agentId").is(agentId)), Agent.class);
		Double agentPer=agentDetails.get(0).getBrokeragePercentage();

		Double agentAmount=0.0;
		Double agentPaidPayment=0.0;
		String agentPaymentStatus;

		for(int i=0;i<bookingList.size();i++)
		{
			agentPaidPayment=0.0;
			if(agentDetails.get(0).getCommissionBased().equalsIgnoreCase("onAgreementValue"))
			{
				agentAmount=(bookingList.get(i).getFlatbasicCost()/100)*agentDetails.get(0).getBrokeragePercentage();
			}
			else if(agentDetails.get(0).getCommissionBased().equalsIgnoreCase("onFlatBasicAmount"))
			{
				agentAmount=(bookingList.get(i).getAggreementValue1()/100)*agentDetails.get(0).getBrokeragePercentage();
			}
			else
			{
				agentAmount=(bookingList.get(i).getGrandTotal1()/100)*agentDetails.get(0).getBrokeragePercentage();
			}

			query =new Query();
			List<AgentPayment> agentPayemtDetails = mongoTemplate.find(query.addCriteria(Criteria.where("agentId").is(agentId).and("bookingId").is(bookingList.get(i).getBookingId())), AgentPayment.class);
			agentPaymentStatus="";
			if(agentPayemtDetails.size()==0)
			{
				agentPaymentStatus="Pending";
			}
			else
			{
				for(int j=0;j<agentPayemtDetails.size();j++)
				{
					agentPaidPayment=agentPaidPayment+agentPayemtDetails.get(j).getPaymentAmount();
				}

				if(Double.compare(agentAmount, agentPaidPayment)==0)
				{
					agentPaymentStatus="Payment Clear";
				}
				else
				{
					agentPaymentStatus="Pending";
				}
			}


			bookingList.get(i).setAgentId(agentDetails.get(0).getAgentfirmName());
			bookingList.get(i).setAgentPer(agentPer);
			bookingList.get(i).setAgentAmount(agentAmount);
			bookingList.get(i).setAgentPaidPayment(agentPaidPayment);
			bookingList.get(i).setAgentPaymentStatus(agentPaymentStatus);
		}

		return bookingList;
	}

	@ResponseBody
	@RequestMapping("/AgentPaymentReport")
	public ResponseEntity<byte[]> PrintWingWiseParkingReport(@RequestParam("agentId") String agentId, HttpServletRequest req, ModelMap model, HttpServletResponse response)
	{
		try 
		{	
			JasperPrint print;
			Query query = new Query();
			HashMap jmap = new HashMap();
			Collection c = new ArrayList();

			query =new Query();
			List<Booking> bookingList = mongoTemplate.find(query.addCriteria(Criteria.where("agentId").is(agentId).and("bookingstatus").ne("Cancel")), Booking.class);

			query =new Query();
			List<Agent> agentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("agentId").is(agentId)), Agent.class);

			Double agentPer=agentDetails.get(0).getBrokeragePercentage();

			Double agentAmount=0.0;
			Double agentPaidPayment=0.0;
			String agentPaymentStatus;
			long paidAmtTotal=0;

			if(bookingList.size()!=0)
			{
				for(int i=0;i<bookingList.size();i++)
				{
					query = new Query();
					jmap = new HashMap();

					agentPaidPayment=0.0;

					jmap.put("srno", "" + (i + 1));
					jmap.put("bookingId", ""+bookingList.get(i).getBookingId());
					jmap.put("custName", ""+bookingList.get(i).getBookingfirstname()+" "+bookingList.get(i).getBookingmiddlename()+" "+bookingList.get(i).getBookinglastname());
					jmap.put("flatBasicAmt", ""+bookingList.get(i).getFlatbasicCost().longValue());
					jmap.put("agentPer", ""+agentPer);

					if(agentDetails.get(0).getCommissionBased().equalsIgnoreCase("onAgreementValue"))
					{
						agentAmount=(bookingList.get(i).getFlatbasicCost()/100)*agentDetails.get(0).getBrokeragePercentage();
					}
					else if(agentDetails.get(0).getCommissionBased().equalsIgnoreCase("onFlatBasicAmount"))
					{
						agentAmount=(bookingList.get(i).getAggreementValue1()/100)*agentDetails.get(0).getBrokeragePercentage();
					}
					else
					{
						agentAmount=(bookingList.get(i).getGrandTotal1()/100)*agentDetails.get(0).getBrokeragePercentage();
					}

					jmap.put("agentAmt", ""+agentAmount);

					query =new Query();
					List<AgentPayment> agentPayemtDetails = mongoTemplate.find(query.addCriteria(Criteria.where("agentId").is(agentId).and("bookingId").is(bookingList.get(i).getBookingId())), AgentPayment.class);

					if(agentPayemtDetails.size()==0)
					{
						agentPaymentStatus="Pending";
					}
					else
					{
						for(int j=0;j<agentPayemtDetails.size();j++)
						{
							agentPaidPayment=agentPaidPayment+agentPayemtDetails.get(j).getPaymentAmount();
						}
						if(agentAmount==agentPaidPayment)
						{
							agentPaymentStatus="Payment Clear";
						}
						else
						{
							agentPaymentStatus="Pending";
						}
					}

					jmap.put("paidAmt", ""+agentPaidPayment);
					paidAmtTotal += agentPaidPayment.longValue();

					jmap.put("paymentStatus", ""+agentPaymentStatus);

					c.add(jmap);
					jmap = null;
				}
			}
			else
			{
				jmap = new HashMap();

				jmap.put("srno", "N.A.");
				jmap.put("bookingId", "N.A.");
				jmap.put("custName", "N.A.");
				jmap.put("flatBasicAmt", "N.A.");
				jmap.put("agentPer", "N.A.");
				jmap.put("agentAmt", "N.A.");
				jmap.put("paidAmt", "N.A.");
				jmap.put("paymentStatus", "N.A.");

				c.add(jmap);
				jmap = null;
			}

			JRDataSource dataSource = new JRMapCollectionDataSource(c);
			Map<String, Object> parameterMap = new HashMap<String, Object>();

			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			String strDate = formatter.format(date);

			/* 1 */ parameterMap.put("printDate", "" + strDate);
			/* 2 */ parameterMap.put("agentName", ""+agentDetails.get(0).getAgentfirmName());
			/* 3 */ parameterMap.put("firmAddress", ""+agentDetails.get(0).getAgentfirmAddress()+", "+agentDetails.get(0).getAgentPincode());
			/* 4 */ parameterMap.put("paidAmtTotal", ""+paidAmtTotal);

			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/AgentPaymentReport.jasper");

			print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(print, baos);

			byte[] contents = baos.toByteArray();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			String filename = "Agent Payment Report.pdf";

			JasperExportManager.exportReportToPdfStream(print, baos);

			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
			ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
			response.setHeader("Content-Disposition", "inline; filename=" + filename);
			return resp;
		} 
		catch (Exception ex) 
		{
			return null;
		}
	}
}

package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Bank;
import com.realestate.bean.BankEmployees;
import com.realestate.bean.BankName;
import com.realestate.bean.City;
import com.realestate.bean.Country;
import com.realestate.bean.Designation;
import com.realestate.bean.LocationArea;
import com.realestate.bean.State;
import com.realestate.repository.BankEmployeesRepository;
import com.realestate.repository.BankNameRepository;
import com.realestate.repository.BankRepository;
import com.realestate.repository.CityRepository;
import com.realestate.repository.CountryRepository;
import com.realestate.repository.DesignationRepository;
import com.realestate.repository.LocationAreaRepository;
import com.realestate.repository.StateRepository;

@Controller
@RequestMapping("/")
public class BankController 
{
	@Autowired
	LocationAreaRepository locationAreaRepository;
	@Autowired
	CityRepository cityRepository;
	@Autowired
	StateRepository stateRepository;
	@Autowired
	BankRepository bankRepository;
	@Autowired
	CountryRepository countryRepository;
	@Autowired
	DesignationRepository designationRepository;
	@Autowired
	BankNameRepository banknameRepository;
	@Autowired
	BankEmployeesRepository bankEmployeesRepository;
	@Autowired
	MongoTemplate mongoTemplate;


	//Code geration for bank code
	String bankCode;
	private String BankCode()
	{
		long bankCount = bankRepository.count();

		if(bankCount<10)
		{
			bankCode = "BK000"+(bankCount+1);
		}
		else if((bankCount>=10) && (bankCount<100))
		{
			bankCode = "BK00"+(bankCount+1);
		}
		else if((bankCount>=100) && (bankCount<1000))
		{
			bankCode = "BK0"+(bankCount+1);
		}
		else if(bankCount>=1000)
		{
			bankCode = "BK"+(bankCount+1);
		}

		return bankCode;
	}

	String bankNameCode;
	private String BankNameCode()
	{
		long bankNameCount = banknameRepository.count();

		if(bankNameCount<10)
		{
			bankNameCode = "BN000"+(bankNameCount+1);
		}
		else if((bankNameCount>=10) && (bankNameCount<100))
		{
			bankNameCode = "BN00"+(bankNameCount+1);
		}
		else if((bankNameCount>=100) && (bankNameCount<1000))
		{
			bankNameCode = "BN0"+(bankNameCount+1);
		}
		else if(bankNameCount>1000)
		{
			bankNameCode = "BN"+(bankNameCount+1);
		}

		return bankNameCode;
	}
	//code for getting all country name
	private List<Country> findAllCountryName()
	{
		List<Country> countryList;

		countryList = countryRepository.findAll(new Sort(Sort.Direction.ASC,"countryId"));
		return countryList;
	}

	//code for retriving all designations
	private List<Designation> getAllDesignations()
	{
		List<Designation> designationList;

		designationList = designationRepository.findAll(new Sort(Sort.Direction.ASC,"designationName"));
		return designationList;
	}
	//Request Mapping For Add Bank Name
	@RequestMapping("/AddBankName")
	public String AddBankName(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			model.addAttribute("bankNameCode", BankNameCode());
			return "AddBankName";

		}catch (Exception e) {
			return "login";
		}
	}
	@RequestMapping(value="/AddBankName", method=RequestMethod.POST)
	public String BankNameSave(@ModelAttribute BankName bankname, Model model)
	{
		try {
			try
			{
				bankname = new BankName(bankname.getBankNameId(),bankname.getBankName(),bankname.getBankType(),bankname.getCreationDate(),bankname.getUpdateDate(),bankname.getUserName());
				banknameRepository.save(bankname);
				model.addAttribute("bankStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("bankStatus","Fail");
			}

			model.addAttribute("countryList", findAllCountryName());
			model.addAttribute("bankCode", BankCode());
			model.addAttribute("designationList", getAllDesignations());
			return "AddBank";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/AddBank")
	public String addBank(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			try
			{
				Query query = new Query();

				Query query1 = new Query();

				model.addAttribute("bankCode", BankCode());
				model.addAttribute("countryList", findAllCountryName());
				model.addAttribute("designationList", getAllDesignations());

				String bankCode = BankCode();
				List<BankEmployees> employeeList = mongoTemplate.find(query.addCriteria(Criteria.where("bankId").is(bankCode)), BankEmployees.class);

				if(employeeList.size()!=0)
				{
					mongoTemplate.remove(query1.addCriteria(Criteria.where("bankId").is(bankCode)), BankEmployees.class);	
				}
			}
			catch(Exception e)
			{

			}

			return "AddBank";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/AddBank", method=RequestMethod.POST)
	public String bankSave(@ModelAttribute Bank bank, Model model)
	{
		
		try {
			try
			{
				System.out.println("bank.getBankId()"+bank.getBankId());
				System.out.println("bank.getBankId()"+bank.getBankName());
				bank = new Bank(bank.getBankId(),bank.getBankName(),bank.getBranchName(),bank.getBankAddress(),bank.getCountryId(),bank.getStateId(),bank.getCityId(),bank.getLocationareaId(),bank.getAreaPincode(),bank.getBankPhoneno(),bank.getBankifscCode(),bank.getApfnumber(),bank.getCreationDate(),bank.getUpdateDate(),bank.getUserName());
				bankRepository.save(bank);
				model.addAttribute("bankStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				de.printStackTrace();
				model.addAttribute("bankStatus","Fail");
			}

			model.addAttribute("countryList", findAllCountryName());
			model.addAttribute("bankCode", BankCode());
			model.addAttribute("designationList", getAllDesignations());
			return "AddBank";

		}catch (Exception e) {
			return "login";
		}
	}

	//Request Mapping For  Bank Master
	@RequestMapping("/BankMaster")
	public String BankMaster(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<Bank> bankList = bankRepository.findAll(); 

			List<LocationArea> locationareaList= locationAreaRepository.findAll();
			List<City> cityList = cityRepository.findAll();
			List<State> stateList= stateRepository.findAll();
			List<Country> countryList= countryRepository.findAll();

			int i,j;
			for(i=0;i<bankList.size();i++)
			{
				try 
				{
					for(j=0;j<countryList.size();j++)
					{
						if(bankList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
						{
							bankList.get(i).setCountryId(countryList.get(j).getCountryName());
							break;
						}
					}

					for(j=0;j<stateList.size();j++)
					{
						if(bankList.get(i).getStateId().equals(stateList.get(j).getStateId()))
						{
							bankList.get(i).setStateId(stateList.get(j).getStateName());
							break;
						}
					}

					for(j=0;j<cityList.size();j++)
					{
						if(bankList.get(i).getCityId().equals(cityList.get(j).getCityId()))
						{
							bankList.get(i).setCityId(cityList.get(j).getCityName());
							break;
						}
					}

					for(j=0;j<locationareaList.size();j++)
					{
						if(bankList.get(i).getLocationareaId().equals(locationareaList.get(j).getLocationareaId()))
						{
							bankList.get(i).setLocationareaId(locationareaList.get(j).getLocationareaName());
							break;
						}
					}

				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}

			model.addAttribute("countryList", findAllCountryName());
			model.addAttribute("bankList",bankList);
			return "BankMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping("/SearchBankNameOrIFCSWiseList")
	public List<Bank> NameOrIfscWiseList(@RequestParam("bankNameorIfSCcode") String bankNameorIfSCcode)
	{
		Query query = new Query();

		List<Bank> bankList = mongoTemplate.find(query.addCriteria(Criteria.where("bankName").regex("^"+bankNameorIfSCcode+".*","i")), Bank.class);
		List<LocationArea> locationareaList= locationAreaRepository.findAll();
		List<City> cityList = cityRepository.findAll();
		List<State> stateList= stateRepository.findAll();
		List<Country> countryList= countryRepository.findAll();

		int i,j;
		for(i=0;i<bankList.size();i++)
		{
			try 
			{
				for(j=0;j<countryList.size();j++)
				{
					if(bankList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
					{
						bankList.get(i).setCountryId(countryList.get(j).getCountryName());
						break;
					}
				}

				for(j=0;j<stateList.size();j++)
				{
					if(bankList.get(i).getStateId().equals(stateList.get(j).getStateId()))
					{
						bankList.get(i).setStateId(stateList.get(j).getStateName());
						break;
					}
				}

				for(j=0;j<cityList.size();j++)
				{
					if(bankList.get(i).getCityId().equals(cityList.get(j).getCityId()))
					{
						bankList.get(i).setCityId(cityList.get(j).getCityName());
						break;
					}
				}

				for(j=0;j<locationareaList.size();j++)
				{
					if(bankList.get(i).getLocationareaId().equals(locationareaList.get(j).getLocationareaId()))
					{
						bankList.get(i).setLocationareaId(locationareaList.get(j).getLocationareaName());
						break;
					}
				}

			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		return bankList;
	}

	//to Retive All Bank in Bank Master by country name
	@ResponseBody
	@RequestMapping(value="/getCountryBankList",method=RequestMethod.POST)
	public List<Bank> CountryWiseBankList(@RequestParam("countryId") String countryId, HttpSession session)
	{
		List<Bank> bankList= new ArrayList<Bank>();

		Query query = new Query();
		bankList = mongoTemplate.find(query.addCriteria(Criteria.where("countryId").is(countryId)), Bank.class);

		List<LocationArea> locationareaList= locationAreaRepository.findAll();
		List<City> cityList = cityRepository.findAll();
		List<State> stateList= stateRepository.findAll();
		List<Country> countryList= countryRepository.findAll();

		int i,j;
		for(i=0;i<bankList.size();i++)
		{
			try 
			{
				for(j=0;j<countryList.size();j++)
				{
					if(bankList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
					{
						bankList.get(i).setCountryId(countryList.get(j).getCountryName());
						break;
					}
				}

				for(j=0;j<stateList.size();j++)
				{
					if(bankList.get(i).getStateId().equals(stateList.get(j).getStateId()))
					{
						bankList.get(i).setStateId(stateList.get(j).getStateName());
						break;
					}
				}

				for(j=0;j<cityList.size();j++)
				{
					if(bankList.get(i).getCityId().equals(cityList.get(j).getCityId()))
					{
						bankList.get(i).setCityId(cityList.get(j).getCityName());
						break;
					}
				}

				for(j=0;j<locationareaList.size();j++)
				{
					if(bankList.get(i).getLocationareaId().equals(locationareaList.get(j).getLocationareaId()))
					{
						bankList.get(i).setLocationareaId(locationareaList.get(j).getLocationareaName());
						break;
					}
				}

			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		return bankList;
	}

	//to Retive All Bank in Bank Master by State name
	@ResponseBody
	@RequestMapping(value="/getCityWiseBankList",method=RequestMethod.POST)
	public List<Bank> BankListByState(@RequestParam("stateId") String stateId, @RequestParam("countryId") String countryId, HttpSession session)
	{
		List<Bank> bankList= new ArrayList<Bank>();

		Query query = new Query();
		bankList = mongoTemplate.find(query.addCriteria(Criteria.where("countryId").is(countryId).and("stateId").is(stateId)), Bank.class);

		List<LocationArea> locationareaList= locationAreaRepository.findAll();
		List<City> cityList = cityRepository.findAll();
		List<State> stateList= stateRepository.findAll();
		List<Country> countryList= countryRepository.findAll();

		int i,j;
		for(i=0;i<bankList.size();i++)
		{
			try 
			{
				for(j=0;j<countryList.size();j++)
				{
					if(bankList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
					{
						bankList.get(i).setCountryId(countryList.get(j).getCountryName());
						break;
					}
				}

				for(j=0;j<stateList.size();j++)
				{
					if(bankList.get(i).getStateId().equals(stateList.get(j).getStateId()))
					{
						bankList.get(i).setStateId(stateList.get(j).getStateName());
						break;
					}
				}

				for(j=0;j<cityList.size();j++)
				{
					if(bankList.get(i).getCityId().equals(cityList.get(j).getCityId()))
					{
						bankList.get(i).setCityId(cityList.get(j).getCityName());
						break;
					}
				}

				for(j=0;j<locationareaList.size();j++)
				{
					if(bankList.get(i).getLocationareaId().equals(locationareaList.get(j).getLocationareaId()))
					{
						bankList.get(i).setLocationareaId(locationareaList.get(j).getLocationareaName());
						break;
					}
				}

			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		return bankList;
	}

	//to Retive All Bank in Bank Master by city name
	@ResponseBody
	@RequestMapping(value="/getLocationAreaWiseBankList",method=RequestMethod.POST)
	public List<Bank> BankListByCity(@RequestParam("cityId") String cityId, @RequestParam("stateId") String stateId, @RequestParam("countryId") String countryId, HttpSession session)
	{
		List<Bank> bankList= new ArrayList<Bank>();

		Query query = new Query();
		bankList = mongoTemplate.find(query.addCriteria(Criteria.where("countryId").is(countryId).and("stateId").is(stateId).and("cityId").is(cityId)), Bank.class);

		List<LocationArea> locationareaList= locationAreaRepository.findAll();
		List<City> cityList = cityRepository.findAll();
		List<State> stateList= stateRepository.findAll();
		List<Country> countryList= countryRepository.findAll();

		int i,j;
		for(i=0;i<bankList.size();i++)
		{
			try 
			{
				for(j=0;j<countryList.size();j++)
				{
					if(bankList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
					{
						bankList.get(i).setCountryId(countryList.get(j).getCountryName());
						break;
					}
				}

				for(j=0;j<stateList.size();j++)
				{
					if(bankList.get(i).getStateId().equals(stateList.get(j).getStateId()))
					{
						bankList.get(i).setStateId(stateList.get(j).getStateName());
						break;
					}
				}

				for(j=0;j<cityList.size();j++)
				{
					if(bankList.get(i).getCityId().equals(cityList.get(j).getCityId()))
					{
						bankList.get(i).setCityId(cityList.get(j).getCityName());
						break;
					}
				}

				for(j=0;j<locationareaList.size();j++)
				{
					if(bankList.get(i).getLocationareaId().equals(locationareaList.get(j).getLocationareaId()))
					{
						bankList.get(i).setLocationareaId(locationareaList.get(j).getLocationareaName());
						break;
					}
				}

			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		return bankList;
	}

	//to Retive All Bank in Bank Master by city name
	@ResponseBody
	@RequestMapping(value="/getBankList",method=RequestMethod.POST)
	public List<Bank> BankListByLocation(@RequestParam("locationareaId") String locationareaId, @RequestParam("cityId") String cityId, @RequestParam("stateId") String stateId, @RequestParam("countryId") String countryId, HttpSession session)
	{
		List<Bank> bankList= new ArrayList<Bank>();

		Query query = new Query();
		bankList = mongoTemplate.find(query.addCriteria(Criteria.where("countryId").is(countryId).and("stateId").is(stateId).and("cityId").is(cityId).and("locationareaId").is(locationareaId)), Bank.class);

		List<LocationArea> locationareaList= locationAreaRepository.findAll();
		List<City> cityList = cityRepository.findAll();
		List<State> stateList= stateRepository.findAll();
		List<Country> countryList= countryRepository.findAll();

		int i,j;
		for(i=0;i<bankList.size();i++)
		{
			try 
			{
				for(j=0;j<countryList.size();j++)
				{
					if(bankList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
					{
						bankList.get(i).setCountryId(countryList.get(j).getCountryName());
						break;
					}
				}

				for(j=0;j<stateList.size();j++)
				{
					if(bankList.get(i).getStateId().equals(stateList.get(j).getStateId()))
					{
						bankList.get(i).setStateId(stateList.get(j).getStateName());
						break;
					}
				}

				for(j=0;j<cityList.size();j++)
				{
					if(bankList.get(i).getCityId().equals(cityList.get(j).getCityId()))
					{
						bankList.get(i).setCityId(cityList.get(j).getCityName());
						break;
					}
				}

				for(j=0;j<locationareaList.size();j++)
				{
					if(bankList.get(i).getLocationareaId().equals(locationareaList.get(j).getLocationareaId()))
					{
						bankList.get(i).setLocationareaId(locationareaList.get(j).getLocationareaName());
						break;
					}
				}

			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		return bankList;
	}
	@ResponseBody
	@RequestMapping("/getAllBankList")
	public List<BankName> getAllBankList(@RequestParam("bankType") String bankType)
	{
		Query query = new Query();
		List<BankName> bankNameList = mongoTemplate.find(query.addCriteria(Criteria.where("bankType").is(bankType)), BankName.class);
		return bankNameList;
	}

	@ResponseBody
	@RequestMapping("/getBranchList")
	public List<Bank> getBranchList(@RequestParam("bankName") String bankName, HttpSession session)
	{
		Query query = new Query();
		List<Bank> bankbranchList = mongoTemplate.find(query.addCriteria(Criteria.where("bankName").is(bankName)), Bank.class);

		return bankbranchList;
	}

	@ResponseBody
	@RequestMapping("/getBranchIfsc")
	public List<Bank> getBranchIfsc(@RequestParam("bankName") String bankName, @RequestParam("branchName") String branchName, HttpSession session)
	{
		Query query = new Query();
		List<Bank> branchIfsc = mongoTemplate.find(query.addCriteria(Criteria.where("bankName").is(bankName).and("branchName").is(branchName)), Bank.class);

		return branchIfsc;
	}

	@ResponseBody
	@RequestMapping("/getallifscList")
	public List<Bank> AllifscList(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		List<Bank> bankList= bankRepository.findAll();
		List<LocationArea> locationareaList= locationAreaRepository.findAll();
		List<City> cityList = cityRepository.findAll();
		List<State> stateList= stateRepository.findAll();
		List<Country> countryList= countryRepository.findAll();

		int i,j;
		for(i=0;i<bankList.size();i++)
		{
			try 
			{
				for(j=0;j<countryList.size();j++)
				{
					if(bankList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
					{
						bankList.get(i).setCountryId(countryList.get(j).getCountryName());
						break;
					}
				}

				for(j=0;j<stateList.size();j++)
				{
					if(bankList.get(i).getStateId().equals(stateList.get(j).getStateId()))
					{
						bankList.get(i).setStateId(stateList.get(j).getStateName());
						break;
					}
				}

				for(j=0;j<cityList.size();j++)
				{
					if(bankList.get(i).getCityId().equals(cityList.get(j).getCityId()))
					{
						bankList.get(i).setCityId(cityList.get(j).getCityName());
						break;
					}
				}

				for(j=0;j<locationareaList.size();j++)
				{
					if(bankList.get(i).getLocationareaId().equals(locationareaList.get(j).getLocationareaId()))
					{
						bankList.get(i).setLocationareaId(locationareaList.get(j).getLocationareaName());
						break;
					}
				}

			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}

		return bankList;
	}

	@RequestMapping("/EditBank")
	public String EditBank(@RequestParam("bankId") String bankId,ModelMap model)
	{
		try {
			Query query = new  Query();
			List<Bank> bankDetails = mongoTemplate.find(query.addCriteria(Criteria.where("bankId").is(bankId)),Bank.class);
			query = new  Query();
			List<BankEmployees> bankEmployeesList = mongoTemplate.find(query.addCriteria(Criteria.where("bankId").is(bankId)), BankEmployees.class);
			try {
				query = new Query();
				Country countryDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("countryId").is(bankDetails.get(0).getCountryId())),Country.class);
				query = new Query();
				State stateDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("stateId").is(bankDetails.get(0).getStateId())),State.class);
				query = new Query();
				City cityDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("cityId").is(bankDetails.get(0).getCityId())),City.class);
				query = new Query();
				LocationArea locationareaDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("locationareaId").is(bankDetails.get(0).getLocationareaId())),LocationArea.class);

				model.addAttribute("countryName", countryDetails.getCountryName());
				model.addAttribute("stateName", stateDetails.getStateName());
				model.addAttribute("cityName", cityDetails.getCityName());
				model.addAttribute("locationareaName", locationareaDetails.getLocationareaName());
			}
			catch (Exception e) {
				// TODO: handle exception
			}


			model.addAttribute("bankDetails", bankDetails);
			model.addAttribute("countryList", findAllCountryName());
			model.addAttribute("designationList", getAllDesignations());
			model.addAttribute("bankEmployeesList", bankEmployeesList);
			return "EditBank";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/EditBank", method=RequestMethod.POST)
	public String EditBankPostMethod(@ModelAttribute Bank bank, Model model)
	{
		try {
			try
			{
				bank = new Bank(bank.getBankId(),bank.getBankName(),bank.getBranchName(),bank.getBankAddress(),bank.getCountryId(),bank.getStateId(),bank.getCityId(),bank.getLocationareaId(),bank.getAreaPincode(),bank.getBankPhoneno(),bank.getBankifscCode(),bank.getApfnumber(),bank.getCreationDate(),bank.getUpdateDate(),bank.getUserName());
				bankRepository.save(bank);
				model.addAttribute("bankStatus","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("bankStatus","Fail");
			}

			List<Bank> bankList = bankRepository.findAll(); 

			List<LocationArea> locationareaList= locationAreaRepository.findAll();
			List<City> cityList = cityRepository.findAll();
			List<State> stateList= stateRepository.findAll();
			List<Country> countryList= countryRepository.findAll();

			int i,j;
			for(i=0;i<bankList.size();i++)
			{
				try 
				{
					for(j=0;j<countryList.size();j++)
					{
						if(bankList.get(i).getCountryId().equals(countryList.get(j).getCountryId()))
						{
							bankList.get(i).setCountryId(countryList.get(j).getCountryName());
							break;
						}
					}

					for(j=0;j<stateList.size();j++)
					{
						if(bankList.get(i).getStateId().equals(stateList.get(j).getStateId()))
						{
							bankList.get(i).setStateId(stateList.get(j).getStateName());
							break;
						}
					}

					for(j=0;j<cityList.size();j++)
					{
						if(bankList.get(i).getCityId().equals(cityList.get(j).getCityId()))
						{
							bankList.get(i).setCityId(cityList.get(j).getCityName());
							break;
						}
					}

					for(j=0;j<locationareaList.size();j++)
					{
						if(bankList.get(i).getLocationareaId().equals(locationareaList.get(j).getLocationareaId()))
						{
							bankList.get(i).setLocationareaId(locationareaList.get(j).getLocationareaName());
							break;
						}
					}

				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}

			model.addAttribute("countryList", findAllCountryName());
			model.addAttribute("bankList",bankList);

			return "BankMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@ResponseBody
	@RequestMapping(value="/getBankCode", method=RequestMethod.POST)
	public List<Bank> getBankCode(@RequestParam("bankName") String bankName, HttpSession session) 
	{
		List<Bank> bankList1= new ArrayList<Bank>();
		Query query = new Query();
		bankList1 = mongoTemplate.find(query.addCriteria(Criteria.where("bankName").is(bankName)), Bank.class);
		return bankList1;
	}

	public int Generate_BankEmployeeId()
	{
		int bankEmployeeId = 0;

		List<BankEmployees> employeeList = bankEmployeesRepository.findAll();

		int size = employeeList.size();

		if(size!=0)
		{
			bankEmployeeId = employeeList.get(size-1).getBankEmployeeId();
		}

		return bankEmployeeId+1;
	}

	@ResponseBody
	@RequestMapping("/AddBankEmployee")
	public List<BankEmployees> AddBankEmployee(@RequestParam("bankId") String bankId, @RequestParam("employeeName") String employeeName, @RequestParam("employeeDesignation") String employeeDesignation, @RequestParam("employeeEmail") String employeeEmail, @RequestParam("employeeMobileno") String employeeMobileno, HttpSession session)
	{

		try
		{
			BankEmployees bankEmployees = new BankEmployees();
			bankEmployees.setBankEmployeeId(Generate_BankEmployeeId());
			bankEmployees.setBankId(bankId);
			bankEmployees.setEmployeeName(employeeName);
			bankEmployees.setEmployeeDesignation(employeeDesignation);
			bankEmployees.setEmployeeEmail(employeeEmail);
			bankEmployees.setEmployeeMobileno(employeeMobileno);

			bankEmployeesRepository.save(bankEmployees);

			Query query = new Query();
			List<BankEmployees> bankEmployeesList = mongoTemplate.find(query.addCriteria(Criteria.where("bankId").is(bankId)), BankEmployees.class);

			return bankEmployeesList;
		}
		catch(Exception e)
		{

			return null;
		}

	}

	@ResponseBody
	@RequestMapping("/EditBankEmployee")
	public BankEmployees EditBankEmployee(@RequestParam("bankId") String bankId, @RequestParam("bankEmployeeId") String bankEmployeeId, HttpSession session)
	{

		try
		{
			Query query = new Query();
			BankEmployees bankEmployee = mongoTemplate.findOne(query.addCriteria(Criteria.where("bankEmployeeId").is(Integer.parseInt(bankEmployeeId)).and("bankId").is(bankId)), BankEmployees.class);

			return bankEmployee;
		}
		catch(Exception e)
		{
			return null;
		}
	}

	@ResponseBody
	@RequestMapping("/DeleteBankEmployee")
	public List<BankEmployees> DeleteBankEmployee(@RequestParam("bankId") String bankId, @RequestParam("bankEmployeeId") String bankEmployeeId, HttpSession session)
	{
		try
		{
			Query query = new Query();
			mongoTemplate.remove(query.addCriteria(Criteria.where("bankEmployeeId").is(Integer.parseInt(bankEmployeeId)).and("bankId").is(bankId)), BankEmployees.class);

			Query query1 = new Query();
			List<BankEmployees> bankEmployeeList = mongoTemplate.find(query1.addCriteria(Criteria.where("bankId").is(bankId)), BankEmployees.class);

			return bankEmployeeList;
		}
		catch(Exception e)
		{
			return null;
		}

	}
}

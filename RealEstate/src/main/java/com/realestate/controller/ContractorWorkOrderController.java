package com.realestate.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.repository.ContractorWorkOrderRepository;
import com.realestate.repository.ContractorRepository;
import com.realestate.repository.ContractorTypeRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.bean.Contractor;
import com.realestate.bean.ContractorPaymentSchedule;
import com.realestate.bean.ContractorQuotation;
import com.realestate.bean.ContractorType;
import com.realestate.bean.ContractorWorkList;
import com.realestate.bean.ContractorWorkListHistory;
import com.realestate.bean.ContractorWorkOrder;
import com.realestate.bean.Login;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.SubContractorType;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.ContractorPaymentScheduleRepository;
import com.realestate.repository.ContractorWorkListRepository;
import com.realestate.repository.ContractorWorkListHistoryRepository;

@Controller
@RequestMapping("/")
public class ContractorWorkOrderController {

	@Autowired
	ContractorRepository contractorRepository;
	@Autowired
	ContractorPaymentScheduleRepository contractorpaymentscheduleRepository;
	@Autowired
	ContractorTypeRepository contractortypeRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	ContractorWorkOrderRepository contractorworkorderRepository;
	@Autowired
	ContractorWorkListRepository contractorworklistRepository;
	@Autowired
	ContractorWorkListHistoryRepository contractorworklisthistoryRepository;
	@Autowired
	MongoTemplate mongoTemplate;


	public long GenerateContractorPaymentId()
	{
		long id = 0;
		List<ContractorPaymentSchedule> contractorpaymentscheduleList = contractorpaymentscheduleRepository.findAll();

		int size = contractorpaymentscheduleList.size();

		if(size!=0)
		{
			id = contractorpaymentscheduleList.get(size-1).getContractorrschedulerId();
		}

		return id+1;
	}
	String workListCode;
	public String GenerateContractorWorkListId()
	{
		long cnt  = contractorworklistRepository.count();

		if(cnt<10)
		{
			workListCode = "WL000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			workListCode = "WL00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			workListCode = "WL0"+(cnt+1);
		}
		else
		{
			workListCode = "WL"+(cnt+1);
		}
		return workListCode;

	}

	public String GenerateContractorWorkOrderId()
	{
		int id = 0;
		String oderId="0";
		String workId="";
		List<ContractorWorkOrder> contractorworkOrderList = contractorworkorderRepository.findAll();

		int size = contractorworkOrderList.size();

		if(size!=0)
		{
			oderId = contractorworkOrderList.get(size-1).getWorkOrderId();
		}

		id=Integer.parseInt(oderId);

		if(id<10)
		{
			workId = "00000"+(id+1);
		}
		else if((id<100) && (id>=10))
		{
			workId = "0000"+(id+1);
		}
		else if((id<1000) && (id>=100))
		{
			workId = "000"+(id+1);
		}
		else if(id>=1000)
		{
			workId = "00"+(id+1);
		}
		else if((id<1000) && (id>=100))
		{
			workId = "0"+(id+1);
		}
		else if(id>=1000)
		{
			workId = ""+(id+1);
		}

		//id=Integer.parseInt(workId);

		return workId;
	}

	public long GenerateContractorWorkListHistoryId()
	{
		long workHistoryId = 0;

		List<ContractorWorkListHistory> contractorworklisthistory = contractorworklisthistoryRepository.findAll();

		int size = contractorworklisthistory.size();

		if(size!=0)
		{
			workHistoryId = contractorworklisthistory.get(size-1).getWorkHistoryId();
		}
		workHistoryId=workHistoryId+1;

		return workHistoryId;
	}


	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping("/ContractorWorkListMaster")
	public String ContractorWorkListMaster(Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			Query query = new Query();

			List<ContractorWorkList> contractorworkList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("Applied")), ContractorWorkList.class);

			for(int i=0;i<contractorworkList.size();i++)
			{
				try {
					query = new Query();
					List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(contractorworkList.get(i).getProjectId())), Project.class);
					query = new Query();
					List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(contractorworkList.get(i).getBuildingId())), ProjectBuilding.class);
					query = new Query();
					List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(contractorworkList.get(i).getWingId())), ProjectWing.class);

					query = new Query();
					List<ContractorType> contractorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("contractortypeId").is(contractorworkList.get(i).getContractortypeId())), ContractorType.class);

					contractorworkList.get(i).setProjectId(projectDetails.get(0).getProjectName());
					contractorworkList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
					contractorworkList.get(i).setWingId(wingDetails.get(0).getWingName());
					contractorworkList.get(i).setContractortypeId(contractorDetails.get(0).getContractorType());
				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}


			model.addAttribute("contractorworkList",contractorworkList);
			return "ContractorWorkListMaster";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping("/AddContractorWorkList")
	public String AddContractorWorkList(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		try {
			List<ContractorType> contractorTypeList = contractortypeRepository.findAll(); 
			List<Project> projectList = GetUserAssigenedProjectList(req,res); 

			model.addAttribute("workListCode",GenerateContractorWorkListId());
			model.addAttribute("contractorTypeList",contractorTypeList);
			model.addAttribute("projectList",projectList);
			return "AddContractorWorkList";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping(value = "/AddContractorWorkList", method = RequestMethod.POST)
	public String AddContractorWorkList(@ModelAttribute ContractorWorkList contractorworklist, Model model) throws ParseException
	{
		try {
			try
			{
				contractorworklist = new ContractorWorkList(contractorworklist.getWorkId(), 
						contractorworklist.getProjectId(), contractorworklist.getBuildingId(),contractorworklist.getWingId(), contractorworklist.getContractortypeId(),contractorworklist.getStatus(),
						contractorworklist.getCreationDate(), contractorworklist.getUpdateDate(), contractorworklist.getUserName());
				contractorworklistRepository.save(contractorworklist);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}

			Query query = new Query();

			List<ContractorWorkList> contractorworkList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("Applied")), ContractorWorkList.class);

			for(int i=0;i<contractorworkList.size();i++)
			{
				try
				{
					query = new Query();
					List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(contractorworkList.get(i).getProjectId())), Project.class);
					query = new Query();
					List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(contractorworkList.get(i).getBuildingId())), ProjectBuilding.class);
					query = new Query();
					List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(contractorworkList.get(i).getWingId())), ProjectWing.class);

					query = new Query();
					List<ContractorType> contractorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("contractortypeId").is(contractorworkList.get(i).getContractortypeId())), ContractorType.class);

					contractorworkList.get(i).setProjectId(projectDetails.get(0).getProjectName());
					contractorworkList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
					contractorworkList.get(i).setWingId(wingDetails.get(0).getWingName());
					contractorworkList.get(i).setContractortypeId(contractorDetails.get(0).getContractorType());
				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}


			model.addAttribute("contractorworkList",contractorworkList);
			return "ContractorWorkListMaster";

		}catch (Exception e) {
			return "login";
		}

	}


	@RequestMapping("/ContractorWorkOrderMaster")
	public String ContractorWorkOrderMaster(Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<Project> projectList = GetUserAssigenedProjectList(req,res); 
			List<ContractorWorkOrder> contarctorworkorderList = contractorworkorderRepository.findAll(); 
			Query query = new Query();
			for(int i=0;i<contarctorworkorderList.size();i++)
			{
				try
				{
					query = new Query();
					List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(contarctorworkorderList.get(i).getProjectId())), Project.class);
					query = new Query();
					List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(contarctorworkorderList.get(i).getBuildingId())), ProjectBuilding.class);
					query = new Query();
					List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(contarctorworkorderList.get(i).getWingId())), ProjectWing.class);

					query = new Query();
					List<ContractorType> contractorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("contractortypeId").is(contarctorworkorderList.get(i).getContractortypeId())), ContractorType.class);

					contarctorworkorderList.get(i).setProjectId(projectDetails.get(0).getProjectName());
					contarctorworkorderList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
					contarctorworkorderList.get(i).setWingId(wingDetails.get(0).getWingName());
					contarctorworkorderList.get(i).setContractortypeId(contractorDetails.get(0).getContractorType());
				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}


			model.addAttribute("contarctorworkorderList",contarctorworkorderList);
			model.addAttribute("projectList",projectList);

			return "ContractorWorkOrderMaster";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping("/AddContractorWorkOrder")
	public String AddContractorWorkOrder(@RequestParam("quotationId") String quotationId, ModelMap model) 
	{
		try {
			Query query = new Query();

			List<ContractorQuotation> contractorquotationDetails = mongoTemplate.find(query.addCriteria(Criteria.where("quotationId").is(quotationId)), ContractorQuotation.class);

			query = new Query();
			List<Contractor> contractorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("contractorId").is(contractorquotationDetails.get(0).getContractorId())), Contractor.class);

			query = new Query();
			List<ContractorWorkList> contractorworkList = mongoTemplate.find(query.addCriteria(Criteria.where("workId").is(contractorquotationDetails.get(0).getWorkId())), ContractorWorkList.class);


			query = new Query();
			List<ContractorWorkListHistory> contractorworkhistoryList = mongoTemplate.find(query.addCriteria(Criteria.where("workId").is(contractorquotationDetails.get(0).getWorkId())), ContractorWorkListHistory.class);

			for(int i=0;i<contractorworkhistoryList.size();i++)
			{
				query = new Query();
				List<SubContractorType> subcontractortypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subcontractortypeId").is(contractorworkhistoryList.get(i).getSubcontractortypeId())), SubContractorType.class);
				contractorworkhistoryList.get(i).setSubcontractortypeId(subcontractortypeDetails.get(0).getSubcontractorType());
			}

			query = new Query();
			List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(contractorworkList.get(0).getProjectId())), Project.class);
			query = new Query();
			List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(contractorworkList.get(0).getBuildingId())), ProjectBuilding.class);
			query = new Query();
			List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(contractorworkList.get(0).getWingId())), ProjectWing.class);

			query = new Query();
			List<ContractorType> contractorDetails1 = mongoTemplate.find(query.addCriteria(Criteria.where("contractortypeId").is(contractorworkList.get(0).getContractortypeId())), ContractorType.class);

			String id=GenerateContractorWorkOrderId();

			List<ContractorPaymentSchedule> contractorPaymentList = mongoTemplate.find(query.addCriteria(Criteria.where("workOrderId").is(id)), ContractorPaymentSchedule.class);
			query = new Query();
			try
			{
				if(contractorPaymentList.size()!=0)
				{
					mongoTemplate.remove(query.addCriteria(Criteria.where("workOrderId").is(id)), ContractorPaymentSchedule.class);	
				}
			}
			catch(Exception e)
			{

			}

			model.addAttribute("projectName", projectDetails.get(0).getProjectName());
			model.addAttribute("buildingName", buildingDetails.get(0).getBuildingName());
			model.addAttribute("wingName", wingDetails.get(0).getWingName());
			model.addAttribute("contractorType", contractorDetails1.get(0).getContractorType());

			model.addAttribute("workOrderCode",GenerateContractorWorkOrderId());

			model.addAttribute("contractorworkhistoryList",contractorworkhistoryList);
			model.addAttribute("contractorworkList",contractorworkList);
			model.addAttribute("contractorquotationDetails",contractorquotationDetails);
			model.addAttribute("contractorDetails",contractorDetails);

			return "AddContractorWorkOrder";

		}catch (Exception e) {
			return "login";
		}
	}


	@RequestMapping(value = "/AddContractorWorkOrder", method = RequestMethod.POST)
	public String SaveAddContractorWorkOrder(@ModelAttribute ContractorWorkOrder contractorworkorder, Model model) throws ParseException
	{
		try {
			try
			{
				contractorworkorder = new ContractorWorkOrder(contractorworkorder.getWorkOrderId(), contractorworkorder.getWorkId(),  contractorworkorder.getQuotationId(), 
						contractorworkorder.getProjectId(), contractorworkorder.getBuildingId(),contractorworkorder.getWingId(), 
						contractorworkorder.getContractortypeId(), contractorworkorder.getContractorId(), contractorworkorder.getContractorfirmName(),
						contractorworkorder.getTotalAmount(), contractorworkorder.getNoofMaleLabour(), contractorworkorder.getNoofFemaleLabour(),contractorworkorder.getNoofInsuredLabour(),
						contractorworkorder.getRetentionPer(), contractorworkorder.getRetentionAmount(),
						contractorworkorder.getCreationDate(), contractorworkorder.getUpdateDate(), contractorworkorder.getUserName(), contractorworkorder.getPaymentStatus());
				contractorworkorderRepository.save(contractorworkorder);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}


			ContractorWorkList contractorworkList1=new ContractorWorkList();

			contractorworkList1 = mongoTemplate.findOne(Query.query(Criteria.where("workId").is(contractorworkorder.getWorkId())), ContractorWorkList.class);
			contractorworkList1.setStatus("Allocated");
			contractorworklistRepository.save(contractorworkList1);

			Query query = new Query();

			List<ContractorWorkList> contractorworkList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("Applied")), ContractorWorkList.class);

			for(int i=0;i<contractorworkList.size();i++)
			{
				try
				{
					query = new Query();
					List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(contractorworkList.get(i).getProjectId())), Project.class);
					query = new Query();
					List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(contractorworkList.get(i).getBuildingId())), ProjectBuilding.class);
					query = new Query();
					List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(contractorworkList.get(i).getWingId())), ProjectWing.class);

					query = new Query();
					List<ContractorType> contractorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("contractortypeId").is(contractorworkList.get(i).getContractortypeId())), ContractorType.class);

					contractorworkList.get(i).setProjectId(projectDetails.get(0).getProjectName());
					contractorworkList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
					contractorworkList.get(i).setWingId(wingDetails.get(0).getWingName());
					contractorworkList.get(i).setContractortypeId(contractorDetails.get(0).getContractorType());
				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}


			model.addAttribute("contractorworkList",contractorworkList);
			return "ContractorWorkListMaster";

		}catch (Exception e) {
			return "login";
		}

	}

	@ResponseBody
	@RequestMapping(value="/getContratorDetails",method=RequestMethod.POST)
	public List<Contractor> getContratorDetails(@RequestParam("contractorId") String contractorId, HttpSession session)
	{
		List<Contractor> contractorList= new ArrayList<Contractor>();

		Query query = new Query();
		contractorList = mongoTemplate.find(query.addCriteria(Criteria.where("contractorId").is(contractorId)), Contractor.class);
		return contractorList;
	}

	@ResponseBody
	@RequestMapping("/AddContractorPayment")
	public List<ContractorPaymentSchedule> AddContractorPayment(@RequestParam("workOrderId") String workOrderId, @RequestParam("contractorId") String contractorId, @RequestParam("paymentDecription") String paymentDecription, @RequestParam("paymentAmountper") Double paymentAmountper, @RequestParam("paymentAmount") Double paymentAmount, @RequestParam("gstAmount") Double gstAmount,  @RequestParam("paymentTotalAmount") Double paymentTotalAmount, @RequestParam("tdsAmount") Double tdsAmount, @RequestParam("grandAmount") Double grandAmount, HttpSession session)
	{
		try
		{
			ContractorPaymentSchedule contractorpaymentschedule = new ContractorPaymentSchedule();
			contractorpaymentschedule.setContractorrschedulerId(GenerateContractorPaymentId());
			contractorpaymentschedule.setWorkOrderId(workOrderId);
			contractorpaymentschedule.setContractorId(contractorId);
			contractorpaymentschedule.setPaymentDecription(paymentDecription);
			contractorpaymentschedule.setPaymentAmountper(paymentAmountper);
			contractorpaymentschedule.setPaymentAmount(paymentAmount);
			contractorpaymentschedule.setGstAmount(gstAmount);
			contractorpaymentschedule.setPaymentTotalAmount(paymentTotalAmount);
			contractorpaymentschedule.setTdsAmount(tdsAmount);
			contractorpaymentschedule.setGrandAmount(grandAmount);
			contractorpaymentschedule.setStatus("Incompleted");
			contractorpaymentscheduleRepository.save(contractorpaymentschedule);

			Query query = new Query();
			List<ContractorPaymentSchedule> contractorpaymentList = mongoTemplate.find(query.addCriteria(Criteria.where("workOrderId").is(workOrderId).and("contractorId").is(contractorId)), ContractorPaymentSchedule.class);
			return contractorpaymentList;
		}
		catch(Exception e)
		{
			return null;
		}

	}	 

	@ResponseBody
	@RequestMapping("/DeleteCustomerPayment")
	public List<ContractorPaymentSchedule> DeleteCustomerPayment(@RequestParam("contractorrschedulerId") String contractorrschedulerId, @RequestParam("workOrderId") String workOrderId, HttpSession session)
	{
		try
		{
			Query query = new Query();
			mongoTemplate.remove(query.addCriteria(Criteria.where("contractorrschedulerId").is(Integer.parseInt(contractorrschedulerId)).and("workOrderId").is(workOrderId)), ContractorPaymentSchedule.class);

			query = new Query();
			List<ContractorPaymentSchedule> contractorpaymentList = mongoTemplate.find(query.addCriteria(Criteria.where("workOrderId").is(workOrderId)), ContractorPaymentSchedule.class);
			return contractorpaymentList;
		}
		catch(Exception e)
		{
			return null;
		}

	} 

	@RequestMapping("/EditContractorWorkOrder")
	public String EditContractorWorkOrder(@RequestParam("workOrderId") String workOrderId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<ContractorWorkOrder> contractorworkorderDetails = mongoTemplate.find(query.addCriteria(Criteria.where("workOrderId").is(workOrderId)),ContractorWorkOrder.class);

			query = new Query();
			List<ContractorPaymentSchedule> contractorpaymentscheduleList = mongoTemplate.find(query.addCriteria(Criteria.where("workOrderId").is(workOrderId)), ContractorPaymentSchedule.class); 

			try
			{

				query = new Query();
				List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(contractorworkorderDetails.get(0).getProjectId())), Project.class);
				query = new Query();
				List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(contractorworkorderDetails.get(0).getBuildingId())), ProjectBuilding.class);
				query = new Query();
				List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(contractorworkorderDetails.get(0).getWingId())), ProjectWing.class);

				query = new Query();
				List<ContractorType> contractorTypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("contractortypeId").is(contractorworkorderDetails.get(0).getContractortypeId())), ContractorType.class);
				query = new Query();
				List<Contractor> contractorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("contractorId").is(contractorworkorderDetails.get(0).getContractorId())), Contractor.class);

				query = new Query();
				List<ContractorWorkListHistory> contractorworkhistoryList = mongoTemplate.find(query.addCriteria(Criteria.where("workId").is(contractorworkorderDetails.get(0).getWorkId())), ContractorWorkListHistory.class);

				for(int i=0;i<contractorworkhistoryList.size();i++)
				{
					query = new Query();
					List<SubContractorType> subcontractortypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subcontractortypeId").is(contractorworkhistoryList.get(i).getSubcontractortypeId())), SubContractorType.class);
					contractorworkhistoryList.get(i).setSubcontractortypeId(subcontractortypeDetails.get(0).getSubcontractorType());
				}

				model.addAttribute("contractorworkhistoryList", contractorworkhistoryList);
				model.addAttribute("projectName", projectDetails.get(0).getProjectName());
				model.addAttribute("buildingName", buildingDetails.get(0).getBuildingName());
				model.addAttribute("wingName", wingDetails.get(0).getWingName());
				model.addAttribute("contractorType", contractorTypeDetails.get(0).getContractorType());
				model.addAttribute("contractorDetails", contractorDetails);
			}
			catch (Exception e) {
				// TODO: handle exception
			}



			model.addAttribute("contractorpaymentscheduleList",contractorpaymentscheduleList);
			model.addAttribute("contractorworkorderDetails", contractorworkorderDetails);

			return "EditContractorWorkOrder";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping(value="/EditContractorWorkOrder",method=RequestMethod.POST)
	public String EditContractorWorkOrder(@ModelAttribute ContractorWorkOrder contractorworkorder, Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			try
			{
				contractorworkorder = new ContractorWorkOrder(contractorworkorder.getWorkOrderId(), contractorworkorder.getWorkId(),  contractorworkorder.getQuotationId(), 
						contractorworkorder.getProjectId(), contractorworkorder.getBuildingId(),contractorworkorder.getWingId(), 
						contractorworkorder.getContractortypeId(), contractorworkorder.getContractorId(), contractorworkorder.getContractorfirmName(),
						contractorworkorder.getTotalAmount(), contractorworkorder.getNoofMaleLabour(), contractorworkorder.getNoofFemaleLabour(),contractorworkorder.getNoofInsuredLabour(),
						contractorworkorder.getRetentionPer(), contractorworkorder.getRetentionAmount(),
						contractorworkorder.getCreationDate(), contractorworkorder.getUpdateDate(), contractorworkorder.getUserName(), contractorworkorder.getPaymentStatus());
				contractorworkorderRepository.save(contractorworkorder);
				model.addAttribute("Status","Success");
			}
			catch(DuplicateKeyException de)
			{
				model.addAttribute("Status","Fail");
			}
			catch(Exception de)
			{
				model.addAttribute("Status","Fail");
			}


			List<Project> projectList = GetUserAssigenedProjectList(req,res); 
			List<ContractorWorkOrder> contarctorworkorderList = contractorworkorderRepository.findAll(); 
			Query query = new Query();
			for(int i=0;i<contarctorworkorderList.size();i++)
			{
				try
				{
					query = new Query();
					List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(contarctorworkorderList.get(i).getProjectId())), Project.class);
					query = new Query();
					List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(contarctorworkorderList.get(i).getBuildingId())), ProjectBuilding.class);
					query = new Query();
					List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(contarctorworkorderList.get(i).getWingId())), ProjectWing.class);

					query = new Query();
					List<ContractorType> contractorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("contractortypeId").is(contarctorworkorderList.get(i).getContractortypeId())), ContractorType.class);

					contarctorworkorderList.get(i).setProjectId(projectDetails.get(0).getProjectName());
					contarctorworkorderList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
					contarctorworkorderList.get(i).setWingId(wingDetails.get(0).getWingName());
					contarctorworkorderList.get(i).setContractortypeId(contractorDetails.get(0).getContractorType());
				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}


			model.addAttribute("contarctorworkorderList",contarctorworkorderList);
			model.addAttribute("projectList",projectList);

			return "ContractorWorkOrderMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/ViewContractorWorkOrder")
	public String ViewContractorWorkOrder(@RequestParam("workOrderId") String workOrderId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<ContractorWorkOrder> contractorworkorderDetails = mongoTemplate.find(query.addCriteria(Criteria.where("workOrderId").is(workOrderId)),ContractorWorkOrder.class);

			query = new Query();
			List<ContractorPaymentSchedule> contractorpaymentscheduleList = mongoTemplate.find(query.addCriteria(Criteria.where("workOrderId").is(workOrderId)), ContractorPaymentSchedule.class); 

			try
			{

				query = new Query();
				List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(contractorworkorderDetails.get(0).getProjectId())), Project.class);
				query = new Query();
				List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(contractorworkorderDetails.get(0).getBuildingId())), ProjectBuilding.class);
				query = new Query();
				List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(contractorworkorderDetails.get(0).getWingId())), ProjectWing.class);

				query = new Query();
				List<ContractorType> contractorTypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("contractortypeId").is(contractorworkorderDetails.get(0).getContractortypeId())), ContractorType.class);
				query = new Query();
				List<Contractor> contractorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("contractorId").is(contractorworkorderDetails.get(0).getContractorId())), Contractor.class);

				query = new Query();
				List<ContractorWorkListHistory> contractorworkhistoryList = mongoTemplate.find(query.addCriteria(Criteria.where("workId").is(contractorworkorderDetails.get(0).getWorkId())), ContractorWorkListHistory.class);

				for(int i=0;i<contractorworkhistoryList.size();i++)
				{
					query = new Query();
					List<SubContractorType> subcontractortypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subcontractortypeId").is(contractorworkhistoryList.get(i).getSubcontractortypeId())), SubContractorType.class);
					contractorworkhistoryList.get(i).setSubcontractortypeId(subcontractortypeDetails.get(0).getSubcontractorType());
				}

				model.addAttribute("contractorworkhistoryList", contractorworkhistoryList);
				model.addAttribute("projectName", projectDetails.get(0).getProjectName());
				model.addAttribute("buildingName", buildingDetails.get(0).getBuildingName());
				model.addAttribute("wingName", wingDetails.get(0).getWingName());
				model.addAttribute("contractorType", contractorTypeDetails.get(0).getContractorType());
				model.addAttribute("contractorDetails", contractorDetails);
			}
			catch (Exception e) {
				// TODO: handle exception
			}



			model.addAttribute("contractorpaymentscheduleList",contractorpaymentscheduleList);
			model.addAttribute("contractorworkorderDetails", contractorworkorderDetails);

			return "ViewContractorWorkOrder";

		}catch (Exception e) {
			return "login";
		}
	}


	@ResponseBody
	@RequestMapping("/AddContractorWorkListHistory")
	public List<ContractorWorkListHistory> AddContractorWorkListHistory(@RequestParam("workId") String workId, @RequestParam("contractortypeId") String contractortypeId, @RequestParam("subcontractortypeId") String subcontractortypeId, @RequestParam("workType") String workType, @RequestParam("type") String type,@RequestParam("unit") Double unit, @RequestParam("workRate") Double workRate, @RequestParam("grandTotal") Double grandTotal,  HttpSession session)
	{
		try
		{
			ContractorWorkListHistory contractorworklisthistory = new ContractorWorkListHistory();
			contractorworklisthistory.setWorkHistoryId(GenerateContractorWorkListHistoryId());
			contractorworklisthistory.setWorkId(workId);
			contractorworklisthistory.setContractortypeId(contractortypeId);
			contractorworklisthistory.setSubcontractortypeId(subcontractortypeId);
			contractorworklisthistory.setWorkType(workType);
			contractorworklisthistory.setType(type);
			contractorworklisthistory.setUnit(unit);
			contractorworklisthistory.setWorkRate(workRate);
			contractorworklisthistory.setGrandTotal(grandTotal);

			contractorworklisthistoryRepository.save(contractorworklisthistory);

			Query query = new Query();
			List<ContractorWorkListHistory> contractorworkhistoryList = mongoTemplate.find(query.addCriteria(Criteria.where("workId").is(workId)), ContractorWorkListHistory.class);

			for(int i=0;i<contractorworkhistoryList.size();i++)
			{
				query = new Query();
				List<SubContractorType> subcontractortypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subcontractortypeId").is(contractorworkhistoryList.get(i).getSubcontractortypeId())), SubContractorType.class);
				contractorworkhistoryList.get(i).setSubcontractortypeId(subcontractortypeDetails.get(0).getSubcontractorType());
			}


			return contractorworkhistoryList;
		}
		catch(Exception e)
		{
			return null;
		}

	}  

	@ResponseBody
	@RequestMapping("/DeleteContractorWorkListHistory")
	public List<ContractorWorkListHistory> DeleteContractorWorkListHistory(@RequestParam("workId") String workId, @RequestParam("workHistoryId") String workHistoryId, HttpSession session)
	{
		try
		{
			Query query = new Query();
			mongoTemplate.remove(query.addCriteria(Criteria.where("workHistoryId").is(Integer.parseInt(workHistoryId)).and("workId").is(workId)), ContractorWorkListHistory.class);


			query = new Query();
			List<ContractorWorkListHistory> contractorworkhistoryList = mongoTemplate.find(query.addCriteria(Criteria.where("workId").is(workId)), ContractorWorkListHistory.class);

			for(int i=0;i<contractorworkhistoryList.size();i++)
			{
				query = new Query();
				List<SubContractorType> subcontractortypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subcontractortypeId").is(contractorworkhistoryList.get(i).getSubcontractortypeId())), SubContractorType.class);
				contractorworkhistoryList.get(i).setSubcontractortypeId(subcontractortypeDetails.get(0).getSubcontractorType());
			}


			return contractorworkhistoryList;
		}
		catch(Exception e)
		{

			return null;
		}

	} 

}

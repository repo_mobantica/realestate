package com.realestate.controller;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;

import java.io.InputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.realestate.bean.Flat;
import com.realestate.bean.Floor;
import com.realestate.bean.Login;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.FlatRepository;
import com.realestate.repository.FloorRepository;
import com.realestate.repository.ProjectBuildingRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.ProjectWingRepository;

import java.io.FileInputStream;
import java.io.IOException;


@Controller
@RequestMapping("/")
public class ImportNewFlatController 
{
	@Autowired
	ServletContext context;

	@Autowired
	FlatRepository flatRepository;
	@Autowired
	FloorRepository floorRepository;
	@Autowired
	ProjectRepository projectRepository;

	@Autowired
	ProjectBuildingRepository projectbuildingRepository;
	@Autowired
	ProjectWingRepository projectwingRepository;

	@Autowired
	MongoTemplate mongoTemplate;

	Flat flat = new Flat();
	HSSFWorkbook workbook;
	XSSFWorkbook workbook1;

	HSSFSheet worksheet;
	XSSFSheet worksheet1; 

	String flatCode;
	private String FlatCode()
	{
		long cnt  = flatRepository.count();
		if(cnt<10)
		{
			flatCode = "FL000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			flatCode = "FL00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			flatCode = "FL0"+(cnt+1);
		}
		else
		{
			flatCode = "FL"+(cnt+1);
		}
		return flatCode;
	}

	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping(value="/ImportNewFlat")
	public String importNewFlat(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		return "ImportNewFlat";
	}


	@SuppressWarnings({ "deprecation"})
	@RequestMapping(value = "/ImportNewFlat", method = RequestMethod.POST)
	public String uploadFlatDetails(@RequestParam("flat_excel") MultipartFile flat_excel, Model model, HttpServletRequest req, HttpServletResponse res, RedirectAttributes redirectAttributes, HttpSession session) 
	{

		String user = (String)req.getSession().getAttribute("user");
		List<Project> projectList = GetUserAssigenedProjectList(req,res);
		Query query = new Query();
		List<ProjectBuilding> buildingList = projectbuildingRepository.findAll();
		List<ProjectWing> wingList = projectwingRepository.findAll();
		List<Floor> floorList = floorRepository.findAll();

		List<Flat> flatRejectedList=new ArrayList<Flat>();
		Flat flatejected=new Flat();

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/M/yyyy");
		LocalDate localDate = LocalDate.now();
		String todayDate=dtf.format(localDate);
		String projectId="",buildingId="",wingId="",floorId="";
		int flag1=0,flag2=0;
		String msg="";

		if (!flat_excel.isEmpty() || flat_excel != null )
		{
			try 
			{
				if(flat_excel.getOriginalFilename().endsWith("xls") || flat_excel.getOriginalFilename().endsWith("xlsx") || flat_excel.getOriginalFilename().endsWith("csv"))
				{
					if(flat_excel.getOriginalFilename().endsWith("xlsx"))
					{
						InputStream stream = flat_excel.getInputStream();
						XSSFWorkbook workbook = new XSSFWorkbook(stream);

						XSSFSheet sheet = workbook.getSheet("Sheet1");  /// this will read 1st workbook of ExcelSheet

						int firstRow = sheet.getFirstRowNum();

						XSSFRow firstrow = sheet.getRow(firstRow);

						@SuppressWarnings("unused")
						int lastColumnCount = firstrow.getLastCellNum();

						@SuppressWarnings("unused")
						Iterator<Row> rowIterator = sheet.iterator();   
						int last_no=sheet.getLastRowNum();

						Flat flat = new Flat();
						String flatType;
						String flatType1;
						double flatbasicCost=0.0;
						double flatArea=0.0;
						long flatArea1=0;
						long loadingPer1=0;
						double loadingPer=0.0;
						double projectLoading=0.0;
						double totalFlatCost=0.0;
						long flatAreawithLoadingInM1=0;
						double flatAreawithLoadingInM=0.0;

						String flatnumber1,projectcname1,buildingname1,wingname1,floorname1,flatfacingtype1,flattype1;
						double floorrice1;
						double flatcost1;
						double minimumflatcost1;
						double flatarea1;
						double dryterracearea1;
						double balconyarea1;
						double terracearea1;
						double carpetarea1;

						for(int i=0;i<last_no;i++)
						{
							projectId="";
							buildingId="";
							wingId="";
							floorId="";
							flag1=0;
							flag2=0;
							msg="";

							flatnumber1="";
							projectcname1="";
							buildingname1="";
							wingname1="";
							floorname1="";
							flatfacingtype1="";
							flattype1="";
							carpetarea1=0;
							terracearea1=0;
							balconyarea1=0;
							dryterracearea1=0;
							flatarea1=0;
							flatcost1=0;
							minimumflatcost1=0;
							floorrice1=0;


							try {
								XSSFRow row = sheet.getRow(i+1);

								// Skip read heading 
								if (row.getRowNum() == 0) 
								{
									continue;
								}


								try {
									row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
									flatnumber1=(String)row.getCell(0).getStringCellValue();

								}catch (Exception e) {
									flag1 = 1;
									msg="Enter the flat Number ";}


								try {

									projectcname1=row.getCell(1).getStringCellValue();
								}catch (Exception e) {
									flag1 = 1;
									msg="Enter the Project Name ";}


								try {

									buildingname1=row.getCell(2).getStringCellValue();
								}catch (Exception e) {
									flag1 = 1;
									msg="Enter the Building Name ";}


								try {

									wingname1=row.getCell(3).getStringCellValue();
								}catch (Exception e) {
									flag1 = 1;
									msg="Enter the Wing name ";}


								try {

									floorname1= row.getCell(4).getStringCellValue();
								}catch (Exception e) {
									flag1 = 1;
									msg="Enter the Floor ";}


								try {

									flatfacingtype1= row.getCell(5).getStringCellValue();
								}catch (Exception e) {
									flag1 = 1;
									msg="Enter the Flat Facing Type";}


								try {

									flattype1=row.getCell(6).getStringCellValue();
								}catch (Exception e) {
									flag1 = 1;
									msg="Enter the flat type";}


								try {

									row.getCell(7).setCellType(Cell.CELL_TYPE_NUMERIC);
									carpetarea1=row.getCell(7).getNumericCellValue();
								}catch (Exception e) {
									flag1 = 1;
									msg="Enter the Carpet Area In Sq.M";}


								try {

									row.getCell(8).setCellType(Cell.CELL_TYPE_NUMERIC);
									terracearea1=row.getCell(8).getNumericCellValue();
								}catch (Exception e) {
									flag1 = 1;
									msg="Enter the Terrace Area in Sq.M";}

								try {

									row.getCell(9).setCellType(Cell.CELL_TYPE_NUMERIC);
									balconyarea1=row.getCell(9).getNumericCellValue();
								}catch (Exception e) {
									flag1 = 1;
									msg="Enter the Balcony Area in Sq.M";}


								try {

									row.getCell(10).setCellType(Cell.CELL_TYPE_NUMERIC);
									dryterracearea1=row.getCell(10).getNumericCellValue();
								}catch (Exception e) {
									flag1 = 1;
									msg="Enter the Dryterrace Area In meter";}

								try {

									row.getCell(11).setCellType(Cell.CELL_TYPE_NUMERIC);
									dryterracearea1=row.getCell(10).getNumericCellValue();
									flatarea1= row.getCell(11).getNumericCellValue();
								}catch (Exception e) {
									flag1 = 1;
									msg="Enter the Flat Area in Ft";}

								try {

									row.getCell(12).setCellType(Cell.CELL_TYPE_NUMERIC);
									flatcost1=row.getCell(12).getNumericCellValue();
								}catch (Exception e) {
									flag1 = 1;
									msg="Enter the Flat Cost per Sq.Ft";}


								try {

									row.getCell(13).setCellType(Cell.CELL_TYPE_NUMERIC);
									minimumflatcost1= row.getCell(13).getNumericCellValue();
								}catch (Exception e) {
									flag1 = 1;
									msg="Enter the Minimum Flat cost perSq.M";}

								try {

									row.getCell(14).setCellType(Cell.CELL_TYPE_NUMERIC);
									floorrice1= row.getCell(14).getNumericCellValue();
								}catch (Exception e) {
									flag1 = 1;
									msg="Enter the Floor Rise";}


								if(flag1==0)
								{

									for(int j=0; j<projectList.size();j++)
									{
										if((projectList.get(j).getProjectName().trim()).equalsIgnoreCase((row.getCell(1).getStringCellValue().trim())))
										{
											projectId=projectList.get(j).getProjectId();
											break;
										}
									}

									for(int j=0; j<buildingList.size();j++)
									{
										if((buildingList.get(j).getBuildingName().trim()).equalsIgnoreCase((row.getCell(2).getStringCellValue().trim())))
										{
											buildingId=buildingList.get(j).getBuildingId();
											break;
										}
									}

									for(int j=0; j<wingList.size();j++)
									{
										if((wingList.get(j).getWingName().trim()).equalsIgnoreCase((row.getCell(3).getStringCellValue().trim())))
										{
											wingId=wingList.get(j).getWingId();
											break;
										}
									}

									for(int j=0; j<floorList.size();j++)
									{
										if((floorList.get(j).getFloortypeName().trim()).equalsIgnoreCase((row.getCell(4).getStringCellValue().trim())))
										{
											floorId=floorList.get(j).getFloorId();
											break;
										}
									}
								}

								if(flag1==0)
								{
									if(projectId.equals(""))
									{
										msg="Invalid project Name"; 
										flag1=1;
									}

									if(buildingId.equals(""))
									{
										msg="Invalid Building Name"; 
										flag1=1;
									}

									if(wingId.equals(""))
									{
										msg="Invalid wing Name"; 
										flag1=1;
									}

									if(floorId.equals(""))
									{
										msg="Invalid floor"; 
										flag1=1;
									}

								}

								if(flag1==0)
								{

									if(!projectId.equals("") && !buildingId.equals("") && !wingId.equals("") && !floorId.equals(""))
									{

										flat.setFlatId(FlatCode());

										row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);

										flat.setFlatNumber(row.getCell(0).getStringCellValue());

										flat.setProjectId(projectId);

										flat.setBuildingId(buildingId);

										flat.setWingId(wingId);

										flat.setFloorId(floorId);

										flat.setFlatfacingName(row.getCell(5).getStringCellValue());

										flatType=row.getCell(6).getStringCellValue();
										flatType1 = flatType.replaceAll("\\s", "");
										flat.setFlatType(flatType1);
										//carpet area
										row.getCell(7).setCellType(Cell.CELL_TYPE_NUMERIC);
										flat.setCarpetArea(row.getCell(7).getNumericCellValue());

										row.getCell(8).setCellType(Cell.CELL_TYPE_NUMERIC);
										flat.setTerraceArea(row.getCell(8).getNumericCellValue());

										row.getCell(9).setCellType(Cell.CELL_TYPE_NUMERIC);
										flat.setBalconyArea(row.getCell(8).getNumericCellValue());

										row.getCell(10).setCellType(Cell.CELL_TYPE_NUMERIC);
										flat.setDryterraceArea(row.getCell(10).getNumericCellValue());

										flatArea1=(long) (row.getCell(7).getNumericCellValue()+row.getCell(8).getNumericCellValue()+row.getCell(8).getNumericCellValue()+row.getCell(10).getNumericCellValue());
										flatArea=flatArea1;
										flat.setFlatArea(flatArea);

										//project Loading
										projectLoading=Findloading(row.getCell(1).getStringCellValue());

										flat.setLoading(projectLoading);
										//loading per
										loadingPer1=(long) ((flatArea*projectLoading)/100);
										loadingPer=Math.round(loadingPer1);
										flat.setLoadingpercentage(loadingPer);

										// Flat Area in M
										flatAreawithLoadingInM1=(long) (flatArea+loadingPer);
										flatAreawithLoadingInM=flatAreawithLoadingInM1;
										flat.setFlatAreawithLoadingInM(flatAreawithLoadingInM);

										//net Flat area in Ft
										row.getCell(11).setCellType(Cell.CELL_TYPE_NUMERIC);
										flat.setFlatAreawithLoadingInFt(row.getCell(11).getNumericCellValue());

										// flat cost without floor rise
										row.getCell(12).setCellType(Cell.CELL_TYPE_NUMERIC);
										flat.setFlatCostwithotfloorise(row.getCell(12).getNumericCellValue());

										//add floor rise
										row.getCell(14).setCellType(Cell.CELL_TYPE_NUMERIC);
										flat.setFloorRise(row.getCell(14).getNumericCellValue());

										//total flat Cost including floor rise
										totalFlatCost=row.getCell(12).getNumericCellValue()+row.getCell(14).getNumericCellValue();
										flat.setFlatCost(totalFlatCost);

										// minimun flat cost
										row.getCell(13).setCellType(Cell.CELL_TYPE_NUMERIC);
										flat.setFlatminimumCost(row.getCell(13).getNumericCellValue());

										flatbasicCost=row.getCell(11).getNumericCellValue()*totalFlatCost;
										// row.getCell(14).setCellType(Cell.CELL_TYPE_NUMERIC);
										flat.setFlatbasicCost(flatbasicCost);

										flat.setFlatstatus("Booking Remaninig");
										flat.setCreationDate(todayDate);
										flat.setUpdateDate(todayDate);
										flat.setUserName(user);
										flatRepository.save(flat);
									}
								}
								else
								{

									//List<Flat> flatRejectedList=new ArrayList<Flat>();
									flatejected=new Flat();
									flatejected.setFlatId(String.valueOf(i+1));
									flatejected.setFlatNumber(flatnumber1);
									flatejected.setProjectId(projectcname1);
									flatejected.setBuildingId(buildingname1);
									flatejected.setWingId(wingname1);
									flatejected.setFloorId(floorname1);
									flatejected.setFlatfacingName(flatfacingtype1);
									flatejected.setFlatType(flattype1);
									flatejected.setCarpetArea(carpetarea1);
									flatejected.setTerraceArea(terracearea1);
									flatejected.setBalconyArea(balconyarea1);
									flatejected.setDryterraceArea(dryterracearea1);
									flatejected.setFlatAreawithLoadingInFt(flatarea1);
									flatejected.setFlatCostwithotfloorise(flatcost1);
									flatejected.setFlatminimumCost(minimumflatcost1);
									flatejected.setFloorRise(floorrice1);
									flatejected.setFlatstatus(msg);
									flatRejectedList.add(flatejected);

								}

							}
							catch (Exception ee) {
							}

						}
						workbook.close();
					}

				}//if after inner if

			}
			catch(Exception e)
			{

			}
		}

		model.addAttribute("flatListSize", flatRejectedList.size());
		model.addAttribute("flatRejectedList", flatRejectedList);

		return "ImportNewFlat";
	}

	public Double Findloading(String projectName)
	{
		Double loading=0.0;
		int loading1;
		Project projectList;
		Query query = new Query();
		projectList = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectName").is(projectName)), Project.class);

		loading1=projectList.getLoading();
		loading=(double) Math.round(loading1);
		return loading;
	}

	//private static final String INTERNAL_FILE="Newflat.xlsx";
	@RequestMapping(value = "/DownloadFlatTemplate")
	public String downloadFlatTemplate(Model model, HttpServletResponse response, HttpServletRequest request, RedirectAttributes redirectAttributes, HttpSession session) throws IOException 
	{

		String filename = "Newflat.xlsx";
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		String filepath = "//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"Template/";
		response.setContentType("APPLICATION/OCTET-STREAM");
		response.setHeader("Content-Disposition", "attachment; filename=\""+ filename + "\"");


		FileInputStream fileInputStream = new FileInputStream(filepath+filename);

		int i;
		while ((i = fileInputStream.read()) != -1) {
			out.write(i);
		}
		fileInputStream.close();
		out.close();


		/*try 
		{
            String downloadFolder = request.getSession().getServletContext().getRealPath("/webapp/Template");
            //context.getRealPath("src/main/webapp/Template");
            System.out.println("path="+downloadFolder);
            File file = new File(downloadFolder + File.separator + filename);

            if (file.exists()) 
            {
                String mimeType = context.getMimeType(file.getPath());

                if (mimeType == null) 
                {
                    mimeType = "application/octet-stream";
                }

                response.setContentType(mimeType);
                response.addHeader("Content-Disposition", "attachment; filename=" + filename);
                response.setContentLength((int) file.length());

                OutputStream os = response.getOutputStream();
                FileInputStream fis = new FileInputStream(file);
                byte[] buffer = new byte[4096];
                int b = -1;

                while ((b = fis.read(buffer)) != -1) 
                {
                    os.write(buffer, 0, b);
                }

                fis.close();
                os.close();
            } 
            else 
            {
                System.out.println("Requested " + filename + " file not found!!");
            }
        } 
		catch (IOException e) 
		{
            System.out.println("Error:- " + e.getMessage());
        }*/


		/*File file = new File(fileName);
		FileInputStream in = new FileInputStream(file);
		byte[] content = new byte[(int) file.length()];
		in.read(content);
		ServletContext sc = request.getSession().getServletContext();
		String mimetype = sc.getMimeType(file.getName());
		response.reset();
		response.setContentType(mimetype);
		response.setContentLength(content.length);
		response.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");
		org.springframework.util.FileCopyUtils.copy(content, response.getOutputStream());*/

		return "ImportNewFlat";
	}

}

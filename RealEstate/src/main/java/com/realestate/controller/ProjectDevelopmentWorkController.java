package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.Agent;
import com.realestate.bean.AgentEmployees;
import com.realestate.bean.City;
import com.realestate.bean.State;
import com.realestate.bean.Country;
import com.realestate.bean.Login;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectDevelopmentWork;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.StateRepository;
import com.realestate.repository.ProjectDevelopmentWorkRepository;
import com.realestate.repository.CityRepository;
import com.realestate.repository.CountryRepository;

@Controller
@RequestMapping("/")
public class ProjectDevelopmentWorkController {


	@Autowired
	CityRepository cityRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	ProjectDevelopmentWorkRepository projectdevelopmentworkRepository;	
	@Autowired
	CountryRepository countryRepository;
	@Autowired
	StateRepository stateRepository;
	@Autowired
	MongoTemplate mongoTemplate;



	long projectWorkId=0;
	private long ProjectWorkId()
	{
		projectWorkId=0;
		List<ProjectDevelopmentWork> projectdevelopmentworkList= projectdevelopmentworkRepository.findAll();
		if(projectdevelopmentworkList.size()!=0)
		{
			projectWorkId=projectdevelopmentworkList.get(0).getProjectWorkId();

			for(int i=0;i<projectdevelopmentworkList.size();i++)
			{
				if(projectWorkId<projectdevelopmentworkList.get(i).getProjectWorkId())
				{
					projectWorkId=projectdevelopmentworkList.get(i).getProjectWorkId();
				}
			}

		}
		projectWorkId=projectWorkId+1;
		return projectWorkId;
	}

	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);

			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);

			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId()).and("reraStatus").is("RERA")), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}
	}

	@RequestMapping("/ProjectDevelopmentWorkMaster")
	public String ProjectDevelopmentWorkMaster(Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<Project> projectList= GetUserAssigenedProjectList(req,res);
			List<City> cityList = cityRepository.findAll();
			List<State> stateList = stateRepository.findAll();

			for(int i=0;i<projectList.size();i++)
			{
				for(int j=0;j<cityList.size();j++)
				{
					if(projectList.get(i).getCityId().equalsIgnoreCase(cityList.get(j).getCityId()))
					{
						projectList.get(i).setCityId(cityList.get(j).getCityName());
						break;
					}

				}
				for(int j=0;j<stateList.size();j++)
				{
					if(projectList.get(i).getStateId().equalsIgnoreCase(stateList.get(j).getStateId()))
					{
						projectList.get(i).setStateId(stateList.get(j).getStateName());
						break;
					}
				}
			}


			List<Country> countryList= countryRepository.findAll();

			model.addAttribute("countryList",countryList);
			model.addAttribute("projectList",projectList);
			return "ProjectDevelopmentWorkMaster";

		}catch (Exception e) {
			return "login";
		}
	}

	@RequestMapping("/AddProjectDevelopmentWork")
	public String AddProjectDevelopmentWork(@RequestParam("projectId") String projectId, ModelMap model)
	{
		try {
			Query query = new Query();
			List<Project> projectList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)),Project.class);

			query = new Query();
			List<ProjectDevelopmentWork> projectdevelopmentworkList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)),ProjectDevelopmentWork.class);

			model.addAttribute("projectdevelopmentworkList",projectdevelopmentworkList);
			model.addAttribute("projectWorkId",ProjectWorkId());
			model.addAttribute("projectName",projectList.get(0).getProjectName());
			model.addAttribute("projectId",projectId);
			return "AddProjectDevelopmentWork";

		}catch (Exception e) {
			return "login";
		}
	}



	@RequestMapping(value = "/AddProjectDevelopmentWork", method = RequestMethod.POST)
	public String AddProjectDevelopmentWork(Model model, HttpServletRequest req, HttpServletResponse res)
	{
		try {
			List<Project> projectList= GetUserAssigenedProjectList(req,res);
			List<Country> countryList= countryRepository.findAll();

			model.addAttribute("countryList",countryList);
			model.addAttribute("projectList",projectList);
			return "ProjectDevelopmentWorkMaster";

		}catch (Exception e) {
			return "login";
		}

	}


	@ResponseBody
	@RequestMapping("/AddProjectWork")
	public List<ProjectDevelopmentWork> AddProjectWork(@RequestParam("projectId") String projectId, @RequestParam("commonAreasAndFacilitiesAmenities") String commonAreasAndFacilitiesAmenities, @RequestParam("availableStatus") String availableStatus, @RequestParam("completionPercent") String completionPercent, @RequestParam("details") String details, HttpSession session)
	{
		try
		{
			try
			{
				ProjectDevelopmentWork projectdevelopmentwork = new ProjectDevelopmentWork();
				projectdevelopmentwork.setProjectWorkId(ProjectWorkId());
				projectdevelopmentwork.setProjectId(projectId);
				projectdevelopmentwork.setCommonAreasAndFacilitiesAmenities(commonAreasAndFacilitiesAmenities);
				projectdevelopmentwork.setAvailableStatus(availableStatus);
				projectdevelopmentwork.setCompletionPercent(completionPercent);
				projectdevelopmentwork.setDetails(details);

				projectdevelopmentworkRepository.save(projectdevelopmentwork);
			}
			catch (Exception e) {
			}
			Query query = new Query();
			List<ProjectDevelopmentWork> projectdevelopmentworkList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)),ProjectDevelopmentWork.class);

			return projectdevelopmentworkList;
		}
		catch(Exception e)
		{
			return null;
		}

	}	  



	@ResponseBody
	@RequestMapping("/DeleteProjectWork")
	public List<ProjectDevelopmentWork> DeleteProjectWork(@RequestParam("projectWorkId") long projectWorkId, @RequestParam("projectId") String projectId, HttpSession session)
	{
		try
		{
			Query query = new Query();
			mongoTemplate.remove(query.addCriteria(Criteria.where("projectWorkId").is(projectWorkId).and("projectId").is(projectId)), ProjectDevelopmentWork.class);

			query = new Query();
			List<ProjectDevelopmentWork> projectdevelopmentworkList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)),ProjectDevelopmentWork.class);

			return projectdevelopmentworkList;
		}
		catch(Exception e)
		{
			return null;
		}

	} 

	@ResponseBody
	@RequestMapping("/UpdateProjectWork")
	public List<ProjectDevelopmentWork> UpdateProjectWork(@RequestParam("projectWorkId") long projectWorkId, @RequestParam("projectId") String projectId, @RequestParam("commonAreasAndFacilitiesAmenities") String commonAreasAndFacilitiesAmenities, @RequestParam("availableStatus") String availableStatus, @RequestParam("completionPercent") String completionPercent, @RequestParam("details") String details, HttpSession session)
	{
		try
		{
			try
			{
				ProjectDevelopmentWork projectdevelopmentwork = new ProjectDevelopmentWork();
				projectdevelopmentwork.setProjectWorkId(projectWorkId);
				projectdevelopmentwork.setProjectId(projectId);
				projectdevelopmentwork.setCommonAreasAndFacilitiesAmenities(commonAreasAndFacilitiesAmenities);
				projectdevelopmentwork.setAvailableStatus(availableStatus);
				projectdevelopmentwork.setCompletionPercent(completionPercent);
				projectdevelopmentwork.setDetails(details);

				projectdevelopmentworkRepository.save(projectdevelopmentwork);
			}
			catch (Exception e) {
			}
			Query query = new Query();
			List<ProjectDevelopmentWork> projectdevelopmentworkList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)),ProjectDevelopmentWork.class);

			return projectdevelopmentworkList;
		}
		catch(Exception e)
		{
			return null;
		}

	}	  

}

package com.realestate.controller;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.ChartofAccount;
import com.realestate.bean.City;
import com.realestate.bean.Company;
import com.realestate.bean.CompanyBanks;
import com.realestate.bean.Employee;
import com.realestate.bean.Item;
import com.realestate.bean.LocationArea;
import com.realestate.bean.MaterialPurchaseRequisition;
import com.realestate.bean.MaterialsPurchased;
import com.realestate.bean.MaterialsPurchasedHistory;
import com.realestate.bean.Project;
import com.realestate.bean.State;
import com.realestate.bean.Store;
import com.realestate.bean.StoreStock;
import com.realestate.bean.StoreStockHistory;
import com.realestate.bean.SubSupplierType;
import com.realestate.bean.Supplier;
import com.realestate.bean.SupplierBillPayment;
import com.realestate.configuration.CommanController;
import com.realestate.repository.ChartofAccountRepository;
import com.realestate.repository.MaterialsPurchasedHistoryRepository;
import com.realestate.repository.MaterialsPurchasedRepository;
import com.realestate.repository.StoreRepository;
import com.realestate.repository.SupplierRepository;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;

@Controller
@RequestMapping("/")
public class SupplierBillPaymentReceiptController
{
	@Autowired
	StoreRepository storeRepository;
	@Autowired
	SupplierRepository supplierRepository;
	@Autowired
	ChartofAccountRepository chartofaccountRepository;
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	MaterialsPurchasedHistoryRepository materialsPurchasedHistoryRepository;
	@Autowired
	MaterialsPurchasedRepository materialsPurchasedRepository;

	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value="/MaterialPurchaseBillReceipt")
	public ResponseEntity<byte[]> MaterialPurchaseBillReceipt(@RequestParam("materialsPurchasedId") String materialsPurchasedId, ModelMap model, HttpServletResponse response, HttpServletRequest request)
	{
		JasperPrint print;

		try 
		{	
			Query query = new Query();
			HashMap jmap = new HashMap();
			Collection c = new ArrayList();
			String challanNo="";
			String grnNo="";
			List<MaterialsPurchasedHistory> materialPurchasedHistory = mongoTemplate.find(query.addCriteria(Criteria.where("materialsPurchasedId").is(materialsPurchasedId)), MaterialsPurchasedHistory.class);

			query = new Query();
			List<StoreStock> storeStockList = mongoTemplate.find(query.addCriteria(Criteria.where("materialsPurchasedId").is(materialsPurchasedId)),StoreStock.class);
			for(int i=0;i<storeStockList.size();i++)
			{
				if(i==0)
				{
					challanNo=""+storeStockList.get(i).getChallanNo();
					grnNo=""+storeStockList.get(i).getGrnNo();
				}
				else
				{
					challanNo=challanNo+"/"+storeStockList.get(i).getChallanNo();
					grnNo=grnNo+"/"+storeStockList.get(i).getGrnNo();
				}
			}
			query = new Query();
			MaterialsPurchased materialPurchased = mongoTemplate.findOne(query.addCriteria(Criteria.where("materialsPurchasedId").is(materialsPurchasedId)), MaterialsPurchased.class);

			query = new Query();
			MaterialPurchaseRequisition materialPurchaseRequisition = mongoTemplate.findOne(query.addCriteria(Criteria.where("requisitionId").is(materialPurchased.getRequisitionId())), MaterialPurchaseRequisition.class);

			query = new Query();
			Project projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(materialPurchaseRequisition.getProjectId())), Project.class);

			query = new Query();
			List<StoreStockHistory> storeStockHistory = mongoTemplate.find(query.addCriteria(Criteria.where("materialsPurchasedId").is(materialsPurchasedId)),StoreStockHistory.class);

			query = new Query();
			List<Company> companyDetails = mongoTemplate.find(query.addCriteria(Criteria.where("companyId").is(projectDetails.getCompanyId())), Company.class);

			query = new Query();
			Supplier supplierDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("supplierId").is(materialPurchased.getSupplierId())), Supplier.class);

			query = new Query();
			List<Store> storeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("storeId").is(materialPurchased.getStoreId())), Store.class);

			//project Address
			query =new Query();
			State projectStateDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("stateId").is(projectDetails.getStateId())), State.class);
			query =new Query();
			City projectCityDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("cityId").is(projectDetails.getCityId())), City.class);

			long totalAmount=0;
			long totalgrandAmount=0;
			long totalgstAmount=0;
			long totaldiscountAmount=0;
			double previousQuantity;
			double remainingQuantity;
			long previousNoOfPieces=0;
			double total;
			double discountAmount;
			double gstAmount;
			double grandTotal;
			for(int i=0;i<materialPurchasedHistory.size();i++)
			{
				try {
					previousQuantity=0.0;
					remainingQuantity=0.0;
					previousNoOfPieces=0;
					total=0.0;
					discountAmount=0.0;
					gstAmount=0.0;
					for(int j=0;j<storeStockHistory.size();j++)
					{
						if(storeStockHistory.get(j).getItemId().equalsIgnoreCase(materialPurchasedHistory.get(i).getItemId()))
						{
							previousQuantity=previousQuantity+storeStockHistory.get(j).getItemQuantity();
							previousNoOfPieces=previousNoOfPieces+storeStockHistory.get(j).getNoOfPieces();

						}
					}


					remainingQuantity=materialPurchasedHistory.get(i).getItemQuantity()-previousQuantity;
					materialPurchasedHistory.get(i).setPreviousQuantity(previousQuantity);
					materialPurchasedHistory.get(i).setPreviousNoOfPieces(previousNoOfPieces);
					materialPurchasedHistory.get(i).setRemainingQuantity(remainingQuantity);

					materialPurchasedHistory.get(i).setItemQuantity(previousQuantity);
					total=previousQuantity*materialPurchasedHistory.get(i).getRate();
					discountAmount=(total/100)*materialPurchasedHistory.get(i).getDiscountPer();
					gstAmount=((total-discountAmount)/100)*materialPurchasedHistory.get(i).getGstPer();
					grandTotal=total-discountAmount+gstAmount;

					materialPurchasedHistory.get(i).setTotal(total);
					materialPurchasedHistory.get(i).setDiscountAmount(discountAmount);
					materialPurchasedHistory.get(i).setGstAmount(gstAmount);
					materialPurchasedHistory.get(i).setGrandTotal(grandTotal);

					totalAmount=(long) (totalAmount+total);
					totalgrandAmount=(long) (totalgrandAmount+grandTotal);
					totalgstAmount=(long) (totalgstAmount+gstAmount);
					totaldiscountAmount=(long) (totaldiscountAmount+discountAmount);
				}catch (Exception e) {
					// TODO: handle exception
				}
			}



			long totalAmount1=0;
			long transportCharges=0;
			long totalchargeWithtransport=0;
			long gstAmt=0;
			long amount=0;
			for(int i=0;i<materialPurchasedHistory.size();i++)
			{
				jmap = new HashMap();
				jmap.put("srno",""+(i+1));
				jmap.put("gstPer",""+(long)materialPurchasedHistory.get(i).getGstPer());
				jmap.put("itemName"," "+materialPurchasedHistory.get(i).getItemName());
				jmap.put("unit", ""+materialPurchasedHistory.get(i).getItemUnit());
				jmap.put("qty",""+(long)materialPurchasedHistory.get(i).getItemQuantity());
				jmap.put("unitRate",""+(long)materialPurchasedHistory.get(i).getRate());

				jmap.put("disc",""+(long)materialPurchasedHistory.get(i).getDiscountPer());

				jmap.put("amount",""+(long)materialPurchasedHistory.get(i).getTotal());
				jmap.put("grandAmount",""+(long)materialPurchasedHistory.get(i).getGrandTotal());

				c.add(jmap);
				totalAmount1=totalAmount1+(long)materialPurchasedHistory.get(i).getGrandTotal();
				gstAmt=gstAmt+(long)materialPurchasedHistory.get(i).getGstAmount();
				amount=amount+(long)materialPurchasedHistory.get(i).getTotal();
				jmap = null;
			}

			double sgstAmount=0.0;
			double cgstAmount=0.0;
			double gsttransportCharges=0.0;
			double gstAllAmount=0.0;
			if(materialPurchased.getTransportCharges()!=null)
			{
				transportCharges=materialPurchased.getTransportCharges().longValue();
				gsttransportCharges=(transportCharges/100)*materialPurchasedHistory.get(0).getGstPer();
			}

			gstAllAmount=gstAmt+gsttransportCharges;
			sgstAmount=gstAllAmount/2;
			cgstAmount=gstAllAmount/2;
			totalchargeWithtransport=(long) (totalAmount1+transportCharges+gsttransportCharges);
/*
			jmap = new HashMap();
			jmap.put("srno","");
			jmap.put("gstPer",""+gstAmt);
			jmap.put("itemName","Total");
			jmap.put("unit", "");
			jmap.put("qty","");
			jmap.put("unitRate","");
			jmap.put("disc","");
			jmap.put("amount",""+amount);
			jmap.put("grandAmount",""+totalAmount1+"/-");
			c.add(jmap);
			jmap = null;
*/
			jmap = new HashMap();
			jmap.put("srno","");
			jmap.put("gstPer","");
			jmap.put("itemName"," Transport Charges  ");
			jmap.put("unit", "");
			jmap.put("qty","");
			jmap.put("unitRate","");
			jmap.put("disc","");
			jmap.put("amount","");
			jmap.put("grandAmount",""+transportCharges+"/-");
			c.add(jmap);
			jmap = null;

			jmap = new HashMap();
			jmap.put("srno","");
			jmap.put("gstPer"," SGST");
			jmap.put("itemName","");
			jmap.put("unit", "");
			jmap.put("qty","");
			jmap.put("unitRate","");
			jmap.put("disc","");
			jmap.put("amount","");
			jmap.put("grandAmount",""+sgstAmount+"/-");
			c.add(jmap);
			jmap = null;
			

			jmap = new HashMap();
			jmap.put("srno","");
			jmap.put("gstPer"," CGST");
			jmap.put("itemName","");
			jmap.put("unit", "");
			jmap.put("qty","");
			jmap.put("unitRate","");
			jmap.put("disc","");
			jmap.put("amount","");
			jmap.put("grandAmount",""+cgstAmount+"/-");
			c.add(jmap);
			jmap = null;
			
			
			
			jmap = new HashMap();
			jmap.put("srno","");
			jmap.put("gstPer","");
			jmap.put("itemName","  Total  ");
			jmap.put("unit", "");
			jmap.put("qty","");
			jmap.put("unitRate","");
			jmap.put("disc","");
			jmap.put("amount","");
			jmap.put("grandAmount",""+totalchargeWithtransport+"/-");
			c.add(jmap);
			jmap = null;


			String realPath =CommanController.GetLogoImagePath();

			JRDataSource dataSource = new JRMapCollectionDataSource(c);
			Map<String, Object> parameterMap = new HashMap<String, Object>();

			Date date = new Date();  
			SimpleDateFormat formatter = new SimpleDateFormat("d/M/yyyy");  
			String strDate= formatter.format(date);

			String invoiceNo="";
			if(materialPurchased.getInvoiceNo()!=null)
			{
				invoiceNo=materialPurchased.getInvoiceNo();
			}
			String delivery=materialPurchased.getDelivery();
			String paymentterms=materialPurchased.getPaymentterms();
			String afterdelivery=materialPurchased.getAfterdelivery();

			/*1*/ parameterMap.put("companyNameAndAddress", ""+companyDetails.get(0).getCompanyName()+" ");
			/*2*/ parameterMap.put("realPath", ""+realPath);
			/*3*/ parameterMap.put("printDate", ""+strDate);
			/*4*/ parameterMap.put("supplierNameAndAddress", ""+supplierDetails.getSupplierfirmName()+", "+supplierDetails.getSupplierfirmAddress()+", "+supplierDetails.getSupplierPincode());
			/*5*/ parameterMap.put("pono", ""+materialPurchased.getMaterialsPurchasedId());
			/*6*/ parameterMap.put("poDate", ""+materialPurchased.getCreationDate());
			/*7*/ parameterMap.put("siteName", ""+projectDetails.getProjectName());
			/*8*/ parameterMap.put("reqNo", ""+materialPurchased.getRequisitionId());
			/*9*/ parameterMap.put("reqDate", ""+materialPurchaseRequisition.getCreationDate());
			/*10*/ parameterMap.put("total", ""+materialPurchased.getTotalPrice().longValue());
			/*11*/ parameterMap.put("gstAmount", ""+materialPurchased.getTotalGstAmount().longValue());
			/*12*/ parameterMap.put("grandTotal", ""+materialPurchased.getTotalAmount().longValue());
			/*13*/ parameterMap.put("companyName", ""+companyDetails.get(0).getCompanyName()+", "+companyDetails.get(0).getCompanyGstno());
			/*14*/ parameterMap.put("siteFullAddress", ""+projectDetails.getProjectAddress()+","+projectCityDetails.getCityName()+", "+projectStateDetails.getStateName()+"-"+projectDetails.getAreaPincode());
			/*15*/ parameterMap.put("contactPerson", "9527098100");
			/*16*/ parameterMap.put("supplierName", ""+supplierDetails.getSupplierfirmName());
			/*17*/ parameterMap.put("supplierAddress", ""+supplierDetails.getSupplierfirmAddress()+", "+supplierDetails.getSupplierPincode());

			/*18*/ parameterMap.put("invoiceNo", ""+invoiceNo);
			/*19*/ parameterMap.put("delivery", ""+delivery);
			/*20*/ parameterMap.put("paymentterms", ""+paymentterms);
			/*20*/ parameterMap.put("afterdelivery", ""+afterdelivery);
			/*20*/ parameterMap.put("billingAddress", companyDetails.get(0).getCompanyAddress()+"-"+companyDetails.get(0).getCompanyPincode());

			HttpSession session = request.getSession(true);
			List<Employee> userName = (List<Employee>) session.getAttribute("userName");

			/*21*/ parameterMap.put("userName", ""+userName.get(0).getEmployeefirstName()+" "+userName.get(0).getEmployeelastName());
			/*22*/ parameterMap.put("discountAmount", ""+materialPurchased.getTotalDiscount().longValue());
			/*23*/ parameterMap.put("challanNo", ""+challanNo);
			/*23*/ parameterMap.put("grnNo", ""+grnNo);

			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/MaterialPurchaseBillReceipt.jasper");

			print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(print, baos);

			byte[] contents = baos.toByteArray();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			String filename = "MaterialPurchaseBillReceipt.pdf";

			JasperExportManager.exportReportToPdfStream(print, baos);


			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

			ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
			response.setHeader("Content-Disposition", "inline; filename=" + filename );

			return resp;					    
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			return null;
		}

	}


	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value="/MaterialPurchaseOrderReceipt")
	public ResponseEntity<byte[]> MaterialPurchaseOrderReceipt(@RequestParam("materialsPurchasedId") String materialsPurchasedId, ModelMap model, HttpServletResponse response, HttpServletRequest request)
	{
		JasperPrint print;

		try 
		{	
			Query query = new Query();
			HashMap jmap = new HashMap();
			Collection c = new ArrayList();
			//String challanNo="";
			//String grnNo="";
			List<MaterialsPurchasedHistory> materialPurchasedHistory = mongoTemplate.find(query.addCriteria(Criteria.where("materialsPurchasedId").is(materialsPurchasedId)), MaterialsPurchasedHistory.class);

			query = new Query();
			List<StoreStock> storeStockList = mongoTemplate.find(query.addCriteria(Criteria.where("materialsPurchasedId").is(materialsPurchasedId)),StoreStock.class);

			query = new Query();
			MaterialsPurchased materialPurchased = mongoTemplate.findOne(query.addCriteria(Criteria.where("materialsPurchasedId").is(materialsPurchasedId)), MaterialsPurchased.class);

			query = new Query();
			MaterialPurchaseRequisition materialPurchaseRequisition = mongoTemplate.findOne(query.addCriteria(Criteria.where("requisitionId").is(materialPurchased.getRequisitionId())), MaterialPurchaseRequisition.class);

			query = new Query();
			Project projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(materialPurchaseRequisition.getProjectId())), Project.class);

			query = new Query();
			List<Company> companyDetails = mongoTemplate.find(query.addCriteria(Criteria.where("companyId").is(projectDetails.getCompanyId())), Company.class);

			query = new Query();
			Supplier supplierDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("supplierId").is(materialPurchased.getSupplierId())), Supplier.class);


			//project Address
			query =new Query();
			State projectStateDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("stateId").is(projectDetails.getStateId())), State.class);
			query =new Query();
			City projectCityDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("cityId").is(projectDetails.getCityId())), City.class);


			long totalAmount=0;
			long transportCharges=0;
			long totalchargeWithtransport=0;
			long gstAmt=0;
			long amount=0;
			for(int i=0;i<materialPurchasedHistory.size();i++)
			{
				jmap = new HashMap();
				jmap.put("srno",""+(i+1));
				jmap.put("gstPer",""+(long)materialPurchasedHistory.get(i).getGstPer());
				jmap.put("itemName",""+materialPurchasedHistory.get(i).getItemName());
				jmap.put("unit", ""+materialPurchasedHistory.get(i).getItemUnit());
				jmap.put("qty",""+(long)materialPurchasedHistory.get(i).getItemQuantity());
				jmap.put("unitRate",""+(long)materialPurchasedHistory.get(i).getRate());

				jmap.put("disc",""+(long)materialPurchasedHistory.get(i).getDiscountPer());

				jmap.put("amount",""+(long)materialPurchasedHistory.get(i).getTotal());
				jmap.put("grandAmount",""+(long)materialPurchasedHistory.get(i).getGrandTotal());

				c.add(jmap);
				totalAmount=totalAmount+(long)materialPurchasedHistory.get(i).getGrandTotal();
				gstAmt=gstAmt+(long)materialPurchasedHistory.get(i).getGstAmount();
				amount=amount+(long)materialPurchasedHistory.get(i).getTotal();
				jmap = null;
			}


			double sgstAmount=0.0;
			double cgstAmount=0.0;
			double gsttransportCharges=0.0;
			double gstAllAmount=0.0;
			if(materialPurchased.getTransportCharges()!=null)
			{
				transportCharges=materialPurchased.getTransportCharges().longValue();
				gsttransportCharges=(transportCharges/100)*materialPurchasedHistory.get(0).getGstPer();
			}

			gstAllAmount=gstAmt+gsttransportCharges;
			sgstAmount=gstAllAmount/2;
			cgstAmount=gstAllAmount/2;
			totalchargeWithtransport=(long) (totalAmount+transportCharges+gsttransportCharges);
/*
			jmap = new HashMap();
			jmap.put("srno","");
			jmap.put("gstPer",""+gstAmt);
			jmap.put("itemName","Total");
			jmap.put("unit", "");
			jmap.put("qty","");
			jmap.put("unitRate","");
			jmap.put("disc","");
			jmap.put("amount",""+amount);
			jmap.put("grandAmount",""+totalAmount1+"/-");
			c.add(jmap);
			jmap = null;
*/
			jmap = new HashMap();
			jmap.put("srno","");
			jmap.put("gstPer","");
			jmap.put("itemName"," Transport Charges  ");
			jmap.put("unit", "");
			jmap.put("qty","");
			jmap.put("unitRate","");
			jmap.put("disc","");
			jmap.put("amount","");
			jmap.put("grandAmount",""+transportCharges+"/-");
			c.add(jmap);
			jmap = null;

			jmap = new HashMap();
			jmap.put("srno","");
			jmap.put("gstPer"," SGST");
			jmap.put("itemName","");
			jmap.put("unit", "");
			jmap.put("qty","");
			jmap.put("unitRate","");
			jmap.put("disc","");
			jmap.put("amount","");
			jmap.put("grandAmount",""+sgstAmount+"/-");
			c.add(jmap);
			jmap = null;
			

			jmap = new HashMap();
			jmap.put("srno","");
			jmap.put("gstPer"," CGST");
			jmap.put("itemName","");
			jmap.put("unit", "");
			jmap.put("qty","");
			jmap.put("unitRate","");
			jmap.put("disc","");
			jmap.put("amount","");
			jmap.put("grandAmount",""+cgstAmount+"/-");
			c.add(jmap);
			jmap = null;
			
			
			
			jmap = new HashMap();
			jmap.put("srno","");
			jmap.put("gstPer","");
			jmap.put("itemName","  Total  ");
			jmap.put("unit", "");
			jmap.put("qty","");
			jmap.put("unitRate","");
			jmap.put("disc","");
			jmap.put("amount","");
			jmap.put("grandAmount",""+totalchargeWithtransport+"/-");
			c.add(jmap);
			jmap = null;


			String realPath =CommanController.GetLogoImagePath();

			JRDataSource dataSource = new JRMapCollectionDataSource(c);
			Map<String, Object> parameterMap = new HashMap<String, Object>();

			Date date = new Date();  
			SimpleDateFormat formatter = new SimpleDateFormat("d/M/yyyy");  
			String strDate= formatter.format(date);

			//String invoiceNo="";
			String delivery=materialPurchased.getDelivery();
			String paymentterms=materialPurchased.getPaymentterms();
			String afterdelivery=materialPurchased.getAfterdelivery();

			/*1*/ parameterMap.put("companyNameAndAddress", ""+companyDetails.get(0).getCompanyName()+" ");
			/*2*/ parameterMap.put("realPath", ""+realPath);
			/*3*/ parameterMap.put("printDate", ""+strDate);
			/*4*/ parameterMap.put("supplierNameAndAddress", ""+supplierDetails.getSupplierfirmName()+", "+supplierDetails.getSupplierfirmAddress()+", "+supplierDetails.getSupplierPincode());
			/*5*/ parameterMap.put("pono", ""+materialPurchased.getMaterialsPurchasedId());
			/*6*/ parameterMap.put("poDate", ""+materialPurchased.getCreationDate());
			/*7*/ parameterMap.put("siteName", ""+projectDetails.getProjectName());
			/*8*/ parameterMap.put("reqNo", ""+materialPurchased.getRequisitionId());
			/*9*/ parameterMap.put("reqDate", ""+materialPurchaseRequisition.getCreationDate());
			/*10*/ parameterMap.put("total", ""+materialPurchased.getTotalPrice().longValue());
			/*11*/ parameterMap.put("gstAmount", ""+materialPurchased.getTotalGstAmount().longValue());
			/*12*/ parameterMap.put("grandTotal", ""+materialPurchased.getTotalAmount().longValue());
			/*13*/ parameterMap.put("companyName",companyDetails.get(0).getCompanyName()+" "+companyDetails.get(0).getCompanyGstno());
			/*14*/ parameterMap.put("siteFullAddress", ""+projectDetails.getProjectAddress()+","+projectCityDetails.getCityName()+", "+projectStateDetails.getStateName()+"-"+projectDetails.getAreaPincode());
			/*15*/ parameterMap.put("contactPerson", "9527098100");
			/*16*/ parameterMap.put("supplierName", ""+supplierDetails.getSupplierfirmName());
			/*17*/ parameterMap.put("supplierAddress", ""+supplierDetails.getSupplierfirmAddress()+", "+supplierDetails.getSupplierPincode());

			/*19*/ parameterMap.put("delivery", ""+delivery);
			/*20*/ parameterMap.put("paymentterms", ""+paymentterms);
			/*20*/ parameterMap.put("afterdelivery", ""+afterdelivery);
			/*20*/ parameterMap.put("billingAddress", companyDetails.get(0).getCompanyAddress()+"-"+companyDetails.get(0).getCompanyPincode());

			HttpSession session = request.getSession(true);
			List<Employee> userName = (List<Employee>) session.getAttribute("userName");

			/*21*/ parameterMap.put("userName", ""+userName.get(0).getEmployeefirstName()+" "+userName.get(0).getEmployeelastName());
			/*22*/ parameterMap.put("discountAmount", ""+materialPurchased.getTotalDiscount().longValue());

			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/MaterialPurchase.jasper");

			print = JasperFillManager.fillReport(inputStream, parameterMap, dataSource);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(print, baos);

			byte[] contents = baos.toByteArray();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			String filename = "MaterialPurchaseBillReceipt.pdf";

			JasperExportManager.exportReportToPdfStream(print, baos);


			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

			ResponseEntity<byte[]> resp = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
			response.setHeader("Content-Disposition", "inline; filename=" + filename );

			return resp;					    
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			return null;
		}

	}

}

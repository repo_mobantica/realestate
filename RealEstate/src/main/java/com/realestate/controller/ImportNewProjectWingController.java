package com.realestate.controller;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;

import java.io.InputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.realestate.bean.ProjectWing;
import com.realestate.bean.Login;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.ProjectWingRepository;
import com.realestate.repository.FloorRepository;
import com.realestate.repository.ProjectBuildingRepository;
import com.realestate.repository.ProjectRepository;

import java.io.FileInputStream;
import java.io.IOException;


@Controller
@RequestMapping("/")
public class ImportNewProjectWingController {

	@Autowired
	ServletContext context;

	@Autowired
	ProjectWingRepository projectWingRepository;
	@Autowired
	FloorRepository floorRepository;
	@Autowired
	ProjectRepository projectRepository;

	@Autowired
	ProjectBuildingRepository projectbuildingRepository;

	@Autowired
	MongoTemplate mongoTemplate;

	ProjectWing projectWing = new ProjectWing();
	HSSFWorkbook workbook;
	XSSFWorkbook workbook1;

	HSSFSheet worksheet;
	XSSFSheet worksheet1; 

	String wingCode;
	private String WingCode()
	{
		long wingCount = projectWingRepository.count();

		if(wingCount<10)
		{
			wingCode = "WG000"+(wingCount+1);
		}
		else if((wingCount>=10) && (wingCount<100))
		{
			wingCode = "WG00"+(wingCount+1);
		}
		else if((wingCount>=100) && (wingCount<1000))
		{
			wingCode = "WG0"+(wingCount+1);
		}
		else 
		{
			wingCode = "WG"+(wingCount+1);
		}

		return wingCode;
	}

	public List<Project> GetUserAssigenedProjectList(HttpServletRequest req,HttpServletResponse res)
	{
		try
		{
			List<UserAssignedProject> projectList1 = new ArrayList<UserAssignedProject>();
			List<Project> projectList = new ArrayList<Project>();

			String userId = (String)req.getSession().getAttribute("user");

			Query query = new Query();
			Login loginDetails = new Login();

			loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(userId)), Login.class);


			query = new Query();
			projectList1 = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), UserAssignedProject.class);


			Project project = new Project();
			for(int i=0;i<projectList1.size();i++)
			{
				project = new Project();
				query = new Query();
				project = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(projectList1.get(i).getProjectId())), Project.class);

				if(project !=null)
				{
					projectList.add(project);  
				}
			}
			return projectList;
		}
		catch(Exception e)
		{
			return null;
		}

	}

	@RequestMapping(value="/ImportNewProjectWing")
	public String importNewProjectWing(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
		return "ImportNewProjectWing";
	}



	@SuppressWarnings({ "deprecation"})
	@RequestMapping(value = "/ImportNewProjectWing", method = RequestMethod.POST)
	public String uploadProjectWingDetails(@RequestParam("projectWing_excel") MultipartFile projectWing_excel, Model model, HttpServletRequest req, HttpServletResponse res, RedirectAttributes redirectAttributes, HttpSession session) 
	{

		String user = (String)req.getSession().getAttribute("user");
		List<Project> projectList = GetUserAssigenedProjectList(req,res);
		Query query = new Query();
		List<ProjectBuilding> buildingList = projectbuildingRepository.findAll();


		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/M/yyyy");
		LocalDate localDate = LocalDate.now();
		String todayDate=dtf.format(localDate);
		String projectId="",buildingId="";
		int total=0;

		if (!projectWing_excel.isEmpty() || projectWing_excel != null )
		{
			try 
			{
				if(projectWing_excel.getOriginalFilename().endsWith("xls") || projectWing_excel.getOriginalFilename().endsWith("xlsx") || projectWing_excel.getOriginalFilename().endsWith("csv"))
				{
					if(projectWing_excel.getOriginalFilename().endsWith("xlsx"))
					{
						InputStream stream = projectWing_excel.getInputStream();
						XSSFWorkbook workbook = new XSSFWorkbook(stream);

						XSSFSheet sheet = workbook.getSheet("Sheet1");  /// this will read 1st workbook of ExcelSheet

						int firstRow = sheet.getFirstRowNum();

						XSSFRow firstrow = sheet.getRow(firstRow);

						@SuppressWarnings("unused")
						int lastColumnCount = firstrow.getLastCellNum();

						@SuppressWarnings("unused")
						Iterator<Row> rowIterator = sheet.iterator();   
						int last_no=sheet.getLastRowNum();

						ProjectWing projectWing = new ProjectWing();

						for(int i=0;i<last_no;i++)
						{
							projectId="";
							buildingId="";
							total=0;

							try {
								XSSFRow row = sheet.getRow(i+1);

								// Skip read heading 
								if (row.getRowNum() == 0) 
								{
									continue;
								}


								for(int j=0; j<projectList.size();j++)
								{
									if((projectList.get(j).getProjectName().trim()).equalsIgnoreCase((row.getCell(2).getStringCellValue().trim())))
									{
										projectId=projectList.get(j).getProjectId();
										break;
									}
								}

								for(int j=0; j<buildingList.size();j++)
								{
									if((buildingList.get(j).getBuildingName().trim()).equalsIgnoreCase((row.getCell(1).getStringCellValue().trim())))
									{
										buildingId=buildingList.get(j).getBuildingId();
										break;
									}
								}


								if(!projectId.equals("") && !buildingId.equals(""))
								{

									projectWing.setWingId(WingCode());

									row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
									projectWing.setWingName(row.getCell(0).getStringCellValue());

									projectWing.setBuildingId(buildingId);

									projectWing.setProjectId(projectId);

									row.getCell(3).setCellType(Cell.CELL_TYPE_NUMERIC);
									projectWing.setInfrastructureCharges((int)row.getCell(3).getNumericCellValue());

									row.getCell(4).setCellType(Cell.CELL_TYPE_NUMERIC);
									projectWing.setMaintenanceCharges((int)row.getCell(4).getNumericCellValue());

									row.getCell(5).setCellType(Cell.CELL_TYPE_NUMERIC);
									projectWing.setHandlingCharges((int)row.getCell(5).getNumericCellValue());


									row.getCell(6).setCellType(Cell.CELL_TYPE_NUMERIC);
									projectWing.setNoofBasement((int)row.getCell(6).getNumericCellValue());

									row.getCell(7).setCellType(Cell.CELL_TYPE_NUMERIC);
									projectWing.setNoofStilts((int)row.getCell(7).getNumericCellValue());

									row.getCell(8).setCellType(Cell.CELL_TYPE_NUMERIC);
									projectWing.setNoofPodiums((int)row.getCell(8).getNumericCellValue());

									row.getCell(9).setCellType(Cell.CELL_TYPE_NUMERIC);
									projectWing.setNoofFloors((int)row.getCell(9).getNumericCellValue());

									total=(int)row.getCell(6).getNumericCellValue()+(int)row.getCell(7).getNumericCellValue()+(int)row.getCell(8).getNumericCellValue()+(int)row.getCell(9).getNumericCellValue();

									projectWing.setTotal(total);

									row.getCell(10).setCellType(Cell.CELL_TYPE_STRING);
									projectWing.setWingName(row.getCell(10).getStringCellValue());

									projectWing.setCreationDate(todayDate);
									projectWing.setUpdateDate(todayDate);
									projectWing.setUserName(user);
									projectWingRepository.save(projectWing);
								}

							}
							catch (Exception ee) {
								System.out.println("kdf== "+ee);
							}

						}
						workbook.close();
					}

				}//if after inner if

			}
			catch(Exception e)
			{

			}
		}
		/*	
		List<ProjectWing> projectWingDetails;
		projectWingDetails = projectWingRepository.findAll();

		model.addAttribute("projectWingDetails", projectWingDetails);
		 */	
		return "ImportNewProjectWing";
	}
	@RequestMapping(value = "/DownloadProjectWingTemplate")
	public String downloadProjectWingTemplate(Model model, HttpServletResponse response, HttpServletRequest request, RedirectAttributes redirectAttributes, HttpSession session) throws IOException 
	{

		String filename = "NewProjectWing.xlsx";
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		String filepath = "//home"+"/"+"qaerp"+"/"+"public_html"+"/"+"Template/";
		response.setContentType("APPLICATION/OCTET-STREAM");
		response.setHeader("Content-Disposition", "attachment; filename=\""+ filename + "\"");


		FileInputStream fileInputStream = new FileInputStream(filepath+filename);

		int i;
		while ((i = fileInputStream.read()) != -1) {
			out.write(i);
		}
		fileInputStream.close();
		out.close();


		return "ImportNewProjectWing";
	}

}
